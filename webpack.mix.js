let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
  .scripts([
    'resources/assets/js/mustache.js',
    'resources/assets/js/plugins/loaders/pace.min.js',
    'resources/assets/js/core/libraries/jquery.min.js',
    'resources/assets/js/core/libraries/bootstrap.min.js',
    'resources/assets/js/plugins/loaders/blockui.min.js',
    'resources/assets/js/plugins/ui/nicescroll.min.js',
    'resources/assets/js/plugins/ui/drilldown.js',
    'resources/assets/js/core/libraries/jquery_ui/interactions.min.js',
    'resources/assets/js/core/libraries/jquery_ui/widgets.min.js',
    'resources/assets/js/core/libraries/jquery_ui/effects.min.js',
    'resources/assets/js/plugins/extensions/mousewheel.min.js',
    'resources/assets/js/plugins/forms/selects/select2.min.js',
    'resources/assets/js/plugins/forms/styling/uniform.min.js',
    'resources/assets/js/plugins/ui/moment/moment.min.js',
    'resources/assets/js/plugins/ui/fullcalendar/fullcalendar.min.js',
    'resources/assets/js/core/app.js',
    'resources/assets/js/plugins/ui/ripple.min.js',
    'resources/assets/js/plugins/tables/datatables/datatables.min.js',
    'resources/assets/js/plugins/tables/datatables/extensions/responsive.min.js',
  ], 'public/js/backend.js')
  .scripts([
    "resources/assets/js/plugins/notifications/bootbox.min.js"
  ], "public/js/bootbox.js")
  .scripts([
    'resources/assets/js/plugins/forms/tags/tagsinput.min.js',
    'resources/assets/js/plugins/forms/tags/tokenfield.min.js',
    'resources/assets/js/plugins/ui/prism.min.js',
    'resources/assets/js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js',
    'resources/assets/js/pages/form_tags_input.js'
  ], 'public/js/tags.js')
  .scripts([
    'resources/assets/js/plugins/forms/styling/switch.min.js',
    'resources/assets/js/plugins/forms/styling/switchery.min.js',
    'resources/assets/js/plugins/forms/styling/uniform.min.js',
    'resources/assets/js/pages/form_checkboxes_radios.js',
  ], 'public/js/switch.js')
  .scripts([
    'resources/assets/js/plugins/pickers/bootstrap-datepicker.js',
    'resources/assets/js/plugins/pickers/pickadate/picker.js',
    'resources/assets/js/plugins/pickers/pickadate/picker.date.js',
    'resources/assets/js/plugins/pickers/daterangepicker.js',
    'resources/assets/js/plugins/pickers/anytime.min.js',
  ], 'public/js/datepicker.js')
  .scripts([
    'resources/assets/js/plugins/notifications/bootbox.min.js',
    'resources/assets/js/plugins/notifications/sweet_alert.min.js',
    'resources/assets/js/pages/notification.js',
  ], "public/js/notification.js")
  .scripts([
    "resources/assets/js/pages/lov.js",
  ], "public/js/lov.js")
  .scripts([
    "resources/assets/js/pages/login.js",
  ], "public/js/login.js")

  .scripts([
    "resources/assets/js/pages/accessories_material_arrival.js",
  ], "public/js/accessories_material_arrival.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_in.js",
  ], "public/js/accessories_material_in.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_out.js",
  ], "public/js/accessories_material_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_stock_opname.js",
  ], "public/js/accessories_material_stock_opname.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_quality_control.js",
  ], "public/js/accessories_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_reverse_out.js",
  ], "public/js/accessories_material_reverse_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_import_material_reverse_out.js",
  ], "public/js/accessories_import_material_reverse_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_import_material_out.js",
  ], "public/js/accessories_import_material_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_import_material_out_carton.js",
  ], "public/js/accessories_import_material_out_carton.js")
  .scripts([
    "resources/assets/js/pages/accessories_import_out.js",
  ], "public/js/accessories_import_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_reverse_quality_control.js",
  ], "public/js/accessories_material_reverse_quality_control.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_stock.js",
  ], "public/js/accessories_material_stock.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_stock_approval.js",
  ], "public/js/accessories_material_stock_approval.js")
  .scripts([
    "resources/assets/js/pages/erp_material_requirement.js",
  ], "public/js/erp_material_requirement.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_material_preparation.js",
  ], "public/js/accessories_barcode_material_preparation.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_material_allocation.js",
  ], "public/js/accessories_barcode_material_allocation.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_bill_of_material.js",
  ], "public/js/accessories_barcode_bill_of_material.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_bill_of_material_import.js",
  ], "public/js/accessories_barcode_bill_of_material_import.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_adjustment.js",
  ], "public/js/accessories_barcode_adjustment.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_reroute.js",
  ], "public/js/accessories_barcode_reroute.js")
  .scripts([
    "resources/assets/js/pages/accessories_allocation_buyer.js",
  ], "public/js/accessories_allocation_buyer.js")
  .scripts([
    "resources/assets/js/pages/accessories_allocation_buyer_import.js",
  ], "public/js/accessories_allocation_buyer_import.js")
  .scripts([
    "resources/assets/js/pages/accessories_allocation_buyer_approval.js",
  ], "public/js/accessories_allocation_buyer_approval.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock.js",
  ], "public/js/accessories_report_material_stock.js")
  .scripts([
    "resources/assets/js/pages/accessories_allocation_non_buyer.js",
  ], "public/js/accessories_allocation_non_buyer.js")
  .scripts([
    "resources/assets/js/pages/accessories_allocation_non_buyer_import.js",
  ], "public/js/accessories_allocation_non_buyer_import.js")
  .scripts([
    "resources/assets/js/pages/accessories_locator.js",
  ], "public/js/accessories_locator.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_daily_material_arrival.js",
  ], "public/js/accessories_report_daily_material_arrival.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_daily_material_preparation.js",
  ], "public/js/accessories_report_daily_material_preparation.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_daily_material_out.js",
  ], "public/js/accessories_report_daily_material_out.js")
  .scripts([
    "resources/assets/js/pages/report_material_movement.js",
  ], "public/js/report_material_movement.js")
  .scripts([
    "resources/assets/js/pages/report_memo_request_production.js",
  ], "public/js/report_memo_request_production.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock_delete_bulk.js",
  ], "public/js/accessories_report_material_stock_delete_bulk.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock_change_type_bulk.js",
  ], "public/js/accessories_report_material_stock_change_type_bulk.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_quality_control.js",
  ], "public/js/accessories_report_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_bapb.js",
  ], "public/js/accessories_report_material_bapb.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_stock_on_the_fly.js",
  ], "public/js/accessories_material_stock_on_the_fly.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_material_quality_control.js",
  ], "public/js/accessories_barcode_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_integration_movement_reject.js",
  ], "public/js/accessories_report_integration_movement_reject.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_monitoring_allocation.js",
  ], "public/js/accessories_report_material_monitoring_allocation.js")
  
  .scripts([
    "resources/assets/js/pages/fabric_barcode_supplier.js",
  ], "public/js/fabric_barcode_supplier.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock.js",
  ], "public/js/fabric_report_material_stock.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_in.js",
  ], "public/js/fabric_material_in.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_planning.js",
  ], "public/js/fabric_material_planning.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_preparation.js",
  ], "public/js/fabric_material_preparation.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_out_cutting.js",
  ], "public/js/fabric_material_out_cutting.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_quality_control.js",
  ], "public/js/fabric_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_out_non_cutting.js",
  ], "public/js/fabric_material_out_non_cutting.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_inspect_lab.js",
  ], "public/js/fabric_material_inspect_lab.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_testing.js",
  ], "public/js/fabric_material_testing.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_inspect_lab.js",
  ], "public/js/fabric_report_material_inspect_lab.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_quality_control.js",
  ], "public/js/fabric_report_daily_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_arrival.js",
  ], "public/js/fabric_report_daily_material_arrival.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_out_non_cutting.js",
  ], "public/js/fabric_report_daily_material_out_non_cutting.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_out.js",
  ], "public/js/fabric_report_daily_material_out.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_bapb.js",
  ], "public/js/fabric_report_material_bapb.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_summary_batch_supplier.js",
  ], "public/js/fabric_report_summary_batch_supplier.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_preparation.js",
  ], "public/js/fabric_report_daily_material_preparation.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_quality_control.js",
  ], "public/js/fabric_report_material_quality_control.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_quality_control_detail_points.js",
  ], "public/js/fabric_report_material_quality_control_detail_points.js")
  .scripts([
    "resources/assets/js/pages/fabric_locator.js",
  ], "public/js/fabric_locator.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_stock_opname.js",
  ], "public/js/fabric_material_stock_opname.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_stock_opname_remove_stock.js",
  ], "public/js/fabric_material_stock_opname_remove_stock.js")
  .scripts([
    "resources/assets/js/pages/fabric_allocation.js",
  ], "public/js/fabric_allocation.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_reject.js",
  ], "public/js/fabric_material_reject.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_stock.js",
  ], "public/js/fabric_material_stock.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_stock_approval.js",
  ], "public/js/fabric_material_stock_approval.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_cancel_preparation.js",
  ], "public/js/fabric_material_cancel_preparation.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_cancel_out_non_cutting.js",
  ], "public/js/fabric_material_cancel_out_non_cutting.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_arrival.js",
  ], "public/js/fabric_material_arrival.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_receive_piping.js",
  ], "public/js/fabric_material_receive_piping.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_cutting_instruction.js",
  ], "public/js/fabric_report_daily_cutting_instruction.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_request_fabric.js",
  ], "public/js/fabric_report_daily_request_fabric.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_integration_movement_reject.js",
  ], "public/js/fabric_report_integration_movement_reject.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_integration_out_issue.js",
  ], "public/js/fabric_report_integration_out_issue.js")
  .scripts([
    "resources/assets/js/pages/fabric_material_steam.js",
  ], "public/js/fabric_material_steam.js")

  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation.js",
  ], "public/js/master_data_auto_allocation.js")
  .scripts([
    "resources/assets/js/pages/account_setting.js",
  ], "public/js/account_setting.js")
  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation_delete.js",
  ], "public/js/master_data_auto_allocation_delete.js")
  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation_recalculate.js",
  ], "public/js/master_data_auto_allocation_recalculate.js")
  .scripts([
    "resources/assets/js/pages/master_data_material_saving.js",
  ], "public/js/master_data_material_saving.js")
  .scripts([
    "resources/assets/js/pages/master_data_po_buyer.js",
  ], "public/js/master_data_po_buyer.js")
  .scripts([
    "resources/assets/js/pages/master_data_purchase_item.js",
  ], "public/js/master_data_purchase_item.js")
  .scripts([
    "resources/assets/js/pages/master_data_defect.js",
  ], "public/js/master_data_defect.js")
  .scripts([
    "resources/assets/js/pages/master_data_fabric_testing.js",
  ], "public/js/master_data_fabric_testing.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_overview_testing_lab.js",
  ], "public/js/fabric_report_overview_testing_lab.js")
  .scripts([
    "resources/assets/js/pages/detail_master_data_fabric_testing.js",
  ], "public/js/detail_master_data_fabric_testing.js")
  .scripts([
    "resources/assets/js/pages/master_data_mapping_stock.js",
  ], "public/js/master_data_mapping_stock.js")
  .scripts([
    "resources/assets/js/pages/master_data_item.js",
  ], "public/js/master_data_item.js")
  .scripts([
    "resources/assets/js/pages/master_data_po_supplier.js",
  ], "public/js/master_data_po_supplier.js")
  .scripts([
    "resources/assets/js/pages/master_data_material_print_bom.js",
  ], "public/js/master_data_material_print_bom.js")
  .scripts([
    "resources/assets/js/pages/master_data_permission.js",
  ], "public/js/master_data_permission.js")
  .scripts([
    "resources/assets/js/pages/master_data_user.js",
  ], "public/js/master_data_user.js")
  .scripts([
    "resources/assets/js/pages/master_data_reroute_buyer.js",
  ], "public/js/master_data_reroute_buyer.js")

  .scripts([
    "resources/assets/js/pages/search.js",
  ], "public/js/search.js")
  .scripts([
    "resources/assets/js/pages/memo_request_production.js",
  ], "public/js/memo_request_production.js")
  .scripts([
    "resources/assets/js/pages/master_data_role.js",
  ], "public/js/master_data_role.js")
  .scripts([
    "resources/assets/js/pages/switch_allocation_accessories.js",
  ], "public/js/switch_allocation_accessories.js")
  .scripts([
    "resources/assets/js/pages/print_barcode_switch_accessories.js",
  ], "public/js/print_barcode_switch_accessories.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock_opname.js",
  ], "public/js/fabric_report_material_stock_opname.js")
  .scripts([
    "resources/assets/js/pages/master_data_destination.js",
  ], "public/js/master_data_destination.js")
  .scripts([
    "resources/assets/js/pages/master_data_area_and_locator.js",
  ], "public/js/master_data_area_and_locator.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock_opname.js",
  ], "public/js/accessories_report_material_stock_opname.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock_material_management.js",
  ], "public/js/fabric_report_material_stock_material_management.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock_edit_bulk.js",
  ], "public/js/accessories_report_material_stock_edit_bulk.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock_material_management_change_type_bulk.js",
  ], "public/js/fabric_report_material_stock_material_management_change_type_bulk.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock_material_management_delete_bulk.js",
  ], "public/js/fabric_report_material_stock_material_management_delete_bulk.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_stock_material_management_edit_bulk.js",
  ], "public/js/fabric_report_material_stock_material_management_edit_bulk.js")  
  .scripts([
    "resources/assets/js/pages/fabric_report_material_receive_piping.js",
  ], "public/js/fabric_report_material_receive_piping.js") 
  .scripts([
    "resources/assets/js/pages/fabric_report_material_reject.js",
  ], "public/js/fabric_report_material_reject.js") 
  .scripts([
    "resources/assets/js/pages/fabric_report_material_balance_marker.js",
  ], "public/js/fabric_report_material_balance_marker.js")
  .scripts([
    "resources/assets/js/pages/accessories_barcode_carton.js",
  ], "public/js/accessories_barcode_carton.js") 
  .scripts([
    "resources/assets/js/pages/accessories_report_allocation_cancel_order.js",
  ], "public/js/accessories_report_allocation_cancel_order.js")   
  .scripts([
    "resources/assets/js/pages/accessories_report_allocation_carton.js",
  ], "public/js/accessories_report_allocation_carton.js")   
  .scripts([
    "resources/assets/js/pages/accessories_report_material_ila.js",
  ], "public/js/accessories_report_material_ila.js")
  .scripts([
    "resources/assets/js/pages/accessories_material_metal_detector.js",
  ], "public/js/accessories_material_metal_detector.js") 
  .scripts([
    "resources/assets/js/pages/fabric_report_summary_material_stock.js",
  ], "public/js/fabric_report_summary_material_stock.js")    
  .scripts([
    "resources/assets/js/pages/accessories_report_summary_material_stock.js",
  ], "public/js/accessories_report_summary_material_stock.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_metal_detector.js",
  ], "public/js/accessories_report_material_metal_detector.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_daily_material_monitoring.js",
  ], "public/js/fabric_report_daily_material_monitoring.js")    
  .scripts([
    "resources/assets/js/pages/accessories_report_material_outstanding_cancel_order.js",
  ], "public/js/accessories_report_material_outstanding_cancel_order.js")    
  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation_eta_delay.js",
  ], "public/js/master_data_auto_allocation_eta_delay.js")
  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation_recalculate_dashboard.js",
  ], "public/js/master_data_auto_allocation_recalculate_dashboard.js")
  .scripts([
    "resources/assets/js/pages/master_data_auto_allocation_remove_from_dashboard.js",
  ], "public/js/master_data_auto_allocation_remove_from_dashboard.js")
  .scripts([
    "resources/assets/js/pages/fabric_print_surat_jalan.js",
  ], "public/js/fabric_print_surat_jalan.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_outstanding_material_out.js",
  ], "public/js/fabric_report_outstanding_material_out.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_balance_marker_create_allocation.js",
  ], "public/js/fabric_report_material_balance_marker_create_allocation.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_stock_change_source_bulk.js",
  ], "public/js/accessories_report_material_stock_change_source_bulk.js")
  .scripts([
    "resources/assets/js/pages/sync_per_item.js",
  ], "public/js/sync_per_item.js")
  .scripts([
    "resources/assets/js/pages/upload_fabric_testing.js",
  ], "public/js/upload_fabric_testing.js")
  .scripts([
    'resources/assets/js/plugins/chartjs/highcharts.js',
  ],'public/js/highcharts.js')    
  .styles([
    'resources/assets/css/font.css',
    'resources/assets/css/icons/icomoon/styles.css',
    'resources/assets/css/bootstrap.css',
    'resources/assets/css/core.css',
    'resources/assets/css/components.css',
    'resources/assets/css/colors.css',
    'resources/assets/css/custom.css'
  ], 'public/css/backend.css')
  .styles([
    "resources/assets/css/print_barcode_fabric.css",
  ], "public/css/print_barcode_fabric.css")
  .styles([
    'resources/assets/css/print_barcode_wpo.css'
  ], 'public/css/print_barcode_wpo.css')
  .styles([
    'resources/assets/css/print_barcode_receive.css'
  ], 'public/css/print_barcode_receive.css')
  .styles([
    'resources/assets/css/barcode_adjustment.css'
  ], 'public/css/barcode_adjustment.css')
  .styles([
    'resources/assets/css/print_mrp.css'
  ], 'public/css/print_mrp.css')
  .styles([
    'resources/assets/css/barcode_area.css'
  ], 'public/css/barcode_area.css')
  .styles([
    'resources/assets/css/barcode_receive_existing.css'
  ], 'public/css/barcode_receive_existing.css')
  .styles([
    'resources/assets/css/barcode_rack_fabric.css'
  ], 'public/css/barcode_rack_fabric.css')
  .scripts([
    "resources/assets/js/pages/fabric_report_material_in.js",
  ], "public/js/fabric_report_material_in.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_out.js",
  ], "public/js/fabric_report_material_out.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_material_adjustment.js",
  ], "public/js/fabric_report_material_adjustment.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_in.js",
  ], "public/js/accessories_report_material_in.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_out.js",
  ], "public/js/accessories_report_material_out.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_adjustment.js",
  ], "public/js/accessories_report_material_adjustment.js")
  .scripts([
    "resources/assets/js/pages/accessories_report_material_out_subcont.js",
  ], "public/js/accessories_report_material_out_subcont.js")
  .scripts([
    "resources/assets/js/pages/fabric_report_preparation_machine.js",
  ], "public/js/fabric_report_preparation_machine.js")
  .copyDirectory('resources/assets/fonts', 'public/css/fonts')
  .copyDirectory('resources/assets/images', 'public/images')
  .copyDirectory('resources/assets/css/icons', 'public/css/icons')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .version();