<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('memo-request-production')->group(function () 
{
    Route::get('', 'MemoRequestProductionController@index')->name('memoRequestProduction.index');
    Route::get('po-buyer-picklist','MemoRequestProductionController@poBuyerPicklist')->name('memoRequestProduction.poBuyerPicklist');
    Route::get('data', 'MemoRequestProductionController@data')->name('memoRequestProduction.data');
    Route::get('export', 'MemoRequestProductionController@export')->name('memoRequestProduction.export');
    Route::get('print', 'MemoRequestProductionController@printPreview')->name('memoRequestProduction.print');
    Route::get('download-all', 'MemoRequestProductionController@downloadAll')->name('memoRequestProduction.downloadAll');
    Route::get('history-data', 'MemoRequestProductionController@dataHistory')->name('memoRequestProduction.dataHistory');
});

Route::get('/mrp', ['uses' => 'HomeController@MRP'])->name('home.mrp');
Route::get('/cutting/reprepare-roll-buyer/{id}', 'FabricReportDailyMaterialPreparationController@rePrepareRollToBuyer')->name('fabricReportDailyMaterialPreparation.rePrepareRollToBuyer');
Route::get('/mrp/print-mrp', ['uses' => 'HomeController@printMrpReport'])->name('home.printMrpReport');
Route::get('/mrp/export-mrp', ['uses' => 'HomeController@exportMrpReport'])->name('home.exportMrpReport');
Route::get('/mrp/export-all-mrp', ['uses' => 'HomeController@exportAllMrpReport'])->name('home.exportAllMrpReport');
Route::get('/home/material-requirement', ['uses' => 'HomeController@materialRequirementIndex'])->name('home.materialRequirementIndex');
Route::get('/home/material-requirement/export', 'HomeController@materialRequirementIndexExport')->name('home.materialRequirementExport');
Route::post('/home/material-requirement/import', 'HomeController@materialRequirementImport')->name('home.materialRequirementImport');
Route::post('/home/material-requirement/store', ['uses' => 'HomeController@materialRequirementStore'])->name('home.materialRequirementStore');


Route::get('/', function () 
{
    return redirect('/home');
});

//dashboard
Route::get('/dashboard', function () {
    return view('dashboard.home');
})->name('dashboard1');

Route::get('/dashboard/preparation_fabric/aoi1', 'DashboardController@preparation_fabric1')->name('prepare1');
Route::get('/dashboard/preparation_fabric/aoi2', 'DashboardController@preparation_fabric2')->name('prepare2');

Route::get('/dashboard/status_fabric/aoi1', 'DashboardController@FabStatusAoi1')->name('statusfab1');
Route::get('/dashboard/status_fabric/aoi2', 'DashboardController@FabStatusAoi2')->name('statusfab2');

Route::get('/dashboard/receiving_acc/aoi1', 'DashboardController@AccReceivingAoi1')->name('rcv1');
Route::get('/dashboard/receiving_acc/aoi2', 'DashboardController@AccReceivingAoi2')->name('rcv2');


//dashboard
Route::get('/dashboard/preparation', 'DashboardController@preparationAoi2')->name('dashboard.preparation');
Route::get('/dashboard/preparation-1', 'DashboardController@preparationAoi1')->name('dashboard.preparation');

Auth::routes();

Route::middleware(['auth'])->group(function () 
{

    Route::get('/home', function () {
        return view('home');
    })->name('dashboard');

    Route::prefix('accessories')->group(function () 
    {
        Route::prefix('material-arrival')->middleware(['permission:menu-material-receive-accessories'])->group(function () 
        {
            Route::get('','AccessoriesMaterialArrivalController@index')->name('accessoriesMaterialArrival.index');
            Route::get('create','AccessoriesMaterialArrivalController@create')->name('accessoriesMaterialArrival.create');
            Route::post('store', 'AccessoriesMaterialArrivalController@store')->name('accessoriesMaterialArrival.store');
        });

        Route::prefix('material-in')->middleware(['permission:menu-material-in-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialInController@index')->name('accessoriesMaterialIn.index');
            Route::get('create', 'AccessoriesMaterialInController@create')->name('accessoriesMaterialIn.create');
            Route::get('rack-picklist', 'AccessoriesMaterialInController@rackPicklist')->name('accessoriesMaterialIn.rackPicklist');
            Route::post('store', 'AccessoriesMaterialInController@store')->name('accessoriesMaterialIn.store');
        });

        Route::prefix('material-out')->middleware(['permission:menu-material-out-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialOutController@index')->name('accessoriesMaterialOut.index');
            Route::get('create', 'AccessoriesMaterialOutController@create')->name('accessoriesMaterialOut.create');
            Route::get('import', 'AccessoriesMaterialOutController@import')->name('accessoriesMaterialOut.import');
            Route::get('download-form-upload', 'AccessoriesMaterialOutController@downloadFormUpload')->name('accessoriesMaterialOut.downloadFormUpload');
            Route::get('destination-picklist', 'AccessoriesMaterialOutController@destinationPicklist')->name('accessoriesMaterialOut.rackPicklist');
            Route::post('store', 'AccessoriesMaterialOutController@store')->name('accessoriesMaterialOut.store');
            Route::post('upload', 'AccessoriesMaterialOutController@upload')->name('accessoriesMaterialOut.upload');
            Route::get('import-carton', 'AccessoriesMaterialOutController@importCarton')->name('accessoriesMaterialOut.importCarton');
            Route::get('download-form-upload', 'AccessoriesMaterialOutController@downloadFormUploadCarton')->name('accessoriesMaterialOut.downloadFormUploadCarton');
            Route::post('upload-carton', 'AccessoriesMaterialOutController@uploadCarton')->name('accessoriesMaterialOut.uploadCarton');
            Route::get('import-out', 'AccessoriesMaterialOutController@importOut')->name('accessoriesMaterialOut.importOut');
            Route::get('download-form-upload-out', 'AccessoriesMaterialOutController@downloadFormUploadOut')->name('accessoriesMaterialOut.downloadFormUploadOut');
            Route::post('upload-out', 'AccessoriesMaterialOutController@uploadOut')->name('accessoriesMaterialOut.uploadOut');
        });

        Route::prefix('material-stock-opname')->middleware(['permission:menu-material-stock-opname-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialStockOpnameController@index')->name('accessoriesMaterialStockOpname.index');
            Route::get('create', 'AccessoriesMaterialStockOpnameController@create')->name('accessoriesMaterialStockOpname.create');
            Route::get('locator-picklist', 'AccessoriesMaterialStockOpnameController@locatorPicklist')->name('accessoriesMaterialStockOpname.locatorPicklist');
            Route::post('store', 'AccessoriesMaterialStockOpnameController@store')->name('accessoriesMaterialStockOpname.store');
        });

        Route::prefix('material-quality-control')->middleware(['permission:menu-material-check-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialQualityControlController@index')->name('accessoriesMaterialQualityControl.index');
            Route::get('create', 'AccessoriesMaterialQualityControlController@create')->name('accessoriesMaterialQualityControl.create');
            Route::post('store', 'AccessoriesMaterialQualityControlController@store')->name('accessoriesMaterialQualityControl.store');
        });

        Route::prefix('material-metal-detector')->middleware(['permission:menu-material-metal-detector-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialMetalDetectorController@index')->name('accessoriesMaterialMetalDetector.index');
            Route::get('create', 'AccessoriesMaterialMetalDetectorController@create')->name('accessoriesMaterialMetalDetector.create');
            Route::post('store', 'AccessoriesMaterialMetalDetectorController@store')->name('accessoriesMaterialMetalDetector.store');
        });

        Route::prefix('material-reverse-out')->middleware(['permission:menu-material-reverse-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialReverseOutController@index')->name('accessoriesMaterialReverseOut.index');
            Route::get('create', 'AccessoriesMaterialReverseOutController@create')->name('accessoriesMaterialReverseOut.create');
            Route::get('download-form-upload', 'AccessoriesMaterialReverseOutController@downloadFormUpload')->name('accessoriesMaterialReverseOut.downloadFormUpload');
            Route::get('import', 'AccessoriesMaterialReverseOutController@import')->name('accessoriesMaterialReverseOut.import');
            Route::post('upload', 'AccessoriesMaterialReverseOutController@upload')->name('accessoriesMaterialReverseOut.upload');
            Route::post('store', 'AccessoriesMaterialReverseOutController@store')->name('accessoriesMaterialReverseOut.store');
        });

        Route::prefix('material-stock-on-the-fly')->middleware(['permission:menu-material-stock-on-the-fly-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialStockOnTheFlyController@index')->name('accessoriesMaterialStockOnTheFly.index');
            Route::get('data', 'AccessoriesMaterialStockOnTheFlyController@data')->name('accessoriesMaterialStockOnTheFly.data');
            Route::get('export', 'AccessoriesMaterialStockOnTheFlyController@export')->name('accessoriesMaterialStockOnTheFly.export');
            Route::get('import', 'AccessoriesMaterialStockOnTheFlyController@import')->name('accessoriesMaterialStockOnTheFly.import');
            Route::get('download-form-import', 'AccessoriesMaterialStockOnTheFlyController@downloadFormImport')->name('accessoriesMaterialStockOnTheFly.downloadFormImport');
            Route::post('upload-form-import', 'AccessoriesMaterialStockOnTheFlyController@uploadFormImport')->name('accessoriesMaterialStockOnTheFly.uploadFormImport');
            Route::post('delete/{id}', 'AccessoriesMaterialStockOnTheFlyController@delete')->name('accessoriesMaterialStockOnTheFly.delete');
            Route::get('delete-bulk', 'AccessoriesMaterialStockOnTheFlyController@deleteFromBulk')->name('accessoriesMaterialStockOnTheFly.deleteFromBulk');
            Route::post('delete-export', 'AccessoriesMaterialStockOnTheFlyController@exportDelete')->name('accessoriesMaterialStockOnTheFly.exportDelete');
            Route::get('delete-import', 'AccessoriesMaterialStockOnTheFlyController@importDelete')->name('accessoriesMaterialStockOnTheFly.importDelete');


        });

        Route::prefix('material-reverse-quality-control')->middleware(['permission:menu-material-reverse-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialReverseQualityControlController@index')->name('accessoriesMaterialReverseQualityControl.index');
            Route::get('create', 'AccessoriesMaterialReverseQualityControlController@create')->name('accessoriesMaterialReverseQualityControl.create');
            Route::post('store', 'AccessoriesMaterialReverseQualityControlController@store')->name('accessoriesMaterialReverseQualityControl.store');
        });

        Route::prefix('material-stock')->middleware(['permission:menu-material-stock-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialStockController@index')->name('accessoriesMaterialStock.index');
            Route::get('data', 'AccessoriesMaterialStockController@data')->name('accessoriesMaterialStock.data');
            Route::get('create', 'AccessoriesMaterialStockController@create')->name('accessoriesMaterialStock.create');
            Route::get('export', 'AccessoriesMaterialStockController@export')->name('accessoriesMaterialStock.export');
            Route::get('export-supplier', 'AccessoriesMaterialStockController@exportSupplier')->name('accessoriesMaterialStock.exportSupplier');
            Route::get('edit/{id}', 'AccessoriesMaterialStockController@edit')->name('accessoriesMaterialStock.edit');
            Route::get('locator-picklist', 'AccessoriesMaterialStockController@locatorPicklist')->name('accessoriesMaterialStock.locatorPicklist');
            Route::get('item-picklist', 'AccessoriesMaterialStockController@itemPicklist')->name('accessoriesMaterialStock.itemPicklist');
            Route::get('po-supplier-picklist', 'AccessoriesMaterialStockController@poSupplierPicklist')->name('accessoriesMaterialStock.poSupplierPicklist');
            Route::get('supplier-name-picklist', 'AccessoriesMaterialStockController@supplierNamePickList')->name('accessoriesMaterialStock.supplierNamePickList');
            Route::get('criteria-picklist', 'AccessoriesMaterialStockController@criteriaPicklist')->name('accessoriesMaterialStock.criteriaPicklist');
            Route::put('update', 'AccessoriesMaterialStockController@update')->name('accessoriesMaterialStock.update');
            Route::post('store', 'AccessoriesMaterialStockController@store')->name('accessoriesMaterialStock.store');
            Route::post('import', 'AccessoriesMaterialStockController@import')->name('accessoriesMaterialStock.import');
            
        });

        Route::prefix('locator')->middleware(['permission:menu-accessories-locator'])->group(function () 
        {
            Route::get('', 'AccessoriesLocatorController@index')->name('accessoriesLocator.index');
            Route::get('data-rack-free-stock', 'AccessoriesLocatorController@dataRackFreeStock')->name('accessoriesLocator.dataRackFreeStock');
            Route::get('data-rack-non-free-stock', 'AccessoriesLocatorController@dataRackNonFreeStock')->name('accessoriesLocator.dataRackNonFreeStock');
            Route::get('buyer-picklist', 'AccessoriesLocatorController@poBuyerPickList')->name('accessoriesLocator.poBuyerPickList');
            
        });

        Route::prefix('material-stock-approval')->middleware(['permission:menu-approval-material-stock-accessories'])->group(function () 
        {
            Route::get('', 'AccessoriesMaterialStockApprovalController@index')->name('accessoriesMaterialStockApproval.index');
            Route::get('data', 'AccessoriesMaterialStockApprovalController@data')->name('accessoriesMaterialStockApproval.data');
            Route::get('locator-picklist', 'AccessoriesMaterialStockApprovalController@locatorPicklist')->name('accessoriesMaterialStockApproval.locatorPicklist');
            Route::get('edit/{id}/{table_name}', 'AccessoriesMaterialStockApprovalController@edit')->name('accessoriesMaterialStockApproval.edit');
            Route::put('approve-per-material/{id}/{table_name}', 'AccessoriesMaterialStockApprovalController@itemApprove')->name('accessoriesMaterialStockApproval.itemApprove');
            Route::put('reject/{id}/{table_name}', 'AccessoriesMaterialStockApprovalController@reject')->name('accessoriesMaterialStockApproval.reject');
            Route::post('approved-all', 'AccessoriesMaterialStockApprovalController@approveAll')->name('accessoriesMaterialStockApproval.approveAll');
            Route::post('approved-selected', 'AccessoriesMaterialStockApprovalController@approveSelected')->name('accessoriesMaterialStockApproval.approveSelected');
            Route::post('update', 'AccessoriesMaterialStockApprovalController@update')->name('accessoriesMaterialStockApproval.update');
            Route::post('store', 'AccessoriesMaterialStockApprovalController@store')->name('accessoriesMaterialStockApproval.store');
            Route::get('export-all', 'AccessoriesMaterialStockApprovalController@exportAll')->name('accessoriesMaterialStockApproval.exportAll');
                
        });

        Route::prefix('allocation')->group(function () 
        {
            Route::prefix('buyer')->middleware(['permission:menu-allocation-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesAllocationBuyerController@index')->name('accessoriesAllocationBuyer.index');
                Route::get('data', 'AccessoriesAllocationBuyerController@data')->name('accessoriesAllocationBuyer.data');
                Route::get('upload', 'AccessoriesAllocationBuyerController@upload')->name('accessoriesAllocationBuyer.upload');
                Route::get('export-file-upload', 'AccessoriesAllocationBuyerController@exportFileUpload')->name('accessoriesAllocationBuyer.exportFileUpload');
                Route::get('material-stock-picklist', 'AccessoriesAllocationBuyerController@materialStockPickList')->name('accessoriesAllocationBuyer.materialStockPickList');
                Route::get('pobuyer-picklist', 'AccessoriesAllocationBuyerController@poBuyerPickList')->name('accessoriesAllocationBuyer.poBuyerPickList');
                Route::get('export','AccessoriesAllocationBuyerController@export')->name('accessoriesAllocationBuyer.export');
                Route::put('delete/{id}', 'AccessoriesAllocationBuyerController@destroy')->name('accessoriesAllocationBuyer.destroy');
                Route::post('store', 'AccessoriesAllocationBuyerController@store')->name('accessoriesAllocationBuyer.store');
                Route::post('import','AccessoriesAllocationBuyerController@import')->name('accessoriesAllocationBuyer.import');
            });

            Route::prefix('non-buyer')->middleware(['permission:menu-allocation-non-buyer-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesAllocationNonBuyerController@index')->name('accessoriesAllocationNonBuyer.index');
                Route::get('data', 'AccessoriesAllocationNonBuyerController@data')->name('accessoriesAllocationNonBuyer.data');
                Route::get('upload', 'AccessoriesAllocationNonBuyerController@upload')->name('accessoriesAllocationNonBuyer.upload');
                Route::get('export-file-upload', 'AccessoriesAllocationNonBuyerController@exportFileUpload')->name('accessoriesAllocationNonBuyer.exportFileUpload');
                Route::get('material-stock-picklist', 'AccessoriesAllocationNonBuyerController@materialStockPickList')->name('accessoriesAllocationNonBuyer.materialStockPickList');
                Route::get('export', 'AccessoriesAllocationNonBuyerController@export')->name('accessoriesAllocationNonBuyer.export');
                Route::put('delete/{id}', 'AccessoriesAllocationNonBuyerController@destroy')->name('accessoriesAllocationNonBuyer.delete');
                Route::post('store', 'AccessoriesAllocationNonBuyerController@store')->name('accessoriesAllocationNonBuyer.store');
                Route::post('import','AccessoriesAllocationNonBuyerController@import')->name('accessoriesAllocationNonBuyer.import');

            });

            Route::prefix('switch')->middleware(['permission:menu-material-switch-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesMaterialSwitchController@index')->name('accessoriesMaterialSwitch.index');
                Route::get('export', 'AccessoriesMaterialSwitchController@export')->name('accessoriesMaterialSwitch.export');
                Route::get('edit', 'AccessoriesMaterialSwitchController@edit')->name('accessoriesMaterialSwitch.edit');
                Route::get('delete', 'AccessoriesMaterialSwitchController@delete')->name('accessoriesMaterialSwitch.delete');
                Route::get('create', 'AccessoriesMaterialSwitchController@create')->name('accessoriesMaterialSwitch.create');
                Route::get('export-file-upload', 'AccessoriesMaterialSwitchController@exportFileUpload')->name('accessoriesMaterialSwitch.exportFileUpload');
                Route::post('update', 'AccessoriesMaterialSwitchController@update')->name('accessoriesMaterialSwitch.update');
                Route::post('import', 'AccessoriesMaterialSwitchController@import')->name('accessoriesMaterialSwitch.import');
            });

            Route::prefix('approval')->middleware(['permission:menu-approve-allocation-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesAllocationBuyerApprovalController@index')->name('accessoriesAllocationBuyerApproval.index');
                Route::get('all', 'AccessoriesAllocationBuyerApprovalController@approveAll')->name('accessoriesAllocationBuyerApproval.approveAll');
                Route::get('data-approved', 'AccessoriesAllocationBuyerApprovalController@dataApproved')->name('accessoriesAllocationBuyerApproval.dataApproved');
                Route::get('data-waiting-for-approved', 'AccessoriesAllocationBuyerApprovalController@dataWaitingForApproved')->name('accessoriesAllocationBuyerApproval.dataWaitingForApproved');
                
                Route::put('approved/{id}', 'AccessoriesAllocationBuyerApprovalController@approved')->name('accessoriesAllocationBuyerApproval.approved');
                Route::put('reject/{id}', 'AccessoriesAllocationBuyerApprovalController@reject')->name('accessoriesAllocationBuyerApproval.reject');
                Route::delete('cancel-booking/{id}','AccessoriesAllocationBuyerApprovalController@cancelBooking')->name('accessoriesAllocationBuyerApproval.cancelBooking');
            });

            
        });

        Route::prefix('barcode')->group(function () 
        {
            Route::prefix('supplier')->middleware(['permission:menu-reprint-barcode-supplier-acc'])->group(function(){
                Route::get('','AccessoriesBarcodeSupplierController@index')->name('accessoriesBarcodeSupplier.index');
            });

            Route::prefix('quality-control')->middleware(['permission:menu-print-barcode-qc-accessories'])->group(function(){
                Route::get('','AccessoriesBarcodeQualityControlController@index')->name('accessoriesPrintBarcodeQualityControl.index');
                Route::get('po-buyer-picklist','AccessoriesBarcodeQualityControlController@poBuyerPicklist')->name('accessoriesPrintBarcodeQualityControl.poBuyerPicklist');
                Route::get('print','AccessoriesBarcodeQualityControlController@index')->name('accessoriesPrintBarcodeQualityControl.print');
            });

            Route::prefix('preparation')->middleware(['permission:menu-print-barcode-preparation-accessories'])->group(function(){
                Route::get('','AccessoriesBarcodeMaterialPreparationController@index')->name('accessoriesBarcodeMaterialPreparation.index');
                Route::get('create','AccessoriesBarcodeMaterialPreparationController@create')->name('accessoriesBarcodeMaterialPreparation.create');
                Route::get('critria-picklist','AccessoriesBarcodeMaterialPreparationController@critriaPicklist')->name('accessoriesBarcodeMaterialPreparation.critriaPicklist');
                Route::post('update-barcode-status','AccessoriesBarcodeMaterialPreparationController@updateBarcodeStatus')->name('accessoriesBarcodeMaterialPreparation.updateBarcodeStatus');
                Route::post('store','AccessoriesBarcodeMaterialPreparationController@store')->name('accessoriesBarcodeMaterialPreparation.store');
                Route::post('print-barcode','AccessoriesBarcodeMaterialPreparationController@barcode')->name('accessoriesBarcodeMaterialPreparation.print');
            });

            Route::prefix('allocation')->middleware(['permission:menu-print-barcode-allocation-accessories'])->group(function(){
                Route::get('','AccessoriesBarcodeMaterialAllocationController@index')->name('accessoriesBarcodeMaterialAllocation.index');
                Route::get('create','AccessoriesBarcodeMaterialAllocationController@create')->name('accessoriesBarcodeMaterialAllocation.create');
                Route::get('export','AccessoriesBarcodeMaterialAllocationController@export')->name('accessoriesBarcodeMaterialAllocation.export');
                Route::get('pobuyer-picklist','AccessoriesBarcodeMaterialAllocationController@poBuyerPicklist')->name('accessoriesBarcodeMaterialAllocation.poBuyerPicklist');
                Route::get('item-picklist','AccessoriesBarcodeMaterialAllocationController@itemPickList')->name('accessoriesBarcodeMaterialAllocation.itemPickList');
                Route::get('print-barcode','AccessoriesBarcodeMaterialAllocationController@barcode')->name('accessoriesBarcodeMaterialAllocation.print');
                Route::post('store','AccessoriesBarcodeMaterialAllocationController@store')->name('accessoriesBarcodeMaterialAllocation.store');
                Route::post('import','AccessoriesBarcodeMaterialAllocationController@import')->name('accessoriesBarcodeMaterialAllocation.import');
            });

            Route::prefix('locator')->middleware(['permission:menu-accessories-print-barcode-locator'])->group(function(){
                Route::get('','AccessoriesBarcodeLocatorController@index')->name('accessoriesBarcodeLocator.index');
                Route::get('print-out/{area}/{rack}', 'AccessoriesBarcodeLocatorController@printOut')->name('accessoriesBarcodeLocator.printOut');
            });

            Route::prefix('adjustment')->middleware(['permission:menu-print-material-adjustment'])->group(function(){
                Route::get('','AccessoriesBarcodeAdjustmentController@index')->name('accessoriesBarcodeAdjustment.index');
                Route::get('create','AccessoriesBarcodeAdjustmentController@create')->name('accessoriesBarcodeAdjustment.create');
                Route::get('printout','AccessoriesBarcodeAdjustmentController@printout')->name('accessoriesBarcodeAdjustment.printout');
                Route::post('store','AccessoriesBarcodeAdjustmentController@store')->name('accessoriesBarcodeAdjustment.store');
            });

            Route::prefix('reroute')->middleware(['permission:menu-print-barcode-reroute'])->group(function(){
                Route::get('','AccessoriesBarcodeReRouteController@index')->name('accessoriesBarcodeReRoute.index');
                Route::get('item-picklist','AccessoriesBarcodeReRouteController@itemPicklist')->name('accessoriesBarcodeReRoute.itemPicklist');
                Route::get('pobuyer-pick-list','AccessoriesBarcodeReRouteController@poBuyerPickList')->name('accessoriesBarcodeReRoute.poBuyerPickList');
                Route::post('printout','AccessoriesBarcodeReRouteController@printout')->name('accessoriesBarcodeReRoute.printout');
            });

            Route::prefix('bill-of-material')->middleware(['permission:menu-print-ila-accessories'])->group(function(){
                Route::get('','AccessoriesBarcodeBillOfMaterialController@index')->name('accessoriesBarcodeBillOfMaterial.index');
                Route::get('upload','AccessoriesBarcodeBillOfMaterialController@upload')->name('accessoriesBarcodeBillOfMaterial.upload');
                Route::get('upload/export-file-upload','AccessoriesBarcodeBillOfMaterialController@exportFileUpload')->name('accessoriesBarcodeBillOfMaterial.exportFileUpload');  
                Route::get('create', 'AccessoriesBarcodeBillOfMaterialController@create')->name('accessoriesBarcodeBillOfMaterial.create');
                Route::get('printout', 'AccessoriesBarcodeBillOfMaterialController@printout')->name('accessoriesBarcodeBillOfMaterial.print');
                Route::post('store', 'AccessoriesBarcodeBillOfMaterialController@store')->name('accessoriesBarcodeBillOfMaterial.store');
                Route::post('printout-all', 'AccessoriesBarcodeBillOfMaterialController@printoutAll')->name('accessoriesBarcodeBillOfMaterial.printoutAll');
                Route::post('import', 'AccessoriesBarcodeBillOfMaterialController@import')->name('accessoriesBarcodeBillOfMaterial.import');
                      
            });

            Route::prefix('switch')->middleware(['permission:menu-print-barcode-switch'])->group(function(){ 
                Route::get('','AccessoriesBarcodeSwitchController@index')->name('printBarcodeSwitch.index');
                Route::get('create','AccessoriesBarcodeSwitchController@create')->name('printBarcodeSwitch.create');
                Route::post('/barcode/accessories/print-barcode-switch','AccessoriesBarcodeSwitchController@printout')->name('printBarcodeSwitch.printout');
                Route::post('/barcode/accessories/print-barcode-switch/printout','AccessoriesBarcodeSwitchController@showBarcode')->name('printBarcodeSwitch.showBarcode');

            });

            Route::prefix('carton')->middleware(['permission:menu-print-barcode-carton'])->group(function () 
        {
            Route::get('', 'AccessoriesBarcodeCartonController@index')->name('accessoriesBarcodeCarton.index');
            Route::get('create', 'AccessoriesBarcodeCartonController@create')->name('accessoriesBarcodeCarton.create');
            Route::post('store', 'AccessoriesBarcodeCartonController@store')->name('accessoriesBarcodeCarton.store');
            Route::post('barcode', 'AccessoriesBarcodeCartonController@barcode')->name('accessoriesBarcodeCarton.barcode');
        });
        });

        Route::prefix('report')->group(function () 
        {
            Route::prefix('daily-material-arrival')->middleware(['permission:menu-report-receivement'])->group(function () 
            {
                Route::get('', 'AccessoriesReportDailyMaterialArrivalController@index')->name('accessoriesReportDailyMaterialArrival.index');
                Route::get('data', 'AccessoriesReportDailyMaterialArrivalController@data')->name('accessoriesReportDailyMaterialArrival.data');
                Route::get('export-to-excel', 'AccessoriesReportDailyMaterialArrivalController@export')->name('accessoriesReportDailyMaterialArrival.export');
            });

            Route::prefix('daily-material-preparation')->middleware(['permission:menu-monitoring-receiving'])->group(function () 
            {
                Route::get('', 'AccessoriesReportDailyMaterialPreparationController@index')->name('accessoriesReportDailyMaterialPreparation.index');
                Route::get('data', 'AccessoriesReportDailyMaterialPreparationController@data')->name('accessoriesReportDailyMaterialPreparation.export');
                Route::get('export-to-excel', 'AccessoriesReportDailyMaterialPreparationController@export')->name('accessoriesReportDailyMaterialPreparation.export');
                Route::get('print-preview', 'AccessoriesReportDailyMaterialPreparationController@printPreview')->name('accessoriesReportDailyMaterialPreparation.printPreview');
            });

            Route::prefix('daily-material-out')->middleware(['permission:menu-monitoring-checkout'])->group(function () 
            {
                Route::get('', 'AccessoriesReportDailyMaterialOutController@index')->name('accessoriesReportDailyMaterialOut.index');
                Route::get('data', 'AccessoriesReportDailyMaterialOutController@data')->name('accessoriesReportDailyMaterialOut.date');
                Route::get('export', 'AccessoriesReportDailyMaterialOutController@export')->name('accessoriesReportDailyMaterialOut.export');
            });

            Route::prefix('integration-monitoring-uncomplete-mr')->middleware(['permission:menu-monitoring-checkout'])->group(function () 
            {
                //    BELUM TAU PENTING PA GAK NYA
                Route::get('/report/integration/monitoring-uncomplete-mr', ['middleware' => ['permission:menu-monitoring-uncomplete-mr'], 'uses' => 'ReportController@monitorUnCompleteMR'])->name('report.monitorUnCompleteMR');
                Route::get('/report/integration/monitoring-uncomplete-mr/total-item', ['middleware' => ['permission:menu-monitoring-uncomplete-mr'], 'uses' => 'ReportController@monitorTotalItemMr'])->name('report.monitorTotalItemMr');
    
            });

            Route::prefix('integration-monitoring-uncomplete-mr')->middleware(['permission:menu-monitoring-checkout'])->group(function () 
            {
                //    BELUM TAU PENTING PA GAK NYA
                Route::get('/report/accessories/monitoring-purchase-order', ['middleware' => ['permission:menu-monitoring-purchase-order'], 'uses' => 'ReportController@monitorPurchaseOrder'])->name('report.purchaseOrder.index');
                Route::get('/report/accessories/monitoring-purchase-order/po-buyer-picklist', ['middleware' => ['permission:menu-monitoring-purchase-order'], 'uses' => 'ReportController@monitorPurchaseOrderPoBuyer'])->name('report.purchaseOrder.poBuyer');
                Route::get('/report/accessories/monitoring-purchase-order/list', ['middleware' => ['permission:menu-monitoring-purchase-order'], 'uses' => 'ReportController@monitorPurchaseOrderList'])->name('report.purchaseOrder.list');
                Route::post('/report/accessories/monitoring-purchase-order/export-to-excel', ['middleware' => ['permission:menu-monitoring-purchase-order'], 'uses' => 'ReportController@monitorPurchaseOrderToExcel'])->name('report.purchaseOrder.exportToExcel');
               
    
            });

            Route::prefix('daily-handover')->middleware(['permission:menu-report-monitoring-handover'])->group(function () 
            {
                //    BELUM TAU PENTING PA GAK NYA
                Route::get('', 'AccessoriesReportDailyHandoverController@index')->name('accessoriesReportDailyHandover.index');
            });

            Route::prefix('statistical-date')->middleware(['permission:menu-monitoring-statistical-date'])->group(function () 
            {
                //    BELUM TAU PENTING PA GAK NYA
                Route::get('', 'AccessoriesReportStatisticalDateController@index')->name('accessoriesReportStatisticalDate.index');
                Route::get('/report/accessories/monitoring-statistical-date/document-picklist', ['middleware' => ['permission:menu-monitoring-statistical-date'], 'uses' => 'ReportController@documentPicklist'])->name('report.documentPicklist');
                Route::get('/report/accessories/monitoring-statistical-date/buyer-on-invoice', ['middleware' => ['permission:menu-monitoring-statistical-date'], 'uses' => 'ReportController@buyerOnInvoice'])->name('report.buyerOnInvoice');
                Route::post('/report/accessories/monitoring-statistical-date/export-buyer', ['middleware' => ['permission:menu-monitoring-statistical-date'], 'uses' => 'ReportController@exportBuyer'])->name('report.exportBuyer');
                Route::get('/report/accessories/monitoring-statistical-date/invoice-picklist', ['middleware' => ['permission:'], 'uses' => 'ReportController@invoicePicklist'])->name('report.invoicePicklist');
        
            });

            Route::prefix('material-stock')->middleware(['permission:menu-report-material-stock'])->group(function () 
            {
                Route::get('','AccessoriesReportMaterialStockController@index')->name('accessoriesReportMaterialStock.index');
                Route::get('active', 'AccessoriesReportMaterialStockController@dataActive')->name('accessoriesReportMaterialStock.dataActive');
                Route::get('inactive', 'AccessoriesReportMaterialStockController@dataInactive')->name('accessoriesReportMaterialStock.dataInactive');
                Route::get('stock-mutation/{id}', 'AccessoriesReportMaterialStockController@stockMutation')->name('accessoriesReportMaterialStock.stockMutation');
                //Route::get('allocation-buyer/{id}', 'AccessoriesReportMaterialStockController@dataAllocationBuyer')->name('accessoriesReportMaterialStock.dataAllocationBuyer');
                //Route::get('allocation-non-buyer/{id}', 'AccessoriesReportMaterialStockController@dataAllocationNonBuyer')->name('accessoriesReportMaterialStock.dataAllocationNonBuyer');
                
                Route::get('locator-picklist', 'AccessoriesReportMaterialStockController@locatorPicklist')->name('accessoriesReportMaterialStock.locatorPicklist');
                Route::get('edit/{id}', 'AccessoriesReportMaterialStockController@editStock')->name('accessoriesReportMaterialStock.editStock');
                Route::get('restore/{id}', 'AccessoriesReportMaterialStockController@restoreStock')->name('accessoriesReportMaterialStock.restoreStock');
                Route::get('delete/{id}', 'AccessoriesReportMaterialStockController@deleteStock')->name('accessoriesReportMaterialStock.deleteStock');
                Route::get('move-locator/{id}', 'AccessoriesReportMaterialStockController@moveLocator')->name('accessoriesReportMaterialStock.moveLocator');
                Route::get('transfer-stock/{id}', 'AccessoriesReportMaterialStockController@transferStock')->name('accessoriesReportMaterialStock.transferStock');
                Route::get('change-type-stock/{id}', 'AccessoriesReportMaterialStockController@changeTypeStock')->name('accessoriesReportMaterialStock.changeTypeStock');
                Route::get('transfer-stock/print-barcode', 'AccessoriesReportMaterialStockController@transferStockBarcode')->name('accessoriesReportMaterialStock.transferStockBarcode');
                Route::get('export-stock-to-excel', 'AccessoriesReportMaterialStockController@export')->name('accessoriesReportMaterialStock.export');
                Route::get('export-allocation-to-excel', 'AccessoriesReportMaterialStockController@exportAllocation')->name('accessoriesReportMaterialStock.exportAllocation');
                Route::get('delete-bulk', 'AccessoriesReportMaterialStockController@deleteBulk')->name('accessoriesReportMaterialStock.deleteBulk');
                Route::get('delete-bulk/export-form-import', 'AccessoriesReportMaterialStockController@exportFormImportDeleteBulk')->name('accessoriesReportMaterialStock.exportFormImportDeleteBulk');
                Route::get('change-type-bulk', 'AccessoriesReportMaterialStockController@changeTypeBulk')->name('accessoriesReportMaterialStock.changeTypeBulk');
                Route::get('change-type-bulk/export-form-import', 'AccessoriesReportMaterialStockController@exportFormImportChangeTypeBulk')->name('accessoriesReportMaterialStock.exportFormImportChangeTypeBulk');
                Route::get('change-source-bulk', 'AccessoriesReportMaterialStockController@changeSourceBulk')->name('accessoriesReportMaterialStock.changeSourceBulk');
                Route::get('change-source-bulk/export-form-import', 'AccessoriesReportMaterialStockController@exportFormImportChangeSourceBulk')->name('accessoriesReportMaterialStock.exportFormImportChangeSourceBulk');
                Route::get('edit-bulk', 'AccessoriesReportMaterialStockController@editBulk')->name('accessoriesReportMaterialStock.editBulk');
                Route::get('edit-bulk/export-form-import', 'AccessoriesReportMaterialStockController@exportFormImportEditBulk')->name('accessoriesReportMaterialStock.exportFormImportEditBulk');
                Route::get('change-source-stock/{id}', 'AccessoriesReportMaterialStockController@changeSourceStock')->name('accessoriesReportMaterialStock.changeSourceStock');
                
                Route::put('recalculate/{id}', 'AccessoriesReportMaterialStockController@recalculate')->name('accessoriesReportMaterialStock.recalculate');
                Route::put('unlock/{id}', 'AccessoriesReportMaterialStockController@unlock')->name('accessoriesReportMaterialStock.unlock');
                Route::post('change-type-stock', 'AccessoriesReportMaterialStockController@storeChangeTypeStock')->name('accessoriesReportMaterialStock.storeChangeTypeStock');
                Route::post('transfer-stock', 'AccessoriesReportMaterialStockController@storeTransferStock')->name('accessoriesReportMaterialStock.storeTransferStock');
                Route::post('move-locator', 'AccessoriesReportMaterialStockController@storeMoveLocator')->name('accessoriesReportMaterialStock.storeMoveLocator');
                Route::post('edit-stock', 'AccessoriesReportMaterialStockController@storeEditStock')->name('accessoriesReportMaterialStock.storeEditStock');
                Route::post('restore-stock', 'AccessoriesReportMaterialStockController@storeRestoreStock')->name('accessoriesReportMaterialStock.storeRestoreStock');
                Route::post('delete-stock', 'AccessoriesReportMaterialStockController@storeDeleteStock')->name('accessoriesReportMaterialStock.storeDeleteStock');
                Route::post('delete-bulk', 'AccessoriesReportMaterialStockController@storeDeleteBulk')->name('accessoriesReportMaterialStock.storeDeleteBulk');
                Route::post('change-source-bulk', 'AccessoriesReportMaterialStockController@storeChangeSourceBulk')->name('accessoriesReportMaterialStock.storeChangeSourceBulk');
                Route::post('change-bulk', 'AccessoriesReportMaterialStockController@storeChangeTypeBulk')->name('accessoriesReportMaterialStock.storeChangeTypeBulk');
                Route::post('edit-bulk', 'AccessoriesReportMaterialStockController@storeEditBulk')->name('accessoriesReportMaterialStock.storeEditBulk');
                Route::post('allocation-stock/{id}', 'AccessoriesReportMaterialStockController@allocatingStock')->name('accessoriesReportMaterialStock.allocatingStock');
                Route::post('cancal-allocation/{id}','AccessoriesReportMaterialStockController@cancelAllocation')->name('accessoriesReportMaterialStock.cancelAllocation');
                Route::post('change-source-stock', 'AccessoriesReportMaterialStockController@storeChangeSourceStock')->name('accessoriesReportMaterialStock.storeChangeSourceStock');
            
                Route::get('downloadChangeTypeStock', 'AccessoriesReportMaterialStockController@downloadChangeTypeStock')->name('accessoriesReportMaterialStock.downloadChangeTypeStock');
                Route::post ('uploadChangeTypeStock', 'AccessoriesReportMaterialStockController@uploadChangeTypeStock')->name('accessoriesReportMaterialStock.uploadChangeTypeStock');
                
            });

            Route::prefix('material-quality-control')->middleware(['permission:menu-report-check-material'])->group(function () 
            {
                Route::get('','AccessoriesReportMaterialQualityControlController@index')->name('accessoriesReportMaterialQualityControl.index');
                Route::get('data','AccessoriesReportMaterialQualityControlController@data')->name('accessoriesReportMaterialQualityControl.data');
                Route::get('export','AccessoriesReportMaterialQualityControlController@export')->name('accessoriesReportMaterialQualityControl.export');
                Route::get('export-all','AccessoriesReportMaterialQualityControlController@exportAll')->name('accessoriesReportMaterialQualityControl.exportAll');
                Route::get('export-summary','AccessoriesReportMaterialQualityControlController@exportSummary')->name('accessoriesReportMaterialQualityControl.exportSummary');
            });

            Route::prefix('material-metal-detector')->middleware(['permission:menu-report-metal-detector'])->group(function () 
            {
                Route::get('','AccessoriesReportMaterialMetalDetectorController@index')->name('accessoriesReportMaterialMetalDetector.index');
                Route::get('data','AccessoriesReportMaterialMetalDetectorController@data')->name('accessoriesReportMaterialMetalDetector.data');
                Route::get('export','AccessoriesReportMaterialMetalDetectorController@export')->name('accessoriesReportMaterialMetalDetector.export');
            });

            Route::prefix('material-ila')->middleware(['permission:menu-report-material-ila'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialIlaController@index')->name('accessoriesReportMaterialIla.index');
                Route::get('data', 'AccessoriesReportMaterialIlaController@data')->name('accessoriesReportMaterialIla.data');
                Route::get('po-buyer-picklist','AccessoriesReportMaterialIlaController@poBuyerPicklist')->name('accessoriesReportMaterialIla.poBuyerPicklist');
            });

            Route::prefix('material-bapb')->middleware(['permission:menu-report-allocation-bapb'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialBAPBController@index')->name('accessoriesReportMaterialBAPB.index');
                Route::get('data', 'AccessoriesReportMaterialBAPBController@data')->name('accessoriesReportMaterialBAPB.data');
            });

            Route::prefix('material-stock-opname')->middleware(['permission:menu-report-material-stock-opname-acc'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialStockOpnameController@index')->name('accessoriesReportMaterialStockOpname.index');
                Route::get('data', 'AccessoriesReportMaterialStockOpnameController@data')->name('accessoriesReportMaterialStockOpname.data');
                Route::get('export', 'AccessoriesReportMaterialStockOpnameController@export')->name('accessoriesReportMaterialStockOpname.export');
            });

            Route::prefix('material-allocation-cancel-order')->middleware(['permission:menu-report-allocation-cancel-order-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialAllocationCancelOrder@index')->name('accessoriesReportMaterialAllocationCancelOrder.index');
                Route::get('data', 'AccessoriesReportMaterialAllocationCancelOrder@data')->name('accessoriesReportMaterialAllocationCancelOrder.data');
                Route::get('export', 'AccessoriesReportMaterialAllocationCancelOrder@export')->name('accessoriesReportMaterialAllocationCancelOrder.export');
            });

            Route::prefix('material-allocation-carton')->middleware(['permission:menu-report-allocation-cancel-order-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialAllocationCarton@index')->name('accessoriesReportMaterialAllocationCarton.index');
                Route::get('data', 'AccessoriesReportMaterialAllocationCarton@data')->name('accessoriesReportMaterialAllocationCarton.data');
                Route::get('export', 'AccessoriesReportMaterialAllocationCarton@export')->name('accessoriesReportMaterialAllocationCarton.export');
            });

            Route::prefix('material-outstanding-cancel-order')->middleware(['permission:menu-report-allocation-cancel-order-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialOutstandingCancelOrder@index')->name('accessoriesReportMaterialOutstandingCancelOrder.index');
                Route::get('data', 'AccessoriesReportMaterialOutstandingCancelOrder@data')->name('accessoriesReportMaterialOutstandingCancelOrder.data');
                Route::get('export', 'AccessoriesReportMaterialOutstandingCancelOrder@export')->name('accessoriesReportMaterialOutstandingCancelOrder.export');
            });


            Route::prefix('material-summary-stock')->middleware(['permission:menu-report-summary-material-stock-acc'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialSummaryStock@index')->name('accessoriesReportMaterialSummaryStock.index');
                Route::get('data', 'AccessoriesReportMaterialSummaryStock@data')->name('accessoriesReportMaterialSummaryStock.data');
                Route::get('export', 'AccessoriesReportMaterialSummaryStock@export')->name('accessoriesReportMaterialSummaryStock.export');
                Route::get('export-detail', 'AccessoriesReportMaterialSummaryStock@exportDetail')->name('AccessoriesReportMaterialSummaryStock.exportDetail');
                Route::get('detail', 'AccessoriesReportMaterialSummaryStock@detail')->name('accessoriesReportMaterialSummaryStock.detail');
                Route::get('detail/data', 'AccessoriesReportMaterialSummaryStock@dataDetail')->name('accessoriesReportMaterialSummaryStock.dataDetail');
            });

            Route::prefix('integration-movement-reject')->middleware(['permission:menu-report-integration-movement-reject-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesReportIntegrationMovementRejectController@index')->name('accessoriesReportIntegrationMovementReject.index');
                Route::get('data', 'AccessoriesReportIntegrationMovementRejectController@data')->name('accessoriesReportIntegrationMovementReject.data');
                Route::get('export', 'AccessoriesReportIntegrationMovementRejectController@export')->name('accessoriesReportIntegrationMovementReject.export');

                Route::post('store', 'AccessoriesReportIntegrationMovementRejectController@store')->name('accessoriesReportIntegrationMovementReject.store');
            });

            Route::prefix('integration-out-issue')->middleware(['permission:menu-report-integration-out-issue-accessories'])->group(function () 
            {
                Route::get('', 'AccessoriesReportIntegrationOutIssueController@index')->name('accessoriesReportIntegrationOutIssue.index');
                Route::get('data', 'AccessoriesReportIntegrationOutIssueController@data')->name('accessoriesReportIntegrationOutIssue.data');
                Route::get('detail/{item_id}/{warehuse_id}', 'AccessoriesReportIntegrationOutIssueController@detail')->name('accessoriesReportIntegrationOutIssue.detail');
            });
            
            Route::prefix('material-monitoring-allocation')->middleware(['permission:menu-report-material-monitoring-allocation'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialMonitoringAllocation@index')->name('accessoriesReportMaterialMonitoringAllocation.index');
                Route::get('data', 'AccessoriesReportMaterialMonitoringAllocation@data')->name('accessoriesReportMaterialMonitoringAllocation.data');
                Route::get('detail/{id}','AccessoriesReportMaterialMonitoringAllocation@detail')->name('accessoriesReportMaterialMonitoringAllocation.detail');
                Route::get('detail-data', 'AccessoriesReportMaterialMonitoringAllocation@detail')->name('accessoriesReportMaterialMonitoringAllocation.detail-data');
                Route::post('export', 'AccessoriesReportMaterialMonitoringAllocation@export')->name('accessoriesReportMaterialMonitoringAllocation.export');
            });

            Route::prefix('report-material-in-acc')->middleware(['permission:report-material-in-acc'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialInController@index')->name('accessoriesReportMaterialIn.index');
                Route::get('data', 'AccessoriesReportMaterialInController@data')->name('accessoriesReportMaterialIn.data');
                Route::get('export', 'AccessoriesReportMaterialInController@exportAll')->name('accessoriesReportMaterialIn.exportAll');
            });

            Route::prefix('report-material-out-acc')->middleware(['permission:report-material-out-acc'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialOutController@index')->name('accessoriesReportMaterialOut.index');
                Route::get('data', 'AccessoriesReportMaterialOutController@data')->name('accessoriesReportMaterialOut.data');
                Route::get('export', 'AccessoriesReportMaterialOutController@exportAll')->name('accessoriesReportMaterialOut.exportAll');
            });
            
            Route::prefix('report-material-adjustment-acc')->middleware(['permission:report-material-adjustment-acc'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialAdjustmentController@index')->name('accessoriesReportMaterialAdjustment.index');
                Route::get('data', 'AccessoriesReportMaterialAdjustmentController@data')->name('accessoriesReportMaterialAdjustment.data');
                Route::get('export', 'AccessoriesReportMaterialAdjustmentController@exportAll')->name('accessoriesReportMaterialAdjustment.exportAll');
            });

            Route::prefix('report-material-out-subcont')->middleware(['permission:report-material-out-subcont'])->group(function () 
            {
                Route::get('', 'AccessoriesReportMaterialOutSubcontController@index')->name('accessoriesReportMaterialOutSubcont.index');
                Route::get('data', 'AccessoriesReportMaterialOutSubcontController@data')->name('accessoriesReportMaterialOutSubcont.data');
                Route::get('export', 'AccessoriesReportMaterialOutSubcontController@exportAll')->name('accessoriesReportMaterialOutSubcont.exportAll');
            });
            
        });
    });

    Route::prefix('fabric')->group(function () 
    {
        Route::prefix('material-arrival')->middleware(['permission:menu-material-receive-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialArrivalController@index')->name('fabricMaterialArrival.index');
            Route::get('create','FabricMaterialArrivalController@create')->name('fabricMaterialArrival.create');
            Route::post('store', 'FabricMaterialArrivalController@store')->name('fabricMaterialArrival.store');
        });

        Route::prefix('material-in')->middleware(['permission:menu-material-in-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialInController@index')->name('fabricMaterialIn.index');
            Route::get('create', 'FabricMaterialInController@create')->name('fabricMaterialIn.create');
            Route::get('locator-picklist', 'FabricMaterialInController@locatorPicklist')->name('fabricMaterialIn.locatorPicklist');
            Route::post('store', 'FabricMaterialInController@store')->name('fabricMaterialIn.store');
        });

        Route::prefix('material-reject')->middleware(['permission:menu-material-reject-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialRejectController@index')->name('fabricMaterialReject.index');
            Route::get('create', 'FabricMaterialRejectController@create')->name('fabricMaterialReject.create');
            Route::post('store', 'FabricMaterialRejectController@store')->name('fabricMaterialReject.store');
        });


        Route::prefix('locator')->middleware(['permission:menu-fabric-locator'])->group(function () 
        {
            Route::get('', 'FabricLocatorController@index')->name('fabricLocator.index');
            Route::get('data-summary', 'FabricLocatorController@dataSummary')->name('fabricLocator.dataSummary');
            Route::get('data-detail', 'FabricLocatorController@dataDetail')->name('fabricLocator.detailDetail');
            Route::get('data-detail-per-po-supplier', 'FabricLocatorController@dataDetailPerPoSupplier')->name('fabricLocator.dataDetailPerPoSupplier');
            Route::get('filter', 'FabricLocatorController@filter')->name('fabricLocator.filter');
           
        });

        Route::prefix('allocation')->middleware(['permission:menu-allocation-fabric'])->group(function () 
        {
            Route::get('','FabricAllocationController@index')->name('fabricAllocation.index');
            Route::get('data-reguler','FabricAllocationController@dataReguler')->name('fabricAllocation.dataReguler');
            Route::get('data-additional','FabricAllocationController@dataAdditional')->name('fabricAllocation.dataAdditional');
            Route::get('export','FabricAllocationController@export')->name('fabricAllocation.export');
        });

        Route::prefix('material-stock')->middleware(['permission:menu-material-stock-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialStockController@index')->name('fabricMaterialStock.index');
            Route::get('data', 'FabricMaterialStockController@data')->name('fabricMaterialStock.data');
            Route::get('create', 'FabricMaterialStockController@create')->name('fabricMaterialStock.create');
            Route::get('edit/{id}', 'FabricMaterialStockController@edit')->name('fabricMaterialStock.edit');
            Route::get('export', 'FabricMaterialStockController@export')->name('fabricMaterialStock.export');
            Route::get('export-form-split-roll', 'FabricMaterialStockController@exportFormSplitRoll')->name('fabricMaterialStock.exportFormSplitRoll');
            Route::get('reprint-barcode/{id}', 'FabricMaterialStockController@reprintBarcode')->name('fabricMaterialStock.reprintBarcode');
            Route::get('show-barcode', 'FabricMaterialStockController@showBarcode')->name('fabricMaterialStock.showBarcode');
            Route::get('create/item-picklist', 'FabricMaterialStockController@itemPicklist')->name('fabricMaterialStock.itemPicklist');
            Route::get('create/document-no-picklist', 'FabricMaterialStockController@poSupplierPicklist')->name('fabricMaterialStock.poSupplierPicklist');
            Route::get('create/supplier-picklist', 'FabricMaterialStockController@supplierPicklist')->name('fabricMaterialStock.supplierPicklist');
            Route::post('store', 'FabricMaterialStockController@store')->name('fabricMaterialStock.store');
            Route::post('import','FabricMaterialStockController@import')->name('fabricMaterialStock.import');
            Route::post('import-form-split-roll','FabricMaterialStockController@importFormSplitRoll')->name('fabricMaterialStock.importFormSplitRoll');
            Route::put('update/{id}', 'FabricMaterialStockController@update')->name('fabricMaterialStock.update');
            Route::put('delete/{id}', 'FabricMaterialStockController@delete')->name('fabricMaterialStock.delete');
        });
        
        Route::prefix('material-stock-approval')->middleware(['permission:menu-material-stock-fabric-approval'])->group(function () 
        {
            Route::get('', 'FabricMaterialStockApprovalController@index')->name('fabricMaterialStockApproval.index');
            Route::get('data', 'FabricMaterialStockApprovalController@data')->name('fabricMaterialStockApproval.data');
            Route::post('reject/{id}', 'FabricMaterialStockApprovalController@reject')->name('fabricMaterialStockApproval.reject');
            Route::post('item-approve/{id}', 'FabricMaterialStockApprovalController@itemApprove')->name('fabricMaterialStockApproval.itemApprove');
            Route::post('approved-selected', 'FabricMaterialStockApprovalController@approveSelected')->name('FabricMaterialStockApprovalController.approveSelected');
            Route::post('approved-all', 'FabricMaterialStockApprovalController@approveAll')->name('FabricMaterialStockApprovalController.approveAll');

        });

        Route::prefix('material-stock-opname')->middleware(['permission:menu-material-stock-opname-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialStockOpnameController@index')->name('fabricMaterialStockOpname.index');
            Route::get('create', 'FabricMaterialStockOpnameController@create')->name('fabricMaterialStockOpname.create');
            Route::get('import', 'FabricMaterialStockOpnameController@import')->name('fabricMaterialStockOpname.import');
            Route::get('export', 'FabricMaterialStockOpnameController@export')->name('fabricMaterialStockOpname.export');
            Route::get('locator-picklist', 'FabricMaterialStockOpnameController@locatorPicklist')->name('fabricMaterialStockOpname.locatorPicklist');
            Route::put('remove-last-used', 'FabricMaterialStockOpnameController@removeLastUser')->name('fabricMaterialStockOpname.removeLastUser');
            Route::post('store', 'FabricMaterialStockOpnameController@store')->name('fabricMaterialStockOpname.store');
            Route::get('import-remove-stock', 'FabricMaterialStockOpnameController@importRemoveStock')->name('fabricMaterialStockOpname.importRemoveStock');
            Route::get('download-remove', 'FabricMaterialStockOpnameController@downloadFormUpload')->name('fabricMaterialStockOpname.downloadFormUpload');
            Route::post('remove-stock', 'FabricMaterialStockOpnameController@removeStock')->name('fabricMaterialStockOpname.removeStock');

        });

        Route::prefix('material-out-cutting')->middleware(['permission:menu-material-out-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialOutCuttingController@index')->name('fabricMaterialOutCutting.index');
            Route::get('create', 'FabricMaterialOutCuttingController@create')->name('fabricMaterialOutCutting.create');
            Route::get('destination-picklist', 'FabricMaterialOutCuttingController@destinationPicklist')->name('fabricMaterialOutCutting.destinationPicklist');
            Route::get('print-barcode', 'FabricMaterialOutCuttingController@printBarcodeSaving')->name('fabricMaterialOutCutting.printBarcodeSaving');
            Route::post('store', 'FabricMaterialOutCuttingController@store')->name('fabricMaterialOutCutting.store');
            Route::get('download-form-upload', 'FabricMaterialOutCuttingController@downloadFormUpload')->name('fabricMaterialOutCutting.downloadFormUpload');
            Route::post('upload-out', 'FabricMaterialOutCuttingController@uploadFormUpload')->name('fabricMaterialOutCutting.uploadFormUpload');
        });

        Route::prefix('material-out-non-cutting')->middleware(['permission:menu-handover-roll-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialOutNonCuttingController@index')->name('fabricMaterialOutNonCutting.index');
            Route::get('create', 'FabricMaterialOutNonCuttingController@create')->name('fabricMaterialOutNonCutting.create');
            Route::get('document-picklist', 'FabricMaterialOutNonCuttingController@documentPicklist')->name('fabricMaterialOutNonCutting.documentPicklist');
            Route::get('print-barcode', 'FabricMaterialOutNonCuttingController@printBarcode')->name('fabricMaterialOutNonCutting.printBarcode');
            Route::put('remove-last-user', 'FabricMaterialOutNonCuttingController@removeLastUser')->name('fabricMaterialOutNonCutting.removeLastUser');
            Route::post('store-summary-material-handover', 'FabricMaterialOutNonCuttingController@storeSummaryHandoverMaterial')->name('fabricMaterialOutNonCutting.storeSummaryHandoverMaterial');
            Route::post('store', 'FabricMaterialOutNonCuttingController@store')->name('fabricMaterialOutNonCutting.store');
        });

        Route::prefix('material-receive-piping')->middleware(['permission:menu-material-receive-piping-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialReceivePipingController@index')->name('fabricMaterialReceivePiping.index');
            Route::get('create','FabricMaterialReceivePipingController@create')->name('fabricMaterialReceivePiping.create');
            Route::post('store', 'FabricMaterialReceivePipingController@store')->name('fabricMaterialReceivePiping.store');
        });

        Route::prefix('material-steam')->middleware(['permission:menu-material-steam-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialSteamController@index')->name('fabricMaterialSteam.index');
            Route::get('create','FabricMaterialSteamController@create')->name('fabricMaterialSteam.create');
            Route::post('store', 'FabricMaterialSteamController@store')->name('fabricMaterialSteam.store');
        });

        Route::prefix('material-quality-control')->middleware(['permission:menu-material-check-fabric'])->group(function()
        {
            Route::get('','FabricMaterialQualityControlController@index')->name('fabricMaterialQualityControl.index');
            Route::get('create','FabricMaterialQualityControlController@create')->name('fabricMaterialQualityControl.create');
            Route::post('store','FabricMaterialQualityControlController@store')->name('fabricMaterialQualityControl.store');
        });

        Route::prefix('material-inspect-lab')->middleware(['permission:menu-qc-lab-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialInspectLabController@index')->name('fabricMaterialInspectLab.index');
            Route::get('create', 'FabricMaterialInspectLabController@create')->name('fabricMaterialInspectLab.create');
            Route::post('store','FabricMaterialInspectLabController@store')->name('fabricMaterialInspectLab.store');
            
        });

        Route::prefix('material-fabric-testing')->middleware(['permission:menu-qc-lab-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialTestingController@index')->name('fabricMaterialTesting.index');
            Route::get('create', 'FabricMaterialTestingController@create')->name('fabricMaterialTesting.create');
            Route::post('store','FabricMaterialTestingController@store')->name('fabricMaterialTesting.store');
            Route::get('get-subtesting/{id}', 'FabricMaterialTestingController@getSubtesting')->name('fabricMaterialTesting.getSubtesting');
            
        });

        Route::prefix('material-planning')->middleware(['permission:menu-material-planning-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialPlanningController@index')->name('fabricMaterialPlanning.index');
            Route::get('data', 'FabricMaterialPlanningController@data')->name('fabricMaterialPlanning.data');
            Route::get('download-spk', 'FabricMaterialPlanningController@downloadSPK')->name('fabricMaterialPlanning.downloadSPK');
            Route::post('store', 'FabricMaterialPlanningController@store')->name('fabricMaterialPlanning.store');
        });

        Route::prefix('material-preparation')->middleware(['permission:menu-material-preparation-fabric'])->group(function () 
        {
            Route::get('', 'FabricMaterialPreparationController@index')->name('fabricMaterialPreparation.index');
            Route::get('get-planning', 'FabricMaterialPreparationController@create')->name('fabricMaterialPreparation.create');
            //reguler
            Route::get('get-planning-per-article', 'FabricMaterialPreparationController@getPlanningPerArticle')->name('fabricMaterialPreparation.getPlanningPerArticle');
            Route::get('get-planning-per-style', 'FabricMaterialPreparationController@getPlanningPerStyle')->name('fabricMaterialPreparation.getPlanningPerStyle');
            Route::get('get-planning-per-date', 'FabricMaterialPreparationController@getPlanningPerDate')->name('fabricMaterialPreparation.getPlanningPerDate');
            //additional
            //Route::get('get-planning-per-article-additional', 'FabricMaterialPreparationController@getPlanningPerArticleAdditional')->name('fabricMaterialPreparation.getPlanningPerArticleAdditional');
            Route::get('get-planning-per-no-booking-additional', 'FabricMaterialPreparationController@getPlanningPerNoBookingAdditional')->name('fabricMaterialPreparation.getPlanningPerNoBookingAdditional');
            Route::get('get-planning-per-date-additional', 'FabricMaterialPreparationController@getPlanningPerDateAdditional')->name('fabricMaterialPreparation.getPlanningPerDateAdditional');
            //balance marker 
            Route::get('get-planning-per-article-balance-marker', 'FabricMaterialPreparationController@getPlanningPerArticleBalanceMarker')->name('fabricMaterialPreparation.getPlanningPerArticleBalanceMarker');
            Route::get('get-planning-per-style-balance-marker', 'FabricMaterialPreparationController@getPlanningPerStyleBalanceMarker')->name('fabricMaterialPreparation.getPlanningPerStyleBalanceMarker');
            Route::get('get-planning-per-date-balance-marker', 'FabricMaterialPreparationController@getPlanningPerDateBalanceMarker')->name('fabricMaterialPreparation.getPlanningPerDateBalanceMarker');

            Route::get('get-planning-per-po-buyer', 'FabricMaterialPreparationController@getPlanningPerPoBuyer')->name('fabricMaterialPreparation.getPlanningPerPoBuyer');
            Route::get('print-barcode-preparation', 'FabricMaterialPreparationController@printBarcode')->name('fabricMaterialPreparation.printBarcode');
            Route::get('get-roll', 'FabricMaterialPreparationController@getRoll')->name('fabricMaterialPreparation.getRoll');
            Route::get('locator-picklist', 'FabricMaterialPreparationController@locatorPicklist')->name('fabricMaterialPreparation.locatorPicklist');
            Route::put('remove-last-used', 'FabricMaterialPreparationController@removeLastUser')->name('fabricMaterialPreparation.removeLastUser');
            Route::put('update', 'FabricMaterialPreparationController@update')->name('fabricMaterialPreparation.update');
            Route::put('reset-rilex', 'FabricMaterialPreparationController@resetRilex')->name('fabricMaterialPreparation.resetRilex');
            Route::put('reset-status-preparation', 'FabricMaterialPreparationController@removeStatusPreparation')->name('fabricMaterialPreparation.removeStatusPreparation');
            Route::post('store', 'FabricMaterialPreparationController@store')->name('fabricMaterialPreparation.store');
            Route::post('store-planning', 'FabricMaterialPreparationController@storePlanning')->name('fabricMaterialPreparation.storePlanning');
            Route::post('store-planning-additional', 'FabricMaterialPreparationController@storePlanning')->name('fabricMaterialPreparation.storePlanningAdd');
            Route::post('store-planning-balance_marker', 'FabricMaterialPreparationController@storePlanning')->name('fabricMaterialPreparation.storePlanningBalanceMarker');
            Route::get('show-barcode', 'FabricMaterialPreparationController@showBarcode')->name('FabricMaterialPreparationController.showBarcode');
        });

        Route::prefix('material-cancel')->group(function () 
        {
            Route::prefix('material-arrival')->middleware(['permission:menu-fabric-cancel-material-arrival'])->group(function () 
            {
                Route::get('', 'FabricMaterialCancelArrivalController@index')->name('fabricMaterialCancelArrival.index');
                Route::get('create', 'FabricMaterialCancelArrivalController@create')->name('fabricMaterialCancelArrival.create');
                Route::post('store', 'FabricMaterialCancelArrivalController@store')->name('fabricMaterialCancelArrival.store');
            });

            Route::prefix('material-preparation')->middleware(['permission:menu-cancel-preparation-fabric'])->group(function () 
            {
                Route::get('', 'FabricMaterialCancelPreparationController@index')->name('fabricMaterialCancelPreparation.index');
                Route::get('create', 'FabricMaterialCancelPreparationController@create')->name('fabricMaterialCancelPreparation.create');
                Route::post('store', 'FabricMaterialCancelPreparationController@store')->name('fabricMaterialCancelPreparation.store');
            });

            Route::prefix('material-out-non-cutting')->middleware(['permission:menu-cancel-out-handover-fabric'])->group(function () 
            {
                Route::get('', 'FabricMaterialCancelOutNonCuttingController@index')->name('fabricMaterialCancelOutNonCutting.index');
                Route::get('create', 'FabricMaterialCancelOutNonCuttingController@create')->name('fabricMaterialCancelOutNonCutting.create');
                Route::post('store', 'FabricMaterialCancelOutNonCuttingController@store')->name('fabricMaterialCancelOutNonCutting.store');
            });
        });

        Route::prefix('barcode')->group(function () 
        {
            Route::prefix('reprint-barcode-supplier')->middleware(['permission:menu-reprint-barcode-supplier-fabric'])->group(function () 
            {
                Route::get('','FabricBarcodeSupplierController@index')->name('fabricBarcodeSupplier.index');
                Route::post('store', 'FabricBarcodeSupplierController@store')->name('fabricBarcodeSupplier.store');
                Route::post('barcode', 'FabricBarcodeSupplierController@barcode')->name('fabricBarcodeSupplier.barcode');
            });

            Route::prefix('print-surat-jalan')->middleware(['permission:menu-reprint-barcode-supplier-fabric'])->group(function () 
            {
                Route::get('','FabricPrintSuratJalanController@index')->name('fabricPrintSuratJalan.index');
                Route::get('data', 'FabricPrintSuratJalanController@data')->name('fabricPrintSuratJalan.data');
                Route::get('print', 'FabricPrintSuratJalanController@print')->name('fabricPrintSuratJalan.print');
            });

            Route::prefix('locator')->middleware(['permission:menu-fabric-print-barcode-locator'])->group(function () 
            {
                Route::get('','FabricBarcodeLocatorController@index')->name('fabricBarcodeLocator.index');
                Route::get('barcode', 'FabricBarcodeLocatorController@barcode')->name('fabricBarcodeLocator.barcode');
            });
        });

        Route::prefix('report')->group(function () 
        {
            Route::prefix('daily-material-arrival')->middleware(['permission:menu-monitoring-material-receive-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportDailyMaterialArrivalController@index')->name('fabricReportDailyMaterialArrival.index');
                Route::get('data', 'FabricReportDailyMaterialArrivalController@data')->name('fabricReportDailyMaterialArrival.data');
                Route::get('export-to-excel', 'FabricReportDailyMaterialArrivalController@export')->name('fabricReportDailyMaterialArrival.export');
            });

            Route::prefix('daily-material-out-non-cutting')->middleware(['permission:menu-report-handover-material-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportDailyMaterialOutNonCuttingController@index')->name('fabricReportDailyMaterialOutNonCutting.index');
                Route::get('data-handover', 'FabricReportDailyMaterialOutNonCuttingController@dataHandover')->name('fabricReportDailyMaterialOutNonCutting.dataHandover');
                Route::get('data-move-order', 'FabricReportDailyMaterialOutNonCuttingController@dataMoveOrder')->name('fabricReportDailyMaterialOutNonCutting.dataMoveOrder');
                Route::get('edit/{id}', 'FabricReportDailyMaterialOutNonCuttingController@edit')->name('fabricReportDailyMaterialOutNonCutting.edit');
                Route::get('detail/{id}', 'FabricReportDailyMaterialOutNonCuttingController@detail')->name('fabricReportDailyMaterialOutNonCutting.detail');
                Route::get('detail/{id}/data', 'FabricReportDailyMaterialOutNonCuttingController@dataDetail')->name('fabricReportDailyMaterialOutNonCutting.dataDetail');
                Route::get('detail/{summary_id}/print-barcode/{id}', 'FabricReportDailyMaterialOutNonCuttingController@printBarcode')->name('fabricReportDailyMaterialOutNonCutting.printBarcode');
                Route::get('export-to-excel', 'FabricReportDailyMaterialOutNonCuttingController@export')->name('fabricReportDailyMaterialOutNonCutting.export');
                Route::post('update', 'FabricReportDailyMaterialOutNonCuttingController@update')->name('fabricReportDailyMaterialOutNonCutting.update');
                Route::post('destroy/{id}', 'FabricReportDailyMaterialOutNonCuttingController@delete')->name('fabricReportDailyMaterialOutNonCutting.destroy');
            });

            Route::prefix('daily-material-out')->middleware(['permission:menu-report-out-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportDailyMaterialOutController@index')->name('fabricReportDailyMaterialOut.index');
                Route::get('data', 'FabricReportDailyMaterialOutController@data')->name('fabricReportDailyMaterialOut.data');
                Route::get('detail', 'FabricReportDailyMaterialOutController@detail')->name('fabricReportDailyMaterialOut.detail');
                Route::get('detail-data', 'FabricReportDailyMaterialOutController@detailData')->name('fabricReportDailyMaterialOut.detailData');
                Route::post('export-to-excel', 'FabricReportDailyMaterialOutController@export')->name('fabricReportDailyMaterialOut.excel');
            });
            
            Route::prefix('daily-material-quality-control')->middleware(['permission:menu-report-fabric-inspect-lab'])->group(function () 
            {
                Route::get('', 'FabricReportDailyMaterialQualityControlController@index')->name('fabricReportDailyMaterialQualityControl.index');
                Route::get('data','FabricReportDailyMaterialQualityControlController@data')->name('fabricReportDailyMaterialQualityControl.data');
                Route::get('detail/{id}','FabricReportDailyMaterialQualityControlController@detail')->name('fabricReportDailyMaterialQualityControl.detail');
                Route::get('get-user-picklist','FabricReportDailyMaterialQualityControlController@getUserPicklist')->name('fabricReportDailyMaterialQualityControl.getUserPicklist');
            });

            Route::prefix('daily-material-preparation')->middleware(['permission:menu-report-preparation-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportDailyMaterialPreparationController@index')->name('fabricReportDailyMaterialPreparation.index');
                Route::get('detail/{id}', 'FabricReportDailyMaterialPreparationController@detail')->name('fabricReportDailyMaterialPreparation.detail');
                Route::get('detail/{id}/data', 'FabricReportDailyMaterialPreparationController@dataDetail')->name('fabricReportDailyMaterialPreparation.dataDetail');
                Route::get('data-reguler', 'FabricReportDailyMaterialPreparationController@dataReguler')->name('fabricReportDailyMaterialPreparation.dataReguler');
                Route::get('data-additional', 'FabricReportDailyMaterialPreparationController@dataAdditional')->name('fabricReportDailyMaterialPreparation.dataAdditional');
                Route::get('data-balance-marker', 'FabricReportDailyMaterialPreparationController@dataBalanceMarker')->name('fabricReportDailyMaterialPreparation.dataBalanceMarker');
                Route::get('data-history/{id}', 'FabricReportDailyMaterialPreparationController@dataHistory')->name('fabricReportDailyMaterialPreparation.dataHistory');
                Route::put('detail/{summary_id}/destroy/{id}', 'FabricReportDailyMaterialPreparationController@deleteDetail')->name('fabricReportDailyMaterialPreparation.deleteDetail');
                Route::get('detail/export-to-excel/{id}', 'FabricReportDailyMaterialPreparationController@exportDetail')->name('fabricReportDailyMaterialPreparation.exportDetail');
                Route::get('detail/{summary_id}/barcode/{id}', 'FabricReportDailyMaterialPreparationController@printBarcode')->name('fabricReportDailyMaterialPreparation.printBarcode');
                Route::get('export-to-excel', 'FabricReportDailyMaterialPreparationController@exportAll')->name('fabricReportDailyMaterialPreparation.exportAll');
                
                Route::post('reprepare-roll-buyer/{id}', 'FabricReportDailyMaterialPreparationController@rePrepareRollToBuyer')->name('fabricReportDailyMaterialPreparation.rePrepareRollToBuyer');
                Route::post('cancel/{id}', 'FabricReportDailyMaterialPreparationController@cancel')->name('fabricReportDailyMaterialPreparation.cancel');
                Route::post('delete/{id}', 'FabricReportDailyMaterialPreparationController@delete')->name('fabricReportDailyMaterialPreparation.delete');
                Route::post('delete-closing/{id}', 'FabricReportDailyMaterialPreparationController@deleteClosing')->name('fabricReportDailyMaterialPreparation.deleteClosing');
                Route::put('recalculate/{id}', 'FabricReportDailyMaterialPreparationController@recalculate')->name('fabricReportDailyMaterialPreparation.recalculate');
                Route::put('remark/{id}', 'FabricReportDailyMaterialPreparationController@remark')->name('fabricReportDailyMaterialPreparation.remark');
                Route::get('machine/{id}', 'FabricReportDailyMaterialPreparationController@machine')->name('fabricReportDailyMaterialPreparation.machine');
                Route::put('update-machine/', 'FabricReportDailyMaterialPreparationController@updateMachine')->name('fabricReportDailyMaterialPreparation.updateMachine');
                Route::put('unlock/{id}', 'FabricReportDailyMaterialPreparationController@unlock')->name('fabricReportDailyMaterialPreparation.unlock');
                Route::put('close/{id}', 'FabricReportDailyMaterialPreparationController@close')->name('fabricReportDailyMaterialPreparation.close');

                Route::get('move-preparation/{id}', 'FabricReportDailyMaterialPreparationController@showChangePlan')->name('fabricReportDailyMaterialPreparation.showChangePlan');
                Route::get('get-planning-per-date', 'FabricReportDailyMaterialPreparationController@getPlanningPerDate')->name('fabricReportDailyMaterialPreparation.getPlanningPerDate');
                Route::get('get-planning-per-article', 'FabricReportDailyMaterialPreparationController@getPlanningPerArticle')->name('fabricReportDailyMaterialPreparation.getPlanningPerArticle');
                Route::get('get-planning-per-style', 'FabricReportDailyMaterialPreparationController@getPlanningPerStyle')->name('fabricReportDailyMaterialPreparation.getPlanningPerStyle');
                Route::post('move-preparation/{id}/store', 'FabricReportDailyMaterialPreparationController@storeChangePlan')->name('fabricReportDailyMaterialPreparation.storeChangePlan');
                Route::post('print-barcode-move-preparation','FabricReportDailyMaterialPreparationController@printBarcodeMovePrepare')->name('fabricReportDailyMaterialPreparation.printBarcodeMovePrepare');
           
            });

            Route::prefix('daily-cutting-instruction')->middleware(['permission:menu-report-cutting-instruction'])->group(function () 
            {
                Route::get('', 'FabricReportDailyCuttingInstructionController@index')->name('fabricReportDailyCuttingInstruction.index');
                Route::get('data', 'FabricReportDailyCuttingInstructionController@data')->name('fabricReportDailyCuttingInstruction.data');
                Route::get('detail/{id}', 'FabricReportDailyCuttingInstructionController@detail')->name('fabricReportDailyCuttingInstruction.detail');
                Route::get('detail/{id}/data', 'FabricReportDailyCuttingInstructionController@dataDetail')->name('fabricReportDailyCuttingInstruction.dataDetail');
                Route::get('export-to-excel', 'FabricReportDailyCuttingInstructionController@export')->name('fabricReportDailyCuttingInstruction.export');
                Route::get('export/{id}', 'FabricReportDailyCuttingInstructionController@exportDetail')->name('fabricReportDailyCuttingInstruction.exportDetail');
                Route::post('detail/{id}/receive', 'FabricReportDailyCuttingInstructionController@store')->name('fabricReportDailyCuttingInstruction.store');

                Route::get('request', 'FabricReportDailyCuttingInstructionController@requestApproval')->name('fabricReportDailyCuttingInstruction.requestApproval');
                Route::post('approve', 'FabricReportDailyCuttingInstructionController@approve')->name('fabricReportDailyCuttingInstruction.approve');
                Route::post('reject', 'FabricReportDailyCuttingInstructionController@reject')->name('fabricReportDailyCuttingInstruction.reject');
                Route::get('request/save', 'FabricReportDailyCuttingInstructionController@requestApprovalSave')->name('fabricReportDailyCuttingInstruction.requestApprovalSave');
                Route::get('approval/save', 'FabricReportDailyCuttingInstructionController@approvalSave')->name('fabricReportDailyCuttingInstruction.approvalSave');
                Route::get('reject/save', 'FabricReportDailyCuttingInstructionController@rejectSave')->name('fabricReportDailyCuttingInstruction.rejectSave');
            
            });

            Route::prefix('daily-request-fabric')->middleware(['permission:menu-report-cutting-instruction'])->group(function () 
            {
                Route::get('', 'FabricReportDailyRequestFabricController@index')->name('fabricReportDailyRequestFabric.index');
                Route::get('data', 'FabricReportDailyRequestFabricController@data')->name('fabricReportDailyRequestFabric.data');
                Route::get('detail/{id}', 'FabricReportDailyRequestFabricController@detail')->name('fabricReportDailyRequestFabric.detail');
                Route::get('detail/{id}/data', 'FabricReportDailyRequestFabricController@dataDetail')->name('fabricReportDailyRequestFabric.dataDetail');
                Route::get('export-to-excel', 'FabricReportDailyRequestFabricController@export')->name('fabricReportDailyRequestFabric.export');
                Route::get('export/{id}', 'FabricReportDailyRequestFabricController@exportDetail')->name('fabricReportDailyRequestFabric.exportDetail');
                Route::post('detail/{id}/receive', 'FabricReportDailyRequestFabricController@store')->name('fabricReportDailyRequestFabric.store');

                Route::get('approve-whs/{id}', 'FabricReportDailyRequestFabricController@approveWhs')->name('fabricReportDailyRequestFabric.approveWhs');
                Route::get('approve-lab/{id}', 'FabricReportDailyRequestFabricController@approveLab')->name('fabricReportDailyRequestFabric.approveLab');
            });

            Route::prefix('material-stock')->middleware(['permission:menu-report-stock-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialStockController@index')->name('fabricReportMaterialStock.index');
                Route::get('data', 'FabricReportMaterialStockController@data')->name('fabricReportMaterialStock.data');
                Route::get('detail/{id}', 'FabricReportMaterialStockController@detail')->name('fabricReportMaterialStock.detail');
                Route::get('detail/{id}/data', 'FabricReportMaterialStockController@dataDetail')->name('fabricReportMaterialStock.dataDetail');
                Route::get('detail/print-barcode/{id}', 'FabricReportMaterialStockController@barcode')->name('fabricReportMaterialStock.barcode');
                Route::get('detail/history/{id}', 'FabricReportMaterialStockController@history')->name('fabricReportMaterialStock.history');
                Route::get('export', 'FabricReportMaterialStockController@export')->name('fabricReportMaterialStock.export');
                Route::get('unlock/{id}', 'FabricReportMaterialStockController@unlockAll')->name('fabricReportMaterialStock.unlockAll');
                Route::get('detail/unlock/{id}', 'FabricReportMaterialStockController@unlockDetail')->name('fabricReportMaterialStock.unlockDetail');
                Route::put('recalculate/{id}', 'FabricReportMaterialStockController@recalculate')->name('fabricReportMaterialStock.recalculate');
            });

            Route::prefix('material-inspect-lab')->middleware(['permission:menu-report-fabric-inspect-lab'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialInspectLabController@index')->name('fabricReportMaterialInspectLab.index');
                Route::get('data','FabricReportMaterialInspectLabController@data')->name('fabricReportMaterialInspectLab.data');
                Route::get('detail/data/{id}','FabricReportMaterialInspectLabController@dataDetail')->name('fabricReportMaterialInspectLab.dataDetail');
                Route::get('detail/{id}','FabricReportMaterialInspectLabController@detail')->name('fabricReportMaterialInspectLab.detail');
                Route::get('export-summary-to-excel', 'FabricReportMaterialInspectLabController@exportSummary')->name('fabricReportMaterialInspectLab.exportSummary');
                Route::get('/detail/{id}/export-to-excel', 'FabricReportMaterialInspectLabController@exportDetail')->name('fabricReportMaterialInspectLab.exportDetail');
                Route::post('update-lot', 'FabricReportMaterialInspectLabController@updateLot')->name('fabricReportMaterialInspectLab.updateLot');
                Route::put('remark/{id}', 'FabricReportMaterialInspectLabController@remarkDetail')->name('fabricReportMaterialInspectLab.remark');
                Route::get('export-all', 'FabricReportMaterialInspectLabController@exportAll')->name('fabricReportMaterialInspectLab.exportAll');
            });

            Route::prefix('material-quality-control')->middleware(['permission:menu-report-fabric-fir'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialQualityControlController@index')->name('fabricReportMaterialQualityControl.index');
                Route::get('import','FabricReportMaterialQualityControlController@import')->name('fabricReportMaterialQualityControl.import');
                Route::get('data','FabricReportMaterialQualityControlController@data')->name('fabricReportMaterialQualityControl.data');
                Route::get('detail/{id}','FabricReportMaterialQualityControlController@detail')->name('fabricReportMaterialQualityControl.detail');
                Route::get('detail/data-other/{id}','FabricReportMaterialQualityControlController@dataDetailOther')->name('fabricReportMaterialQualityControl.dataDetailOther');
                Route::get('detail/data/{id}','FabricReportMaterialQualityControlController@dataDetail')->name('fabricReportMaterialQualityControl.dataDetail');
                Route::get('detail/cuttable-width/{id}','FabricReportMaterialQualityControlController@dataDetailCuttableWidth')->name('fabricReportMaterialQualityControl.dataDetailCuttableWidth');
                Route::get('export-summary-to-excel', 'FabricReportMaterialQualityControlController@exportSummary')->name('fabricReportMaterialQualityControl.exportSummary');
                Route::get('export-all', 'FabricReportMaterialQualityControlController@exportAll')->name('fabricReportMaterialQualityControl.exportAll');
                Route::get('detail/export-to-excel/{id}', 'FabricReportMaterialQualityControlController@exportDetail')->name('fabricReportMaterialQualityControl.exportDetail');
                Route::get('detail/{summary_id}/detail-point/{id}', 'FabricReportMaterialQualityControlController@DetailMaterialCheck')->name('fabricReportMaterialQualityControl.moreDetail');
                Route::put('detail/{summary_id}/update-note/{id}', 'FabricReportMaterialQualityControlController@updateNote')->name('fabricReportMaterialQualityControl.updateNote');
                Route::post('detail/{summary_id}/update-point/{id}','FabricReportMaterialQualityControlController@updatePoint')->name('fabricReportMaterialQualityControl.updatePoint');
                Route::post('update-actual-width','FabricReportMaterialQualityControlController@updateActualWidth')->name('fabricReportMaterialQualityControl.updateActualWidth');
                Route::post('store','FabricReportMaterialQualityControlController@store')->name('fabricReportMaterialQualityControl.store');
                Route::post('confirm-final-result','FabricReportMaterialQualityControlController@confirmFinalResult')->name('fabricReportMaterialQualityControl.confirmFinalResult');
      
            });

            Route::prefix('material-bapb')->middleware(['permission:menu-report-bapb-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialBapbController@index')->name('fabricReportMaterialBapb.index');
                Route::get('data', 'FabricReportMaterialBapbController@data')->name('fabricReportMaterialBapb.data');
                Route::get('export', 'FabricReportMaterialBapbController@export')->name('fabricReportMaterialBapb.export');
            });

            Route::prefix('material-stock-opname')->middleware(['permission:menu-report-material-stock-opname-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialStockOpnameController@index')->name('fabricReportMaterialStockOpname.index');
                Route::get('data', 'FabricReportMaterialStockOpnameController@data')->name('fabricReportMaterialStockOpname.data');
                Route::get('export', 'FabricReportMaterialStockOpnameController@export')->name('fabricReportMaterialStockOpname.export');
            });

            Route::prefix('summary-batch-supplier')->middleware(['permission:menu-report-summary-batch-per-supplier'])->group(function () 
            {
                Route::get('', 'FabricReportSummaryBatchSupplierController@index')->name('fabricReportSummaryBatchSupplier.index');
                Route::get('data', 'FabricReportSummaryBatchSupplierController@data')->name('fabricReportSummaryBatchSupplier.data');
                Route::get('detail/{c_bpartner_id}/{year}/{month}/data', 'FabricReportSummaryBatchSupplierController@dataDetail')->name('fabricReportSummaryBatchSupplier.dataDetail');
                Route::get('export-to-excel', 'FabricReportSummaryBatchSupplierController@export')->name('fabricReportSummaryBatchSupplier.export');

            });

            Route::prefix('material-stock-material-management')->middleware(['permission:menu-report-stock-material-management-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialStockMaterialManagementController@index')->name('fabricReportMaterialStockMaterialManagement.index');
                Route::get('active', 'FabricReportMaterialStockMaterialManagementController@dataActive')->name('fabricReportMaterialStockMaterialManagement.dataActive');
                Route::get('inactive', 'FabricReportMaterialStockMaterialManagementController@dataInactive')->name('fabricReportMaterialStockMaterialManagement.dataInactive');
                Route::get('export', 'FabricReportMaterialStockMaterialManagementController@export')->name('fabricReportMaterialStockMaterialManagement.export');
                Route::get('edit/{id}', 'FabricReportMaterialStockMaterialManagementController@editStock')->name('fabricReportMaterialStockMaterialManagement.editStock');
                Route::get('delete/{id}', 'FabricReportMaterialStockMaterialManagementController@deleteStock')->name('fabricReportMaterialStockMaterialManagement.deleteStock');
                Route::get('transfer-stock/{id}', 'FabricReportMaterialStockMaterialManagementController@transferStock')->name('fabricReportMaterialStockMaterialManagement.transferStock');
                Route::get('change-type-stock/{id}', 'FabricReportMaterialStockMaterialManagementController@changeTypeStock')->name('fabricReportMaterialStockMaterialManagement.changeTypeStock');
                Route::get('detail-allocation/{id}', 'FabricReportMaterialStockMaterialManagementController@detailAllocation')->name('fabricReportMaterialStockMaterialManagement.detailAllocation');
                Route::get('export-stock-to-excel', 'FabricReportMaterialStockMaterialManagementController@export')->name('fabricReportMaterialStockMaterialManagement.export');
                Route::get('export-allocation-to-excel', 'FabricReportMaterialStockMaterialManagementController@exportAllocation')->name('fabricReportMaterialStockMaterialManagement.exportAllocation');
                Route::get('delete-bulk', 'FabricReportMaterialStockMaterialManagementController@deleteBulk')->name('fabricReportMaterialStockMaterialManagement.deleteBulk');
                Route::get('delete-bulk/export-form-import', 'FabricReportMaterialStockMaterialManagementController@exportFormImportDeleteBulk')->name('fabricReportMaterialStockMaterialManagement.exportFormImportDeleteBulk');
                Route::get('change-type-bulk', 'FabricReportMaterialStockMaterialManagementController@changeTypeBulk')->name('fabricReportMaterialStockMaterialManagement.changeTypeBulk');
                Route::get('change-type-bulk/export-form-import', 'FabricReportMaterialStockMaterialManagementController@exportFormImportChangeTypeBulk')->name('fabricReportMaterialStockMaterialManagement.exportFormImportChangeTypeBulk');
                Route::get('edit-bulk', 'FabricReportMaterialStockMaterialManagementController@editBulk')->name('fabricReportMaterialStockMaterialManagement.editBulk');
                Route::get('edit-bulk/export-form-import', 'FabricReportMaterialStockMaterialManagementController@exportFormImportEditBulk')->name('fabricReportMaterialStockMaterialManagement.exportFormImportEditBulk');
                Route::put('recalculate/{id}', 'FabricReportMaterialStockMaterialManagementController@recalculate')->name('fabricReportMaterialStockMaterialManagement.recalculate');
                Route::post('change-type-stock', 'FabricReportMaterialStockMaterialManagementController@storeChangeTypeStock')->name('fabricReportMaterialStockMaterialManagement.storeChangeTypeStock');
                Route::post('transfer-stock', 'FabricReportMaterialStockMaterialManagementController@storeTransferStock')->name('fabricReportMaterialStockMaterialManagement.storeTransferStock');
                Route::post('edit-stock', 'FabricReportMaterialStockMaterialManagementController@storeEditStock')->name('fabricReportMaterialStockMaterialManagement.storeEditStock');
                Route::post('delete-stock', 'FabricReportMaterialStockMaterialManagementController@storeDeleteStock')->name('fabricReportMaterialStockMaterialManagement.storeDeleteStock');
                Route::post('delete-bulk', 'FabricReportMaterialStockMaterialManagementController@storeDeleteBulk')->name('fabricReportMaterialStockMaterialManagement.storeDeleteBulk');
                Route::post('store-change-source-bulk', 'FabricReportMaterialStockMaterialManagementController@storeChangeSourceBulk')->name('fabricReportMaterialStockMaterialManagement.storeChangeSourceBulk');
                Route::post('change-bulk', 'FabricReportMaterialStockMaterialManagementController@storeChangeTypeBulk')->name('fabricReportMaterialStockMaterialManagement.storeChangeTypeBulk');
                Route::post('edit-bulk', 'FabricReportMaterialStockMaterialManagementController@storeEditBulk')->name('fabricReportMaterialStockMaterialManagement.storeEditBulk');
                Route::post('allocation-stock/{id}', 'FabricReportMaterialStockMaterialManagementController@allocatingStock')->name('fabricReportMaterialStockMaterialManagement.allocatingStock');
                Route::post('cancal-allocation/{id}','FabricReportMaterialStockMaterialManagementController@cancelAllocation')->name('fabricReportMaterialStockMaterialManagement.cancelAllocation');
            });

            Route::prefix('material-receive-piping')->middleware(['permission:menu-report-receive-piping-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialReceivePipingController@index')->name('fabricReportMaterialReceivePiping.index');
                Route::get('data', 'FabricReportMaterialReceivePipingController@data')->name('fabricReportMaterialReceivePiping.data');
                Route::get('export', 'FabricReportMaterialReceivePipingController@export')->name('fabricReportMaterialReceivePiping.export');
            });

            Route::prefix('material-reject-fabric')->middleware(['permission:menu-report-reject-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialRejectController@index')->name('fabricReportMaterialReject.index');
                Route::get('data', 'FabricReportMaterialRejectController@data')->name('fabricReportMaterialReject.data');
                Route::get('export', 'FabricReportMaterialRejectController@export')->name('fabricReportMaterialReject.export');
            });

            Route::prefix('material-monitoring-fabric')->middleware(['permission:menu-report-monitoring-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialMonitoringController@index')->name('fabricReportMaterialMonitoring.index');
                Route::get('data', 'FabricReportMaterialMonitoringController@data')->name('fabricReportMaterialMonitoring.data');
                Route::get('export', 'FabricReportMaterialMonitoringController@export')->name('fabricReportMaterialMonitoring.export');
            });
           
            Route::prefix('material-balance-marker')->middleware(['permission:menu-report-request-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialBalanceMarkerController@index')->name('fabricReportMaterialBalanceMarker.index');
                Route::get('data', 'FabricReportMaterialBalanceMarkerController@data')->name('fabricReportMaterialBalanceMarker.data');
                Route::get('get-po-supplier-picklist','FabricReportMaterialBalanceMarkerController@getPoSupplierPickList')->name('fabricReportMaterialBalanceMarker.getPoSupplier');
                Route::get('confirm', 'FabricReportMaterialBalanceMarkerController@confirm')->name('fabricReportMaterialBalanceMarker.confirm');
                Route::post('confirm-store', 'FabricReportMaterialBalanceMarkerController@confirmStore')->name('fabricReportMaterialBalanceMarker.confirmStore');
                Route::post('reject', 'FabricReportMaterialBalanceMarkerController@reject')->name('fabricReportMaterialBalanceMarker.reject');
                Route::get('export', 'FabricReportMaterialBalanceMarkerController@export')->name('fabricReportMaterialBalanceMarker.export');
                Route::get('create-allocation','FabricReportMaterialBalanceMarkerController@createAllocation')->name('fabricReportMaterialBalanceMarker.createAllocation');
                Route::get('download-create-allocation','FabricReportMaterialBalanceMarkerController@downloadCreateAllocation')->name('fabricReportMaterialBalanceMarker.downloadCreateAllocation');
                Route::post('upload-create-allocation','FabricReportMaterialBalanceMarkerController@uploadCreateAllocation')->name('fabricReportMaterialBalanceMarker.uploadCreateAllocation');
                Route::get('confirm-saving', 'FabricReportMaterialBalanceMarkerController@confirmSaving')->name('fabricReportMaterialBalanceMarker.confirmSaving');
                Route::post('confirm-saving-store', 'FabricReportMaterialBalanceMarkerController@confirmSavingStore')->name('fabricReportMaterialBalanceMarker.confirmSavingStore');
                Route::get('list-po-supplier-saving', 'FabricReportMaterialBalanceMarkerController@getPoSupplierSaving')->name('fabricReportMaterialBalanceMarker.getPoSupplierSaving');
                Route::get('list-nomor-roll-saving','FabricReportMaterialBalanceMarkerController@getNomorRollSaving')->name('fabricReportMaterialBalanceMarker.getNomorRollSaving');
                
            });

            Route::prefix('outstanding-material-out')->middleware(['permission:menu-report-request-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportOutstandingMaterialOutController@index')->name('fabricReportOutstandingMaterialOut.index');
                Route::get('data', 'FabricReportOutstandingMaterialOutController@data')->name('fabricReportOutstandingMaterialOut.data');
                Route::get('export', 'FabricReportOutstandingMaterialOutController@export')->name('fabricReportOutstandingMaterialOut.export');
                
            });

            Route::prefix('material-summary-stock')->middleware(['permission:menu-report-summary-stock-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialSummaryStock@index')->name('fabricReportMaterialSummaryStock.index');
                Route::get('data', 'FabricReportMaterialSummaryStock@data')->name('fabricReportMaterialSummaryStock.data');
                Route::get('detail/{item_id}/{warehuse_id}', 'FabricReportMaterialSummaryStock@detail')->name('fabricReportMaterialSummaryStock.detail');
                Route::get('detail/data', 'FabricReportMaterialSummaryStock@dataDetail')->name('fabricReportMaterialSummaryStock.dataDetail');
                Route::get('export', 'FabricReportMaterialSummaryStock@export')->name('fabricReportMaterialSummaryStock.export');
                Route::get('export-detail', 'FabricReportMaterialSummaryStock@exportDetail')->name('fabricReportMaterialSummaryStock.exportDetail');
            });

            Route::prefix('integration-movement-reject')->middleware(['permission:menu-report-integration-movement-reject-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportIntegrationMovementRejectController@index')->name('fabricReportIntegrationMovementReject.index');
                Route::get('data', 'FabricReportIntegrationMovementRejectController@data')->name('fabricReportIntegrationMovementReject.data');
                Route::post('store', 'FabricReportIntegrationMovementRejectController@store')->name('fabricReportIntegrationMovementReject.store');
            });

            Route::prefix('integration-out-issue')->middleware(['permission:menu-report-integration-out-issue-fabric'])->group(function () 
            {
                Route::get('', 'FabricReportIntegrationOutIssueController@index')->name('fabricReportIntegrationOutIssue.index');
                Route::get('data', 'FabricReportIntegrationOutIssueController@data')->name('fabricReportIntegrationOutIssue.data');
                Route::get('detail/{item_id}/{warehuse_id}', 'FabricReportIntegrationOutIssueController@detail')->name('fabricReportIntegrationOutIssue.detail');
            });
            Route::prefix('report-material-in')->middleware(['permission:material-report-in'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialInController@index')->name('fabricReportMaterialIn.index');
                Route::get('data', 'FabricReportMaterialInController@data')->name('fabricReportMaterialIn.data');
                Route::get('export', 'FabricReportMaterialInController@exportAll')->name('fabricReportMaterialIn.exportAll');
             });

            Route::prefix('report-material-out')->middleware(['permission:report-material-out'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialOutController@index')->name('fabricReportMaterialOut.index');
                Route::get('data', 'FabricReportMaterialOutController@data')->name('fabricReportMaterialOut.data');
                Route::get('export', 'FabricReportMaterialOutController@exportAll')->name('fabricReportMaterialOut.exportAll');
            });

            Route::prefix('report-material-adjustment')->middleware(['permission:report-material-adjustment'])->group(function () 
            {
                Route::get('', 'FabricReportMaterialAdjustmentController@index')->name('fabricReportMaterialAdjustment.index');
                Route::get('data', 'FabricReportMaterialAdjustmentController@data')->name('fabricReportMaterialAdjustment.data');
                Route::get('export', 'FabricReportMaterialAdjustmentController@exportAll')->name('fabricReportMaterialAdjustment.exportAll');
            });

         

           

        });
    });

    Route::prefix('master-data')->group(function () 
    {
        Route::prefix('purchase-item')->middleware(['permission:menu-master-purchase-item'])->group(function () 
        {
            Route::get('', 'MasterDataPurchaseItemController@index')->name('masterDataPurchaseItem.index');
            Route::get('data', 'MasterDataPurchaseItemController@data')->name('masterDataPurchaseItem.data');
        });

        Route::prefix('auto-allocation')->middleware(['permission:menu-master-auto-allocation'])->group(function () 
        {
            Route::get('','MasterDataAutoAllocationController@index')->name('masterDataAutoAllocation.index');
            Route::get('active','MasterDataAutoAllocationController@activeAllocation')->name('masterDataAutoAllocation.activeAllocation');
            Route::get('additional','MasterDataAutoAllocationController@additionalAllocation')->name('masterDataAutoAllocation.additionalAllocation');
            Route::get('inactive','MasterDataAutoAllocationController@inactiveAllocation')->name('masterDataAutoAllocation.inactiveAllocation');
            Route::get('create','MasterDataAutoAllocationController@create')->name('masterDataAutoAllocation.create');
            Route::get('delete-bulk','MasterDataAutoAllocationController@deleteFromBulk')->name('masterDataAutoAllocation.deleteFromBulk');
            Route::get('edit','MasterDataAutoAllocationController@edit')->name('masterDataAutoAllocation.edit');
            Route::get('{id}/history','MasterDataAutoAllocationController@history')->name('masterDataAutoAllocation.history');
            Route::get('get-po-supplier-picklist','MasterDataAutoAllocationController@getPoSupplierPickList')->name('masterDataAutoAllocation.getPoSupplier');
            Route::get('get-po-buyer-picklist','MasterDataAutoAllocationController@getPoBuyerPickList')->name('masterDataAutoAllocation.getPoBuyer');
            Route::get('get-item-picklist','MasterDataAutoAllocationController@getItemPicklist')->name('masterDataAutoAllocation.getItem');
            Route::get('export-form-input','MasterDataAutoAllocationController@exportToExcel')->name('masterDataAutoAllocation.exportToExcel');
            Route::get('export-form-input-pr','MasterDataAutoAllocationController@exportToExcelPR')->name('masterDataAutoAllocation.exportToExcelPR');
            Route::get('export-po-supplier-all','MasterDataAutoAllocationController@poSupplierAll')->name('masterDataAutoAllocation.poSupplierAll');
            Route::get('export-form-update','MasterDataAutoAllocationController@exportFormInput')->name('masterDataAutoAllocation.exportFormInput');
            Route::get('export-form-delete','MasterDataAutoAllocationController@exportFormDeleteInput')->name('masterDataAutoAllocation.exportFormDeleteInput');
            Route::get('export-to-excel','MasterDataAutoAllocationController@exportFormUpdate')->name('masterDataAutoAllocation.exportFormUpdate');
            Route::get('download-form-booking','MasterDataAutoAllocationController@downloadFormBooking')->name('masterDataAutoAllocation.downloadFormBooking');
            Route::post('cancel-allocation','MasterDataAutoAllocationController@cancelAllocation')->name('masterDataAutoAllocation.cancelAllocation');
            Route::post('allocating-pobuyer', 'MasterDataAutoAllocationController@allocatingPoBuyer')->name('masterDataAutoAllocation.allocatingPoBuyer');
            Route::put('recalculate/{id}', 'MasterDataAutoAllocationController@recalculate')->name('masterDataAutoAllocation.recalculate');
            Route::post('delete', 'MasterDataAutoAllocationController@destroy')->name('masterDataAutoAllocation.delete');
            Route::post('delete-bulk', 'MasterDataAutoAllocationController@deleteBulk')->name('masterDataAutoAllocation.deleteBulk');
            Route::post('update', 'MasterDataAutoAllocationController@update')->name('masterDataAutoAllocation.update');
            Route::post('store', 'MasterDataAutoAllocationController@store')->name('masterDataAutoAllocation.store');
            Route::post('update-bulk', 'MasterDataAutoAllocationController@uploadFormInput')->name('masterDataAutoAllocation.uploadFormInput');
            Route::post('upload-new-allocation', 'MasterDataAutoAllocationController@uploadFormInput')->name('masterDataAutoAllocation.uploadFormInput');
            Route::post('upload-update-allocation', 'MasterDataAutoAllocationController@uploadFormUpdate')->name('masterDataAutoAllocation.uploadFormUpdate');
            Route::get('upload-recalculate','MasterDataAutoAllocationController@uploadRecalculate')->name('masterDataAutoAllocation.uploadRecalculate');
            Route::post('import-recalculate', 'MasterDataAutoAllocationController@importRecalculate')->name('masterDataAutoAllocation.importRecalculate');
            Route::get('eta-delay','MasterDataAutoAllocationController@etaDelay')->name('masterDataAutoAllocation.etaDelay');
            Route::get('download-eta-delay','MasterDataAutoAllocationController@downloadEtaDelay')->name('masterDataAutoAllocation.downloadEtaDelay');
            Route::post('upload-eta-delay','MasterDataAutoAllocationController@uploadEtaDelay')->name('masterDataAutoAllocation.uploadEtaDelay');
            Route::get('recalculate-dashboard','MasterDataAutoAllocationController@recalculateDashboard')->name('masterDataAutoAllocation.recalculateDashboard');
            Route::get('download-recalculate-dashboard','MasterDataAutoAllocationController@downloadRecalculateDashboard')->name('masterDataAutoAllocation.downloadRecalculateDashboard');
            Route::post('upload-recalculate-dashboard','MasterDataAutoAllocationController@uploadRecalculateDashboard')->name('masterDataAutoAllocation.uploadRecalculateDashboard');
            Route::get('remove-from-dashboard','MasterDataAutoAllocationController@removeFromDashboard')->name('masterDataAutoAllocation.removeFromDashboard');
            Route::get('download-remove-from-dashboard','MasterDataAutoAllocationController@downloadRemoveFromDashboard')->name('masterDataAutoAllocation.downloadRemoveFromDashboard');
            Route::post('upload-remove-from-dashboard','MasterDataAutoAllocationController@uploadRemoveFromDashboard')->name('masterDataAutoAllocation.uploadRemoveFromDashboard');


        });

        Route::prefix('defect')->middleware(['permission:menu-defect'])->group(function () 
        {
            Route::get('','MasterDataDefectController@index')->name('masterDataDefect.index');
            Route::get('data', 'MasterDataDefectController@data')->name('masterDataDefect.data');
            Route::get('create', 'MasterDataDefectController@create')->name('masterDataDefect.create');
            Route::get('edit/{id}','MasterDataDefectController@edit')->name('masterDataDefect.edit');
            Route::post('store', 'MasterDataDefectController@store')->name('masterDataDefect.store');
            Route::delete('delete/{id}', 'MasterDataDefectController@destroy')->name('masterDataDefect.delete');
            Route::post('update/{id}', 'MasterDataDefectController@update')->name('masterDataDefect.update');
           
        });

        Route::prefix('fabric-testing')->middleware(['permission:menu-testing-lab'])->group(function () 
        {
            Route::get('','MasterDataFabricTestingController@index')->name('masterDataFabricTesting.index');
            Route::get('data', 'MasterDataFabricTestingController@data')->name('masterDataFabricTesting.data');
            Route::get('create', 'MasterDataFabricTestingController@create')->name('masterDataFabricTesting.create');
            Route::get('getRequirements', 'MasterDataFabricTestingController@getRequirements')->name('masterDataFabricTesting.getRequirements');
            Route::post('import', 'MasterDataFabricTestingController@import')->name('masterDataFabricTesting.import');
            Route::get('getTypeSpecimen', 'MasterDataFabricTestingController@getTypeSpecimen')->name('masterDataFabricTesting.getTypeSpecimen');
            Route::post('save', 'MasterDataFabricTestingController@save')->name('masterDataFabricTesting.save');
            Route::get('barcode/{id}', 'MasterDataFabricTestingController@barcode')->name('masterDataFabricTesting.barcode');
            Route::get('getPoreference', 'MasterDataFabricTestingController@getPoreference')->name('masterDataFabricTesting.getPoreference');
            Route::get('getSize', 'MasterDataFabricTestingController@getSize')->name('masterDataFabricTesting.getSize');
            Route::get('getStyle', 'MasterDataFabricTestingController@getStyle')->name('masterDataFabricTesting.getStyle');
            Route::get('getCategory', 'MasterDataFabricTestingController@getCategory')->name('masterDataFabricTesting.getCategory');
            Route::post('update', 'MasterDataFabricTestingController@update')->name('masterDataFabricTesting.update');
            Route::get('print_detail/{id}', 'MasterDataFabricTestingController@print_detail')->name('masterDataFabricTesting.print_detail');
            Route::get('getBarcodeFabric', 'MasterDataFabricTestingController@getBarcodeFabric')->name('masterDataFabricTesting.getBarcodeFabric');
           
            
           
        });

        Route::prefix('overview-fabric-testing')->middleware(['permission:menu-testing-lab'])->group(function () 
        {
            Route::get('','FabricReportOverviewTestingLabController@index')->name('reportOverviewTestingLab.index');
            Route::get('data', 'FabricReportOverviewTestingLabController@data')->name('reportOverviewTestingLab.data');
                 
        });



        Route::prefix('material-saving')->middleware(['permission:menu-material-saving-fabric'])->group(function () 
        {
            Route::get('', 'MasterDataMaterialSavingController@index')->name('masterDataMaterialSaving.index');
            Route::get('data', 'MasterDataMaterialSavingController@data')->name('masterDataMaterialSaving.data');
            Route::get('create', 'MasterDataMaterialSavingController@create')->name('masterDataMaterialSaving.create');
            Route::get('edit/{id}', 'MasterDataMaterialSavingController@edit')->name('masterDataMaterialSaving.edit');
            Route::get('export-file-upload', 'MasterDataMaterialSavingController@exportFileUpload')->name('masterDataMaterialSaving.exportFileUpload');
            Route::get('export', 'MasterDataMaterialSavingController@export')->name('masterDataMaterialSaving.export');
            Route::post('import-file-excel', 'MasterDataMaterialSavingController@importFileExcel')->name('masterDataMaterialSaving.importFileExcel');
            Route::post('update/{id}', 'MasterDataMaterialSavingController@update')->name('masterDataMaterialSaving.update');
            Route::post('print/{id}', 'MasterDataMaterialSavingController@print')->name('masterDataMaterialSaving.print');
            Route::delete('delete/{id}', 'MasterDataMaterialSavingController@delete')->name('masterDataMaterialSaving.delete');
    
        });

        Route::prefix('area-and-locator')->middleware(['permission:menu-area'])->group(function () 
        {
            Route::get('','MasterDataAreaAndLocatorController@index')->name('masterDataAreaAndLocator.index');
            Route::get('data','MasterDataAreaAndLocatorController@data')->name('masterDataAreaAndLocator.create');
            Route::get('create','MasterDataAreaAndLocatorController@create')->name('masterDataAreaAndLocator.create');
            Route::get('edit/{id}','MasterDataAreaAndLocatorController@edit')->name('masterDataAreaAndLocator.edit');
            Route::post('store', 'MasterDataAreaAndLocatorController@store')->name('masterDataAreaAndLocator.store');
            Route::post('update/{id}', 'MasterDataAreaAndLocatorController@update')->name('masterDataAreaAndLocator.update');
            Route::delete('delete/{id}', 'MasterDataAreaAndLocatorController@destroy')->name('masterDataAreaAndLocator.destroy');
            
        });

        Route::prefix('reroute')->middleware(['permission:menu-reroute-buyer'])->group(function () 
        {
            Route::get('', 'MasterDataReroutePoBuyerController@index')->name('masterDataReroute.index');
            Route::get('data', 'MasterDataReroutePoBuyerController@data')->name('masterDataReroute.data');
            Route::get('buyer-picklist', 'MasterDataReroutePoBuyerController@BuyerPickList')->name('masterDataReroute.oldBuyerPickList');
            Route::get('import', 'MasterDataReroutePoBuyerController@import')->name('masterDataReroute.import');
            Route::get('import/download-form-import', 'MasterDataReroutePoBuyerController@downloadFromImport')->name('masterDataReroute.downloadFromImport');
            Route::post('refresh/{id}', 'MasterDataReroutePoBuyerController@refresh')->name('masterDataReroute.refresh');
            Route::post('store', 'MasterDataReroutePoBuyerController@store')->name('masterDataReroute.store');
            Route::post('import/update-form-import', 'MasterDataReroutePoBuyerController@uploadFromImport')->name('masterDataReroute.uploadFromImport');
        });

        Route::prefix('destination')->middleware(['permission:menu-destination'])->group(function () 
        {
            Route::get('','MasterDataDestinationController@index')->name('destination.index');
            Route::get('data','MasterDataDestinationController@data')->name('destination.data');
            Route::get('create','MasterDataDestinationController@create')->name('destination.create');
            Route::get('edit/{id}','MasterDataDestinationController@edit')->name('destination.edit');
            Route::post('store', 'MasterDataDestinationController@store')->name('destination.store');
            Route::post('update/{id}', 'MasterDataDestinationController@update')->name('destination.update');
            Route::delete('delete/{id}', 'MasterDataDestinationController@destroy')->name('destination.delete');
        });

        Route::prefix('material-print-bom')->middleware(['permission:menu-material-exclude'])->group(function () 
        {
            Route::get('','MasterDataMaterialPrintBomController@index')->name('masterDataMaterialPrintBom.index');
            Route::get('data','MasterDataMaterialPrintBomController@data')->name('masterDataMaterialPrintBom.data');
            Route::get('create','MasterDataMaterialPrintBomController@create')->name('masterDataMaterialPrintBom.create');
            Route::get('edit/{id}','MasterDataMaterialPrintBomController@edit')->name('masterDataMaterialPrintBom.edit');
            Route::get('import','MasterDataMaterialPrintBomController@import')->name('masterDataMaterialPrintBom.import');
            Route::get('download-file-import','MasterDataMaterialPrintBomController@downloadFileImport')->name('masterDataMaterialPrintBom.downloadFileImport');
            Route::post('store', 'MasterDataMaterialPrintBomController@store')->name('masterDataMaterialPrintBom.store');
            Route::post('upload-from-excel', 'MasterDataMaterialPrintBomController@uploadFromImport')->name('masterDataMaterialPrintBom.uploadFromImport');
            Route::post('update/{id}', 'MasterDataMaterialPrintBomController@update')->name('masterDataMaterialPrintBom.update');
            Route::delete('delete/{id}', 'MasterDataMaterialPrintBomController@destroy')->name('masterDataMaterialPrintBom.delete');
        });

        Route::prefix('po-buyer')->middleware(['permission:menu-master-po-buyer'])->group(function () 
        {
            Route::get('','PoBuyerController@index')->name('masterDataPoBuyer.index');
            Route::get('data','PoBuyerController@data')->name('masterDataPoBuyer.data');
            Route::get('create','PoBuyerController@create')->name('masterDataPoBuyer.create');
            Route::get('cancel-bulk','PoBuyerController@cancelBulk')->name('masterDataPoBuyer.cancelBulk');
            Route::get('cancel-bulk/export','PoBuyerController@exportCancelForm')->name('masterDataPoBuyer.exportCancelForm');
            Route::post('cancel-bulk/import', 'PoBuyerController@importCancelForm')->name('masterDataPoBuyer.importCancelForm');
            Route::post('cancel/{po_buyer}', 'PoBuyerController@cancelPoBuyer')->name('masterDataPoBuyer.cancel');
            Route::get('upload-psd', 'PoBuyerController@uploadPSD')->name('masterDataPoBuyer.uploadPSD');
            Route::get('upload-psd/export-psd', 'PoBuyerController@exportPSD')->name('masterDataPoBuyer.exportPSD');
            Route::post('upload-psd/import-psd', 'PoBuyerController@importPSD')->name('masterDataPoBuyer.importPSD');
        });

        Route::prefix('mapping-stock')->middleware(['permission:menu-master-mapping-stock'])->group(function () 
        {
            Route::get('','MasterDataMappingStockController@index')->name('masterDataMappingStock.index');
            Route::get('data','MasterDataMappingStockController@data')->name('masterDataMappingStock.data');
            Route::get('create','MasterDataMappingStockController@create')->name('masterDataMappingStock.create');
            Route::get('import','MasterDataMappingStockController@import')->name('masterDataMappingStock.import');
            Route::get('edit/{id}','MasterDataMappingStockController@edit')->name('masterDataMappingStock.edit');
            Route::post('update/{id}', 'MasterDataMappingStockController@update')->name('masterDataMappingStock.update');
            Route::post('store', 'MasterDataMappingStockController@store')->name('masterDataMappingStock.store');
            Route::delete('delete/{id}', 'MasterDataMappingStockController@destroy')->name('masterDataMappingStock.delete');
           
        });

        Route::prefix('item')->middleware(['permission:menu-master-item'])->group(function () 
        {
            Route::get('','MasterDataItemController@index')->name('masterDataItem.index');
            Route::get('data','MasterDataItemController@data')->name('masterDataItem.data');
            Route::get('uom-conversion/{item_id}','MasterDataItemController@dataUomConversion')->name('masterDataItem.dataUomConversion');
            Route::get('erp-item-picklist','MasterDataItemController@erpItemPickList')->name('masterDataItem.erpItemPickList');
            Route::post('store','MasterDataItemController@store')->name('masterDataItem.store');
        });

        Route::prefix('po-supplier')->middleware(['permission:menu-master-supplier'])->group(function () 
        {
            Route::get('','MasterDataPoSupplierController@index')->name('masterDataPoSupplier.index');
            Route::get('data','MasterDataPoSupplierController@data')->name('masterDataPoSupplier.data');
            Route::get('item/{c_order_id}','MasterDataPoSupplierController@dataItem')->name('masterDataPoSupplier.dataItem');
            Route::get('erp-po-supplier-picklist','MasterDataPoSupplierController@erpPoSupplierPickList')->name('masterDataPoSupplier.erpPoSupplierPickList');
            Route::post('store','MasterDataPoSupplierController@store')->name('masterDataPoSupplier.store');
        });

        Route::prefix('criteria')->middleware(['permission:menu-criteria'])->group(function () 
        {
            Route::get('','CriteriaController@index')->name('criteria.index');
            Route::get('create','CriteriaController@create')->name('criteria.create');
            Route::get('{id}/edit','CriteriaController@edit')->name('criteria.edit');
            Route::get('{id}/detail','CriteriaController@detail')->name('criteria.detail');
        });
        
        Route::prefix('user-management')->group(function () 
        {
            Route::prefix('permission')->middleware(['permission:menu-permission'])->group(function () 
            {
                Route::get('','MasterDataPermissionController@index')->name('masterDataPermission.index');
                Route::get('data','MasterDataPermissionController@data')->name('masterDataPermission.data');
                Route::get('create','MasterDataPermissionController@create')->name('masterDataPermission.create');
                Route::get('edit/{id}','MasterDataPermissionController@edit')->name('masterDataPermission.edit');
                Route::post('store', 'MasterDataPermissionController@store')->name('masterDataPermission.store');
                Route::delete('delete/{id}', 'MasterDataPermissionController@destroy')->name('masterDataPermission.delete');
                Route::post('update/{id}', 'MasterDataPermissionController@update')->name('masterDataPermission.update');
            });

            Route::prefix('role')->middleware(['permission:menu-role'])->group(function () 
            {
                Route::get('','MasterDataRoleController@index')->name('masterDataRole.index');
                Route::get('data','MasterDataRoleController@data')->name('masterDataRole.data');
                Route::get('create','MasterDataRoleController@create')->name('masterDataRole.create');
                Route::get('edit/{id}','MasterDataRoleController@edit')->name('masterDataRole.edit');
                Route::post('store', 'MasterDataRoleController@store')->name('masterDataRole.store');
                Route::put('update/{id}','MasterDataRoleController@update')->name('masterDataRole.update');
                Route::delete('delete/{id}', 'MasterDataRoleController@destroy')->name('masterDataRole.delete');
                Route::get('/permission/list', 'MasterDataPermissionController@picklist')->name('masterDataPermission.picklist');
            });

            Route::prefix('user')->middleware(['permission:menu-user'])->group(function () {
                Route::get('','MasterDataUserController@index')->name('masterDataUser.index');
                Route::get('data','MasterDataUserController@data')->name('masterDataUser.data');
                Route::get('data-role/{id}','MasterDataUserController@dataRole')->name('masterDataUser.dataRole');
                Route::get('create','MasterDataUserController@create')->name('masterDataUser.create');
                Route::get('edit/{id}','MasterDataUserController@edit')->name('masterDataUser.edit');
                Route::get('employee-picklist', 'MasterDataUserController@employeePickList')->name('masterDataUser.employeePickList');
                Route::get('role-picklist', 'MasterDataUserController@rolePickList')->name('masterDataUser.rolePickList');
                Route::post('store', 'MasterDataUserController@store')->name('masterDataUser.store');
                Route::post('store/role', 'MasterDataUserController@storeRole')->name('masterDataUser.storeRole');
                Route::post('update/{id}', 'MasterDataUserController@update')->name('masterDataUser.update');
                Route::put('delete/{id}', 'MasterDataUserController@destroy')->name('masterDataUser.delete');
                Route::put('reset-password/{id}', 'MasterDataUserController@resetPassword')->name('masterDataUser.resetPassword');
                Route::post('delete/{id}/role-user/{role_id}', 'MasterDataUserController@destroyRoleUser')->name('masterDataUser.deleteRoleUser');
                
            });

        });
    });
    
    Route::prefix('erp')->group(function () 
    {
        Route::prefix('material-requirement')->middleware(['permission:menu-material-requirement'])->group(function () 
        {
            Route::get('', 'ErpMaterialRequirementController@index')->name('erpMaterialRequirement.index');
            Route::get('export','ErpMaterialRequirementController@export')->name('erpMaterialRequirement.export');
            Route::get('manual-upload-export', 'ErpMaterialRequirementController@uploadManualexport')->name('erpMaterialRequirement.uploadManualexport');
            Route::post('import', 'ErpMaterialRequirementController@import')->name('erpMaterialRequirement.import');
            Route::post('import-manual-upload', 'ErpMaterialRequirementController@importUploadManual')->name('erpMaterialRequirement.importUploadManual');
            Route::post('store', 'ErpMaterialRequirementController@store')->name('erpMaterialRequirement.store');
            Route::get('recalculate', 'ErpMaterialRequirementController@recalculate')->name('erpMaterialRequirement.recalculate');
            Route::get('sync-per-item', 'ErpMaterialRequirementController@syncPerItem')->name('erpMaterialRequirement.syncPerItem');
            Route::get('export-sync-per-item', 'ErpMaterialRequirementController@exportSyncPerItem')->name('erpMaterialRequirement.exportSyncPerItem');
            Route::post('import-sync-per-item', 'ErpMaterialRequirementController@importsyncPerItem')->name('erpMaterialRequirement.importsyncPerItem');
        });
    });

    Route::prefix('report')->group(function () 
    {
        Route::prefix('material-movement')->group(function () 
        {
            Route::get('', 'ReportMaterialMovementController@index')->name('reportMaterialMovement.index');
            Route::get('po-buyer-picklist','ReportMaterialMovementController@poBuyerPicklist')->name('reportMaterialMovement.poBuyerPicklist');
            Route::get('data', 'ReportMaterialMovementController@data')->name('reportMaterialMovement.data');
            Route::get('export', 'ReportMaterialMovementController@export')->name('reportMaterialMovement.export');
        });

        Route::prefix('memo-request-production')->group(function () 
        {
            Route::get('', 'ReportMemoRequestProductionController@index')->name('reportMemoRequestProduction.index');
            Route::get('po-buyer-picklist','ReportMemoRequestProductionController@poBuyerPicklist')->name('reportMemoRequestProduction.poBuyerPicklist');
            Route::get('data', 'ReportMemoRequestProductionController@data')->name('reportMemoRequestProduction.data');
            Route::get('export', 'ReportMemoRequestProductionController@export')->name('reportMemoRequestProduction.export');
            Route::get('print', 'ReportMemoRequestProductionController@printPreview')->name('reportMemoRequestProduction.print');
            Route::get('download-all', 'ReportMemoRequestProductionController@downloadAll')->name('reportMemoRequestProduction.downloadAll');
            Route::post('closing', 'ReportMemoRequestProductionController@closing')->name('reportMemoRequestProduction.closing');
            Route::get('history-data', 'ReportMemoRequestProductionController@dataHistory')->name('reportMemoRequestProduction.dataHistory');
            Route::get('import', 'ReportMemoRequestProductionController@import')->name('reportMemoRequestProduction.import');
            Route::get('export-closing', 'ReportMemoRequestProductionController@exportClosing')->name('reportMemoRequestProduction.exportClosing');
            Route::post('import-closing', 'ReportMemoRequestProductionController@importClosing')->name('reportMemoRequestProduction.importClosing');
            Route::get('detail-handover-data', 'ReportMemoRequestProductionController@dataDetailHandover')->name('reportMemoRequestProduction.dataDetailHandover');
        });

        Route::prefix('report-preparation-machine')->group(function () 
        {
            Route::get('', 'FabricReportPreparationMachine@index')->name('reportPreparationMachine.index');
            Route::get('data', 'FabricReportPreparationMachine@data')->name('reportPreparationMachine.data');
            Route::get('export', 'FabricReportPreparationMachine@export')->name('reportPreparationMachine.export');
          
        });
    });

    Route::prefix('search')->group(function () 
    {
        Route::get('', 'SearchController@index')->name('search.index');
        Route::get('detail', 'SearchController@detail')->name('search.detail');
        Route::get('reprint/{id}', 'SearchController@print')->name('search.print');
        Route::get('po-buyer-picklist', 'SearchController@poBuyerPicklist')->name('search.poBuyerPicklist');
    });

    
    Route::middleware(['session.timeout'])->group(function () 
    {
        Route::get('/temporary/is-used-by-other', 'TemporaryController@isUsedByOther')->name('temporary.isUsedByOther');
    });

    Route::get('account-setting/{id}', 'HomeController@accountSetting')->name('user.accountSetting');
    Route::get('account-setting/{id}/show-avatar', 'HomeController@showAvatar')->name('user.showAvatar');
    Route::post('account-setting/{id}/update', 'HomeController@updateAccountSetting')->name('user.updateAccountSetting');
         

    Route::post('/material-moq/import',['middleware' => ['permission:menu-material-moq'], 'uses'=> 'MaterialMoqController@import'])->name('materialmoq.import');
    Route::post('/temporary', 'TemporaryController@post')->name('temporary.post');
    Route::delete('/temporary/delete', 'TemporaryController@destroy')->name('temporary.delete');
    Route::delete('/temporary/reset-all', 'TemporaryController@resetAll')->name('temporary.resetAll');
    Route::put('/knowledge-base/search/{id}/reprint-barcode', 'KnowledgedBaseController@reprint')->name('knowledgebase.reprint');
});
