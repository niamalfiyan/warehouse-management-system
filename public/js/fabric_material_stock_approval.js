$(function()
{
	warehouse_id = $('#select_warehouse').val();
	$('#warehouse_id').val(warehouse_id);

    $('#materialStockApprovalTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:-1,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/material-stock-approval/data',
            data: function (d) {
                return $.extend({}, d, {
                    "warehouse": $('#select_warehouse').val(),
                });
            }
        },
        columns: [
			{data: 'history_material_stock_opname_id', name: 'history_material_stock_opname_id',searchable:true,visible:false,orderable:false},
			{data: 'checkbox', name: 'checkbox',searchable:false,orderable:false},
            {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'created_by', name: 'created_by',searchable:false,visible:true,orderable:true},
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'batch_number', name: 'batch_number',searchable:false,visible:true,orderable:true},
            {data: 'actual_lot', name: 'actual_lot',searchable:false,visible:true,orderable:true},
            {data: 'operator', name: 'operator',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#materialStockApprovalTable').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
	dtable.draw();

	$('#select_warehouse').on('change',function()
	{
		dtable.draw();
	});
});

$('#select_warehouse').on('change',function()
{
	warehouse_id = $('#select_warehouse').val();
	$('#warehouse_id').val(warehouse_id);
});

$('#form').submit(function (event) 
{
	event.preventDefault();
	selectedApproval()
	var list_history_material_stock_opname = $('#list_history_material_stock_opname').val();

	if (!list_history_material_stock_opname) {
	$("#alert_warning").trigger("click", 'Tidak ada item yang dipilih');
	return false;
	}

	bootbox.confirm("Are you sure want to approve this material ?.", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {
					$.unblockUI();
					$('#form').trigger('reset');
					$("#alert_success").trigger("click", 'Data successfully approved');
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500)
						$("#alert_error").trigger("click", 'Please contact ICT');


					if (response['status'] == 422 || response['status'] == 400)
						$("#alert_warning").trigger("click", response.responseJSON);
				}
			})
			.done(function () 
			{
				$('#materialStockApprovalTable').DataTable().ajax.reload();
			});
		}
	});

});

$('#form-approve-all').submit(function (event) 
{
	event.preventDefault();

	bootbox.confirm("Are you sure want to approve all ?.", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form-approve-all').attr('action'),
				data: $('#form-approve-all').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {
					$.unblockUI();
					$('#form-approve-all').trigger('reset');
					$("#alert_success").trigger("click", 'Data successfully approved');
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500)
						$("#alert_error").trigger("click", 'Please contact ICT');


					if (response['status'] == 422 || response['status'] == 400)
						$("#alert_warning").trigger("click", response.responseJSON);
				}
			})
			.done(function () 
			{
				$('#materialStockApprovalTable').DataTable().ajax.reload();
			});
		}
	});

});

function approve(url) 
{
	bootbox.confirm("Are you sure want to approve this data ?", function (result) 
	{
		if (result) 
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "POST",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 400) {
						for (i in response['responseJSON']['errors']) {
							$('#' + i + '_error').addClass('has-error');
							$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
						}
					} else if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please contact ICT');
					}else if (response['status'] == 422) {
						$("#alert_error").trigger("click", response.responseJSON);
					}
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully approved.');
				$('#materialStockApprovalTable').DataTable().ajax.reload();
			});
		}
	});
}

function reject(url) 
{
	bootbox.confirm("Are you sure want to reject this data ?", function (result) 
	{
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "POST",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 400) {
						for (i in response['responseJSON']['errors']) {
							$('#' + i + '_error').addClass('has-error');
							$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
						}
					} if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please contact ICT');
					}
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully rejected');
				$('#materialStockApprovalTable').DataTable().ajax.reload();
			});
		}
	});
}
function selectedApproval()
{
	var list_history_material_stock_opname = new Array();
	$("#materialStockApprovalTable input[type=checkbox]:checked").each(function (){
		list_history_material_stock_opname.push(this.id);
	});
	$('#list_history_material_stock_opname').val(list_history_material_stock_opname);
}