$(function()
{
    $('#report_material_allocation_cancel_order_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-allocation-cancel-order/data',
            data: function(d) {
                return $.extend({}, d, {
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'date_stock', name: 'date_stock',searchable:true,visible:true,orderable:false},
            {data: 'po_buyer_old', name: 'po_buyer_old',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'stock', name: 'stock',searchable:true,visible:true,orderable:true},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:true,visible:true,orderable:true},
            {data: 'available_qty', name: 'available_qty',searchable:true,visible:true,orderable:true},
            {data: 'warehouse_stock', name: 'warehouse_stock',searchable:true,visible:true,orderable:true},
            {data: 'po_buyer_new', name: 'po_buyer_new',searchable:false,visible:true,orderable:false},
            {data: 'lc_date', name: 'lc_date',searchable:false,visible:true,orderable:false},
            {data: 'barcode', name: 'barcode',searchable:false,visible:true,orderable:false},
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
            {data: 'date_alokasi', name: 'date_alokasi',searchable:false,visible:true,orderable:false},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:false},
            {data: 'warehouse_alokasi', name: 'warehouse_alokasi',searchable:false,visible:true,orderable:false}
	    ]
    });

    var dtable = $('#report_material_allocation_cancel_order_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });;
});