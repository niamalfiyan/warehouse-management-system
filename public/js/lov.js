function lov(name, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax(){
		if (item_name == "#item_codeName"){
			var q = $(search).val() + '_' + $('#po_buyerId').val() + '_' + $('#code_from').val();
		} else{
			var q = $(search).val();
		}
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		var obj 		= $(this).data('obj');
		var destination = $(this).data('destination');
		var pobuyer 	= $(this).data('pobuyer');

		if (item_name == '#filter_item_fabricName' )
		{
			$('#filter_item_fabricName').val(id);
			$('#item_id_locator').val(id);
		}else if (item_name == '#po_buyer_locatorName') 
		{
			$('#po_buyer_locatorName').val(id);
			$('#po_buyer_locatorId').val(id);
			$('#filter_po_buyer').val(id);
		}else if (item_name == '#filter_batch_numberName')
		{
			$('#filter_batch_numberName').val(id);
			$('#filter_batch_number').val(id);
		}else if (item_name == '#filter_roll_numberName') 
		{
			$('#filter_roll_numberName').val(id);
			$('#filter_nomor_roll').val(id);
		}else if (item_name == '#supplier_nameName') 
		{
			$(item_id).val(id);
			$(item_name).val(name);
			var _c_bpartner_id = $('#c_bpartner_id').val();
			if(_c_bpartner_id == null || _c_bpartner_id == '')
			{
				$('#c_bpartner_id').val(id);
				$('#supplier_name').val(name);
			}
		}else if (item_name == '#from_rackinName') 
		{
			$('#_from_rackin_id').val(id);
			$(item_id).val(id);
			$(item_name).val(name);
		}
		else if (item_name == '#locator_in_accesoriesName') 
		{
			$('#_to_destination_id').val(id);
			$(item_id).val(id);
			$(item_name).val(name);
		}else if (item_name == '#to_destinationName') 
		{
			$('#_to_destinationId').val(id);
			$(item_id).val(id);
			$(item_name).val(name);
		}else 
		{
			$(item_id).val(id);
			$(item_name).val(name);
		}

		var flag = ['#item_generalName','#po_buyerilaName','#criteriaName','#from_rackinName','#item_outName', '#item_out_febricName', '#item_code_locator', '#po_buyerName', '#po_buyer_inName','#to_destinationName', '#from_rackName','#to_rackName'];

		if (flag.indexOf(item_name) != -1)
		{
			if (item_name == '#po_buyerName')
			{
				$(item_id).trigger("change", id + ';' + destination);
			}else if (item_name == '#from_rackName' || item_name == '#to_rackName')
			{
				$(item_id).trigger("change", id + ';' + pobuyer);
			}else if (item_name == '#criteriaName')
			{
				$('#flag').val(obj['is_know_data_source']);
			}else if (item_name == '#from_rackinName')
			{
				var has_many_po_buyer = $(this).data('manypobuyer');
				$('#rack_po_buyer').val(pobuyer);
				$('#has_many_po_buyer').val(has_many_po_buyer);
				$(item_id).trigger("change", id);
			}else if (item_name == '#item_outName')
			{
				var uom = $(this).data('uom');
				var category = $(this).data('category');
				$('#uom').val(uom);
				$('#category').val(category);
			}else if (item_name == '#item_out_febricName' )
			{
				var uom = $(this).data('uom');
				var batch_number = $(this).data('batchnumber');
				var nomor_roll = $(this).data('noroll');
				var po_buyer = $(this).data('pobuyer');
				var category = $(this).data('category');
				$('#uom').val(uom);
				$('#category').val(category);
				$('#batch_number').val(batch_number);
				$('#nomor_roll').val(nomor_roll);
				$('#po_buyer').val(po_buyer);
			}else if (item_name == '#po_buyerilaName')
			{
				var item_code = $(this).data('itemcode');
				var uom = $(this).data('uom');
				var po_buyer = $(this).data('id');
				var qty = $(this).data('qty');

				$('#msg').addClass('hidden');
				$('#item_code').removeClass('hidden');
				$('#uom').removeClass('hidden');
				$('#po_buyer').removeClass('hidden');
				$('#qty').removeClass('hidden');

				$('#_item_code').html(item_code);
				$('#_uom').html(uom);
				$('#_po_buyer').html(po_buyer);
				$('#_qty').html(qty);
				$('#__item_code').val(item_code);
			}else
			{
				$(item_id).trigger("change", id);
			}
		}

		if (!obj || obj.length == 0)
			obj = [{}];

		//$(item_id).trigger('change', obj);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		if (item_name == '#po_buyer_inName'){
			var cur_po_buyer = $('#curr_po_buyer').val();
			if (cur_po_buyer ==''){
				$(item_id).val('');
				$(item_name).val('');
			}
		}

		if (item_name == '#criteriaName') {
			$('#flag').val('');
			$(item_id).val('');
			$(item_name).val('');
		}

		if ('#po_buyerilaName'){
			$('#msg').removeClass('hidden');
			$('#item_code').addClass('hidden');
			$('#uom').addClass('hidden');
			$('#po_buyer').addClass('hidden');
			$('#qty').addClass('hidden');
			$('#__item_code').val('');
		}

		$(item_id).val('');
		$(item_name).val('');


		var flag = ['#from_rackinName','#po_buyerName', '#to_destinationName', '#from_rackName', '#to_rackName'];
		if (flag.indexOf(item_name) != -1)
			$(item_id).trigger("change", '');

	});

	itemAjax();
}
