list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	var _warehouse_id = $('#select_warehouse').val();
	desinationLov('to_destination',_warehouse_id, '/accessories/material-out/destination-picklist?');
	$('.barcode_value').focus();

	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		//var is_allowed = isAllow();
		var is_not_complated 	= isNotComplated();


		if (list_barcodes.length == 0) {
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			$("#alert_warning").trigger("click", 'Please scan barcode first.');
			return false;
		}

		if (is_not_complated > 0) 
		{
			$('#barcode_value').val('');
			$('.alert_warning').focus();
			$("#alert_error").trigger("click", 'Ada item yang belum lengkap total karton, silahkan cek kembali.');
			return false;
		}

		var destination_id = $('#to_destinationId').val();
		var destination_name = $('#to_destinationName').val();

		if (!destination_id) 
		{
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			$("#alert_warning").trigger("click", 'Please select destination first. (Silahkan pilih tujuan terlebih dahulu)');
			return false;

		}

		bootbox.confirm("Are you sure want to supply material to " + destination_name + " ?.", function (result) {
			if (result) {
				$.ajax({
						type: "POST",
						url: $('#form').attr('action'),
						data: $('#form').serialize(),
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
						},
						success: function (response) {
							$('#barcode_value').val('');
							$('.barcode_value').focus();
							$('#to_destinationName').val('');
							$('#po_buyer').val('');

							$('#form').trigger("reset");
							list_barcodes = [];
							render();
						},
						error: function (response) {
							$.unblockUI();
							$('#barcode_value').val('');
							$('.barcode_value').focus();

							if (response['status'] == 500)
								$("#alert_error").trigger("click", 'Please Contact ICT');


							if (response['status'] == 422)
								$("#alert_info").trigger("click", response.responseJSON);
						}
					})
					.done(function (response) {
						$("#alert_success").trigger("click", 'Material successfully supplied.');
						$("#alert_checkout").addClass('hidden');
						$("#alert_cancel_order").addClass('hidden');
						$("#to_destinationName").attr('disabled', false)
						$("#to_destinationButtonDel").attr('disabled', false)
					});
			}

			if (!result) {
				$("#to_destinationName").val('');
				$('#to_destinationId').val('');
			}
		});

	});

	$('#reset_all_checkout').click(function (event) 
	{
		bootbox.confirm("Are you sure want to reset this data ?.", function (result) {
			if (result) {
				var url_temporary_reset = $('#url_temporaryreset').val();

				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
						type: "DELETE",
						url: url_temporary_reset,
						data: {
							status: 'out'
						},
						beforeSend: function () {
							$.blockUI({
								message: '<i class="icon-spinner4 spinner"></i>',
								overlayCSS: {
									backgroundColor: '#fff',
									opacity: 0.8,
									cursor: 'wait'
								},
								css: {
									border: 0,
									padding: 0,
									backgroundColor: 'transparent'
								}
							});
						},
						complete: function () {
							$.unblockUI();
							$('#barcode_value').val('');
							$('.barcode_value').focus();
							list_barcodes = [];
							$('#form').trigger("reset");
						},
						error: function (response) {
							$.unblockUI();
							if (response['status'] == 500) {
								$("#alert_error").trigger("click", 'Please contact ICT');
							} else {
								for (i in response.responseJSON) {
									$("#alert_error").trigger("click", response.responseJSON[i]);
								}
							}
						}
					})
					.done(function (response) {
						$("#alert_success").trigger("click", 'Successfully Reset');

						render();
					});
			}
		});
	})

});

$('#select_warehouse').on('change',function(){
	var _warehouse_id = $('#select_warehouse').val();
	$('#warehouse_id').val(_warehouse_id);
	desinationLov('to_destination',_warehouse_id, '/accessories/material-out/destination-picklist?');
});



function desinationLov(name,_warehouse_id, url) 
{
	var search 			= '#' + name + 'Search';
	var list 			= '#' + name + 'List';
	var item_id 		= '#' + name + 'Id';
	var item_name 		= '#' + name + 'Name';
	var modal 			= '#' + name + 'Modal';
	var table 			= '#' + name + 'Table';
	var buttonSrc 		= '#ButtonSrc';
	var buttonDel 		= '#' + name + 'ButtonDel';
	var is_cancel 		= $('#is_cancel_order').val();

	function itemAjax() {
		var q = $(search).val();

		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
				url: url + '&_warehouse_id=' + _warehouse_id + '&q=' + q + '&is_cancel=' + is_cancel
			})
			.done(function (data) {
				$(table).html(data);
				pagination(name);
				$(search).focus();
				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');

		$('#_to_destinationId').val(id);
		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function render() 
{
	getIndex();

	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#accessories_material_out_table').html();
	Mustache.parse(tmpl);
	var data = {
		list: list_barcodes
	};
	var html = Mustache.render(tmpl, data);
	$('#tbody_accessories_material_out').html(html);
	$('.barcode_value').focus();
	setPobuyer();
	bind();
}

function isNotComplated() 
{
	var is_not_complated = 0;
	for (var i in list_barcodes) {
		var barcode = list_barcodes[i];
		if (barcode.counter != barcode.total_carton) 
		{
			$('#panel_' + i).css('background-color', '#edb8fc');
			is_not_complated++;
		}

	}
	return is_not_complated;
}

function setPobuyer() 
{
	for (var i in list_barcodes) 
	{
		var barcode 	= list_barcodes[i];
		var _po_buyer 	= $('#po_buyer').val();

		if (!_po_buyer) 
		{
			$('#po_buyer').val(barcode.po_buyer);
			if (barcode.is_po_buyer_cancel) 
			{
				var msg = 'PO BUYER ' + barcode.po_buyer + ' INI CANCEL, SILAHKAN DI MASUKAN KE FREE STOCK !!!. JANGAN DI KELUARIN KE LINE / DISTRIBUSI. KARENA AKAN MENYEBABKAN ERROR KETIKA INTEGRASI';
				$('#is_cancel_order').val(barcode.is_po_buyer_cancel);
				$("#alert_info").trigger("click", msg);
				$("#alert_checkout").removeClass('hidden');
				$('#msg_checkout').html(msg);
			}else if (barcode.is_need_to_handover) 
			{
				if (barcode.warehouse_place == '1000002') var msg = 'Po buyer ' + barcode.po_buyer + ' running production in AOI-1,please checkout to AOI-1';
				else var msg = 'Po buyer ' + barcode.po_buyer + ' running production in AOI-2,please checkout to AOI-2';

				$("#alert_info_2").trigger("click", msg);
				$("#alert_checkout").removeClass('hidden');
				$('#msg_checkout').html(msg);
			}
		}
	}
}

function getIndex() 
{
	for (idx in list_barcodes) {
		list_barcodes[idx]['_id'] = idx;
		list_barcodes[idx]['no'] = parseInt(idx) + 1;
	}
}

function bind() 
{
	$('#barcode_value').on('change', tambahItem);
	$('.ict-log').on('change', gantiValueICTLog);
	$('#_ict_log').on('change', changeValueLog);
	$('.btn-delete-item').on('click', deleteItem);
}


function deleteItem() 
{
	var i = $(this).data('id');
	list_barcodes.splice(i, 1);
	if(list_barcodes.length == 0)
	{
		$('#po_buyer').val('');
		$('#is_cancel_order').val('');
		$('#is_reroute').val('');
		$('#is_need_to_handover').val('');

		$("#alert_checkout").addClass('hidden');
		$('#msg_checkout').html('');

	}
	render();
}

function changeValueLog() 
{
	var _log_log = $('#_ict_log').val();
	_log_log = _log_log.toUpperCase();
	$('#_ict_log').val(_log_log);
}

function checkItem(barcode) 
{
	var has_dash = barcode.indexOf("-");
	var _barcode = '';
	var num_of_row = 0;
	var is_exist = 0;

	if (has_dash != -1) {
		_barcode = barcode.split("-")[0];
		num_of_row = barcode.split("-")[1];
	} else {
		_barcode = barcode;
	}

	for (var i in list_barcodes) {
		var data = list_barcodes[i];
		if (data.barcode.trim() == _barcode.trim()) {
			is_exist = 1;
			var qty_carton = parseInt(data.total_carton);
			var counter = parseInt(data.counter);
			var flag_is_exists = false;

			if (counter >= qty_carton)
				return 4;

			if (num_of_row != 0) {
				if (num_of_row > qty_carton) {
					return 5;
				}
			}

			for (var id in data.details) {
				var detail = data.details[id];

				if (barcode == detail['barcode_supplier'])
					flag_is_exists = true;

				if (counter <= qty_carton) {
					var input = {
						'barcode_supplier': barcode,
					};
				} else {
					return 3;
				}
			}

			if (flag_is_exists == false) {
				data.counter = counter + 1;
				data.details.push(input);
			} else {
				return 1;
			}

			render();
			return 2;
		}
	}

	if (is_exist == 0);
	return 6;
}

function checkPoBuyer(po_buyer, index) 
{
	for (var i in list_barcodes) {
		var barcode = list_barcodes[i];
		if (i == index)
			continue;

		if (barcode.po_buyer != po_buyer) {
			$(".barcode_value").val("");
			$('.barcode_value').focus();
			return false;
		}

	}

	return true;
}

function gantiValueICTLog() 
{
	var i = $(this).data('id');
	list_barcodes[i].ict_log = $(this).val();
	render();
}

function tambahItem() 
{
	var warehouse_id 						= $('#select_warehouse').val();
	var barcode 							= $('#barcode_value').val();
	var url_accessories_material_out_create = $('#url_accessories_material_out_create').val();
	var diff 								= checkItem(barcode);

	if (diff == 1) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		return false;
	} else if (diff == 2) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return false;
	} else if (diff == 3) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Total Carton is higher than data.');
		return false;
	} else if (diff == 4) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'All carton is ready to supply. (Semua karton sudah siap untuk disupplai)');
		return false;
	} else if (diff == 5) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_warning").trigger("click", 'Barcode is wrong. (Barcode salah)');
		return false;
	} else {
		$.ajax({
				type: "GET",
				url: url_accessories_material_out_create,
				data: {
					barcode: barcode,
					warehouse_id:warehouse_id
				},
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function (response) {
					var diff_pobuyer_item = checkPoBuyer(response.po_buyer);

					if (!diff_pobuyer_item) 
					{
						$("#alert_warning").trigger("click", 'Po buyer must be same. (PO Buyer tidak boleh beda)');
						return false;
					}

					if (response.is_reroute) 
					{
						$("#alert_info").trigger("click", 'Po buyer ' + response.po_buyer + ' is reroute.');
						return false;
					}

					var _ict_log = $('#_ict_log').val();
					if (_ict_log != null) 
					{
						response.ict_log = _ict_log;
					}

					if (response.is_error) 
					{
						$("#alert_info").trigger("click", response.remark_error);
					}

					list_barcodes.push(response);
				},
				error: function (response) 
				{
					$.unblockUI();
					$('#barcode_value').val('');
					$('.barcode_value').focus();

					if (response['status'] == 500)
						$("#alert_error").trigger('click', 'Please contact ICT');

					if (response['status'] == 422)
						$("#alert_warning").trigger('click', response.responseJSON);

				}
			})
			.done(function (response) {
				render();
				
			});
	}
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}