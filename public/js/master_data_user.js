$(function()
{
	var page = $('#page').val();
	
	if(page == 'index')
	{
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
		else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully changed.');
		
		var userTable = $('#master_data_user_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/master-data/user-management/user/data',
			},
			fnCreatedRow: function (row, data, index) {
				var info = userTable.page.info();
				var value = index+1+info.start;
				$('td', row).eq(0).html(value);
			},
			columns: [
				{data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
				{data: null, sortable: false, orderable: false, searchable: false},
				{data: 'nik', name: 'nik',searchable:true,visible:true,orderable:true},
				{data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'email', name: 'email',searchable:true,orderable:true},
				{data: 'department', name: 'department',searchable:true,orderable:true},
				{data: 'sex', name: 'sex',searchable:true,orderable:true},
				{data: 'action', name: 'action',searchable:false,orderable:false},
			]
		});
	
		var dtable = $('#master_data_user_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	}else
	{
		if(page == 'create') employeePicklist('employee', '/master-data/user-management/user/employee-picklist?');
		else
		{
			var url_master_data_user_data_role = $('#url_master_data_user_data_role').val();
			$('#master_data_user_role_table').DataTable().destroy();
			$('#master_data_user_role_table tbody').empty();

			var table = $('#master_data_user_role_table').DataTable({
				dom: 'Bfrtip',
				processing: true,
				serverSide: true,
				pageLength:10,
				scrollY:250,
				scroller:true,
				destroy:true,
				deferRender:true,
				bFilter:true,
				ajax: {
					type: 'GET',
					url: url_master_data_user_data_role,
				},
				fnCreatedRow: function (row, data, index) {
					var info = table.page.info();
					var value = index+1+info.start;
					$('td', row).eq(0).html(value);
				},
				columns: [
					{data: null, sortable: false, orderable: false, searchable: false},
					{data: 'display_name', name: 'display_name',searchable:true,orderable:true},
					{data: 'action', name: 'action',searchable:false,orderable:false},
				]
			});
	
			var dtable2 = $('#master_data_user_role_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					if (e.keyCode == 13) {
						// Call the API search function
						dtable2.search(this.value).draw();
					}
					if (this.value == "") {
						dtable2.search("").draw();
					}
					return;
			});
			dtable2.draw();
		}
		
		rolePicklist('role', '/master-data/user-management/user/role-picklist?');
		list_roles = JSON.parse($('#roles').val());
		render();
	}
});

function reset(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () {
        $('#master_data_user_table').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Password successfully reset');
    });
}

$('#form').submit(function (event) 
{
	event.preventDefault();

	var warehouse 	= $('#warehouse').val();
	var nik 		= $('#nik').val();
	
	if (!warehouse) 
	{
		$("#alert_warning").trigger("click", 'Please select warehouse first.');
		return false;
	}

	if (!nik) 
	{
		$("#alert_warning").trigger("click", 'Please insert department first.');
		return false;
	}
	
	bootbox.confirm("Are you sure want to save this data ?.", function (result) 
	{
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					var url = $('#url_master_data_user').attr('href'); 
					document.location.href = url;
				},
				error: function (response) {
					$.unblockUI();
					if(response['status']==500){
						$("#alert_error").trigger("click", 'Please Contact ICT');
					}else{
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
						}
					}
				}
			});
		}
	});
});

function render()
{
	getIndex();
	$('#roles').val(JSON.stringify(list_roles));
	var tmpl = $('#role_table').html();
	Mustache.parse(tmpl);
	var data = { item: list_roles };
	var html = Mustache.render(tmpl, data);
	$('#tbody_role').html(html);
	bind();
}

function bind()
{
	$('#AddItemButton').on('click', tambahItem);
	$('.btn-delete-item').on('click', deleteItem);
		
	$('.input-new').keypress(function(e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			tambahItem();
		}
	});

	$('.input-edit').keypress(function(e) {
		var i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});
}

function checkItem(role_id, index) 
{
	for (var i in list_roles) {
		var role = list_roles[i];
		if (i == index)
			continue;

		if (role.id == role_id)
			return false;
	}

	return true;
}

function getIndex()
{
	for (idx in list_roles) {
		list_roles[idx]['_id'] = idx;
		list_roles[idx]['no'] = parseInt(idx) + 1;
	}
}

function tambahItem()
{
	var name = $('#roleName').val();
	var id = $('#roleId').val();
	var description = $('#description').val();
	
	var input = {
		'id': id,
		'name': name,
		'description': description
	};

	var diff = checkItem(id);
	if (!diff) {
		$('#errorInput').removeClass('hidden');
		return;
	}

	if (name)
		list_roles.push(input);

	render();
}

function deleteItem()
{
	var i = parseInt($(this).data('id'), 10);

	list_roles.splice(i, 1);
	render();
}

function hapus(url) {
	bootbox.confirm("Are you sure want to delete this data ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
					type: "put",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data successfully deleted.');
					$('#master_data_user_table').DataTable().ajax.reload();
				});
		}
	});
}


function employeePicklist(name, url) 
{
	var search 		= '#' + name + 'Search';
	var list 		= '#' + name + 'List';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax() 
	{
		var q = $(search).val();
		
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q,
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);

			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() 
	{
		var nik 		= $(this).data('nik');
		var name 		= $(this).data('name');
		var department = $(this).data('department');
		
		$('#nik').val(nik);
		$('#department').val(department);
		$('#name').val(name);
	}

	function pagination() 
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) 
	{
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');
		$(description).val('');
	});

	itemAjax();
}

function rolePicklist(name, url) 
{
	var search 		= '#' + name + 'Search';
	var list 		= '#' + name + 'List';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax() 
	{
		var q = $(search).val();
		
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q,
		})
		.done(function (data) 
		{
			$(table).html(data);
			pagination(name);

			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() 
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		var page 		= $('#page').val();

		var input = 
		{
			'id': id,
			'name': name
		};

		if(page == 'edit')
        {
			var user_id = $('#user_id').val(); 
			
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url: '/master-data/user-management/user/store/role',
                data: {
                    user_id: user_id,
                    role_id: id
                }
            })
			.done(function () 
			{
                $('#master_data_user_role_table').DataTable().ajax.reload();
            });
		}
		
		list_roles.push(input);
		render();
	}

	function pagination() 
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');
		$(description).val('');
	});

	itemAjax();
}

function hapusModal(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "post",
        url: url,
		beforeSend: function () 
		{
            mappings = [];
        },
        success: function (response) {
            for (idx in response) 
            {
                var data = response[idx];
				var input = 
				{
                    'id': data.id,
                    'name': data.display_name
                };
                list_roles.push(input);
            }
            
            
        }
    })
    .done(function () {
        $('#master_data_user_role_table').DataTable().ajax.reload();
        render();
    });
}