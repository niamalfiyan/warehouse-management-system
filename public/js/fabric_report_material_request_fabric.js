$(function()
{
    $('#fabric_report_material_request_fabric').DataTable({
        dom: 'Bfrtip',
		processing: true,
		serverSide: true,
		pageLength:100,
        ajax: {
            type: 'POST',
            url: 'http://cdms.aoi.co.id/api/report-fabric',

            data: function(d) {
                return $.extend({}, d, {
                    "cutting_date"     : changeFormatDate(),
                });
           },
            dataSrc: '',
        },
        columnDefs: [
            {
                targets: 10,
                createdCell: function (td, cellData, rowData, row, col) {
                    if ( cellData < 0 ) {
                        $(td).css({'color': 'red'});
                    }
                }
            }
        ],
        columns: [
            {data: 'queu', name: 'queu',searchable:true,visible:true,orderable:false},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            {data: 'material', name: 'material',searchable:true,orderable:true},
            {data: 'color', name: 'color',searchable:true,orderable:true},
            {data: 'csi_qty', name: 'csi_qty',searchable:true,orderable:true},
            {data: 'ssp_qty', name: 'ssp_qty',searchable:true,orderable:true},
            {data: 'already_prepared', name: 'over_consum',searchable:true,orderable:true},
            {data: 'over_consum', name: 'over_consum',searchable:true,orderable:true},
            {data: 'balance_whs', name: 'balance_whs',searchable:true,orderable:true},
            {data: 'total_balance', name: 'total_balance',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:true,orderable:true},
            {data: 'remark', name: 'remark',searchable:true,orderable:true},
            {data: 'params', name: 'params',searchable:false,orderable:false,visible:false},
            {data: 'action', render: function (data, type, row) 
            { return '<ul class="icons-list">' +
            '<li class="dropdown">' +
                '<a href="#" class="dropdown-toggle" data-toggle="dropdown">' +
                    '<i class="icon-menu9"></i>'+
                '</a>'+
                '<ul class="dropdown-menu dropdown-menu-right">'+
                '<li><a onclick="remark(\''+ row['params']+'\')" ><i class="icon-pencil6"></i> Remark</a></li>'+
                '<li><a onclick="confirm(\''+ row['params']+'\')" ><i class="icon-pencil6"></i> Confirm</a></li>'+
                '<li><a onclick="reject(\''+ row['params']+'\')" ><i class="icon-pencil6"></i> Reject</a></li>'+
                '</ul>'+
            '</li>'+
            '</ul>'; } },
        ],
    });

    var dtable = $('#fabric_report_material_request_fabric').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();
    
    $('#cutting_date').on('change',function(){
        dtable.draw();
    });

});
function changeFormatDate()
    {
        var tanggal_input = $('#cutting_date').val();
        var tanggal_array = tanggal_input.split('/');
        var cutting_date  = tanggal_array[2]+'-'+tanggal_array[1]+'-'+tanggal_array[0];
        return cutting_date;
    }

function remark(parameter)
{
    var parameters = parameter.split(',');
    var id_plan = parameters[0];
    var cutting_date = parameters[1];
    var articleno = parameters[2];
    var material = parameters[3];
    var part_no = parameters[4];
    var style = parameters[5];

        swal({
            title: "Add Remark",
            text: "Please type remark :",
            type: "input",
            showCancelButton: true,
            confirmButtonColor: "#2196F3",
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Please type remark here"
        },
        function (inputValue) 
        {
            if (inputValue === false) return false;
            if (inputValue === "") 
            {
                swal.showInputError("Bro / Sist, please write it first.");
                return false
            }
            
            
            $.ajaxSetup({
            
            });
    
            $.ajax({
                type: "post",
                url: 'http://cdms.aoi.co.id/api/update-remark-material',
                data: {
                    'remark': inputValue,
                    'id_plan': id_plan,
                    'style': style,
                    'articleno': articleno,
                    'part_no': part_no,
                    'material': material,
                    'cutting_date': cutting_date,
                },
                beforeSend: function () {
                    swal.close();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 422) $("#alert_error").trigger("click", response['responseJSON']);
                    if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                }
            })
            .done(function () {
                $("#alert_success").trigger("click", 'Data successfully updated.');
                $('#fabric_report_material_request_fabric').DataTable().ajax.reload();
            });
        });
    }

function confirm(parameter)
{
    var parameters = parameter.split(',');
    var id_plan = parameters[0];
    var cutting_date = parameters[1];
    var articleno = parameters[2];
    var material = parameters[3];
    var part_no = parameters[4];
    var style = parameters[5];
            
            $.ajaxSetup({
            
            });
    
            $.ajax({
                type: "post",
                url: 'http://cdms.aoi.co.id/api/update-status-material',
                data: {
                    'status': 'confirm',
                    'id_plan': id_plan,
                    'style': style,
                    'articleno': articleno,
                    'part_no': part_no,
                    'material': material,
                    'cutting_date': cutting_date,
                },
                beforeSend: function () {
                    swal.close();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 422) $("#alert_error").trigger("click", response['responseJSON']);
                    if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                }
            })
            .done(function () {
                $("#alert_success").trigger("click", 'Data successfully updated.');
                $('#fabric_report_material_request_fabric').DataTable().ajax.reload();
            });
    }

function reject(parameter)
{
    var parameters = parameter.split(',');
    var id_plan = parameters[0];
    var cutting_date = parameters[1];
    var articleno = parameters[2];
    var material = parameters[3];
    var part_no = parameters[4];
    var style = parameters[5];
            
            $.ajaxSetup({
            
            });
    
            $.ajax({
                type: "post",
                url: 'http://cdms.aoi.co.id/api/update-status-material',
                data: {
                    'status': 'reject',
                    'id_plan': id_plan,
                    'style': style,
                    'articleno': articleno,
                    'part_no': part_no,
                    'material': material,
                    'cutting_date': cutting_date,
                },
                beforeSend: function () {
                    swal.close();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 422) $("#alert_error").trigger("click", response['responseJSON']);
                    if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                }
            })
            .done(function () {
                $("#alert_success").trigger("click", 'Data successfully updated.');
                $('#fabric_report_material_request_fabric').DataTable().ajax.reload();
            });
    }