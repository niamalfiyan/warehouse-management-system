$(function () 
{
	var page 		= $('#page').val();

	if(page == 'index')
	{
		var flag_msg 	= $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
	
		$('#accessories_material_stock_data_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			scroller:true,
			deferRender:true,
			ajax: {
				type: 'GET',
				url: '/accessories/material-stock/data'
			},
			columns: [
				{data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
				{data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
				{data: 'name', name: 'name',searchable:false,visible:true,orderable:true},
				{data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
				{data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
				{data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
				{data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
				{data: 'item_desc', name: 'item_desc',searchable:false,visible:true,orderable:true},
				{data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
				{data: 'uom', name: 'uom',searchable:true,visible:true,orderable:false},
				{data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
				{data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false},
			]
		});

		var dtable = $('#accessories_material_stock_data_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	}else
	{
		list_materials 	= JSON.parse($('#materials').val());
		mapping_stocks 	= JSON.parse($('#mapping_stocks').val());
		suppliers	   	= JSON.parse($('#suppliers').val());
		warehouse_id	= $('#warehouse_id').val();

		POSupplierLov('document_no', '/accessories/material-stock/po-supplier-picklist?');
		locatorLov('locator',warehouse_id, '/accessories/material-stock/locator-picklist?');
		lov('supplier_name', '/accessories/material-stock/supplier-name-picklist?');
		itemLov('item', '/accessories/material-stock/item-picklist?',null);
		render();
	}
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
	locatorLov('locator',warehouse_id, '/accessories/material-stock/locator-picklist?');
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully, please check data before save it.');
			list_materials = response;
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

});

$('#form').submit(function (event) 
{
	event.preventDefault();
	render();
	let url_materialother = $('#url_materialother').attr('href');

	if (checkLocator() > 0){
		$("#alert_warning").trigger("click", 'There some data which doesn\'t have locator yet.');
		return false;
	}

	if(checkHasError()){
		$("#alert_warning").trigger("click", 'Please check item first.');
		return false;
	}

	if(checkHasRemark()){
		$("#alert_warning").trigger("click", 'Please input remark first.');
		return false;
	}

	
	bootbox.confirm("Are you sure want to save this data ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					document.location.href = url_materialother;
				},
				error: function (response) {
					$.unblockUI();
					
					if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
					if(response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
						
				}
			});
		}
	});
	
});


function render() 
{
	getIndex();
	$('#materials').val(JSON.stringify(list_materials));

	var tmpl = $('#material-other-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_materials };
	var html = Mustache.render(tmpl, data);
	$('#tbody-material-other').html(html);

	bind();
}

function getIndex() 
{
	for (idx in list_materials) 
	{
		list_materials[idx]['_id'] = idx;
		list_materials[idx]['no'] = parseInt(idx) + 1;
	}
}

function bind() 
{
	$('#AddItemButton').on('click', tambahItem);
	$('.input-remark-edit').on('change', changeRemark);
	$('.btn-delete-item').on('click', deleteItem);
	$('#itemName').on('click',showModalItemPickList);
	$('#itemButtonLookup').on('click',showModalItemPickList);
	$('.input-new').keypress(function (e) {
		if (e.keyCode == 13) {
			e.preventDefault();
			tambahItem();
		}
	});

	$('.input-edit').keypress(function (e) {
		var i = $(this).data('id');

		if (e.keyCode == 13) {
			e.preventDefault();
			$('#simpan_' + i).click();
		}
	});

	$('.input-number').keypress(function (e) {
		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}


function showModalItemPickList()
{
	$('#itemcModal').modal();
}

function changeRemark()
{
	var i 		= $(this).data('id');
	var data 	= list_materials[i];

	var value	= $('#remarkInput_'+i).val();
	data.remark = value.trim();

	if(value) data.is_warning = false;
	$('#materials').val(JSON.stringify(list_materials));
}

function tambahItem() 
{
	let c_bpartner_id 		= $('#c_bpartner_id').val();
	let supplier_name 		= $('#supplier_name').val();
	let supplier_code		= $('#supplier_code').val();
	let c_order_id 			= 'FREE STOCK';//$('#document_noId').val();
	let document_no 		= 'FREE STOCK';//$('#document_noName').val();
	let po_buyer 			= $('#po_buyer').val();
	let item_id 			= $('#itemId').val();
	let item_code 			= $('#itemName').val();
	let item_desc 			= $('#item_desc').val();
	let uom 				= $('#uom').val();
	let qty 				= $('#qty').val();
	let remark 				= $('#remark').val();
	let locator_name 		= $('#locatorName').val();
	let locator_id 			= $('#locatorId').val();
	let category 			= $('#category').val();
	let source 				= $('#source').val();
	let type_stocks			= $('#type_stock').val();
	let mapping_stock_id	= null;
	let _document_no 		= (document_no)?document_no:'FREE STOCK';
	let _supplier_name 		= (supplier_name)?supplier_name:'FREE STOCK';
	let type_stock 			= null;
	let type_stock_erp_code = null;

	if (!item_id){
		$("#alert_warning").trigger("click", 'Please select item first.');
		return;
	}

	if (!locator_id){
		$("#alert_warning").trigger("click", 'Please select locator first.');
		return;
	}

	if (!qty) {
		$("#alert_warning").trigger("click", 'Please type qty first.');
		return;
	}

	if (!remark) {
		$("#alert_warning").trigger("click", 'Please type remark first.');
		return;
	}

	//get selected value from combo box type stock
	if(type_stocks == 0){
		if(_document_no == 'FREE STOCK' || _document_no == 'FREE STOCK-CELUP')
		{
			if(c_bpartner_id != 'FREE STOCK' && c_bpartner_id != '') 
			{
				supplier_code = getSupplierCode(c_bpartner_id);
			}else
			{
				c_bpartner_id 		= 'FREE STOCK';
				supplier_code 		= 'FREE STOCK';
			}
			if(!c_order_id) c_order_id = 'FREE STOCK';
			let mapping_stock 	= getMappingStock(_document_no);
			type_stock 			= mapping_stock.type_stock;
			type_stock_erp_code = mapping_stock.type_stock_erp_code;
		}else
		{
			supplier_code = getSupplierCode(c_bpartner_id);
			let get_4_digit_from_beginning = document_no.substr(0,4);
			let mapping_stock = getMappingStock(get_4_digit_from_beginning);

			if(mapping_stock!=null){
				type_stock			= mapping_stock.type_stock;
				type_stock_erp_code = mapping_stock.type_stock_erp_code;
			}else{
				let get_5_digit_from_beginning = document_no.substr(0,5);
				mapping_stock 				   = getMappingStock(get_5_digit_from_beginning);
				if(mapping_stock){
					type_stock			= mapping_stock.type_stock;
					type_stock_erp_code = mapping_stock.type_stock_erp_code;
				}else{
					type_stock 			= null;
					type_stock_erp_code = null;
				}
			}
		}
	}else
	{
		//get value from combo box
		_type_stock 		 = $('#type_stock option:selected').text();
		_type_stock_erp_code = type_stocks;
		//set value combo box to input
		type_stock 			 = _type_stock;
		type_stock_erp_code  = _type_stock_erp_code;

		if(c_bpartner_id != 'FREE STOCK' && c_bpartner_id != '') {
			supplier_code = getSupplierCode(c_bpartner_id);
		}else{
			c_bpartner_id 		= 'FREE STOCK';
			supplier_code 		= 'FREE STOCK';
		}
		if(!c_order_id) c_order_id = 'FREE STOCK'; 
	}

	var input = {
		'c_bpartner_id'		: c_bpartner_id,
		'c_order_id'		: c_order_id,
		'document_no'		: _document_no,
		'supplier_code'		: supplier_code,
		'supplier_name'		: _supplier_name,
		'po_buyer'			: po_buyer,
		'item_id'			: item_id,
		'item_code'			: item_code,
		'item_desc'			: item_desc,
		'uom'				: uom,
		'qty'				: qty,
		'remark'			: remark,
		'locator_id'		: locator_id,
		'locator_name'		: locator_name,
		'source'			: source,
		'category'			: category,
		'is_error'			: false,
		'is_warning'		: false,
		'type_stock' 		: type_stock,
		'mapping_stock_id' 	: mapping_stock_id,
		'type_stock_erp_code': type_stock_erp_code,
	};
	list_materials.push(input);
	render();
}

function deleteItem() 
{
	var i = $(this).data('id');
	list_materials.splice(i, 1);
	render();
}

function itemLov(name, url , document_no) 
{
	let search 		= '#' + name + 'Search';
	let item_id 	= '#' + name + 'Id';
	let item_name 	= '#' + name + 'Name';
	let modal 		= '#' + name + 'Modal';
	let table 		= '#' + name + 'Table';
	let buttonSrc 	= '#' + name + 'ButtonSrc';
	let buttonDel 	= '#' + name + 'ButtonDel';
	
	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q +'&document_no='+document_no
		})
		.done(function (data) {
			$(table).html(data);
			pagination();
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		let id 		= $(this).data('id');
		let code 	= $(this).data('code');
		let name 	= $(this).data('name');
		let uom 	= $(this).data('uom');
		let category = $(this).data('category');
		
		$(item_id).val(id);
		$(item_name).val(code);
		$('#item_desc').val(name);
		$('#category').val(category);
		$('#uom').val(uom);
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			let params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');
			e.preventDefault();
			itemAjax();
		});
	}
	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();
	$(buttonSrc).on('click', itemAjax);
	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});
	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function POSupplierLov(name, url) 
{
	let search 		= '#' + name + 'Search';
	let item_id 	= '#' + name + 'Id';
	let item_name 	= '#' + name + 'Name';
	let modal 		= '#' + name + 'Modal';
	let table 		= '#' + name + 'Table';
	let buttonSrc 	= '#' + name + 'ButtonSrc';
	let buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax() 
	{
		let q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) 
		{
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() 
	{
		let id 				= $(this).data('id');
		let name 			= $(this).data('name');
		let supplier 		= $(this).data('supplier');
		let supplier_code 	= $(this).data('suppliercode');
		let bpartner 		= $(this).data('bpartner');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$('#supplier_name').val(supplier);
		$('#c_bpartner_id').val(bpartner);
		$('#supplier_code').val(supplier_code);
		$('#itemName').val(null);
		$('#item_desc').val(null);
		$('#category').val(null);
		$('#uom').val(null);
		$('#itemId').val(null);

		itemLov('item', '/accessories/material-stock/item-picklist?',name);

	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			let params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function locatorLov(name,warehouse_id,url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax(){
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q+'&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}

function locatorPicklist(url) 
{
	let search 		= '#UpdateSearch';
	let item_id 	= '#LocatorId';
	let modal 		= '#updateModal';
	let table 		= '#UpdateTable';
	let buttonSrc 	= '#UpdateButtonSrc';

	function itemAjax() {
		let q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		let id = $(this).data('id');
		let update_id = $('#update_id').val();
		$(item_id).val(id);

		bootbox.confirm("Apakah anda yakin akan mengubah rak untuk data ini ?", function (result) {
			if (result) {
				$.ajax({
					type: "put",
					url: $('#updateformnya').attr('action'),
					data: $('#updateformnya').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 400) {
							for (i in response['responseJSON']['errors']) {
								$('#' + i + '_error').addClass('has-error');
								$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
							}
						} if (response['status'] == 500) {
							$("#alert_error").trigger("click", 'Please Contact ICT');
						}
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data berhasil disimpan');
					$('#dataTableBuilder').DataTable().ajax.reload();
				});
			}else{
				editLocator(update_id);
			}
		});

		
		
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			let params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		
		if (e.keyCode == 13){
			e.preventDefault();
			itemAjax();
		}
			
	});

	itemAjax();
}

function editLocator(id) 
{
	let url = $('#url_material_other_update').attr('href');
	locatorPicklist('/material-other/locator/picklist?');
	$('#updateformnya').attr('action', url);
	$('#update_id').val(id);
	$('#updateModal').modal();
}

function checkLocator()
{
	let flag = 0;
	for (let i in list_materials) {
		let material = list_materials[i];
		if (!material.locator_id)
			flag++;
	}
	return flag;
}

function checkItem() 
{
	let flag = 0;
	for (let i in list_materials) {
		let material = list_materials[i];
		if (!material.item_id)
			flag++;
	}
	return flag;
}

function checkHasError()
{
	var result=false;
	for(var i in list_materials)
	{
		var material = list_materials[i];
		if(material.is_error)
		{
			result = true;
			break;
		}
	}
	return result
}

function checkHasRemark()
{
	var result	= false;
	for(var i in list_materials)
	{
		var material = list_materials[i];

		if(!material.remark && !material.is_error)
		{
			material.is_warning = true;
			result = true;
			break;
		}
	}

	render();
	return result
}

function getMappingStock(document_no)
{
	let findResult = null
	mapping_stocks.forEach(i => {
		let _document_no = i.document_number;
		if(document_no == _document_no){
			findResult = i;
		}
	});

	return findResult;
}

function getSupplierCode(c_bpartner_id)
{
	let findResult = null
	suppliers.forEach(i => {
		let _c_bpartner_id = i.c_bpartner_id;
		if(c_bpartner_id == _c_bpartner_id){
			findResult = i.supplier_code;
		}
	});

	return findResult;
}