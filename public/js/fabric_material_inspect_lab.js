list_barcode_header = JSON.parse($('#barcode-header').val());


$(function()
{
	$('.barcode_value').focus();

	$('#form').submit(function (event) 
	{
		event.preventDefault();

		var load_text								= $('#inspect_load').val();
		var select_inspect_lot_result				= $('#select_inspect_lot_result').val();

		if (list_barcode_header.length == 0)
		{
			$("#alert_warning").trigger("click", 'Please scan barcode first.');
			return false;
		}

		if (!load_text)
		{
			$("#alert_warning").trigger("click", 'Please input actual lot first.');
			return false;
		}

		if (!select_inspect_lot_result)
		{
			$("#alert_warning").trigger("click", 'Please select inspect lot result first.');
			return false;
		}

		bootbox.confirm("Are you sure want to save this data ?.", function (result) {
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function (response) {
						$('#barcode_value').val('');
						$('.barcode_value').focus();
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if(response['return'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');

						if(response['return'] == 422)
							console.log(response);
							$("#alert_error").trigger("click", response.responseJSON);

					}
				})
				.done(function (response) {
					$("#alert_success").trigger("click", 'Data successfully saved.');
					list_barcode_header = [];
					render();
				});
			}
		});
	});
	render();
});

$('#selectAll').click(function () 
{
	var _selectAll = $('#_selectAll').val();
	var status_all = $('#status_all').val();
	if (_selectAll == 0) 
	{
		$('#_selectAll').val('1');
		$("#selectAll").css("background-color", '#ffd400');
		$("#selectAll").css("color", 'white');
		checked();
		changeLoadActual($('#inspect_load').val());
	} else 
	{
		$('#_selectAll').val('0');
		$("#selectAll").css("background-color", '#fcfcfc');
		$("#selectAll").css("color", 'black');
		checked();
		selected();
	}
	render();
});

$('#inspect_load').change(function () 
{
	var load_text = $(this).val();
	changeLoadActual(load_text);
	render();

});

$('#select_inspect_lot_result').change(function () 
{
	var status = $(this).val();
	changeResult(status);
	render();

});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
});


function render()
{
	getIndex();
	$('#barcode-header').val(JSON.stringify(list_barcode_header));
	var tmpl = $('#reject-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_barcode_header };
	var html = Mustache.render(tmpl, data);
	$('#tbody-reject').html(html);
	$('.barcode_value').focus();

	bind();
}

function bind()
{
	$('#barcode_value').on('change', tambahItem);
	$('.checkbox_item').on('click', checkedItem);
	$('.load_Actual').on('change', changeStatus);
	$('.inspect_lab_remark').on('change', changeRemark);
	$('.input-remark').on('change', ChangeRemark);
	$('.btn-delete').on('click',deleteItem);
}

function deleteItem()
{	
	var i = $(this).data('id');
	list_barcode_header.splice(i, 1);
	render();
}

function getIndex()
{
	var status_all = $('#status_all').val();

	for (idx in list_barcode_header) 
	{
		var data = list_barcode_header[idx];
		list_barcode_header[idx]['_idx'] = idx;
		list_barcode_header[idx]['no'] = parseInt(idx) + 1;
		for (var id in list_barcode_header[idx]['status_options']) {
			if (list_barcode_header[idx]['check_all'] == true){
				if (status_all == list_barcode_header[idx]['status_options'][id]['id']) {
					list_barcode_header[idx]['status_options'][id]['selected'] = 'selected';
				}else{
					list_barcode_header[idx]['status_options'][id]['selected'] = '';
				}
			}else{
				if (list_barcode_header[idx]['status'] == list_barcode_header[idx]['status_options'][id]['id']) {
					list_barcode_header[idx]['status_options'][id]['selected'] = 'selected';
				}else{
					list_barcode_header[idx]['status_options'][id]['selected'] = '';
				}
			}
		}
	}
}

function checked()
{
	var _selectAll = $('#_selectAll').val();
	for (var i in list_barcode_header) {
		var data = list_barcode_header[i];
		if (_selectAll == 1)
		{
			$('#check_' + i).prop('checked', true);
			data.check_all = true;
		}else
		{
			$('#check_' + i).prop('checked', false);
			data.check_all = false;
		}
	}
}

function changeStatus()
{
	var i = $(this).data('id');
	load_actual=$('#load_Actual_'+i).val();
	list_barcode_header[i].check_all = false;
	list_barcode_header[i][selected] = 'selected';
	list_barcode_header[i].load_actual=load_actual;
	render();
}

function changeRemark()
{
	var i 				= $(this).data('id');
	inspect_lab_remark	= $('#inspect_lab_remark_'+i).val();
	list_barcode_header[i].inspect_lab_remark=inspect_lab_remark;
	render();
}

function ChangeRemark() 
{
	var i 		= $(this).data('id');
	var remark 	= $('#remarkInput_' + i).val();
	list_barcode_header[i].remark = remark;
	render();
}

function checkedItem()
{
	var i = $(this).data('id');
	var status_all = $('#status_all').val();
	var _selectAll = $('#_selectAll').val();
	if (_selectAll == 1) 
	{
		list_barcode_header[i].check_all = true;
		list_barcode_header[i].status = status_all;
	}
	render();
}

function changeLoadActual(loadText)
{
	for (var i in list_barcode_header) 
	{
		var data = list_barcode_header[i];
		if (_selectAll == 1) 
		{
			$('#load_Actual_'+i).val(list_barcode_header[i].load_actual);
			data.status = selected;
		}else
		{
			$('#load_Actual_'+i).val(loadText);
			list_barcode_header[i].load_actual=$('#load_Actual_'+i).val();
			//data.status=1;
			data.status = data._status;
		}

	}
	render();
}

function changeResult(result)
{
	for (var i in list_barcode_header) 
	{
		var data = list_barcode_header[i];
		data.inspect_lot_result = result;

	}
}

function selected(selected)
{
	var _selectAll = $('#_selectAll').val();
	for (var i in list_barcode_header) 
	{
		var data = list_barcode_header[i];
		if (_selectAll == 1) 
		{
			$('#status_' + i).removeClass('hidden');
			$('#status_' + i).text(selected);
			$('#status_option_' + i).addClass('hidden');
			data.status = selected;
		}else
		{
			$('#status_' + i).addClass('hidden');
			$('#status_option_' + i).removeClass('hidden');
			data.status = data._status;
		}

	}
	render();
}

function checkItem(barcode_val, index) 
{
	var n = barcode_val.includes("-");

	if(n)
		var _temp = barcode_val.split("-")[0];
	else
		var _temp = barcode_val;
	for (var i in list_barcode_header) 
	{
		var barcode = list_barcode_header[i];
		if (i == index)
			continue;

		if (barcode.barcode.toUpperCase() == _temp.toUpperCase()){
			return false;
		}

	}
	return true;
}

function tambahItem()
{
	var warehouse_id 							= $('#select_warehouse').val();
	var barcode 								= $('#barcode_value').val();
	var load_text								= $('#inspect_load').val();
	var select_inspect_lot_result				= $('#select_inspect_lot_result').val();
	var url_fabric_material_inspect_lab_create 	= $('#url_fabric_material_inspect_lab_create').val();
	var diff 									= checkItem(barcode);

	if (!diff) 
	{
		$("#alert_warning").trigger("click", 'Barcode has scanned.');
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		return;
	}

	$.ajax({
		type: "GET",
		url: url_fabric_material_inspect_lab_create,
		data: {
			barcode			: barcode,
			warehouse_id 	: warehouse_id
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			if(load_text) response.load_actual 		= load_text;
			if(select_inspect_lot_result) response.inspect_lot_result = select_inspect_lot_result;
			
			list_barcode_header.push(response);
			$('#barcode_value').val('');
			$('.barcode_value').focus();
		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT');

			if (response['status'] == 422)
				$("#alert_warning").trigger("click", response.responseJSON);
		}
	})
	.done(function () 
	{
		checked();
		render();
	});;
}