$(function()
{
    $('#select_warehouse').trigger('change');
    $('#daily_material_quality_control_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/daily-material-quality-control/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                    "user_id"       : $('#select_user_inspection').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'inspection_date', name: 'inspection_date',searchable:true,visible:true,orderable:true},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
            {data: 'user_name', name: 'user_name',searchable:false,visible:true,orderable:true},
            {data: 'receiving_date', name: 'receiving_date',searchable:false,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:false},
            {data: 'total_point', name: 'total_point',searchable:false,visible:true,orderable:false},
            {data: 'percentage', name: 'percentage',searchable:false,visible:true,orderable:false},
            {data: 'remark', name: 'remark',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });

    var dtable = $('#daily_material_quality_control_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#select_user_inspection').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
})

$('#select_warehouse').on('change',function()
{
    var value   = $(this).val(); 
    var user_id = $('#user_id').val();
    
    if(value)
    {
        $.ajax({
            type: 'get',
            url: '/fabric/report/daily-material-quality-control/get-user-picklist?warehouse_id='+value+'&user_id='+user_id
        })
        .done(function(response){
            var users = response.users;
            var user_id = response.user_id;
            
            $("#select_user_inspection").empty();
            $("#select_user_inspection").append('<option value="">-- Select User Inspection --</option>');

            $.each(users,function(id,name){
                if(user_id == id) var selected = 'selected';
                else var selected = null;

                $("#select_user_inspection").append('<option value="'+id+'"'+selected+'>'+name+'</option>');
            });

            if(user_id)
            {
                $('#select_user_inspection').trigger('change');
            }
        })
    }else
    {
        $("#select_user_inspection").empty();
        $("#select_user_inspection").append('<option value="">-- Select User Inspection --</option>');
    }
});

$('#select_user_inspection').on('change',function()
{
    var value   = $(this).val(); 
    var dtable = $('#daily_material_quality_control_table').dataTable().api();
    $('#select_user_inspection').on('change',function(){
        dtable.draw();
    });
});