list_print_barcode 	= JSON.parse($('#print-barcode').val());
is_check_all		= false;

POBuyerLov('po_buyer_reroute', '/accessories/barcode/reroute/pobuyer-pick-list?');

$('#btn_check_all').on('click',checkAll);
$('#btn_uncheck_all').on('click',unCheckAll);

$('#upload_button').on('click', function () {
	$('#upload_file').trigger('click');
});


$('#btn_print').on('click',function(){
	$('#from_printout').trigger('submit');

	list_print_barcode = [];
	render();
});


function render() 
{
	getIndex();
	$('#print-barcode').val(JSON.stringify(list_print_barcode));
	var tmpl = $('#print-barcode-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_print_barcode };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-print-barcode').html(html);
	bind();
}

function getIndex() 
{
	for (idx in list_print_barcode) 
	{
		list_print_barcode[idx]['_id'] 	= idx;
		list_print_barcode[idx]['no'] 	= parseInt(idx) + 1;
	}
}

function bind() 
{
	$('.input-checked').on('click', checkItem);
}

function checkAll()
{
	for (idx in list_print_barcode) 
	{
		var data 		= list_print_barcode[idx];
		data.checked 	= true;
	}

	render();
}

function unCheckAll()
{
	for (idx in list_print_barcode) 
	{
		var data 		= list_print_barcode[idx];
		data.checked 	= false;
	}

	render();
}


function checkItem()
{
	var i 		= $(this).data('id');
	var data 	= list_print_barcode[i];

	if (document.getElementById('check_'+i).checked) 
	{
		data.checked = true;
	} else 
	{
		data.checked = false;
	}

	render();
}

function POBuyerLov(name, url)
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax() 
	{
		var q = $(search).val();
		
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&po_buyer=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() 
	{
		var url_get_item 	= $('#url_get_item').val();
		var warehouse_id 	= $('#select_warehouse').val();
		var old_po_buyer	= $(this).data('old');
		var po_buyer 		= $(this).data('name');
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "get",
			url: url_get_item,
			data: {
				old_po_buyer	: old_po_buyer,
				po_buyer		: po_buyer,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
				
				list_print_barcode = [];
			},
			complete: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				
				if(response.status == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				if(response.status == 422) $("#alert_warning").trigger("click", response.responseText);
			}
		})
		.done(function (response)
		{
			for (var id in response) 
			{
				var data = response[id];
				list_print_barcode.push(data);
			}
			
			render();
		});
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();
	
	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		var document_no = $('#document_no_fabricId').val();
		var item_code = $('#item_code_planning').val();
		var c_bpartner_id = $('#c_bpartner_id').val();
		
		updateLastPlanning(document_no, c_bpartner_id, item_code, -1)
		$(item_id).val('');
		$(item_name).val('');
		list_data_planning = [];
		render();

	});

	itemAjax();
}


$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_file_print_barcode').attr('action'),
		data: new FormData(document.getElementById("upload_file_print_barcode")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully, please check again.');

			for (idx in response) {
				var data = response[idx];
				var input = 
				{
					'pcd': data.pcd,
					'style': data.style,
					'article': data.article,
					'item_code': data.item_code,
					'po_supplier': data.po_supplier,
					'supplier_name': data.supplier_name,
					'qty_saving': data.qty_saving,
					'warehouse': data.warehouse,
					'status': data.status,
				};
				list_print_barcode.push(input);
			}

		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_print_barcode').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_file_print_barcode').trigger('reset');
		render();
	});

})