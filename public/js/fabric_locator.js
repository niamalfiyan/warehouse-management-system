$('.btn-show-item').on('click', function (e) 
{
	var header 	= $(this).data('header');
	var rack 	= $(this).data('rack');
	var locator = $(this).data('id');

	var rack_name 	= $('#rack_' + header + '_' + rack + '_' + locator).val();
	var locator_id 	= $('#locatorId_' + header + '_' + rack + '_' + locator).val();
	var counter_in 	= $('#counterIn_' + header + '_' + rack + '_' + locator).val();

	if (counter_in == 0) 
	{
		$("#alert_info").trigger("click", 'Locator ' + rack_name + ' is empty');
		return false;
	}

	$('#locator_id').val(locator_id);
	$('#detail_header').text(rack_name);
	$('#detilModal').modal();
	var active_tab = $('.nav-tabs .active').text()
	if(active_tab == 'Summary') $('#active_tab').val('summary').trigger('change');
	if(active_tab == 'Detail Per Po Supplier') $('#active_tab').val('detailPerPoSupplier').trigger('change');
	if(active_tab == 'Detail') $('#active_tab').val('detail').trigger('change');
});


function changeTab(status)
{
	$('#active_tab').val(status).trigger('change');
}

$('#active_tab').on('change',function()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'summary')
	{
		summaryTable();

		var dtableSummary = $('#summary_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableSummary.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableSummary.search("").draw();
				}
				return;
		});
		dtableSummary.draw();

		$('#locator_id').on('change',function(){
			dtableSummary.draw();
		});

		
	}else if(active_tab == 'detailPerPoSupplier')
	{
		detailPerPoSupplier();

		var dtableDetailPerPoSupplier = $('#detail_per_po_supplier_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableDetailPerPoSupplier.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableDetailPerPoSupplier.search("").draw();
				}
				return;
		});
		dtableDetailPerPoSupplier.draw();
		
		$('#locator_id').on('change',function(){
			dtableDetailPerPoSupplier.draw();
		});
	}else if( active_tab == 'detail')
	{
		detailTabel();

		var dtableDetail = $('#detail_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableDetail.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableDetail.search("").draw();
				}
				return;
		});
		dtableDetail.draw();
		
		$('#locator_id').on('change',function(){
			dtableDetail.draw();
		});
	} 
});

function summaryTable()
{
	$('#summary_table').DataTable().destroy();
	$('#summary_table tbody').empty();
	
	$('#summary_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/fabric/locator/data-summary',
			data: function(d) {
                return $.extend({}, d, {
                    "locator_id" : $('#locator_id').val(),
                });
            }
        },
        columns: [
			{data: 'upc', name: 'upc',orderable:true},
			{data: 'total_roll', name: 'total_roll',searchable:false},
			{data: 'total_yard', name: 'total_yard',searchable:false},
		]
    });
}

function detailPerPoSupplier()
{
	$('#detail_per_po_supplier_table').DataTable().destroy();
	$('#detail_per_po_supplier_table tbody').empty();
	
	$('#detail_per_po_supplier_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/fabric/locator/data-detail-per-po-supplier',
			data: function(d) {
                return $.extend({}, d, {
                    "locator_id" : $('#locator_id').val(),
                });
            }
        },
        columns: [
			{data: 'upc', name: 'upc',searchable:true,orderable:true},
			{data: 'document_no', name: 'document_no',searchable:true,orderable:true},
			{data: 'item_code', name: 'item_code',searchable:true,orderable:true},
			{data: 'item_desc', name: 'item_desc',searchable:false},
			{data: 'total_roll', name: 'total_roll',searchable:false},
			{data: 'total_yard', name: 'total_yard',searchable:false},
		]
    });
}

function detailTabel()
{
	$('#detail_table').DataTable().destroy();
	$('#detail_table tbody').empty();
	
	$('#detail_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/fabric/locator/data-detail',
			data: function(d) {
                return $.extend({}, d, {
                    "locator_id" : $('#locator_id').val()
                });
            }
        },
        columns: [
			{data: 'barcode_supplier', name: 'barcode_supplier'},
			{data: 'document_no', name: 'document_no'},
			{data: 'item_code', name: 'item_code'},
			{data: 'item_desc', name: 'item_desc',searchable:false},
			{data: 'batch_number', name: 'batch_number'},
			{data: 'nomor_roll', name: 'nomor_roll'},
			{data: 'uom', name: 'uom'},
			{data: 'load_actual', name: 'load_actual',searchable:false},
			{data: 'available_qty', name: 'available_qty',searchable:false},
		]
    });
}