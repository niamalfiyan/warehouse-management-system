list_barcodes           = JSON.parse($('#barcode_products').val());
list_data_allocation    = JSON.parse($('#data_allocation').val());
temporary_data_handover = JSON.parse($('#temporary_data_handover').val());

$(function () {
  render();
  renderAllocation();
  setFocusToTextBox();

  $('#formSummaryHandover').submit(function (event) {
    event.preventDefault();
    var input_qty_handover = $('#input_qty_handover').val();
    var no_pt_id = $('#no_ptId').val();
    var input_no_pt = $('#input_no_pt').val();

    if (!input_qty_handover) {
      $("#alert_warning").trigger("click", 'Please insert qty move first');
      return false;
    }

    if (!no_pt_id && !input_no_pt) {
      $("#alert_warning").trigger("click", 'Please insert no PT first');
      return false;
    }

    $.ajax({
        type: "post",
        url: $('#formSummaryHandover').attr('action'),
        data: $('#formSummaryHandover').serialize(),
        beforeSend: function () 
        {
          $('#confirmationHandoverModal').modal('hide');
          $.blockUI({
            message: '<i class="icon-spinner4 spinner"></i>',
            overlayCSS: {
              backgroundColor: '#fff',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        complete: function () {
          $.unblockUI();

        },
        success: function (response) {
          $.unblockUI();
          var roll = temporary_data_handover;
          var input = {
            'summary_handover_material_id'  : response.summary_handover_material_id,
            'document_no'                   : response.document_no,
            'item_code'                     : response.item_code,
            'c_bpartner_id'                 : response.c_bpartner_id,
            '_total_qty_outstanding'        : response.total_qty_outstanding,
            'total_qty_outstanding'         : response.total_qty_outstanding,
            'total_qty_supply'              : response.total_qty_supply,
            'uom'                           : response.uom,
            'is_move_order'                 : false,
            'handover_document_no'          : response.handover_document_no,
            'item_id'                       : response.item_id,
            'c_order_id'                    : response.c_order_id,
            'warehouse_id'                  : response.warehouse_id,
          };

          list_data_allocation.push(input);
          selectedPlanning(roll);

          $('#formSummaryHandover').trigger('reset');
          setFocusToTextBox();

          temporary_data_handover = [];
          $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
          $('#barcode_value').val('');
        },
        error: function (response) {
          $.unblockUI();
          $('#barcode_value').focus();
          $('#barcode_value').val('');
          $(".modal-backdrop").remove();

          if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
          if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

          temporary_data_handover = [];
          $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
        }
      })
      .done(function () {
        $('#barcode_value').val('');
        //$('#data_planning').val(JSON.stringify(sort_list(list_data_planning, 'sorting')));

        render();
        renderAllocation();
        setFocusToTextBox();
      });
  });

  $('#formSummaryHandoverMoveOrder').submit(function (event) {
    event.preventDefault();
    var input_qty_handover  = $('#input_qty_handover_move_order').val();
    var no_pt_id            = $('#no_ptId_move_order').val();
    var input_no_pt         = $('#input_no_pt_move_order').val();

    if (!input_qty_handover) {
      $("#alert_warning").trigger("click", 'Please insert qty move first');
      return false;
    }

    if (!no_pt_id && !input_no_pt) {
      $("#alert_warning").trigger("click", 'Please insert destination first');
      return false;
    }

    $.ajax({
      type: "post",
      url: $('#formSummaryHandoverMoveOrder').attr('action'),
      data: $('#formSummaryHandoverMoveOrder').serialize(),
      beforeSend: function () 
      {
        $('#confirmationMoveOrderModal').modal('hide');
        $.blockUI({
          message: '<i class="icon-spinner4 spinner"></i>',
          overlayCSS: {
              backgroundColor: '#fff',
              opacity: 0.8,
              cursor: 'wait'
            },
            css: {
              border: 0,
              padding: 0,
              backgroundColor: 'transparent'
            }
          });
        },
        complete: function () {
          $.unblockUI();

        },
        success: function (response) {
          $.unblockUI();
          var roll = temporary_data_handover;
          var input = {
            'summary_handover_material_id'  : response.summary_handover_material_id,
            'document_no'                   : response.document_no,
            'item_code'                     : response.item_code,
            'c_bpartner_id'                 : response.c_bpartner_id,
            '_total_qty_outstanding'        : response.total_qty_outstanding,
            'total_qty_outstanding'         : response.total_qty_outstanding,
            'total_qty_supply'              : response.total_qty_supply,
            'uom'                           : response.uom,
            'is_move_order'                 : true,
            'handover_document_no'          : response.handover_document_no,
            'item_id'                       : response.item_id,
            'c_order_id'                    : response.c_order_id,
          };

          list_data_allocation.push(input);
          selectedPlanning(roll);

          $('#formSummaryHandoverMoveOrder').trigger('reset');
          setFocusToTextBox();

          temporary_data_handover = [];
          $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
          $('#barcode_value').val('');
        },
        error: function (response) {
          $.unblockUI();
          $('#barcode_value').focus();
          $('#barcode_value').val('');
          $(".modal-backdrop").remove();

          if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
          if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

          temporary_data_handover = [];
          $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
        }
      })
      .done(function () {
        $('#barcode_value').val('');
        //$('#data_planning').val(JSON.stringify(sort_list(list_data_planning, 'sorting')));

        render();
        renderAllocation();
        setFocusToTextBox();
      });

  });

  $('#form').submit(function (event) {
    event.preventDefault();

    if (list_barcodes.length == 0) 
    {
      $('#barcode_value').val('');
      $('.barcode_value').focus();
      $("#alert_warning").trigger("click", 'Please scan item first.');
      return false;
    }

    bootbox.confirm("Are you sure want to save this data ?", function (result) {
      if (result) {
        $.ajax({
            type: "POST",
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            beforeSend: function () {
              $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                  backgroundColor: '#fff',
                  opacity: 0.8,
                  cursor: 'wait'
                },
                css: {
                  border: 0,
                  padding: 0,
                  backgroundColor: 'transparent'
                }
              });
            },
            complete: function () {
              $.unblockUI();
            },
            success: function () {
              $('#barcode_value').val('');
              $('.barcode_value').focus();
              list_barcodes = [];
              list_data_allocation = [];
              temporary_data_handover = [];

            },
            error: function (response) {
              $.unblockUI();
              $('#barcode_value').val('');
              $('.barcode_value').focus();

              if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
              if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);
            }
          })
          .done(function (response) {
            $("#alert_success").trigger("click", 'Data successfully saved.');
            $('#selected_div').addClass('hidden');

            if (response != 'no_need_print') {
              $('#list_barcodes').val(JSON.stringify(response));
              $('#getBarcode').submit();
              $('#list_barcodes').val('');
            }

            render();
            renderAllocation();
            setFocusToTextBox();
          });
      }
    });

  });
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
	$('#warehouse_handover_id').val(warehouse_id);
	$('#warehouse_move_order_id').val(warehouse_id);
});

function render() {
  getIndex();
  $('#barcode_products').val(JSON.stringify(list_barcodes));
  var tmpl = $('#out-fabric-table').html();
  Mustache.parse(tmpl);
  var data = {
    list: list_barcodes
  };
  var html = Mustache.render(tmpl, data);
  $('#tbody-material-out-fabric').html(html);
  bind();
  setFocusToTextBox();
}

function renderAllocation() {
  getIndexAllocation();
  $('#data_allocation').val(JSON.stringify(list_data_allocation));
  var data = {
    list: list_data_allocation
  };
}

function getIndex() 
{
  for (idx in list_barcodes) {
    list_barcodes[idx]['_id'] = idx;
    list_barcodes[idx]['no'] = parseInt(idx) + 1;
  }
}

function getIndexAllocation() 
{
  for (idx in list_data_allocation) {
    list_data_allocation[idx]['_id'] = idx;
    list_data_allocation[idx]['no'] = parseInt(idx) + 1;
  }
}

function bind() 
{
  $('.btn-delete-item').on('click', deleteItem);
  $('#barcode_value').on('change', tambahItem);
  $('.reserved-qty').on('change', changeReservedQty);

}

function changeReservedQty() 
{
  var id            = this.id;
  var idx           = $('#' + id).data('id');
  var reserved_qty  = $('#' + id).val();
  var available_qty = list_barcodes[idx].stock - reserved_qty;

  $('#available_qty_' + (idx + 1)).val(available_qty);
  list_barcodes[idx].reserved_qty   = reserved_qty;
  list_barcodes[idx].available_qty  = available_qty;
}

function deleteItem() 
{
  var i             = $(this).data('id');
  var data_prapared = list_barcodes[i];
  var c_order_id    = data_prapared.c_order_id;
  var item_id       = data_prapared.item_id;
  var warehouse_id  = data_prapared.warehouse_id;

  list_barcodes.splice(i, 1);
  removeLastUsed(c_order_id, item_id, warehouse_id);
  recalculate(c_order_id, item_id, warehouse_id);
  renderAllocation();
  render();
}

function recalculate(c_order_id, item_id, warehouse_id) 
{
  var selected;

  for (id in list_data_allocation) 
  {
    var allocation                  = list_data_allocation[id];
    var allocation_supplier_name    = allocation.supplier_name;
    var allocation_document_no      = allocation.document_no;
    var allocation_c_order_id       = allocation.c_order_id;
    var allocation_uom              = allocation.uom;
    var allocation_item_code        = allocation.item_code;
    var allocation_item_id          = allocation.item_id;
    var allocation_warehouse_id     = allocation.warehouse_id;
    var allocation_c_bpartner_id    = allocation.c_bpartner_id;
    var _allocation_qty_outstanding = parseFloat(allocation._total_qty_outstanding);
    var allocation_qty_outstanding  = parseFloat(allocation.total_qty_outstanding);
    var allocation_qty_supply       = parseFloat(allocation.total_qty_supply);
    //var qty_booking = parseFloat(0);

    if (allocation_c_order_id == c_order_id && 
        allocation_item_id == item_id && 
        allocation_warehouse_id == warehouse_id
      ) 
      {
        var new_total_qty_supply = parseFloat(0);

        for (idx in list_barcodes) 
        {
          var roll                = list_barcodes[idx];
          var roll_c_order_id     = roll.c_order_id;
          var roll_warehouse_id   = roll.warehouse_id;
          var roll_item_id   = roll.item_id;
          var roll_qty_booking    = parseFloat(roll.qty_booking);

          if (roll_c_order_id == c_order_id && 
              roll_warehouse_id == warehouse_id && 
              roll_item_id == item_id
          ) 
          {
            new_total_qty_supply += parseFloat(roll_qty_booking);
          }
        }

        var new_total_qty_outstanding     = parseFloat(_allocation_qty_outstanding) - parseFloat(new_total_qty_supply);
        allocation.total_qty_supply       = new_total_qty_supply;
        allocation.total_qty_outstanding  = new_total_qty_outstanding;

        selected = 
        {
          'c_order_id'            : c_order_id,
          'item_id'               : item_id,
          'warehouse_id'          : warehouse_id,
          'document_no'           : allocation_document_no,
          'supplier_name'         : allocation_supplier_name,
          'item_code'             : allocation_item_code,
          'c_bpartner_id'         : allocation_c_bpartner_id,
          '_total_qty_outstanding': _allocation_qty_outstanding,
          'total_qty_outstanding' : parseFloat(new_total_qty_outstanding).toFixed(4),
          'uom'                   : allocation_uom,
        };

    }

  }


  if (list_barcodes.length != 0) 
  {
    var temp = 0;
    for (idx in list_barcodes) 
    {
      var data = list_barcodes[idx];

      if (data.c_order_id == c_order_id && 
        data.warehouse_id == warehouse_id && 
        data.item_id == item_id)
        temp++;
    }

    if (temp > 0) {
      $('#selected_div').removeClass('hidden');
      $('#selected_supplier_name').text(selected.supplier_name);
      $('#selected_document_no').text(selected.document_no);
      $('#selected_item_code').text(selected.item_code);
      $('#selected_qty').text(selected._total_qty_outstanding + ' (' + selected.uom + ')');
      $('#selected_qty_curr').text(selected.total_qty_outstanding + ' (' + selected.uom + ')');
    }

  } else 
  {
    for (id in list_data_allocation) 
    {
      var allocation                = list_data_allocation[id];
      var allocation_c_order_id     = allocation.c_order_id;
      var allocation_item_id        = allocation.item_id;
      var allocation_warehouse_id   = allocation.warehouse_id;

      if (allocation_c_order_id == c_order_id && 
        allocation_warehouse_id == warehouse_id && 
        allocation_item_id == item_id) list_data_allocation.splice(id, 1);
    }
    $('#selected_div').addClass('hidden');
  }
}

function checkHandoverExists(c_order_id, item_id, c_bpartner_id) 
{
  var flag = 0;
  for (var idx in list_data_allocation) 
  {
    var data = list_data_allocation[idx];

    if (data.c_order_id == c_order_id &&
      data.item_id == item_id &&
      data.c_bpartner_id == c_bpartner_id)
      flag++;
  }

  return flag;
}



function removeLastUsed(c_order_id, item_id, warehouse_id) 
{
  var flag = 0;
  for (idx in list_barcodes) {
    var data = list_barcodes[idx];

    if (data.c_order_id == c_order_id 
      && data.item_id == item_id 
      && data.warehouse_id == warehouse_id
    )
      flag++;
  }

  if (flag == 0) {
    var url_remove_last_used = $('#url_remove_last_used').attr('href');

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
      type: "put",
      url: url_remove_last_used,
      data: {
        c_order_id    : c_order_id,
        item_id       : item_id,
        warehouse_id  : warehouse_id
      }
    });
  }

}

function selectedPlanning(response) 
{
  var available_qty                 = parseFloat(response.available_qty);
  var qty_booking                   = parseFloat(0);
  var is_already_full               = 0;
  var _summary_handover_material_id = null;
  var _is_move_order                = null;
  var _handover_document_no         = null;

  for (idx in list_data_allocation) 
  {
    var data = list_data_allocation[idx];

    if (
      data.c_order_id == response.c_order_id &&
      data.item_id == response.item_id
    ) 
    {
      if (parseFloat(data.total_qty_outstanding) <= 0) 
      {
        $("#alert_info").trigger("click", 'kebutuhan untuk item ' + data.item_code + ' asal dari ' + response.document_no + ' sudah terpenuhi semua');
        is_already_full++;
        break;
      }


      if ((available_qty / parseFloat(data.total_qty_outstanding)) >= 1) var reserved_qty = data.total_qty_outstanding;
      else var reserved_qty = available_qty;


      _summary_handover_material_id = data.summary_handover_material_id;
      _is_move_order                = data.is_move_order;
      _handover_document_no         = data.handover_document_no;

      available_qty                 -= parseFloat(reserved_qty);
      data.total_qty_outstanding    = parseFloat(parseFloat(data.total_qty_outstanding) - parseFloat(reserved_qty)).toFixed(4);
      data.total_qty_supply         = parseFloat(parseFloat(data.total_qty_supply) + parseFloat(reserved_qty)).toFixed(4);
      qty_booking                   += parseFloat(reserved_qty);

      selected = {
        'c_order_id'            : data.c_order_id,
        'document_no'           : data.document_no,
        'supplier_name'         : data.supplier_name,
        'item_id'               : data.item_id,
        'item_code'             : data.item_code,
        'c_bpartner_id'         : data.c_bpartner_id,
        '_total_qty_outstanding': data._total_qty_outstanding,
        'total_qty_outstanding' : parseFloat(data.total_qty_outstanding).toFixed(4),
        'uom'                   : data.uom,
      };

      if (available_qty == 0)
        break;

    }

  }

  if (selected) {
    $('#selected_div').removeClass('hidden');
    $('#selected_supplier_name').text(selected.supplier_name);
    $('#selected_document_no').text(selected.document_no);
    $('#selected_item_code').text(selected.item_code);
    $('#selected_qty').text(selected._total_qty_outstanding + ' (' + selected.uom + ')');
    $('#selected_qty_curr').text(selected.total_qty_outstanding + ' (' + selected.uom + ')');

    response.summary_handover_material_id = _summary_handover_material_id;
    response.is_move_order                = _is_move_order;
    response.handover_document_no         = _handover_document_no;

    var _reserved_qty                     = parseFloat(response._reserved_qty);
    response.reserved_qty                 = parseFloat(_reserved_qty + qty_booking).toFixed(4);
    response.qty_booking                  = parseFloat(qty_booking).toFixed(4);
    response.available_qty                = parseFloat(available_qty).toFixed(4);

    if (is_already_full == 0) list_barcodes.push(response);

  }else 
  {
    $('#selected_div').addClass('hidden');
  }

}

function checkBarcode(barcode) {
  var flag = 0;
  for (idx in list_barcodes) {
    var data = list_barcodes[idx];

    if (data.barcode == barcode) 
    {
      flag++;
      break;
    }

  }
  return flag;
}

function ListPTLov(name,c_bpartner_id,item_code,item_id,document_no,c_order_id,supplier_name,warehouse_id,url)
{
  var search      = '#' + name + 'Search';
  var modal       = '#' + name + 'Modal';
  var table       = '#' + name + 'Table';
  var buttonSrc   = '#' + name + 'ButtonSrc';
  var buttonDel   = '#' + name + 'ButtonDel';

  function itemAjax() {
    var q = $(search).val();

    $(table).addClass('hidden');
    $(modal).find('.shade-screen').removeClass('hidden');
    $(modal).find('.form-search').addClass('hidden');

    $.ajax({
      url: url + '&c_order_id=' + c_order_id + '&c_bpartner_id=' + c_bpartner_id +'&type=' + name + '&item_id=' + item_id + '&q=' + q+ '&warehouse_id=' + warehouse_id
    })
    .done(function (data) 
    {
      $(table).html(data);
      pagination(name);
      $(search).focus();
      $(table).removeClass('hidden');
      $(modal).find('.shade-screen').addClass('hidden');
      $(modal).find('.form-search').removeClass('hidden');

        $(table).find('.btn-choose').on('click', chooseItem);
      });
  }

  function chooseItem() 
  {
    var id                  = $(this).data('id');
    var handover_document   = $(this).data('name');
    var qty                 = $(this).data('qty');
    var type                = $(this).data('type');

    if(type == 'handover')
    {
      $('#__summary_handover_id').val(id);
      $('#_supplier_name').text(supplier_name);
      $('#_po_supplier').text(document_no);
      $('#_item_code').text(item_code);

      $('#input_no_pt').val(handover_document);
      $('#input_qty_handover').val(qty);
      $('#input_no_pt').attr('readonly',true);
      $('#input_qty_handover').attr('readonly', true);
      

      $('#__supplier_name').val(supplier_name);
      $('#__c_bpartner_id').val(c_bpartner_id);
      $('#__item_code').val(item_code);
      $('#__document_no').val(document_no);
      $('#__c_order_id').val(c_order_id);
      $('#__item_id').val(item_id);
      $('#__is_move_order_1').val('1');

      $('#confirmationHandoverModal').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      });
    }else
    {
      $('#_supplier_name_move_order').text(supplier_name);
      $('#_po_supplier_move_order').text(document_no);
      $('#_item_code_move_order').text(item_code);

      $('#input_no_pt_move_order').val(handover_document);
      $('#input_qty_handover_move_order').val(qty);
      $('#input_no_pt_move_order').attr('readonly',true);
      $('#input_qty_handover_move_order').attr('readonly', true);

      $('#__summary_handover_id_move_order').val(id);
      $('#no_ptId_move_order').val(id);
      $('#__supplier_name_move_order').val(supplier_name);
      $('#__c_bpartner_id_move_order').val(c_bpartner_id);
      $('#__item_code_move_order').val(item_code);
      $('#__document_no_move_order').val(document_no);
      $('#__c_order_id_move_order').val(c_order_id);
      $('#__item_id_move_order').val(item_id);
      $('#__is_move_order_1').val('2');
      
      $('#confirmationMoveOrderModal').modal({
        backdrop: 'static',
        keyboard: false  // to prevent closing with Esc button (if you want this too)
      })
    }
    
  }

  function pagination() {
    $(modal).find('.pagination a').on('click', function (e) {
      var params = $(this).attr('href').split('?')[1];
      url = $(this).attr('href') + (params == undefined ? '?' : '');

      e.preventDefault();
      itemAjax();
    });
  }

  //$(search).val("");
  $(buttonSrc).unbind();
  $(search).unbind();

  $(buttonSrc).on('click', itemAjax);

  $(search).on('keypress', function (e) {
    if (e.keyCode == 13)
      itemAjax();
  });

  $(buttonDel).on('click', function () {
    $(item_id).val('');
    $(item_name).val('');

  });

  itemAjax();
}

function tambahItem() 
{
  var warehouse_id  = $('#select_warehouse').val();
  var barcode       = $('#barcode_value').val();
  var url_get_data  = $('#url_get_data').attr('href');

  var check_barcode = checkBarcode(barcode);

  if (check_barcode > 0) {
    $("#alert_warning").trigger("click", 'Barcode already scanned');
    $('#barcode_value').val('');
    setFocusToTextBox();
    return false;
  }

  $.ajax({
      type: "GET",
      url: url_get_data,
      data: {
        barcode       : barcode,
        warehouse_id  : warehouse_id
      },
      beforeSend: function () {
        $.blockUI({
          message: '<i class="icon-spinner4 spinner"></i>',
          overlayCSS: {
            backgroundColor: '#fff',
            opacity: 0.8,
            cursor: 'wait'
          },
          css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
          }
        });
      },
      complete: function () {
        $.unblockUI();
      },
      success: function (response) {
        var supplier_name = response.supplier_name;
        var c_bpartner_id = response.c_bpartner_id;
        var item_code     = response.item_code;
        var document_no   = response.document_no;
        var c_order_id    = response.c_order_id;
        var item_id       = response.item_id;
        var warehouse_id  = response.warehouse_id;

        var flag = checkHandoverExists(c_order_id, item_id, c_bpartner_id);

      if (flag == 0)
      {
          temporary_data_handover = response;
          $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
          ListPTLov('handover', c_bpartner_id, item_code,item_id,document_no,c_order_id, supplier_name,warehouse_id, '/fabric/material-out-non-cutting/document-picklist?');

          $('#documentConfirmationModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
          });

          $('#_supplier_name').text(supplier_name);
          $('#_po_supplier').text(document_no);
          $('#_item_code').text(item_code);

          $('#__supplier_name_move_order').val(supplier_name);
          $('#__c_bpartner_id_move_order').val(c_bpartner_id);
          $('#__item_code_move_order').val(item_code);
          $('#__c_order_id_move_order').val(c_order_id);
          $('#__document_no_move_order').val(document_no);
          $('#__item_id_move_order').val(item_id);

          $('#__supplier_name').val(supplier_name);
          $('#__c_bpartner_id').val(c_bpartner_id);
          $('#__item_code').val(item_code);
          $('#__document_no').val(document_no);
          $('#__c_order_id').val(c_order_id);
          $('#__item_id').val(item_id);
        }else 
        {
          selectedPlanning(response);
        }


      },
      error: function (response) {
        $.unblockUI();
        $('#barcode_value').val('');
        $('.barcode_value').focus();

        if (response['status'] == 500) $("#alert_error").trigger('click', 'PLEASE CONTACT ICT');
        else if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseText);

        setFocusToTextBox();
      }
    })
    .done(function () {
      render();
      renderAllocation();
      $('#barcode_value').val('');
      setFocusToTextBox();
    });
}

function changeTab(value)
{
  var c_bpartner_id = $('#__c_bpartner_id').val();
  var item_code     = $('#__item_code').val();
  var item_id       = $('#__item_id').val();
  var document_no   = $('#__document_no').val();
  var c_order_id    = $('#__c_order_id').val();
  var supplier_name = $('#__supplier_name').val();
  var warehouse_id  = $('#select_warehouse').val();

  ListPTLov(value, c_bpartner_id, item_code,item_id,document_no,c_order_id, supplier_name,warehouse_id, '/fabric/material-out-non-cutting/document-picklist?');
}

function setFocusToTextBox()
{
  $('#barcode_value').focus();
}

var selected;

function dismisModal() 
{
  var flag_1 = $('#__is_move_order_1').val();
  
  if(flag_1 == 1)$('#confirmationHandoverModal').modal('hide');
  else $('#confirmationMoveOrderModal').modal('hide');
  
  $(".modal-backdrop").remove();
  var url_remove_last_used  = $('#url_remove_last_used').attr('href');
  var c_order_id            = temporary_data_handover.c_order_id;
  var item_id               = temporary_data_handover.item_id;
  var warehouse_id          = $('#select_warehouse').val();

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: "put",
    url: url_remove_last_used,
    data: {
      c_order_id    : c_order_id,
      item_id       : item_id,
      warehouse_id  : warehouse_id
    }
  });

  temporary_data_handover = [];
  $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
  $('#barcode_value').focus();
}

function dismisModalLov() 
{
  var url_remove_last_used  = $('#url_remove_last_used').attr('href');
  var c_order_id            = temporary_data_handover.c_order_id;
  var item_id               = temporary_data_handover.item_id;
  var warehouse_id          = $('#select_warehouse').val();

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $.ajax({
    type: "put",
    url: url_remove_last_used,
    data: {
      c_order_id    : c_order_id,
      item_id       : item_id,
      warehouse_id  : warehouse_id
    }
  });

  temporary_data_handover = [];
  $('#documentConfirmationModal').modal('hide');
  $(".modal-backdrop").remove();
  $('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
  $('#barcode_value').focus();
}

$('#input_qty_handover').keypress(function (e) {

  if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
    return false;
  }
  return true;
});

function showInputPT()
{
  $('#documentConfirmationModal').modal('hide');
  $('#confirmationHandoverModal').modal({
    backdrop: 'static',
    keyboard: false // to prevent closing with Esc button (if you want this too)
  });

  $('#__summary_handover_id').val('');
  $('#__is_move_order_1').val('1');
  $('#input_no_pt').val('');
  $('#input_qty_handover').val('');
  $('#input_no_pt').attr('readonly', false);
  $('#input_qty_handover').attr('readonly', false);
}

function showInputDestination()
{
  $('#documentConfirmationModal').modal('hide');
  $('#confirmationMoveOrderModal').modal({
    backdrop: 'static',
    keyboard: false  // to prevent closing with Esc button (if you want this too)
  });

  $('#__summary_handover_id').val('');
  $('#__is_move_order_1').val('2');
  $('#input_no_pt').val('');
  $('#input_qty_handover').val('');
  $('#input_no_pt').attr('readonly', false);
  $('#input_qty_handover').attr('readonly', false);
}
