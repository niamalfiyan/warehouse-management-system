list_barcodes = JSON.parse($('#barcode_products').val());


$(function () 
{
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		
		var locator_in_accesories = $('#locator_in_accesoriesName').val();

		if (locator_in_accesories == null || locator_in_accesories == '')
		{
			$('#locator_in_accesories').val('');
			$('.locator_in_accesories').focus();
			$("#alert_error").trigger("click", 'Please select locator first. (Silahkan pilih locator terlebih dahulu)');
			return false;
		}

		
		bootbox.confirm("Are you sure want to save this data ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#barcode_value').val('');
						$('#form').trigger("reset");
						list_barcodes = [];
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						setFocusToTextBox();
					
						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please Contact ICT');
						
						
						if (response['status'] == 422)
							$("#alert_warning").trigger("click", response.responseJSON);

					}
				})
				.done(function (){
					$("#alert_success").trigger("click", 'Data successfully saved.');
					setFocusToTextBox();
					render();
				});
			}
		});
	});

	var _warehouse_id = $('#warehouse_id').val();
	locatorPicklist('locator_in_accesories', _warehouse_id,'/accessories/material-stock-opname/locator-picklist?');
});

$('#select_warehouse').on('change',function(){
	var _warehouse_id = $(this).val();
	$('#warehouse_id').val(_warehouse_id);

	locatorPicklist('locator_in_accesories',_warehouse_id, '/accessories/material-stock-opname/locator-picklist?');
});

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#accessories_material_stock_opname_table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody_accessories_material_stock_opname').html(html);
	setFocusToTextBox();
	bind();
}

function bind() 
{
	$('#barcode_value').on('change', tambahItem);
}

function getIndex() 
{
	for (idx in list_barcodes) 
	{
		list_barcodes[idx]['_id'] = idx;
		list_barcodes[idx]['no'] = parseInt(idx) + 1;
	}
}

function checkItem(barcode)
{
	var has_dash = barcode.indexOf("-");
	var _barcode = '';
	var is_exist = 0;

	if (has_dash != -1) 
	{
		_barcode = barcode.split("-")[0];
	} else {
		_barcode = barcode;
	}

	for (var i in list_barcodes)
	{
		var data = list_barcodes[i];
		if (data.barcode.trim() == _barcode.trim())
		{
			is_exist++;
		}
	}

	return is_exist;

}

function tambahItem()
{
	var warehouse_id 				= $('#select_warehouse').val();
	var url_sto_create_accessories 	= $('#url_sto_create_accessories').val();
	var barcode 					= $('#barcode_value').val();
	
	if (!barcode) 
	{
		$("#alert_warning").trigger("click", 'Please scan barcode first. (Silahkan scan barcode terlebih dahulu)');
		return;
	}

	var diff = checkItem(barcode);
	if (diff > 0) 
	{
		$("#barcode_value").val("");
		$(".barcode_value").focus();
		$("#alert_error").trigger("click", 'Barcode already scanned.');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_sto_create_accessories,
		data: {
			barcode			: barcode,
			warehouse_id	:warehouse_id
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			list_barcodes.push(response);
		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500)
				$("#alert_error").trigger('click', 'Please contact ICT');

			if (response['status'] == 422)
				$("#alert_error").trigger('click', response.responseJSON);

		}
	})
	.done(function () {
		render();
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').val('');
	$('#barcode_value').focus();
}

function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax()
	{
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q +'&warehouse_id=' + warehouse_id
		})
		.done(function (data) 
		{
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		
		$('#_to_destination_id').val(id);
		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}
