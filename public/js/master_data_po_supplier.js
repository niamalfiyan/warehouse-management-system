$(function () 
{
    var page = $('#page').val();
    lov('erpPoSupplierPicklist', '/master-data/po-supplier/erp-po-supplier-picklist?');

    if(page == 'index')
    {
        $('#master_data_po_supplier_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/po-supplier/data'
            },
            columns: [
                {data: 'supplier_code', name: 'supplier_code',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'type_stock', name: 'type_stock',searchable:true,visible:true,orderable:true},
                {data: 'periode_po', name: 'periode_po',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_po_supplier_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }else
    {
        list_po_buyer = JSON.parse($('#po-buyer').val());
        render();
    }
});

function lov(name, url) 
{
    var search      = '#' + name + 'Search';
    var modal       = '#' + name + 'Modal';
    var table       = '#' + name + 'Table';
    var buttonSrc   = '#' + name + 'ButtonSrc';
    
    function itemAjax() 
    {
        var q = $(search).val();
        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
            url: url + '&q=' + q
        })
        .done(function (data) {
            $(table).html(data);
            pagination(name);
            $(search).focus();
            $(search).val('');
            $(table).removeClass('hidden');
            $(modal).find('.shade-screen').addClass('hidden');
            $(modal).find('.form-search').removeClass('hidden');

            $(table).find('.btn-choose').on('click', chooseItem);
        });
    }

    function chooseItem() 
    {
        var c_order_id      = $(this).data('orderid');
        var c_bpartner_id    = $(this).data('bpartnerid');
       
        $.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "post",
			url: '/master-data/po-supplier/store',
			data : {
				c_order_id      : c_order_id,
				c_bpartner_id   : c_bpartner_id,
			},
			beforeSend: function () 
			{
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function () {
				$.unblockUI();
				$("#alert_error").trigger("click", 'Please contact ict.');
			},
		}).done(function () {
			$("#alert_success").trigger("click", 'Data successfully synchronized.');
			$('#master_data_po_supplier_table').DataTable().ajax.reload();
		});
    }

    function pagination() 
    {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    $(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) 
    {
        if (e.keyCode == 13)
            itemAjax();
    });

    itemAjax();
}