list_material_allocation_preparation = JSON.parse($('#material_allocation_preparations').val());

$(function()
{
	render();
	var _warehouse_id = $('#select_warehouse').val();
	poBuyerPickList('po_buyer_print_material_preparation',_warehouse_id,'/accessories/barcode/allocation/pobuyer-picklist?');
});


$('#select_warehouse').on('change',function(){
	var _warehouse_id = $(this).val();
	$('#warehouse_id').val(_warehouse_id);
	$('#_warehouse_id').val(_warehouse_id);
	$('#__warehouse_id').val(_warehouse_id);
	poBuyerPickList('po_buyer_print_material_preparation',_warehouse_id,'/accessories/barcode/allocation/pobuyer-picklist?');
});

$('#item_code_print_material_preparationName').click(function () {
	var po_buyer 		= $('#po_buyer_print_material_preparationId').val();
	var split 			= po_buyer.split(":");
	var _warehouse_id 	= $('#select_warehouse').val();

	$('#style').val(split[1]);

	if (!po_buyer) {
		$("#alert_warning").trigger("click", 'Please select item code first');
		return false;
	}

	itemCodeLov('item_code_print_material_preparation',_warehouse_id, '/accessories/barcode/allocation/item-picklist?', split[0], split[1]);
});

$('#item_code_print_material_preparationButtonLookup').click(function () {
	var po_buyer 		= $('#po_buyer_print_material_preparationId').val();
	var split 			= po_buyer.split(":");
	var _warehouse_id 	= $('#select_warehouse').val();

	$('#style').val(split[1]);

	if (!po_buyer) {
		$("#alert_warning").trigger("click", 'Please select item code first');
		return false;
	}

	itemCodeLov('item_code_print_material_preparation',_warehouse_id, '/accessories/barcode/allocation/item-picklist?', split[0], split[1]);
});

$('#btn_check_all').click(function () 
{
	var total = list_material_allocation_preparation.length;
	if (total == 0) 
	{
		$("#alert_warning").trigger("click", 'Data is empty');
		return false;
	} else 
	{
		for (id in list_material_allocation_preparation) 
		{
			list_material_allocation_preparation[id].selected = true;
		}
		render();
	}


});

$('#btn_uncheck_all').click(function () 
{
	var total = list_material_allocation_preparation.length;
	if (total == 0) 
	{
		$("#alert_warning").trigger("click", 'Data is empty');
		return false;
	} else 
	{
		for (id in list_material_allocation_preparation) 
		{
			list_material_allocation_preparation[id].selected = false;
		}
		render();
	}
});

function poBuyerPickList(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax(){
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q + '&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		
		$(item_id).val(id);
		$(item_name).val(name);
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});


	itemAjax();
}


function itemCodeLov(name, warehouse_id,url, po_buyer,style) 
{
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';
	var po_buyer = po_buyer;
	var style = style;

	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q + '&po_buyer=' + po_buyer + '&style=' + style+ '&warehouse_id=' + warehouse_id
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$(item_id).trigger("change", id);
		$('#searchcriteria').trigger('click');
	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

function render()
{
	getIndex();
	$('#material_allocation_preparations').val(JSON.stringify(list_material_allocation_preparation));
	var tmpl = $('#allocation-preparation-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_material_allocation_preparation };
	var html = Mustache.render(tmpl, data);
	$('#tbody-allocation-preparation').html(html);
	bind();
}

function bind() 
{
	$('.checkbox_item').on('click', checkedItem);
	$('.btn-remove').on('click',removeList);
}

function removeList()
{
	var id 		= $(this).data('id');
	list_material_allocation_preparation[id].splice(id,1);
	render();
}

function checkedItem() 
{
	var id = $(this).data('id');

	if ($('#check_'+ id).is(':checked')) var checked = true;
	else var checked = false;

	list_material_allocation_preparation[id].selected = checked;

	render();
}

function getIndex()
{
	for (id in list_material_allocation_preparation)
	{
		list_material_allocation_preparation[id]['_id'] = id;
		list_material_allocation_preparation[id]['no'] = parseInt(id)+1;
	}
}

function checkItem(po_buyer, item_code, style)
{
	for (var id in list_material_allocation_preparation) 
	{
		var data = list_material_allocation_preparation[id];
		if ((po_buyer != '' 
		&& po_buyer != null) 
		&& (item_code == null || item_code == '') 
		&& (style == null || style == ''))
		{
			if (data.po_buyer == po_buyer) return false;
		}

		if ((item_code != null || item_code != '') 
		&& (po_buyer == '' && po_buyer == null)
		&& (style == null || style == '') ) 
		{
			if (data.item_code == item_code) return false;
		}

		if ((style != null || style != '') 
		&& (item_code == '' 
		&& item_code == null) 
		&& (po_buyer == null || po_buyer == '') ) 
		{
			if (data.item_code == item_code) return false;
		}

		if ((item_code != null || item_code != '') 
		&& (po_buyer != '' 
		&& po_buyer != null) 
		&& (style != '' 
		&& style != null)) 
		{
			if (data.item_code == item_code && data.po_buyer == po_buyer && data.style == style) return false;
		}
	
		
	}

	return true;
}

$('#searchcriteria').click(function ()
{
	var url_material_allocation_list 	= $('#url_material_allocation_list').attr('href');
	var po_buyer 						= $('#po_buyer_print_material_preparationId').val().split(':')[0];
	var item_code 						= $('#item_code_print_material_preparationId').val();
	var style 							= $('#po_buyer_print_material_preparationId').val().split(':')[1];
	var warehouse_id 					= $('#select_warehouse').val();

	var diff = checkItem(po_buyer, item_code,style);

	if (!diff)
	{
		$("#alert_info").trigger("click", 'Data already on list');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_material_allocation_list,
		data: {
			po_buyer			: po_buyer,
			item_code			: item_code,
			style				: style,
			warehouse_id		: warehouse_id,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			for (var i in response) 
			{
				var input = response[i];
				list_material_allocation_preparation.push(input);
			}

			$('#po_buyer_print_material_preparationId').val('');
			$('#po_buyer_print_material_preparationName').val('');
			$('#item_code_print_material_preparationId').val('');
			$('#item_code_print_material_preparationName').val('');
		},	
		error: function (response) {
			$.unblockUI();
			$('#po_buyer_print_material_preparationId').val('');
			$('#po_buyer_print_material_preparationName').val('');
			$('#item_code_print_material_preparationId').val('');
			$('#item_code_print_material_preparationName').val('');

			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT');

			if (response['status'] == 400) {
				$('#po_buyer_error').addClass('has-error');
				$('#item_code_error').addClass('has-error');
				$('#document_no_error').addClass('has-error');

				$('#po_buyer_danger').text('Harap salah satu field ini diisi');
				$('#item_code_danger').text('Harap salah satu field ini diisi');
				$('#document_no_danger').text('Harap salah satu field ini diisi');
			}

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response['responseJSON']);

		}
	})
	.done(function (response) {
		$('#po_buyer').val('');
		$('#item_code').val('');
		$('#document_no').val('');

		$('#po_buyer_error').removeClass('has-error');
		$('#item_code_error').removeClass('has-error');
		$('#document_no_error').removeClass('has-error');

		$('#po_buyer_danger').text('* Optional');
		$('#item_code_danger').text('* Optional');
		$('#document_no_danger').text('* Optional');
		render();
	});
});

$('#form').submit(function (event) 
{
	event.preventDefault();
	
	/*var duplicate = checkDuplicateData();
	if (duplicate.length > 0) {
		$('#alert-duplicate').removeClass('hidden');
		$('#alert-duplicate-text').text('');
		for (i = 0; i < duplicate.length; i++) {
			var text =
				'<hr>PO SUPPLIER : ' + duplicate[i]['document_no'] + '<br>' +
				'PO BUYER : ' + duplicate[i]['po_buyer'] + '<br>' +
				'ITEM : ' + duplicate[i]['item_code'] + '<br>' +
				'QTY : ' + duplicate[i]['qty_conversion'] + '<br>' +
				'NO : ' + duplicate[i]['no'] + '<br>'
			$('#alert-duplicate-text').append(text);
		}

		return false;
	}*/

	bootbox.confirm("Are you sure want to print this data ?.", function (result) {
		if (result) {
			$('#alert-duplicate').removeClass('hidden');
			$('#alert-duplicate').addClass('hidden');
			$.ajax({
				type: "POST",
				cache:false,
				url: $('#form').attr('action'),
				data:new FormData($("#form")[0]),
				contentType: false,
				processData:false,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function (response) {
					if (response['flag'] == 1)
						$("#alert_success").trigger("click", 'Data successfully printed');
					else if (response['flag'] == 0)
						$("#alert_error").trigger("click", response['message']);


				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please contact ICT');

					} else {
						$("#alert_error").trigger("click", response)
					}
				}
			})
				.done(function (response) {
					list_material_allocation_preparation = [];
					render();

					$('#list_barcodes').val(JSON.stringify(response));
					$('#getBarcode').submit();
					$('#list_barcodes').val('');

					/*var arrStr = encodeURIComponent(JSON.stringify(response['data_print']));
					var win = window.open('/barcode/accessories/print-barcode-allocation/barcode?id=' + arrStr);

					if (win)
						win.focus();*/
				});
			
		}
	});
});

function checkDuplicateData()
{
	var list		= list_material_allocation_preparation;
	var duplicate	= [];

	for(i=0;i<list.length;i++)
	{
		for (j = 0; j < list.length - 1; j++) 
		{
			if (list[j]['selected'] == true && list[j + 1]['selected'] == true){
				duplicate = checkDuplicate(list[j], list[j + 1], duplicate);
			}
			
		}
		
	}
	return duplicate;
}

function checkDuplicate(item1,item2,duplicate)
{
	if (item1.id == (item2.id)) 
	{
		duplicate = addDataToDuplicateList(item1, item2, duplicate);
	}
	return duplicate;
	
}

function addDataToDuplicateList(item1,item2,duplicate)
{
	if(duplicate.length==0)
	{
		var temp = 
		{
			'id'			:item2.id,
			'po_buyer'		:item2.po_buyer,
			'document_no'	:item2.document_no,
			'item_code'		:item2.item_code,
			'style'			:item2.style,
			'qty_conversion':item2.qty_conversion,
			'no'			:item1.no+','+item2.no
		}
		duplicate[0]=temp;
	}else{
		for(i=0;i<duplicate.length;i++)
		{
			duplicate[i]['no'] = duplicate[i]['no']+','+item2['no'];
		}
	}
	return duplicate;
}

$('#upload_button').on('click', function () {
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () {
	//$('#upload_file_allocation').submit();
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'upload successfully, please check data before print.');
			list_material_allocation_preparation = [];
			for (var i in response) 
			{
				var input = response[i];
				list_material_allocation_preparation.push(input);
			}
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})