list_material_arrival = JSON.parse($('#material_arrival').val());

$(function () 
{
    setFocusToTextBox();
    render();

    $('#form').submit(function (event) 
    {
        event.preventDefault();

        bootbox.confirm("Are you sure want to save this data ?.", function (result) 
        {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () 
                    {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () 
                    {
                        $.unblockUI();
                    },
                    success: function () 
                    {
                        $("#alert_success").trigger("click", 'Material successfully received');
                    },
                    error: function (response) 
                    {
                        $.unblockUI();
                        $('#barcode_value').val('');
                        $('.barcode_value').focus();

                        if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                        else if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);

                    }
                })
                .done(function (response) 
                {
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();
                    list_material_arrival = [];
                    render();
                });
            }
        });
    });
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() 
{
    getIndex();
    $('#material_arrival').val(JSON.stringify(list_material_arrival));
    var tmpl = $('#material_arrival_accessories_table').html();
    Mustache.parse(tmpl);
    var data = { item: list_material_arrival };
    var html = Mustache.render(tmpl, data);
    $('#tbody_material_arrival').html(html);
    $('.barcode_value').focus();
    is_exists();
    isNeedPrepare();
    bind();
}

function bind() 
{
    $('#barcode_value').on('change', tambahItem);
}

function is_exists() 
{
    for (var i in list_material_arrival) 
    {
        var barcode = list_material_arrival[i];
        if (barcode.is_exists == 1) $("#alert_info").trigger("click", 'barcode sudah diterima sebelumnya.');

    }
}

function isNeedPrepare() 
{
    for (var i in list_material_arrival) 
    {
        var data = list_material_arrival[i];
        if (data.is_backlog) 
        {
            $('#panel_' + i).css('background-color', '#ffa3c8');
            $('#panel_' + i).css('color', 'white');
        }

        if (data.is_other_buyer_checkout) 
        {
            $('#panel_' + i).css('background-color', '#8c8c8c');
            $('#panel_' + i).css('color', 'white');
        }

        if (data.prepared_status == 'NEED PREPARED' || data.prepared_status == 'NEED PREPARED ALLOCATION' || data.prepared_status == 'NEED PREPARED TOP & BOTTOM') 
        {
            $('#panel_' + i).css('background-color', '#ffc694');
        }

        if (data.is_moq) 
        {
            $('#panel_' + i).css('background-color', '#96d5ff');
        }

        if (data.is_po_cancel) 
        {
            $('#panel_' + i).css('background-color', '#fab1b1');
        }
    }
}

function getIndex() 
{
    for (idx in list_material_arrival) 
    {
        list_material_arrival[idx]['_idx'] = idx;
        list_material_arrival[idx]['nox'] = parseInt(idx) + 1;

        for (var id in list_material_arrival[idx]['details']) 
        {
            list_material_arrival[idx]['details'][id]['_id'] = idx + '_' + id;
            list_material_arrival[idx]['details'][id]['no'] = parseInt(id) + 1;
        }
    }
}

function checkItem(barcode_val) 
{
    var has_dash        = barcode_val.indexOf("-");
    var po_detail_id    = '';
    var num_of_row      = 0;
    var is_exist        = 0;

    if (has_dash != -1) 
    {
        po_detail_id    = barcode_val.split("-")[0];
        po_detail_id    = po_detail_id.toUpperCase();
        num_of_row      = barcode_val.split("-")[1];
    }else 
    {
    po_detail_id    = barcode_val;
    po_detail_id    = po_detail_id.toUpperCase();
}

for (var i in list_material_arrival) 
{
var barcode = list_material_arrival[i];

if (barcode.po_detail_id == po_detail_id) 
{
    is_exist            = 1;
    var qty_carton      = parseInt(barcode.qty_carton);
    var counter         = parseInt(barcode.counter);
    var flag_is_exists  = false;

    if (barcode.po_detail_id == po_detail_id) 
    {
        if (counter >= qty_carton)
            return 4;

        if (num_of_row != 0) 
        {
            if (num_of_row > qty_carton) 
            {
                return 5;
            }
        }


        for (var id in barcode.details) 
        {
            var detail = barcode.details[id];

            if (barcode_val == detail.barcode_supplier)
                flag_is_exists = true;


            if (counter <= parseInt(barcode.qty_carton))
            {
                var input = {
                    'material_arrival_id'   : barcode.id,
                    'po_buyer'              : barcode.po_buyer,
                    'barcode_supplier'      : barcode_val,
                    'barcode_supplier'      : barcode_val,
                    'qty_delivered'         : barcode.qty_delivered,
                    'qty_upload'            : barcode.qty_upload,
                };
            } else 
            {
                return 3;
            }
        }

        if (flag_is_exists == false) 
        {
            barcode.counter     = counter + 1;
            barcode.is_exists   = 0;
            barcode.details.push(input);
        } else 
        {
            return 1;
        }
        render();
        return 2;
    }
}
}


if (is_exist == 0);
return 6;
}

function tambahItem() 
{
    var warehouse_id                                = $('#select_warehouse').val();
    var barcode_val                                 = $('#barcode_value').val();
    var url_material_arrival_accessories_create     = $('#url_material_arrival_accessories_create').val();
    var diff                                        = checkItem(barcode_val);

    if (diff == 1) 
    {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        $("#alert_error").trigger("click", 'Material already scanned.');
        return;
    } else if (diff == 2) 
    {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        return;
    } else if (diff == 3) 
    {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        $("#alert_error").trigger("click", 'Total carton not same with receive.');
        return;
    } else if (diff == 4) 
    {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        $("#alert_error").trigger("click", 'All carton already received.');
        return;
    } else if (diff == 5) 
    {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        $("#alert_warning").trigger("click", 'kok bisa barcode melebihi jumlah karton yang ada disistem. salah barcode tuh.');
        return;
    } else 
    {
        
        if(list_material_arrival.length > 49)
        {
            $("#barcode_value").val("");
            $(".barcode_value").focus();
            $("#alert_warning").trigger("click", 'You already reach a limit, please save it first.');
            return false;
        }
        
        $.ajax({
            type: "GET",
            url: url_material_arrival_accessories_create,
            data:
            {
                po_detail_id: barcode_val,
                warehouse_id: warehouse_id
            },
            beforeSend: function () 
            {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });

            },
            complete: function () 
            {
                $.unblockUI();
            },
            success: function (response) 
            {
                if (response.prepared_status == 'NEED PREPARED' 
                || response.prepared_status == 'NEED PREPARED ALLOCATION' 
                || response.prepared_status == 'NEED PREPARED TOP & BOTTOM')
                {
                    if (response.po_buyer) var msg = response.item_code + ' Please print barcode preparation';
                    else var msg = response.item_code + ' Please print barcode allocation';

                    if (response.flag_moq) msg = msg + ',and ' + response.flag_moq;
                    if (response.is_backlog)msg = 'PO Buyer Backlog';

                    $("#alert_info").trigger("click", msg );
                }else if (response.prepared_status != 'NEED PREPARED' && response.flag_moq)
                {
                    $("#alert_info").trigger("click", response.flag_moq);
                }else if (response.prepared_status != 'NEED PREPARED' && response.is_backlog)
                {
                    $("#alert_info").trigger("click", "PO Buyer Backlog");
                }else if (response.prepared_status != 'NEED PREPARED' && response.is_other_buyer_checkout) 
                {
                    msg = 'beberapa item untuk po buyer ' + response.po_buyer + ' sudah supplai, silahkan di supplai segera';
                    $("#alert_info").trigger("click", msg);
                }else if (response.is_po_cancel) 
                {
                    if(response.is_sotf)
                    {
                        msg = 'material ini di booking mm, silahkan di berikan ke free stock segera';
                        $("#alert_info").trigger("click", msg);
                    }else
                    {
                        msg = 'po buyer ' + response.po_buyer + ' di cancel, silahkan di berikan ke free stock segera';
                        $("#alert_info").trigger("click", msg);
                    }
                }


                list_material_arrival.push(response);
            },
            error: function (response) 
            {
                $.unblockUI();
                $('#barcode_value').val('');
                $('.barcode_value').focus();

                if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');

                if (response['status'] == 422) $("#alert_info").trigger("click", response.responseJSON);
            }
        })
        .done(function (response) 
        {
            $('#barcode_value').val('');
            $('.barcode_value').focus();
            render();
        });;
    }
}

function setFocusToTextBox() 
{
    $('#barcode_value').focus();
}

