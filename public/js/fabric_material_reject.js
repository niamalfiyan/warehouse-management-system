list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	$('.barcode_value').focus();
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		bootbox.confirm("Are you sure want to save material to locator reject ?.", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () 
					{
						$('#barcode_value').val('');
						$('#locator_in_fabricName').val('');
						$('#locator_in_fabricId').val('');
						$('.barcode_value').focus();
						
						$('#form').trigger("reset");
						list_barcodes = [];
						render();

					},
					error: function (response) 
					{
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();
					
						if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
						if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

						if (response['status'] == 400){
							for (i in response['responseJSON']['errors']) 
							{
								$('#' + i + '_error').addClass('has-error');
								$('#' + i + '_danger').text(response['responseJSON']['errors'][i]);
							}
						}
					}
				})
				.done(function (response)
				{
					$("#alert_success").trigger("click", 'Data successfully saved.');
				});
			}
		});
	});
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes));
	var tmpl = $('#fabric_material_reject_table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody_fabric_material_reject').html(html);
	$('.barcode_value').focus();

	bind();
}

function getIndex() 
{
	for (idx in list_barcodes) {
		list_barcodes[idx]['_id'] = idx;
		list_barcodes[idx]['no'] = parseInt(idx) + 1;
	}
}

function checkItem(barcode, index) 
{
	for (var i in list_barcodes) {
		var data = list_barcodes[i];

		if (i == index)
			continue;
		
		if (data.barcode == barcode) 
		{
			return false;
		}

	}

	return true;
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
}


function deleteItem()
{
	var i = parseInt($(this).data('id'), 10);
	list_barcodes.splice(i, 1);
	render();
}


function tambahItem()
{
	var warehouse_id 						= $('#select_warehouse').val();
	var barcode 							= $('#barcode_value').val();
	var url_fabric_material_reject_create	= $('#url_fabric_material_reject_create').val();
	var diff_dup_item						= checkItem(barcode);

	if (!diff_dup_item) 
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();
		$("#alert_warning").trigger("click", 'Barcode already scanned.');
		return;
	}

	$.ajax({
		type: "GET",
		url: url_fabric_material_reject_create,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id
		},
		beforeSend: function () 
		{
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			list_barcodes.push(response);
		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();
			
			if (response['status'] == 500) $("#alert_error").trigger('click','Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);
		}
	})
	.done(function ()
	{
		$('#barcode_value').val('');
		$('.barcode_value').focus();
		render();
	});
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}