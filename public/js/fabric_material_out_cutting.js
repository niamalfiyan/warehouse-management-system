list_barcodes = JSON.parse($('#barcode_products').val());

$(function () 
{
	setFocusToTextBox();
	render();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		if (list_barcodes.length == 0) 
		{
			$('.barcode_value').focus();
			$("#alert_warning").trigger("click", 'Please scan barcode first.');
			return false;
		}

		

		var check_actual_lot = checkActualLot();
		if (check_actual_lot > 0) 
		{
			$("#alert_warning").trigger("click", 'Some material does\'t have lot. Please remove from list.');
			return false;
		}

		bootbox.confirm("Are you sure want to supply this material ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#barcode_value').val('');
						$('.barcode_value').focus();
						$('#to_destinationName').val('');
						$('#po_buyer').val('');

						$('#form').trigger("reset");
						list_data_saving = [];
						list_barcodes = [];
						render();
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						$('.barcode_value').focus();

						if (response['status'] == 500)
							$("#alert_error").trigger("click", 'Please contact ICT');


						if (response['status'] == 422 || response['status'] == 400)
							$("#alert_warning").trigger("click", response.responseJSON);
					}
				})
				.done(function () 
				{
					$("#alert_success").trigger("click", 'Data successfully supplied');
					render();
				});
			}
		});

	});
});

$('#select_warehouse').on('change',function()
{
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);
	$('#upload_warehouse_id').val(warehouse_id);
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	//$('#upload_file_allocation').submit();
	$.ajax({
		type: "post",
		url: $('#upload_file_form').attr('action'),
		data: new FormData(document.getElementById("upload_file_form")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			$("#alert_info").trigger("click", 'Upload successfully, please check again before save.');
			
			for(idx in response)
			{
				var input = response[idx];
				list_barcodes.push(input);
			}
			
			render();
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_form').trigger('reset');
			
			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT.');
			else if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () 
	{
		$('#upload_file_form').trigger('reset');
		render();
	});

})

function render() 
{
	getIndex();
	$('#barcode_products').val(JSON.stringify(list_barcodes.sort()));
	var tmpl = $('#out-fabric-table').html();
	Mustache.parse(tmpl);
	var data = { list: list_barcodes };
	var html = Mustache.render(tmpl, data);
	$('#tbody-material-out-fabric').html(html);
	$('.barcode_value').focus();
	bind();
}

function getIndex() 
{
	for (id in list_barcodes) 
	{
		list_barcodes[id]['_id'] = id;
		list_barcodes[id]['no'] = parseInt(id) + 1;

		var detail = list_barcodes[id]['details'];
		for (idx in detail) {
			detail[idx]['_idx'] = idx;
			detail[idx]['no'] = parseInt(idx) + 1;
		}
	}
}

function bind() 
{
	$('.input-actual-lot-edit').on('change', changeLot);
	$('.btn-delete-item').on('click', deleteItem);
	$('#barcode_value').on('change', tambahItem);
}

function checkItem(barcode) 
{
	var flag = 0;
	for (var i in list_barcodes) {
		var datas = list_barcodes[i];
		if (datas.barcode.toUpperCase() == barcode.toUpperCase())
			flag++;
		
	}
	return flag;
}

function deleteItem() 
{
	var i = $(this).data('id');
	list_barcodes.splice(i, 1);
	render();
}

function changeLot()
{
	var i = $(this).data('id');
	var actual_lot = $('#actualLotInput_'+i).val();

	if (!actual_lot){
		$("#alert_warning").trigger('click', 'Actual lot can\'t be empty.');
		return false;
	}

	list_barcodes[i].actual_lot 				= actual_lot.toUpperCase();
	list_barcodes[i].is_already_have_actual_lot = false;
		
	render();
}

function tambahItem()
{
	var warehouse_id 	= $('#select_warehouse').val();
	var barcode 		= $('#barcode_value').val();
	var url_get_data 	= $('#url_get_data').attr('href');

	var check_item = checkItem(barcode);
	if (check_item > 0) 
	{
		$("#alert_info").trigger('click', 'Barcode already scanned.');
		$('#barcode_value').val('');
		$('#barcode_value').focus();
		return false;
	}

	$.ajax({
		type: "GET",
		url: url_get_data,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) 
		{
			list_barcodes.push(response);

		},
		error: function (response) 
		{
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response['status'] == 500) $("#alert_error").trigger('click', 'Please contact ICT');
			if (response['status'] == 422) $("#alert_warning").trigger('click', response.responseJSON);

		}
	})
	.done(function () 
	{
		render();
	});
}

function setFocusToTextBox()
{
	$('#barcode_value').focus();
}

function checkActualLot()
{
	var flag = 0;
	for (var i in list_barcodes) {
		var datas = list_barcodes[i];
		if (datas.actual_lot == null || datas.actual_lot == 0)
			flag++;

	}
	return flag;
}