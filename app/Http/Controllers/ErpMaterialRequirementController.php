<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\UomConversion;
use App\Models\PurchaseItem;
use App\Models\ReroutePoBuyer;
use App\Models\AllocationItem;
use App\Models\PoSupplier;
use App\Models\MappingStocks;
use App\Models\MaterialStock;
use App\Models\AutoAllocation;
use App\Models\SummaryStockFabric;
use App\Models\HistoryAutoAllocations;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialPlanningFabric;
use App\Models\MaterialRequirementPerPart;
use App\Models\DetailMaterialRequirement;
use App\Models\MaterialReadyPreparation;
use App\Models\MaterialPlanningFabricPiping;

use App\Http\Controllers\FabricMaterialPlanningController as PlanningFabric;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\MasterDataAutoAllocationController;

class ErpMaterialRequirementController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder)
    {
        //return view('errors.503');
        if ($request->ajax()) 
        { 
            if($request->has('_po_buyer'))
            {
                $_po_buyer = $request->_po_buyer;
                $material_requirements = MaterialRequirement::whereIn('po_buyer',$_po_buyer)
                ->orderby('statistical_date','desc');
            }else
            {
                $po_buyer = $request->po_buyer;
                $material_requirements = MaterialRequirement::orderby('statistical_date','desc')
                ->where('po_buyer',$po_buyer);
            }
            
            return DataTables::of($material_requirements)
            ->editColumn('lc_date',function ($material_requirements)
            {
                if($material_requirements->lc_date) return Carbon::createFromFormat('Y-m-d H:i:s', $material_requirements->lc_date)->format('d/M/Y');
                else return null;

            })
            ->editColumn('promise_date',function ($material_requirements)
            {
                if($material_requirements->promise_date) return Carbon::createFromFormat('Y-m-d H:i:s', $material_requirements->promise_date)->format('d/M/Y');
                else return null;

            })
            ->editColumn('statistical_date',function ($material_requirements)
            {
                if($material_requirements->statistical_date) return Carbon::createFromFormat('Y-m-d H:i:s', $material_requirements->statistical_date)->format('d/M/Y');
                else return null;
            })
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'lc_date', 'name'=>'lc_date', 'title'=>'LC DATE'])
        ->addColumn(['data'=>'promise_date', 'name'=>'statistical_date', 'title'=>'PROMISE DATE'])
        ->addColumn(['data'=>'statistical_date', 'name'=>'statistical_date', 'title'=>'STATISTICAL DATE'])
        ->addColumn(['data'=>'season', 'name'=>'season', 'title'=>'SEASON'])
        ->addColumn(['data'=>'po_buyer', 'name'=>'po_buyer', 'title'=>'PO BUYER'])
        ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'ITEM CODE'])
        ->addColumn(['data'=>'category', 'name'=>'category', 'title'=>'CATEGORY'])
        ->addColumn(['data'=>'style', 'name'=>'style', 'title'=>'STYLE'])
        ->addColumn(['data'=>'article_no', 'name'=>'article_no', 'title'=>'ARTICLE NO'])
        ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
        ->addColumn(['data'=>'warehouse_name', 'name'=>'warehouse_name', 'title'=>'WAREHOUSE PACKING'])
        ->addColumn(['data'=>'qty_required', 'name'=>'qty_required', 'title'=>'QTY','searchable'=>false]);
        
        return view('erp_material_requirement.index',compact('html'));
    }

    static function getPobuyer($po_buyer, $item_code)
    {
        $adidas         = PoBuyer::where('po_buyer', $po_buyer)->where('brand', 'ADIDAS')->exists();
        if($adidas)
        {
            $po_buyer = str_replace('-S', '',$po_buyer);
        } 
        $app_env = Config::get('app.env');
        
        //if($app_env == 'dev') $data = DB::connection('dev_erp');
        //else if($app_env == 'live') $data = DB::connection('erp');
        //else 
        
        $data = DB::connection('erp');

        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();
        $data = $data->table('wms_lc_buyers')
        ->select('kst_lcdate as lc_date',
                'poreference as po_buyer',
                'kst_season as season'
                ,'kst_statisticaldate as statistical_date'
                ,'kst_statisticaldate2 as statistical_date_2'
                ,'ordertype as order_type'
                ,'brand as brand'
                ,'is_japan_china'
                ,'country'
                ,'issubcont'
                ,'datepromised as promise_date'
                ,'so_id')
        ->where('poreference',$po_buyer)
        ->first();
        
        $_po_buyer = PoBuyer::select('po_buyer')
        ->where('po_buyer',$po_buyer)
        ->groupby('po_buyer')
        ->exists();
        
        if($data)
        {
            if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
            else $brand = $data->brand;

            if($data->order_type == 'MTF')
            {
                $type_stock                 = 'MTFC';
                $type_stock_erp_code        = '4';
            }else if($data->order_type == 'SLT')
            {
                $type_stock                 = 'SLT';
                $type_stock_erp_code        = '1';
            }else if($data->order_type == 'SR')
            {
                $type_stock                 = 'SR';
                $type_stock_erp_code        = '3';
            }else if($data->order_type == 'NB')
            {
                $type_stock                 = 'NB';
                $type_stock_erp_code        = '5';
            }else
            {
                if($brand == 'ADIDAS')
                {
                    $type_stock             = 'REGULER';
                    $type_stock_erp_code    = '2';
                }else
                {
                    $type_stock             = null;
                    $type_stock_erp_code    = null;
                }
            }

            if($data->statistical_date_2) $statistical_date = $data->statistical_date_2;
            else  $statistical_date = $data->statistical_date;
        }else
        {
            $statistical_date       = null;
            $type_stock             = null;
            $type_stock_erp_code    = null;
        }

        if(!$_po_buyer)
        {
            if($data)
            {
                if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $data->brand;

                if($data->order_type == 'MTF')
                {
                    $type_stock                 = 'MTFC';
                    $type_stock_erp_code        = '4';
                }else if($data->order_type == 'SLT')
                {
                    $type_stock                 = 'SLT';
                    $type_stock_erp_code        = '1';
                }else if($data->order_type == 'SR')
                {
                    $type_stock                 = 'SR';
                    $type_stock_erp_code        = '3';
                }else if($data->order_type == 'NB')
                {
                    $type_stock                 = 'NB';
                    $type_stock_erp_code        = '5';
                }else
                {
                    if($brand == 'ADIDAS')
                    {
                        $type_stock             = 'REGULER';
                        $type_stock_erp_code    = '2';
                    }else
                    {
                        $type_stock             = null;
                        $type_stock_erp_code    = null;
                    }
                }
                
                PoBuyer::FirstorCreate([
                    'po_buyer'                      => $data->po_buyer,
                    'lc_date'                       => $data->lc_date,
                    'podd'                          => $data->statistical_date,
                    'statistical_date'              => $statistical_date,
                    'promise_date'                  => $data->promise_date,
                    'season'                        => $data->season,
                    'brand'                         => $brand,
                    'type_stock'                    => $type_stock,
                    'type_stock_erp_code'           => $type_stock_erp_code,
                    'is_ordering_for_japan_china'   => $data->is_japan_china,
                    'destination'                   => strtoupper($data->country),
                    'is_po_subcont'                 => ($data->issubcont == 'N' ? false : true),
                    'so_id'                         => $data->so_id,
                ]);
                
                // if($statistical_date) 
                //     AutoAllocation::where('po_buyer',$data->po_buyer)->update([
                //         'promise_date'                  => $statistical_date
                //         , 'is_ordering_for_japan_china' => $data->is_japan_china
                //     ]);
                // else 
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update([
                        'promise_date'                  => $data->promise_date
                        ,'is_ordering_for_japan_china'  => $data->is_japan_china
                    ]);
            }
        }else
        {
            if($data)
            {
                if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $data->brand;

                if($data->order_type == 'MTF')
                {
                    $type_stock                 = 'MTFC';
                    $type_stock_erp_code        = '4';
                }else if($data->order_type == 'SLT')
                {
                    $type_stock                 = 'SLT';
                    $type_stock_erp_code        = '1';
                }else if($data->order_type == 'SR')
                {
                    $type_stock                 = 'SR';
                    $type_stock_erp_code        = '3';
                }else if($data->order_type == 'NB')
                {
                    $type_stock                 = 'NB';
                    $type_stock_erp_code        = '5';
                }else
                {
                    if($brand == 'ADIDAS')
                    {
                        $type_stock             = 'REGULER';
                        $type_stock_erp_code    = '2';
                    }else
                    {
                        $type_stock             = null;
                        $type_stock_erp_code    = null;
                    }
                }
                
                if(!$is_exist)
                {
                    PoBuyer::where('po_buyer',$data->po_buyer)
                    ->update([
                        'promise_date'                  => $data->promise_date
                        ,'podd'                         => $data->statistical_date
                        ,'statistical_date'             => $statistical_date
                        ,'lc_date'                      => $data->lc_date
                        ,'season'                       => $data->season
                        ,'brand'                        => $brand
                        ,'type_stock'                   => $type_stock
                        ,'type_stock_erp_code'          => $type_stock_erp_code,
                        'is_ordering_for_japan_china'   => $data->is_japan_china,
                        'destination'                   => strtoupper($data->country),
                        'is_po_subcont'                 => ($data->issubcont == 'N' ? false : true)
                    ]);
                }
                

                // if($statistical_date) 
                //     AutoAllocation::where('po_buyer',$data->po_buyer)->update([
                //         'promise_date'                  => $statistical_date
                //         ,'is_ordering_for_japan_china'  => $data->is_japan_china
                //     ]);
                // else 
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update([
                        'promise_date'                  => $data->promise_date
                        , 'is_ordering_for_japan_china' => $data->is_japan_china
                    ]);
                
            }
        }

        if(!$is_exist)
        {
            $material_requirements = DB::connection('erp')->select(db::raw("SELECT style,
            item_id,
            item_code,
            item_desc,
            uom,
            po_buyer,
            job_order,
            qty_required,
            article_no,
            is_piping,
            season,
            garment_size,
            part_no,
            warehouse_id,
            warehouse_name,
            city_name,
            component,
            category,
            supp_code,
            supp_name,
            qty_ordered_garment FROM get_wms_material_requirement_detail_new(
                '".$po_buyer."') where item_code like '%".$item_code."%';"
                ));

            if(count($material_requirements) > 0)
            {
                DetailMaterialRequirement::where('po_buyer',$po_buyer)
                ->where('item_code','like', '%' . $item_code . '%')
                ->delete();
                
                foreach ($material_requirements as $key2 => $material_requirement) 
                {
                    DetailMaterialRequirement::FirstOrCreate([
                        'season'              => $material_requirement->season,
                        'po_buyer'            => $material_requirement->po_buyer,
                        'article_no'          => $material_requirement->article_no,
                        'item_id'             => $material_requirement->item_id,
                        'item_code'           => $material_requirement->item_code,
                        'item_desc'           => $material_requirement->item_desc,
                        'uom'                 => $material_requirement->uom,
                        'style'               => $material_requirement->style,
                        'category'            => $material_requirement->category,
                        'job_order'           => $material_requirement->job_order,
                        'statistical_date'    => $statistical_date,
                        'lc_date'             => ($data)? $data->lc_date : null,
                        'promise_date'        => ($data)? $data->promise_date : null,
                        'is_piping'           => $material_requirement->is_piping,
                        'garment_size'        => $material_requirement->garment_size,
                        'part_no'             => $material_requirement->part_no,
                        'qty_required'        => sprintf('%0.8f',$material_requirement->qty_required),
                        'warehouse_id'        => $material_requirement->warehouse_id,
                        'warehouse_name'      => $material_requirement->warehouse_name,
                        'component'           => $material_requirement->component,
                        'destination_name'    => $material_requirement->city_name,
                        'supplier_code'       => $material_requirement->supp_code,
                        'supplier_name'       => $material_requirement->supp_name,
                        'qty_ordered_garment' => $material_requirement->qty_ordered_garment,
                    ]);
                }
            }
            else
            {
                DetailMaterialRequirement::where('po_buyer',$po_buyer)->delete();
            }
            
        }
    }

    static function getSummaryBuyer($po_buyer, $item_code)
    {
        $adidas         = PoBuyer::where('po_buyer', $po_buyer)->where('brand', 'ADIDAS')->exists();
        if($adidas)
        {
            $po_buyer = str_replace('-S', '',$po_buyer);
        }
        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();
        $create_material_requirements = array();
        if(!$is_exist){

            $queryWhereItem = null;
            if($item_code) $queryWhereItem = "WHERE item_code = '$item_code'";

            $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement(
                '".$po_buyer."'
                ) $queryWhereItem;"
            ));

            //($material_requirements);

            $_po_buyer = PoBuyer::select('statistical_date','lc_date','promise_date')
            ->where('po_buyer',$po_buyer)
            ->groupby('statistical_date','lc_date','promise_date')
            ->first();
            
            if(count($material_requirements) > 0){
                MaterialRequirement::where([
                    ['po_buyer',$po_buyer],
                    ['is_upload_manual',false],
                ])
                ->when($item_code,function($query) use($item_code) {
                    $query->where('item_code',$item_code);
                })
                ->delete();

                foreach ($material_requirements as $key2 => $material_requirement) 
                {
                    $item   = Item::where('item_id',$material_requirement->item_id)->first();
                    $_style = explode('::',$material_requirement->job_order)[0];

                    if($material_requirement->item_desc) $_item_desc = $material_requirement->item_desc;
                    else ($item)? $_item_desc = $item->item_desc : null;

                    // $is_exists = MaterialRequirement::where([
                    //             ['po_buyer', $material_requirement->po_buyer],
                    //             ['item_id', $material_requirement->item_id],
                    //             ['style', $material_requirement->style],
                    //             ['article_no', $material_requirement->article_no],
                    //             //['qty_required', $material_requirement->qty_required],
                    //             ['warehouse_id', $material_requirement->warehouse_id],
                    //             ['is_upload_manual',false],
                    //     ])->exists();
                    
                    // if(!$is_exists)
                    //{
                            $create_material_requirement = MaterialRequirement::Create([
                                'lc_date'           => ($_po_buyer)? $_po_buyer->lc_date : null,
                                'season'            => $material_requirement->season,
                                'po_buyer'          => $material_requirement->po_buyer,
                                'article_no'        => $material_requirement->article_no,
                                'item_id'           => $material_requirement->item_id,
                                'item_code'         => strtoupper($material_requirement->item_code),
                                'item_desc'         => $_item_desc,
                                'uom'               => $material_requirement->uom,
                                'style'             => $material_requirement->style,
                                '_style'            => $_style,
                                'category'          => $material_requirement->category,
                                'job_order'         => $material_requirement->job_order,
                                'statistical_date'  => ($_po_buyer)? $_po_buyer->statistical_date : null,
                                'promise_date'      => ($_po_buyer)? $_po_buyer->promise_date : null,
                                'qty_required'      => sprintf('%0.8f',$material_requirement->qty_required),
                                'warehouse_id'      => $material_requirement->warehouse_id,
                                'warehouse_name'    => $material_requirement->warehouse_name,
                                'component'         => $material_requirement->component,
                                'destination_name'  => $material_requirement->destination_name,
                                'supplier_code'     => $material_requirement->supplier_code,
                                'supplier_name'     => $material_requirement->supplier_name
                            ]);

                            if($item_code !='')
                            {
                                $obj                    = new stdClass();
                                $obj->lc_date           = ($create_material_requirement->lc_date)?$create_material_requirement->lc_date->format('Y-m-d'):null;
                                $obj->statistical_date  = ($create_material_requirement->statistical_date)?$create_material_requirement->statistical_date->format('Y-m-d'):null;
                                $obj->season            = $create_material_requirement->season;
                                $obj->po_buyer          = $create_material_requirement->po_buyer;
                                $obj->item_code         = $create_material_requirement->item_code;
                                $obj->style             = $create_material_requirement->style;
                                $obj->article_no        = $create_material_requirement->article_no;
                                $obj->uom               = $create_material_requirement->uom;
                                $obj->warehouse_name = $create_material_requirement->warehouse_name;
                                $obj->qty_required      = $create_material_requirement->qty_required;

                                $create_material_requirements []               = $obj;
                            }
                        //dd($create_material_requirement);
                    // }
                    // else
                    // {
                    //     continue;
                    // }
                }
                return $create_material_requirements;
            }
        }
            

        
       

       
    }

    static function getSummaryBuyerPerPart($po_buyer, $item_code)
    {
        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();

        if(!$is_exist)
        {
            $queryWhereItem = null;
            if($item_code) $queryWhereItem = "WHERE item_code = '$item_code'";
            $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement_per_part(
                '".$po_buyer."'
                ) $queryWhereItem;"
            ));

            //$material_requirements = $material_requirements->where('item_code','like', '%' . $item_code . '%');

            $_po_buyer = PoBuyer::select('statistical_date','promise_date')
            ->where('po_buyer',$po_buyer)
            ->groupby('statistical_date','promise_date')
            ->first();

            if(count($material_requirements) > 0)
            {
                MaterialRequirementPerPart::where('po_buyer',$po_buyer)
                ->when($item_code,function($query) use ($item_code)
                {
                    $query->where('item_code', $item_code);
                })
                ->delete();

                foreach ($material_requirements as $key2 => $material_requirement) 
                {
                    MaterialRequirementPerPart::create([
                        'po_buyer'          => $material_requirement->po_buyer,
                        'item_id'           => $material_requirement->item_id,
                        'item_code'         => $material_requirement->item_code,
                        'article_no'        => $material_requirement->article_no,
                        'is_piping'         => $material_requirement->is_piping,
                        'style'             => $material_requirement->style,
                        'category'          => $material_requirement->category,
                        'uom'               => $material_requirement->uom,
                        'part_no'           => $material_requirement->part_no,
                        'qty_required'      => sprintf('%0.8f',$material_requirement->qty_required),
                        'statistical_date'  => ($_po_buyer)?$_po_buyer->statistical_date : null,
                        'promise_date'      => ($_po_buyer)? $_po_buyer->promise_date : null,
                        'lc_date'           => $material_requirement->lc_date
                    ]);
                }
            }
        }
    }

    static function getPobuyerLc()
    {
        $set_po_buyer = new PoBuyer();
        $insert = array();
        $app_env = Config::get('app.env');
        //if($app_env == 'dev') $data = DB::connection('dev_erp');
        //else if($app_env == 'live') $data = DB::connection('erp');
        //else  
        $data = DB::connection('erp');

        //$max_lc_date = PoBuyer::max(db::raw("(to_char(lc_date,'YYYYMM'))"));
        $data =  $data->table('wms_lc_buyers')
        ->select('kst_lcdate as lc_date',
                'poreference as po_buyer'
                ,'datepromised as promise_date'
                ,'kst_statisticaldate as statistical_date'
                ,'kst_statisticaldate2 as statistical_date_2'
                ,'ordertype as order_type'
                ,'brand as brand'
                ,'is_japan_china'
                ,'country'
                ,'issubcont'
                ,'kst_season as season'
                ,'so_id')
        //->whereIn(db::raw("to_char(kst_lcdate,'YYYYMM')"),['202102','202103','202104'])
        ->whereIn(db::raw("to_char(kst_lcdate,'YYYY-MM-DD')"),['2022-04-29', '2022-04-28'])
        //->where('poreference', 'like', '%-S%')
        //->where(db::raw("to_char(kst_lcdate,'YYYYMMDD')"),'20190328')
        //->where(db::raw("to_char(kst_lcdate,'YYYYMM')"),$max_lc_date)
        ->get();

        foreach ($data as $key => $value) 
        {
            $is_exists =  PoBuyer::where('po_buyer',$value->po_buyer)->first();
            if($value->statistical_date_2) $statistical_date = $value->statistical_date_2;
            else  $statistical_date = $value->statistical_date;

            if($value->brand == 'NEW BALANC') $brand ='NEW BALANCE';
            else $brand = $value->brand;

            if($value->order_type == 'MTF')
            {
                $type_stock                 = 'MTFC';
                $type_stock_erp_code        = '4';
            }else if($value->order_type == 'SLT')
            {
                $type_stock                 = 'SLT';
                $type_stock_erp_code        = '1';
            }else if($value->order_type == 'SR')
            {
                $type_stock                 = 'SR';
                $type_stock_erp_code        = '3';
            }else if($value->order_type == 'NB')
            {
                $type_stock                 = 'NB';
                $type_stock_erp_code        = '5';
            }else
            {
                if($brand == 'ADIDAS')
                {
                    $type_stock             = 'REGULER';
                    $type_stock_erp_code    = '2';
                }else
                {
                    $type_stock             = null;
                    $type_stock_erp_code    = null;
                }
            }
            
            if(!$is_exists)
            {
                PoBuyer::FirstorCreate([
                    'po_buyer'                      => $value->po_buyer,
                    'lc_date'                       => $value->lc_date,
                    'podd'                          => $value->statistical_date,
                    'statistical_date'              => $statistical_date,
                    'promise_date'                  => $value->promise_date,
                    'season'                        => $value->season,
                    'brand'                         => $brand,
                    'type_stock'                    => $type_stock,
                    'type_stock_erp_code'           => $type_stock_erp_code,
                    'is_ordering_for_japan_china'   => $value->is_japan_china,
                    'destination'                   => strtoupper($value->country),
                    'is_po_subcont'                 => ($value->issubcont == 'N' ? false : true),
                    'so_id'                         => $value->so_id,
                ]);

                if($statistical_date) AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $statistical_date, 'is_ordering_for_japan_china' => $value->is_japan_china]);
                else AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $value->promise_date,'is_ordering_for_japan_china' => $value->is_japan_china]);
                

            }else
            {
                PoBuyer::where('po_buyer',$value->po_buyer)
                ->update([
                    'promise_date'                  => $value->promise_date
                    ,'podd'                         => $value->statistical_date
                    ,'statistical_date'             => $statistical_date
                    ,'lc_date'                      => $value->lc_date
                    ,'season'                       => $value->season
                    ,'brand'                        => $brand
                    ,'type_stock'                   => $type_stock
                    ,'type_stock_erp_code'          => $type_stock_erp_code,
                    'is_ordering_for_japan_china'   => $value->is_japan_china,
                    'destination'                   => strtoupper($value->country),
                    'is_po_subcont'                 => ($value->issubcont == 'N' ? false : true)
                ]);

                if($value->statistical_date) AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $statistical_date, 'is_ordering_for_japan_china' => $value->is_japan_china]);
                else AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $value->promise_date, 'is_ordering_for_japan_china' => $value->is_japan_china]);
                
            }
            
        }
        
      
        
    }

    static function dailyUpdateStatisticalDate()
    {
        $po_buyers = PoBuyer::where(function($query){
            $query->whereNull('statistical_date')
            ->orWhereNull('promise_date')
            ->orWhereNull('lc_date');
        })
        ->whereNull('cancel_date')
        ->get();
 
        foreach ($po_buyers as $key => $value) 
        {
            $app_env = Config::get('app.env');

            //if($app_env == 'dev') $po_buyer = DB::connection('dev_erp');
            //else if($app_env == 'live') $po_buyer = DB::connection('erp');
            //else  
            $po_buyer = DB::connection('erp');
            
            $po_buyer = $po_buyer->table('wms_lc_buyers')
            ->select('kst_statisticaldate as statistical_date'
            ,'kst_statisticaldate2 as statistical_date_2'
            ,'ordertype as order_type'
            ,'brand as brand'
            ,'kst_season as season'
            ,'is_japan_china'
            ,'datepromised as promise_date')
            ->where('poreference',$value->po_buyer)
            ->first();

            if($po_buyer)
            {
                if($po_buyer->statistical_date_2) $statistical_date = $po_buyer->statistical_date_2;
                else $statistical_date = $po_buyer->statistical_date;

                if($po_buyer->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $po_buyer->brand;

                if($po_buyer->order_type == 'MTF')
                {
                    $type_stock = 'MTFC';
                    $type_stock_erp_code = '4';
                }else if($po_buyer->order_type == 'SLT')
                {
                    $type_stock = 'SLT';
                    $type_stock_erp_code = '1';
                }else if($po_buyer->order_type == 'SR')
                {
                    $type_stock = 'SR';
                    $type_stock_erp_code = '3';
                }else
                {
                    $type_stock = 'REGULER';
                    $type_stock_erp_code = '2';
                }
                
                if($statistical_date) AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $statistical_date,'is_ordering_for_japan_china' => $po_buyer->is_japan_china]);
                else AutoAllocation::where('po_buyer',$value->po_buyer)->update(['promise_date'  => $po_buyer->promise_date,'is_ordering_for_japan_china' => $po_buyer->is_japan_china]);
                

                if($statistical_date && ( $value->statistical_date == '' || $value->statistical_date == null))
                {
                    $value->statistical_date = $statistical_date;
                    $value->requirement_date = null;
                }

                 $value->podd                        = $po_buyer->statistical_date;
                 $value->promise_date                = $po_buyer->promise_date;
                 $value->season                      = $po_buyer->season;
                 $value->brand                       = $brand;
                 $value->type_stock                  = $type_stock;
                 $value->type_stock_erp_code         = $type_stock_erp_code;
                 $value->is_ordering_for_japan_china = $po_buyer->is_japan_china;
                $value->save();
            }
        }

        $update_po = DB::connection('erp');    
        $update_po = $update_po->table('rma_podd_check')
                    ->whereNotNull('poreference')
                    ->whereRaw('old_podd != new_podd')
                    ->where('is_integrate', false)
                    ->get();

        foreach ($update_po as $key => $value) 
        {
            $po_buyer = PoBuyer::where('po_buyer', $value->poreference)
                        ->update(['statistical_date' => $value->new_podd,
                                  'podd' => $value->old_podd
                        ]);
            DB::connection('erp') //dev_erp //erp
            ->table('rma_podd_check')
            ->where('id',$value->id)
            ->update([
                'is_integrate' => true,
            ]);
        }


    }

    static function dailyCronInsert()
    {
        /*$max_lc_date = PoBuyer::max(db::raw("(to_char(lc_date,'YYYYMM'))"));
        $po_buyers = PoBuyer::select('po_buyer')
        ->whereNull('requirement_date')
        ->whereNull('cancel_user_id')
        ->groupby('po_buyer');*/
        
        $new_po_buyers = PoBuyer::select('po_buyer')
        ->whereIn(db::raw("to_char(lc_date,'YYYY-MM-DD')"),['2022-04-29', '2022-04-28'])
        //->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['202102','202103','202104'])
        ->whereNull('cancel_user_id')
        ->where('is_reroute', false)
        ->groupby('po_buyer')
        ->get();

        //$get_po_buyers = $po_buyers->union($new_po_buyers)->get();
        
        $movement_date = carbon::now()->toDateTimeString();
        foreach ($new_po_buyers as $key1 => $value) 
        {

            $adidas         = PoBuyer::where('po_buyer', $value->po_buyer)->where('brand', 'ADIDAS')->exists();
            if($adidas)
            {
                $po_buyer = str_replace('-S', '',$value->po_buyer);
            }
            else
            {
                $po_buyer = $value->po_buyer;
            }

            $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();
            if(!$is_exist)
            {
                $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement(
                    '".$po_buyer."'
                    );"
                ));

                if(count($material_requirements) > 0)
                {
                    MaterialRequirement::where([
                        ['po_buyer',$po_buyer],
                        ['is_upload_manual',false],
                    ])
                    ->delete();

                    foreach ($material_requirements as $key2 => $material_requirement) 
                    {
                        $item = Item::select('item_desc')
                        ->where('item_code',$material_requirement->item_code)
                        ->groupby('item_desc')
                        ->first();

                        if($material_requirement->item_desc) $_item_desc = $material_requirement->item_desc;
                        else{
                            if($item) $_item_desc   =  $item->item_desc;
                            else $_item_desc        = 'DESKRIPSI ITEM TIDAK DITEMUKAN';
                        }

                        $_style = explode('::',$material_requirement->job_order)[0];
                           
                        $_po_buyer = PoBuyer::select('statistical_date','promise_date')
                        ->where('po_buyer',$material_requirement->po_buyer)
                        ->groupby('statistical_date','promise_date')
                        ->first();

                        MaterialRequirement::create([
                            'lc_date'           => $material_requirement->lc_date,
                            'season'            => $material_requirement->season,
                            'po_buyer'          => $material_requirement->po_buyer,
                            'article_no'        => $material_requirement->article_no,
                            'item_id'           => $material_requirement->item_id,
                            'item_code'         => $material_requirement->item_code,
                            'item_desc'         => $_item_desc,
                            'uom'               => $material_requirement->uom,
                            'style'             => $material_requirement->style,
                            '_style'            => $_style,
                            'category'          => $material_requirement->category,
                            'job_order'         => $material_requirement->job_order,
                            'statistical_date'  => ($_po_buyer)? $_po_buyer->statistical_date : null,
                            'promise_date'      => ($_po_buyer)? $_po_buyer->promise_date : null,
                            'qty_required'      => sprintf('%0.8f',$material_requirement->qty_required),
                            'warehouse_id'      => $material_requirement->warehouse_id,
                            'warehouse_name'    => $material_requirement->warehouse_name,
                            'component'         => $material_requirement->component,
                            'destination_name'  => $material_requirement->destination_name,
                            'supplier_code'     => $material_requirement->supplier_code,
                            'supplier_name'     => $material_requirement->supplier_name
                        ]);
                    }

                    Temporary::FirstOrCreate([
                        'barcode'       => $po_buyer,
                        'status'        => 'mrp',
                        'user_id'       => 1,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);

                    PoBuyer::where('po_buyer',$po_buyer)
                    ->whereNotNull('statistical_date')
                    ->update(['requirement_date' => carbon::now()]);
                }
                
            }
        }

        //DB::select(db::raw("select * from public.update_po_buyers();"));
    }

    static function dailyCronInsertPerPart()
    {
        /*$max_lc_date = PoBuyer::max(db::raw("(to_char(lc_date,'YYYYMM'))"));
        
        $po_buyers = PoBuyer::select('po_buyer')
        ->whereNull('requirement_date')
        ->whereNull('cancel_user_id')
        ->groupby('po_buyer');*/
        
        $new_po_buyers = PoBuyer::select('po_buyer')
        ->whereIn(db::raw("to_char(lc_date,'YYYY-MM-DD')"),['2022-04-29', '2022-04-28'])
        //->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['202102','202103','202104'])
        ->whereNull('cancel_user_id')
        ->where('is_reroute', false)
        ->groupby('po_buyer')
        ->get();

        //$get_po_buyers = $po_buyers->union($new_po_buyers)->get();
        

        foreach ($new_po_buyers as $key => $value) 
        {
            $adidas         = PoBuyer::where('po_buyer', $value->po_buyer)->where('brand', 'ADIDAS')->exists();
            if($adidas)
            {
                $po_buyer = str_replace('-S', '',$value->po_buyer);
            }
            else
            {
                $po_buyer = $value->po_buyer;
            }
            //$is_exist = ReroutePoBuyer::where('old_po_buyer',$value->po_buyer)->exists();
            //if(!$is_exist){
                $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement_per_part(
                    '".$po_buyer."'
                    );"
                ));

                if(count($material_requirements) > 0)
                {
                    MaterialRequirementPerPart::where('po_buyer',$po_buyer)->delete();

                    foreach ($material_requirements as $key2 => $material_requirement) 
                    {
                        $_po_buyer = PoBuyer::select('statistical_date','lc_date','promise_date')
                        ->where('po_buyer',$material_requirement->po_buyer)
                        ->groupby('statistical_date','lc_date','promise_date')
                        ->first();

                        MaterialRequirementPerPart::create([
                            'po_buyer'          => $material_requirement->po_buyer,
                            'item_id'           => $material_requirement->item_id,
                            'item_code'         => $material_requirement->item_code,
                            'article_no'        => $material_requirement->article_no,
                            'is_piping'         => $material_requirement->is_piping,
                            'style'             => $material_requirement->style,
                            'category'          => $material_requirement->category,
                            'uom'               => $material_requirement->uom,
                            'part_no'           => $material_requirement->part_no,
                            'qty_required'      => sprintf('%0.8f',$material_requirement->qty_required),
                            'statistical_date'  => ($_po_buyer) ? $_po_buyer->statistical_date : null,
                            'lc_date'           => ($_po_buyer) ? $_po_buyer->lc_date : null,
                            'promoise_date'     => ($_po_buyer) ? $_po_buyer->promise_date : null,
                        ]);
                    }
                }
                
            //}
        }
    }

    static function dailyDetailCronInsert()
    {
        /*$max_lc_date = PoBuyer::max(db::raw("(to_char(lc_date,'YYYYMM'))"));
        
        $po_buyers = PoBuyer::select('po_buyer')
        ->whereNull('requirement_date')
        ->whereNull('cancel_user_id')
        ->groupby('po_buyer');*/
        
        $new_po_buyers = PoBuyer::select('po_buyer')
        //->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['202102','202103','202104'])
        // 3 bulan
        ->whereIn(db::raw("to_char(lc_date,'YYYY-MM-DD')"),['2022-04-29', '2022-04-28']) 
        // ini sinkron 1 lc
        ->whereNull('cancel_user_id')
        ->where('is_reroute', false)
        ->groupby('po_buyer')
        ->get();

        //$get_po_buyers = $po_buyers->union($new_po_buyers)->get();
        

        foreach ($new_po_buyers as $key1 => $value) 
        {
            $is_exist = ReroutePoBuyer::where('old_po_buyer',$value->po_buyer)->exists();
            if(!$is_exist)
            {
                $adidas         = PoBuyer::where('po_buyer', $value->po_buyer)->where('brand', 'ADIDAS')->exists();
                if($adidas)
                {
                    $po_buyer = str_replace('-S', '',$value->po_buyer);
                }
                else
                {
                    $po_buyer = $value->po_buyer;
                }

                $app_env = Config::get('app.env');

                if($app_env == 'dev') $material_requirements = DB::connection('dev_erp');
                else if($app_env == 'live') $material_requirements = DB::connection('erp');
                else  $material_requirements = DB::connection('erp');
                
                $material_requirements = $material_requirements->select(db::raw("SELECT style,
                item_id,
                item_code,
                item_desc,
                uom,
                po_buyer,
                job_order,
                qty_required,
                article_no,
                is_piping,
                season,
                garment_size,
                part_no,
                warehouse_id,
                warehouse_name,
                city_name,
                component,
                category,
                supp_code,
                supp_name,
                qty_ordered_garment FROM get_wms_material_requirement_detail_new(
                    '".$po_buyer."');"
                    ));

                if(count($material_requirements) > 0)
                {
                    DetailMaterialRequirement::where('po_buyer',$po_buyer)->delete();
                    
                    foreach ($material_requirements as $key2 => $material_requirement) 
                    {
                        $_po_buyer = PoBuyer::select('statistical_date','lc_date','promise_date')
                        ->where('po_buyer',$material_requirement->po_buyer)
                        ->groupby('statistical_date','lc_date','promise_date')
                        ->first();

                        DetailMaterialRequirement::create([
                            'season'              => $material_requirement->season,
                            'po_buyer'            => $material_requirement->po_buyer,
                            'article_no'          => $material_requirement->article_no,
                            'item_id'             => $material_requirement->item_id,
                            'item_code'           => $material_requirement->item_code,
                            'item_desc'           => $material_requirement->item_desc,
                            'uom'                 => $material_requirement->uom,
                            'style'               => $material_requirement->style,
                            'category'            => $material_requirement->category,
                            'job_order'           => $material_requirement->job_order,
                            'statistical_date'    => ($_po_buyer)? $_po_buyer->statistical_date : null,
                            'promoise_date'       => ($_po_buyer)? $_po_buyer->promise_date : null,
                            'lc_date'             => ($_po_buyer)? $_po_buyer->lc_date : null,
                            'is_piping'           => $material_requirement->is_piping,
                            'garment_size'        => $material_requirement->garment_size,
                            'part_no'             => $material_requirement->part_no,
                            'qty_required'        => sprintf('%0.8f',$material_requirement->qty_required),
                            'warehouse_id'        => $material_requirement->warehouse_id,
                            'warehouse_name'      => $material_requirement->warehouse_name,
                            'component'           => $material_requirement->component,
                            'destination_name'    => $material_requirement->city_name,
                            'supplier_code'       => $material_requirement->supp_code,
                            'supplier_name'       => $material_requirement->supp_name,
                            'qty_ordered_garment' => $material_requirement->qty_ordered_garment,
                        ]);
                    }
                }
                
            }
        }
    }

    static function dailyUpdatePobuyer()
    {
        $material_preparations = MaterialPreparation::whereNull('style')
        ->orderby('created_at','asc')
        ->get();
        
        foreach ($material_preparations as $key => $value) {
            $rows_update_style_job = '';
            $flag=0;

            $material_requirement = MaterialRequirement::select('item_code','po_buyer','category')
            ->where([
                ['po_buyer',$value->po_buyer],
                ['item_code',$value->item_code],
                ['category',$value->category]
            ])
            ->groupby('item_code','po_buyer','category')
            ->first();
           
            if($material_requirement){
                $_material_requirement = $material_requirement->dataMarge($material_requirement->item_code,$material_requirement->po_buyer,$material_requirement->category);
                $rows_update_style_job .= "(" 
                    ."'". $value->id . "'," 
                    ."'". $_material_requirement->style . "',"
                    ."'". $_material_requirement->article_no . "',"
                    ."'". $_material_requirement->job_order . "'". 
                "),";
                $flag++;
            }
        }

        if($flag > 0){
            try{
                db::beginTransaction();

                $rows_update_style_job = substr_replace($rows_update_style_job, "", -1);
                db::select(db::raw("
                        update material_preparations as tbl
                        set
                            style = col.style,
                            article_no = col.article_no,
                            job_order = col.job_order,
                            updated_at = '".carbon::now()."'
                        from (values
                            " . $rows_update_style_job . "
                        ) as col(
                            id,
                            style,
                            article_no,
                            job_order
                        )
                        where col.id = tbl.id;
                    "));
                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message); 
            }
        }


    }

    private function getPurchaseItems($_po_buyer)
    {
        $movement_date  = carbon::now()->todatetimestring();
        $reroute        = ReroutePoBuyer::where('old_po_buyer',$_po_buyer)->first();
        if($reroute)
        {
            $old_po_buyer = $reroute->old_po_buyer;
            $new_po_buyer = $reroute->new_po_buyer;
        }else
        {
            $old_po_buyer = $_po_buyer;
            $new_po_buyer = $_po_buyer;
        }

        $app_env        = Config::get('app.env');
        $array          = array();
        $concatenate    = '';
        $purchase_items = DB::connection('erp')
        ->table('rma_wms_planning_v2')
        ->select('document_no',
                'item_id',
                'c_order_id',
                'c_bpartner_id',
                'supplier_name',
                'po_buyer',
                'm_warehouse_id as warehouse',
                'item_code',
                'uom_pr as uom',
                'uom_po as uom_po',
                'category',
                db::raw('sum(qty_pr) as qty'),
                db::raw('sum(qtyordered) as qty_order'))
        ->where([
            ['po_buyer',$new_po_buyer],
        ])
        ->where('document_no','NOT LIKE',"%^%")
        ->orderBy('po_buyer','desc')
        ->groupby('document_no',
        'item_id',
        'c_order_id',
        'c_bpartner_id',
        'supplier_name',
        'po_buyer',
        'm_warehouse_id',
        'item_code',
        'uom_pr',
        'uom_po',
        'category')
        ->get();

        $__po_buyer = PoBuyer::where(db::raw('po_buyer'),$new_po_buyer)->first();
        
        $lc_date    = ($__po_buyer)? $__po_buyer->lc_date : null;

        if($__po_buyer)
        {
            if($__po_buyer->statistical_date) $promise_date = ($__po_buyer)? $__po_buyer->statistical_date : null;
            else $promise_date = ($__po_buyer)? $__po_buyer->promise_date : null;
        }else
        {
            $promise_date = null;
        }
        
        $season                 = ($__po_buyer)? $__po_buyer->season : null;
        $type_stock_erp_code    = ($__po_buyer)? $__po_buyer->type_stock_erp_code : null;

        if($type_stock_erp_code == 1) $type_stock       = 'SLT';
        else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
        else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
        else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
        else if($type_stock_erp_code == 5) $type_stock  = 'NB';
        else $type_stock = null;
        
        $cancel_date    = ($__po_buyer)? $__po_buyer->cancel_date : null;
        if($cancel_date != null) $status_po_buyer = 'cancel';
        else $status_po_buyer = 'active';
        
        $concatenate    .= "'" .$_po_buyer."',";
        
        try 
        {
            DB::beginTransaction();

            foreach ($purchase_items as $key => $purchase_item)
            {
                $document_no                = strtoupper($purchase_item->document_no);
                $c_order_id                 = $purchase_item->c_order_id;
                $c_bpartner_id              = $purchase_item->c_bpartner_id;
                $supplier_name              = strtoupper($purchase_item->supplier_name);
                $po_buyer                   = $purchase_item->po_buyer;
                $item_id                    = $purchase_item->item_id;
                $item_code                  = strtoupper($purchase_item->item_code);
                $warehouse                  = $purchase_item->warehouse;
                $qty                        = sprintf('%0.8f',$purchase_item->qty);
                $qty_order                  = sprintf('%0.8f',$purchase_item->qty_order);
                $qty_moq                    = sprintf('%0.8f',$qty_order - $qty);
                $uom                        = $purchase_item->uom;
                $uom_po                     = $purchase_item->uom_po;
                $category                   = $purchase_item->category;

                if($warehouse == '1000002') $warehouse_name = 'ACCESSORIES AOI-1';
                else if($warehouse == '1000013') $warehouse_name = 'ACCESSORIES AOI-2';
                else if($warehouse == '1000011') $warehouse_name = 'FABRIC AOI-2';
                else if($warehouse == '1000001') $warehouse_name = 'FABRIC AOI-1';

                $system                     = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse]
                ])
                ->first();

                $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();

                $item                       = Item::where('item_id',$item_id)->first();
                $item_desc                  = ($item)? strtoupper($item->item_desc): 'data not found';
                $uom                        = ($item)? strtoupper($item->uom): 'data not found';
                $category                   = ($item)? strtoupper($item->category): 'data not found';

                if($category == 'FB' && ($new_po_buyer != null || $new_po_buyer != ''))
                {
                    if($lc_date >= '2019-08-15')// cut off mulai lc tanggal ini, user baru setuju mau maintain
                    {   
                        if($warehouse == '1000011') $warehouse_name = 'FABRIC AOI 2';
                        else if($warehouse == '1000001') $warehouse_name = 'FABRIC AOI 1';

                        $is_exists = AutoAllocation::where([
                            [db::raw('upper(document_no)'),$document_no],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['po_buyer',$new_po_buyer],
                            [db::raw('upper(item_code)'),$item_code],
                            ['warehouse_id',$warehouse],
                            ['qty_allocation',$qty],
                            ['is_fabric',true],
                        ])
                        ->exists();

                        if(!$is_exists)
                        {
                            AutoAllocation::firstorcreate([
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'c_order_id'                        => $c_order_id,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => true,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'user_id'                           => $system->id,
                                'so_id'                             => $so_id->so_id
                            ]);
                        }
                    }
                }else
                {
                    if($category != 'FB' && $new_po_buyer != null || $new_po_buyer != '')
                    {
                        // NEW 
                        $is_exists = AutoAllocation::where([
                            [db::raw('upper(document_no)'),$document_no],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['po_buyer',$new_po_buyer],
                            [db::raw('upper(item_code)'),$item_code],
                            ['warehouse_id',$warehouse],
                            ['qty_allocation',$qty],
                            ['is_fabric',false],
                        ])
                        ->exists();

                        if(!$is_exists)
                        {
                            $tgl_date                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                            $month_lc                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('my')  : 'LC NOT FOUND');
        
                            if($warehouse == '1000002') $warehouse_sequnce = '001';
                            else if($warehouse == '1000013') $warehouse_sequnce = '002';
        
                            $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;

                            AutoAllocation::firstorcreate([
                                'document_allocation_number'        => $document_allocation_number,
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'c_order_id'                        => $c_order_id,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => false,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'user_id'                           => $system->id,
                                'so_id'                             => $so_id->so_id
                            ]);
                        }

                    }
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        //
    }

    static function dailyPlanningFabric()
    {
        /*
            COMMENT DLU BELUM TAU INI BERGUNA APA GAK, SOALNYA UDAH ADA SCHEDULE PLANNING TIAP MALAM
        $max_lc_date = PoBuyer::max(db::raw("(to_char(lc_date,'YYYYMM'))"));
        
        $po_buyers = PoBuyer::select('po_buyer')
        ->whereNull('requirement_date')
        ->whereNull('cancel_user_id')
        ->groupby('po_buyer');
        
        $new_po_buyers = PoBuyer::select('po_buyer')
        ->whereIn(db::raw("to_char(lc_date,'YYYY-MM-DD')"),['2019-08-29'])
        //->whereIn(db::raw("to_char(lc_date,'YYYYMM')"),['201906','201907','201908'])
        ->whereNull('cancel_user_id')
        ->groupby('po_buyer')
        ->get();

        $preparation_fabrics    = array();
        $app_env                = Config::get('app.env');

        if($app_env == 'dev') $cutting_instructions = DB::connection('dev_erp');
        else if($app_env == 'live') $cutting_instructions = DB::connection('erp');
        else  $cutting_instructions = DB::connection('erp');

        foreach ($new_po_buyers as $key_0 => $value0) 
        {
            $po_buyer = $value0->po_buyer;
            MaterialPlanningFabric::where('po_buyer',$po_buyer)->delete();
           
            $cutting_instructions =  $cutting_instructions->select(db::raw("SELECT * FROM adempiere.wms_get_csi_buyer_all_warehouse(
                '".$po_buyer."'
                )order by datestartschedule asc;"
            )); 

            foreach ($cutting_instructions as $key => $cutting_instruction) 
            {
                $item_code              = strtoupper($cutting_instruction->item_code);
                $_warehouse_id          = $cutting_instruction->m_warehouse_id;
                $style                  = $cutting_instruction->style;
                $planning_date          = $cutting_instruction->datestartschedule;
                $article_no             = $cutting_instruction->article_no;
                $job_order              = $cutting_instruction->job_order;
                $uom                    = $cutting_instruction->uom;
                $color                  = $cutting_instruction->color;
                $is_piping              = $cutting_instruction->is_piping;
                $qty_consumtion         = sprintf('%0.8f',$cutting_instruction->qty_consumtion);

                if($_warehouse_id == '1000012') $warehouse_id = '1000011';
                else if($_warehouse_id == '1000005') $warehouse_id = '1000001';

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id],
                ])
                ->first();

                $is_planning_exists = MaterialPlanningFabric::where([
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    [db::raw('upper(item_code)'),$item_code],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_piping',$is_piping],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('deleted_at')
                ->first();
                
                if(!$is_planning_exists)
                {
                    $material_planning_fabric = MaterialPlanningFabric::firstorcreate([
                        'po_buyer'          => $po_buyer,
                        'item_code'         => $item_code,
                        'color'             => $color,
                        'planning_date'     => $planning_date,
                        'style'             => $style,
                        'article_no'        => $article_no,
                        'job_order'         => $job_order,
                        'uom'               => $uom,
                        'is_piping'         => $is_piping,
                        'qty_consumtion'    => $qty_consumtion,
                        'warehouse_id'      => $warehouse_id,
                        'user_id'           => $system->id
                    ]);

                    $preparation_fabrics [] = $material_planning_fabric->id;
                }else{
                    $is_planning_exists->qty_consumtion = $qty_consumtion;
                    $is_planning_exists->save();
                    $preparation_fabrics [] = $is_planning_exists->id;
                }
            }
        }*/
    }

    static function getPlanningFabric($_po_buyer)
    {
        /*
            COMMENT DLU BELUM TAU INI BERGUNA APA GAK, SOALNYA UDAH ADA SCHEDULE PLANNING TIAP MALAM
        
        $app_env = Config::get('app.env');
        $check_csi_on_wms = DB::connection('erp');
        $check_csi_on_wms =  $check_csi_on_wms->select(db::raw("SELECT * FROM adempiere.wms_get_csi_date(
            '".$_po_buyer."'
            )"
        )); 

        foreach ($check_csi_on_wms as $key => $value) 
        {
            $_planning_date = $value->planning_date;
            $item_code      = $value->item_code;
            $_warehouse_id  = $value->warehouse_id;

            if($_warehouse_id == '1000012') $warehouse_id = '1000011';
            else if($_warehouse_id == '1000005') $warehouse_id = '1000001';

            $pos = strpos($_planning_date, ',');
            if ($pos === false)
            {
                $planning_date = [$_planning_date];
            } else 
            {
                $planning_date = explode(',',$_planning_date);
            }

            $material_planning_fabrics = MaterialPlanningFabric::where([
                ['po_buyer',$_po_buyer],
                [db::raw('upper(item_code)'),$item_code],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->get();

            foreach ($material_planning_fabrics as $key_2 => $material_planning_fabric) 
            {
                $planning_date_wms = $material_planning_fabric->planning_date;
                if(!in_array($planning_date_wms, $planning_date))
                {
                    $material_planning_fabric->deleted_at = carbon::now();
                    $material_planning_fabric->remark = 'CANCEL PLANNING';
                    $material_planning_fabric->save();
                    MaterialPlanningFabricPiping::where('material_planning_fabric_id',$material_planning_fabric->id)->update(['deleted_at' => carbon::now()]);;
                    
                }

                
            }
        }
        
        $cutting_instructions =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_buyer_all_warehouse(
            '".$_po_buyer."'
            )order by datestartschedule asc;"
        )); 

        $preparation_fabrics = array();
        foreach ($cutting_instructions as $key => $cutting_instruction) 
        {
            $item_code          = strtoupper($cutting_instruction->item_code);
            $_warehouse_id      = $cutting_instruction->m_warehouse_id;
            $style              = $cutting_instruction->style;
            $planning_date      = $cutting_instruction->datestartschedule;
            $article_no         = $cutting_instruction->article_no;
            $job_order          = $cutting_instruction->job_order;
            $uom                = $cutting_instruction->uom;
            $color              = $cutting_instruction->color;
            $is_piping          = $cutting_instruction->is_piping;
            $qty_consumtion     = sprintf('%0.8f',$cutting_instruction->qty_consumtion);

            if($_warehouse_id == '1000012') $warehouse_id = '1000011';
            else if($_warehouse_id == '1000005') $warehouse_id = '1000001';
            else $warehouse_id = null;

            $is_planning_exists = MaterialPlanningFabric::where([
                ['planning_date',$planning_date],
                ['po_buyer',$_po_buyer],
                [db::raw('upper(item_code)'),$item_code],
                ['style',$style],
                ['article_no',$article_no],
                ['is_piping',$is_piping],
                ['qty_consumtion',$qty_consumtion],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->first();
            
            if(!$is_planning_exists){
                $material_planning_fabric = MaterialPlanningFabric::firstorcreate([
                    'po_buyer' => $_po_buyer,
                    'item_code' => $item_code,
                    'color' => $color,
                    'planning_date' => $planning_date,
                    'style' => $style,
                    'article_no' => $article_no,
                    'job_order' => $job_order,
                    'uom' => $uom,
                    'is_piping' => $is_piping,
                    'qty_consumtion' => $qty_consumtion,
                    'warehouse_id' => $warehouse_id,
                    'user_id' => auth::user()->id
                ]);

                $preparation_fabrics [] = $material_planning_fabric->id;
            }else{
                $is_planning_exists->qty_consumtion = $qty_consumtion;
                $is_planning_exists->save();
                $preparation_fabrics [] = $is_planning_exists->id;
            }
        }*/
    }

    static function insertAllocationFabric($po_buyer)
    {
        /*
         COMMENT DLU BELUM TAU INI BERGUNA APA GAK, SOALNYA UDAH ADA SCHEDULE PLANNING TIAP MALAM
        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['po_buyer',$po_buyer],
        ])
        ->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();

        foreach ($auto_allocations as $key_2 => $auto_allocation) 
        {
            $auto_allocation_id = $auto_allocation->id;
            $document_no = $auto_allocation->document_no;
            $c_bpartner_id = $auto_allocation->c_bpartner_id;
            $po_buyer = $auto_allocation->po_buyer;
            $item_code = $auto_allocation->item_code;
            $qty_outstanding = sprintf('%0.8f',$auto_allocation->qty_outstanding);
            $qty_allocated = sprintf('%0.8f',$auto_allocation->qty_allocated);
            $warehouse_id = $auto_allocation->warehouse_id;
            $supplier = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
            $supplier_name = ($supplier)? $supplier->supplier_name : null;
            $supplier_code = ($supplier)? $supplier->supplier_code : null;
            
            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
            
            $material_requirements = MaterialRequirement::where([
                ['po_buyer',$po_buyer],
                [db::raw('upper(item_code)'),$item_code],
            ])
            ->get();

            if($qty_outstanding > 0)
            {
                $count_mr = $material_requirements->count();
                foreach ($material_requirements as $key => $material_requirement) 
                {
                    $lc_date = $material_requirement->lc_date;
                    $item_desc = strtoupper($material_requirement->item_desc);
                    $uom = $material_requirement->uom;
                    $category = $material_requirement->category;
                    $style = $material_requirement->style;
                    $job_order = $material_requirement->job_order;
                    $_style = explode('::',$job_order)[0];
                    $article_no = $material_requirement->article_no;
                    
                    $month_lc = $material_requirement->lc_date->format('m');
                    if($month_lc <= '02') $qty_required = sprintf("%0.2f",$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                    else $qty_required = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                    
                    if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                    else $__supplied = sprintf('%0.8f',$qty_outstanding);

                    $_warehouse_erp_id = $material_requirement->warehouse_id;

                    if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                    else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                    else $warehouse_production_id = $warehouse_id;
                    
                    if($qty_required > 0 && $qty_outstanding > 0){
                        if ($qty_outstanding/$__supplied >= 1) $_supply = $__supplied;
                        else $_supply = $qty_outstanding;

                        if($month_lc <= '02') $__supply = sprintf("%0.2f",$_supply);
                        else $__supply = sprintf('%0.8f',$_supply);
                    
                        $is_exists = AllocationItem::where([
                            ['auto_allocation_id',$auto_allocation_id],
                            ['item_code',$item_code],
                            ['style',$style],
                            ['po_buyer',$po_buyer],
                            ['qty_booking',$__supply],
                            ['is_additional',false],
                            ['is_from_allocation_fabric',true],
                            ['warehouse_inventory',$warehouse_id] // warehouse inventory
                        ])
                        ->where(function($query){
                            $query->where('confirm_by_warehouse','approved')
                            ->OrWhereNull('confirm_by_warehouse');
                        })
                        ->whereNull('deleted_at')
                        ->exists();
                        
                        if(!$is_exists && $_supply > 0){
                            $allocation_item = AllocationItem::FirstOrCreate([
                                'auto_allocation_id' => $auto_allocation_id,
                                'lc_date' => $lc_date,
                                'c_bpartner_id' => $c_bpartner_id,
                                'supplier_code' => $supplier_code,
                                'supplier_name' => $supplier_name,
                                'document_no' => $document_no,
                                'item_code' => $item_code,
                                'item_desc' => $item_desc,
                                'category' => $category,
                                'uom' => $uom,
                                'job' => $job_order,
                                'style' => $style,
                                '_style' => $_style,
                                'article_no' => $article_no,
                                'po_buyer' => $po_buyer,
                                'warehouse' => $warehouse_production_id,
                                'warehouse_inventory' => $warehouse_id,
                                'is_need_to_handover' => false,
                                'qty_booking' => $__supply,
                                'is_from_allocation_fabric' => true,
                                'user_id' => auth::user()->id,
                                'confirm_by_warehouse' => 'approved',
                                'confirm_date' => Carbon::now(),
                                'remark' => 'ALLOCATION PURCHASE',
                                'confirm_user_id' => auth::user()->id,
                                'deleted_at' => null
                            ]);

                            //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                            //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                            $qty_required -= $_supply;
                            $qty_outstanding -= $_supply;
                            $qty_allocated += $_supply;
                        }
                    }
                }
            }
            
            //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
            
            if(intVal($qty_outstanding)<= 0)
            {
                $auto_allocation->is_already_generate_form_booking = true;
                $auto_allocation->generate_form_booking = carbon::now();
                $auto_allocation->qty_outstanding = 0;
            }else
            {
                $auto_allocation->qty_outstanding = sprintf('%0.8f',$qty_outstanding);
            
            }

            $auto_allocation->qty_allocated = sprintf('%0.8f',$qty_allocated);
            $auto_allocation->save();
            
        }*/
    }

    public function export()
    {
         return Excel::create('upload_buyer',function ($excel)
         {
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','IS_REDUCE');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
            
            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function import(Request $request)
    {
        $array = array();
        $obj = new stdClass();
        $po_buyer = [];
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $movement_date  = carbon::now()->toDateTimeString();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    
                    foreach ($data as $key => $value) 
                    {
                        $_is_reduce = strtoupper(trim($value->is_reduce));
                        if($_is_reduce == 'TRUE' || $_is_reduce =='T') $is_reduce = true;
                        else $is_reduce = false;

                        $is_cancel = PoBuyer::where('po_buyer',trim($value->po_buyer))
                        ->whereNotNull('cancel_user_id')
                        ->exists();
                        
                        if(!$is_cancel)
                        {
                            $reroute = ReroutePoBuyer::where('old_po_buyer',trim($value->po_buyer))->exists();

                            if(!$reroute)
                            {
                                $po_buyer[] = trim($value->po_buyer);
                                
                                if($value->po_buyer != '')
                                {
                                    $exists = Temporary::where('barcode', trim($value->po_buyer))->exists();
                                    if(!$exists)
                                    {
                                        Temporary::FirstOrCreate([
                                            'barcode'       => trim($value->po_buyer),
                                            'boolean_1'     => $is_reduce,
                                            'status'        => 'sync_material_requirement',
                                            'user_id'       => 1,
                                            'created_at'    => $movement_date,
                                            'updated_at'    => $movement_date,
                                        ]);
                                    }
                                }
                            }
                        }
                        
                    }
                
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            return route('erpMaterialRequirement.index',[
                '_po_buyer'=>$po_buyer
            ]);
        }
    }

    static function syncMaterialRequirement()
    {
        $movement_date  = carbon::now()->toDateTimeString();
        $data           = Temporary::where('status','sync_material_requirement')->take(2)->get();

        foreach ($data as $key => $value) 
        {
            ErpMaterialRequirementController::getPobuyer(trim($value->barcode), '');
            ErpMaterialRequirementController::getSummaryBuyerPerPart(trim($value->barcode), '');
            ErpMaterialRequirementController::getSummaryBuyer(trim($value->barcode), '');
            //boolean_1
            
            if($value->boolean_1)  ErpMaterialRequirementController::reduceAllocation($value->barcode);

            //$this->getPurchaseItems(trim($value->po_buyer)); //ga boleh di uncomment
            PlanningFabric::getAllocationFabricPerBuyer([$value->barcode]);
            Temporary::FirstOrCreate([
                'barcode'       => $value->barcode,
                'status'        => 'mrp',
                'user_id'       => 1,
                'created_at'    => $movement_date,
                'updated_at'    => $movement_date,
            ]);

            $value->delete();
        }
    }

    public function store(Request $request)
    {
        $po_buyer       = $request->po_buyer;
        $is_reduce      = ($request->exists('is_reduce') ? true:false);
        $movement_date  = carbon::now()->toDateTimeString();

        $is_cancel = PoBuyer::where('po_buyer',$po_buyer)
        ->whereNotNull('cancel_user_id')
        ->exists();

        if($is_cancel) return response()->json('PO BUYER INI CANCEL','422');
        $reroute = ReroutePoBuyer::where('old_po_buyer',trim($po_buyer))->exists();
        if($reroute) return response()->json('PO BUYER INI REROUTE','422');

        try 
        {
            DB::beginTransaction();

            $this->getPobuyer($po_buyer, '');
            $this->getSummaryBuyerPerPart($po_buyer, '');
            $this->getSummaryBuyer($po_buyer, '');
            if($is_reduce)  $this->reduceAllocation($po_buyer);

            //$this->getPurchaseItems($po_buyer); //ga boleh di uncomment
            PlanningFabric::getAllocationFabricPerBuyer([$po_buyer]);

            Temporary::FirstOrCreate([
                'barcode'       => $po_buyer,
                'status'        => 'mrp',
                'user_id'       => 1,
                'created_at'    => $movement_date,
                'updated_at'    => $movement_date,
            ]);


            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json($po_buyer);
    }

    static function reduceAllocation($po_buyer)
    {
        $data = DB::select(db::raw("SELECT * FROM get_total_allocation_vs_need(
                '".$po_buyer."'
            );"
        ));

        $upload_date    = carbon::now();
        
        foreach ($data as $key => $datum) 
        {
            $po_buyer           = $datum->po_buyer;
            $item_id_book       = $datum->item_id_book;
            $total_required     = $datum->total_required; //108
            
            $auto_allocations   = AutoAllocation::where([
                ['po_buyer',$po_buyer],
                ['item_id_book',$item_id_book],
                ['is_additional',false],
            ])
            ->whereNull('deleted_at')
            ->orderby('is_allocation_purchase','desc')
            ->orderby('qty_allocation','desc')
            ->orderby('qty_allocated','desc')
            ->get();

            foreach ($auto_allocations as $key => $auto_allocation) 
            {
                $warehouse_id       = $auto_allocation->warehouse_id;
                $qty_oustanding     = sprintf('%0.8f',$auto_allocation->qty_oustanding);
                $qty_allocation     = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $total_qty_in       = sprintf('%0.8f',$auto_allocation->getTotalQtyIn($auto_allocation->id));
                
                if ($total_required/$qty_allocation >= 1) $new_qty_need  = $qty_allocation;
                else $new_qty_need   = $total_required;
                
                $user           = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id],
                ])
                ->first();

                if($total_required > 0)
                {
                    if($new_qty_need > 0)
                    {
                        $qty_reduce                         = sprintf('%0.8f',$auto_allocation->qty_allocation - $new_qty_need);
                        $new_qty_outstanding                = sprintf('%0.8f',$new_qty_need - $auto_allocation->qty_allocated);
                        $auto_allocation->qty_allocation    = sprintf('%0.8f',$new_qty_need);
                        $auto_allocation->qty_adjustment    = sprintf('%0.8f',$qty_reduce);
                        // ini di biarin kayak gini biar bisa di prepare sesuai qty awal
                        if($auto_allocation->qty_outstanding == 0) $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_qty_outstanding);

                        if($auto_allocation->is_fabric)
                        {
                            $auto_allocation->save();
                            AllocationItem::where('auto_allocation_id',$auto_allocation->id)->delete();
                            MasterDataAutoAllocationController::doRecalculate($auto_allocation->id);
                            MasterDataAutoAllocationController::updateAllocationFabricItems([$auto_allocation->id],$auto_allocation->id);
                        }else
                        {
                            if($qty_oustanding >= $qty_reduce || $total_qty_in >= $qty_reduce)
                            {
                                if(!$auto_allocation->is_fabric && $auto_allocation->is_allocation_purchase)
                                {
                                    $c_order_id                     = $auto_allocation->c_order_id;
                                    $document_no                    = $auto_allocation->document_no;
                                    $c_bpartner_id                  = $auto_allocation->c_bpartner_id;
                                    $warehouse_id                   = $auto_allocation->warehouse_id;
                                    $supplier_name                  = $auto_allocation->supplier_name;
                                    $item_id                        = $auto_allocation->item_id_source;
                                    $item_code                      = $auto_allocation->item_code_source;
                                    $item_desc                      = $auto_allocation->item_desc;
                                    $category                       = $auto_allocation->category;
                                    $allocation_po_buyer            = $auto_allocation->po_buyer;
                                    $uom                            = $auto_allocation->uom;
        
                                    if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK') $supplier_code = 'FREE STOCK';
                                    else
                                    {
                                        $supplier                       = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                        if($supplier) $supplier_code    = $supplier->supplier_code;
                                        else $supplier_code             = null;
                                    }
        
                                    if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK')
                                    {
                                        $c_bpartner_id                  = 'FREE STOCK';
                                        $supplier_code                  = 'FREE STOCK';
                        
                                        $mapping_stock_id               = null;
                                        $type_stock_erp_code            = '2';
                                        $type_stock                     = 'REGULER';
                                        
                                    }else
                                    {
                                        
                                        $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                        if($_supplier) $supplier_code   = $_supplier->supplier_code;
                                        else $supplier_code             = null;
                        
                                        $get_4_digit_from_beginning     = substr($document_no,0, 4);
                                        $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                                        
                                        if($_mapping_stock_4_digt)
                                        {
                                            $mapping_stock_id           = $_mapping_stock_4_digt->id;
                                            $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                                            $type_stock                 = $_mapping_stock_4_digt->type_stock;
                                        }else
                                        {
                                            $get_5_digit_from_beginning = substr($document_no,0, 5);
                                            $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                            if($_mapping_stock_5_digt)
                                            {
                                                $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                                $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                                $type_stock             = $_mapping_stock_5_digt->type_stock;
                                            }else
                                            {
                                                $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
                                                $mapping_stock_id       = null;
                                                $type_stock_erp_code    = ($_po_buyer)? $_po_buyer->type_stock_erp_code : NULL;
                                                $type_stock             = ($_po_buyer)? $_po_buyer->type_stock : null;
                                            }
                                        }
                                    }
        
                                    $check_material_stock = MaterialStock::where([
                                        ['c_order_id',$c_order_id],
                                        ['c_bpartner_id',$c_bpartner_id],
                                        ['item_code',$item_code],
                                        ['warehouse_id',$warehouse_id],
                                        ['is_stock_on_the_fly',true],
                                        ['is_running_stock',false],
                                        ['is_closing_balance',false],
                                        ['stock',$qty_reduce],
                                        ['type_stock_erp_code',$type_stock_erp_code],
                                    ])
                                    ->whereNull('deleted_at');
        
                                    if($po_buyer) $check_material_stock = $check_material_stock->where('po_buyer',$po_buyer);
                                    $check_material_stock = $check_material_stock->exists();
        
                                    if(!$check_material_stock)
                                    {
                                        $material_stock = MaterialStock::FirstOrCreate([
                                            'type_stock_erp_code'   => $type_stock_erp_code,
                                            'type_stock'            => $type_stock,
                                            'supplier_code'         => $supplier_code,
                                            'supplier_name'         => $supplier_name,
                                            'document_no'           => $document_no,
                                            'c_bpartner_id'         => $c_bpartner_id,
                                            'c_order_id'            => $c_order_id,
                                            'item_id'               => $item_id,
                                            'item_code'             => $item_code,
                                            'po_buyer'              => ($po_buyer ? $po_buyer : null),
                                            'item_desc'             => $item_desc,
                                            'category'              => $category,
                                            'type_po'               => 2,
                                            'warehouse_id'          => $warehouse_id,
                                            'uom'                   => $uom,
                                            'qty_carton'            => 1,
                                            'stock'                 => $qty_reduce,
                                            'remark'                => 'REDUCE',
                                            'reserved_qty'          => 0,
                                            'created_at'            => $upload_date,
                                            'updated_at'            => $upload_date,
                                            'is_closing_balance'    => false,
                                            'deleted_at'            => null,
                                            'is_active'             => true,
                                            'is_stock_on_the_fly'   => true,
                                            'source'                => 'STOCK ON THE FLY DUE REDUCE FROM '.$allocation_po_buyer,
                                            'user_id'               => $user->id
                                        ]);
        
                                        HistoryStock::approved($material_stock->id
                                        ,$material_stock->stock
                                        ,'0'
                                        ,null
                                        ,null
                                        ,null
                                        ,null
                                        ,null
                                        ,'SOTF DUE REDUCE FROM '.$allocation_po_buyer
                                        ,$user->name
                                        ,$user->id
                                        ,$type_stock_erp_code
                                        ,$type_stock
                                        ,false
                                        ,false);
                                    }
                                }
                            }
                        }
                        
                        $auto_allocation->is_reduce         = true;
                        if($qty_reduce > 0)
                        {
                            MaterialPreparation::where('auto_allocation_id',$auto_allocation->id)
                            ->update([
                                'is_reduce' => true
                            ]);
                        }
                        
                    }
                }else
                {
                    // lgsg di 0 kan.
                    $qty_reduce                         = sprintf('%0.8f',$qty_allocation);
                    $new_qty_outstanding                = sprintf('%0.8f',0 - $auto_allocation->qty_allocated);
                    $auto_allocation->qty_allocation    = sprintf('%0.8f',0);
                    $auto_allocation->qty_adjustment     = sprintf('%0.8f',$qty_reduce);
                    // ini di biarin kayak gini biar bisa di prepare sesuai qty awal
                    if($auto_allocation->qty_outstanding == 0) $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_qty_outstanding);
                    
                    if($auto_allocation->is_fabric)
                    {
                        $auto_allocation->deleted_at = carbon::now();
                        $auto_allocation->save();
                        AllocationItem::where('auto_allocation_id',$auto_allocation->id)->delete();
                        //MasterDataAutoAllocationController::doRecalculate($auto_allocation->id);
                        //MasterDataAutoAllocationController::updateAllocationFabricItems([$auto_allocation->id],$auto_allocation->id);
                    }else
                    {
                        if(!$auto_allocation->is_fabric && $auto_allocation->is_allocation_purchase)
                        {
                            $c_order_id                     = $auto_allocation->c_order_id;
                            $document_no                    = $auto_allocation->document_no;
                            $c_bpartner_id                  = $auto_allocation->c_bpartner_id;
                            $warehouse_id                   = $auto_allocation->warehouse_id;
                            $supplier_name                  = $auto_allocation->supplier_name;
                            $item_id                        = $auto_allocation->item_id_source;
                            $item_code                      = $auto_allocation->item_code_source;
                            $item_desc                      = $auto_allocation->item_desc;
                            $category                       = $auto_allocation->category;
                            $uom                            = $auto_allocation->uom;
    
                            if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK') $supplier_code = 'FREE STOCK';
                            else
                            {
                                $supplier                       = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                if($supplier) $supplier_code    = $supplier->supplier_code;
                                else $supplier_code             = null;
                            }
    
                            if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK')
                            {
                                $c_bpartner_id                  = 'FREE STOCK';
                                $supplier_code                  = 'FREE STOCK';
                
                                $mapping_stock_id               = null;
                                $type_stock_erp_code            = '2';
                                $type_stock                     = 'REGULER';
                                
                            }else
                            {
                                
                                $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                if($_supplier) $supplier_code   = $_supplier->supplier_code;
                                else $supplier_code             = null;
                
                                $get_4_digit_from_beginning     = substr($document_no,0, 4);
                                $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                                
                                if($_mapping_stock_4_digt)
                                {
                                    $mapping_stock_id           = $_mapping_stock_4_digt->id;
                                    $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                                    $type_stock                 = $_mapping_stock_4_digt->type_stock;
                                }else
                                {
                                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                                    $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                    if($_mapping_stock_5_digt)
                                    {
                                        $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                                    }else
                                    {
                                        $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
                                        $mapping_stock_id       = null;
                                        $type_stock_erp_code    = ($_po_buyer)? $_po_buyer->type_stock_erp_code : NULL;
                                        $type_stock             = ($_po_buyer)? $_po_buyer->type_stock : null;
                                    }
                                }
                            }
    
                            $check_material_stock = MaterialStock::where([
                                ['c_order_id',$c_order_id],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['item_code',$item_code],
                                ['warehouse_id',$warehouse_id],
                                ['is_stock_on_the_fly',true],
                                ['is_running_stock',false],
                                ['is_closing_balance',false],
                                ['stock',$qty_reduce],
                                ['type_stock_erp_code',$type_stock_erp_code],
                            ])
                            ->whereNull('deleted_at');
    
                            if($po_buyer) $check_material_stock = $check_material_stock->where('po_buyer',$po_buyer);
                            $check_material_stock = $check_material_stock->exists();
    
                            if(!$check_material_stock)
                            {
                                $material_stock = MaterialStock::FirstOrCreate([
                                    'type_stock_erp_code'   => $type_stock_erp_code,
                                    'type_stock'            => $type_stock,
                                    'supplier_code'         => $supplier_code,
                                    'supplier_name'         => $supplier_name,
                                    'document_no'           => $document_no,
                                    'c_bpartner_id'         => $c_bpartner_id,
                                    'c_order_id'            => $c_order_id,
                                    'item_id'               => $item_id,
                                    'item_code'             => $item_code,
                                    'po_buyer'              => ($po_buyer ? $po_buyer : null),
                                    'item_desc'             => $item_desc,
                                    'category'              => $category,
                                    'type_po'               => 2,
                                    'warehouse_id'          => $warehouse_id,
                                    'uom'                   => $uom,
                                    'qty_carton'            => 1,
                                    'stock'                 => $qty_reduce,
                                    'remark'                => 'REDUCE',
                                    'reserved_qty'          => 0,
                                    'created_at'            => $upload_date,
                                    'updated_at'            => $upload_date,
                                    'deleted_at'            => null,
                                    'is_closing_balance'    => false,
                                    'is_active'             => true,
                                    'deleted_at'            => null,
                                    'is_stock_on_the_fly'   => true,
                                    'source'                => 'STOCK ON THE FLY DUE REDUCE',
                                    'user_id'               => $user->id
                                ]);
    
                                HistoryStock::approved($material_stock->id
                                ,$material_stock->stock
                                ,'0'
                                ,null
                                ,null
                                ,null
                                ,null
                                ,null
                                ,'SOTF DUE REDUCE'
                                ,$user->name
                                ,$user->id
                                ,$type_stock_erp_code
                                ,$type_stock
                                ,false
                                ,false);
                            }
                        }
                    }
                    
                    $auto_allocation->is_reduce         = true;

                    if($qty_reduce > 0)
                    {
                        MaterialPreparation::where('auto_allocation_id',$auto_allocation->id)
                        ->update([
                            'is_reduce' => true
                        ]);
                    }

                }

                $total_required -= $new_qty_need;
                $auto_allocation->save();
                
            }

        }
    }

    public function uploadManualexport()
    {
        return Excel::create('upload_manual_buyer',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','STYLE');
                $sheet->setCellValue('D1','ARTICLE');
                $sheet->setCellValue('E1','QTY_REQUIRED');
                $sheet->setCellValue('F1','IS_CARTON');

                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);
                $sheet->setWidth('E', 15);
                $sheet->setWidth('F', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                ));
            
            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function importUploadManual(Request $request)
    {
        if($request->hasFile('upload_manual_file')){
            $validator = Validator::make($request->all(), [
                'upload_manual_file' => 'required|mimes:xls'
            ]);
          
            $path = $request->file('upload_manual_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();
            if(!empty($data) && $data->count())
            {
                
                $carbon = Carbon::now();
                $code   = "MR-".Carbon::parse($carbon)->format('ymd').Carbon::now()->format('u');
                //return response()->json($data);
                try 
            {
                
            DB::beginTransaction();
            
                foreach ($data as $key => $value) {
                    $po_buyer       = trim($value->po_buyer);
                    $material_requirement =MaterialRequirement::where('po_buyer', $po_buyer)
                                      ->whereNotNull('warehouse_name')
                                      ->first();
                    $warehouse_name = ($material_requirement->warehouse_name) ? $material_requirement->warehouse_name : null;
                    $item_code      = trim($value->item_code);
                    $article        = trim($value->article);
                    $style          = trim($value->style);
                    $job            = $style.'::'.$po_buyer.'::'.$article;
                    $qty_required   = trim($value->qty_required);
                    $is_carton      = trim($value->is_carton);

                    if($is_carton==true){
                        $item_wms = Item::where('alias',$item_code)->first();

                        if(!$item_wms) return response()->json($item_code.' Item not found.',422);
                        $item_code = $item_wms->item_code;
                    }

                    $list_po_buyer[] = $po_buyer;
                    $_po_buyer = PoBuyer::
                    select(
                        'lc_date',
                        'statistical_date',
                        'promise_date',
                        'season'
                    )
                    ->where('po_buyer',$po_buyer)
                    ->first();

                    if(!$_po_buyer) return response()->json($po_buyer.'Po buyer not found.',422); 

                    $item = Item::
                    select(
                        'item_desc',
                        'uom',
                        'category',
                        'composition',
                        'item_id'
                    )
                    ->where('item_code',$item_code)
                    ->first();
                   
                    MaterialRequirement::where([
                        ['po_buyer',$po_buyer],
                        ['item_code',$item_code],
                        //['style',$style],
                        //['article_no',$article],
                        ['is_upload_manual',true],
                    ])
                    ->delete();

                    $material_requirement = MaterialRequirement::create([
                        'po_buyer'         => $po_buyer,
                        'item_code'        => $item_code,
                        'item_desc'        => $item->item_desc,
                        'job_order'        => $job,
                        'style'            => $style,
                        '_style'           => $style,
                        'uom'              => $item->uom,
                        'category'         => $item->category,
                        'qty_required'     => $qty_required,
                        'statistical_date' => $_po_buyer->statistical_date,
                        'article_no'       => $article,
                        'season'           => $_po_buyer->season,
                        'lc_date'          => $_po_buyer->lc_date,
                        'component'        => $item->composition,
                        'promise_date'     => $_po_buyer->promise_date,
                        'item_id'          => $item->item_id,
                        'is_upload_manual' => true,
                        'batch_code'       => $code,
                        'warehouse_name'   => $warehouse_name
                    ]);
                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            }


            return response()->json(
            [   'code' => $code, 
                'success', 
                200,
                'url' => route('erpMaterialRequirement.index',['_po_buyer'=> $list_po_buyer]),
            ]);
            return ;
        }
    }

    //nggak kepake
    public function recalculate(Request $request)
    {
        //return view('errors.503');
        // if ($request->ajax()) 
        // { 
            if($request->has('batch_code'))
            {
                
                $batch_code              = $request->batch_code;
                
                return Excel::create('recalculate_'.$batch_code,function ($excel) use($batch_code){
                    
                    // $excel->sheet('active', function($sheet) use($batch_code){
                    //     $sheet->setCellValue('A1','PO_BUYER');
                    //     $sheet->setCellValue('B1','ITEM_CODE');
                    //     $sheet->setCellValue('C1','QTY REQUIRED');
                    //     $sheet->setCellValue('D1','QTY ALLOCATED');
                    //     $sheet->setCellValue('E1','PR ORDER');
                    //     $sheet->setWidth('A', 15);
                    //     $sheet->setColumnFormat(array(
                    //         'A' => '@',
                    //         'B' => '@',
                    //         'C' => '@',
                    //         'D' => '@',
                    //         'E' => '@',
                    //     ));
                    // });
                    $excel->sheet('list_recalculate', function($sheet) use($batch_code){

                        // dd($batch_code);
                        $material_requirements = MaterialRequirement::where('batch_code',$batch_code)
                        // ->where('category','CT')
                        ->orderby('item_code','asc')
                        ->orderby('promise_date','asc')
                        ->orderby('qty_required','asc')
                        ->get();

                        $sheet->setCellValue('A1','PO BUYER');
                        $sheet->setCellValue('B1','ITEM CODE');
                        $sheet->setCellValue('C1','QTY REQUIRED');
                        $sheet->setCellValue('D1','QTY ALLOCATED');
                        $sheet->setCellValue('E1','PR ORDER');
                        $sheet->setCellValue('F1','WAREHOUSE');
        
                        $index       = 0;
                        $kode_karton = '';
                        $kuota_stock = 0;

                        foreach ($material_requirements as $key => $mr) {
                            
                            $item_code  = $mr->item_code;
                            $po_buyer   = $mr->po_buyer;
                            $dibutuhkan = sprintf('%0.8f',$mr->qty_required);

                            $summary_material_requirements = DetailMaterialRequirement::where('po_buyer', $po_buyer)
                                                             ->first();
                            //dd($po_buyer, $summary_material_requirements);
                            $warehouse_erp = $summary_material_requirements->warehouse_id;
                            //dd($warehouse_erp);
                            if($warehouse_erp == '1000007')
                            {
                                $warehouse_id = '1000002';
                                $warehouse_name = 'AOI 1';
                            }
                            elseif($warehouse_erp == '1000010')
                            {
                                $warehouse_id = '1000013';
                                $warehouse_name = 'AOI 2';
                            }
                             
                            //CEK PER WAREHOUSE
                            $material_stock = MaterialStock::select('item_code',db::raw('sum(available_qty) as qty'))
                            ->where('item_code',$item_code)
                            ->where('warehouse_id',$warehouse_id)
                            ->where('deleted_at',null)
                            ->where('available_qty','>','0')
                            ->where('is_running_stock', false)
                            ->groupby('item_code')
                            ->first();

                            if($kode_karton != $mr->item_code){
                                $kuota_stock = 0;
                                $stock       = sprintf('%0.8f',$material_stock->qty);
                                
                            }else{
                                $kuota_stock = $kuota_stock;
                                $stock       = $kuota_stock;
                            }   

                            $kuota = $stock - $dibutuhkan;
                            if($kuota>=0){
                                $hasil = $dibutuhkan;
                                $PR = 0;
                            }else{
                                $hasil = $stock;
                                $PR = $kuota;
                            }

                            if($hasil<=0){
                                $hasil=0;
                            }

                            // if($kuota >= 0){
                            //     $PR = 0;
                            // }else{
                            //     $PR = $kuota;
                            // }

                            $row = $index + 2;
                            $sheet->setCellValue('A'.$row, $po_buyer);
                            $sheet->setCellValue('B'.$row, $item_code);
                            $sheet->setCellValue('C'.$row, $mr->qty_required);
                            $sheet->setCellValue('D'.$row, $hasil);
                            $sheet->setCellValue('E'.$row, abs($PR));
                            $sheet->setCellValue('F'.$row, $warehouse_name);

                            $kode_karton = $mr->item_code;
                            $kuota_stock = $kuota;
                            $index++;
                        }
                    });
        
                    $excel->setActiveSheetIndex(0);
        
                })->export('xlsx');

                // return response()->json(
                // [
                //     'excel' => $excel,
                // ]
                // );
                
            }
        // } 
    }

    public function syncPerItem(Request $request)
    {
        return view('erp_material_requirement.sync_per_item');
    }

    public function exportSyncPerItem()
    {
        return Excel::create('upload_sync_per_item',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }
     
    public function importsyncPerItem(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                $movement_date = carbon::now();
                foreach ($data as $key => $value) 
                {
                    $po_buyer  = $value->po_buyer;
                    $item_code = $value->item_code;

                    $is_cancel = PoBuyer::where('po_buyer',$po_buyer)
                    ->whereNotNull('cancel_user_id')
                    ->exists();

                    $reroute = ReroutePoBuyer::where('old_po_buyer',trim($po_buyer))->exists();
                
                    if($is_cancel)
                    {
                        $obj                    = new stdClass();
                        $obj->lc_date           = null;
                        $obj->statistical_date  = null;
                        $obj->season            = null;
                        $obj->po_buyer          = $po_buyer;
                        $obj->item_code         = $item_code;
                        $obj->style             = null;
                        $obj->article_no        = null;
                        $obj->uom               = null;
                        $obj->warehouse_packing = null;
                        $obj->qty_required      = null;
                        $obj->error_upload      = true;
                        $obj->remark       = 'PO INI CANCEL';
                        $array []          = $obj;

                    }
                    else
                    {
                        if($reroute)
                        {
                            $obj                    = new stdClass();
                            $obj->lc_date           = null;
                            $obj->statistical_date  = null;
                            $obj->season            = null;
                            $obj->po_buyer          = $po_buyer;
                            $obj->item_code         = $item_code;
                            $obj->style             = null;
                            $obj->article_no        = null;
                            $obj->uom               = null;
                            $obj->warehouse_packing = null;
                            $obj->qty_required      = null;
                            $obj->error_upload      = true;
                            $obj->remark            = 'PO INI CANCEL';
                            $array []               = $obj;

                        }
                        else
                        {
                            try 
                            {
                                DB::beginTransaction();

                                $this->getPobuyer($po_buyer, $item_code);
                                $this->getSummaryBuyerPerPart($po_buyer, $item_code);
                                $create_material_requirements = $this->getSummaryBuyer($po_buyer, $item_code);

                                Temporary::FirstOrCreate([
                                    'barcode'       => $po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => 1,
                                    'created_at'    => $movement_date,
                                    'updated_at'    => $movement_date,
                                ]);


                                DB::commit();
                            } catch (Exception $e) 
                            {
                                DB::rollBack();
                                $message = $e->getMessage();
                                ErrorHandler::db($message);
                            }

                            $obj                    = new stdClass();
                            //dd($create_material_requirements);
                            if($create_material_requirements != null)
                            {
                                foreach($create_material_requirements as $key => $create_material_requirement)
                                {
                                    $obj->lc_date           = $create_material_requirement->lc_date;
                                    $obj->statistical_date  = $create_material_requirement->statistical_date;
                                    $obj->season            = $create_material_requirement->season;
                                    $obj->po_buyer          = $create_material_requirement->po_buyer;
                                    $obj->item_code         = $create_material_requirement->item_code;
                                    $obj->style             = $create_material_requirement->style;
                                    $obj->article_no        = $create_material_requirement->article_no;
                                    $obj->uom               = $create_material_requirement->uom;
                                    $obj->warehouse_packing = $create_material_requirement->warehouse_name;
                                    $obj->qty_required      = $create_material_requirement->qty_required;
                                    $obj->error_upload      = false;
                                    $obj->remark            = 'SUCCESS, SYNC BERHASIL';
                                    $array []                      = $obj;
                                }
                            }
                            else
                            {
                                $obj->lc_date           = null;
                                $obj->statistical_date  = null;
                                $obj->season            = null;
                                $obj->po_buyer          = $po_buyer;
                                $obj->item_code         = $item_code;
                                $obj->style             = null;
                                $obj->article_no        = null;
                                $obj->uom               = null;
                                $obj->warehouse_packing = null;
                                $obj->qty_required      = null;
                                $obj->error_upload      = true;
                                $obj->remark            = 'FAILED, BOM TIDAK DITEMUKAN';
                                $array []                      = $obj;
                            }
                        }
                    }
                }
            }
        }
        return response()->json($array,'200');

    }

    static function SyncMaterialRequirementNotExists()
    {
        $bom_not_exists = db::table('alokasi_vs_bom_v')->take(10)->get();

        foreach($bom_not_exists as $key => $bom_not_exist)
        {
                $po_buyer  = $value->po_buyer;
                $item_code = $value->item_code;

                $is_cancel = PoBuyer::where('po_buyer',$po_buyer)
                ->whereNotNull('cancel_user_id')
                ->exists();

                $reroute = ReroutePoBuyer::where('old_po_buyer',trim($po_buyer))->exists();
            
                if($is_cancel)
                {
                    continue;

                }
                else
                {
                    if($reroute)
                    {
                        continue;

                    }
                    else
                    {
                        try 
                        {
                            DB::beginTransaction();

                            ErpMaterialRequirementController::getPobuyer($po_buyer, $item_code);
                            ErpMaterialRequirementController::getSummaryBuyerPerPart($po_buyer, $item_code);
                            $create_material_requirements = ErpMaterialRequirementController::getSummaryBuyer($po_buyer, $item_code);

                            Temporary::FirstOrCreate([
                                'barcode'       => $po_buyer,
                                'status'        => 'mrp',
                                'user_id'       => 1,
                                'created_at'    => $movement_date,
                                'updated_at'    => $movement_date,
                            ]);


                            DB::commit();
                        } catch (Exception $e) 
                        {
                            DB::rollBack();
                            $message = $e->getMessage();
                            ErrorHandler::db($message);
                        }
                    }
                }
            }
    }

}

