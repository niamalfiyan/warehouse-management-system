<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use stdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Item;
use App\Models\MaterialExclude;

class MasterDataMaterialPrintBomController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_material_print_bom.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $material_print_boms = MaterialExclude::orderBy('created_at','desc');

            return DataTables::of($material_print_boms)
            ->editColumn('is_ila',function ($material_print_boms){
                if ($material_print_boms->is_ila) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->editColumn('is_from_buyer',function ($material_print_boms){
                if ($material_print_boms->is_from_buyer) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->editColumn('is_paxar',function ($material_print_boms){
                if ($material_print_boms->is_paxar) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->addColumn('action', function($material_print_boms) {
                return view('master_data_defect._action', [
                    'model'     => $material_print_boms,
                    'edit'      => route('masterDataMaterialPrintBom.edit',$material_print_boms->id),
                    'delete'    => route('masterDataMaterialPrintBom.delete',$material_print_boms->id),
                ]);
            })
            ->rawColumns(['action','is_paxar','is_from_buyer','is_ila'])
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_material_print_bom.create');
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_code' => 'required',
        ]);

        $item_code = strtoupper(trim($request->item_code));
        $is_exists = MaterialExclude::where('item_code',$item_code)->exists();
        $item      = Item::where('item_code',$item_code)->first();

        if($is_exists) return response()->json(['message' => 'Data already exists.'], 422);

        if(!$item) return response()->json(['message' => 'Item not found in master data, please sync first.'], 422);

        if(Session::has('flag')) Session::forget('flag');

        try 
        {
            DB::beginTransaction();

            $material_exclude = MaterialExclude::FirstOrCreate([
                'item_code'         => $item_code,
                'category'          => $item->category,
                'is_ila'            => ($request->exists('is_ila')) ? true:false,
                'is_from_buyer'     => ($request->exists('is_from_buyer')) ? true:false,
                'is_paxar'          => ($request->exists('is_paxar')) ? true:false,
                'user_id'           => auth::user()->id
            ]);

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        Session::flash('flag', 'success');
        return response()->json('success', 200);
    }

    public function edit($id)
    {
        $material_print_bom = MaterialExclude::find($id);
        return view('master_data_material_print_bom.edit',compact('material_print_bom'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'item_code' => 'required',
        ]);

        $item_code = strtoupper(trim($request->item_code));
        $is_exists = MaterialExclude::where([
            ['item_code',$item_code],
            ['id','!=',$id],
        ])
        ->exists();
        $item      = Item::where('item_code',$item_code)->first();

        if($is_exists) return response()->json(['message' => 'Data already exists.'], 422);

        if(!$item) return response()->json(['message' => 'Item not found in master data, please sync first.'], 422);

        if(Session::has('flag')) Session::forget('flag');

        try 
        {
            DB::beginTransaction();
            
            $material_exclude = MaterialExclude::find($id);
            $material_exclude->item_code        = $item_code;
            $material_exclude->category         = $item->category;
            $material_exclude->is_ila           = ($request->exists('is_ila')) ? true:false;
            $material_exclude->is_from_buyer    = ($request->exists('is_from_buyer')) ? true:false;
            $material_exclude->is_paxar         = ($request->exists('is_paxar')) ? true:false;
            $material_exclude->save();

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        Session::flash('flag', 'success_2');
        return response()->json('success', 200);
    }

    public function import()
    {
        return view('master_data_material_print_bom.import');
    }

    public function downloadFileImport()
    {
        return Excel::create('upload_material_print_bom',function ($excel)
        {
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','ITEM_CODE');
                $sheet->setCellValue('B1','IS_ILA');
                $sheet->setCellValue('C1','IS_FROM_BUYER');
                $sheet->setCellValue('D1','IS_PAXAR');
 
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 10);
                $sheet->setWidth('D', 10);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })->export('xlsx');
    }

    public function uploadFromImport(Request $request)
    {
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);
  
            $path   = $request->file('upload_file')->getRealPath();
            $data   = Excel::selectSheets('active')->load($path,function($render){})->get();
            $result = array();
            
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    
                    foreach($data as $key => $value)
                    {
                        $item_code      = strtoupper(trim($value->item_code));
                        $is_exists      = MaterialExclude::where('item_code',$item_code)->exists();
                        $item           = Item::where('item_code',$item_code)->first();
                        
                        $is_ila         = ($value->is_ila=='TRUE'   || $value->is_ila=='T' || $value->is_ila=='true' || $value->is_ila=='t' ? true:false);
                        $is_from_buyer  = ($value->is_from_buyer=='TRUE'   || $value->is_from_buyer=='T' || $value->is_from_buyer=='true' || $value->is_from_buyer=='t' ? true:false);
                        $is_paxar       = ($value->is_paxar=='TRUE'   || $value->is_paxar=='T' || $value->is_paxar=='true' || $value->is_paxar=='t' ? true:false);
                        
                        if(!$is_exists)
                        {
                            if($item)
                            {
                                $total_chacked = $is_ila + $is_from_buyer + $is_paxar;
                                if($total_chacked > 0)
                                {

                                    $material_exclude = MaterialExclude::FirstOrCreate([
                                        'item_code'         => $item_code,
                                        'category'          => $item->category,
                                        'is_ila'            => $is_ila,
                                        'is_from_buyer'     => $is_from_buyer,
                                        'is_paxar'          => $is_paxar,
                                        'user_id'           => auth::user()->id
                                    ]);
                                
                                    $obj                = new stdClass();
                                    $obj->item_code     = $item_code;
                                    $obj->category      = $item->category;
                                    $obj->is_ila        = ($is_ila ? $is_ila : null);
                                    $obj->is_paxar      = ($is_paxar ? $is_paxar : null);
                                    $obj->is_from_buyer = ($is_from_buyer ? $is_from_buyer : null);
                                    $obj->is_error      = false;
                                    $obj->status        = 'success';
                                    $result [] = $obj;
                                }else
                                {
                                    $obj                = new stdClass();
                                    $obj->item_code     = $item_code;
                                    $obj->category      = null;
                                    $obj->is_ila        = ($is_ila ? $is_ila : null);
                                    $obj->is_paxar      = ($is_paxar ? $is_paxar : null);
                                    $obj->is_from_buyer = ($is_from_buyer ? $is_from_buyer : null);
                                    $obj->is_error      = true;
                                    $obj->status        = 'Silahkan pilih tipe salah satunya';
                                    $result [] = $obj;
                                }
                            }else
                            {
                                $obj                = new stdClass();
                                $obj->item_code     = $item_code;
                                $obj->category      = null;
                                $obj->is_ila        = ($is_ila ? $is_ila : null);
                                $obj->is_paxar      = ($is_paxar ? $is_paxar : null);
                                $obj->is_from_buyer = ($is_from_buyer ? $is_from_buyer : null);
                                $obj->is_error      = true;
                                $obj->status        = 'item tidak ditemukan';
                                $result [] = $obj;
                            }
                        }else
                        {
                            $obj                = new stdClass();
                            $obj->item_code     = $item_code;
                            $obj->category      = null;
                            $obj->is_ila        = ($is_ila ? $is_ila : null);
                            $obj->is_paxar      = ($is_paxar ? $is_paxar : null);
                            $obj->is_from_buyer = ($is_from_buyer ? $is_from_buyer : null);
                            $obj->is_error      = true;
                            $obj->status        = 'data sudah ada';
                            $result [] = $obj;
                        }
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                return response()->json($result,200);
            }
            
            return response()->json($result,200);
        }
        return response()->json('Import failed, please check again',422);
    }

    public function destroy($id)
    {
        MaterialExclude::where('id',$id)->delete();
        return response()->json(200);
    }
}
