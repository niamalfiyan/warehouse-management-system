<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Role;

class MasterDataUserController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_user.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        { 
            $users = User::whereNull('deleted_at')
            ->orderBy('created_at','desc');

            return DataTables::of($users)
            ->addColumn('action', function($users)
            { 
                return view('master_data_user._action', 
                [
                    'edit'      => route('masterDataUser.edit', $users->id),
                    'delete'    => route('masterDataUser.delete', $users->id),
                    'reset'     => route('masterDataUser.resetPassword', $users->id)
                ]);
            })    
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_user.create');
    }

    public function employeePickList(Request $request)
    {
        $q              = trim(strtoupper($request->q));
        $lists          = DB::connection('absence_aoi')
        ->table('get_employee') 
        ->select('nik','name','department_name')
        ->where('nik','LIKE',"%$q%")
        ->orwhere('name','LIKE',"%$q%")
        ->paginate(10);
        
        
        return view('master_data_user._employee_list',compact('lists'));
    }

    public function rolePickList(Request $request)
    {
        $q              = trim(strtoupper($request->q));
        $lists          = Role::orderby('created_at','desc')
        ->where('name','LIKE',"%$q%")
        ->orwhere('display_name','LIKE',"%$q%")
        ->paginate(10);
        
        return view('master_data_user._role_list',compact('lists'));
    }

    public function accountSetting($id)
    {

        $user = User::find($id); 
        return view('user.account_setting') 
            ->with('user',$user); 
    } 

    public function store(Request $request)
    {
        if(Session::has('flag')) Session::forget('flag');

        $this->validate($request, [
            'name' => 'required|min:3',
            'nik' => 'required',
            'warehouse' => 'required',
        ]);

        $roles      =  json_decode($request->roles);
        $role_array = array();

        foreach ($roles as $key => $value)  $role_array [] = [ 'id' => $value->id ];
        

        if($request->warehouse == '1000002' || $request->warehouse == '1000013' || $request->warehouse == '1000001' || $request->warehouse == '1000011') $application = 'WAREHOUSE';
        else if($request->warehouse == '1000005' || $request->warehouse == '1000012') $application = 'CUTTING';

        $user = User::firstorCreate([
            'name'          => strtolower($request->name),
            'nik'           => $request->nik,
            'sex'           => $request->sex,
            'application'   => $application,
            'warehouse'     => $request->warehouse,
            'department'    => strtolower($request->department),
            'email'         => $request->nik.'dummy@aoi.co.id',
            'password'      => bcrypt('password1'),
        ]);

        if($user->save()) $user->attachRoles($role_array);
        

        Session::flash('flag', 'success');
        return response()->json(200);
    }

    public function edit($id)
    {
        $user = User::find($id);
        $user_roles     = $user->roles()->get();
        $mappings       = array();

        foreach ($user_roles as $key => $user_role) 
        {
            $obj           = new stdClass;
            $obj->id       = $user_role->id;
            $obj->name     = $user_role->name;
            $mappings []   = $obj;
        }

        return view('master_data_user.edit',compact('user','mappings'));
    }

    public function dataRole(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data = db::select(db::raw("select roles.id as id
            ,roles.display_name
            from roles 
            join role_user on role_user.role_id = roles.id 
            where role_user.user_id = '".$id."'
            "));

            return DataTables::of($data)
            ->addColumn('action', function($data)use($id)
            {
                return view('master_data_user._action_modal', [
                    'model' => $data,
                    'delete' => route('masterDataUser.deleteRoleUser',[$id,($data)?$data->id : null]),
                ]);

               
            })
            ->make(true);
        }
        
    }
    
    public function storeRole(Request $request)
    {
        $user = User::find($request->user_id);
        $user->attachRoles([$request->role_id]);
        return response()->json(200);
    }
    
    public function update(Request $request, $id)
    {
        if(Session::has('flag'))
            Session::forget('flag');

        $this->validate($request, [
            'name' => 'required|min:3',
            'nik' => 'required',
        ]);

        $roles      =  json_decode($request->roles);
        $role_array = array();
        foreach ($roles as $key => $value) $role_array [] = [ 'id' => $value->id];
        

        if($request->warehouse == '1000002' || $request->warehouse == '1000013' || $request->warehouse == '1000001' || $request->warehouse == '1000011') $application = 'WAREHOUSE';
        else if($request->warehouse == '1000005' || $request->warehouse == '1000012') $application = 'CUTTING';

        $user = User::find($id);
        $user->nik          = $request->nik;
        $user->application  = $application;
        $user->warehouse    = $request->warehouse;
        $user->sex          = $request->sex;
        
        if($user->update())
        {
            $user->roles()->sync([]);
            $user->attachRoles($role_array);
        }
        
        Session::flash('flag', 'success_2');
        return response()->json(200);

    }

    public function resetPassword($id)
    {
        $user = User::findorFail($id);
        $user->password = bcrypt('password1');
        $user->save();
        return response()->json(200);
    }

    
    public function updatepassword(Request $request, $id){ 
        $validator =  Validator::make($request->all(), [ 
            'password_new' => 'required', 
            'password_confirm' => 'required|same:password_new', 
        ]); 
        if ($validator->passes()) { 
            try{ 
                db::beginTransaction(); 
                $user = User::find($id); 
                $user->password = bcrypt($request->password_new); 
 
                if($user->update()){ 
                    db::commit(); 
                    return response()->json('success', 200); 
                } 
                 
            }catch (Exception $ex){ 
                db::rollback(); 
                $message = $ex->getMessage(); 
                ErrorHandler::db($message);  
            } 
            
        }else{ 
            return response()->json([ 
                'errors' => $validator->getMessageBag()->toArray() 
            ],400);  
        } 
    } 

    public function destroy($id)
    {
        $user               = User::find($id); 
        $user->deleted_at   = carbon::now();
        $user->save();

        return response()->json(200);
    }

    public function destroyRoleUser($user_id,$role_id)
    {
        $user = User::find($user_id);
        $roles = $user->roles()->where('role_id','!=',$role_id)->get();
        $array = array();
        foreach ($roles as $key => $role) 
        {
            $array [] = $role->id;
        }
        
        $user->roles()->sync([]);
        $user->attachRoles($array);

        return response()->json($roles);
    }
}
