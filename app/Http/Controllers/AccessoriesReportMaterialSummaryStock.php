<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

class AccessoriesReportMaterialSummaryStock extends Controller
{
    public function index()
    {
        return view('accessories_report_material_summary_stock.index');
    }

    public function data(Request $request)
    {
        //dd($request->warehouse);
        if(request()->ajax())
        {
            $warehouse_id       = $request->warehouse;

            if($warehouse_id == null || $warehouse_id == '')
            {
                $summary_stocks = db::table('summary_stock_accessories_v')
                ->orderby('item_code','asc');
            }
            else
            {
                $summary_stocks = db::table('summary_stock_accessories_v')
                ->where('warehouse_id',$warehouse_id)
                ->orderby('item_code','asc');
            }

            return DataTables::of($summary_stocks)
            ->editColumn('total_per_item',function ($summary_stocks)
            {
                return number_format($summary_stocks->total_per_item, 4, '.', ',');
            })
            ->addColumn('action',function($summary_stock){
                return view('accessories_report_material_summary_stock._action',[
                    'model'             => $summary_stock,
                    'detail'            => route('accessoriesReportMaterialSummaryStock.detail',['item_id'=>$summary_stock->item_id, 'warehouse_id'=>$summary_stock->warehouse_id]),
                ]);
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = ($request->_warehouse_id ?  $request->_warehouse_id : 'all');
        
        if($warehouse_id == 'all') $warehouse_name = 'all';
        else ($warehouse_id == '1000002' ? $warehouse_name = 'warehouse_acc_aoi_1': $warehouse_name = 'warehouse_acc_aoi_2' );
        

        $material_stocks      = DB::table('summary_stock_accessories_v');
        
        if($warehouse_id != 'all') $material_stocks = $material_stocks->where('warehouse_id','LIKE',"%$warehouse_id%");
        
        $material_stocks = $material_stocks->orderby('item_code','asc')
        ->get();

        $file_name = 'summary_material_stock_per_item_'.$warehouse_name;
        return Excel::create($file_name,function($excel) use ($material_stocks)
        {
            $excel->sheet('active',function($sheet)use($material_stocks)
            {
                $sheet->setCellValue('A1','WAREHOUSE');
                $sheet->setCellValue('B1','ITEM CODE');
                $sheet->setCellValue('C1','CATEGORY');
                $sheet->setCellValue('D1','UOM');
                $sheet->setCellValue('E1','TOTAL QTY');

            
            $row=2;
            foreach ($material_stocks as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->warehouse);
                $sheet->setCellValue('B'.$row,$i->item_code);
                $sheet->setCellValue('C'.$row,$i->category);
                $sheet->setCellValue('D'.$row,$i->uom);
                $sheet->setCellValue('E'.$row,$i->total_per_item);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

    public function exportDetail(Request $request)
    {
        $warehouse_id       = ($request->_warehouse_id ?  $request->_warehouse_id : 'all');

        if($warehouse_id == 'all') $warehouse_name = 'all';
        else ($warehouse_id == '1000002' ? $warehouse_name = 'warehouse_acc_aoi_1': $warehouse_name = 'warehouse_acc_aoi_2' );
        
        $detail_material_stocks  = DB::table('gn_detail_stock_per_item_acc_v');
        
        if($warehouse_id != 'all') $detail_material_stocks = $detail_material_stocks->where('warehouse_id','LIKE',"%$warehouse_id%");
        
        $detail_material_stocks = $detail_material_stocks
        ->where('qty','>','0')
        ->orderby('item_code','asc')
        ->get();

        $file_name = 'detail_material_stock_per_item_'.$warehouse_name;
        return Excel::create($file_name,function($excel) use ($detail_material_stocks)
        {
            $excel->sheet('active',function($sheet)use($detail_material_stocks)
            {
                $sheet->setCellValue('A1','WAREHOUSE');
                $sheet->setCellValue('B1','LOCATOR');
                $sheet->setCellValue('C1','BARCODE');
                $sheet->setCellValue('D1','DOCUMENT_NO');
                $sheet->setCellValue('E1','PO_BUYER');
                $sheet->setCellValue('F1','ITEM_CODE');
                $sheet->setCellValue('G1','CATEGORY');
                $sheet->setCellValue('H1','UOM');
                $sheet->setCellValue('I1','TOTAL QTY');
                $sheet->setCellValue('J1','STATUS STOCK');
                $sheet->setCellValue('K1','LAST UPDATE');

            
            $row=2;
            foreach ($detail_material_stocks as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->warehouse);
                $sheet->setCellValue('B'.$row,$i->locator);
                $sheet->setCellValue('C'.$row,$i->barcode);
                $sheet->setCellValue('D'.$row,$i->document_no);
                $sheet->setCellValue('E'.$row,$i->po_buyer);
                $sheet->setCellValue('F'.$row,$i->item_code);
                $sheet->setCellValue('G'.$row,$i->category);
                $sheet->setCellValue('H'.$row,$i->uom);
                $sheet->setCellValue('I'.$row,$i->qty);
                $sheet->setCellValue('J'.$row,$i->status_stock);
                $sheet->setCellValue('K'.$row,$i->last_transaction);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }

    public function detail(Request $request)
    {
        $warehouse_id = $request->warehouse_id;
            $item_id      = $request->item_id;
        $summary_stock_acc =  db::table('summary_stock_accessories_v')
        ->where('warehouse_id','LIKE',"%$warehouse_id%")
        ->where('item_id', $item_id)
        ->first();
        return view('accessories_report_material_summary_stock.detail',compact('summary_stock_acc'));
    }

    public function dataDetail(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id = $request->warehouse_id;
            $item_id      = $request->item_id;

            //dd($warehouse_id, $item_id);
            
            $detail_material_stocks = db::table('detail_stock_per_item_acc_v')
            ->where([
                ['warehouse_id',$warehouse_id],
                ['item_id', $item_id],
                ['qty','>','0'],
            ])
            ->orderby('qty','desc')
            ->get();
            
            return DataTables::of($detail_material_stocks)
            ->editColumn('qty',function ($detail_material_stocks)
            {
                return number_format($detail_material_stocks->qty, 4, '.', ',');
            }) 
            ->make(true);
        }
    }
}
