<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Rma;
use App\Models\Locator;
use App\Models\UomConversion;
use App\Models\MaterialCheck;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialPreparation;

class AccessoriesMaterialMetalDetectorController extends Controller
{
    public function index()
    {
        return view('accessories_material_metal_detector.index');
    }

    public function create(request $request)
    {
        $barcode        = trim($request->barcode);
        $_warehouse_id  = $request->warehouse_id;
        $has_dash       = strpos($barcode, "-");

        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
        }

        $material_preparation = MaterialPreparation::where([
            ['barcode',strtoupper($_barcode)],
            ['warehouse',$_warehouse_id],
            //['category', 'ZP']
        ])
        ->first();

        $total_receive = $material_preparation->qty_conversion;

        $is_exists = MaterialCheck::where('barcode_preparation',$_barcode)
        ->where('is_from_metal_detector', false)
        ->whereNull('deleted_at')
        ->exists();

        $material_check = MaterialCheck::where('barcode_preparation',$_barcode)
        ->where('is_from_metal_detector', true)
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found.',422);
        
        if(!$is_exists) return response()->json('Barcode belum discan QC.',422);

        if($material_check){
            if($material_check->status != 'HOLD' && $material_check->status != 'QUARANTINE')
             return response()->json('Barcode already scanned',422); 
        }
        
        if($material_preparation->last_status_movement == 'in' ||  $material_preparation->last_status_movement == 'change' )
        {
            $destination = Locator::find($material_preparation->last_locator_id);
            return response()->json('This data still in locator, please checkout first to qc. (Barang ini masih ada di rak '.$destination->code.'. Silahkan dikeluarkan ke QC)',422);
        }

        if($material_preparation->deleted_at <> null){
            $destination = Locator::find($material_preparation->last_locator_id);
            return response()->json('Barcode already supplied to '.$destination->code,422); 
        }
            

        $material_requirement = $material_preparation->dataRequirement($material_preparation->po_buyer,$material_preparation->item_code,$material_preparation->style,$material_preparation->article_no);
        $qty_need             = ($material_requirement) ? $material_requirement->qty_required : '0';
        
        $options = [
            [
                'id' => 'QUARANTINE',
                'name' => 'QUARANTINE'
            ],
            [
                'id' => 'HOLD',
                'name' => 'HOLD'
            ],
            [
                'id' => 'RELEASE',
                'name' => 'RELEASE'
            ],
            [
                'id' => 'REJECT',
                'name' => 'REJECT'
            ],
            [
                'id' => 'RANDOM',
                'name' => 'RANDOM'
            ]
        ];


        $material_preparation_id = $material_preparation->id;
        if(!$material_preparation->is_allocation)
        {
            $arrival = MaterialArrival::select('qty_ordered')
            ->whereIn('id',function($query) use($material_preparation_id)
            {
                $query->select('material_arrival_id')
                ->from('detail_material_preparations')
                ->where('material_preparation_id',$material_preparation_id);
            })
            ->groupby('qty_ordered')
            ->first();

            if($arrival) $qty_order = $arrival->qty_ordered;
            else $qty_order         = null;
        }else
        {
            $allocation = AllocationItem::select('qty_booking')
            ->whereIn('id',function($query) use($material_preparation_id){
                $query->select('allocation_item_id')
                ->from('detail_material_preparations')
                ->where('material_preparation_id',$material_preparation_id);
            })
            ->groupby('qty_booking')
            ->first();

            if($allocation) $qty_order  = $allocation->qty_booking;
            else $qty_order             = null;
        }

        
        //alokasi subcont / beda whs 
        $erp = DB::connection('erp');
        $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
            ['poreference',$material_preparation->po_buyer],
            ['componentid',$material_preparation->item_id]
        ])
        ->select('warehouse_place')
        ->first();

        $sp = null;
        if($sewing_place != null)
        {
            $sp = $sewing_place->warehouse_place;
        }else{
            $sp = null;
        }

        
        $obj                                        = new StdClass();
        $obj->check_all                             = false;
        $obj->barcode                               = $material_preparation->barcode;
        $obj->material_preparation_id               = $material_preparation->id;
        $obj->supplier_name                         = $material_preparation->supplier_name;
        $obj->c_order_id                            = $material_preparation->c_order_id;
        $obj->c_bpartner_id                         = $material_preparation->c_bpartner_id;
        $obj->document_no                           = $material_preparation->document_no;
        $obj->po_buyer                              = $material_preparation->po_buyer;
        $obj->item_code                             = $material_preparation->item_code;
        $obj->item_desc                             = $material_preparation->item_desc;
        $obj->category                              = $material_preparation->category;
        $obj->uom_conversion                        = $material_preparation->uom_conversion;
        $obj->qty_need                              = $qty_need;
        $obj->_qty                                  = $material_preparation->qty_conversion;
        $obj->qty                                   = $material_preparation->qty_conversion;
        $obj->status_options                        = $options;
        $obj->_status                               = ($material_check) ? $material_check->status:'QUARANTINE';
        $obj->status                                = ($material_check) ? $material_check->status:'QUARANTINE';
        $obj->qty_check                             = 0;
        $obj->qty_order                             = $qty_order;
        $obj->movement_date                         = Carbon::now()->toDateTimeString();
        $obj->remark                                = null;
        $obj->total_receive                         = $total_receive;
        $obj->persentage                            = ($material_preparation) ? $material_preparation->persentage_metal_detector : 0;
        $obj->sewing_place                          = $sp;
        
        return response()->json($obj,200); 
       
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcodes' => 'required|not_in:[]'
        ]);
        
        if ($validator->passes())
        {
            //return response()->json($barcodes);
            
            $_warehouse_id      = $request->warehouse_id;
            $barcodes           = json_decode($request->barcodes);
            $update_status      = array();
            $concatenate        = '';
            $_movement_date     = carbon::now()->todatetimestring();

            try 
            {
                DB::beginTransaction();

                foreach ($barcodes as $key => $value) 
                {
                    $is_exists = MaterialCheck::where('barcode_preparation',$value->barcode)
                    ->whereNull('deleted_at')
                    ->where('is_from_metal_detector', true)
                    ->first();
                    
                    $material_preparation_id = $value->material_preparation_id;
                    $movement_date              = ($value->movement_date) ? $value->movement_date : $_movement_date;
                    $material_preparation       = MaterialPreparation::find($material_preparation_id);
                        

                    if($is_exists)
                    {
                        $is_exists->updated_at = $movement_date;
                        $is_exists->deleted_at = $movement_date;
                        $is_exists->save();
                    }

                    $material_check = MaterialCheck::create([
                        'material_preparation_id' => $material_preparation_id,
                        'barcode_preparation'     => $value->barcode,
                        'uom_po'                  => $value->_qty,
                        'uom_conversion'          => $value->uom_conversion,
                        'document_no'             => $value->document_no,
                        'po_buyer'                => $value->po_buyer,
                        'status'                  => $value->status,
                        'item_code'               => $value->item_code,
                        'item_desc'               => $value->item_desc,
                        'qty_order'               => isset($value->qty_order)? $value->qty_order : null,
                        'qty_reject'              => $value->qty_check,
                        'remark'                  => strtoupper(trim($value->remark)),
                        'user_id'                 => Auth::user()->id,
                        'is_reject'               => ($value->status == 'REJECT' ? true : false ),
                        'is_from_metal_detector'  => true,
                        'created_at'              => $movement_date,
                        'updated_at'              => $movement_date,
                    ]);

                    if ($value->qty == 0 && $value->status == 'REJECT')
                    {
                        $status_reject = 'FULL REJECT';
                    }else
                    {
                        if($value->status == 'HOLD' || $value->status == 'RELEASE' || $value->status == 'RANDOM') $status_reject = null;
                        else $status_reject = 'PARTIAL REJECT';
                    }

                    $update_status [] = [
                        'material_preparation_id'   => $value->material_preparation_id,
                        'status'                    => $value->status,
                        'qty'                       => $value->qty,
                        'qty_old'                   => $value->_qty,
                        'qty_reject'                => $value->qty_check,
                        'remark'                    => strtoupper(trim($value->remark)),
                        'movement_date'             => $movement_date,
                        'status_reject'             => $status_reject
                    ];

                    if($value->status == 'REJECT')
                    {
                        if($material_preparation)
                        {
                            $auto_allocation_id             = $material_preparation->auto_allocation_id;
                           
                            if($auto_allocation_id)
                            {
                                $auto_allocation                = AutoAllocation::find($auto_allocation_id);
                                $qty_reject_auto_allocation     = $auto_allocation->qty_reject;
                                $qc_note_auto_allocation        = $auto_allocation->qc_note;

                                $new_qty_reject_auto_allocation = sprintf('%0.8f',$qty_reject_auto_allocation + $value->qty_check);
                                $new_note                       = 'QTY SEBESAR '.$new_qty_reject_auto_allocation.' DI REJECT QC';
                               
                                $auto_allocation->qty_reject    = $new_qty_reject_auto_allocation;
                                $auto_allocation->qc_note       = $new_note;
                                $auto_allocation->save();
                            }
                        }
                    }
                        

                    $concatenate .= "'" .$value->material_preparation_id."',";
                }

                $concatenate = substr_replace($concatenate, '', -1);
                if($concatenate !=''){
                    DB::select(db::raw("SELECT * FROM delete_duplicate_qc_acc(array[".$concatenate."]);"));
                }
                
                $this->updateQcStatus($update_status,$_warehouse_id);

                if($concatenate !='')
                {
                    DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
                }
            
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            return response()->json(200); 
        }else{
            return response()->json('Please scan barcode first.',422); 
        }
    }

    static function updateQcStatus($update_status,$_warehouse_id)
    {
        $checking_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC') 
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','CHECKING')
        ->first();

        $locator_reject_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC')
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','REJECT')
        ->first();

        $locator_hold_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC')
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','HOLD')
        ->first();

        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($update_status as $key => $value) 
            {
                $material_preparation   = MaterialPreparation::find($value['material_preparation_id']);
                $qty_old                = sprintf('%0.8f',$value['qty_old']);
                $movement_date          = carbon::now();

                if($value['status'] == 'REJECT')
                {
                    if($value['status_reject'] == 'FULL REJECT') $qc_status = 'FULL REJECT';
                    elseif($value['status_reject'] == 'PARTIAL REJECT') $qc_status = $_new_status = 'REJECT '.number_format($value['qty_reject'], 4, '.', ',');
                
                    
                    $conversion = $material_preparation->conversion($material_preparation->item_code,$material_preparation->category,$material_preparation->uom_conversion);
                    if($conversion) $multiplyrate = $conversion->multiplyrate;
                    else $multiplyrate = 1;

                    $material_preparation->qty_reconversion = sprintf('%0.8f',$value['qty'] * $multiplyrate);
                    $material_preparation->qty_conversion   = sprintf('%0.8f',$value['qty']);

                }else
                {
                    $qc_status = $value['status'];
                }
                
                if($material_preparation)
                {
                    $last_locator_id                    = $material_preparation->last_locator_id;
                    $last_status_movement               = $material_preparation->last_status_movement;
                    $po_detail_id                       = $material_preparation->po_detail_id;
                    $material_preparation->qc_status    = $qc_status;
                    $_temp_locator                      = null;

                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();

                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    }
                    
                    if($value['status'] == 'REJECT' 
                    || $value['status_reject'] == 'FULL REJECT' 
                    || $value['status_reject'] == 'PARTIAL REJECT')
                    {
                        $locator_reject_qc = Locator::whereHas('area',function($query){
                            $query->where('name','QC')
                            ->where('warehouse',auth::user()->warehouse);
                        })
                        ->where('rack','REJECT')
                        ->first();

                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$locator_reject_qc->id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$locator_reject_qc->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',strtolower($value['status']).'-metal-detector'],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {
                    
                        $qc_rack = MaterialMovement::FirstOrCreate([
                            'from_location'       => $checking_qc->id,
                            'to_destination'      => $locator_reject_qc->id,
                            'from_locator_erp_id' => $checking_qc->area->erp_id,
                            'to_locator_erp_id'   => $locator_reject_qc->area->erp_id,
                            'po_buyer'            => $material_preparation->po_buyer,
                            'is_active'           => true,
                            'status'              => strtolower($value['status']).'-metal-detector',
                            'is_integrate'        => false,
                            'created_at'          => $value['movement_date'],
                            'updated_at'          => $value['movement_date'],
                            'no_packing_list'     => $no_packing_list,
                            'no_invoice'          => $no_invoice,
                        ]);

                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$qc_rack->id],
                            ['material_preparation_id',$material_preparation->id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$value['movement_date']],
                            ['qty_movement',sprintf('%0.8f',$value['qty'])],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
        
                        MaterialMovementLine::FirstOrCreate([
                            'material_movement_id'    => $qc_rack->id,
                            'material_preparation_id' => $material_preparation->id,
                            'item_id'                 => $material_preparation->item_id,
                            'item_code'               => $material_preparation->item_code,
                            'c_order_id'              => $material_preparation->c_order_id,
                            'c_orderline_id'          => $c_orderline_id,
                            'c_bpartner_id'           => $material_preparation->c_bpartner_id,
                            'supplier_name'           => $material_preparation->supplier_name,
                            'type_po'                 => $material_preparation->type_po,
                            'qty_movement'            => sprintf('%0.8f',$value['qty']),
                            'date_movement'           => $value['movement_date'],
                            'created_at'              => $value['movement_date'],
                            'updated_at'              => $value['movement_date'],
                            'is_active'               => true,
                            'is_integrate'            => false,
                            'nomor_roll'              => '-',
                            'warehouse_id'            => $material_preparation->warehouse,
                            'document_no'             => $material_preparation->document_no,
                            'note'                    => $value['remark'].' [ AWAL QTYNYA ADALAH '.$qty_old.' LALU DI REJECT '.$value['qty_reject'].' ]',
                            'user_id'                 => Auth::user()->id
                        ]);

                        }

                        if($value['status_reject'] == 'FULL REJECT')
                        {
                            $material_preparation->last_status_movement     = $last_status_movement;
                            $material_preparation->last_locator_id          = $locator_reject_qc->id;
                            $material_preparation->last_movement_date       = $value['movement_date'];
                            $material_preparation->last_user_movement_id    = auth::user()->id;
                            $material_preparation->deleted_at               = $value['movement_date'];
                        }

                        $material_preparation->qty_reject   = $value['qty_reject'];
                        $material_preparation->remark       = $value['remark'];
                    }

                    if($value['status'] == 'HOLD')
                    {
                        $locator_hold_qc = Locator::whereHas('area',function($query){
                            $query->where('name','QC')
                            ->where('warehouse',auth::user()->warehouse);
                        })
                        ->where('rack','HOLD')
                        ->first();

                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$locator_reject_qc->id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$locator_reject_qc->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',strtolower($value['status']).'-metal-detector'],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {
                        
                        $qc_rack = MaterialMovement::FirstOrCreate([
                            'from_location'       => $checking_qc->id,
                            'to_destination'      => $locator_hold_qc->id,
                            'from_locator_erp_id' => $checking_qc->area->erp_id,
                            'to_locator_erp_id'   => $locator_reject_qc->area->erp_id,
                            'po_buyer'            => $material_preparation->po_buyer,
                            'is_active'           => true,
                            'status'              => strtolower($value['status']).'-metal-detector',
                            'is_integrate'        => false,
                            'created_at'          => $value['movement_date'],
                            'updated_at'          => $value['movement_date'],
                            'no_packing_list'     => $no_packing_list,
                            'no_invoice'          => $no_invoice,

                        ]);

                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$qc_rack->id],
                            ['material_preparation_id',$material_preparation->id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$movement_date],
                            ['qty_movement',$qty],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
        
                        MaterialMovementLine::FirstOrCreate([
                            'material_movement_id'    => $qc_rack->id,
                            'material_preparation_id' => $material_preparation->id,
                            'item_id'                 => $material_preparation->item_id,
                            'item_code'               => $material_preparation->item_code,
                            'c_order_id'              => $material_preparation->c_order_id,
                            'c_orderline_id'          => $c_orderline_id,
                            'c_bpartner_id'           => $material_preparation->c_bpartner_id,
                            'supplier_name'           => $material_preparation->supplier_name,
                            'type_po'                 => $material_preparation->type_po,
                            'qty_movement'            => sprintf('%0.8f',$qty_old),
                            'date_movement'           => $value['movement_date'],
                            'created_at'              => $value['movement_date'],
                            'updated_at'              => $value['movement_date'],
                            'is_active'               => true,
                            'is_integrate'            => false,
                            'nomor_roll'              => '-',
                            'warehouse_id'            => $material_preparation->warehouse,
                            'document_no'             => $material_preparation->document_no,
                            'note'                    => $value['remark'],
                            'user_id'                 => Auth::user()->id
                        ]);

                        }

                        $material_preparation->last_status_movement     = $last_status_movement;
                        $material_preparation->last_locator_id          = $locator_hold_qc->id;
                        $material_preparation->remark                   = $value['remark'];
                        $material_preparation->last_movement_date       = $value['movement_date'];
                        $material_preparation->last_user_movement_id    = auth::user()->id;
                    }

                    if($value['status'] == 'RELEASE' || $value['status'] == 'RANDOM')
                    {
                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$locator_reject_qc->id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$locator_reject_qc->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',strtolower($value['status']).'-metal-detector'],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {

                        $qc_rack = MaterialMovement::FirstOrCreate([
                            'from_location'       => $checking_qc->id,
                            'to_destination'      => $last_locator_id,
                            'from_locator_erp_id' => $checking_qc->area->erp_id,
                            'to_locator_erp_id'   => $locator_reject_qc->area->erp_id,
                            'po_buyer'            => $material_preparation->po_buyer,
                            'is_active'           => true,
                            'status'              => strtolower($value['status']).'-metal-detector',
                            'is_integrate'        => false,
                            'created_at'          => $value['movement_date'],
                            'updated_at'          => $value['movement_date'],
                            'no_packing_list'     => $no_packing_list,
                            'no_invoice'          => $no_invoice,
                        ]);

                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$qc_rack->id],
                            ['material_preparation_id',$material_preparation->id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$movement_date],
                            ['qty_movement',$qty_old],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
        
                        MaterialMovementLine::FirstOrCreate([
                            'material_movement_id'    => $qc_rack->id,
                            'material_preparation_id' => $material_preparation->id,
                            'item_id'                 => $material_preparation->item_id,
                            'item_code'               => $material_preparation->item_code,
                            'c_order_id'              => $material_preparation->c_order_id,
                            'c_orderline_id'          => $c_orderline_id,
                            'c_bpartner_id'           => $material_preparation->c_bpartner_id,
                            'supplier_name'           => $material_preparation->supplier_name,
                            'type_po'                 => $material_preparation->type_po,
                            'qty_movement'            => sprintf('%0.8f',$qty_old),
                            'date_movement'           => $value['movement_date'],
                            'created_at'              => $value['movement_date'],
                            'updated_at'              => $value['movement_date'],
                            'is_active'               => true,
                            'is_integrate'            => false,
                            'nomor_roll'              => '-',
                            'warehouse_id'            => $material_preparation->warehouse,
                            'document_no'             => $material_preparation->document_no,
                            'note'                    => $value['remark'],
                            'user_id'                 => Auth::user()->id
                        ]);

                        }
                    }
                
                    $material_preparation->updated_at = $value['movement_date'];
                    $material_preparation->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
