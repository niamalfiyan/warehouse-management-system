<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Barcode;
use App\Models\Locator;
use App\Models\AutoAllocation;
use App\Models\MaterialCheck;
use App\Models\MaterialStock;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MovementStockHistory;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialPlanningFabric;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\HistoryMaterialStockOpname;
use App\Models\MaterialRequirementPerPart;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\DetailMaterialPlanningFabricPerPart;

use App\Http\Controllers\FabricReportDailyMaterialPreparationController as FabricReportDailyMaterialPreparation;


class FabricMaterialPreparationController extends Controller
{
    public function index()
    {
        return view('fabric_material_preparation.index');
    }

    public function storePlanning(Request $request)
    {
        //dd($request->toArray());
        $movement_date                      = carbon::now();
        $_material_preparation_fabric_id    = $request->material_preparation_fabric_id;
        $warehouse_id                       = $request->warehouse_id;
        $is_edit                            = $request->is_edit;
        $_is_piping                         = $request->is_piping;
        $is_additional                      = $request->is_additional;
        $article                            = $request->article;
        $style                              = $request->style;
        $item_code                          = $request->item_code;
        $item_code_source                   = $request->item_code_source;
        $item_id_book                       = $request->item_id_book;
        $item_id_source                     = $request->item_id_source;
        $c_order_id                         = $request->c_order_id;
        $c_bpartner_id                      = $request->c_bpartner_id;
        $supplier_name                      = $request->supplier_name;
        $document_no                        = $request->document_no;
        $qty_preparation                    = $request->qty_preparation;
        $qty_outstanding                    = $request->qty_outstanding;
        $actual_planning_date               = carbon::now()->format('Y-m-d');
            
        if($_is_piping == 1) $is_piping = true;
        else $is_piping = false;

        $is_exists = MaterialStock::where([
            ['c_bpartner_id',$c_bpartner_id],
            ['c_order_id',$c_order_id],
            ['item_id',$item_id_source],
            ['warehouse_id',$warehouse_id],
        ])
        ->exists();

        $obj = new StdClass();

        if($is_additional == -1)
        {
            $planning_date = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
            
            
            $check_planning = MaterialPlanningFabric::where([
                ['planning_date',$planning_date],
                ['item_id',$item_id_book],
                ['warehouse_id',$warehouse_id],
            ])
            ->exists();

            if(!$check_planning) return response()->json('Planning Cutting doesnt exists yet in wms. Please info admin to sync planning cutting first.',422);
            
            if($_material_preparation_fabric_id)
            {
                $material_preparation_fabric = MaterialPreparationFabric::find($_material_preparation_fabric_id);

                if($material_preparation_fabric->preparation_date)
                {
                    if($material_preparation_fabric->preparation_by)
                    {
                        return response()->json('Planning ini dilock, karena sedang di kerjakan oleh komputer '.$material_preparation_fabric->preparation_by,422);
                    }
                }

                if($is_edit)
                {
                    $material_preparation_fabric->preparation_date                      = carbon::now();
                    $material_preparation_fabric->preparation_by                        = $this->getUserIpAddr();
                    $material_preparation_fabric->total_reserved_qty                    = sprintf('%0.8f',$qty_preparation);
                    $material_preparation_fabric->total_qty_outstanding                 = sprintf('%0.8f',$qty_outstanding);
                    if($qty_outstanding > 0) $material_preparation_fabric->relax_date   = null;
                    
                    $material_preparation_fabric->save();
                }else
                {
                    $total_reserved_qty = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$material_preparation_fabric->total_qty_rilex);
                    if($qty_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$qty_preparation - $total_qty_relax);
                        if($new_qty_oustanding < 0) return response()->json('Please do cancel preparation first before do preparation.',422);

                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $material_preparation_fabric->id,
                            'remark'                            => 'perubahan total qty prepare karena ada tambahan alokasi',
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $qty_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $material_preparation_fabric->preparation_date      = carbon::now();
                        $material_preparation_fabric->preparation_by        = $this->getUserIpAddr();
                        $material_preparation_fabric->total_reserved_qty    = $qty_preparation;
                        $material_preparation_fabric->total_qty_outstanding = $new_qty_oustanding;
                        $material_preparation_fabric->save();
                    }else
                    {
                        if($material_preparation_fabric->total_qty_outstanding <= 0) return response()->json('This planning already prepared.',422);
                    }
                }

                $obj->id                        = $material_preparation_fabric->id;
                $obj->planning_date             = $material_preparation_fabric->planning_date->format('Y-m-d');
                $obj->is_piping                 = strtoupper($material_preparation_fabric->is_piping);
                $obj->is_from_additional        = strtoupper($material_preparation_fabric->is_from_additional);
                $obj->actual_planning_date      = $actual_planning_date;
                $obj->uom                       = $material_preparation_fabric->uom;
                $obj->_style                    = $material_preparation_fabric->_style;
                $obj->article_no                = $material_preparation_fabric->article_no;
                $obj->total_reserved_qty        = $material_preparation_fabric->total_reserved_qty;
                $obj->warehouse_id              = $material_preparation_fabric->warehouse_id;
                $obj->document_no               = strtoupper($material_preparation_fabric->document_no);
                $obj->item_code                 = strtoupper($material_preparation_fabric->item_code);
                $obj->item_id_book              = $material_preparation_fabric->item_id_book;
                $obj->item_id_source            = $material_preparation_fabric->item_id_source;
                $obj->c_order_id                = $material_preparation_fabric->c_order_id;
                $obj->item_code_source          = strtoupper($material_preparation_fabric->item_code_source);
                $obj->c_bpartner_id             = $material_preparation_fabric->c_bpartner_id;
                $obj->supplier_name             = $material_preparation_fabric->supplier_name;
                $obj->total_reserved_qty        = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                $obj->is_from_allocation        = $material_preparation_fabric->is_from_allocation;
                $obj->additional_note           = $material_preparation_fabric->additional_note;
                $obj->total_qty_rilex           = 0;
                $obj->_total_qty_outstanding    = sprintf('%0.8f',$material_preparation_fabric->total_qty_outstanding);
                $obj->total_qty_outstanding     = sprintf('%0.8f',$material_preparation_fabric->total_qty_outstanding);
                $obj->is_exists                 = $is_exists;
                $material_preparation_fabric_id = $material_preparation_fabric->id;

            }else
            {
                try 
                {
                    DB::beginTransaction();
                    $_material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                        'c_bpartner_id'         => $c_bpartner_id,
                        'supplier_name'         => $supplier_name,
                        'document_no'           => $document_no,
                        'c_order_id'            => $c_order_id,
                        'item_code'             => $item_code,
                        'item_code_source'      => $item_code_source,
                        'item_id_book'          => $item_id_book,
                        'item_id_source'        => $item_id_source,
                        'uom'                   => 'YDS',
                        'total_reserved_qty'    => sprintf('%0.8f',$qty_preparation),
                        'total_qty_outstanding' => sprintf('%0.8f',$qty_preparation),
                        'planning_date'         => $planning_date,
                        'is_piping'             => $is_piping,
                        'article_no'            => $article,
                        '_style'                => $style,
                        'is_from_additional'    => false,
                        'preparation_date'      => carbon::now(),
                        'preparation_by'        => $this->getUserIpAddr(),
                        'warehouse_id'          => $warehouse_id,
                        'user_id'               => auth::user()->id,
                        'created_at'            => $movement_date,
                        'updated_at'            => $movement_date,
                    ]);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                $obj->id                        = $_material_preparation_fabric->id;
                $obj->planning_date             = $_material_preparation_fabric->planning_date->format('Y-m-d');
                $obj->is_piping                 = strtoupper($_material_preparation_fabric->is_piping);
                $obj->is_from_additional        = strtoupper($_material_preparation_fabric->is_from_additional);
                $obj->actual_planning_date      = $actual_planning_date;
                $obj->uom                       = $_material_preparation_fabric->uom;
                $obj->article_no                = $_material_preparation_fabric->article_no;
                $obj->_style                    = $_material_preparation_fabric->_style;
                $obj->total_reserved_qty        = $_material_preparation_fabric->total_reserved_qty;
                $obj->warehouse_id              = $_material_preparation_fabric->warehouse_id;
                $obj->document_no               = strtoupper($_material_preparation_fabric->document_no);
                $obj->item_code                 = strtoupper($_material_preparation_fabric->item_code);
                $obj->item_code_source          = strtoupper($_material_preparation_fabric->item_code_source);
                $obj->item_id_book              = $_material_preparation_fabric->item_id_book;
                $obj->item_id_source            = $_material_preparation_fabric->item_id_source;
                $obj->c_order_id                = $_material_preparation_fabric->c_order_id;
                $obj->c_bpartner_id             = $_material_preparation_fabric->c_bpartner_id;
                $obj->supplier_name             = $_material_preparation_fabric->supplier_name;
                $obj->total_reserved_qty        = sprintf('%0.8f',$_material_preparation_fabric->total_reserved_qty);
                $obj->total_qty_rilex           = 0;
                $obj->_total_qty_outstanding    = sprintf('%0.8f',$_material_preparation_fabric->total_qty_outstanding);
                $obj->total_qty_outstanding     = sprintf('%0.8f',$_material_preparation_fabric->total_qty_outstanding);
                $obj->is_exists                 = $is_exists;
                $material_preparation_fabric_id = $_material_preparation_fabric->id;
            }
            
            DB::select(db::raw("SELECT * FROM delete_duplicate_summary_preparation_fabric('".$material_preparation_fabric_id."');"));
        }else
        {
            $additional_note                    = trim(strtoupper($request->additional_note));

            try 
            {
                DB::beginTransaction();

                $_material_preparation_fabric = MaterialPreparationFabric::where('id', $_material_preparation_fabric_id)
                                                ->first();
                // $_material_preparation_fabric = MaterialPreparationFabric::create([
                //     'c_bpartner_id'         => $c_bpartner_id,
                //     'supplier_name'         => $supplier_name,
                //     'document_no'           => $document_no,
                //     'c_order_id'            => $c_order_id,
                //     'item_code'             => $item_code,
                //     'item_code_source'      => $item_code_source,
                //     'item_id_book'          => $item_id_book,
                //     'item_id_source'        => $item_id_source,
                //     'uom'                   => 'YDS',
                //     'total_reserved_qty'    => sprintf('%0.8f',$qty_preparation),
                //     'total_qty_outstanding' => sprintf('%0.8f',$qty_preparation),
                //     'planning_date'         => null,
                //     'is_piping'             => $is_piping,
                //     'additional_note'       => $additional_note,
                //     'is_from_additional'    => true,
                //     'warehouse_id'          => $warehouse_id,
                //     'user_id'               => auth::user()->id,
                //     'created_at'            => $movement_date,
                //     'updated_at'            => $movement_date,
                // ]);
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $obj->id                        = $_material_preparation_fabric->id;
            $obj->is_piping                 = strtoupper($_material_preparation_fabric->is_piping);
            $obj->is_from_additional        = strtoupper($_material_preparation_fabric->is_from_additional);
            $obj->planning_date             = $_material_preparation_fabric->planning_date;
            $obj->actual_planning_date      = $actual_planning_date;
            $obj->uom                       = $_material_preparation_fabric->uom;
            $obj->total_reserved_qty        = $_material_preparation_fabric->total_reserved_qty;
            $obj->warehouse_id              = $_material_preparation_fabric->warehouse_id;
            $obj->document_no               = strtoupper($_material_preparation_fabric->document_no);
            $obj->item_code                 = strtoupper($_material_preparation_fabric->item_code);
            $obj->item_code_source          = strtoupper($_material_preparation_fabric->item_code_source);
            $obj->c_bpartner_id             = $_material_preparation_fabric->c_bpartner_id;
            $obj->item_id_book              = $_material_preparation_fabric->item_id_book;
            $obj->item_id_source            = $_material_preparation_fabric->item_id_source;
            $obj->c_order_id                = $_material_preparation_fabric->c_order_id;
            $obj->supplier_name             = $_material_preparation_fabric->supplier_name;
            $obj->additional_note           = $_material_preparation_fabric->additional_note;
            $obj->total_reserved_qty        = sprintf('%0.8f',$_material_preparation_fabric->total_reserved_qty);
            $obj->total_qty_rilex           = 0;
            $obj->_total_qty_outstanding    = sprintf('%0.8f',$_material_preparation_fabric->total_qty_outstanding);
            $obj->total_qty_outstanding     = sprintf('%0.8f',$_material_preparation_fabric->total_qty_outstanding);
            $obj->is_exists                 = $is_exists;
            $material_preparation_fabric_id = $_material_preparation_fabric->id;
        }
      
        return response()->json($obj,200);
    }

    public function getRoll(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $barcode        = strtoupper(trim($request->barcode));

        $material_stock = MaterialStock::where([
            ['barcode_supplier',$barcode],
            ['is_active',true],
            ['warehouse_id',$warehouse_id],
        ])
        ->orderby('created_at','desc')
        ->first();

       
        if(!$material_stock) return response()->json('Roll not found',422);

        $check_is_need_lot = Item::where('item_id',$material_stock->item_id)->first();

        if($check_is_need_lot->required_lot)
        {
            if($material_stock->load_actual == null || $material_stock->load_actual == '') return response()->json('This material doesnt have lot, please call qc to do inspect lab first',422);
        }

        if($material_stock->is_short_roll)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_short_roll) return response()->json('Actual yard is not same with barcode, please move to locator reject first',422);
        } 

        if($material_stock->is_reject_by_lot)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Roll cannot be used due of lot is not match,please move to locator reject first',422);
        } 

        if($material_stock->is_reject_by_rma)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_rma) return response()->json('cannot be used due of material is wrong,please move to locator reject first',422);
        } 

        if($material_stock->is_reject)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Roll cannot be used due of reject result by qc,please move to locator reject first',422);
        } 
        
        if($material_stock->inspect_lot_result == 'HOLD') return response()->json('Roll is in hold by qc due of lot inspection.',422);
        if($material_stock->qc_result == 'HOLD') return response()->json('Roll is in hold by qc due of fir inspection.',422);
       
        if($material_stock->last_status == 'sto') return response()->json('Roll is in use for sto by '.$material_stock->userLastUsed->name.' at '.$material_stock->last_date_used,422);
        if($material_stock->last_status == 'handover') return response()->json('Roll is in use for handover prosess by '.$material_stock->userLastUsed->name.' at '.$material_stock->last_date_used,422);
        if($material_stock->last_status == 'inspect_lab') return response()->json('Roll is in use by qc',422);
        if($material_stock->inspect_lot_result == 'HOLD') return response()->json('Roll is in hold by qc due of lot inspection.',422);
        if($material_stock->ip_address)
        {
            if($material_stock->ip_address) 
            {
                if($material_stock->last_user_used_id && $material_stock->last_user_used_id) return response()->json('Roll is in use for process '.$material_stock->last_status.' by '.$material_stock->userLastUsed->name.' at '.$material_stock->last_date_used.' IP '.$material_stock->ip_address,422);
                else return response()->json('Roll is in used by IP '.$material_stock->ip_address,422);
            }
        }
        if($material_stock->last_status == 'relax' && $material_stock->ip_address != $this->getUserIpAddr()) return response()->json('Roll is in use for process '.$material_stock->last_status.' by '. $material_stock->userLastUsed->name.' at '.$material_stock->last_date_used.' IP '.$material_stock->ip_address,422);
        if($material_stock->is_allocated == true || $material_stock->deleted_at != null) return response()->json('Roll doesnt have qty left ',422);

        $is_material_on_checking = MaterialCheck::whereNotNull('material_stock_id')
        ->where('material_stock_id',$material_stock->id)
        ->whereNull('confirm_user_id')
        ->whereNull('deleted_at')
        ->exists();

        if($is_material_on_checking) return response()->json('Roll is under checking by qc and not yet had confirmation from them, please tell qc team to give confirmation.',422);

        if(!$material_stock->approval_date) return response()->json('Barcode cannot used due approval is empty',422);
        if($material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Barcode cannot used due reject by lot',422);
        if($material_stock->movement_to_locator_reject_date_from_rma) return response()->json('Barcode cannot used due reject by return material',422);
        //if($material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Barcode cannot used due reject by qc',422);

        $is_outstanding_approval_exists = HistoryMaterialStockOpname::where('material_stock_id',$material_stock->id)
        ->where(function($query){
            $query->whereNull('approval_date')
            ->OrwhereNull('accounting_approval_date');
        })
        ->exists();
        
        if($is_outstanding_approval_exists) return response()->json('Roll cannot be STO due outstanding approval STO',422);

        $material_stock->last_date_used     = carbon::now();
        $material_stock->last_status        = 'relax';
        $material_stock->last_user_used_id  = auth::user()->id;
        $material_stock->ip_address         = $this->getUserIpAddr();
        $material_stock->save();

        $actual_planning_date   = carbon::now()->format('Y-m-d');
        /*$get_barcode            = $this->randomCode($barcode);
        $barcode                = $get_barcode->barcode;
        $referral_code          = $get_barcode->referral_code;
        $sequence               = $get_barcode->sequence;*/
        
        //dd($data_planning);
        $obj                                    = new stdclass();
        $obj->material_preparation_fabric_id    = null;
        $obj->is_additional                     = null;
        $obj->id                                = $material_stock->id;
        $obj->locator_id                        = $material_stock->locator_id;
        $obj->barcode                           = $material_stock->barcode_supplier;
        $obj->c_bpartner_id                     = $material_stock->c_bpartner_id;
        $obj->supplier_name                     = $material_stock->supplier_name;
        $obj->document_no                       = strtoupper($material_stock->document_no);
        $obj->batch_number                      = $material_stock->batch_number;
        $obj->c_order_id                        = $material_stock->c_order_id;
        $obj->item_id                           = $material_stock->item_id;
        $obj->item_code                         = strtoupper($material_stock->item_code);
        $obj->load_actual                       = $material_stock->load_actual;
        $obj->begin_width                       = ($material_stock->begin_width) ? $material_stock->begin_width: 0;
        $obj->middle_width                      = ($material_stock->middle_width) ? $material_stock->middle_width: 0;
        $obj->end_width                         = ($material_stock->end_width) ? $material_stock->end_width: 0;
        $obj->actual_width                      = ($material_stock->actual_width) ? $material_stock->actual_width: 0;
        $obj->_reserved_qty                     = ($material_stock->reserved_qty) ? $material_stock->reserved_qty: 0;
        $obj->reserved_qty                      = ($material_stock->reserved_qty) ? $material_stock->reserved_qty: 0;
        $obj->_available_qty                    = $material_stock->available_qty;
        $obj->available_qty                     = $material_stock->available_qty;
        $obj->nomor_roll                        = $material_stock->nomor_roll;
        $obj->stock                             = $material_stock->stock;
        $obj->uom                               = trim($material_stock->uom);
        $obj->actual_length                     = $material_stock->available_qty;
        $obj->qty_booking                       = 0;
        $obj->sorting                           = false;
        $obj->is_actual_not_null                = false;
        $obj->planning_date                     = null;
        $obj->is_selected                       = false;
        $obj->actual_planning_date              = $actual_planning_date;
        $obj->new_barcode                       = 'BELUM DIPRINT';
        $obj->referral_code                     = '-1';
        $obj->sequence                          = '-1';

        return response()->json($obj,200);

 
    }

    public function getPlanningPerArticle(Request $request)
    {
        $_is_piping     = $request->is_piping;
        $item_id        = $request->item_id;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
        $c_order_id     = $request->c_order_id;

        $articles       = MaterialPreparationFabric::select('article_no')
        ->where([
            ['is_piping',$_is_piping],
            ['item_id_source',$item_id],
            ['warehouse_id',$warehouse_id],
            ['planning_date',$planning_date],
            ['c_order_id', $c_order_id],
        ])
        ->pluck('article_no','article_no')
        ->all();

        //dd( $articles);

        if(count($articles) > 0)
        {
            return response()->json([
                'articles'  => $articles,
                'total'     => count($articles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }

    public function getPlanningPerStyle(Request $request)
    {
        $is_piping      = $request->is_piping;
        $article_no     = $request->article_no;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');

        $styles         = MaterialRequirement::select('_style')
        ->whereIn('po_buyer',function($query) use($is_piping,$article_no,$planning_date,$warehouse_id){
            $query->select('po_buyer')
            ->from('material_planning_fabrics')
            ->where([
                ['is_piping',$is_piping],
                ['article_no',$article_no],
                ['planning_date',$planning_date],
                ['warehouse_id',$warehouse_id],
            ])
            ->groupby('po_buyer');
        })
        ->whereNotNull('_style')
        ->where('is_upload_manual', false)
        ->pluck('_style','_style')
        ->all();

        if(count($styles) > 0)
        {
            return response()->json([
                'styles'    => $styles,
                'total'     => count($styles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }

    public function getPlanningPerDate(Request $request)
    {
        //dd($request->toArray());
        try 
        {
            DB::beginTransaction();

            $_is_piping     = $request->is_piping;
            $c_order_id     = $request->c_order_id;
            $item_code      = $request->item_code;
            $item_id_source = $request->item_id_source;
            $c_bpartner_id  = $request->c_bpartner_id;
            $document_no    = $request->document_no;
            $style          = $request->style;
            $article_no     = $request->article_no;
            $warehouse_id   = $request->warehouse_id;
            $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
            
            if($_is_piping == 1) $is_piping = true;
            else $is_piping = false;

            $material_preparation_fabric = MaterialPreparationFabric::where([
                ['c_order_id',$c_order_id],
                ['item_id_source',$item_id_source],
                ['planning_date',$planning_date],
                //['c_bpartner_id',$c_bpartner_id],
                ['is_piping',$is_piping],
                ['article_no',$article_no],
                ['_style',$style],
                ['warehouse_id',$warehouse_id],
                ['is_from_additional', false]
            ])
            ->whereNull('deleted_at')
            ->orderby('planning_date','asc')
            ->first();
            // dd($material_preparation_fabric);

            if($material_preparation_fabric)
            {
                // if(auth::user()->warehouse == '1000011')
                // {
                    $allocation = DetailMaterialPlanningFabric::select('document_no'
                        ,'c_order_id'
                        ,'_style'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'planning_date'
                        ,'c_bpartner_id'
                        ,'is_piping'
                        ,db::raw("sum(qty_booking) as qty_prepare"))
                    ->where([
                        ['c_order_id',$c_order_id],
                        ['item_id_source',$item_id_source],
                        ['planning_date',$planning_date],
                        //['c_bpartner_id',$c_bpartner_id],
                        ['is_piping',$is_piping],
                        ['_style',$style],
                        ['article_no',$article_no],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->groupby('document_no'
                        ,'c_order_id'
                        ,'_style'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'planning_date'
                        ,'c_bpartner_id'
                        ,'is_piping')
                    ->first();

                    if($allocation)
                    {
                        $qty_prepare        = $allocation->qty_prepare;
                        $total_reserved_qty = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                        $total_qty_relax    = sprintf('%0.8f',$material_preparation_fabric->total_qty_rilex);
                        if($qty_prepare != $total_reserved_qty)
                        {
                            $new_qty_oustanding = sprintf('%0.8f',$qty_prepare - $total_qty_relax);
                           
                            $material_preparation_fabric->total_reserved_qty    = $qty_prepare;
                            $material_preparation_fabric->total_qty_outstanding = $new_qty_oustanding;
                            $material_preparation_fabric->save();
                        }
                        $update_preparation = MaterialPreparationFabric::find($material_preparation_fabric->id);
                        return response()->json($update_preparation,200);

                    }else return response()->json($material_preparation_fabric,200);
                }else return response()->json($material_preparation_fabric,200);
            // }else
            // {
            //     $allocation = DetailMaterialPlanningFabric::select('document_no'
            //         ,'c_order_id'
            //         ,'_style'
            //         ,'article_no'
            //         ,'item_code'
            //         ,'item_code_source'
            //         ,'item_id_book'
            //         ,'item_id_source'
            //         ,'planning_date'
            //         ,'c_bpartner_id'
            //         ,'is_piping'
            //         ,db::raw("sum(qty_booking) as qty_prepare"))
            //     ->where([
            //         ['c_order_id',$c_order_id],
            //         ['item_id_source',$item_id_source],
            //         ['planning_date',$planning_date],
            //         //['c_bpartner_id',$c_bpartner_id],
            //         ['is_piping',$is_piping],
            //         ['article_no',$article_no],
            //         ['_style',$style],
            //         ['warehouse_id',auth::user()->warehouse],
            //     ])
            //     ->groupby('document_no'
            //         ,'c_order_id'
            //         ,'_style'
            //         ,'article_no'
            //         ,'item_code'
            //         ,'item_code_source'
            //         ,'item_id_book'
            //         ,'item_id_source'
            //         ,'planning_date'
            //         ,'c_bpartner_id'
            //         ,'is_piping')
            //     ->first();

            //     if($allocation)
            //     {
            //         $obj                            = new stdClass();
            //         $obj->id                        = null;
            //         $obj->document_no               = $allocation->document_no;
            //         $obj->item_code                 = $allocation->item_code;
            //         $obj->article_no                = $allocation->article_no;
            //         $obj->style                     = $allocation->style;
            //         $obj->item_code_source          = $allocation->item_code_source;
            //         $obj->item_id_book              = $allocation->item_id_book;
            //         $obj->item_id_source            = $allocation->item_id_source;
            //         $obj->planning_date             = $allocation->planning_date;
            //         $obj->warehouse_id              = auth::user()->warehouse;
            //         $obj->c_bpartner_id             = $allocation->c_bpartner_id;
            //         $obj->is_piping                 = $allocation->is_piping;
            //         $obj->total_reserved_qty        = $allocation->qty_prepare;
            //         $obj->total_qty_rilex           = 0;
            //         $obj->total_qty_outstanding     = $allocation->qty_prepare;

            //     }else
            //     {
            //         $obj                            = new stdClass();
            //         $obj->id                        = null;
            //         $obj->document_no               = $document_no;
            //         $obj->style                     = $style;
            //         $obj->item_code                 = $item_code;
            //         $obj->item_code_source          = $item_code;
            //         $obj->item_id_book              = $item_id_source;
            //         $obj->item_id_source            = $item_id_source;
            //         $obj->article_no                = $article_no;
            //         $obj->planning_date             = $planning_date;
            //         $obj->warehouse_id              = auth::user()->warehouse;
            //         $obj->c_bpartner_id             = $c_bpartner_id;
            //         $obj->is_piping                 = $is_piping;
            //         $obj->total_reserved_qty        = 0;
            //         $obj->total_qty_rilex           = 0;
            //         $obj->total_qty_outstanding     = 0;
            //     } //return response()->json('-1',200);

            //     return response()->json($obj,200);
            // }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function getPlanningPerDateAdditional(Request $request)
    {
        $_is_piping     = false;
        $item_id        = $request->item_id;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
        $c_order_id     = $request->c_order_id;

        $auto_allocations = AutoAllocation::where('item_id_source', $item_id)
                            ->where('c_order_id', $c_order_id)
                            ->where('warehouse_id', $warehouse_id)
                            ->where('is_fabric', true)
                            ->where('is_additional', true)
                            ->where('document_allocation_number', '!=','')
                            ->whereNotNull('document_allocation_number')
                            ->where(db::raw('generate_form_booking::date'), $planning_date)
                            ->pluck('document_allocation_number','document_allocation_number')
                            ->all();

        if(count($auto_allocations) > 0)
        {
            return response()->json([
                'articles'  => $auto_allocations,
                'total'     => count($auto_allocations),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }

                            


    }

    public function getPlanningPerNoBookingAdditional(Request $request)
    {
        $_is_piping     = false;
        $document_allocation_number = $request->booking_number;
        $item_id        = $request->item_id;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
        $c_order_id     = $request->c_order_id;

        // dd($item_id);

        // $auto_allocation = AutoAllocation::where('document_allocation_number', $document_allocation_number)
        //                     ->where(db::raw('generate_form_booking::date'), $planning_date)
        //                     ->whereNull('deleted_at')
        //                     ->first();
               
        // $auto_allocation_id          = $auto_allocation->id;
        // // dd($auto_allocation_id);
        // $material_preparation_fabric = MaterialPreparationFabric::where('auto_allocation_id', $auto_allocation_id)
        //                              ->first();
        
        $material_preparation_fabric = DB::table('auto_allocations')
                            ->select('material_preparation_fabrics.*')
                            ->join('material_preparation_fabrics', 'material_preparation_fabrics.auto_allocation_id', '=', 'auto_allocations.id')
                            ->where('auto_allocations.document_allocation_number', $document_allocation_number)
                            ->where(db::raw('auto_allocations.generate_form_booking::date'), $planning_date)
                            ->where('auto_allocations.is_additional', true)
                            ->whereNull('auto_allocations.deleted_at')
                            ->first();
        
        // dd($auto_allocation);
        // $total_qty_outstanding = sprintf('%0.8f',$material_preparation_fabric->total_qty_outstanding);
        // $total_reserved_qty    = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
        // $total_qty_relax       = sprintf('%0.8f',$material_preparation_fabric->total_qty_rilex);

        if($material_preparation_fabric)
        {
            return response()->json($material_preparation_fabric,200);
        }else
        {
            return response()->json('Allocation not found',422);
        }

    }

    public function removeLastUser(Request $request)
    {
        $id                                 = $request->id;
        $material_stock                     = MaterialStock::find($id);
        $material_stock->last_date_used     = null;
        $material_stock->last_status        = null;
        $material_stock->last_user_used_id  = null;
        $material_stock->ip_address         = null;
        $material_stock->save();
       
        return response()->json(200);
    }

    public function locatorPicklist(Request $request)
    {
        $q          = (isset($request->q) ? strtoupper($request->q) : null );
        $warehouse  = $request->warehouse_id;
        $lists      = Locator::whereHas('area',function($query) use($warehouse)
        {
            $query->where('warehouse',$warehouse)
            ->where('is_active',true)
            ->where('is_destination',false)
            ->whereNotIn('name',['ADJUSTMENT','RECEIVING','SUPPLIER']);
        })
        ->where('is_active',true)
        ->Where(function($query){
            $query->where('last_po_buyer','RLX')
            ->orWhereNull('last_po_buyer');
        })
        ->where(function($query) use($q)
        {
            $query->where('barcode','like',"%$q%")
            ->orWhere('code','like',"%$q%")
            ->orWhere('rack','like',"%$q%");
        })
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->paginate('10');

        return view('fabric_material_preparation._rack_fabric_list',compact('lists'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data_planning' => 'required|not_in:[]',
            'data_prepare'  => 'required|not_in:[]',
        ]);

        if($validator->passes())
        {
            $data_planning      = json_decode($request->data_planning);
            $data_prepare       = json_decode($request->data_prepare);
            $warehouse_id       = $request->warehouse_id;
            $machine            = $request->machine;
           
            $source_locator     = array();
            $insert_barcode     = array();
            $update_summary     = array();
            $return_print       = array();
            $movement_date      = carbon::now();
            $flag_insert        = 0;
            $array_1            = array();


            if($request->locator_in_fabric_id)
            {
                $locator_id         = $request->locator_in_fabric_id;
                $locator            = Locator::find($locator_id);
            }else
            {
                $locator = Locator::whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();
                $locator_id         = $locator->id;

            }
            
            //dd($data_prepare);
           
            try 
            {
                DB::beginTransaction();

                $locator->last_po_buyer = 'RLX';
                $locator->save();
                DB::commit();

                foreach ($data_prepare as $key_1 => $value)
                {
                    $material_preparation_fabric_id = $value->material_preparation_fabric_id;
                    $material_stock_id              = $value->id;
                    $material_stock                 = MaterialStock::find($material_stock_id);
                    
                    $is_match = MaterialPreparationFabric::where([
                        ['id',$material_preparation_fabric_id],
                        // ['warehouse_id',$material_stock->warehouse_id],
                        // //['c_bpartner_id',$material_stock->c_bpartner_id],
                        // ['c_order_id',$material_stock->c_order_id],
                        // ['item_id_source',$material_stock->item_id],
                        // ['planning_date',$value->planning_date],
                    ])
                    ->exists();


                    if($is_match)
                    {
                        $old_locator            = $material_stock->locator_id;
                        $begin_width            = $value->begin_width;
                        $is_additional          = $value->is_additional;
                        $planning_date          = $value->planning_date;
                        $middle_width           = $value->middle_width;
                        $end_width              = $value->end_width;
                        $actual_width           = $value->actual_width;
                        $item_id                = $value->item_id;
                        $uom                    = trim($value->uom);
                        $item_code              = trim($value->item_code);
                        $actual_planning_date   = $value->actual_planning_date;
                        $available_qty          = sprintf('%0.8f',$value->available_qty);
                        $reserved_qty           = sprintf('%0.8f',$value->reserved_qty);
                        $qty_booking            = sprintf('%0.8f',$value->qty_booking);
                        $actual_length          = (isset($value->actual_length)) ? $value->actual_length: '0';

                        if(sprintf('%0.8f',$value->available_qty) == 0.0000)
                        {
                            $barcode            = $material_stock->barcode_supplier;
                            $referral_code      = $barcode;
                            $sequence           = $material_stock->sequence;
                        } else 
                        {
                            $barcode            = $value->new_barcode;
                            $referral_code      = $value->referral_code;
                            $sequence           = $value->sequence;
                        }

                        if($actual_width)
                        {
                            $material_stock->begin_width    = $begin_width;
                            $material_stock->middle_width   = $middle_width;
                            $material_stock->end_width      = $end_width;
                            $material_stock->actual_width   = $actual_width;
                        }

                        $material_stock->reserved_qty       = $reserved_qty;
                        $material_stock->available_qty      = $available_qty;
                        $material_stock->actual_length      = $actual_length;
                        $material_stock->different_yard     = $actual_length - $material_stock->stock;
                        $material_stock->is_used_to_relax   = true;
                        $material_stock->last_date_used     = null;
                        $material_stock->last_status        = null;
                        $material_stock->last_user_used_id  = null;
                        $material_stock->ip_address         = null;

                        if($available_qty <= 0.000)
                        {
                            $source_locator []              = $old_locator;
                            $material_stock->available_qty  = 0;
                            $material_stock->locator_id     = null;
                            $material_stock->is_allocated   = true;
                            $material_stock->is_active      = false;
                            $material_stock->deleted_at     = carbon::now();
                        }

                        $is_detail_exists = DetailMaterialPreparationFabric::where([
                            ['material_preparation_fabric_id',$material_preparation_fabric_id],
                            ['material_stock_id',$material_stock_id],
                            ['barcode',$barcode],
                            ['reserved_qty',$qty_booking]
                        ])
                        ->exists();

                        if(!$is_detail_exists)
                        {
                            $material_preparation_fabric        = MaterialPreparationFabric::find($material_preparation_fabric_id);
                            $detail_material_preparation_fabric = DetailMaterialPreparationFabric::UpdateOrCreate([
                                'material_preparation_fabric_id'        => $material_preparation_fabric_id,
                                'material_stock_id'                     => $material_stock_id,
                                'barcode'                               => $barcode,
                                'referral_code'                         => $referral_code,
                                'sequence'                              => $sequence,
                                'uom'                                   => $uom,
                                'item_id'                               => $item_id,
                                'item_code'                             => $item_code,
                                'color'                                 => $material_stock->color,
                                'upc_item'                              => $material_stock->upc_item,
                                'begin_width'                           => $begin_width,
                                'middle_width'                          => $middle_width,
                                'end_width'                             => $end_width,
                                'actual_width'                          => $actual_width,
                                'actual_length'                         => $actual_length,
                                'actual_lot'                            => $material_stock->load_actual,
                                'actual_preparation_date'               => $actual_planning_date,
                                'last_movement_date'                    => $movement_date,
                                'preparation_date'                      => $movement_date,
                                'last_locator_id'                       => $locator_id,
                                'last_status_movement'                  => 'relax',
                                'last_user_movement_id'                 => auth::user()->id,
                                'warehouse_id'                          => $warehouse_id,
                                'reserved_qty'                          => $qty_booking,
                                'deleted_at'                            => null,
                                'user_id'                               => auth::user()->id,
                            ]);

                            if($available_qty > 0) $return_print [] = $detail_material_preparation_fabric->id;

                            if($is_additional == true) $note = 'DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK ADDITIONAL OLEH '.strtoupper(auth::user()->name);
                            else $note = ' DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK PCD '.$planning_date.' OLEH '.strtoupper(auth::user()->name).' UNTUK STYLE '.$material_preparation_fabric->_style.', DAN ARTICLE '.$material_preparation_fabric->article_no;

                            if($detail_material_preparation_fabric->barcode != 'BELUM DIPRINT')
                            {
                                MovementStockHistory::UpdateOrCreate([
                                    'material_stock_id' => $material_stock_id,
                                    'from_location'     => $old_locator,
                                    'to_destination'    => $locator_id,
                                    'status'            => 'relax',
                                    'uom'               => $uom,
                                    'qty'               => $qty_booking,
                                    'movement_date'     => $movement_date,
                                    'note'              => $note.' ( NOMOR SERI BARCODE '.$barcode.')',
                                    'user_id'           => auth::user()->id
                                ]);
                            }

                            $flag_insert++;
                            $material_preparation_fabric->machine = $machine;
                            $array_1        []                    = $material_preparation_fabric_id;
                            $update_summary []                    = $detail_material_preparation_fabric->id;
                            $material_stock->save();
                            $material_preparation_fabric->save();
                            
                        }
                    }
                }

                foreach ($source_locator as $key => $value) 
                {
                    $total_item_on_locator = MaterialStock::whereNotNull('locator_id')
                    ->where('locator_id',$value)
                    ->whereNull('deleted_at')
                    ->count();
    
                    $counter                = Locator::find($value);
                    if($counter)
                    {
                        $counter->counter_in    = $total_item_on_locator;
                        $counter->save();
                    }
                    
                }

                if(count($array_1) > 0) $this->updateMaterialPreparationFabrics($array_1);
                //if($flag_insert > 0) $this->updateSummaryStockFabric($update_summary);
                
                
                DB::commit();
               
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            $total_item_on_locator_1  = MaterialStock::where('locator_id',$locator_id)
            ->whereNull('deleted_at')
            ->count();

            $total_item_on_locator_2  = DetailMaterialPreparationFabric::where('last_locator_id',$locator_id)
            ->whereNull('deleted_at')
            ->count();

            $locator->counter_in    = $total_item_on_locator_1 + $total_item_on_locator_2;
            $locator->save();
            

            if(count($return_print) == 0) return response()->json('no_need_print',200);
            else return response()->json($return_print,200);
            
           
        }else{
            return response()->json('data tidak ditemukan.',422);
        }


    }

    public function update(Request $request)
    {
        $data = json_decode(json_encode($request->obj,true));
        $material_stock_id = $data->material_stock_id;
        $material_preparation_id = $data->id;
        $actual_load = $data->actual_load;
        $actual_width = $data->actual_width;
        $addition_qty = $data->addition_qty;
        $qty_booking_actual = $data->qty_booking_actual;
        $available_qty = $data->available_qty;

        $material_stock = MaterialStock::find($material_stock_id);
        $material_preparation = MaterialPreparationFabric::find($material_preparation_id);

        $material_stock->available_qty = $available_qty;
        $material_stock->reserved_qty = $qty_booking_actual;
        $material_preparation->qty_booking_actual = $qty_booking_actual;
        $material_preparation->addition_qty = $addition_qty;
        $material_preparation->rilex_date = carbon::now();
        $material_preparation->user_rilex_id = auth::user()->id;


        if($actual_width){
            $material_stock->actual_width = $actual_width;
            $material_preparation->actual_width = $actual_width;
        }

        if($actual_load){
            $material_stock->load_actual = $actual_load;
            $material_preparation->load_actual = $actual_load;
        }

        $material_stock->last_status = null;
        $material_stock->last_user_used_id = null;
        $material_stock->last_date_used = null;
        $material_stock->save();
        $material_preparation->save();


        return response()->json(200);

    }

    public function resetRilex(Request $request)
    {
        try 
        {
            DB::beginTransaction();
            MaterialStock::where([
                ['last_user_used_id',auth::user()->id],
                ['last_status','relax'],
            ])
            ->update([
                'last_user_used_id' => null,
                'last_status' => null,
                'last_date_used' => null,
            ]);
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json(200);
    }

    public function removeStatusPreparation(Request $request)
    {
        try 
        {
            DB::beginTransaction();
            $material_preparation_fabric                    = MaterialPreparationFabric::find($request->id);
            $material_preparation_fabric->preparation_date  = null;
            $material_preparation_fabric->preparation_by    = null;
            $material_preparation_fabric->save();
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json(200);
    }

    public function printBarcode(Request $request)
    {
        $list_barcodes                  = json_decode($request->list_barcodes);
        $barcode_preparation_fabrics    = DetailMaterialPreparationFabric::wherein('id',$list_barcodes)->get();

        foreach ($barcode_preparation_fabrics as $key => $barcode_preparation_fabric) 
        {
           if($barcode_preparation_fabric->barcode =='BELUM DIPRINT')
           {
                $get_barcode            = $this->randomCode($barcode_preparation_fabric->materialStock->barcode_supplier);
                $barcode                = $get_barcode->barcode;
                $referral_code          = $get_barcode->referral_code;
                $sequence               = $get_barcode->sequence;
                
                $warehouse_id           = $barcode_preparation_fabric->materialStock->warehouse_id;
                $is_additional          = $barcode_preparation_fabric->materialPreparationFabric->is_from_additional;
                $planning_date          = $barcode_preparation_fabric->materialPreparationFabric->planning_date;
                $qty_booking            = $barcode_preparation_fabric->reserved_qty;
                

                $inventory  = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                $relax = Locator::whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                if($is_additional == true) $note = 'DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK ADDITIONAL OLEH '.strtoupper($barcode_preparation_fabric->user->name);
                else $note = ' DIGUNAKAN SEBANYAK '.$qty_booking.' UNTUK PCD '.$planning_date.' OLEH '.strtoupper($barcode_preparation_fabric->user->name);


                MovementStockHistory::create([
                    'material_stock_id' => $barcode_preparation_fabric->material_stock_id,
                    'from_location'     => $inventory->id,
                    'to_destination'    => $relax->id,
                    'status'            => 'relax',
                    'uom'               => $barcode_preparation_fabric->uom,
                    'qty'               => $barcode_preparation_fabric->reserved_qty,
                    'movement_date'     => $barcode_preparation_fabric->created_at,
                    'note'              => $note.' ( NOMOR SERI BARCODE '.$barcode.')',
                    'user_id'           => $barcode_preparation_fabric->user_id
                ]);

                $barcode_preparation_fabric->barcode        = $barcode;
                $barcode_preparation_fabric->referral_code  = $referral_code;
                $barcode_preparation_fabric->sequence       = $sequence;
                $barcode_preparation_fabric->save();
           }
        }
        //return response()->json($material_arrivals);
        return view('fabric_material_preparation.barcode',compact('barcode_preparation_fabrics'));

    }

    private function updateMaterialPreparationFabrics($material_preparation_fabrics)
    {
        try 
        {
            DB::beginTransaction();
            foreach ($material_preparation_fabrics as $key => $material_preparation_fabric) 
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_fabric('".$material_preparation_fabric."');"));
            }


            $total_preparations = DetailMaterialPreparationFabric::select('material_preparation_fabric_id',db::raw('sum(reserved_qty) as total_qty_rilex'))
            ->whereIn('material_preparation_fabric_id',$material_preparation_fabrics)
            ->groupby('material_preparation_fabric_id')
            ->get();

            $array          = array();
            $relax_date     = carbon::now();
            foreach ($total_preparations as $key => $total_preparation) 
            {
                $material_preparation_fabric_id     = $total_preparation->material_preparation_fabric_id;
                $material_preparation_fabric        = MaterialPreparationFabric::find($material_preparation_fabric_id);
                $total_reserved_qty                 = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                $total_qty_rilex                    = sprintf('%0.8f',$total_preparation->total_qty_rilex);
                
                if($total_reserved_qty < $total_qty_rilex) $_total_reserved_qty = $total_qty_rilex;
                else $_total_reserved_qty = $total_reserved_qty;

                $new_total_qty_outstanding = sprintf('%0.8f',$_total_reserved_qty - $total_qty_rilex);

                if($new_total_qty_outstanding <= 0)
                {

                    $material_preparation_fabric->total_qty_outstanding = 0;
                    $material_preparation_fabric->relax_date            = $relax_date;
                 }else
                {
                    $material_preparation_fabric->total_qty_outstanding = $new_total_qty_outstanding;
                }

                $material_preparation_fabric->total_reserved_qty = sprintf('%0.8f',$_total_reserved_qty);
                $material_preparation_fabric->total_qty_rilex    = sprintf('%0.8f',$total_qty_rilex);
                $material_preparation_fabric->remark_planning    = null;
                $material_preparation_fabric->preparation_date   = null;
                $material_preparation_fabric->preparation_by     = null;
                $material_preparation_fabric->save();

                $array []                                        = $material_preparation_fabric_id;
                
            }

            $this->insertPoBuyerPerRoll($array);
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertPoBuyerPerRoll($array)
    {
        $material_preparation_fabrics           = MaterialPreparationFabric::whereIn('id',$array)->get();
        foreach ($material_preparation_fabrics as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_preparation_fabric_id         = $value->id;
                
                DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($material_preparation_fabric_id)
                {
                    $query->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$material_preparation_fabric_id);
                })
                ->delete();
                
                FabricReportDailyMaterialPreparation::insertRePrepareRollToBuyer($material_preparation_fabric_id,0);

                DB::commit();
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
       

    }

    static function updateSummaryStockFabric($update_summary)
    {
        $summary_stocks = DetailMaterialPreparationFabric::select('material_stocks.summary_stock_fabric_id',db::raw('sum(COALESCE(detail_material_preparation_fabrics.reserved_qty,0)) as reserved_qty'))
        ->leftjoin('material_stocks','material_stocks.id','detail_material_preparation_fabrics.material_stock_id')
        ->whereIn('detail_material_preparation_fabrics.id',$update_summary)
        ->groupby('material_stocks.summary_stock_fabric_id')
        ->get();
        
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($summary_stocks as $key => $summary_stock) 
            {
                $id                         = $summary_stock->summary_stock_fabric_id;
                $reserved_qty              = $summary_stock->reserved_qty;
                $summary_stock_fabric_data = SummaryStockFabric::find($id);

                if($summary_stock_fabric_data)
                {
                    $curr_reserved_qty                          = sprintf('%0.8f',$summary_stock_fabric_data->reserved_qty);
                    $stock                                      = sprintf('%0.8f',$summary_stock_fabric_data->stock);
                    $new_reserved_qty                           = sprintf('%0.8f',$reserved_qty + $curr_reserved_qty) ;
                    $available_qty                              = sprintf('%0.8f',$stock - $new_reserved_qty);
                    $summary_stock_fabric_data->reserved_qty    = $new_reserved_qty; 
                    $summary_stock_fabric_data->available_qty   = $available_qty; 
                    $summary_stock_fabric_data->save();
                }
            
            }
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function getUserIpAddr()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';    
        return $ipaddress;
    }

    static function randomCode($barcode)
    {
        $sequence = Barcode::where('referral_code',$barcode)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'           => $barcode.'-'.$sequence,
            'referral_code'     => $barcode,
            'sequence'          => $sequence,
        ]);
        
        $obj                = new stdClass();
        $obj->barcode       = $barcode.'-'.$sequence;
        $obj->referral_code = $barcode;
        $obj->sequence      = $sequence;

        return $obj;

    }

    public function getPlanningPerDateBalanceMarker(Request $request)
    {
        try 
        {
            DB::beginTransaction();
            //dd($request->toArray());
            $c_order_id     = $request->c_order_id;
            $item_code      = $request->item_code;
            $item_id_source = $request->item_id_source;
            $c_bpartner_id  = $request->c_bpartner_id;
            $document_no    = $request->document_no;
            $style          = $request->style;
            $article_no     = $request->article_no;
            $warehouse_id   = $request->warehouse_id;
            $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
            
            $material_preparation_fabric = MaterialPreparationFabric::where([
                ['c_order_id',$c_order_id],
                ['item_id_source',$item_id_source],
                ['planning_date',$planning_date],
                ['is_balance_marker', true],
                ['is_piping',false],
                ['article_no',$article_no],
                ['_style',$style],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->orderby('planning_date','asc')
            ->first();

            //dd($material_preparation_fabric);

            if($material_preparation_fabric)
            {
                // if(auth::user()->warehouse == '1000011')
                // {
                    $allocation = MaterialPreparationFabric::select('document_no'
                        ,'c_order_id'
                        ,'_style'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'planning_date'
                        ,'c_bpartner_id'
                        ,'is_piping'
                        ,db::raw("sum(total_reserved_qty) as qty_prepare"))
                    ->where([
                        ['c_order_id',$c_order_id],
                        ['item_id_source',$item_id_source],
                        ['planning_date',$planning_date],
                        ['is_balance_marker', true],
                        ['is_piping',false],
                        ['_style',$style],
                        ['article_no',$article_no],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->groupby('document_no'
                        ,'c_order_id'
                        ,'_style'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_book'
                        ,'item_id_source'
                        ,'planning_date'
                        ,'c_bpartner_id'
                        ,'is_piping')
                    ->first();

                    if($allocation)
                    {
                        $qty_prepare        = $allocation->qty_prepare;
                        $total_reserved_qty = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                        $total_qty_relax    = sprintf('%0.8f',$material_preparation_fabric->total_qty_rilex);
                        if($qty_prepare != $total_reserved_qty)
                        {
                            $new_qty_oustanding = sprintf('%0.8f',$qty_prepare - $total_qty_relax);
                           
                            $material_preparation_fabric->total_reserved_qty    = $qty_prepare;
                            $material_preparation_fabric->total_qty_outstanding = $new_qty_oustanding;
                            $material_preparation_fabric->save();
                        }
                        $update_preparation = MaterialPreparationFabric::find($material_preparation_fabric->id);
                        return response()->json($update_preparation,200);

                    }else return response()->json($material_preparation_fabric,200);
                }else return response()->json($material_preparation_fabric,200);

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    public function getPlanningPerArticleBalanceMarker(Request $request)
    {
        $item_id        = $request->item_id;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');
        $c_order_id     = $request->c_order_id;

        $articles       = MaterialPreparationFabric::select('article_no')
        ->where([
            ['is_piping', false],
            ['item_id_source',$item_id],
            ['is_balance_marker', true],
            ['warehouse_id',$warehouse_id],
            ['planning_date',$planning_date],
            ['c_order_id', $c_order_id],
        ])
        ->pluck('article_no','article_no')
        ->all();

        //dd($articles);

        if(count($articles) > 0)
        {
            return response()->json([
                'articles'  => $articles,
                'total'     => count($articles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }

    public function getPlanningPerStyleBalanceMarker(Request $request)
    {
        //dd($request->toArray());
        $article_no     = $request->article_no;
        $warehouse_id   = $request->warehouse_id;
        $planning_date  = Carbon::createFromFormat('d/m/Y', $request->planning_date)->format('Y-m-d');

        $styles         = MaterialPreparationFabric::select('_style')
                          ->where([
                                ['is_piping', false],
                                ['is_balance_marker', true],
                                ['article_no',$article_no],
                                ['planning_date',$planning_date],
                                ['warehouse_id',$warehouse_id],
                         ])
                         ->whereNotNull('_style')
                         ->pluck('_style','_style')
                         ->all();
        //dd($styles);
        if(count($styles) > 0)
        {
            return response()->json([
                'styles'    => $styles,
                'total'     => count($styles),
                
            ],200);
        }else
        {
            return response()->json('Planning not found',422);
        }
    }

}
