<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;
 
use App\Models\User;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Locator;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\AllocationItem;
use App\Models\AutoAllocation;
use App\Models\ReroutePoBuyer;
use App\Models\MaterialBacklog;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialStock;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesAllocationBuyerApprovalController extends Controller
{
    public function index()
    {
        //return view('errors.503');
        $list_users = User::whereIn('id',function($query){
            $query->select('user_id')
            ->from('allocation_items')
            ->where('warehouse',auth::user()->warehouse)
            ->groupby('user_id');
        })
        ->where([
            ['warehouse',auth::user()->warehouse],
            ['name','!=','system'],
        ])
        ->orderBy('name')
        ->pluck('users.name','users.id')
        ->all();

        $active_tab = 'waiting';
        return view('accessories_allocation_buyer_approval.index',compact('list_users','active_tab'));
    }

    public function dataWaitingForApproved(Request $request)
    {
        //return view('errors.503');
        $warehouse_id   = $request->warehouse;
        $user           = $request->user;

        $allocations = AllocationItem::select(
            'id',
            'created_at',
            'user_id',
            'confirm_date',
            'supplier_code',
            'supplier_name',
            'document_no',
            'item_code',
            'item_desc',
            'category',
            'lc_date',
            'po_buyer',
            'old_po_buyer_reroute',
            'uom',
            'job',
            'style',
            'qty_booking',
            'warehouse',
            'type_stock_buyer',
            'type_stock_material',
            'is_additional',
            'is_additional',
            'confirm_by_warehouse',
            'remark'
        )
        ->whereNull('deleted_at')
        ->where('is_not_allocation',false)
        ->where('warehouse','LIKE',"%$warehouse_id%");

        if($user) $allocations = $allocations->where('user_id',$user);

        $allocations = $allocations->whereNull('confirm_by_warehouse')
        ->orderby('created_at','desc')
        ->whereNull('purchase_item_id');

        return DataTables::of($allocations)
        ->editColumn('is_additional',function ($allocations)
        {
            if($allocations->is_additional) return '<span class="label label-success">Additional</span>';
            else return null;
        })
        ->editColumn('created_at',function ($allocations)
        {
            return $allocations->created_at->format('d/M/Y H:i:s');
        })
        ->editColumn('confirm_date',function ($allocations)
        {
            if($allocations->confirm_date) return $allocations->confirm_date->format('d/M/Y H:i:s');
            else null;
        })
        ->editColumn('user_id',function ($allocations)
        {
            return strtoupper($allocations->user->name);
        })
        ->editColumn('warehouse',function ($allocations)
        {
            if($allocations->warehouse == '1000002') return 'Warehouse accessories AOI 1';
            else return 'Warehouse accessories AOI 2';
        })
        ->addColumn('qty_booking',function ($allocations)
        {
            return number_format($allocations->qty_booking, 4, '.', ',');
        })
        ->addColumn('barcode_status',function ($allocations)
        {
            if($allocations->material_preparation_id != null)return '<span class="label label-success">Printed</span>';
            else return '<span class="label label-default">Not printed</span>';
        })
        ->addColumn('action', function($allocations) 
        {
            if($allocations->confirm_by_warehouse == null)
            {
                return view('_action', [
                    'model'     => $allocations,
                    'approve'   => route('accessoriesAllocationBuyerApproval.approved',$allocations->id),
                    'reject'    => route('accessoriesAllocationBuyerApproval.reject',$allocations->id)
                ]);
            }
        })
        ->setRowAttr([
            'style' => function($allocations) 
            {
                $is_exists = PoBuyer::where('po_buyer',$allocations->po_buyer)->whereNotNull('cancel_date')->exists();
                if($is_exists) return  'background-color: red;color:white';
            },
        ])
        ->rawColumns(['confirm_by_warehouse','action','is_additional','style','barcode_status'])
        ->make(true);
    } 

    public function dataApproved(Request $request)
    {
        //return view('errors.503');
        $warehouse_id   = $request->warehouse;

        $allocations = AllocationItem::
        select(
            'id',
            'created_at',
            'user_id',
            'confirm_date',
            'supplier_code',
            'supplier_name',
            'document_no',
            'item_code',
            'item_desc',
            'category',
            'po_buyer',
            'old_po_buyer_reroute',
            'lc_date',
            'uom',
            'job',
            'style',
            'qty_booking',
            'warehouse',
            'is_additional',
            'type_stock_buyer',
            'type_stock_material',
            'confirm_by_warehouse',
            'remark'
        )
        ->whereNull('deleted_at')
        ->where('is_not_allocation',false)
        ->where('warehouse','LIKE',"%$warehouse_id%")
        ->where('confirm_by_warehouse','approved')
        ->orderby('created_at','desc')
        ->whereNull('purchase_item_id');

        return DataTables::of($allocations)
        ->editColumn('confirm_by_warehouse',function ($allocations)
        {
            if($allocations->confirm_by_warehouse == 'approved') return '<span class="label label-success">Approved</span>';
            else if($allocations->confirm_by_warehouse == 'reject') return '<span class="label label-danger">Reject</span>';
            else if($allocations->confirm_by_warehouse == 'cancel') return '<span class="label label-danger">Cancel</span>';
            else return '<span class="label label-default">Waiting for approval</span>';
        })
        ->editColumn('created_at',function ($allocations)
        {
            return $allocations->created_at->format('d/M/Y H:i:s');
        })
        ->editColumn('confirm_date',function ($allocations)
        {
            if($allocations->confirm_date) return $allocations->confirm_date->format('d/M/Y H:i:s');
            else null;
        })
        ->editColumn('is_additional',function ($allocations){
            if($allocations->is_additional) return '<span class="label label-success">Additional</span>';
            else return null;
        })
        ->addColumn('qty_booking',function ($allocations)
        {
            return number_format($allocations->qty_booking, 4, '.', ',');
        })
        ->EditColumn('user_id',function ($allocations){
            return strtoupper($allocations->user->name);
        })
        ->editColumn('warehouse',function ($allocations){
            if($allocations->warehouse == '1000002') return 'Warehouse accessories AOI 1';
            else return 'Warehouse accessories AOI 2';
            
        })
        ->addColumn('barcode_status',function ($allocations)
        {
            if($allocations->material_preparation_id != null)return '<span class="label label-success">Printed</span>';
            else return '<span class="label label-default">Not printed</span>';
        })
        ->addColumn('action', function($allocations) {
            return view('_action', [
                'model' => $allocations,
                'cancel' => route('accessoriesAllocationBuyerApproval.cancelBooking',$allocations->id)
            ]);
        })
        ->setRowAttr([
            'style' => function($allocations) 
            {
                $is_exists = PoBuyer::where('po_buyer',$allocations->po_buyer)->whereNotNull('cancel_date')->exists();
                if($is_exists) return  'background-color: red;color:white';
            },
        ])
        ->rawColumns(['confirm_by_warehouse','action','is_additional','style','barcode_status'])
        ->make(true);
    } 

    public function approveAll(Request $request)
    {
        //return view('errors.503');
        $flag_update = 0;
        $rows_update = '';
        $allocation_items = AllocationItem::where('warehouse',auth::user()->warehouse)
        ->whereNull('deleted_at')
        ->whereNull('confirm_by_warehouse')
        ->whereNull('confirm_date');

        if($request->has('selected_user')) $allocation_items = $allocation_items->where('user_id',$request->selected_user);
        $allocation_items = $allocation_items->get();

        //return response()->json($allocation_items);
        foreach ($allocation_items as $key => $allocation_item) {
            $rows_update .= "("
                ."'". $allocation_item->id . "'".
            "),";
            $flag_update++;

        }


        if($flag_update > 0
        ){
            $rows_update = substr_replace($rows_update, "", -1);
            try{
                db::beginTransaction();
                db::select(db::raw("
                    update allocation_items as tbl
                    set
                        confirm_by_warehouse = 'approved',
                        confirm_date = '".carbon::now()."',
                        confirm_user_id = ".auth::user()->id."
                    from (values
                        " . $rows_update . "
                    ) as col(
                        id
                    )
                    where col.id = tbl.id;
                "));

                db::commit();

            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
        }

        return response()->json(200);

    }

    public function approved(Request $request,$id)
    {
        //return view('errors.503');
        try 
        {
            DB::beginTransaction();
            $allocation_item                        = AllocationItem::find($id);
            $allocation_item->confirm_by_warehouse  = 'approved';
            $allocation_item->confirm_date          = carbon::now();
            $allocation_item->confirm_user_id       = auth::user()->id;
            $allocation_item->save();
            DB::commit();
                    
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        return response()->json(200);
    }

    public function reject(Request $request,$id)
    {
        //return view('errors.503');
        $upload_date                = Carbon::now()->toDateTimeString();;
        $allocation_item            = AllocationItem::find($id);
        $auto_allocation_id         = $allocation_item->auto_allocation_id;
        $material_stock             = MaterialStock::find($allocation_item->material_stock_id);
        
        $free_stock_destination     = Locator::with('area')
        ->whereHas('area',function ($query){
            $query->where([
                ['warehouse',auth::user()->warehouse],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();
        
        $stock                  =  sprintf('%0.8f',$material_stock->stock);
        $reserved_qty           =  sprintf('%0.8f',$material_stock->reserved_qty);
        $new_reserverd_qty      =  sprintf('%0.8f',$reserved_qty - $allocation_item->qty_booking);
        $new_available_qty      =  sprintf('%0.8f',$stock - $new_reserverd_qty);
        $type_stock             = $material_stock->type_stock;
        $type_stock_erp_code    = $material_stock->type_stock_erp_code;

        try 
        {
            DB::beginTransaction();

            if($auto_allocation_id)
            {
                $auto_allocation                    = AutoAllocation::find($auto_allocation_id);
                $qty_auto_allocation_outstanding    = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_auto_allocation_allocated      = sprintf('%0.8f',$auto_allocation->qty_allocated);
                $new_qty_outstanding                = sprintf('%0.8f',$qty_auto_allocation_outstanding + $allocation_item->qty_booking);
                $new_qty_allocated                  = sprintf('%0.8f',$qty_auto_allocation_allocated - $allocation_item->qty_booking);

                if($new_qty_outstanding > 0)
                {
                    $auto_allocation->is_already_generate_form_booking  = false;
                    $auto_allocation->generate_form_booking             = null; 
                }
                
                $auto_allocation->qty_outstanding   = $new_qty_outstanding;
                $auto_allocation->qty_allocated     = $new_qty_allocated;
                $auto_allocation->save();
            }

            $allocation_item->confirm_by_warehouse  = 'reject';
            $allocation_item->confirm_date          = carbon::now();
            $allocation_item->confirm_user_id       = auth::user()->id;
            $allocation_item->save();

            if($material_stock->is_closing_balance == false)
            {
                $old_available_qty      = sprintf('%0.8f',$material_stock->available_qty);
                $_new_available_qty     = sprintf('%0.8f',$old_available_qty + $allocation_item->qty_booking);

                $material_stock->reserved_qty   = $new_reserverd_qty;
                $material_stock->available_qty  = $new_available_qty;

                if($new_available_qty > 0)
                {
                    $material_stock->is_allocated = false;
                    $material_stock->is_active = true;
                }
                $material_stock->save();
                
                HistoryStock::approved($allocation_item->material_stock_id
                ,$_new_available_qty
                ,$old_available_qty
                ,(-1*$allocation_item->qty_booking)
                ,$allocation_item->po_buyer
                ,$allocation_item->style
                ,$allocation_item->article_no
                ,$allocation_item->lc_date
                ,'CANCEL ALLOCATION'
                ,auth::user()->name
                ,auth::user()->id
                ,$type_stock_erp_code
                ,$type_stock
                ,false
                ,true);
                        
                $material_stock->save();
            }else
            {
                $document_no    = $material_stock->document_no;
                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id = null;
                            $type_stock_erp_code = null;
                            $type_stock = null;
                        }
                        
                    }
                }

                $is_exists = MaterialStock::where([
                    'type_stock_erp_code'       => $type_stock_erp_code,
                    'document_no'               => $material_stock->document_no,
                    'c_order_id'                => $material_stock->c_order_id,
                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                    'locator_id'                => $free_stock_destination->id,
                    'supplier_name'             => trim(strtoupper($material_stock->supplier_name)),
                    'po_buyer'                  => $allocation_item->po_buyer,
                    'item_id'                   => $material_stock->item_id,
                    'item_code'                 => $material_stock->item_code,
                    'type_po'                   => 2,
                    'warehouse_id'              => auth::user()->warehouse,
                    'source'                    => 'CANCEL BOOKING',
                    'uom'                       => $material_stock->uom,
                    'is_stock_already_created'  => false,
                    'is_material_others'        => true,
                ])
                ->whereNull('approval_date')
                ->first();

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstorCreate([
                        'type_stock_erp_code'   =>  $type_stock_erp_code,
                        'mapping_stock_id'      =>  $mapping_stock_id,
                        'type_stock'            =>  $type_stock,
                        'c_order_id'            =>  $material_stock->c_order_id,
                        'c_bpartner_id'         =>  $material_stock->c_bpartner_id,
                        'locator_id'            =>  $free_stock_destination->id,
                        'supplier_code'         =>  $material_stock->supplier_code,
                        'supplier_name'         =>  trim(strtoupper($material_stock->supplier_name)),
                        'document_no'           =>  $allocation_item->document_no,
                        'po_buyer'              =>  $allocation_item->po_buyer,
                        'item_id'               =>  $material_stock->item_id,
                        'item_code'             =>  $material_stock->item_code,
                        'item_desc'             =>  $material_stock->item_desc,
                        'category'              =>  $material_stock->category,
                        'type_po'               =>  2,
                        'warehouse_id'          =>  auth::user()->warehouse,
                        'uom'                   =>  $material_stock->uom,
                        'qty_carton'            =>  1,
                        'stock'                 =>  $allocation_item->qty_booking,
                        'reserved_qty'          =>  0,
                        'available_qty'         =>  $allocation_item->qty_booking,
                        'is_active'             =>  true,
                        'is_material_others'    =>  true,
                        'created_at'            => $upload_date,
                        'updated_at'            => $upload_date,
                        'source'                =>  'CANCEL BOOKING',
                        'user_id'               =>  Auth::user()->id
                    ]);
                    $material_stock_id = $material_stock->id;

                    if($material_stock->po_buyer) $note_po_buyer = 'ASAL STOCK DARI PO BUYER '.$material_stock->po_buyer;
                    else $note_po_buyer = null;
                    $locator_id = $material_stock->locator_id;

                }else
                {
                    $material_stock_id          = $is_exists->id;
                    $locator_id                 = $is_exists->locator_id;
                    $stock                      = sprintf('%0.8f',$is_exists->stock);
                    $reserved_qty               = sprintf('%0.8f',$is_exists->reserved_qty);
                    $new_stock                  = sprintf('%0.8f',$stock + $allocation_item->qty_booking);

                    if($is_exists->po_buyer) $note_po_buyer = 'ASAL STOCK DARI PO BUYER '.$is_exists->po_buyer;
                    else $note_po_buyer = null;
                    
                    

                    $is_exists->stock           = $new_stock;
                    $is_exists->available_qty   = sprintf('%0.8f',$new_stock - $reserved_qty);
                    $is_exists->save();

                }

                DetailMaterialStock::FirstOrCreate([
                    'material_preparation_id'   => $material_preapration_id,
                    'material_stock_id'         => $material_stock_id,
                    'remark'                    => 'CANCEL BOOKING, '.$note_po_buyer,
                    'uom'                       => $material_stock->uom,
                    'qty'                       => sprintf('%0.8f',$allocation_item->qty_booking),
                    'user_id'                   => Auth::user()->id,
                    'locator_id'                => $locator_id,
                    'created_at'                => $upload_date,
                    'updated_at'                => $upload_date
                ]);

                $counter_in                         = $free_stock_destination->counter_in;
                $free_stock_destination->counter_in = $counter_in +1;
                $free_stock_destination->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json(200);
    }

    public function cancelBooking(Request $request,$id)
    {
        return view('errors.503');
        $upload_date            = Carbon::now()->toDateTimeString();
        $cancel_reason          = $request->cancel_reason;
        $allocation_item        = AllocationItem::find($id);
        $material_stock         = MaterialStock::find($allocation_item->material_stock_id);
        //$detail_material_preparation = DetailMaterialPreparation::where('allocation_item_id',$allocation_item->id)->first();

        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query){
            $query->where([
                ['warehouse',auth::user()->warehouse],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $qty_booking            = sprintf('%0.8f',$allocation_item->qty_booking);
        $stock                  = sprintf('%0.8f',$material_stock->stock);
        $old_available_qty      = sprintf('%0.8f',$material_stock->available_qty);
        $reserved_qty           = sprintf('%0.8f',$material_stock->reserved_qty);
        $type_stock             = sprintf('%0.8f',$material_stock->type_stock);
        $type_stock_erp_code    = $material_stock->type_stock_erp_code;

        $new_reserverd_qty      = sprintf('%0.8f',$reserved_qty - $qty_booking);
        $new_available_qty      = sprintf('%0.8f',$stock - $new_reserverd_qty);
        $_new_available_qty     = sprintf('%0.8f',$old_available_qty + $qty_booking);

        try 
        {
            DB::beginTransaction();

            if($allocation_item->auto_allocation_id)
            {
                $auto_allocation        = AutoAllocation::find($allocation_item->auto_allocation_id);
                $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);

                $new_qty_allocated      = sprintf('%0.8f',$qty_allocated - $qty_booking);
                $new_qty_outstanding    = sprintf('%0.8f',$qty_outstanding + $qty_booking);

                if($new_qty_outstanding > 0)
                {
                    $auto_allocation->generate_form_booking = null;
                    $auto_allocation->is_already_generate_form_booking = false;
                }

                $auto_allocation->qty_outstanding   = $new_qty_outstanding;
                $auto_allocation->qty_allocated     = $new_qty_allocated;
                $auto_allocation->save();
            }

            $allocation_item->cancel_reason         = $cancel_reason;
            $allocation_item->confirm_by_warehouse  = 'cancel';
            $allocation_item->cancel_date           = carbon::now();
            $allocation_item->cancel_user_id        = auth::user()->id;
            $allocation_item->save();

            if($material_stock->is_closing_balance == false)
            {
                $material_stock->reserved_qty   = sprintf('%0.8f',$new_reserverd_qty);
                $material_stock->available_qty  = sprintf('%0.8f',$new_available_qty);

                if($new_available_qty > 0)
                {
                    $material_stock->is_allocated   = false;
                    $material_stock->is_active      = true;
                }

                HistoryStock::approved($allocation_item->material_stock_id
                ,$_new_available_qty
                ,$old_available_qty
                ,(-1*$allocation_item->qty_booking)
                ,$allocation_item->po_buyer
                ,$allocation_item->style
                ,$allocation_item->article_no
                ,$allocation_item->lc_date
                ,'CANCEL ALLOCATION'
                ,auth::user()->name
                ,auth::user()->id
                ,$type_stock_erp_code
                ,$type_stock
                ,false
                ,true);

                $material_stock->save();
            }else
            {
                $document_no = $material_stock->document_no;
                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = NULL;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                    }
                }

                $is_exists = MaterialStock::where([
                    'type_stock_erp_code'       => $type_stock_erp_code,
                    'document_no'               => $material_stock->document_no,
                    'c_order_id'                => $material_stock->c_order_id,
                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                    'locator_id'                => $free_stock_destination->id,
                    'supplier_name'             => trim(strtoupper($material_stock->supplier_name)),
                    'po_buyer'                  => $allocation_item->po_buyer,
                    'item_id'                   => $material_stock->item_id,
                    'item_code'                 => $material_stock->item_code,
                    'type_po'                   => 2,
                    'warehouse_id'              => auth::user()->warehouse,
                    'source'                    => 'CANCEL BOOKING',
                    'uom'                       => $material_stock->uom,
                    'is_stock_already_created'  => false,
                    'is_material_others'        => true,
                ])
                ->whereNull('approval_date')
                ->first();

                if(!$is_exists)
                {
                    $material_stock = MaterialStock::FirstorCreate([
                        'type_stock_erp_code'   =>  $type_stock_erp_code,
                        'mapping_stock_id'      =>  $mapping_stock_id,
                        'type_stock'            =>  $type_stock,
                        'c_order_id'            =>  $material_stock->c_order_id,
                        'c_bpartner_id'         =>  $material_stock->c_bpartner_id,
                        'locator_id'            =>  $free_stock_destination->id,
                        'supplier_code'         =>  $material_stock->supplier_code,
                        'supplier_name'         =>  trim(strtoupper($material_stock->supplier_name)),
                        'document_no'           =>  $allocation_item->document_no,
                        'po_buyer'              =>  $allocation_item->po_buyer,
                        'item_id'               =>  $material_stock->item_id,
                        'item_code'             =>  $material_stock->item_code,
                        'item_desc'             =>  $material_stock->item_desc,
                        'category'              =>  $material_stock->category,
                        'type_po'               =>  2,
                        'warehouse_id'          =>  auth::user()->warehouse,
                        'uom'                   =>  $material_stock->uom,
                        'qty_carton'            =>  1,
                        'stock'                 =>  $allocation_item->qty_booking,
                        'reserved_qty'          =>  0,
                        'available_qty'         =>  $allocation_item->qty_booking,
                        'is_active'             =>  true,
                        'is_material_others'    =>  true,
                        'source'                =>  'CANCEL BOOKING',
                        'user_id'               =>  Auth::user()->id,
                        'created_at'            => $upload_date,
                        'updated_at'            => $upload_date
                    ]);

                    $material_stock_id = $material_stock->id;
                    if($material_stock->po_buyer) $note_po_buyer = 'ASAL STOCK DARI PO BUYER '.$material_stock->po_buyer;
                    else $note_po_buyer = null;

                }else
                {
                    if($is_exists->po_buyer) $note_po_buyer = 'ASAL STOCK DARI PO BUYER '.$is_exists->po_buyer;
                    else $note_po_buyer = null;

                    $material_stock_id          = $is_exists->id;
                    $stock                      = sprintf('%0.8f',$is_exists->stock);
                    $reserved_qty               = sprintf('%0.8f',$is_exists->reserved_qty);
                    $new_stock                  = sprintf('%0.8f',$stock + $allocation_item->qty_booking);
                    $is_exists->stock           = sprintf('%0.8f',$new_stock);
                    $is_exists->available_qty   = sprintf('%0.8f',$new_stock - $reserved_qty);
                    $is_exists->save();

                }

                DetailMaterialStock::FirstOrCreate([
                    'material_preparation_id'   => $allocation_item->material_preparation_id,
                    'material_stock_id'         => $material_stock_id,
                    'remark'                    => 'CANCEL BOOKING, '.$note_po_buyer,
                    'uom'                       => $material_stock->uom,
                    'qty'                       => sprintf('%0.8f',$allocation_item->qty_booking),
                    'user_id'                   => Auth::user()->id,
                    'created_at'                => $upload_date,
                    'updated_at'                => $upload_date
                ]);

                $counter_in = $free_stock_destination->counter_in;
                $free_stock_destination->counter_in = $counter_in +1;
                $free_stock_destination->save();
            }

            if($allocation_item->material_preparation_id)
            {
                $material_preparation = MaterialPreparation::find($allocation_item->material_preparation_id);
                $material_preparation->delete();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }
}
