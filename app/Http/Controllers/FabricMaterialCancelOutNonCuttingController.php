<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\SummaryHandoverMaterial;
use App\Models\MaterialRollHandoverFabric;


class FabricMaterialCancelOutNonCuttingController extends Controller
{
    public function index()
    {
        return view('fabric_material_cancel_out_non_cutting.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $barcode        = strtoupper($request->barcode);

        $material_roll_handover_fabric = MaterialRollHandoverFabric::where([
            ['barcode',$barcode],
            ['warehouse_from_id',$_warehouse_id]
        ])
        ->first();
        
        if(!$material_roll_handover_fabric) return response()->json('Barocde not found', 422);
        if($material_roll_handover_fabric->receive_date) return response()->json('Barcode already received on destination, received by '.$material_roll_handover_fabric->userReceive->name.' at '.$material_roll_handover_fabric->receive_date->format('d/M/Y H:i:s'), 422);
        
        $obj = new stdClass();
        $obj->id                            = $material_roll_handover_fabric->id;
        $obj->barcode                       = $material_roll_handover_fabric->barcode;
        $obj->referral_code                 = $material_roll_handover_fabric->referral_code;
        $obj->sequence                      = $material_roll_handover_fabric->sequence;
        $obj->material_stock_id             = $material_roll_handover_fabric->material_stock_id;
        $obj->summary_handover_material_id  = $material_roll_handover_fabric->summary_handover_material_id;
        $obj->actual_lot                    = $material_roll_handover_fabric->actual_lot;
        $obj->batch_number                  = $material_roll_handover_fabric->batch_number;
        $obj->c_bpartner_id                 = $material_roll_handover_fabric->c_bpartner_id;
        $obj->document_no                   = $material_roll_handover_fabric->document_no;
        $obj->item_code                     = $material_roll_handover_fabric->item_code;
        $obj->uom                           = $material_roll_handover_fabric->uom;
        $obj->nomor_roll                    = $material_roll_handover_fabric->nomor_roll;
        $obj->qty_handover                  = sprintf('%0.8f',$material_roll_handover_fabric->qty_handover);
        $obj->supplier_name                 = $material_roll_handover_fabric->supplier_name;
        $obj->movement_date                 = carbon::now()->toDateTimeString();

        return response()->json($obj, 200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'items' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $_warehouse_id        = $request->warehouse_id;
            $area_receive_fabric  = Locator::whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where('name','RECEIVING')
                ->where('warehouse',$_warehouse_id);
            })
            ->first();

            $from_location = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            $to_location = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            $items                      = json_decode($request->items);
            $update_summary_stocks      = array();
            $update_summary_handover    = array();
            $locator_source             = array();
            $concatenate                = '';

            try 
            {
                DB::beginTransaction();

                foreach($items as $item)
                {
                    $material_stock_id                  = $item->material_stock_id;
                    $material_roll_handover_fabric_id   = $item->id;
                    $summary_handover_material_id       = $item->summary_handover_material_id;
                    $movement_date                      = $item->movement_date;
                    $barcode                            = $item->barcode;
                    $referral_code                      = $item->referral_code;
                    $sequence                           = $item->sequence;
                    $qty_handover                       = sprintf('%0.8f',$item->qty_handover);

                    $summary_handover                   = SummaryHandoverMaterial::find($summary_handover_material_id);
                    
                    if($summary_handover->is_move_order)
                    {
                        $status_movement    = 'cancel-move-order';
                        $source             = 'CANCEL MOVE ORDER';
                        $note_movement      = 'NOMOR SERI BARCODE '.$barcode.' TIDAK JADI DI MOVE ORDER, QTY '.$qty_handover.' KEMBALI KE STOCK';
                    }else
                    {
                        $status_movement    = 'cancel-handover';
                        $source             = 'CANCEL HANDOVER';
                        $note_movement      = 'NOMOR SERI BARCODE '.$barcode.' TIDAK JADI DI PINDAH TANGAN, QTY '.$qty_handover.' KEMBALI KE STOCK';
                    }

                    $material_stock                         = MaterialStock::find($material_stock_id);
                    $reserved_qty                           = sprintf('%0.8f',$material_stock->reserved_qty);
                    $qty_order                              = sprintf('%0.8f',$material_stock->qty_order);
                    $stock                                  = sprintf('%0.8f',$material_stock->stock);
                    $barcode_parent                         = $material_stock->barcode_supplier;
                    $item_id                                = $material_stock->item_id;
                    $po_detail_id                           = $material_stock->po_detail_id;
                    $no_packing_list                        = $material_stock->no_packing_list;
                    $no_invoice                             = $material_stock->no_invoice;
                    $c_order_id                             = $material_stock->c_order_id;
                    $batch_number                           = $material_stock->batch_number;
                    $nomor_roll                             = $material_stock->nomor_roll;
                    $uom                                    = $material_stock->uom;
                    $document_no                            = $material_stock->document_no;
                    $c_bpartner_id                          = $material_stock->c_bpartner_id;
                    $warehouse_id                           = $material_stock->warehouse_id;
                    $summary_stock_fabric_id                = $material_stock->summary_stock_fabric_id;
                    $item_code                              = $material_stock->item_code;
                    $item_desc                              = $material_stock->item_desc;
                    $color                                  = $material_stock->color;
                    $upc                                    = $material_stock->upc;
                    $category                               = $material_stock->category;
                    $supplier_name                          = $material_stock->supplier_name;
                    $supplier_code                          = $material_stock->supplier_code;
                    $load_actual                            = $material_stock->load_actual;
                    $_material_roll_handover_fabric_id      = $material_stock->material_roll_handover_fabric_id;
                    //$summary_handover_material_id           = $material_stock->summary_handover_material_id;
                    $jenis_po                               = $material_stock->jenis_po;
                    $qc_result                              = $material_stock->qc_result;
                    $begin_width                            = $material_stock->begin_width;
                    $middle_width                           = $material_stock->middle_width;
                    $end_width                              = $material_stock->end_width;
                    $actual_width                           = $material_stock->actual_width;
                    $actual_length                          = $material_stock->actual_length;
                    $different_yard                         = $material_stock->different_yard;
                    $inspect_lab_date                       = $material_stock->inspect_lab_date;
                    $user_lab_id                            = $material_stock->user_lab_id;
                    $inspect_lot_result                     = $material_stock->inspect_lot_result;
                    $inspect_lab_remark                     = $material_stock->inspect_lab_remark;
                    $is_from_handover                       = $material_stock->is_from_handover;
                    $monitoring_receiving_fabric_id         = $material_stock->monitoring_receiving_fabric_id;
                    $upc_item                               = $material_stock->upc_item;
                    $type_stock_erp_code                    = $material_stock->type_stock_erp_code;
                    $type_stock                             = $material_stock->type_stock;
                    $new_qty_order                          = sprintf('%0.8f',$qty_order - $qty_handover);

                    $material_stock->qty_order              = $new_qty_order;

                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $c_order_id             = 'FREE STOCK';
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();
                        
                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : '-');
                    }
                    
                    //insert new roll
                    $is_stock_exists = MaterialStock::where([
                        ['locator_id',$area_receive_fabric->id],
                        ['summary_stock_fabric_id',$summary_stock_fabric_id],
                        ['barcode_supplier',$barcode],
                        ['item_id',$item_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['c_order_id',$c_order_id],
                        ['batch_number',$batch_number],
                        ['nomor_roll',$nomor_roll],
                        ['document_no',$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_material_others',true],
                        ['is_roll_cancel',true],
                        ['stock',$qty_handover],
                        ['type_po',1],
                        ['source',$source.', INDUK '.$barcode_parent],
                    ])
                    ->first();

                    //dd($is_stock_exists);
                    if(!$is_stock_exists)
                    {
                        $new_material_stock = MaterialStock::FirstOrCreate([
                            'locator_id'                            => $area_receive_fabric->id,
                            'barcode_supplier'                      => $barcode,
                            'referral_code'                         => $referral_code,
                            'sequence'                              => $sequence,
                            'monitoring_receiving_fabric_id'        => $monitoring_receiving_fabric_id,
                            'po_detail_id'                          => $po_detail_id,
                            'item_code'                             => $item_code,
                            'item_desc'                             => $item_desc,
                            'user_lab_id'                           => $user_lab_id,
                            'color'                                 => $color,
                            'category'                              => $category,
                            'no_packing_list'                       => $no_packing_list,
                            'inspect_lab_remark'                    => $inspect_lab_remark,
                            'no_invoice'                            => $no_invoice,
                            'inspect_lot_result'                    => $inspect_lot_result,
                            'load_actual'                           => $load_actual,
                            'qc_result'                             => $qc_result,
                            'is_from_handover'                      => $is_from_handover,
                            'upc_item'                              => $upc_item,
                            'actual_length'                         => $actual_length,
                            'type_po'                               => 1,
                            'c_order_id'                            => $c_order_id,
                            'item_id'                               => $item_id,
                            'begin_width'                           => $begin_width,
                            'middle_width'                          => $middle_width,
                            'end_width'                             => $end_width,
                            'actual_width'                          => $actual_width,
                            'inspect_lab_date'                      => $inspect_lab_date,
                            'qty_order'                             => $qty_handover,
                            'qty_arrival'                           => $qty_handover,
                            'type_stock_erp_code'                   => $type_stock_erp_code,
                            'type_stock'                            => $type_stock,
                            'summary_stock_fabric_id'               => $summary_stock_fabric_id,
                            'stock'                                 => $qty_handover,
                            'available_qty'                         => $qty_handover,
                            'batch_number'                          => $batch_number,
                            'is_material_others'                    => true,
                            'is_roll_cancel'                        => true,
                            'nomor_roll'                            => $nomor_roll,
                            'uom'                                   => $uom,
                            'different_yard'                        => $different_yard,
                            'source'                                => $source.', INDUK '.$barcode_parent,
                            'is_active'                             => true,
                            'supplier_name'                         => $supplier_name,
                            'document_no'                           => $document_no,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'supplier_code'                         => $supplier_code,
                            'warehouse_id'                          => $warehouse_id,
                            'material_roll_handover_fabric_id'      => $_material_roll_handover_fabric_id,
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'jenis_po'                              => $jenis_po,
                            'user_id'                               => auth::user()->id,
                            'approval_user_id'                      => auth::user()->id,
                            'approval_date'                         => $movement_date,
                            'created_at'                            => $movement_date,
                            'updated_at'                            => $movement_date,
                        ]);

                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id'     => $new_material_stock->id,
                            'from_location'         => null,
                            'to_destination'        => $area_receive_fabric->id,
                            'status'                => $status_movement,
                            'uom'                   => $uom,
                            'qty'                   => $qty_handover,
                            'movement_date'         => $movement_date,
                            'note'                  => $source.', BARCODE INDUK '.$barcode_parent.$note_movement,
                            'user_id'               => auth::user()->id
                        ]);

                        $concatenate                .= "'".$new_material_stock->id."',";
                        $material_stock->save();
                    }else
                    {
                        $locator_source []  = $is_stock_exists->locator_id;
                        $stocks             = sprintf('%0.8f',$is_stock_exists->stock);
                        $reserved_qty       = sprintf('%0.8f',$is_stock_exists->reserved_qty);
                        $new_stocks         = sprintf('%0.8f',$stocks + $qty_handover);
                        $new_available_qty  = sprintf('%0.8f',$stocks - $reserved_qty);

                        if($new_available_qty > 0)
                        {
                            $is_stock_exists->is_allocated  = false;
                            $is_stock_exists->deleted_at    = null;
                            $is_stock_exists->is_active     = true;
                        }

                        $is_stock_exists->stock         = $new_stocks;
                        $is_stock_exists->available_qty = $new_available_qty;

                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id'     => $is_stock_exists->id,
                            'from_location'         => null,
                            'to_destination'        => $area_receive_fabric->id,
                            'status'                => 'cancel-preparation',
                            'uom'                   => $is_stock_exists->uom,
                            'qty'                   => $qty_handover,
                            'movement_date'         => $movement_date,
                            'note'                  => $source.', BARCODE INDUK '.$barcode_parent.$note_movement,
                            'user_id'               => auth::user()->id
                        ]);

                        $is_stock_exists->locator_id = $area_receive_fabric->id;
                        $is_stock_exists->save();
                    }

                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$from_location->id],
                        ['to_destination',$to_location->id],
                        ['from_locator_erp_id',$from_location->area->erp_id],
                        ['to_locator_erp_id',$to_location->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();

                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $from_location->id,
                            'to_destination'        => $to_location->id,
                            'from_locator_erp_id'   => $from_location->area->erp_id,
                            'to_locator_erp_id'     => $to_location->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $movement_date,
                            'updated_at'            => $movement_date,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $movement_date;
                        $is_movement_exists->save();

                        $material_movement_id = $is_movement_exists->id;
                    }

                    $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$material_movement_id],
                        ['material_stock_id',$material_stock_id],
                        ['qty_movement',$qty_handover],
                        ['item_id',$item_id],
                        ['warehouse_id',$warehouse_id],
                        ['date_movement',$movement_date],
                        ['is_integrate',false],
                        ['is_active',true],
                    ])
                    ->exists();

                    if(!$is_material_movement_line_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $material_movement_id,
                            'material_stock_id'             => $material_stock_id,
                            'item_code'                     => $item_code,
                            'item_id'                       => $item_id,
                            'c_order_id'                    => $c_order_id,
                            'c_orderline_id'                => $c_orderline_id,
                            'c_bpartner_id'                 => $c_bpartner_id,
                            'supplier_name'                 => $supplier_name,
                            'type_po'                       => 1,
                            'is_integrate'                  => false,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $qty_handover,
                            'date_movement'                 => $movement_date,
                            'date_receive_on_destination'   => $movement_date,
                            'nomor_roll'                    => $nomor_roll,
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => $document_no,
                            'note'                          => $note_movement,
                            'is_active'                     => true,
                            'user_id'                       => auth::user()->id,
                        ]);
                    }


                    
                    $update_summary_stocks []   = $summary_stock_fabric_id;
                    $update_summary_handover [] = $summary_handover_material_id;

                    MaterialRollHandoverFabric::find($material_roll_handover_fabric_id)->delete();
                }

                $total_item_on_locator  = MaterialStock::where('locator_id',$area_receive_fabric->id)
                ->whereNull('deleted_at')
                ->count();

                $counter                = Locator::find($area_receive_fabric->id);
                $counter->counter_in    = $total_item_on_locator;
                $counter->save();

                foreach ($locator_source as $key => $value) 
                {
                    $total_item_on_locator = MaterialStock::where('locator_id',$value)
                    ->whereNull('deleted_at')
                    ->count();
    
                    $counter                = Locator::find($value);
                    $counter->counter_in    = $total_item_on_locator;
                    $counter->save();
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_stock_fabric(array[".$concatenate ."]);" ));
            }

            $this->updateSummaryStockFabric($update_summary_stocks);
            $this->updateSummaryHandoverFabrics($update_summary_handover);
            return response()->json(200);
        }
    }


    static function updateSummaryStockFabric($summary_stock_fabric_ids)
    {
        $summary_stock_fabrics = DB::table('summary_vs_stock_v')
        ->whereIn('id',$summary_stock_fabric_ids)
        ->get();
        
        try 
        {
          DB::beginTransaction();
         
          foreach ($summary_stock_fabrics as $key => $value) 
          {
            $summary_stock_fabric_id  = $value->id;
            $total_stock                          = sprintf('%0.8f',$value->detail_total_stock);
            $total_reserved_qty                   = sprintf('%0.8f',$value->detail_total_reserved_qty);
            $total_available_qty                  = sprintf('%0.8f',$value->detail_total_available_qty);
            
            $summary_stock_fabric                 = SummaryStockFabric::find($summary_stock_fabric_id);
            $summary_stock_fabric->stock          = $total_stock; 
            $summary_stock_fabric->reserved_qty   = $total_reserved_qty; 
            $summary_stock_fabric->available_qty  = $total_available_qty; 
            $summary_stock_fabric->save();
          }
          
          DB::commit();
        } catch (Exception $e) 
        {
          DB::rollBack();
          $message = $e->getMessage();
          ErrorHandler::db($message);
        }
    }

    static function updateSummaryHandoverFabrics($summary_handover_ids)
    {
        $summary_handover_fabrics = DB::table('summary_vs_detail_handover_fabric_v')
        ->whereIn('id',$summary_handover_ids)
        ->orderby('created_at','desc')
        ->get();

        try 
        {
          DB::beginTransaction();
          
          foreach ($summary_handover_fabrics as $key => $value) 
          {
            $summary_handover_fabric_id     = $value->id;
            $summary_handover_fabric        = SummaryHandoverMaterial::find($summary_handover_fabric_id);
            $total_qty_handover             = sprintf('%0.8f',$summary_handover_fabric->total_qty_handover);
            $total_qty_supply               = sprintf('%0.8f',$summary_handover_fabric->total_qty_supply);
            $total_reserved                 = sprintf('%0.8f',$value->total_reserved);
            
            if($total_reserved == '0.000' || $total_reserved == '0')
            {
                $summary_handover_fabric->complete_date             = null;
                $summary_handover_fabric->total_qty_outstanding     = sprintf('%0.8f',$total_qty_handover);
                $summary_handover_fabric->total_qty_supply          =  '0';
                $summary_handover_fabric->save();
            }else
            {
                $new_total_outstanding  = sprintf('%0.8f',$total_qty_handover - $total_reserved);

                if($new_total_outstanding > 0)
                {
                    $summary_handover_fabric->complete_date             = null;
                    $summary_handover_fabric->total_qty_outstanding     = sprintf('%0.8f',$new_total_outstanding);
                    $summary_handover_fabric->total_qty_supply          = $total_reserved;
                    $summary_handover_fabric->save();
                }
            }

          }

          DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }
}
