<?php namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\Barcode;
use App\Models\Supplier;
use App\Models\MaterialCheck;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\DetailMaterialCheck;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialStock;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\SummaryHandoverMaterial;
use App\Models\MaterialReadyPreparation;
use App\Models\MonitoringReceivingFabric;
use App\Models\MaterialRollHandoverFabric;

class FabricMaterialArrivalController extends Controller
{
    public function index()
    {
        // return view('errors.migration');
        return view('fabric_material_arrival.index');
    }

    public function create(request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $barcode        = trim($request->barcode);
        $array          = array();

        $is_handover = MaterialRollHandoverFabric::where([
            ['barcode',strtoupper($barcode)],
            ['warehouse_to_id',$_warehouse_id],
        ])
        ->exists();

        if($is_handover)
        {
            $material_handover = MaterialRollHandoverFabric::where([
                ['barcode',strtoupper($barcode)],
                ['warehouse_to_id',$_warehouse_id],
            ])
            //->whereNull('user_receive_id')
            ->orderby('created_at','desc')
            ->first();

            if(!$material_handover) return response()->json('Barcode not found ',422);
            if($material_handover->user_receive_id != null) return response()->json('Barcode already scanned by '.$material_handover->userReceive->name,422);
            
            $supplier                               = Supplier::where('c_bpartner_id',$material_handover->c_bpartner_id)->first();
            
            $_item                                  = Item::where('item_code',$material_handover->item_code)->first();
            $item_desc                              = ($_item)? strtoupper($_item->item_desc) : 'MASTER ITEM NOT FOUND';
            $category                               = ($_item)? strtoupper($_item->category) : 'MASTER ITEM NOT FOUND';
            $item_id                                = ($_item)? $_item->item_id : 'MASTER ITEM NOT FOUND';
            $upc                                    = ($_item)? $_item->upc : 'MASTER ITEM NOT FOUND';
            
            
            $obj                                     = new stdclass();
            $obj->material_roll_handover_fabric_id   = $material_handover->id;
            $obj->c_bpartner_id                      = $material_handover->c_bpartner_id;
            $obj->supplier_code                      = ($supplier ? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND' );
            $obj->supplier_name                      = ($supplier ? strtoupper($supplier->supplier_name) : 'MASTER SUPPLIER NOT FOUND' );
            $obj->c_order_id                         = $material_handover->c_order_id;
            $obj->no_invoice                         = $material_handover->no_invoice;
            $obj->no_packing_list                    = $material_handover->no_packing_list;
            $obj->document_no                        = $material_handover->document_no;
            $obj->item_id                            = $item_id;
            $obj->item_code                          = $material_handover->item_code;
            $obj->color                              = $material_handover->color;
            $obj->item_desc                          = $item_desc;
            $obj->category                           = $category;
            $obj->upc                                = $upc;
            $obj->po_detail_id                       = $material_handover->po_detail_id;
            $obj->nomor_roll                         = $material_handover->nomor_roll;
            $obj->batch_number                       = $material_handover->batch_number;
            $obj->actual_lot                         = $material_handover->actual_lot;
            $obj->begin_width                        = $material_handover->begin_width;
            $obj->middle_width                       = $material_handover->middle_width;
            $obj->end_width                          = $material_handover->end_width;
            $obj->actual_width                       = $material_handover->actual_width;
            $obj->actual_length                      = $material_handover->actual_length;
            $obj->uom                                = $material_handover->uom;
            $obj->qty_upload                         = $material_handover->qty_handover;
            $obj->warehouse_id                       = $material_handover->warehouse_to_id;
            $obj->barcode                            = $material_handover->barcode;
            $obj->sequence                           = $material_handover->sequence;
            $obj->referral_code                      = $material_handover->referral_code;
            $obj->is_handover                        = true;
            $obj->movement_date                      = carbon::now()->toDateTimeString();
           
        }else
        {
            return response()->json('Barcode not found',422); 
        }

        return response()->json($obj,200);
       
    } 

    public function store(request $request)
    {
        $validator = Validator::make($request->all(), [
            'material_arrival_fabrics' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $warehouse_id       = $request->warehouse_id;
            $barcodes           = json_decode($request->material_arrival_fabrics);

            $handover_location  = Locator::with('area')
            ->whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','HANDOVER');
            })
            ->first();

            $area_receive_fabric = Locator::whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('name','RECEIVING')
                ->where('warehouse',$warehouse_id);
            })
            ->first();

            //return response()->json($barcodes,200);
            $insert_movements               = array();
            $insert_stocks                  = array();
            $summary_stock_fabrics          = array();
            $material_roll_handover_fabrics = array();
            $concatenate_1                  = '';
            $concatenate_2                  = '';
            $approval_date                  = carbon::now()->toDateTimeString();

            try 
            {
                DB::beginTransaction();

                foreach ($barcodes as $key => $value) 
                {
                    $material_roll_handover_fabric_id   = $value->material_roll_handover_fabric_id;
                    $c_bpartner_id                      = $value->c_bpartner_id;
                    $po_detail_id                       = $value->po_detail_id;
                    $supplier_name                      = $value->supplier_name;
                    $supplier_code                      = $value->supplier_code;
                    $c_order_id                         = $value->c_order_id;
                    $no_invoice                         = $value->no_invoice;
                    $no_packing_list                    = $value->no_packing_list;
                    $document_no                        = strtoupper($value->document_no);
                    $item_code                          = strtoupper($value->item_code);
                    $item_desc                          = $value->item_desc;
                    $category                           = $value->category;
                    $item_id                            = $value->item_id;
                    $upc                                = $value->upc;
                    $color                              = $value->color;
                    $nomor_roll                         = $value->nomor_roll;
                    $batch_number                       = $value->batch_number;
                    $actual_lot                         = $value->actual_lot;
                    $begin_width                        = $value->begin_width;
                    $middle_width                       = $value->middle_width;
                    $end_width                          = $value->end_width;
                    $actual_width                       = $value->actual_width;
                    $actual_length                      = $value->actual_length;
                    $uom                                = $value->uom;
                    $qty_upload                         = sprintf('%0.8f', $value->qty_upload);
                    $barcode                            = $value->barcode;
                    $movement_date                      = $value->movement_date;
                    $is_handover                        = $value->is_handover;
                    $sequence                           = $value->sequence;
                    $referral_code                      = $value->referral_code;
                    $material_roll_handover_fabric      = MaterialRollHandoverFabric::find($material_roll_handover_fabric_id);
                    $summary_handover_material_id       = $material_roll_handover_fabric->summary_handover_material_id;

                    if($is_handover)
                    {
                        if($material_roll_handover_fabric)
                        {
                            $warehouse_from_id = $material_roll_handover_fabric->warehouse_from_id;
                            if($warehouse_from_id == '1000001') $remark = 'HANDOVER DARI AOI 1';
                            else if($warehouse_from_id == '1000011') $remark = 'HANDOVER DARI AOI 2';

                            $inspect_lab_date   = null;//($actual_lot)? $material_roll_handover_fabric->materialStock->inspect_lab_date : null;
                            $user_lab_id        = null;//($actual_lot)? $material_roll_handover_fabric->materialStock->user_lab_id : null;
                        }else
                        {
                            $remark             = null;
                            $inspect_lab_date   = null;//($actual_lot)? carbon::now() : null;
                            $user_lab_id        = null;
                        }

                        $is_arrival_exists = MaterialArrival::where([
                            ['po_detail_id',$barcode],
                            ['material_roll_handover_fabric_id',$material_roll_handover_fabric_id],
                            ['summary_handover_material_id',$summary_handover_material_id],
                            ['warehouse_id',$warehouse_id],
                        ])
                        ->exists();

                        if(!$is_arrival_exists)
                        {
                            $material_arrival = MaterialArrival::FirstOrCreate([
                                'material_roll_handover_fabric_id'  => $material_roll_handover_fabric_id,
                                'summary_handover_material_id'      => $summary_handover_material_id,
                                'po_detail_id'                      => $barcode,
                                'item_id'                           => $item_id,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'supplier_name'                     => $supplier_name,
                                'document_no'                       => $document_no,
                                'type_po'                           => 1,
                                'c_order_id'                        => $c_order_id,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'warehouse_id'                      => $warehouse_id,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'no_packing_list'                   => $no_packing_list,
                                'no_invoice'                        => $no_invoice,
                                'qty_entered'                       => $qty_upload,
                                'qty_ordered'                       => $qty_upload,
                                'qty_carton'                        => 1,
                                'qty_delivered'                     => $qty_upload,
                                'qty_upload'                        => $qty_upload,
                                'qty_available_rma'                 => $qty_upload,
                                'qty_reserved_rma'                  => 0,
                                'is_subcont'                        => true,
                                'prepared_status'                   => 'NEED PREPARED',
                                'is_active'                         => false,
                                'user_id'                           => Auth::user()->id
                            ]);

                            $_material_arrival_id   = $material_arrival->id;
                            $insert_stocks []       = $_material_arrival_id;
                        }
                    }

                    $insert_movements [] = $material_roll_handover_fabric_id;

                }

                if($concatenate_1 != '')
                {
                    $concatenate_1 = substr_replace($concatenate_1, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_arrival(array[".$concatenate_1 ."]);" ));
                }
                
                $this->insertMaterialHandoverStock($insert_stocks,$warehouse_id);
                $this->insertToMovementInventory($insert_movements);
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            return response()->json('success',200);

        }else
        {
            return response()->json('Please scan barcode first.',422);
        }
        return response()->json('success',200);
    }

    private function insertMaterialHandoverStock($insert_material_stocks,$warehouse_id)
    {
        $concatenate = '';
        $area_receive_fabric = Locator::whereHas('area',function ($query) use ($warehouse_id)
        {
            $query->where('name','RECEIVING')
            ->where('warehouse',$warehouse_id);
        })
        ->first();

        $handover_location = Locator::with('area')
        ->whereHas('area',function ($query) use ($warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$warehouse_id);
            $query->where('name','HANDOVER');
        })
        ->first();
        
        $approval_date                  = carbon::now()->toDateTimeString();
        $material_stock_ids             = array();
        $material_roll_handover_fabrics = array();
        $return_barcode                 = array();

        $material_stocks = MaterialArrival::whereIn('id',$insert_material_stocks)->get();
        
        if(count($material_stocks) > 0)
        {
            try 
            {
                DB::beginTransaction();

                foreach ($material_stocks as $key => $value) 
                {
                    $item_code                          = $value->item_code;
                    $item_id                            = $value->item_id;
                    $material_roll_handover_fabric_id   = $value->material_roll_handover_fabric_id;
                    $_item                              = Item::where('item_id',$item_id)->first();
                    $material_roll_handover_fabric      = MaterialRollHandoverFabric::find($material_roll_handover_fabric_id);
                    $material_stock_id                  = $material_roll_handover_fabric->material_stock_id;
                    $warehouse_from_id                  = $material_roll_handover_fabric->warehouse_from_id;
                    $material_arrival_id                = $value->id;
                    $item_desc                          = ($_item ? $_item->item_desc : $value->item_desc);
                    $color                              = ($_item ? $_item->color : null);
                    $upc                                = ($_item ? $_item->upc : null);
                    $is_require_lot                     = ($_item ? $_item->required_lot : false);
                    $barcode                            = $material_roll_handover_fabric->barcode;
                    $referral_code                      = $material_roll_handover_fabric->referral_code;
                    $sequence                           = $material_roll_handover_fabric->sequence;
                    $summary_handover_material_id       = $material_roll_handover_fabric->summary_handover_material_id;
                    $po_detail_id                       = $material_roll_handover_fabric->po_detail_id;
                    $category                           = $value->category;
                    $no_packing_list                    = $value->no_packing_list;
                    $no_invoice                         = $value->no_invoice;
                    $c_order_id                         = $value->c_order_id;
                    $stock                              = sprintf('%0.8f',$value->qty_upload);
                    $qty_order                          = sprintf('%0.8f',$value->qty_upload);
                    $available_qty                      = sprintf('%0.8f',$value->qty_upload);
                    $batch_number                       = $material_roll_handover_fabric->batch_number;
                    $nomor_roll                         = $material_roll_handover_fabric->nomor_roll;
                    $uom                                = $value->uom;
                    $document_no                        = strtoupper($value->document_no);
                    $c_bpartner_id                      = $value->c_bpartner_id;
                    $warehouse_id                       = $value->warehouse_id;
                    $jenis_po                           = strtoupper($value->jenis_po);
                    $supplier                           = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                    $supplier_name                      = ($supplier ? strtoupper($supplier->supplier_name) : strtoupper($value->supplier_name));
                    $supplier_code                      = ($supplier ? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND');
                    
                    if($warehouse_from_id == '1000011') $warehouse_from_name = 'FAB-AOI-2';
                    else $warehouse_from_name = 'FAB-AOI-1';

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    if($upc == '60001344' || $upc == '62712089' || $upc == '70000121')
                    {
                        if($warehouse_id == '1000011')
                        {
                            $inspect_lot_result = 'RELEASE';
                            $load_actual        = 'POCKET';
                            $user_lab_id        = $system->id;
                            $inspect_lab_remark = '60001344,62712089,70000121 SECARA SISTEM AKAN LGSG DI ISIKAN LOTNYA POCKET';
                            $inspect_lab_date   = $approval_date;
                        }else
                        {
                            $inspect_lot_result = 'HOLD';
                            $load_actual        = 'POCKET';
                            $user_lab_id        = $system->id;
                            $inspect_lab_remark = '60001344,62712089,70000121 SECARA SISTEM AKAN LGSG DI ISIKAN LOTNYA POCKET';
                            $inspect_lab_date   = $approval_date;
                        }
                        
                        
                    }else
                    {
                        if(!$is_require_lot)
                        {
                            if($warehouse_id == '1000011')
                            {
                                $inspect_lot_result = 'RELEASE';
                                $load_actual        = 'POCKET';
                                $user_lab_id        = $system->id;
                                $inspect_lab_remark = 'ITEM '.$upc.' TIDAK MEMERLUKAN LOT, MAKA LGSG DI ANGGAP POCKET ( SESUAI KESEPAKATAN TANGGAL 24/DES/2019 DI SKYPE AOI 2 LGSG RELEASE, AOI 1 PERLU KONFIRMASI)';
                                $inspect_lab_date   = $approval_date;
                            }else
                            {
                                $inspect_lot_result = 'HOLD';
                                $load_actual        = 'POCKET';
                                $user_lab_id        = $system->id;
                                $inspect_lab_remark = 'ITEM '.$upc.' TIDAK MEMERLUKAN LOT, MAKA LGSG DI ANGGAP POCKET ( SESUAI KESEPAKATAN TANGGAL 24/DES/2019 DI SKYPE AOI 2 LGSG RELEASE, AOI 1 PERLU KONFIRMASI)';
                                $inspect_lab_date   = $approval_date;
                            }
                        }else
                        {
                            $material_stock = MaterialStock::find($material_stock_id);
                            $inspect_lot_result  = 'HOLD';
                            $load_actual        = $material_stock->load_actual;
                            $user_lab_id        = $material_stock->user_lab_id;
                            $inspect_lab_remark = $material_stock->inspect_lab_remark;
                            $inspect_lab_date   = $material_stock->inspect_lab_date;
                        }
                        
                    }

                    $is_stock_exists = MaterialStock::where([
                        ['locator_id',$area_receive_fabric->id],
                        ['summary_handover_material_id',$summary_handover_material_id],
                        ['material_roll_handover_fabric_id',$material_roll_handover_fabric_id],
                        ['barcode_supplier',$barcode],
                        ['item_id',$item_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['c_order_id',$c_order_id],
                        ['batch_number',$batch_number],
                        ['nomor_roll',$nomor_roll],
                        ['document_no',$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['stock',$stock],
                        ['type_po',1],
                        ['is_master_roll',true],
                        ['source','DI DAPATKAN DARI HANDOVER '.$warehouse_from_name],
                    ]);

                    if($po_detail_id) $is_stock_exists = $is_stock_exists->where('po_detail_id',$po_detail_id);
                    $is_stock_exists = $is_stock_exists->exists();

                    if(!$is_stock_exists)
                    {
                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }else
                        {
                            $get_4_digit_from_beginning = substr($document_no,0, 4);
                            $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                            
                            if($_mapping_stock_4_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_4_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_4_digt->type_stock;
                            }else
                            {
                                $get_5_digit_from_beginning = substr($document_no,0, 5);
                                $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                
                                if($_mapping_stock_5_digt)
                                {
                                    $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                    $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                    $type_stock             = $_mapping_stock_5_digt->type_stock;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                                
                            }
                        }

                        $material_stock = MaterialStock::FirstOrCreate([
                            'locator_id'                            => $area_receive_fabric->id,
                            'barcode_supplier'                      => $barcode,
                            'referral_code'                         => $referral_code,
                            'sequence'                              => $sequence,
                            'item_code'                             => $item_code,
                            'item_desc'                             => $item_desc,
                            'color'                                 => $color,
                            'upc_item'                              => $upc,
                            'category'                              => $category,
                            'no_packing_list'                       => $no_packing_list,
                            'no_invoice'                            => $no_invoice,
                            'type_po'                               => 1,
                            'c_order_id'                            => $c_order_id,
                            'item_id'                               => $item_id,
                            'qty_order'                             => $qty_order,
                            'qty_arrival'                           => $qty_order,
                            'stock'                                 => $stock,
                            'available_qty'                         => $available_qty,
                            'batch_number'                          => $batch_number,
                            'nomor_roll'                            => $nomor_roll,
                            'uom'                                   => $uom,
                            'source'                                => 'DI DAPATKAN DARI HANDOVER '.$warehouse_from_name,
                            'is_master_roll'                        => true,
                            'is_active'                             => true,
                            'is_from_handover'                      => true,
                            'inspect_lot_result'                    => $inspect_lot_result,
                            'load_actual'                           => $load_actual,
                            'user_lab_id'                           => $user_lab_id,
                            'inspect_lab_remark'                    => $inspect_lab_remark,
                            'inspect_lab_date'                      => $inspect_lab_date,
                            'po_detail_id'                          => $po_detail_id,
                            'supplier_name'                         => $supplier_name,
                            'document_no'                           => $document_no,
                            'type_stock'                            => $type_stock,
                            'type_stock_erp_code'                   => $type_stock_erp_code,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'supplier_code'                         => $supplier_code,
                            'warehouse_id'                          => $warehouse_id,
                            'material_roll_handover_fabric_id'      => $material_roll_handover_fabric_id,
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'jenis_po'                              => $jenis_po,
                            'user_id'                               => auth::user()->id,
                            'approval_user_id'                      => auth::user()->id,
                            'approval_date'                         => $approval_date,
                            'created_at'                            => $approval_date,
                            'updated_at'                            => $approval_date,
                        ]);

                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'     => $material_stock->id,
                            'material_arrival_id'   => $material_arrival_id,
                            'remark'                => 'DI DAPATKAN DARI HANDOVER '.$warehouse_from_name,
                            'uom'                   => $uom,
                            'qty'                   => $stock,
                            'user_id'               => Auth::user()->id,
                            'created_at'            => $approval_date,
                            'updated_at'            => $approval_date,
                        ]);
    
                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id' => $material_stock->id,
                            'from_location'     => $handover_location->id,
                            'to_destination'    => $area_receive_fabric->id,
                            'uom'               => $uom,
                            'qty'               => $stock,
                            'movement_date'     => $approval_date,
                            'status'            => 'receiving',
                            'user_id'           => auth::user()->id
                        ]);
    
                        $material_roll_handover_fabrics[]   = $value->material_roll_handover_fabric_id;
                        $material_stock_ids[]               = $material_stock->id;
                        $concatenate                        .= "'".$material_stock->id."',";
    
                        $material_roll_handover_fabric->receive_date    = carbon::now();
                        $material_roll_handover_fabric->user_receive_id = auth::user()->id;
                        $material_roll_handover_fabric->save();
                    }

                   
                }

                $total_item_on_locator              = MaterialStock::where('locator_id',$area_receive_fabric->id)
                ->whereNull('deleted_at')
                ->count();
                
                $area_receive_fabric->counter_in    = $total_item_on_locator;
                $area_receive_fabric->save();
                
                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_stock_fabric_handover(array[".$concatenate ."]);" ));
                }
            
                $this->storeSummaryStockFabric($material_stock_ids,$warehouse_id);
                $this->storeMonitoringHandoverReceiving($insert_material_stocks);
                $this->copyFir($material_roll_handover_fabrics);
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
            
        }
    }

    static function insertToMovementInventory($insert_movements)
    {
        try
        {
            $data = MaterialRollHandoverFabric::whereIn('id',$insert_movements)->get();
            foreach ($data as $key => $datum) 
            {
                $warehouse_id           = $datum->warehouse_to_id;
                $material_stock_id      = $datum->material_stock_id;
                $created_at             = $datum->receive_date;
                $_created_at            = $datum->receive_date->format('d/m/Y H:i:s');
                $is_move_order          = $datum->summaryHandoverMaterial->is_move_order;
                $handover_document_no   = $datum->summaryHandoverMaterial->handover_document_no;
                $qty_handover           = sprintf('%0.8f',$datum->qty_handover);
                $user_id                = $datum->user_id;
                
                if($is_move_order)
                {
                    if($datum->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($datum->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$datum->summaryHandoverMaterial->no_kk;
                        if($datum->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$datum->summaryHandoverMaterial->no_bc;
                    }
                    
                    $note           = 'PROSES INTEGRASI UNTUK MOVE ORDER, ROLL DIKELUARKAN KE '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper($datum->user->name).', SUDAH DITERIMA OLEH '.strtoupper($datum->userReceive->name).' PADA TANGGAL '.$_created_at;
                }else
                {
                    if($datum->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($datum->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$datum->summaryHandoverMaterial->no_kk;
                        if($datum->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$datum->summaryHandoverMaterial->no_bc;
                    }

                    $note           = 'PROSES INTEGRASI UNTUK PINDAH TANGAN, PINDAH TANGAN NOMOR PT '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper($datum->user->name).', SUDAH DITERIMA OLEH '.strtoupper($datum->userReceive->name).' PADA TANGGAL '.$_created_at;
                }

                $from_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();
    
                $to_location = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $material_stock     = MaterialStock::find($material_stock_id);
                $po_detail_id       = $material_stock->po_detail_id;
                $item_code          = $material_stock->item_code;
                $item_id            = $material_stock->item_id;
                $c_bpartner_id      = $material_stock->c_bpartner_id;
                $supplier_name      = $material_stock->supplier_name;
                $document_no        = $material_stock->document_no;
                $nomor_roll         = $material_stock->nomor_roll;
                $uom                = $material_stock->uom;

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $c_order_id             = 'FREE STOCK';
                    $no_packing_list        = null;
                    $no_invoice             = null;
                    $c_orderline_id         = null;
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();
                    
                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                    $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : 'FREE STOCK');
                }

                $is_movement_exists = MaterialMovement::where([
                    ['from_location',$from_location->id],
                    ['to_destination',$to_location->id],
                    ['from_locator_erp_id',$from_location->area->erp_id],
                    ['to_locator_erp_id',$to_location->area->erp_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();

                if(!$is_movement_exists)
                {
                    $material_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $from_location->id,
                        'to_destination'        => $to_location->id,
                        'from_locator_erp_id'   => $from_location->area->erp_id,
                        'to_locator_erp_id'     => $to_location->area->erp_id,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $created_at,
                        'updated_at'            => $created_at,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_movement_exists->updated_at = $created_at;
                    $is_movement_exists->save();

                    $material_movement_id = $is_movement_exists->id;
                }

                $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['item_id',$item_id],
                    ['material_stock_id',$material_stock_id],
                    ['qty_movement',$qty_handover],
                    ['date_movement',$created_at],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_stock_id'             => $material_stock_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => 1,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $qty_handover,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => $nomor_roll,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => $note,
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }
                
            }

        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function storeSummaryStockFabric($material_stock_ids,$warehouse_id)
    {
        $material_stocks = MaterialStock::select('document_no'
        ,'c_order_id'
        ,'c_bpartner_id'
        ,'item_code'
        ,'item_id'
        ,'uom'
        ,db::raw('sum(stock) as stock')
        ,db::raw('sum(qty_order) as qty_order'))
        ->whereIn('id',$material_stock_ids)
        ->groupby('document_no','c_order_id','c_bpartner_id','item_code','item_id','uom')
        ->get();

        $summary_stocks = array();
        try 
        {
            DB::beginTransaction();

            foreach ($material_stocks as $key => $material_stock) 
            {
                $item                   = Item::where('item_code',$material_stock->item_code)->first();
                $new_c_order_id         = $material_stock->c_order_id;
                $new_c_bpartner_id      = $material_stock->c_bpartner_id;
                $new_document_no        = $material_stock->document_no;
                $new_item_id            = $material_stock->item_id;
                $new_item_code          = $material_stock->item_code;

                $supplier               = Supplier::select('supplier_code','supplier_name')->where('c_bpartner_id',$new_c_bpartner_id)->groupby('supplier_code','supplier_name')->first();
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $new_color              = ($item)? $item->color : 'NOT FOUND';
                $new_uom                = $material_stock->uom;
                $new_stock              = sprintf('%0.8f',$material_stock->stock);
                $qty_order              = sprintf('%0.8f',$material_stock->qty_order);
            
                if($new_document_no == 'FREE STOCK' || $new_document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($new_document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($new_document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $is_exists = SummaryStockFabric::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$warehouse_id],
                    ['is_from_handover', true]
                ])
                ->first();

                if($is_exists)
                {
                    $_summary_stock_id = $is_exists->id;
                }else
                {
                    if($new_stock >= $qty_order) $_qty_order = $new_stock;
                    else $_qty_order = $qty_order;

                    $summary_stock_fabric = SummaryStockFabric::FirstOrCreate([
                        'c_bpartner_id'         => $new_c_bpartner_id,
                        'c_order_id'            => $new_c_order_id,
                        'document_no'           => $new_document_no,
                        'supplier_code'         => $supplier_code,
                        'supplier_name'         => $supplier_name,
                        'item_code'             => $new_item_code,
                        'item_id'               => $new_item_id,
                        'color'                 => $new_color,
                        'category_stock'        => $type_stock,
                        'uom'                   => $new_uom,
                        'stock'                 => $new_stock,
                        'reserved_qty'          => 0,
                        'available_qty'         => $new_stock,
                        'qty_order'             => sprintf('%0.8f',$_qty_order),
                        'warehouse_id'          => $warehouse_id,
                    ]);
                    
                    $_summary_stock_id = $summary_stock_fabric->id;
                }

                MaterialStock::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('summary_stock_fabric_id')
                ->wherein('id',$material_stock_ids)
                ->update(['summary_stock_fabric_id' => $_summary_stock_id]);

                $summary_stocks [] =  $_summary_stock_id;
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();
            $sum_summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$summary_stocks)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($sum_summary_stocks as $key => $sum_summary_stock) 
            {
                $summary_stock_fabric   = SummaryStockFabric::find($sum_summary_stock->summary_stock_fabric_id);
                $qty_order              = $summary_stock_fabric->qty_order;
                $qty_order_reserved     = $summary_stock_fabric->qty_order_reserved;

                $new_stock              = $sum_summary_stock->total_stock;
                $total_reserved         = $sum_summary_stock->total_reserved;
                $total_available        = $sum_summary_stock->total_available;
            

                if($new_stock >= $qty_order) $_qty_order = $new_stock;
                else $_qty_order = $qty_order;

                $new_available_qty_order = $_qty_order - $qty_order_reserved;

                if($new_available_qty_order > 0)
                    $summary_stock_fabric->is_allocated = false;
                
                $summary_stock_fabric->stock                = sprintf('%0.8f',$new_stock);
                $summary_stock_fabric->reserved_qty         = sprintf('%0.8f',$total_reserved);
                $summary_stock_fabric->available_qty        = sprintf('%0.8f',$total_available);
                $summary_stock_fabric->qty_order            = sprintf('%0.8f',$_qty_order);
                $summary_stock_fabric->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return $summary_stocks;

    }

    static function copyFir($material_roll_handover_fabrics)
    {
        $material_roll_handovers = MaterialRollHandoverFabric::wherein('id',$material_roll_handover_fabrics)->get();
        $date                    = carbon::now()->toDateTimeString();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($material_roll_handovers as $key => $material_roll_handover) 
            {
                $material_roll_handover_id  = $material_roll_handover->id;
                $material_stock_id_source   = $material_roll_handover->material_stock_id;
                $material_stock             = MaterialStock::where('material_roll_handover_fabric_id',$material_roll_handover_id)->first();
                
                if($material_stock)
                {
                    $material_stock_id_new  = $material_stock->id;
                    $barcode_new            = $material_stock->barcode_supplier;
                    $warehouse_id_new       = $material_stock->warehouse_id;
                }else
                {
                    $material_stock_id_new  = -1;
                    $barcode_new            = -1;
                    $warehouse_id_new       = -1;
                }
                

                $update_total_qty_inspect_roll = MaterialStock::select('monitoring_receiving_fabric_id',db::raw('count(0) as total_roll'))
                ->where([
                    ['material_roll_handover_fabric_id',$material_roll_handover_id],
                    ['inspect_lot_result', 'RELEASE'],
                ])
                ->whereNotNull('load_actual')
                ->groupby('monitoring_receiving_fabric_id')
                ->first();

                if($update_total_qty_inspect_roll)
                {
                    $monitoring_receiving_fabric_id = $update_total_qty_inspect_roll->monitoring_receiving_fabric_id;
                    $total_roll_inspected           = $update_total_qty_inspect_roll->total_roll;

                    $monitoring_receiving_fabric = MonitoringReceivingFabric::find($monitoring_receiving_fabric_id);

                    if($monitoring_receiving_fabric)
                    {
                        $monitoring_receiving_fabric->total_roll_inspected = $total_roll_inspected;
                        $monitoring_receiving_fabric->save();
                    }
                }
                
                $material_check_source = MaterialCheck::where('material_stock_id',$material_stock_id_source)->first();

                if($material_check_source)
                {
                    $detail_material_check_sources = DetailMaterialCheck::where('material_check_id',$material_check_source->id)->get();

                    if($material_stock_id_new != -1 && $barcode_new != -1 && $warehouse_id_new != -1)
                    {
                        $material_check_new = MaterialCheck::firstOrCreate([
                            'material_stock_id'         => $material_stock_id_new,
                            'barcode_preparation'       => $barcode_new,
                            'c_bpartner_id'             => $material_check_source->c_bpartner_id,
                            'supplier_name'             => $material_check_source->supplier_name,
                            'document_no'               => $material_check_source->document_no,
                            'type_po'                   => 1,
                            'item_code'                 => $material_check_source->item_code,
                            'color'                     => $material_check_source->color,
                            'no_invoice'                => $material_check_source->no_invoice,
                            'category'                  => $material_check_source->category,
                            'nomor_roll'                => $material_check_source->nomor_roll,
                            'batch_number'              => $material_check_source->batch_number,
                            'uom_conversion'            => $material_check_source->uom,
                            'status'                    => $material_check_source->status,
                            'qty_on_barcode'            => $material_check_source->qty_on_barcode,
                            'actual_length'             => $material_check_source->actual_length,
                            'begin_width'               => $material_check_source->begin_width,
                            'middle_width'              => $material_check_source->middle_width,
                            'end_width'                 => $material_check_source->end_width,
                            'actual_width'              => $material_check_source->actual_width,
                            'kg'                        => $material_check_source->kg,
                            'point_1'                   => $material_check_source->point_1,
                            'point_2'                   => $material_check_source->point_2,
                            'point_3'                   => $material_check_source->point_3,
                            'point_4'                   => $material_check_source->point_4,
                            'total_point'               => $material_check_source->total_point,
                            'percentage'                => $material_check_source->percentage,
                            'user_id'                   => $material_check_source->user_id,
                            'different_yard'            => $material_check_source->different_yard,
                            'warehouse_id'              => $warehouse_id_new,
                            'created_at'                => $date,
                            'updated_at'                => $date
                        ]);

                        foreach ($detail_material_check_sources as $key_2 => $detail_material_check_source) 
                        {
                            $detail_material_check = DetailMaterialCheck::FirstOrCreate([
                                'material_check_id'         => $material_check_new->id,
                                'start_point_check'         => $detail_material_check_source->start_point_check,
                                'end_point_check'           => $detail_material_check_source->end_point_check,
                                'defect_code'               => $detail_material_check_source->defect_code,
                                'defect_value'              => $detail_material_check_source->defect_value,
                                'is_selected_1'             => $detail_material_check_source->is_selected_1,
                                'is_selected_2'             => $detail_material_check_source->is_selected_2,
                                'is_selected_3'             => $detail_material_check_source->is_selected_3,
                                'is_selected_4'             => $detail_material_check_source->is_selected_4,
                                'remark'                    => trim($detail_material_check_source->remark),
                                'multiply'                  => $detail_material_check_source->multiply,
                                'total_point_defect'        => $detail_material_check_source->total_point_defect,
                                'user_id'                   => $detail_material_check_source->user_id,
                                'created_at'                => $date,
                                'updated_at'                => $date
                            ]);
                        }
                    }
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function storeMonitoringHandoverReceiving($material_arrival_ids)
    {
        $arrivals = MaterialArrival::select('summary_handover_material_id')
        ->whereIn('id',$material_arrival_ids)
        ->groupby('summary_handover_material_id')
        ->get();

        try 
        {
            DB::beginTransaction();
            $array = array();
            foreach ($arrivals as $key => $arrival) 
            {
                $summary_handover_material_id       = $arrival->summary_handover_material_id;

                $monitoring                         = DB::select(db::raw("SELECT * FROM get_monitoring_receiving_fabric_handover_new('".$summary_handover_material_id."');"));
                $summary_handover_material          = SummaryHandoverMaterial::find($summary_handover_material_id);
                $no_pt                              = $summary_handover_material->handover_document_no;

                foreach ($monitoring as $key_2 => $value)
                {
                    $c_bpartner_id                  = $value->c_bpartner_id;
                    $arrival_date                   = $value->arrival_date;
                    $summary_handover_material_id   = $value->summary_handover_material_id;
                    $document_no                    = $value->document_no;
                    $c_order_id                     = $value->c_order_id;
                    $no_invoice                     = $value->no_invoice;
                    $item_id                        = $value->item_id;
                    $item_code                      = $value->item_code;
                    $no_packing_list                = $value->no_packing_list;
                    $supplier_name                  = $value->supplier_name;
                    $color                          = $value->color;
                    $warehouse_id                   = $value->warehouse_id;
                    $total_batch                    = $value->total_batch;
                    $total_roll                     = $value->total_roll;
                    $total_yard                     = $value->total_yard;
                    $user_receive                   = $value->user_receive;
                    $type_supplier                  = $value->type_supplier;

                    
                    $is_exists = MonitoringReceivingFabric::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['summary_handover_material_id',$summary_handover_material_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        [db::raw('upper(item_code)'),$item_code],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_handover',true],
                    ])
                    ->first();

                    if($is_exists)
                    {
                        $is_exists->arrival_date        = $arrival_date;
                        $is_exists->c_order_id          = $c_order_id;
                        $is_exists->item_id             = $item_id;
                        $is_exists->total_batch         = $total_batch;
                        $is_exists->total_roll          = $total_roll;
                        $is_exists->total_yard          = $total_yard;
                        $is_exists->user_receive        = $user_receive;
                        $is_exists->no_pt               = $no_pt;
                        $is_exists->jenis_po            = $type_supplier;
                        $is_exists->save();
                        
                        $monitoring_receiving_fabric_id = $is_exists->id;

                    }else
                    {
                        $monitoring_receiving_fabric    = MonitoringReceivingFabric::FirstOrCreate([
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'arrival_date'                          => $arrival_date,
                            'c_order_id'                            => $c_order_id,
                            'document_no'                           => $document_no,
                            'no_packing_list'                       => $no_packing_list,
                            'no_invoice'                            => $no_invoice,
                            'supplier_name'                         => $supplier_name,
                            'item_id'                               => $item_id,
                            'item_code'                             => $item_code,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'color'                                 => $color,
                            'warehouse_id'                          => $warehouse_id,
                            'total_batch'                           => $total_batch,
                            'total_roll'                            => $total_roll,
                            'total_yard'                            => $total_yard,
                            'user_receive'                          => $user_receive,
                            'is_handover'                           => true,
                            'jenis_po'                              => $type_supplier,
                            'no_pt'                                 => $no_pt
                        ]);

                        $monitoring_receiving_fabric_id = $monitoring_receiving_fabric->id;
                    }

                    MaterialStock::where([
                        ['summary_handover_material_id',$summary_handover_material_id],
                        ['c_order_id',$c_order_id],
                        ['item_id',$item_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                    ])
                    ->whereNotNull('summary_handover_material_id')
                    ->whereNotNull('material_roll_handover_fabric_id')
                    ->whereNull('monitoring_receiving_fabric_id')
                    ->update([
                        'monitoring_receiving_fabric_id' => $monitoring_receiving_fabric_id
                    ]);
                    
                    $array [] = $monitoring_receiving_fabric_id;
                }
            }

            $data = MonitoringReceivingFabric::whereIn('id',$array)->get();

            foreach ($data as $key => $value) 
            {
                $total_roll_inspected           = MaterialStock::where([
                    ['is_master_roll',true],
                    ['monitoring_receiving_fabric_id',$value->id],
                    ['inspect_lot_result', 'RELEASE'],
                ])
                ->whereNotNull('load_actual')
                ->count();

                $value->total_roll_inspected    = $total_roll_inspected;
                $value->save();
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }  

}
