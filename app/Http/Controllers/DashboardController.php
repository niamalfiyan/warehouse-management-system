<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;
use App\Models\User;

use App\Models\MaterialPreparationFabric;
use App\Models\MaterialCheck;
class DashboardController extends Controller
{
    public function preparationAoi2()
    {
        $planning_date = Carbon::now()->parse()->format('Y-m-d');
        $preparation = db::table('dashboard_preparation_v')
                       ->where('warehouse_id', '1000011')
                       ->where('planning_date', $planning_date)
                       ->orderBy('_style', 'asc')
                       ->get();
        $label = '';
        $qty_prepare = '';
        $qty_plan ='';
        $qty_cancel ='';
        $waiting_mm ='';
        $color = '';
        $outstanding_confirmation_qc = '';
        $qty_check = '';
        
        foreach ($preparation as $key => $value) {

            if((sprintf('%0.1f',$value->qty_plan) - sprintf('%0.1f',$value->qty_prepare)) <= 0)
            {
                $color = "color : '#40eb34'";
            }
            else
            {
                $color = "color : '#3477eb'";
            }

            if($value->waiting_mm  < 0) 
            {
                $qty_mm = 0;
            }
            else
            {
                $qty_mm = $value->waiting_mm;
            }
            $material_preparation = MaterialPreparationFabric::where('_style', $value->_style)
                                    ->where('article_no', $value->article_no)
                                    ->first();
                                    
            if($material_preparation)
            {
                $qty_check = MaterialCheck::where('document_no', $material_preparation->document_no)
                                    ->where('item_code', $material_preparation->item_code)
                                    ->where(function ($query) {
                                        $query->whereNull('confirm_user_id')
                                        ->orwhere('final_result', 'HOLD');
                                    })
                                    ->whereNull('deleted_at')
                                    ->sum('qty_on_barcode');

            }                                           
            

            if($value->is_cancel == true)
            {
                $qty_prepare .= '{y:0, '.$color.'},';
                $qty_cancel .= ''.sprintf('%0.1f',$value->qty_prepare).', ';
            }
            else
            {
                $qty_prepare .= '{y:'.sprintf('%0.1f',$value->qty_prepare).', '.$color.'},';
                $qty_cancel .= '0, ';
            }

            $label .= '"'.$value->_style.'::'. $value->article_no.'", ';
            $qty_plan .= ''.sprintf('%0.1f',$value->qty_plan).', ';
            $waiting_mm .= ''.$qty_mm.', ';
            $outstanding_confirmation_qc .= ''.sprintf('%0.1f',$qty_check).', ';
        }
        $planning_date = Carbon::now()->parse()->format('d-M-Y');
        $label = rtrim($label, ', ');
        $target = rtrim($qty_plan, ', ');
        $qty_prepare = rtrim($qty_prepare, ', ');
        return view('dashboard/preparation',compact('label', 'target', 'qty_prepare', 'qty_cancel', 'waiting_mm', 'color', 'planning_date', 'outstanding_confirmation_qc'));
        
    }

    public function preparationAoi1()
    {
        $planning_date = Carbon::now()->parse()->format('Y-m-d');
        $preparation = db::table('dashboard_preparation_v')
                       ->where('warehouse_id', '1000001')
                       ->where('planning_date', $planning_date)
                       ->orderBy('_style', 'asc')
                       ->get();
        $label = '';
        $qty_prepare = '';
        $qty_plan ='';
        $qty_cancel ='';
        $waiting_mm ='';
        $color = '';
        $outstanding_confirmation_qc = '';
        
        foreach ($preparation as $key => $value) {

            if((sprintf('%0.1f',$value->qty_plan) - sprintf('%0.1f',$value->qty_prepare)) <= 0)
            {
                $color = "color : '#40eb34'";
            }
            else
            {
                $color = "color : '#3477eb'";
            }

            if($value->waiting_mm  < 0) 
            {
                $qty_mm = 0;
            }
            else
            {
                $qty_mm = $value->waiting_mm;
            }
            $material_preparation = MaterialPreparationFabric::where('_style', $value->_style)
                                    ->where('article_no', $value->article_no)
                                    ->first();
                                    
            if($material_preparation)
            {
                $qty_check = MaterialCheck::where('document_no', $material_preparation->document_no)
                                    ->where('item_code', $material_preparation->item_code)
                                    ->where(function ($query) {
                                        $query->whereNull('confirm_user_id')
                                        ->orwhere('final_result', 'HOLD');
                                    })
                                    ->whereNull('deleted_at')
                                    ->sum('qty_on_barcode');

            }                                           
            

            if($value->is_cancel == true)
            {
                $qty_prepare .= '{y:0, '.$color.'},';
                $qty_cancel .= ''.sprintf('%0.1f',$value->qty_prepare).', ';
            }
            else
            {
                $qty_prepare .= '{y:'.sprintf('%0.1f',$value->qty_prepare).', '.$color.'},';
                $qty_cancel .= '0, ';
            }

            $label .= '"'.$value->_style.'::'. $value->article_no.'", ';
            $qty_plan .= ''.sprintf('%0.1f',$value->qty_plan).', ';
            $waiting_mm .= ''.$qty_mm.', ';
            $outstanding_confirmation_qc .= ''.sprintf('%0.1f',$qty_check).', ';
        }
        $planning_date = Carbon::now()->parse()->format('d-M-Y');
        $label = rtrim($label, ', ');
        $target = rtrim($qty_plan, ', ');
        $qty_prepare = rtrim($qty_prepare, ', ');
        return view('dashboard/preparation',compact('label', 'target', 'qty_prepare', 'qty_cancel', 'waiting_mm', 'color', 'planning_date', 'outstanding_confirmation_qc'));
        
    }

    // dashboard baru

    public function preparation_fabric1()
    {
        // qty_need = sedang prepare
         // qty_prepare = yg telah selese
        $wms_replication = DB::connection('replication_wms_live');

 
         $wh = "AOI 1";
         $planning_date_now      = Carbon::now()->parse()->format('Y-m-d');
         $planning_date_tomorrow = Carbon::tomorrow()->format('Y-m-d');
 
         $preparation = $wms_replication->select("SELECT
                        mpp.STYLE,
                        mpp.article_no,
                        mpp.planning_date,
                        sum(mpp.total_qty_need) as qty_need,
                        sum(mpp.total_qty_supply) AS supply,
                        sum(( mpp.total_qty_need - mpp.total_qty_supply )) AS on_progress,
                        COALESCE(SUM(( SELECT total_qty_inspected 
                        FROM material_stocks 
                        WHERE deleted_at IS NULL AND ID = mp.material_stock_id and mp.last_status !='supply' )),0) AS qty_inspect,
                        mpp.factory_id
                    FROM
                        material_planning_preparations mpp
                        LEFT JOIN material_preparations mp ON mp.material_planning_preparation_id = mpp.ID 
                    WHERE
                        mpp.deleted_at IS NULL
                        AND mpp.uom = 'yds'
                        AND mpp.style is not null
                        AND mp.deleted_at IS NULL
                        AND mpp.is_new_bima = 't'
                        and mpp.planning_date = '$planning_date_now'
                        and mpp.factory_id ='1000001'
                    GROUP BY 
                    mpp.STYLE,
                        mpp.article_no,
                        mpp.planning_date,
                        mpp.factory_id;");

                        // dd($preparation);
         $label = '';
         $sent = '';
         $qty_plan ='';
         $inspect = '';
         $qty_check = '';
 
         $progress = '';
 
         foreach ($preparation as $key => $value) {
 
 
            
 
             // $x .= (sprintf('%0.1f',$value->qty_plan) - sprintf('%0.1f',$value->qty_prepare));
             $label .= '"'.$value->style.'::'. $value->article_no.'", ';
             $qty_plan .= ''.sprintf('%0.1f',$value->qty_need).', ';
             $sent .= ''.sprintf('%0.1f', $value->supply).', ';
             $inspect .= ''.sprintf('%0.1f',$value->qty_inspect).', ';
             $progress .= ''.sprintf('%0.1f',$value->on_progress).', ';
 
             
         }
 
         $label = rtrim($label, ', ');
         $inspect = rtrim($inspect, ', ');
         $target = rtrim($qty_plan, ', ');
         $progress = rtrim($progress, ', ');
         $sent = rtrim($sent, ', ');
 
         return view('dashboard.pre_fab',compact('wh', 'label', 'planning_date_now', 'target', 'sent', 'inspect', 'progress','planning_date_tomorrow'));
         // return view('dashboard.pre_fab');
    }

    public function preparation_fabric2()
    {
       // qty_need = sedang prepare
        // qty_prepare = yg telah selese
        $wms_replication = DB::connection('replication_wms_live');


        $wh = "AOI 2";
        $planning_date_now      = Carbon::now()->parse()->format('Y-m-d');
        $planning_date_tomorrow = Carbon::tomorrow()->format('Y-m-d');
      

        $preparation = $wms_replication->select("SELECT
                        mpp.STYLE,
                        mpp.article_no,
                        mpp.planning_date,
                        sum(mpp.total_qty_need) as qty_need,
                        sum(mpp.total_qty_supply) AS supply,
                        sum(( mpp.total_qty_need - mpp.total_qty_supply )) AS on_progress,
                        COALESCE(SUM(( SELECT total_qty_inspected 
                        FROM material_stocks 
                        WHERE deleted_at IS NULL AND ID = mp.material_stock_id and mp.last_status !='supply' )),0) AS qty_inspect,
                        mpp.factory_id
                    FROM
                        material_planning_preparations mpp
                        LEFT JOIN material_preparations mp ON mp.material_planning_preparation_id = mpp.ID 
                    WHERE
                        mpp.deleted_at IS NULL
                        AND mpp.uom = 'yds'
                        AND mpp.style is not null
                        AND mp.deleted_at IS NULL
                        AND mpp.is_new_bima = 't'
                        and mpp.planning_date = '$planning_date_now'
                        and mpp.factory_id ='1000011'
                    GROUP BY 
                    mpp.STYLE,
                        mpp.article_no,
                        mpp.planning_date,
                        mpp.factory_id;");

        $label = '';
        $sent = '';
        $qty_plan ='';
        $inspect = '';
        $qty_check = '';

        $progress = '';

        foreach ($preparation as $key => $value) {

            $label .= '"'.$value->style.'::'. $value->article_no.'", ';
             $qty_plan .= ''.sprintf('%0.1f',$value->qty_need).', ';
             $sent .= ''.sprintf('%0.1f', $value->supply).', ';
             $inspect .= ''.sprintf('%0.1f',$value->qty_inspect).', ';
             $progress .= ''.sprintf('%0.1f',$value->on_progress).', ';

            
        }

        $label = rtrim($label, ', ');
        $inspect = rtrim($inspect, ', ');
        $target = rtrim($qty_plan, ', ');
        $progress = rtrim($progress, ', ');
        $sent = rtrim($sent, ', ');

        return view('dashboard.pre_fab',compact('wh', 'label', 'planning_date_now', 'target', 'sent', 'inspect', 'progress','planning_date_tomorrow'));
        // return view('dashboard.pre_fab');
    }


    // public function preparation_fabric1()
    // {
    //     // qty_need = sedang prepare
    //      // qty_prepare = yg telah selese
 
    //      $wh = "AOI 1";
    //      $planning_date_now      = Carbon::now()->parse()->format('Y-m-d');
    //      $planning_date_tomorrow = Carbon::tomorrow()->format('Y-m-d');
 
    //      $preparation = db::table('dashboard_preparation_v')
    //                     ->where('warehouse_id', '1000001')
    //                     ->where('planning_date', $planning_date_now)
    //                     ->where('is_cancel', false)
    //                     ->orderBy('_style', 'asc')
    //                     ->get();
    //      $label = '';
    //      $sent = '';
    //      $qty_plan ='';
    //      $inspect = '';
    //      $qty_check = '';
 
    //      $progress = '';
 
    //      foreach ($preparation as $key => $value) {
 
    //          $material_preparation = DB::table('material_preparation_fabrics')
    //                                  ->where('_style', $value->_style)
    //                                  ->where('article_no', $value->article_no)
    //                                  ->first();
 
    //          if($material_preparation)
    //          {
    //              $qty_check = DB::table('material_checks')
    //                              ->where('document_no', $material_preparation->document_no)
    //                              ->where('item_code', $material_preparation->item_code)
    //                              ->where(function ($query) {
    //                                      $query->whereNull('confirm_user_id')
    //                                      ->orwhere('final_result', 'HOLD');
    //                                  })
    //                                  ->whereNull('deleted_at')
    //                                  ->sum('qty_on_barcode');
    //          }
 
    //          // $x .= (sprintf('%0.1f',$value->qty_plan) - sprintf('%0.1f',$value->qty_prepare));
    //          $label .= '"'.$value->_style.'::'. $value->article_no.'", ';
    //          $qty_plan .= ''.sprintf('%0.1f',$value->qty_plan).', ';
    //          $sent .= ''.sprintf('%0.1f', $value->qty_prepare).', ';
    //          $inspect .= ''.sprintf('%0.1f',$qty_check).', ';
 
    //          $onProgress = ($value->qty_plan - $value->qty_prepare);
 
    //          if ($onProgress <= 0) {
    //              $proses = 0;
    //          } else {
    //              $proses = $onProgress;
    //          }
 
    //          $progress .= ''.sprintf('%0.1f',$proses).', ';
 
             
    //      }
 
    //      $label = rtrim($label, ', ');
    //      $inspect = rtrim($inspect, ', ');
    //      $target = rtrim($qty_plan, ', ');
    //      $progress = rtrim($progress, ', ');
    //      $sent = rtrim($sent, ', ');
 
    //      return view('dashboard.pre_fab',compact('wh', 'label', 'planning_date_now', 'target', 'sent', 'inspect', 'progress','planning_date_tomorrow'));
    //      // return view('dashboard.pre_fab');
    // }

    // public function preparation_fabric2()
    // {
    //    // qty_need = sedang prepare
    //     // qty_prepare = yg telah selese

    //     $wh = "AOI 2";
    //     $planning_date_now      = Carbon::now()->parse()->format('Y-m-d');
    //     $planning_date_tomorrow = Carbon::tomorrow()->format('Y-m-d');
      

    //     $preparation = db::table('dashboard_preparation_v')
    //                    ->where('warehouse_id', '1000011')
    //                    ->where('planning_date', $planning_date_now)
    //                    ->where('is_cancel', false)
    //                    ->orderBy('_style', 'asc')
    //                    ->get();
    //     $label = '';
    //     $sent = '';
    //     $qty_plan ='';
    //     $inspect = '';
    //     $qty_check = '';

    //     $progress = '';

    //     foreach ($preparation as $key => $value) {

    //         $material_preparation = DB::table('material_preparation_fabrics')
    //                                 ->where('_style', $value->_style)
    //                                 ->where('article_no', $value->article_no)
    //                                 ->first();

    //         if($material_preparation)
    //         {
    //             $qty_check = DB::table('material_checks')
    //                             ->where('document_no', $material_preparation->document_no)
    //                             ->where('item_code', $material_preparation->item_code)
    //                             ->where(function ($query) {
    //                                     $query->whereNull('confirm_user_id')
    //                                     ->orwhere('final_result', 'HOLD');
    //                                 })
    //                                 ->whereNull('deleted_at')
    //                                 ->sum('qty_on_barcode');
    //         }

    //         // $x .= (sprintf('%0.1f',$value->qty_plan) - sprintf('%0.1f',$value->qty_prepare));
    //         $label .= '"'.$value->_style.'::'. $value->article_no.'", ';
    //         $qty_plan .= ''.sprintf('%0.1f',$value->qty_plan).', ';
    //         $sent .= ''.sprintf('%0.1f', $value->qty_prepare).', ';
    //         $inspect .= ''.sprintf('%0.1f',$qty_check).', ';

    //         $onProgress = ($value->qty_plan - $value->qty_prepare);

    //         if ($onProgress <= 0) {
    //             $proses = 0;
    //         } else {
    //             $proses = $onProgress;
    //         }

    //         $progress .= ''.sprintf('%0.1f',$proses).', ';

            
    //     }

    //     $label = rtrim($label, ', ');
    //     $inspect = rtrim($inspect, ', ');
    //     $target = rtrim($qty_plan, ', ');
    //     $progress = rtrim($progress, ', ');
    //     $sent = rtrim($sent, ', ');

    //     return view('dashboard.pre_fab',compact('wh', 'label', 'planning_date_now', 'target', 'sent', 'inspect', 'progress','planning_date_tomorrow'));
    //     // return view('dashboard.pre_fab');
    // }

    // public function AccReceivingAoi1()
    // {
    //     $wh = "AOI 1";
    //     $receiving_date1 = Carbon::now()->format('d F Y');
    //     $receiving_date2 = Carbon::now()->subDays(6)->format('d F Y');

    //     $now = Carbon::now()->format('Y-m-d');
    //     $fivedays = Carbon::now()->subDays(5)->format('Y-m-d');

    //     //definisi
    //     $date="";
    //     $receive = "";
    //     $metal_check = "";
    //     $in = "";
    //     $reject = "";
    //     $qc_release = "";
    //     $hold= "";

    //     $acc = DB::table('gn_receiving_status_acc_v')
    //             ->whereBetween('date', [$fivedays, $now])
    //             ->where('warehouse', '1000002')
    //             ->get();

    //     foreach ($acc as $key => $value){
    //         $date .= '"'.$value->date.'", ';
    //         $receive .= ''.($value->metal_check+$value->in+$value->reject+$value->qc_release+$value->hold).', ';
    //         $metal_check .= ''.$value->metal_check.', ';
    //         $in .= ''.$value->in.', ';
    //         $reject .= ''.$value->reject.', ';
    //         $qc_release .= ''.$value->qc_release.', ';
    //         $hold .= ''.$value->hold.', ';
    //     }

    //     $date = rtrim($date, ', ');
    //     $receive = rtrim($receive, ', ');
    //     $metal_check = rtrim($metal_check, ', ');
    //     $in = rtrim($in, ', ');
    //     $reject = rtrim($reject, ', ');
    //     $qc_release = rtrim($qc_release, ', ');
    //     $hold = rtrim($hold, ', ');

    //     return view('dashboard.receiving_acc', compact('wh', 'receiving_date1', 'receiving_date2', 'date', 'receive', 'metal_check', 'in', 'reject', 'qc_release', 'hold'));
    // }

    public function AccReceivingAoi1()
    {
        $wms_replication = DB::connection('replication_wms_live');

        $wh = "AOI 1";
        $receiving_date1 = Carbon::now()->format('d F Y');
        $receiving_date2 = Carbon::now()->subDays(6)->format('d F Y');

        $now = Carbon::now()->format('Y-m-d');
        $fivedays = Carbon::now()->subDays(5)->format('Y-m-d');

        //definisi
        $date="";
        $receive = "";
        $metal_check = "";
        $in = "";
        $reject = "";
        $qc_release = "";
        $hold= "";

        
        $acc = $wms_replication->select("SELECT date(last_movement_date) as date, factory_id,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'receive'::text) THEN 'receive'::text
                                        ELSE NULL::text
                                    END) AS receive,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'check'::text) THEN 'check'::text
                                        ELSE NULL::text
                                    END) AS metal_check,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'in'::text) THEN 'in'::text
                                        ELSE NULL::text
                                    END) AS in,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'reject'::text) THEN 'reject'::text
                                        ELSE NULL::text
                                    END) AS reject,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'qc-release'::text) THEN 'RELEASE'::text
                                        ELSE NULL::text
                                    END) AS qc_release,
                                    count(
                                    CASE
                                        WHEN ((material_preparations.qc_result)::text = 'hold'::text) THEN 'HOLD'::text
                                        ELSE NULL::text
                                    END) AS hold
                            FROM
                                material_preparations 
                            WHERE
                                factory_id = '1000002'
                                AND is_new_bima = 't'
                                AND created_at is not null
                                AND deleted_at is null
                                AND last_movement_date is not null
                                and date(last_movement_date) between '$fivedays' and '$now'
                            GROUP BY 
                            (date(material_preparations.last_movement_date)), 
                            material_preparations.factory_id
                            ORDER BY
                            (date(material_preparations.last_movement_date)) desc");

        foreach ($acc as $key => $value){
            $date .= '"'.$value->date.'", ';
            $receive .= ''.($value->metal_check+$value->in+$value->reject+$value->qc_release+$value->hold).', ';
            $metal_check .= ''.$value->metal_check.', ';
            $in .= ''.$value->in.', ';
            $reject .= ''.$value->reject.', ';
            $qc_release .= ''.$value->qc_release.', ';
            $hold .= ''.$value->hold.', ';
        }

        $date = rtrim($date, ', ');
        $receive = rtrim($receive, ', ');
        $metal_check = rtrim($metal_check, ', ');
        $in = rtrim($in, ', ');
        $reject = rtrim($reject, ', ');
        $qc_release = rtrim($qc_release, ', ');
        $hold = rtrim($hold, ', ');

        return view('dashboard.receiving_acc', compact('wh', 'receiving_date1', 'receiving_date2', 'date', 'receive', 'metal_check', 'in', 'reject', 'qc_release', 'hold'));
    }

    // public function AccReceivingAoi2()
    // {
    //     $wh = "AOI 2";
    //     $receiving_date1 = Carbon::now()->format('d F y');
    //     $receiving_date2 = Carbon::now()->subDays(5)->format('d F y');

    //     $now = Carbon::now()->format('Y-m-d');
    //     $fivedays = Carbon::now()->subDays(5)->format('Y-m-d');

    //     //definisi
    //     $date="";
    //     $receive = "";
    //     $metal_check = "";
    //     $in = "";
    //     $reject = "";
    //     $qc_release = "";
    //     $hold= "";

    //     $acc = DB::table('gn_receiving_status_acc_v')
    //             ->whereBetween('date', [$fivedays, $now])
    //             ->where('warehouse', '1000013')
    //             ->orderBy('date')
    //             ->get();

    //     foreach ($acc as $key => $value){
    //         $date .= '"'.$value->date.'", ';
    //         $receive .= ''.($value->metal_check+$value->in+$value->reject+$value->qc_release+$value->hold).', ';
    //         $metal_check .= ''.$value->metal_check.', ';
    //         $in .= ''.$value->in.', ';
    //         $reject .= ''.$value->reject.', ';
    //         $qc_release .= ''.$value->qc_release.', ';
    //         $hold .= ''.$value->hold.', ';
    //     }

    //     $date = rtrim($date, ', ');
    //     $receive = rtrim($receive, ', ');
    //     $metal_check = rtrim($metal_check, ', ');
    //     $in = rtrim($in, ', ');
    //     $reject = rtrim($reject, ', ');
    //     $qc_release = rtrim($qc_release, ', ');
    //     $hold = rtrim($hold, ', ');

    //     return view('dashboard.receiving_acc', compact('wh', 'receiving_date1', 'receiving_date2', 'date', 'receive', 'metal_check', 'in', 'reject', 'qc_release', 'hold'));
    // }

    public function AccReceivingAoi2()
    {
        $wms_replication = DB::connection('replication_wms_live');

     
        $wh = "AOI 2";
        $receiving_date1 = Carbon::now()->format('d F y');
        $receiving_date2 = Carbon::now()->subDays(5)->format('d F y');

        $now = Carbon::now()->format('Y-m-d');
        $fivedays = Carbon::now()->subDays(5)->format('Y-m-d');

        //definisi
        $date="";
        $receive = "";
        $metal_check = "";
        $in = "";
        $reject = "";
        $qc_release = "";
        $hold= "";

        // $acc = DB::table('gn_receiving_status_acc_v')
        //         ->whereBetween('date', [$fivedays, $now])
        //         ->where('warehouse', '1000013')
        //         ->orderBy('date')
        //         ->get();

        $acc = $wms_replication->select("SELECT date(last_movement_date) as date, factory_id,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'receive'::text) THEN 'receive'::text
                                        ELSE NULL::text
                                    END) AS receive,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'check'::text) THEN 'check'::text
                                        ELSE NULL::text
                                    END) AS metal_check,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'in'::text) THEN 'in'::text
                                        ELSE NULL::text
                                    END) AS in,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'reject'::text) THEN 'reject'::text
                                        ELSE NULL::text
                                    END) AS reject,
                                count(
                                    CASE
                                        WHEN ((material_preparations.last_status)::text = 'qc-release'::text) THEN 'RELEASE'::text
                                        ELSE NULL::text
                                    END) AS qc_release,
                                    count(
                                    CASE
                                        WHEN ((material_preparations.qc_result)::text = 'hold'::text) THEN 'HOLD'::text
                                        ELSE NULL::text
                                    END) AS hold
                            FROM
                                material_preparations 
                            WHERE
                                factory_id = '1000013'
                                AND is_new_bima = 't'
                                AND created_at is not null
                                AND deleted_at is null
                                AND last_movement_date is not null
                                and date(last_movement_date) between '$fivedays' and '$now'
                            GROUP BY 
                            (date(material_preparations.last_movement_date)), 
                            material_preparations.factory_id
                            ORDER BY
                            (date(material_preparations.last_movement_date)) desc");
                            // dd($acc);

        foreach ($acc as $key => $value){
            $date .= '"'.$value->date.'", ';
            $receive .= ''.($value->metal_check+$value->in+$value->reject+$value->qc_release+$value->hold).', ';
            $metal_check .= ''.$value->metal_check.', ';
            $in .= ''.$value->in.', ';
            $reject .= ''.$value->reject.', ';
            $qc_release .= ''.$value->qc_release.', ';
            $hold .= ''.$value->hold.', ';
        }

        $date = rtrim($date, ', ');
        $receive = rtrim($receive, ', ');
        $metal_check = rtrim($metal_check, ', ');
        $in = rtrim($in, ', ');
        $reject = rtrim($reject, ', ');
        $qc_release = rtrim($qc_release, ', ');
        $hold = rtrim($hold, ', ');

        return view('dashboard.receiving_acc', compact('wh', 'receiving_date1', 'receiving_date2', 'date', 'receive', 'metal_check', 'in', 'reject', 'qc_release', 'hold'));
    }

    // public function FabStatusAoi1()
    // {
    //     $wh = 'AOI 1';
    //     $now = Carbon::now()->parse()->format('d F Y');
    //     $max = 1113000;
    //     $free= 0;
    //     $all = 0;

    //     $allocated   = 0; $slowmo = 0;
    //     $unallocated = 0; $running= 0;

    //     $alllYDS = 0;    $slowmoYDS = 0;    $runningYDS = 0;    $unallocatedYDS = 0;    $allocatedYDS = 0;
    //     $allM    = 0;    $slowmoM   = 0;    $runningM   = 0;    $unallocatedM   = 0;    $allocatedM   = 0;
    //     $allMc   = 0;    $slowmoMc = 0;     $runningMc  = 0;    $unallocatedMc   = 0;   $allocatedMc = 0;

    //     $alokasi = DB::select("SELECT SUM
    //                             ( stock - reserved_qty ) AS unallocated,
    //                             SUM ( available_qty ) AS allocated,
    //                             uom 
    //                         FROM
    //                             material_stocks 
    //                         WHERE
    //                             deleted_at IS NULL 
    //                             AND locator_id IS NOT NULL 
    //                             AND summary_stock_fabric_id IS NOT NULL 
    //                             AND approval_user_id IS NOT NULL 
    //                             AND approval_date IS NOT NULL 
    //                             AND load_actual IS NOT NULL 
    //                             AND warehouse_id = '1000001' 
    //                         GROUP BY
    //                             uom");
        
    //     foreach ($alokasi as $key => $alokasis) {
    //         if ($alokasis->uom == 'YDS') {
    //             $allocatedYDS   = $alokasis->allocated;
    //             $unallocatedYDS = $alokasis->unallocated;
    //         } elseif ($alokasis->uom == 'M') {
    //             $allocatedM     = $alokasis->allocated;
    //             $unallocatedM = $alokasis->unallocated;

    //             $allocatedMc = $allocatedM*1.094;
    //             $unallocatedMc = $unallocatedM*1.094;
    //         }
    //     }

    //     // jumlah semua alokasi dan unalokasi
    //     $allocated  = $allocatedYDS + $allocatedMc;
    //     $unallocated= $unallocatedYDS + $unallocatedMc;

    //     //jml all
    //     // $allYDS = $allocatedYDS + $unallocatedYDS;
    //     // $allM   = $allocatedM   + $allocatedM;
    //     // $allMc  = $allocatedMc  + $unallocatedMc;

    //     $all = $allocated + $unallocated;

    //     //free kapasitas
    //     $free   = $max - ($allocated + $unallocated);
    //     $freeCapacity = ($free < 0) ? 0 : $free ;
        
    //     $year = DB::select("SELECT SUM
    //                         ( stock - reserved_qty ) + SUM ( available_qty ) AS jml,
    //                         uom,
    //                         date_part( 'year', age( created_at ) ) AS tahun 
    //                     FROM
    //                         material_stocks 
    //                     WHERE
    //                         deleted_at IS NULL 
    //                         AND locator_id IS NOT NULL 
    //                         AND summary_stock_fabric_id IS NOT NULL 
    //                         AND approval_user_id IS NOT NULL 
    //                         AND approval_date IS NOT NULL 
    //                         AND load_actual IS NOT NULL 
    //                         AND warehouse_id = '1000001'  
    //                     GROUP BY
    //                         uom,
    //                         tahun");

    //     foreach ($year as $key => $byear) {
    //         if ($byear->tahun >= '2') {
    //             if ($byear->uom == 'YDS') {
    //                 $slowmoYDS = $byear->jml;
    //             } else {
    //                 $slowmoM   = $byear->jml;
    //                 $slowmoMc  = $slowmoM*1.094;
    //             }
    //         } else {
    //             if ($byear->uom == 'YDS') {
    //                 $runningYDS += $byear->jml;
    //             } else {
    //                 $runningM   += $byear->jml;
    //                 $runningMc  = $runningM*1.094;
    //             }
    //         }
    //     }

    //     $slowmo     = $slowmoYDS + $slowmoMc;
    //     $running    = $runningYDS + $runningMc;

    //     // return view('dashboard.dashboardFabStats', compact('wh', 'max', 'now', 'allYDS', 'allM', 'allMc', 'slowmoYDS', 'slowmoM',
    //     //             'runningYDS', 'runningM', 'unallocatedYDS', 'unallocatedM', 'allocatedYDS', 'allocatedM',
    //     //             'slowmoMc', 'runningMc', 'unallocatedMc', 'allocatedMc', 'freeCapacity'));

    //     return view('dashboard.dashboardFabStats', compact('wh', 'now', 'max', 'free', 'freeCapacity', 'all', 
    //     'slowmo', 'running', 'allocated', 'unallocated'));
    // }

    // public function FabStatusAoi2()
    // {
    //     $wh = 'AOI 2';
    //     $now = Carbon::now()->parse()->format('d F Y');
    //     $max = 1432200;
    //     $free= 0;
    //     $all = 0;

    //     $allocated   = 0; $slowmo = 0;
    //     $unallocated = 0; $running= 0;

    //     $alllYDS = 0;    $slowmoYDS = 0;    $runningYDS = 0;    $unallocatedYDS = 0;    $allocatedYDS = 0;
    //     $allM    = 0;    $slowmoM   = 0;    $runningM   = 0;    $unallocatedM   = 0;    $allocatedM   = 0;
    //     $allMc   = 0;    $slowmoMc = 0;     $runningMc  = 0;    $unallocatedMc   = 0;   $allocatedMc = 0;

    //     $alokasi = DB::select("SELECT SUM
    //                             ( stock - reserved_qty ) AS unallocated,
    //                             SUM ( available_qty ) AS allocated,
    //                             uom 
    //                         FROM
    //                             material_stocks 
    //                         WHERE
    //                             deleted_at IS NULL 
    //                             AND locator_id IS NOT NULL 
    //                             AND summary_stock_fabric_id IS NOT NULL 
    //                             AND approval_user_id IS NOT NULL 
    //                             AND approval_date IS NOT NULL 
    //                             AND load_actual IS NOT NULL 
    //                             AND warehouse_id = '1000011' 
    //                         GROUP BY
    //                             uom");
        
    //     foreach ($alokasi as $key => $alokasis) {
    //         if ($alokasis->uom == 'YDS') {
    //             $allocatedYDS   = $alokasis->allocated;
    //             $unallocatedYDS = $alokasis->unallocated;
    //         } elseif ($alokasis->uom == 'M') {
    //             $allocatedM     = $alokasis->allocated;
    //             $unallocatedM = $alokasis->unallocated;

    //             $allocatedMc = $allocatedM*1.094;
    //             $unallocatedMc = $unallocatedM*1.094;
    //         }
    //     }

    //     // jumlah semua alokasi dan unalokasi
    //     $allocated  = $allocatedYDS + $allocatedMc;
    //     $unallocated= $unallocatedYDS + $unallocatedMc;

    //     //jml all
    //     // $allYDS = $allocatedYDS + $unallocatedYDS;
    //     // $allM   = $allocatedM   + $allocatedM;
    //     // $allMc  = $allocatedMc  + $unallocatedMc;

    //     $all = $allocated + $unallocated;

    //     //free kapasitas
    //     $free   = $max - ($allocated + $unallocated);
    //     $freeCapacity = ($free < 0) ? 0 : $free ;
        
    //     $year = DB::select("SELECT SUM
    //                         ( stock - reserved_qty ) + SUM ( available_qty ) AS jml,
    //                         uom,
    //                         date_part( 'year', age( created_at ) ) AS tahun 
    //                     FROM
    //                         material_stocks 
    //                     WHERE
    //                         deleted_at IS NULL 
    //                         AND locator_id IS NOT NULL 
    //                         AND summary_stock_fabric_id IS NOT NULL 
    //                         AND approval_user_id IS NOT NULL 
    //                         AND approval_date IS NOT NULL 
    //                         AND load_actual IS NOT NULL 
    //                         AND warehouse_id = '1000011'  
    //                     GROUP BY
    //                         uom,
    //                         tahun");

    //     foreach ($year as $key => $byear) {
    //         if ($byear->tahun >= '2') {
    //             if ($byear->uom == 'YDS') {
    //                 $slowmoYDS = $byear->jml;
    //             } else {
    //                 $slowmoM   = $byear->jml;
    //                 $slowmoMc  = $slowmoM*1.094;
    //             }
    //         } else {
    //             if ($byear->uom == 'YDS') {
    //                 $runningYDS += $byear->jml;
    //             } else {
    //                 $runningM   += $byear->jml;
    //                 $runningMc  = $runningM*1.094;
    //             }
    //         }
    //     }

    //     $slowmo     = $slowmoYDS + $slowmoMc;
    //     $running    = $runningYDS + $runningMc;

    //     // return view('dashboard.dashboardFabStats', compact('wh', 'max', 'now', 'allYDS', 'allM', 'allMc', 'slowmoYDS', 'slowmoM',
    //     //             'runningYDS', 'runningM', 'unallocatedYDS', 'unallocatedM', 'allocatedYDS', 'allocatedM',
    //     //             'slowmoMc', 'runningMc', 'unallocatedMc', 'allocatedMc', 'freeCapacity'));

    //     return view('dashboard.dashboardFabStats', compact('wh', 'now', 'max', 'free', 'freeCapacity', 'all', 
    //     'slowmo', 'running', 'allocated', 'unallocated'));
    // }

    public function FabStatusAoi1()
    {
        $wms_replication = DB::connection('wms_live');

        $wh = 'AOI 1';
        $now = Carbon::now()->parse()->format('d F Y');
        $max = 1113000;
        $free= 0;
        $all = 0;

        $allocated   = 0; $slowmo = 0;
        $unallocated = 0; $running= 0;

        $alllYDS = 0;    $slowmoYDS = 0;    $runningYDS = 0;    $unallocatedYDS = 0;    $allocatedYDS = 0;
        $allM    = 0;    $slowmoM   = 0;    $runningM   = 0;    $unallocatedM   = 0;    $allocatedM   = 0;
        $allMc   = 0;    $slowmoMc = 0;     $runningMc  = 0;    $unallocatedMc   = 0;   $allocatedMc = 0;


        $todayDate =Carbon::now()->format('Y-m-d');
        $weekago = Carbon::now()->subDays(7)->format('Y-m-d');
        $allocatedYds = $wms_replication->select("SELECT sum(qty_inhouse) as allocated 
                from gn_report_dashboard_material 
                where warehouse_update='Packing AOI 1' 
                and (pcd>='".$todayDate."' or pcd is null)");
        
        //dd($allocatedYds);        

        // $convertAllocatedM = $allocatedM[0]->relax_qty * 1.094;
        $allocated = $allocatedYds[0]->allocated;
        
        $q_all=$wms_replication->select("
                                SELECT SUM
                                ( total_stock_inventory ) AS total_stock,
                                    factory_id,
                                                    uom_conversion	
                                FROM
                                    material_stocks 
                                WHERE
                                    deleted_at IS NULL
                                    AND created_at is not null	
                                    AND is_fabric IS TRUE 
                                    AND factory_id='1000001'										
                                GROUP BY factory_id, uom_conversion"
            );
        $all='0';
        foreach ($q_all as $hsl_all) {
            if ($hsl_all->uom_conversion == 'YDS' || $hsl_all->uom_conversion == 'yds' ) {
                $all=$all+$hsl_all->total_stock;
            }
            elseif($hsl_all->uom_conversion == 'M' || $hsl_all->uom_conversion == 'm'){
                $stokm=$hsl_all->total_stock;
                $stokyds=$stokm*1.094;
                $all=$all+$stokyds;
            }
            elseif ($hsl_all->uom_conversion == 'PCS' || $hsl_all->uom_conversion == 'pcs') {
                $all=$all+$hsl_all->total_stock;
            }
            else{}
        }

        $unallocatedAll=$all-$allocated;

        //free kapasitas
        $free   = $max - ($allocated + $unallocatedAll);
        $freeCapacity = ($free < 0) ? 0 : $free ;

        $stockSlowMoving = $wms_replication->select ("
                SELECT SUM
                    ( total_stock_inventory ) AS total_stock,
                    factory_id
                FROM
                    material_stocks 
                WHERE
                    deleted_at IS NULL
                AND created_at is not null	
                    AND is_fabric IS TRUE 
                    AND factory_id IN ('1000001')
                    AND created_at < now() - INTERVAL '2 YEAR'
                GROUP BY
                    factory_id                                    
        ");
                                    
        $slowMoving = 0;
        $slowMovingUom = 0;
        $Running = 0;
        $RunningUom = 0;
        $slowMovingP = 0;
        $slowMovingPM = 0;
        $RunningP = 0;
        $RunningPM = 0;
        if($stockSlowMoving != null)
        {
            $slowMoving = $stockSlowMoving[0]->total_stock;
        }
        

        $newSlowMoving = $slowMoving ;
        // $newSlowMoving = $slowMoving + $slowMovingUom;
      
        $Running = $all-$newSlowMoving;
      
        $newRunning = $Running ;
        // $newRunning = $Running + $RunningUom;
        //$usedLeft = $newRunning + $newSlowMoving;
        // dd($usedLeft);
        $usedLeft=$all;
        $newFreeCapacity = $max - ($usedLeft);
        
        return view('dashboard.dashboardFabStats', compact('wh', 'now', 'max', 'free', 'freeCapacity', 'all', 
        'newSlowMoving', 'newRunning','usedLeft', 'allocated', 'unallocatedAll','newFreeCapacity'));
    }

    public function FabStatusAoi2()
    {
        $wh = 'AOI 2';
        $now = Carbon::now()->parse()->format('d F Y');
        $max = 1132200;
        $free= 0;
        $all = 0;
        $wms_replication = DB::connection('wms_live');
        $erp = DB::connection('erp');
        $allocated   = 0; $slowmo = 0;
        $unallocated = 0; $running= 0;

        $alllYDS = 0;    $slowmoYDS = 0;    $runningYDS = 0;    $unallocatedYDS = 0;    $allocatedYDS = 0;
        $allM    = 0;    $slowmoM   = 0;    $runningM   = 0;    $unallocatedM   = 0;    $allocatedM   = 0;
        $allMc   = 0;    $slowmoMc = 0;     $runningMc  = 0;    $unallocatedMc   = 0;   $allocatedMc = 0;

        $todayDate =Carbon::now()->format('Y-m-d');
        $weekago = Carbon::now()->subDays(7)->format('Y-m-d');
        $allocatedYds = $wms_replication->select("SELECT sum(qty_inhouse) as allocated 
                from gn_report_dashboard_material 
                where warehouse_update='Packing AOI 2' 
                and (pcd>='".$todayDate."' or pcd is null)");
        
        //dd($allocatedYds);        

        // $convertAllocatedM = $allocatedM[0]->relax_qty * 1.094;
        $allocated = $allocatedYds[0]->allocated;
        
        $q_all=$wms_replication->select("
                                SELECT SUM
                                ( total_stock_inventory ) AS total_stock,
                                    factory_id,
                                                    uom_conversion	
                                FROM
                                    material_stocks 
                                WHERE
                                    deleted_at IS NULL
                                    AND created_at is not null	
                                    AND is_fabric IS TRUE 
                                    AND factory_id='1000011'										
                                GROUP BY factory_id, uom_conversion"
            );
        $all='0';
        foreach ($q_all as $hsl_all) {
            if ($hsl_all->uom_conversion == 'YDS' || $hsl_all->uom_conversion == 'yds' ) {
                $all=$all+$hsl_all->total_stock;
            }
            elseif($hsl_all->uom_conversion == 'M' || $hsl_all->uom_conversion == 'm'){
                $stokm=$hsl_all->total_stock;
                $stokyds=$stokm*1.094;
                $all=$all+$stokyds;
            }
            elseif ($hsl_all->uom_conversion == 'PCS' || $hsl_all->uom_conversion == 'pcs') {
                $all=$all+$hsl_all->total_stock;
            }
            else{}

        }
        
        //$all=$q_all[0]->total_stock;
        $unallocatedAll=$all-$allocated;
        //$all = $unallocatedAll + $allocated;
        // dd($allocated);

        //free kapasitas
        $free   = $max - ($allocated + $unallocatedAll);
        $freeCapacity = ($free < 0) ? 0 : $free ;


        //slow moving uom yds free
        $stockSlowMoving = $wms_replication->select ("
        SELECT SUM
                    ( total_stock_inventory ) AS total_stock,
                    factory_id
                FROM
                    material_stocks 
                WHERE
                    deleted_at IS NULL
                AND created_at is not null	
                    AND is_fabric IS TRUE 
                    AND factory_id IN ('1000011')
                    AND created_at < now() - INTERVAL '2 YEAR'
                GROUP BY
                    factory_id                                   
    ");
                                    // dd($stockSlowMoving == null);
                     
        $slowMoving = 0;
        $slowMovingUom = 0;
        $Running = 0;
        $RunningUom = 0;
        $slowMovingP = 0;
        $slowMovingPM = 0;
        $RunningP = 0;
        $RunningPM = 0;
        if($stockSlowMoving != null)
        {
            $slowMoving = $stockSlowMoving[0]->total_stock;
        }
        

        $newSlowMoving = $slowMoving ;
        // $newSlowMoving = $slowMoving + $slowMovingUom;
      
        $Running = $all-$newSlowMoving;
      
        $newRunning = $Running ;
        // $newRunning = $Running + $RunningUom;
        //$usedLeft = $newRunning + $newSlowMoving;
        $usedLeft=$all;

        $newFreeCapacity = $max - ($usedLeft);
        
        return view('dashboard.dashboardFabStats', compact('wh', 'now', 'max', 'free', 'freeCapacity', 'all', 
        'newSlowMoving', 'newRunning','usedLeft', 'allocated', 'unallocatedAll','newFreeCapacity'));
    }
}
    
