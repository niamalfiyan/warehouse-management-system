<?php namespace App\Http\Controllers;

use DB;
use PDF;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\MappingStocks;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\AllocationItem;
use App\Models\MaterialPreparation;
use App\Models\AutoAllocation;
use App\Models\HistoryAutoAllocations;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialStock;
use App\Models\HistoryMaterialStocks;

use App\Http\Controllers\AccessoriesMaterialStockOnTheFlyController as SOTF;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;

class AccessoriesMaterialStockApprovalController extends Controller
{
    public function index()
    {
        return view('accessories_material_stock_approval.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        { 
            $warehouse          = $request->warehouse;
            $user_department    = auth::user()->department;

            $data = DB::table('accessories_material_stock_approval_v')
            ->where('warehouse_id','LIKE',"%$warehouse%");

            if($user_department == 'finance & accounting' || $user_department == 'ict') $data = $data->whereNull('accounting_approval_date');
            else if($user_department == 'material management') $data = $data->whereNull('mm_approval_date');

            return DataTables::of($data)
            ->editColumn('checkbox',function($data){
                return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
           })
            ->editColumn('qty',function ($data)
            {
                if ($data->table_name == 'detail_material_stocks')
                {
                    return '+'.number_format($data->qty, 4, '.', ',').'('.trim($data->uom).')';
                }
                elseif ($data->table_name == 'auto_allocations')
                {
                    return '+'.number_format($data->qty, 4, '.', ',').'('.trim($data->uom).')';   
                }
                else
                {
                    return '-'.number_format($data->qty, 4, '.', ',').'('.trim($data->uom).')';
                }
            })
            ->editColumn('created_at',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
            })
            ->addColumn('action', function($data) use($user_department)
            {
                if($user_department == 'finance & accounting' || auth::user()->hasRole(['approval-free-stock']))
                {
                    return view('_action', [
                        'model'         => $data,
                        'approve'       => route('accessoriesMaterialStockApproval.itemApprove',[$data->id,$data->table_name]),
                        'reject'        => route('accessoriesMaterialStockApproval.reject',[$data->id,$data->table_name]),
                    ]);
                }else
                {
                    if(auth::user()->hasRole('admin-ict-acc'))
                    {
                        return view('_action', [
                            'model'         => $data,
                            'reject'        => route('accessoriesMaterialStockApproval.reject',[$data->id,$data->table_name]),
                            'edit_modal'    => route('accessoriesMaterialStockApproval.edit',[$data->id,$data->table_name]),
                        ]);
                    }else
                    {
                        return view('_action', [
                            'model'         => $data,
                            'edit_modal'    => route('accessoriesMaterialStockApproval.edit',[$data->id,$data->table_name]),
                        ]);
                    }

                    
                }
               
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if($data->mm_approval_date) return  'background-color: #fffed9;';
                    else if($data->accounting_approval_date) return  'background-color: #d9ffde;';
                },
            ])     
            ->rawColumns(['style','action', 'checkbox'])
            ->make(true);
        }
    }
 
    public function edit(Request $request,$id,$table_name)
    {
        $data                   = DB::table('accessories_material_stock_approval_v')->where('id',$id)->first();
        $obj                    = new StdClass();
        $obj->table_id          = $data->id;
        $obj->table_name        = $data->table_name;
        $obj->document_no       = $data->document_no;
        $obj->supplier_name     = $data->supplier_name;
        $obj->item_code         = $data->item_code;
        $obj->category          = $data->category;
        $obj->uom               = $data->uom;
        $obj->remark            = $data->remark;
        $obj->created_at        = $data->created_at;
        $obj->created_by        = $data->created_by;
        $obj->warehouse_id      = $data->warehouse_id;
        $obj->qty               = $data->qty;
        
        if($data->table_name == 'detail_material_stocks')
        {
            $detail_material_stock  = DetailMaterialStock::find($data->id);
            if($detail_material_stock->approve_date_mm) return response()->json('This data cannot be edited because MM already approved it',422);
            if($detail_material_stock->approve_date_accounting) return response()->json('This data cannot be edited because Accounting already approved it',422);
        }else
        {
            $allocation_item  = AllocationItem::find($data->id);
            if($allocation_item->mm_approval_date) return response()->json('This data cannot be edited because MM already approved it',422);
            if($allocation_item->accounting_approval_date) return response()->json('This data cannot be edited because Accounting already approved it',422);
        }

        return response()->json($obj,200);
    }

    public function update(Request $request)
    {
        try 
        {
            DB::beginTransaction();

            $remark_update      = trim(strtoupper($request->remark_update));
            $table_id           = $request->table_id;
            $table_name         = $request->table_name;
            $locator_id         = $request->locator_id;
            $qty_adjustment     = $request->qty_adjustment;
            
            if($table_name == 'detail_material_stocks')
            {
            
                $detail_material_stock                             = DetailMaterialStock::find($table_id);

                if(!$detail_material_stock->approve_date_mm && !$detail_material_stock->approve_date_accounting)
                {
                    $curr_material_stock_id                            = $detail_material_stock->material_stock_id;
                    $curr_remark                                       = $detail_material_stock->remark;
                    $curr_qty                                          = $detail_material_stock->qty;
                    $curr_locator_id                                   = $detail_material_stock->materialStock->locator_id;
                    $curr_approval_date                                = $detail_material_stock->materialStock->approval_date;

                    if($curr_locator_id != $locator_id)
                    {
                        $material_stock = MaterialStock::find($curr_material_stock_id);
                        if(!$curr_approval_date)
                        {   
                            $material_stock->locator_id = $locator_id;
                            $material_stock->save();
                        }else
                        {
                            $is_exists = MaterialStock::whereNull('deleted_at')
                            ->where([
                                ['c_order_id',$material_stock->c_order_id],
                                ['type_po', 2],
                                ['uom',$material_stock->uom],
                                ['locator_id',$locator_id],
                                ['warehouse_id',$material_stock->warehouse_id],
                                ['type_stock',$material_stock->type_stock],
                                ['is_material_others',$material_stock->is_material_others],
                                ['is_closing_balance',false],
                                ['is_stock_on_the_fly',false],
                                ['is_running_stock',false],
                            ])
                            ->whereNotNull('approval_date');

                            if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                            else $is_exists = $is_exists->whereNull('po_detail_id');
            
                            if($material_stock->item_id) $is_exists = $is_exists->where('item_id',$material_stock->item_id);
                            else $is_exists = $is_exists->where('item_code',$material_stock->item_code);

                            if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                            else $is_exists = $is_exists->whereNull('c_bpartner_id');
            
                            if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                            else $is_exists = $is_exists->whereNull('po_buyer');
            
                            if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                            else $is_exists = $is_exists->whereNull('source');
            
                            $is_exists = $is_exists->first();

                            if($is_exists)
                            {
                                $detail_material_stock->material_stock_id = $is_exists->id;
                            }else
                            {
                                $new_material_stock = MaterialStock::FirstOrCreate([
                                    'po_detail_id'          => $material_stock->po_detail_id,
                                    'document_no'           => $material_stock->document_no,
                                    'supplier_code'         => $material_stock->supplier_code,
                                    'supplier_name'         => $material_stock->supplier_name,
                                    'c_bpartner_id'         => $material_stock->c_bpartner_id,
                                    'c_order_id'            => $material_stock->c_order_id,
                                    'locator_id'            => $locator_id,
                                    'item_id'               => $material_stock->item_id,
                                    'item_code'             => $material_stock->item_code,
                                    'uom_source'            => $material_stock->uom_source,
                                    'item_desc'             => $material_stock->item_desc,
                                    'category'              => $material_stock->category,
                                    'type_po'               => $material_stock->type_po,
                                    'warehouse_id'          => $material_stock->warehouse_id,
                                    'uom'                   => $material_stock->uom,
                                    'po_buyer'              => $material_stock->po_buyer,
                                    'qty_carton'            => $material_stock->qty_carton,
                                    'stock'                 => sprintf('%0.8f',$detail_material_stock->qty),
                                    'reserved_qty'          => 0,
                                    'available_qty'         => sprintf('%0.8f',$detail_material_stock->qty),
                                    'is_active'             => $material_stock->is_active,
                                    'is_material_others'    => $material_stock->is_material_others,
                                    'approval_date'         => null,
                                    'approval_user_id'      => null,
                                    'source'                => $material_stock->source,
                                    'user_id'               => $material_stock->user_id,
                                    'mapping_stock_id'      => $material_stock->mapping_stock_id,
                                    'type_stock_erp_code'   => $material_stock->type_stock_erp_code,
                                    'type_stock'            => $material_stock->type_stock,
                                    'created_at'            => $material_stock->created_at,
                                    'updated_at'            => $material_stock->updated_at,
                                ]);

                                $detail_material_stock->material_stock_id = $new_material_stock->id;
                            }
                        }
                    }

                    if($curr_qty != $qty_adjustment)
                    {
                        $detail_material_stock->remark                     = $curr_remark.' [UPDATE '.carbon::now().' , QTY DI EDIT OLEH '.auth::user()->name.' MENJADI '.$qty_adjustment.', QTY SEMULA ADALAH '.$curr_qty.'. DIGANTI DENGAN ALASAN '.$remark_update.']';
                        $detail_material_stock->qty                        = sprintf('%0.8f',$qty_adjustment);
                    }
                    
                    
                    $detail_material_stock->updated_at                 = carbon::now();
                    $detail_material_stock->save();
                }
                
                
            }else
            {
                /*$allocation_item      = AllocationItem::find($id);
                $allocation_item->accounting_approval_date    = carbon::now();
                $allocation_item->accounting_user_id         = auth::user()->id;
                $allocation_item->save();*/
            }
           
            DB::commit();
            
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
        return response()->json(200);
    }

    public function approveAll(Request $request)
    {
        $warehouse_id          = $request->warehouse_id;

        $list_approvals = DB::table('accessories_material_stock_approval_v')
        ->where('warehouse_id',$warehouse_id)
        ->get();

        foreach($list_approvals as $key => $list_approval)
        { 
            try 
            {
                DB::beginTransaction();
                
                $id                     = $list_approval->id;
                $table_name             = $list_approval->table_name;
                $this->itemApprove($id, $table_name);

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

        return response()->json(200);
    }
    
    static function itemApprove($id,$table_name)
    {
        $user_department                = auth::user()->department;
        $data                           = DB::table('accessories_material_stock_approval_v')->where('id', $id) ->first();
        $note_adjusment                 = $data->remark;
        $movement_date                  = carbon::now();
        $flag                           = false;

        try 
        {
            DB::beginTransaction();

            if($table_name == 'auto_allocations')
            {
                $auto_allocation_id = $id;
                $auto_allocation                        = AutoAllocation::find($auto_allocation_id);
                $auto_allocation_document_no            = strtoupper($auto_allocation->document_no);
                $auto_allocation_c_bpartner_id          = $auto_allocation->c_bpartner_id;
                $auto_allocation_warehouse_id           = $auto_allocation->warehouse_id;
                $auto_allocation_type_stock_erp_code    = $auto_allocation->type_stock_erp_code;
                $auto_allocation_type_stock             = $auto_allocation->type_stock;
                $auto_allocation_uom                    = $auto_allocation->uom;
                $auto_allocation_lc_date                = $auto_allocation->lc_date;
                $auto_allocation_item_code_source       = $auto_allocation->item_code_source;
                $auto_allocation_item_code_book         = $auto_allocation->item_code_book;
                $auto_allocation_po_buyer               = $auto_allocation->po_buyer;
                $auto_allocation_qty_allocated          = $auto_allocation->qty_allocated;
                $auto_allocation_is_integrate           = ($auto_allocation->is_upload_manual) ? true: false; 

                $_supplier                              = PoSupplier::where('document_no',$auto_allocation_document_no)->first();
                $auto_allocation_c_order_id             = ($_supplier)?$_supplier->c_order_id : 'FREE STOCK';

                $item_source                            = Item::where(db::raw('item_code'),$auto_allocation_item_code_source)->first();
                $item_book                              = Item::where(db::raw('item_code'),$auto_allocation_item_code_book)->first();
                
                $auto_allocation_item_source_id         = ($item_source) ? $item_source->item_id : null;
                $auto_allocation_item_book_id           = ($item_book) ? $item_book->item_id : null;

                $auto_allocation_supplier_name          = ($auto_allocation_c_bpartner_id == 'FREE STOCK ' && $auto_allocation_document_no == 'FREE STOCK')? 'FREE STOCK': $auto_allocation->supplier_name;
                $auto_allocation_month_lc_date          = ($auto_allocation->lc_date)? $auto_allocation->lc_date->format('m') : '05';
                $auto_allocation_qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $auto_allocation_operator               = -1*$auto_allocation_qty_allocation;
                $auto_allocation_note                   = 'TERJADI PENGHAPUSAN ALOKASI';
                $auto_allocation_note_code              = '2';
                $auto_allocation_is_fabric              = $auto_allocation->is_fabric;
                $auto_allocation_deleted_at             = $auto_allocation->deleted_at;
                $remark_update                          = $auto_allocation->remark_update;

                $allocation_items   = MaterialPreparation::where('auto_allocation_id',$auto_allocation_id)
                                      ->whereNull('deleted_at')->get();
                        
                if(count($allocation_items) > 0)
                {
                    foreach ($allocation_items as $key_2 => $allocation_item) 
                    {
                            $material_stock_id                  = $allocation_item->material_stock_id;
                            $po_buyer                           = $allocation_item->po_buyer;
                            $style                              = $allocation_item->style;
                            $article_no                         = $allocation_item->article_no;
                            $lc_date                            = $allocation_item->lc_date;
                            $material_preparation_id            = $allocation_item->id;
                            $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_conversion);

                            Temporary::Create([
                                'barcode'   => $po_buyer,
                                'status'    => 'mrp',
                                'user_id'   => Auth::user()->id
                            ]);
                            
                            $material_stock                     = MaterialStock::find($material_stock_id);
                            $stock                              = sprintf('%0.8f',$material_stock->stock);
                            $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
                            $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                            $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);
                            $new_reserverd_qty                  = $reserved_qty - $qty_booking;
                            $new_available_qty                  = $stock - $new_reserverd_qty;
                            $curr_type_stock_erp_code           = $material_stock->type_stock_erp_code;
                            $curr_type_stock                    = $material_stock->type_stock;
                            $is_running_stock                   = $allocation_item->materialStock->is_running_stock;
                            $is_integrate                       = ( $is_running_stock ? true : false);

                            $material_stock->reserved_qty       = sprintf('%0.8f',$new_reserverd_qty);
                            $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty);

                            if($new_available_qty > 0)
                            {
                                $material_stock->is_allocated       = false;
                                $material_stock->is_active          = true;
                                $material_stock->is_closing_balance = false;
                            }
                            
                            $remark = 'CANCEL ALLOCATION, UNTUK PO BUYER '.$po_buyer.', STYLE '.$style.', ARTICLE '.$article_no.', DAN QTY '.$qty_booking;
                            AccessoriesMaterialStockApprovalController::approved($material_stock_id
                            ,$_new_available_qty
                            ,$old_available_qty
                            ,(-1*$qty_booking)
                            ,$po_buyer
                            ,$style
                            ,$article_no
                            ,$lc_date
                            ,$remark
                            ,auth::user()->name
                            ,auth::user()->id
                            ,$curr_type_stock_erp_code
                            ,$curr_type_stock
                            ,$is_integrate
                            ,true );

                            $material_stock->save();
                            $allocation_item->delete();
                    }

                    HistoryAutoAllocations::Create([
                        'auto_allocation_id'        => $auto_allocation_id,
                        'document_no_old'           => $auto_allocation_document_no,
                        'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                        'item_code_old'             => $auto_allocation_item_code_book,
                        'uom_old'                   => $auto_allocation_uom,
                        'item_id_old'               => $auto_allocation_item_book_id,
                        'item_source_id_old'        => $auto_allocation_item_source_id,
                        'item_code_source_old'      => $auto_allocation_item_code_source,
                        'po_buyer_old'              => $auto_allocation_po_buyer,
                        'supplier_name_old'         => $auto_allocation_supplier_name,
                        'qty_allocated_old'         => $auto_allocation_qty_allocation,
                        'warehouse_id_old'          => $auto_allocation_warehouse_id,
                        'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                        'type_stock_old'            => $auto_allocation_type_stock,
            
                        'document_no_new'           => $auto_allocation_document_no,
                        'item_code_new'             => $auto_allocation_item_code_book,
                        'item_id_new'               => $auto_allocation_item_book_id,
                        'item_source_id_new'        => $auto_allocation_item_source_id,
                        'item_code_source_new'      => $auto_allocation_item_code_source,
                        'uom_new'                   => $auto_allocation_uom,
                        'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                        'supplier_name_new'         => $auto_allocation_supplier_name,
                        'qty_allocated_new'         => $auto_allocation_qty_allocation,
                        'warehouse_id_new'          => $auto_allocation_warehouse_id,
                        'po_buyer_new'              => $auto_allocation_po_buyer,
                        'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                        'type_stock_new'            => $auto_allocation_type_stock,
                        'note_code'                 => $auto_allocation_note_code,
                        'note'                      => $auto_allocation_note,
                        'operator'                  => $auto_allocation_operator,
                        'remark'                    => 'BULK DELETE',
                        'is_integrate'              => $auto_allocation_is_integrate,
                        'lc_date'                   => $auto_allocation_lc_date,
                        'c_order_id'                => $auto_allocation_c_order_id,
                        'user_id'                   => auth::user()->id
                    ]);

                    $auto_allocation->update_user_id                    = auth::user()->id;
                    $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                    $auto_allocation->qty_allocated                     = 0;
                    $auto_allocation->is_already_generate_form_booking  = false;
                    $auto_allocation->generate_form_booking             = null;
                    $auto_allocation->remark                            = 'DELETE FROM APPROVAL BY '.auth::user()->id;
                    $auto_allocation->remark_update                     = $remark_update;
                    $auto_allocation->deleted_at                        = carbon::now();
                    $auto_allocation->save();

                }

            }
            elseif($table_name == 'detail_material_stocks')
            {
                $detail_material_stock      = DetailMaterialStock::find($id);
                
                //disimpen dulu kalau kalau minta perubahan lagi :)
                // if($user_department == 'finance & accounting' || $user_department == 'ict')
                // {
                    //if(!$detail_material_stock->approve_date_mm) return response()->json('MM must approve first then you can approve it.',422); 

                    if(!$detail_material_stock->approve_date_accounting)
                    {
                        $detail_material_stock->approve_date_accounting     = $movement_date;
                        $detail_material_stock->accounting_user_id          = auth::user()->id;
                        $detail_material_stock->save();

                        AccessoriesMaterialStockApprovalController::checkApprovalStatus($id,$table_name,$note_adjusment);

                        $material_stock_id = $detail_material_stock->material_stock_id;
                        $flag = true;
                    }
                    
                // }else if($user_department == 'material management')
                // {
                    // if(!$detail_material_stock->approve_date_mm)
                    // {   
                    //     $detail_material_stock->approve_date_mm             = $movement_date;
                    //     $detail_material_stock->mm_user_id                  = auth::user()->id;
                    //     $detail_material_stock->save();
                    //     $this->checkApprovalStatus($id,$table_name,$note_adjusment);
                    // }
                    
                //}
            }else
            {
                $allocation_item      = AllocationItem::find($id);

                // if($user_department == 'finance & accounting' || $user_department == 'ict')
                // {
                    //if(!$allocation_item->mm_approval_date) return response()->json('MM must approve first then you can approve it.',422); 
                    
                    if(!$allocation_item->accounting_approval_date)
                    {
                        $allocation_item->accounting_approval_date      = $movement_date;
                        $allocation_item->accounting_user_id            = auth::user()->id;
                        $allocation_item->save();
                        AccessoriesMaterialStockApprovalController::checkApprovalStatus($id,$table_name,$note_adjusment);

                        $material_stock_id = $allocation_item->material_stock_id;
                        $flag = true;
                    }
                    
                // }else if($user_department == 'material management')
                // {
                //     if(!$allocation_item->mm_approval_date)
                //     {
                //         $allocation_item->mm_approval_date              = $movement_date;
                //         $allocation_item->mm_user_id                    = auth::user()->id;
                //         $allocation_item->save();

                //         $this->checkApprovalStatus($id,$table_name,$note_adjusment);
                //     }
                    
                // }
            }
        
            DB::commit();
            
            if($flag)
            {
                SOTF::updateStockArrival([$material_stock_id]);

                $material_stock     = MaterialStock::where([
                    ['id',$material_stock_id],
                    ['is_closing_balance',false],
                    ['is_allocated',false],
                    ['category','!=','CT'],
                ])
                ->whereNull('last_status')
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->first();
    
                if($material_stock)
                {
                    $material_stock->last_status    = 'prepared';
                    $material_stock->save();
    
                    $system                     = User::where([
                        ['name','system'],
                        ['warehouse',$material_stock->warehouse_id]
                    ])
                    ->first();
    
                    ReportStock::doAllocation($material_stock,$system->id,$movement_date);
    
                    $material_stock->last_status                = null;
                    $material_stock->save();
                }
            }
            


            return response()->json(200);
        }catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}

        return response()->json(200);
    }

    public function reject($id,$table_name)
    {
        $user_department               = auth::user()->department;
        
        if($table_name == 'detail_material_stocks')
        {
            DetailMaterialStock::where('id',$id)
            ->update([
                'reject_date'    => carbon::now(),
                'reject_user_id' => Auth::user()->id
            ]);
        }
        elseif($table_name == 'auto_allocations')
        {
            AutoAllocation::where('id',$id)
            ->update([
                'is_request_delete'    => false,
            ]);
        }
        else
        {
            $allocation_item      = AllocationItem::find($id)->delete();
        }
        
        return response()->json(200); 
    }


    public function locatorPicklist(Request $request)
    {
        $q              = strtoupper($request->q);
        $warehouse_id   = $request->warehouse_id;

        $lists = Locator::whereHas('area',function($query) use ($warehouse_id)
        {
            $query->where('name','FREE STOCK')
            ->where('warehouse',$warehouse_id);
        })
        ->where([
            ['is_active',true],
            ['rack','<>','PRINT BARCODE']
        ])
        ->where(function($query) use ($q){
            $query->where('code','LIKE',"%$q%")
            ->orWhere('rack','LIKE',"%$q%");
        })
        ->orderby('rack','asc')
        ->orderby('y_row','asc')
        ->orderby('z_column','asc')
       ->paginate('10');

       return view('accessories_material_stock_approval._locator_list',compact('lists'));
    }

    static function checkApprovalStatus($id,$table_name,$note_adjustment)
    {
        try 
        {
            DB::beginTransaction();
            $created_at                     = carbon::now();

            if($table_name == 'detail_material_stocks')
            {
                $summary_stock_fabrics          = array();
                $update_counter_old_locators    = array();
                $update_counter_new_locators    = array();
                $update_summary_stock           = array();
                $detail_material_stock          = DetailMaterialStock::find($id);
                
                if($detail_material_stock->approve_date_accounting)
                {
                    if(!$detail_material_stock->approve_date_mm)
                    {
                        $detail_material_stock->approve_date_mm     = $created_at;
                        $detail_material_stock->mm_user_id          = auth::user()->id;
                        $detail_material_stock->save();
                    }

                    $material_stock_id                  = $detail_material_stock->material_stock_id;
                    $material_stock                     = MaterialStock::find($material_stock_id);
                    $warehouse_id                       = $material_stock->warehouse_id;
                    $locator_id                         = $material_stock->locator_id;
                    $po_detail_id                       = $material_stock->po_detail_id;
                    $c_order_id                         = $material_stock->c_order_id;
                    $item_code                          = $material_stock->item_code;
                    $item_id                            = $material_stock->item_id;
                    $c_bpartner_id                      = $material_stock->c_bpartner_id;
                    $supplier_name                      = $material_stock->supplier_name;
                    $is_stock_already_created           = $material_stock->is_stock_already_created;
                    $document_no                        = $material_stock->document_no;
                    $nomor_roll                         = '-';
                    $uom                                = $material_stock->uom;

                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();

                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    }

                    $curr_stock                         = sprintf('%0.8f', $material_stock->stock);
                    $curr_reserved_qty                  = sprintf('%0.8f', $material_stock->reserved_qty);
                    $total_detail_stock                 = sprintf('%0.8f',DetailMaterialStock::where('material_stock_id',$material_stock_id)->whereNotNull('approve_date_mm')->whereNotNull('approve_date_accounting')->sum('qty'));
                    $curr_available_qty                 = sprintf('%0.8f',$total_detail_stock - $curr_reserved_qty);
                    
                    $material_stock->stock              = $total_detail_stock;
                    $material_stock->available_qty      = $curr_available_qty;

                    if($curr_available_qty <= 0)
                    {
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                    }else
                    {
                        $material_stock->is_allocated   = false;
                        $material_stock->is_active      = true;
                    }

                    $material_stock->save();
    
                    if($material_stock->approval_date)
                    {
                        $operator   = sprintf('%0.8f',$curr_available_qty - $curr_stock);

                        AccessoriesMaterialStockApprovalController::approved($material_stock_id
                        ,$curr_available_qty
                        ,$curr_stock
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'APPROVAL STOCK KARENA '.$detail_material_stock->remark
                        ,$detail_material_stock->user->name
                        ,$detail_material_stock->user_id
                        ,$material_stock->type_stock_erp_code
                        ,$material_stock->type_stock
                        ,false
                        ,false);
                    }else
                    {
                        $operator   = sprintf('%0.8f',$curr_available_qty - '0');

                        AccessoriesMaterialStockApprovalController::approved($material_stock_id
                        ,$curr_available_qty
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'APPROVAL STOCK KARENA '.$detail_material_stock->remark
                        ,$detail_material_stock->user->name
                        ,$detail_material_stock->user_id
                        ,$material_stock->type_stock_erp_code
                        ,$material_stock->type_stock
                        ,false
                        ,$is_stock_already_created);
                    }
    
                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();


                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();
                    
                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $created_at;
                        $is_movement_exists->save();

                        $material_movement_id = $is_movement_exists->id;
                    }

                    $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$material_movement_id],
                        ['warehouse_id',$warehouse_id],
                        ['item_id',$item_id],
                        ['material_stock_id',$material_stock_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['date_movement',$created_at],
                        ['qty_movement',$operator],
                    ])
                    ->exists();

                    if(!$is_line_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $material_movement_id,
                            'material_stock_id'             => $material_stock_id,
                            'item_code'                     => $item_code,
                            'item_id'                       => $item_id,
                            'c_order_id'                    => $c_order_id,
                            'c_orderline_id'                => $c_orderline_id,
                            'c_bpartner_id'                 => $c_bpartner_id,
                            'supplier_name'                 => $supplier_name,
                            'type_po'                       => 2,
                            'is_integrate'                  => false,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $operator,
                            'date_movement'                 => $created_at,
                            'created_at'                    => $created_at,
                            'updated_at'                    => $created_at,
                            'date_receive_on_destination'   => $created_at,
                            'nomor_roll'                    => $nomor_roll,
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => $document_no,
                            'note'                          => 'APPROVAL STOCK KARENA '.$note_adjustment,
                            'is_active'                     => true,
                            'user_id'                       => auth::user()->id,
                        ]);
                    }
                    
                    $count_item_on_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->whereNotNull('approval_date')
                    ->count();
    
                    $locator = Locator::find($locator_id);
                    if($locator)
                    {
                        $locator->counter_in = $count_item_on_locator;
                        $locator->save();
                    }
                    
                    
                }
            }else
            {
                $allocation_item                = AllocationItem::find($id);
                if($allocation_item->accounting_approval_date)
                {
                    if(!$allocation_item->mm_approval_date)
                    {
                        $allocation_item->mm_approval_date            = $created_at;
                        $allocation_item->mm_user_id                  = auth::user()->id;
                        $allocation_item->save();
                    }

                    $material_stock_id                  = $allocation_item->material_stock_id;
                    $material_stock                     = MaterialStock::find($material_stock_id);
                    $locator_id                         = $material_stock->locator_id;
                    $curr_stock                         = sprintf('%0.8f', $material_stock->stock);
                    $cur_available_qty                  = sprintf('%0.8f', $material_stock->available_qty);
                    $curr_reserved_qty                  = sprintf('%0.8f', $material_stock->reserved_qty);
                    $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_booking);
                    $new_reserved_qty                   = sprintf('%0.8f', $curr_reserved_qty + $qty_booking);
                    $new_available_qty                  = sprintf('%0.8f',$curr_stock - $new_reserved_qty);
                    $po_detail_id                       = $material_stock->po_detail_id;
                    $item_code                          = $material_stock->item_code;
                    $item_id                            = $material_stock->item_id;
                    $c_bpartner_id                      = $material_stock->c_bpartner_id;
                    $supplier_name                      = $material_stock->supplier_name;
                    $document_no                        = $material_stock->document_no;
                    $warehouse_id                       = $material_stock->warehouse_id;
                    $nomor_roll                         = '-';
                    $uom                                = $material_stock->uom;

                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $c_order_id             = 'FREE STOCK';
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();

                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : 'FREE STOCK');
                    }

                    $material_stock->reserved_qty       = $new_reserved_qty;
                    $material_stock->available_qty      = $new_available_qty;

                    if($new_available_qty <= 0)
                    {
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                    }else
                    {
                        $material_stock->is_allocated   = false;
                        $material_stock->is_active      = true;
                    }
                    
                    $material_stock->save();
                    
                    $operator   = sprintf('%0.8f',$new_available_qty - $cur_available_qty);

                    AccessoriesMaterialStockApprovalController::approved($material_stock_id
                    ,$new_available_qty
                    ,$cur_available_qty
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$allocation_item->remark
                    ,$allocation_item->user->name
                    ,$allocation_item->user_id
                    ,$material_stock->type_stock_erp_code
                    ,$material_stock->type_stock
                    ,false
                    ,false);

                    $inventory_erp = Locator::with('area')
                    ->where([
                        ['is_active',true],
                        ['rack','ERP INVENTORY'],
                    ])
                    ->whereHas('area',function ($query) use($warehouse_id)
                    {
                        $query->where('is_active',true);
                        $query->where('warehouse',$warehouse_id);
                        $query->where('name','ERP INVENTORY');
                    })
                    ->first();


                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();
                    
                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $inventory_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'integration-to-inventory-erp',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                        ]);

                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $created_at;
                        $is_movement_exists->save();

                        $material_movement_id = $is_movement_exists->id;
                    }

                    $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                    ->where([
                        ['material_movement_id',$material_movement_id],
                        ['warehouse_id',$warehouse_id],
                        ['item_id',$item_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['material_stock_id',$material_stock_id],
                        ['qty_movement',$operator],
                        ['date_movement',$created_at],
                    ])
                    ->exists();

                    if(!$is_line_exists)
                    {
                        MaterialMovementLine::Create([
                            'material_movement_id'          => $material_movement_id,
                            'material_stock_id'             => $material_stock_id,
                            'item_code'                     => $item_code,
                            'item_id'                       => $item_id,
                            'c_order_id'                    => $c_order_id,
                            'c_orderline_id'                => $c_orderline_id,
                            'c_bpartner_id'                 => $c_bpartner_id,
                            'supplier_name'                 => $supplier_name,
                            'type_po'                       => 1,
                            'is_integrate'                  => false,
                            'uom_movement'                  => $uom,
                            'qty_movement'                  => $operator,
                            'date_movement'                 => $created_at,
                            'created_at'                    => $created_at,
                            'updated_at'                    => $created_at,
                            'date_receive_on_destination'   => $created_at,
                            'nomor_roll'                    => $nomor_roll,
                            'warehouse_id'                  => $warehouse_id,
                            'document_no'                   => $document_no,
                            'note'                          => 'APPROVAL STOCK KARENA '.$note_adjustment,
                            'is_active'                     => true,
                            'user_id'                       => auth::user()->id,
                        ]);
                    }
                    
                    $count_item_on_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->whereNotNull('approval_date')
                    ->count();
    
                    $locator = Locator::find($locator_id);
                    if($locator)
                    {
                        $locator->counter_in = $count_item_on_locator;
                        $locator->save();
                    }

                    $count_item_on_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->whereNotNull('approval_date')
                    ->count();
    
                    $locator = Locator::find($locator_id);
                    if($locator)
                    {
                        $locator->counter_in = $count_item_on_locator;
                        $locator->save();
                    }
                    
                }
            }
            

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function approved($id
        ,$new_stock                 = null
        ,$old_stock                 = null
        ,$qty_allocation            = null
        ,$po_buyer_allocation       = null
        ,$style_allocation          = null
        ,$article_allocation        = null
        ,$lc_date_allocation        = null
        ,$allocation_source         = null
        ,$user_name                 = null
        ,$user_id                   = null
        ,$new_type_stock_erp_code   = null
        ,$new_type_stock            = null
        ,$is_integrate              = false
        ,$is_allocation             = false
    )
    {
        $date_now                   = carbon::now();
        $material_stock             = MaterialStock::find($id);
        $_document_no               = strtoupper($material_stock->document_no);
        $is_stock_already_created   = $material_stock->is_stock_already_created;
        $c_order_id                 = ($material_stock->c_order_id) ? $material_stock->c_order_id : 'FREE STOCK';
        $c_bpartner_id              = $material_stock->c_bpartner_id;
        $po_buyer                   = $material_stock->po_buyer;
        $item_id                    = $material_stock->item_id;
        $item_code                  = $material_stock->item_code;
        $item_desc                  = $material_stock->item_desc;
        $source                     = $material_stock->source;
        $warehouse_id               = $material_stock->warehouse_id;
        $is_material_others         = $material_stock->is_material_others;
        $uom                        = $material_stock->uom;
        $is_running_stock           = $material_stock->is_running_stock;
        $document_no                = ($_document_no == 'FREE STOCK-CELUP' ? 'FREE STOCK': $_document_no);
        $is_stock_allocation        = ($document_no == 'FREE STOCK')? false : true;
        $user_id                    = ($user_id)? $user_id : $material_stock->user_id;
        $user_name                  = ($user_name)? $user_name : $material_stock->user->name;
        
        if($is_running_stock) $_is_integrate = true;
        else $_is_integrate = $is_integrate;

        $system = User::where([
            ['name','system'],
            ['warehouse',$warehouse_id]
        ])
        ->first();

        if($new_type_stock_erp_code != null)
        {
            $_type_stock_erp_code    = $new_type_stock_erp_code;
            $type_stock             = $new_type_stock;
        }else
        {
            $_type_stock_erp_code    = $material_stock->type_stock_erp_code;
            $type_stock             = $material_stock->type_stock;
        }
        
        if($_type_stock_erp_code == 'SLT') $type_stock_erp_code = '1';
        else if($_type_stock_erp_code == 'REGULER') $type_stock_erp_code = '2';
        else if ($_type_stock_erp_code == 'PR/SR') $type_stock_erp_code = '3';
        else if ($_type_stock_erp_code == 'MTFC') $type_stock_erp_code = '4';
        else if ($_type_stock_erp_code == 'NB') $type_stock_erp_code = '5';
        else if ($_type_stock_erp_code == 'ALLOWANCE') $type_stock_erp_code = '6';
        else $type_stock_erp_code = $_type_stock_erp_code;


        if($po_buyer)
        {
            $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
            $lc_date                = ($_po_buyer) ? $_po_buyer->lc_date : null;
        }else{
            $lc_date                = null;
        }

        $supplier_name              = $material_stock->supplier_name;
        $stock                      = sprintf('%0.8f',$material_stock->stock);
        $_new_stock                 = (strval($new_stock) != null) ? $new_stock : $stock;
        $_old_stock                 = (strval($old_stock) != null) ? $old_stock : '0';
        $operator                   = sprintf('%0.8f',$_new_stock - $_old_stock);
        $warehouse_id               = $material_stock->warehouse_id;

        if(!$material_stock->approval_date)
        {
            $material_stock->approval_date      = $date_now;
            $material_stock->approval_user_id   = $system->id;
        }
        
        if($allocation_source)
        {
            $_source                = $allocation_source.', DILAKUKAN OLEH '.$user_name;
        }else
        {
            $_source                = (!$is_material_others) ? 'DITERIMA DARI SUPPLIER OLEH '.strtoupper(auth::user()->name) : ($source)? $source.', DIUPLOAD OLEH '.strtoupper(auth::user()->name) : 'DIUPLOAD OLEH '.strtoupper(auth::user()->name);
        }

        $is_header_exists = MaterialStock::where([
            ['document_no',$document_no],
            ['c_bpartner_id',$c_bpartner_id],
            ['item_code',$item_code],
            ['warehouse_id',$warehouse_id],
            ['type_po',2],
            ['is_closing_balance',false],
            ['type_stock_erp_code',$type_stock_erp_code]
        ])
        ->whereNotNull('approval_date')
        ->exists();

        $note                       = ($is_header_exists) ? 'MUTASI' : 'NEW';  

        HistoryMaterialStocks::Create([
            'type_stock_erp_code'           => $type_stock_erp_code,
            'type_stock'                    => $type_stock,
            'material_stock_id'             => $id,
            'lc_date'                       => $lc_date,
            'po_buyer'                      => $po_buyer,
            'item_id'                       => $item_id,
            'item_code'                     => $item_code,
            'item_desc'                     => $item_desc,
            'uom'                           => $uom,
            'c_order_id'                    => $c_order_id,
            'document_no'                   => $document_no,
            'c_bpartner_id'                 => $c_bpartner_id,
            'supplier_name'                 => $supplier_name,
            'warehouse_id'                  => $warehouse_id,
            'stock_old'                     => sprintf('%0.8f',$_old_stock),
            'stock_new'                     => sprintf('%0.8f',$_new_stock),
            'source'                        => $_source,
            'user_id'                       => $user_id,
            'operator'                      => $operator,
            'created_at'                    => $date_now,
            'note'                          => $note,
            'is_integrate'                  => $_is_integrate,
            'is_stock_allocation'           => $is_stock_allocation,
            'qty_allocation'                => sprintf('%0.8f',$qty_allocation),
            'po_buyer_allocation'           => $po_buyer_allocation,
            'style_po_buyer_allocation'     => $style_allocation,
            'article_po_buyer_allocation'   => $article_allocation,
            'lc_date_po_buyer_allocation'   => $lc_date_allocation,
        ]);
        
        $material_stock->save();
    }

    public function exportAll()
    {
        $filename   = 'accessories_material_stock_approval.csv';
        $file       = Config::get('storage.report') . '/' . $filename;
        
        if(!file_exists($file)) return 'File not found.';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function approveSelected(Request $request)
    {
        $list_detail_material_stocks    = explode(',',$request->list_detail_material_stock);
        $movement_date                  = carbon::now();

        $list_approvals = DB::table('accessories_material_stock_approval_v')
        ->whereIN('id', $list_detail_material_stocks)
        ->get();

        foreach($list_approvals as $key => $list_approval)
        {
            try 
            {
                DB::beginTransaction();
                
                $id                     = $list_approval->id;
                $table_name             = $list_approval->table_name;
                $note_adjusment         = $list_approval->remark;
                $user_department        = auth::user()->department;
        
                $this->itemApprove($id,$table_name);
                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }

        return response()->json(200);

    }
}
