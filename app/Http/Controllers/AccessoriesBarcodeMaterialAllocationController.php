<?php namespace App\Http\Controllers;

use Excel;
use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Barcode;
use App\Models\Locator;
use App\Models\Temporary;
use App\Models\UomConversion;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialBacklog;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\MaterialCheckController;
//use App\Http\Controllers\MaterialSwitchAccessoriesController as MaterialSwitch;

class AccessoriesBarcodeMaterialAllocationController extends Controller
{
    public function index()
    {
        // return view('errors.migration');
        return view('accessories_barcode_material_allocation.index');
    }

    public function poBuyerPicklist(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $q                  = strtoupper($request->q);
        $lists              = MaterialPreparation::select('po_buyer','style')
        ->where(function($query) use ($q){
            $query->where('po_buyer','like',"%$q%");
        })
        ->where([
            ['warehouse',$_warehouse_id],
        ])
        ->whereIn('auto_allocation_id',function($query)
        {
            $query->select('id')
            ->from('auto_allocations')
            ->where('is_allocation_purchase',false);
        })
        ->whereNotNull('auto_allocation_id')
        ->groupby('po_buyer','style')
        ->paginate(10);

        return view('modal_lov._print_allocation_buyer_list',compact('lists'));
    }

    public function itemPickList(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $po_buyer           = $request->po_buyer;
        $style              = $request->style;
        $q                  = strtoupper($request->q);

        $lists              = MaterialPreparation::select('item_code','item_desc')
        ->where(function($query) use ($q){
            $query->where('item_code','like',"%$q%")
            ->orWhere('item_desc','like',"%$q%");
        })
        ->where([
            ['warehouse',$_warehouse_id],
            ['po_buyer',$po_buyer],
            ['style',$style],
        ])
        ->whereIn('auto_allocation_id',function($query)
        {
            $query->select('id')
            ->from('auto_allocations')
            ->where('is_allocation_purchase',false);
        })
        ->whereNotNull('auto_allocation_id')
        ->groupby('item_code','item_desc')
        ->paginate(10);
        
        
        return view('modal_lov._print_allocation_item_list',compact('lists'));
    }

    public function create(Request $request)
    {
        $po_buyer       = trim(explode(':',$request->po_buyer)[0]);
        $item_code      = trim($request->item_code);
        $style          = trim($request->style);
        $warehouse_id   = $request->warehouse_id;
        $array          = array();

        if(!$po_buyer && !$item_code) return response()->json('error',400);

        $allocations = MaterialPreparation::where([
            ['warehouse',$warehouse_id],
            ['po_buyer',$po_buyer],
            ['style',$style],
            ['item_code',$item_code],
            ['is_reroute',false],
        ])
        ->whereIn('auto_allocation_id',function($query)
        {
            $query->select('id')
            ->from('auto_allocations')
            ->where('is_allocation_purchase',false);
        })
        ->orderby('created_at','desc')
        ->get();


        foreach ($allocations as $key => $allocation) 
        {
            $user = auth::user();
            if($user->hasRole(['admin-ict-acc','supervisor','approval-free-stock'])) $show_selected = true;
            else$show_selected = false;

            if($allocation->first_print_date)
            {
                $note = 'ALREADY PRINT ON '.$allocation->first_print_date->format('d/m/y H:m:s');
            }else
            {
                $note = 'READY TO PRINT';
            }
            
            if($allocation->is_reduce) $note .= ', PO INI REDUCE SILAHKAN DI SCAN ADJUSTMENT';

            $auto_allocation            = AutoAllocation::find($allocation->auto_allocation_id);

            $old_po_buyer               = ($auto_allocation ? $auto_allocation->old_po_buyer : null);
            
            if($old_po_buyer!= $po_buyer)  $show_po_buyer   = $po_buyer.' (po buyer ini reroute dari po '.$old_po_buyer.')'; 
            else    $show_po_buyer   = $po_buyer; 

            $obj                = new stdClass();
            $obj->show_selected = $show_selected;
            $obj->selected      = (!$allocation->first_print_date ? true : false);
            $obj->id            = $allocation->id;
            $obj->document_no   = $allocation->document_no;
            $obj->po_buyer      = $allocation->po_buyer;
            $obj->show_po_buyer = $show_po_buyer;
            $obj->item_code     = $allocation->item_code;
            $obj->style         = $allocation->style;
            $obj->article       = $allocation->article_no;
            $obj->job_order     = $allocation->job_order;
            $obj->uom_conversion= $allocation->uom_conversion;
            $obj->qty_conversion= $allocation->qty_conversion;
            $obj->is_additional = $allocation->is_additional;
            $obj->note          = $note;
           

            $array [] = $obj;
        }

        return response()->json($array,200);
    }

    public function store(Request $request)
    {
        $return_print           = array();
        $__material_arrival_id  = array();
        $flag_return_print      = 0;
        $movement_date          = carbon::now()->toDateTimeString();

        $validator = Validator::make($request->all(), [
            'material_allocation_preparations' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $warehouse_id   = $request->warehouse_id;
            $items          = json_decode($request->material_allocation_preparations);
            $free_stock     = array();
            $concatenate    = '';   

            $freestock_location = Locator::with('area')
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','FREE STOCK');
            })
            ->where('rack','PRINT BARCODE')
            ->first();

            $receiving_location = Locator::with('area')
            ->whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','RECEIVING');
            })
            ->first();

            try 
            {
                DB::beginTransaction();

                //return response()->json($items);
                foreach ($items as $key => $value) 
                {
                    $selected = $value->selected;
                    if($selected == true)
                    {
                        $id                     = $value->id;
                        $material_preparation   = MaterialPreparation::find($id);

                        if($material_preparation->first_print_date)
                        {
                            $reprint_counter                        = $material_preparation->reprint_counter;
                            $material_preparation->reprint_user_id  = auth::user()->id;
                            $material_preparation->reprint_date     = carbon::now();
                            $material_preparation->reprint_counter  = $reprint_counter+1;
                        }else
                        {
                            if($material_preparation->barcode == 'BELUM DI PRINT')
                            {
                                $get_barcode                            = $this->randomCode();
                                $barcode                                = $get_barcode->barcode;
                                $referral_code                          = $get_barcode->referral_code;
                                $sequence                               = $get_barcode->sequence;

                                $auto_allocation_id                     = $material_preparation->auto_allocation_id;
                                $auto_allocation                        = AutoAllocation::find($auto_allocation_id);
                                $is_reroute                             = $auto_allocation->is_reroute;
                                $old_po_buyer                           = $auto_allocation->old_po_buyer;

                                if($is_reroute)
                                {
                                    $reroute_destination = Locator::with('area') 
                                    ->whereHas('area',function ($query) use ($material_preparation)
                                    {
                                        $query->where([
                                            ['warehouse',$material_preparation->warehouse],
                                            ['name','REROUTE'],
                                            ['is_destination',true]
                                        ]);
                        
                                    })
                                    ->first();

                                    $supplier_location = Locator::with('area')
                                    ->whereHas('area',function ($query) use ($material_preparation)
                                    {
                                        $query->where('is_destination',false);
                                        $query->where('is_active',true);
                                        $query->where('warehouse',$material_preparation->warehouse);
                                        $query->where('name','SUPPLIER');
                                    })
                                    ->first();

                                    $system                     = User::where([
                                        ['name','system'],
                                        ['warehouse',$warehouse_id]
                                    ])
                                    ->first();

                                    $is_old_exists = MaterialPreparation::where([
                                        ['material_stock_id',$material_preparation->material_stock_id],
                                        ['auto_allocation_id',$material_preparation->auto_allocation_id],
                                        ['c_order_id',$material_preparation->c_order_id],
                                        ['po_buyer',$old_po_buyer],
                                        ['item_id',$material_preparation->item_id],
                                        ['uom_conversion',$material_preparation->uom_conversion],
                                        ['warehouse',$material_preparation->warehouse],
                                        ['style',$material_preparation->style],
                                        ['article_no',$material_preparation->article_no],
                                        ['qty_conversion',$material_preparation->qty_conversion],
                                        ['is_backlog',$material_preparation->is_backlog],
                                    ]);

                                    if($material_preparation->po_detail_id) $is_old_exists = $is_old_exists->where('po_detail_id',$material_preparation->po_detail_id);

                                    $is_old_exists = $is_old_exists->exists();

                                    if(!$is_old_exists)
                                    {
                                        $old_material_preparation = MaterialPreparation::FirstOrCreate([
                                            'material_stock_id'         => $material_preparation->material_stock_id,
                                            'auto_allocation_id'        => $material_preparation->auto_allocation_id,
                                            'po_detail_id'              => $material_preparation->po_detail_id,
                                            'barcode'                   => 'REROUTE_'.$barcode,
                                            'referral_code'             => $referral_code,
                                            'sequence'                  => $sequence,
                                            'item_id'                   => $material_preparation->item_id,
                                            'item_id_source'            => $material_preparation->item_id_source,
                                            'c_order_id'                => $material_preparation->c_order_id,
                                            'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                            'supplier_name'             => $material_preparation->supplier_name,
                                            'document_no'               => $material_preparation->document_no,
                                            'po_buyer'                  => $old_po_buyer,
                                            'uom_conversion'            => $material_preparation->uom_conversion,
                                            'qty_conversion'            => $material_preparation->qty_conversion,
                                            'qty_reconversion'          => $material_preparation->qty_reconversion,
                                            'job_order'                 => $material_preparation->job_order,
                                            'style'                     => $material_preparation->style,
                                            '_style'                    => $material_preparation->_style,
                                            'article_no'                => $material_preparation->article_no,
                                            'warehouse'                 => $material_preparation->warehouse,
                                            'item_code'                 => $material_preparation->item_code,
                                            'item_desc'                 => $material_preparation->item_desc,
                                            'category'                  => $material_preparation->category,
                                            'is_backlog'                => $material_preparation->is_backlog,
                                            'total_carton'              => 1,
                                            'type_po'                   => 2,
                                            'user_id'                   => $material_preparation->user_id,
                                            'last_status_movement'      => 'reroute',
                                            'is_reroute'                => true,
                                            'is_need_to_be_switch'      => false,
                                            'last_locator_id'           => $reroute_destination->id, 
                                            'last_movement_date'        => $material_preparation->last_movement_date,
                                            'created_at'                => $material_preparation->created_at,
                                            'updated_at'                => $material_preparation->updated_at,
                                            'first_print_date'          => carbon::now(),
                                            'deleted_at'                => $material_preparation->created_at,
                                            'last_user_movement_id'     => $material_preparation->last_user_movement_id,
                                            'is_stock_already_created'  => $material_preparation->is_stock_already_created,
                                            'is_reduce'                 => $material_preparation->is_reduce
                                        ]);

                                        if($old_material_preparation->po_detail_id == 'FREE STOCK' || $old_material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                        {
                                            $c_order_id             = 'FREE STOCK';
                                            $no_packing_list        = '-';
                                            $no_invoice             = '-';
                                            $c_orderline_id         = '-';
                                        }else
                                        {
                                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                            ->whereNull('material_roll_handover_fabric_id')
                                            ->where('po_detail_id',$old_material_preparation->po_detail_id)
                                            ->first();

                                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                        }
                                        
                                        $is_old_movement_exists = MaterialMovement::where([
                                            ['from_location',$supplier_location->id],
                                            ['to_destination',$receiving_location->id],
                                            ['from_locator_erp_id',$supplier_location->area->erp_id],
                                            ['to_locator_erp_id',$receiving_location->area->erp_id],
                                            ['po_buyer',$old_material_preparation->po_buyer],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','receive'],
                                        ])
                                        ->first();

                                        if(!$is_old_movement_exists)
                                        {
                                            $old_material_movement = MaterialMovement::firstOrCreate([
                                                'from_location'         => $supplier_location->id,
                                                'to_destination'        => $receiving_location->id,
                                                'from_locator_erp_id'   => $supplier_location->area->id,
                                                'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                'po_buyer'              => $old_material_preparation->po_buyer,
                                                'is_integrate'          => false,
                                                'is_active'             => true,
                                                'no_packing_list'       => $no_packing_list,
                                                'no_invoice'            => $no_invoice,
                                                'status'                => 'receive',
                                                'created_at'            => $old_material_preparation->created_at,
                                                'updated_at'            => $old_material_preparation->updated_at,
                                            ]);
                                            $old_material_movement_id = $old_material_movement->id;
                                        }else
                                        {
                                            $is_old_movement_exists->updated_at = $old_material_preparation->updated_at;
                                            $is_old_movement_exists->save();

                                            $old_material_movement_id = $is_old_movement_exists->id;
                                        }

                                        $is_old_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                        ->where([
                                            ['material_movement_id',$old_material_movement_id],
                                            ['material_preparation_id',$old_material_preparation->id],
                                            ['qty_movement',sprintf('%0.8f',$old_material_preparation->qty_conversion)],
                                            ['date_movement',$old_material_preparation->created_at],
                                            ['item_id',$old_material_preparation->item_id],
                                            ['warehouse_id',$old_material_preparation->warehouse],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                        ]) 
                                        ->exists();

                                        if(!$is_old_material_movement_line_exists)
                                        {
                                            $new_material_movement_line = MaterialMovementLine::Create([
                                                'material_movement_id'          => $old_material_movement_id,
                                                'material_preparation_id'       => $old_material_preparation->id,
                                                'item_code'                     => $old_material_preparation->item_code,
                                                'item_id'                       => $old_material_preparation->item_id,
                                                'item_id_source'                => $old_material_preparation->item_id_source,
                                                'c_order_id'                    => $old_material_preparation->c_order_id,
                                                'c_orderline_id'                => $c_orderline_id,
                                                'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                'supplier_name'                 => $old_material_preparation->supplier_name,
                                                'type_po'                       => 2,
                                                'is_integrate'                  => false,
                                                'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                'qty_movement'                  => sprintf('%0.8f',$old_material_preparation->qty_conversion),
                                                'date_movement'                 => $old_material_preparation->created_at,
                                                'created_at'                    => $old_material_preparation->created_at,
                                                'updated_at'                    => $old_material_preparation->created_at,
                                                'date_receive_on_destination'   => $old_material_preparation->created_at,
                                                'nomor_roll'                    => '-',
                                                'warehouse_id'                  => $old_material_preparation->warehouse,
                                                'document_no'                   => $old_material_preparation->document_no,
                                                'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                'is_active'                     => true,
                                                'user_id'                       => $old_material_preparation->last_user_movement_id,
                                            ]);

                                            $old_material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                            $old_material_preparation->save();
                                        }
                                       
                                        $reroute_date = carbon::now();
                                        $is_old_movement_reroute_exists = MaterialMovement::where([
                                            ['from_location',$receiving_location->id],
                                            ['to_destination',$reroute_destination->id],
                                            ['from_locator_erp_id',$receiving_location->area->erp_id],
                                            ['to_locator_erp_id',$reroute_destination->area->erp_id],
                                            ['po_buyer',$old_material_preparation->po_buyer],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','reroute'],
                                        ])
                                        ->first();

                                        if(!$is_old_movement_reroute_exists)
                                        {
                                            $old_movement_reroute = MaterialMovement::firstOrCreate([
                                                'from_location'         => $receiving_location->id,
                                                'to_destination'        => $reroute_destination->id,
                                                'from_locator_erp_id'   => $receiving_location->area->id,
                                                'to_locator_erp_id'     => $reroute_destination->area->erp_id,
                                                'po_buyer'              => $old_material_preparation->po_buyer,
                                                'is_integrate'          => false,
                                                'is_active'             => true,
                                                'no_packing_list'       => $no_packing_list,
                                                'no_invoice'            => $no_invoice,
                                                'status'                => 'receive',
                                                'created_at'            => $reroute_date,
                                                'updated_at'            => $reroute_date,
                                            ]);

                                            $old_movement_reroute_id = $old_movement_reroute->id;
                                        }else
                                        {
                                            $is_old_movement_reroute_exists->updated_at = $reroute_date;
                                            $is_old_movement_reroute_exists->save();

                                            $old_movement_reroute_id = $is_old_movement_reroute_exists->id;
                                        }

                                        $is_old_material_movement_line_reroute_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                        ->where([
                                            ['material_movement_id',$old_movement_reroute_id],
                                            ['material_preparation_id',$old_material_preparation->id],
                                            ['item_id',$old_material_preparation->item_id],
                                            ['qty_movement',sprintf('%0.8f',$old_material_preparation->qty_conversion)],
                                            ['warehouse_id',$old_material_preparation->warehouse],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['date_movement',$reroute_date],
                                        ]) 
                                        ->exists();

                                        if(!$is_old_material_movement_line_exists)
                                        {
                                            $new_material_movement_line_reroute = MaterialMovementLine::Create([
                                                'material_movement_id'          => $old_movement_reroute_id,
                                                'material_preparation_id'       => $old_material_preparation->id,
                                                'item_code'                     => $old_material_preparation->item_code,
                                                'item_id'                       => $old_material_preparation->item_id,
                                                'item_id_source'                => $old_material_preparation->item_id_source,
                                                'c_order_id'                    => $old_material_preparation->c_order_id,
                                                'c_orderline_id'                => $c_orderline_id,
                                                'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                'supplier_name'                 => $old_material_preparation->supplier_name,
                                                'type_po'                       => 2,
                                                'is_integrate'                  => false,
                                                'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                'qty_movement'                  => sprintf('%0.8f',$old_material_preparation->qty_conversion),
                                                'date_movement'                 => $reroute_date,
                                                'created_at'                    => $reroute_date,
                                                'updated_at'                    => $reroute_date,
                                                'date_receive_on_destination'   => $reroute_date,
                                                'nomor_roll'                    => '-',
                                                'warehouse_id'                  => $old_material_preparation->warehouse,
                                                'document_no'                   => $old_material_preparation->document_no,
                                                'note'                          => 'REROUTE KE PO BUYER '.$material_preparation->po_buyer,
                                                'is_active'                     => true,
                                                'user_id'                       => $system->id,
                                            ]);

                                            $old_material_preparation->last_material_movement_line_id = $new_material_movement_line_reroute->id;
                                            $old_material_preparation->save();
                                        }

                                        $old_detail_material_preparation = DetailMaterialPreparation::where('material_preparation_id',$material_preparation->id)->first();
                                        DetailMaterialPreparation::FirstOrCreate([
                                            'material_preparation_id'   => $old_material_preparation->id,
                                            'material_arrival_id'       => ($old_detail_material_preparation ? $old_detail_material_preparation->material_arrival_id : null),
                                            'user_id'                   => $old_material_preparation->user_id
                                        ]);

                                    }
                                }
                                
                                $material_preparation->last_status_movement  = 'print';
                                $material_preparation->last_movement_date    = $movement_date;
                                $material_preparation->last_locator_id       = $freestock_location->id;
                                $material_preparation->barcode               = $barcode;
                                $material_preparation->referral_code         = $referral_code;
                                $material_preparation->sequence              = $sequence;


                                if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$material_preparation->po_detail_id)
                                    ->first();

                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                // masukin history
                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$receiving_location->id],
                                    ['to_destination',$freestock_location->id],
                                    ['from_locator_erp_id',$receiving_location->area->erp_id],
                                    ['to_locator_erp_id',$freestock_location->area->erp_id],
                                    ['po_buyer',$material_preparation->po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','print'],
                                ])
                                ->first();
                                
                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $receiving_location->id,
                                        'to_destination'        => $freestock_location->id,
                                        'from_locator_erp_id'   => $receiving_location->area->erp_id,
                                        'to_locator_erp_id'     => $freestock_location->area->erp_id,
                                        'po_buyer'              => $material_preparation->po_buyer,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'print',
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
                                    ]);
            
                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->updated_at = $movement_date;
                                    $is_movement_exists->save();
            
                                    $material_movement_id = $is_movement_exists->id;
                                }
        

                                $is_material_movement_line_exits = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['material_preparation_id',$material_preparation->id],
                                    ['qty_movement',sprintf('%0.8f',$material_preparation->qty_conversion)],
                                    ['is_integrate',false],
                                    ['item_id',$material_preparation->item_id],
                                    ['warehouse_id',$material_preparation->warehouse],
                                    ['is_active',true],
                                    ['date_movement',$movement_date],
                                ]) 
                                ->exists();

                                if(!$is_material_movement_line_exits)
                                {
                                    $new_material_movement_line = MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_id,
                                        'material_preparation_id'       => $material_preparation->id,
                                        'item_code'                     => $material_preparation->item_code,
                                        'item_id'                       => $material_preparation->item_id,
                                        'item_id_source'                => $material_preparation->item_id_source,
                                        'c_order_id'                    => $material_preparation->c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                        'supplier_name'                 => $material_preparation->supplier_name,
                                        'type_po'                       => 2,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $material_preparation->uom_conversion,
                                        'qty_movement'                  => sprintf('%0.8f',$material_preparation->qty_conversion),
                                        'date_movement'                 => $movement_date,
                                        'created_at'                    => $movement_date,
                                        'updated_at'                    => $movement_date,
                                        'date_receive_on_destination'   => $movement_date,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $material_preparation->warehouse,
                                        'document_no'                   => $material_preparation->document_no,
                                        'note'                          => null,
                                        'is_active'                     => true,
                                        'user_id'                       => auth::user()->id,
                                    ]);

                                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                }

                                Temporary::Create([
                                    'barcode'       => $material_preparation->po_buyer,
                                    'status'        => 'mrp',
                                    'user_id'       => Auth::user()->id,
                                    'created_at'    => Auth::user()->id,
                                    'updated_at'    => $movement_date,

                                ]);
                            }

                            $material_preparation->first_print_date = $movement_date;
                        }
                        
                        $material_preparation->save();

                        $__material_arrival_id [] = $material_preparation->id;
                        $flag_return_print++;

                        $return_print [] = $material_preparation->id;
                        $concatenate .= "'" .$material_preparation->barcode."',";
                    }
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !='') DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            

            return response()->json($return_print,200);
        }else
        {
            return response()->json('Data not found.',422);
        }
    }

    public function barcode(request $request)
    {
        $list_barcodes = json_decode($request->list_barcodes);

        $items = array();
        foreach (array_unique($list_barcodes) as $key => $list_barcode) 
        {
            $items [] = MaterialPreparation::find($list_barcode);
        }
        /*$items = MaterialPreparation::whereIn('material_preparations.id',$list_barcodes)
        ->get();*/

        return view('accessories_barcode_material_allocation.barcode', compact('items'));
    }

    public function export()
    {
        return Excel::create('upload_barcode_allocation',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'B' => '@',
                    'D' => '@'
                ));

            });

            $excel->setActiveSheetIndex(0);



        })->export('xlsx');
    }

    public function import(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $warehouse_id   = $request->warehouse_id;

            if(!empty($data) && $data->count())
            {
                $array = array();
                foreach ($data as $key => $value) 
                {
                    $po_buyer   = strtoupper(trim($value->po_buyer));
                    $item_code  = strtoupper(trim($value->item_code));
                    
                    $allocations = MaterialPreparation::where([
                        ['warehouse',$warehouse_id],
                        ['po_buyer',$po_buyer],
                        ['item_code',$item_code],
                    ])
                    ->whereIn('auto_allocation_id',function($query)
                    {
                        $query->select('id')
                        ->from('auto_allocations')
                        ->where('is_allocation_purchase',false);
                    })
                    ->orderby('created_at','desc')
                    ->get();

                    
                    foreach ($allocations as $key => $allocation) 
                    {
                        $user = auth::user();
                        if($user->hasRole(['admin-ict-acc','supervisor','approval-free-stock'])) $show_selected = true;
                        else$show_selected = false;
            
                        if($allocation->first_print_date)
                        {
                            $note = 'ALREADY PRINT ON '.$allocation->first_print_date->format('d/m/y H:m:s');
                        }else
                        {
                            $note = 'READY TO PRINT';
                        }
                        
                        $auto_allocation            = AutoAllocation::find($allocation->auto_allocation_id);

                        $old_po_buyer               = ($auto_allocation ? $auto_allocation->old_po_buyer : null);
                        
                        if($old_po_buyer!= $po_buyer)  $show_po_buyer   = $po_buyer.' (po buyer ini reroute dari po '.$old_po_buyer.')'; 
                        else    $show_po_buyer   = $po_buyer;
                        
                        $obj                = new stdClass();
                        $obj->show_selected = $show_selected;
                        $obj->selected      = (!$allocation->first_print_date ? true : false);
                        $obj->id            = $allocation->id;
                        $obj->document_no   = $allocation->document_no;
                        $obj->show_po_buyer = $show_po_buyer;
                        $obj->item_code     = $allocation->item_code;
                        $obj->style         = $allocation->style;
                        $obj->job_order     = $allocation->job_order;
                        $obj->uom_conversion= $allocation->uom_conversion;
                        $obj->qty_conversion= $allocation->qty_conversion;
                        $obj->is_additional = $allocation->is_additional;
                        $obj->note          = $note;
            
                        $array [] = $obj;
                    }
                }

                return response()->json($array,200);
            }else
            {
                  return response()->json('Import failed, please check you file first.',422);
            }
        }

    }

    static function randomCode()
    {
        $referral_code = '2A'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;

        $obj                    = new stdClass();
        $obj->barcode           = $referral_code.''.$sequence;
        $obj->referral_code     = $referral_code;
        $obj->sequence          = $sequence;
        
        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);
        return $obj;
    }

    static function checkIsBacklog($po_buyer,$item_code)
    {
        /*$baclog_exists = MaterialBacklog::where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code]
        ])
        ->exists();

        if($baclog_exists) $is_backlog = true;
        else $is_backlog = false;
        

        return $is_backlog;*/
    }
}
