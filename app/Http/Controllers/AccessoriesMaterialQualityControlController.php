<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Rma;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\UomConversion;
use App\Models\MaterialCheck;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialStock;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
class AccessoriesMaterialQualityControlController extends Controller
{
    public function index()
    {
        return view('accessories_material_quality_control.index');
    }

    public function create(request $request)
    {
        $barcode                   = trim($request->barcode);
        $_warehouse_id             = $request->warehouse_id;
        $has_dash                  = strpos($barcode, "-");
        $persentage_metal_detector = $request->persentage_metal_detector;

        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
        }

        $exists_preparation = MaterialPreparation::where([
            ['barcode',strtoupper($_barcode)],
            ['warehouse',$_warehouse_id]
        ])
        //->whereNull('deleted_at')
        ->exists();

        if(!$exists_preparation)
        {
            //ambil qty dari free stock buat reject stock buffer
            $material_preparation = MaterialStock::where([
                ['po_detail_id',strtoupper($_barcode)],
                ['warehouse_id',$_warehouse_id],
                ['available_qty','>', 0],
                ['is_running_stock', false]
            ])
            //->whereNull('po_buyer')
            ->first();  
            
            if($material_preparation)
            {
                $barcode = $material_preparation->po_detail_id;
                $uom     = $material_preparation->uom;
                $qty     = $material_preparation->available_qty;
                //$arrival = MaterialArrival::where('po_detail_id', $barcode)->first();


                $qty_need = 0;
                $qty_order = $material_preparation->available_qty;
            }
            else
            {
                $material_stock = MaterialStock::where([
                    ['po_detail_id',strtoupper($_barcode)],
                    ['warehouse_id',$_warehouse_id],
                    ['is_running_stock', true]
                ])
                ->first();  
                if($material_stock)
                {
                    return response()->json('Proses Alocating Stock Sedang Berjalan',422);
                }
                else
                {
                    return response()->json('Barcode Tidak Ditemukan ',422);
                }

            }
            
        }
        else
        {
            $material_preparation = MaterialPreparation::where([
                ['barcode',strtoupper($_barcode)],
                ['warehouse',$_warehouse_id]
            ])
            ->first();

            $barcode = $material_preparation->barcode;
            $uom     = $material_preparation->uom_conversion;
            $qty     = $material_preparation->qty_conversion;

            if($material_preparation->last_status_movement == 'in' ||  $material_preparation->last_status_movement == 'change' )
            {
                $destination = Locator::find($material_preparation->last_locator_id);
                return response()->json('This data still in locator, please checkout first to qc. (Barang ini masih ada di rak '.$destination->code.'. Silahkan dikeluarkan ke QC)',422);
            }

            if($material_preparation->deleted_at <> null){
                $destination = Locator::find($material_preparation->last_locator_id);
                return response()->json('Barcode already supplied to '.$destination->code,422); 
            }

            $material_requirement = $material_preparation->dataRequirement($material_preparation->po_buyer,$material_preparation->item_code,$material_preparation->style,$material_preparation->article_no);
            $qty_need             = ($material_requirement) ? $material_requirement->qty_required : '0';

            $material_preparation_id = $material_preparation->id;
            if(!$material_preparation->is_allocation)
            {
                $arrival = MaterialArrival::select('qty_ordered')
                ->whereIn('id',function($query) use($material_preparation_id)
                {
                    $query->select('material_arrival_id')
                    ->from('detail_material_preparations')
                    ->where('material_preparation_id',$material_preparation_id);
                })
                ->groupby('qty_ordered')
                ->first();

                if($arrival) $qty_order = $arrival->qty_ordered;
                else $qty_order         = null;
            }else
            {
                $allocation = AllocationItem::select('qty_booking')
                ->whereIn('id',function($query) use($material_preparation_id){
                    $query->select('allocation_item_id')
                    ->from('detail_material_preparations')
                    ->where('material_preparation_id',$material_preparation_id);
                })
                ->groupby('qty_booking')
                ->first();

                if($allocation) $qty_order  = $allocation->qty_booking;
                else $qty_order             = null;
            }
        }


        $material_check = MaterialCheck::whereNotNull('barcode_preparation')
        ->where('barcode_preparation',$_barcode)
        ->where('is_from_metal_detector', false)
        ->whereNull('deleted_at')
        ->first();

        //dd($material_check);
        if(!$material_preparation) return response()->json('Barcode not found.',422); 
        if($material_preparation->po_detail_id = null || $material_preparation->po_detail_id = '') return response()->json('Please contact ICT',422);
        if($material_check){
            if($material_check->status != 'HOLD' && $material_check->status != 'QUARANTINE')
             return response()->json('Barcode already scanned',422); 
        }
        
        $options = [
            [
                'id' => 'QUARANTINE',
                'name' => 'QUARANTINE'
            ],
            [
                'id' => 'HOLD',
                'name' => 'HOLD'
            ],
            [
                'id' => 'RELEASE',
                'name' => 'RELEASE'
            ],
            [
                'id' => 'REJECT',
                'name' => 'REJECT'
            ],
            [
                'id' => 'RANDOM',
                'name' => 'RANDOM'
            ]
        ];

        $material_requirement = MaterialRequirement::where([
            ['po_buyer',$material_preparation->po_buyer],
            ['item_id',$material_preparation->item_id],
            ['style',$material_preparation->style],
            ['article_no',$material_preparation->article_no]
        ])
        ->first();

        if($material_requirement)
        {
            if($material_requirement->warehouse_id)
            {
                $warehouse_place = $material_requirement->warehouse_id;
                if($warehouse_place == '1000007') $_warehouse_place = '1000002';
                else if($warehouse_place == '1000010') $_warehouse_place = '1000013';
                else $_warehouse_place = $_warehouse_id;

                if($_warehouse_place != $_warehouse_id) $is_handover = true;
                else $is_handover = false;
            } else
            {
                $is_handover = false;
                $_warehouse_place = $_warehouse_id;
            }
        }
        else
        {
            $is_handover = false;
            $_warehouse_place = $_warehouse_id;

        }

        
        //alokasi subcont / beda whs 
        $erp = DB::connection('erp');
        $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
            ['poreference',$material_preparation->po_buyer],
            ['componentid',$material_preparation->item_id]
        ])
        ->select('warehouse_place')
        ->first();
        $sp = null;
        if($sewing_place != null)
        {
            $sp = $sewing_place->warehouse_place;
        }else{
            $sp = null;
        }


        $subconts = PoBuyer::whereNotNull('note')
                ->where('po_buyer',$material_preparation->po_buyer)
                ->first();

        if($subconts)
        {
            $is_subcont = true;
            $note       = $subconts->note;
        }
        else
        {
            $is_subcont = false;
            $note       = null;
        }

        $is_booking = AutoAllocation::where('id', $material_preparation->auto_allocation_id)
                                    ->where('is_allocation_purchase', false)
                                    ->exists();

        if($is_booking)
        {
            $remark_stock_booking = 'Berasal dari booking';
            $is_missing_item = MaterialPreparation::where([
                ['po_buyer',$material_preparation->po_buyer],
                ['item_code',$material_preparation->item_code],
                ['last_status_movement', 'in'],
                ['warehouse',$_warehouse_id],
                ['barcode', 'like', '%2A%'],
            ])
            ->first();

            if($is_missing_item)
            {
                $missing_item = $remark_stock_booking. ".Sebagian Stock Booking sudah ada dirack ". $is_missing_item->lastLocator->code;
            }
            else
            {
                $missing_item = $remark_stock_booking;
            }
        }
        else
        {
            $missing_item ='';
        }

        
        $obj                            = new StdClass();
        $obj->check_all                 = false;
        $obj->barcode                   = $barcode;
        $obj->material_preparation_id   = $material_preparation->id;
        $obj->supplier_name             = $material_preparation->supplier_name;
        $obj->c_order_id                = $material_preparation->c_order_id;
        $obj->c_bpartner_id             = $material_preparation->c_bpartner_id;
        $obj->document_no               = $material_preparation->document_no;
        $obj->po_buyer                  = $material_preparation->po_buyer;
        $obj->item_code                 = $material_preparation->item_code;
        $obj->item_desc                 = $material_preparation->item_desc;
        $obj->category                  = $material_preparation->category;
        $obj->uom_conversion            = $uom;
        $obj->qty_need                  = $qty_need;
        $obj->_qty                      = $qty;
        $obj->qty                       = $qty;
        $obj->status_options            = $options;
        $obj->_status                   = ($material_check) ? $material_check->status:'QUARANTINE';
        $obj->status                    = ($material_check) ? $material_check->status:'QUARANTINE';
        $obj->qty_check                 = 0;
        $obj->qty_order                 = $qty_order;
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->remark                    = null;
        $obj->warehouse_place           = $_warehouse_place;
        $obj->is_handover               = $is_handover;
        $obj->is_subcont                = $is_subcont;
        $obj->note                      = $note;
        $obj->persentage_metal_detector = $persentage_metal_detector;
        $obj->missing_item              = $missing_item;
        $obj->is_booking                = $is_booking;        
        $obj->sewing_place              = $sp;

        //dd($_warehouse_place);
        
        return response()->json($obj,200); 
       
    }
    
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcodes' => 'required|not_in:[]'
        ]);
        
        if ($validator->passes())
        {
            //return response()->json($barcodes);
            
            $_warehouse_id      = $request->warehouse_id;
            $barcodes           = json_decode($request->barcodes);
            $update_status      = array();
            $concatenate        = '';
            $_movement_date     = carbon::now()->todatetimestring();

            try 
            {
                DB::beginTransaction();

                foreach ($barcodes as $key => $value) 
                {
                    $is_exists = MaterialCheck::where('barcode_preparation',$value->barcode)
                    ->whereNull('deleted_at')
                    ->first();
                    
                    $material_preparation_id = $value->material_preparation_id;
                    $movement_date              = ($value->movement_date) ? $value->movement_date : $_movement_date;
                    $persentage_metal_detector  = $value->persentage_metal_detector;
                    $is_prepare_exists                  = MaterialPreparation::where('id', $material_preparation_id)->exists();
                    if($is_prepare_exists)
                    {
                        $material_preparation       = MaterialPreparation::find($material_preparation_id);
                    }
                    else
                    {
                        $material_stock = MaterialStock::find($material_preparation_id);
                        $po_detail_id    = $material_stock->po_detail_id;
                        $item_id         = $material_stock->item_id;
                        $c_order_id      = $material_stock->c_order_id;
                        $c_bpartner_id   = $material_stock->c_bpartner_id;
                        $supplier_name   = $material_stock->supplier_name;
                        $document_no     = $material_stock->document_no;
                        $uom             = $material_stock->uom;
                        $qty             = $value->qty_check;
                        $warehouse_id    = $material_stock->warehouse_id;
                        $item_code       = $material_stock->item_code;
                        $item_desc       = $material_stock->item_desc;
                        $category        = $material_stock->category;
                        $warehouse_id    = $material_stock->warehouse_id;
                        $locator_id      = $material_stock->locator_id;
                        $type_stock_erp_code = $material_stock->type_stock_erp_code;
                        $type_stock      = $material_stock->type_stock;

                        //potong stocknya 
                        $available_qty = $material_stock->available_qty;
                        $reserved_qty = $material_stock->reserved_qty;
                        $new_available_qty = $available_qty - $qty;
                        $new_reserved_qty  = $reserved_qty + $qty;
                        $operator = $qty * -1;

                        //
                        HistoryStock::approved($material_preparation_id
                        ,$new_available_qty
                        ,$available_qty
                        ,$operator
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'REJECT STOCK BUFFER , '
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,true
                        );

                        $material_stock->available_qty = $new_available_qty;
                        $material_stock->reserved_qty = $new_reserved_qty;

                        $material_stock->save();

                        //insert preparation dulu
                        $material_preparation = MaterialPreparation::Create([
                            'material_stock_id'                             => $material_preparation_id,
                            //'auto_allocation_id'                            => null,
                            'po_detail_id'                                  => $po_detail_id,
                            'barcode'                                       => '-',
                            'referral_code'                                 => '-',
                            'sequence'                                      => 0,
                            'item_id'                                       => $item_id,
                            'c_order_id'                                    => $c_order_id,
                            'c_bpartner_id'                                 => $c_bpartner_id,
                            'supplier_name'                                 => $supplier_name,
                            'document_no'                                   => $document_no,
                            'po_buyer'                                      => '-',
                            'uom_conversion'                                => $uom,
                            'qty_conversion'                                => 0,
                            'qty_reconversion'                              => 0,
                            'job_order'                                     => '-',
                            'style'                                         => '-',
                            '_style'                                        => '-',
                            'article_no'                                    => '-',
                            'warehouse'                                     => $warehouse_id,
                            'item_code'                                     => $item_code,
                            'item_desc'                                     => $item_desc,
                            'category'                                      => $category,
                            'is_additional'                                 => false,
                            'is_backlog'                                    => false,
                            'total_carton'                                  => 1,
                            'type_po'                                       => 2,
                            'user_id'                                       => auth::user()->id,
                            'last_status_movement'                          => 'receive',
                            'is_need_to_be_switch'                          => false,
                            'is_need_inserted_to_locator_free_stock_erp'    => false,
                            'last_locator_id'                               => $locator_id,
                            'last_movement_date'                            => $movement_date,
                            'created_at'                                    => $movement_date,
                            'updated_at'                                    => $movement_date,
                            'last_user_movement_id'                         => auth::user()->id,
                            'is_stock_already_created'                      => false
                        ]);

                        $material_preparation_id = $material_preparation->id;

                    }
                    
                    $conversion = UomConversion::where([
                        ['item_id',$material_preparation->item_id],
                        ['uom_to',$material_preparation->uom_conversion]
                    ]);

                    if($material_preparation->material_stock_id)
                    {
                        $uom_source = $material_preparation->materialStock->uom_source;
                        if($uom_source) $conversion = $conversion->where('uom_from',$material_preparation->materialStock->uom_source);
                    }

                    $conversion = $conversion->first();
                
                    if($conversion) $multiplyrate   = $conversion->multiplyrate;
                    else $multiplyrate   = 1;
                
                    if($is_exists)
                    {
                        $is_exists->updated_at = $movement_date;
                        $is_exists->deleted_at = $movement_date;
                        $is_exists->save();
                    }

                    $material_check = MaterialCheck::create([
                        'material_preparation_id'   => $material_preparation_id,
                        'barcode_preparation'       => $value->barcode,
                        'uom_po'                    => $value->_qty,
                        'uom_conversion'            => $value->uom_conversion,
                        'document_no'               => $value->document_no,
                        'po_buyer'                  => $value->po_buyer,
                        'status'                    => $value->status,
                        'status'                    => $value->status,
                        'status'                    => $value->status,
                        'item_code'                 => $value->item_code,
                        'item_desc'                 => $value->item_desc,
                        'qty_order'                 => isset($value->qty_order)? $value->qty_order : null,
                        'qty_reject'                => $value->qty_check,
                        'remark'                    => strtoupper(trim($value->remark)),
                        'user_id'                   => Auth::user()->id,
                        'is_reject'                 => ($value->status == 'REJECT' ? true : false ),
                        'created_at'                => $movement_date,
                        'updated_at'                => $movement_date,
                    ]);

                    if ($value->qty == 0 && $value->status == 'REJECT')
                    {
                        $status_reject = 'FULL REJECT';
                    }else
                    {
                        if($value->status == 'HOLD' || $value->status == 'RELEASE' || $value->status == 'RANDOM') $status_reject = null;
                        else $status_reject = 'PARTIAL REJECT';
                    }

                    $obj                            = new StdClass();
                    $obj->material_preparation_id   = $material_preparation_id;
                    $obj->status                    = $value->status;
                    $obj->qty                       = $value->qty;
                    $obj->qty_old                   = $value->_qty;
                    $obj->qty_reject                = $value->qty_check;
                    $obj->remark                    = strtoupper(trim($value->remark));
                    $obj->movement_date             = $movement_date;
                    $obj->status_reject             = $status_reject;
                    $obj->multiplyrate              = $multiplyrate;
                    $obj->persentage_metal_detector = $persentage_metal_detector;
                    $update_status []               = $obj;

                    if($value->status == 'REJECT')
                    {
                        if($material_preparation)
                        {
                            $auto_allocation_id             = $material_preparation->auto_allocation_id;
                           
                            if($auto_allocation_id)
                            {
                                $auto_allocation                = AutoAllocation::find($auto_allocation_id);
                                $qty_reject_auto_allocation     = $auto_allocation->qty_reject;
                                $qc_note_auto_allocation        = $auto_allocation->qc_note;

                                $new_qty_reject_auto_allocation = sprintf('%0.8f',$qty_reject_auto_allocation + $value->qty_check);
                                $new_note                       = 'QTY SEBESAR '.$new_qty_reject_auto_allocation.' DI REJECT QC';
                               
                                $auto_allocation->qty_reject    = $new_qty_reject_auto_allocation;
                                $auto_allocation->qc_note       = $new_note;
                                $auto_allocation->save();
                            }


                        }
                    }
                        

                    $concatenate .= "'" .$value->material_preparation_id."',";
                }

                $concatenate = substr_replace($concatenate, '', -1);
                if($concatenate !=''){
                    DB::select(db::raw("SELECT * FROM delete_duplicate_qc_acc(array[".$concatenate."]);"));
                }
                
                $this->updateQcStatus($update_status,$_warehouse_id);

                if($concatenate !='')
                {
                    DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
                }
            
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            return response()->json(200); 
        }else{
            return response()->json('Please scan barcode first.',422); 
        }
    }

    static function updateQcStatus($update_status,$_warehouse_id)
    {
        $checking_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC') 
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','CHECKING')
        ->first();

        $locator_reject_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC')
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','REJECT')
        ->first();

        $locator_hold_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
        {
            $query->where('name','QC')
            ->where('warehouse',$_warehouse_id);
        })
        ->where('rack','HOLD')
        ->first();

        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($update_status as $key => $value) 
            {
                $material_preparation   = MaterialPreparation::find($value->material_preparation_id);
                //dd($material_preparation)
                $qty                  = sprintf('%0.8f',$value->qty);
                $qty_old              = sprintf('%0.8f',$value->qty_old);
                $qty_reject           = sprintf('%0.8f',$value->qty_reject);
                $movement_date        = $value->movement_date;
                $status               = $value->status;
                $_status              = strtolower($value->status);
                $status_reject        = $value->status_reject;
                $remark               = $value->remark;
                $persentage_metal_detector = $value->persentage_metal_detector;

                if($status == 'REJECT')
                {
                    if($status_reject == 'FULL REJECT') $qc_status = 'FULL REJECT';
                    elseif($status_reject == 'PARTIAL REJECT') $qc_status = $_new_status = 'REJECT '.number_format($qty_reject, 4, '.', ',');
                
                    
                    $conversion = $material_preparation->conversion($material_preparation->item_code,$material_preparation->category,$material_preparation->uom_conversion);
                    if($conversion) $multiplyrate = $conversion->multiplyrate;
                    else $multiplyrate = 1;

                    if($material_preparation->po_buyer != '-')
                    {
                        $material_preparation->qty_reconversion = sprintf('%0.8f',$qty * $multiplyrate);
                        $material_preparation->qty_conversion   = sprintf('%0.8f',$qty);
                    }

                }else
                {
                    $qc_status = $status;
                }
                
                if($material_preparation)
                {
                    $c_order_id           = $material_preparation->c_order_id;
                    $po_detail_id         = $material_preparation->po_detail_id;
                    $warehouse_id         = $material_preparation->warehouse;
                    $last_locator_id      = $material_preparation->last_locator_id;
                    $last_locator_erp_id  = $material_preparation->lastLocator->area->erp_id;
                    $last_status_movement = $material_preparation->last_status_movement;
                    if($qc_status == 'RELEASE')
                    {
                        $last_status_movement = 'qc-release';
                    }

                    $material_preparation->qc_status    = $qc_status;
                    $_temp_locator                      = null;

                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();
                        
                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    }

                    if($status == 'REJECT' 
                    || $status_reject == 'FULL REJECT' 
                    || $status_reject == 'PARTIAL REJECT')
                    {
                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$locator_reject_qc->id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$locator_reject_qc->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',$_status],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {
                            $material_movement_qc = MaterialMovement::FirstOrCreate([
                                'from_location'             => $checking_qc->id,
                                'to_destination'            => $locator_reject_qc->id,
                                'from_locator_erp_id'       => $checking_qc->area->erp_id,
                                'to_locator_erp_id'         => $locator_reject_qc->area->erp_id,
                                'po_buyer'                  => $material_preparation->po_buyer,
                                'is_active'                 => true,
                                'status'                    => $_status,
                                'is_integrate'              => false,
                                'no_packing_list'           => $no_packing_list,
                                'no_invoice'                => $no_invoice,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                            ]);

                            $material_movement_qc_id = $material_movement_qc->id;
                        }else
                        {
                            $is_movement_qc_exists->updated_at = $movement_date;
                            $is_movement_qc_exists->save();

                            $material_movement_qc_id = $is_movement_qc_exists->id;
                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_qc_id],
                            ['material_preparation_id',$material_preparation->id],
                            ['item_id',$material_preparation->item_id],
                            ['warehouse_id',$material_preparation->warehouse],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$movement_date],
                            ['qty_movement',$qty],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
                            $new_material_movement_line_qc = MaterialMovementLine::FirstOrCreate([
                                'material_movement_id'      => $material_movement_qc_id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_id'                   => $material_preparation->item_id,
                                'item_id_source'            => $material_preparation->item_id_source,
                                'item_code'                 => $material_preparation->item_code,
                                'c_order_id'                => $material_preparation->c_order_id,
                                'c_orderline_id'            => $c_orderline_id,
                                'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                'supplier_name'             => $material_preparation->supplier_name,
                                'type_po'                   => $material_preparation->type_po,
                                'uom_movement'              => $material_preparation->uom_conversion,
                                'qty_movement'              => $qty,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_integrate'              => false,
                                'is_active'                 => true,
                                'nomor_roll'                => '-',
                                'warehouse_id'              => $material_preparation->warehouse,
                                'document_no'               => $material_preparation->document_no,
                                'note'                      => $remark.' [ AWAL QTYNYA ADALAH '.$qty_old.' LALU DI REJECT '.$qty_reject.' ]',
                                'user_id'                   => Auth::user()->id
                            ]);
                        }
                       
                        if($status_reject == 'FULL REJECT')
                        {
                            $material_preparation->last_material_movement_line_id   = $new_material_movement_line_qc->id;
                            $material_preparation->last_status_movement             = $last_status_movement;
                            $material_preparation->last_locator_id                  = $locator_reject_qc->id;
                            $material_preparation->last_movement_date               = $movement_date;
                            $material_preparation->last_user_movement_id            = auth::user()->id;
                            $material_preparation->deleted_at                       = $movement_date;
                        }

                        $material_preparation->qty_reject   = $qty_reject;
                        $material_preparation->remark       = $remark;


                        // motong stock erp jika asalnya dari free stock, pake internal use
                        if($po_detail_id == 'FREE STOCK')
                        {
                            $is_movement_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$material_preparation->po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_exists)
                            {
                                $material_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'po_buyer'              => $material_preparation->po_buyer,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $movement_date,
                                    'updated_at'            => $movement_date,
                                ]);
        
                                $material_movement_id = $material_movement->id;
                            }else
                            {
                                $is_movement_exists->updated_at = $movement_date;
                                $is_movement_exists->save();
        
                                $material_movement_id = $is_movement_exists->id;
                            }
    
                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_movement_id],
                                ['item_id',$material_preparation->item_id],
                                ['warehouse_id',$material_preparation->warehouse],
                                ['material_preparation_id',$material_preparation->id],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['date_movement',$movement_date],
                                ['qty_movement',-1*$qty_reject],
                            ])
                            ->exists();
    
                            if(!$is_line_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_movement_id,
                                    'material_preparation_id'       => $material_preparation->id,
                                    'item_code'                     => $material_preparation->item_code,
                                    'item_id'                       => $material_preparation->item_id,
                                    'item_id_source'                => $material_preparation->item_id_source,
                                    'c_order_id'                    => $material_preparation->c_order_id,
                                    'c_orderline_id'                => $material_preparation->c_orderline_id,
                                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                    'supplier_name'                 => $material_preparation->supplier_name,
                                    'type_po'                       => 2,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $material_preparation->uom_conversion,
                                    'qty_movement'                  => -1*$qty_reject,
                                    'date_movement'                 => $movement_date,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'date_receive_on_destination'   => $movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $material_preparation->warehouse,
                                    'document_no'                   => $material_preparation->document_no,
                                    'note'                          => 'MATERIAL ACC REJECT ASALNYA FREE STOCK JADI PAKE INTERNAL USE UNTUK MOTONG STOCK ERP, PAKE MENU QC REJECT DIREJECT KARENA '.$remark,
                                    'is_active'                     => true,
                                    'user_id'                       => auth::user()->id,
                                ]);
                            }
                        }
                    }

                    if($status == 'HOLD')
                    {
                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$locator_hold_qc->id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$locator_hold_qc->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',$_status],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {
                            $material_movement_qc = MaterialMovement::FirstOrCreate([
                                'from_location'             => $checking_qc->id,
                                'to_destination'            => $locator_hold_qc->id,
                                'from_locator_erp_id'       => $checking_qc->area->erp_id,
                                'to_locator_erp_id'         => $locator_hold_qc->area->erp_id,
                                'po_buyer'                  => $material_preparation->po_buyer,
                                'is_active'                 => true,
                                'status'                    => $_status,
                                'is_integrate'              => false,
                                'no_packing_list'           => $no_packing_list,
                                'no_invoice'                => $no_invoice,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                            ]);

                            $material_movement_qc_id = $material_movement_qc->id;
                        }else
                        {
                            $is_movement_qc_exists->updated_at = $movement_date;
                            $is_movement_qc_exists->save();

                            $material_movement_qc_id = $is_movement_qc_exists->id;
                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_qc_id],
                            ['material_preparation_id',$material_preparation->id],
                            ['warehouse_id',$material_preparation->warehouse],
                            ['item_id',$material_preparation->item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$movement_date],
                            ['qty_movement',$qty_old],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
                            $new_material_movement_line_qc = MaterialMovementLine::FirstOrCreate([
                                'material_movement_id'      => $material_movement_qc_id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_id'                   => $material_preparation->item_id,
                                'item_id_source'            => $material_preparation->item_id_source,
                                'item_code'                 => $material_preparation->item_code,
                                'c_order_id'                => $material_preparation->c_order_id,
                                'c_orderline_id'            => $c_orderline_id,
                                'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                'supplier_name'             => $material_preparation->supplier_name,
                                'type_po'                   => $material_preparation->type_po,
                                'uom_movement'              => $material_preparation->uom_conversion,
                                'qty_movement'              => $qty_old,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_integrate'              => false,
                                'is_active'                 => true,
                                'nomor_roll'                => '-',
                                'warehouse_id'              => $material_preparation->warehouse,
                                'document_no'               => $material_preparation->document_no,
                                'note'                      => $remark,
                                'user_id'                   => Auth::user()->id
                            ]);
                        }
        
                        $material_preparation->last_material_movement_line_id   = $new_material_movement_line_qc->id;
                        $material_preparation->last_status_movement             = $last_status_movement;
                        $material_preparation->last_locator_id                  = $locator_hold_qc->id;
                        $material_preparation->remark                           = $remark;
                        $material_preparation->last_movement_date               = $movement_date;
                        $material_preparation->last_user_movement_id            = auth::user()->id;
                    }

                    if($status == 'RELEASE' || $status == 'RANDOM')
                    {
                        $is_movement_qc_exists = MaterialMovement::where([
                            ['from_location',$checking_qc->id],
                            ['to_destination',$last_locator_id],
                            ['from_locator_erp_id',$checking_qc->area->erp_id],
                            ['to_locator_erp_id',$last_locator_erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',$_status],
                        ])
                        ->first();

                        if(!$is_movement_qc_exists)
                        {
                            $material_movement_qc = MaterialMovement::FirstOrCreate([
                                'from_location'             => $checking_qc->id,
                                'to_destination'            => $last_locator_id,
                                'from_locator_erp_id'       => $checking_qc->area->erp_id,
                                'to_locator_erp_id'         => $last_locator_erp_id,
                                'po_buyer'                  => $material_preparation->po_buyer,
                                'is_active'                 => true,
                                'status'                    => $_status,
                                'is_integrate'              => false,
                                'no_packing_list'           => $no_packing_list,
                                'no_invoice'                => $no_invoice,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                            ]);

                            $material_movement_qc_id = $material_movement_qc->id;
                        }else
                        {
                            $is_movement_qc_exists->updated_at = $movement_date;
                            $is_movement_qc_exists->save();

                            $material_movement_qc_id = $is_movement_qc_exists->id;
                        }

                        $is_material_movement_line_qc_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_qc_id],
                            ['material_preparation_id',$material_preparation->id],
                            ['warehouse_id',$material_preparation->warehouse],
                            ['item_id',$material_preparation->item_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['date_movement',$movement_date],
                            ['qty_movement',$qty_old],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_qc_exists)
                        {
                            $new_material_movement_line_qc = MaterialMovementLine::FirstOrCreate([
                                'material_movement_id'      => $material_movement_qc_id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_id'                   => $material_preparation->item_id,
                                'item_id_source'            => $material_preparation->item_id_source,
                                'item_code'                 => $material_preparation->item_code,
                                'c_order_id'                => $material_preparation->c_order_id,
                                'c_orderline_id'            => $c_orderline_id,
                                'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                'supplier_name'             => $material_preparation->supplier_name,
                                'type_po'                   => $material_preparation->type_po,
                                'uom_movement'              => $material_preparation->uom_conversion,
                                'qty_movement'              => $qty_old,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_integrate'              => false,
                                'is_active'                 => true,
                                'nomor_roll'                => '-',
                                'warehouse_id'              => $material_preparation->warehouse,
                                'document_no'               => $material_preparation->document_no,
                                'note'                      => $remark,
                                'user_id'                   => Auth::user()->id
                            ]);
                        }

                        $material_preparation->last_material_movement_line_id = $new_material_movement_line_qc->id;
                    }
                
                    $material_preparation->last_status_movement = $last_status_movement;
                    $material_preparation->persentage_metal_detector = $persentage_metal_detector;
                    $material_preparation->updated_at           = $movement_date;
                    $material_preparation->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
