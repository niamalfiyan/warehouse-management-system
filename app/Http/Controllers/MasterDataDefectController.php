<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Defect;

class MasterDataDefectController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_defect.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $defect = Defect::orderBy('created_at','desc');

            return DataTables::of($defect)
            ->addColumn('action', function($defect) {
                return view('master_data_defect._action', [
                    'model' => $defect,
                    'edit' => route('masterDataDefect.edit',$defect->id),
                    'delete' => route('masterDataDefect.delete',$defect->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_defect.create');
    }
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);
        
        if(Defect::where(db::raw('lower(code)'),str_slug(strtolower($request->code)))->exists())
            return response()->json('Code '.$request->code.' already exists.', 422);
       
        if(Session::has('flag')) Session::forget('flag');
        
        try 
        {
            DB::beginTransaction();

            Defect::FirstOrCreate([
                'code'          => strtolower($request->code),
                'description'   => strtolower($request->description_in_english),
                'description_2' => strtolower($request->description_in_bahasa),
                'user_id'       => Auth::user()->id,
            ]);

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        Session::flash('flag', 'success');
        return response()->json('success', 200);

      
    }
    

    public function edit($id)
    {
      $defect = Defect::find($id);
      return view('master_data_defect.edit',compact('defect'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        if(Defect::where(db::raw('lower(code)'),strtolower($request->code))->where('id','!=',$request->id)->exists())
            return response()->json('Code '.$request->code.' already exists.', 422);

        if(Session::has('flag')) Session::forget('flag');
        try 
        {
            DB::beginTransaction();
            $defect                 = Defect::find($request->id);
            $defect->code           = strtolower($request->code);
            $defect->description    = strtolower($request->description_in_english);
            $defect->description_2  = strtolower($request->description_in_bahasa);
            $defect->save();

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        Session::flash('flag', 'success_2');
        return response()->json('success', 200);
    }

    public function destroy($id)
    {
        Defect::Find($id)->delete();
        return response()->json(200);
    }
}
