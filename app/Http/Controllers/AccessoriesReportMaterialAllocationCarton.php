<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class AccessoriesReportMaterialAllocationCarton extends Controller
{
    public function index()
    {
        //return view('errors.503');
        return view('accessories_report_material_allocation_carton.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $material_cancel_order = db::table('material_requirement_cartons_v')
            ->orderby('lc_date','desc')
            ->take(50)
            ->get();

            return DataTables::of($material_cancel_order)
            ->make(true);
        }
    }

    public function export(Request $request)
    { 

        $material_cancel_order = db::table('material_requirement_cartons_v')
                                ->get();

        $file_name = 'accessories_report_allocation_carton ';
        return Excel::create($file_name,function($excel) use ($material_cancel_order)
        {
            $excel->sheet('active',function($sheet)use($material_cancel_order)
            {
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','BRAND');
                $sheet->setCellValue('C1','DESTINATION');
                $sheet->setCellValue('D1','LC_DATE');
                $sheet->setCellValue('E1','ITEM_CODE');
                $sheet->setCellValue('F1','STYLE');
                $sheet->setCellValue('G1','ARTICLE_NO');
                $sheet->setCellValue('H1','QTY_REQUIRED');
                $sheet->setCellValue('I1','QTY_ALLOCATION');
                $sheet->setCellValue('J1','QTY_OUTSTANDING');
                $sheet->setCellValue('K1','SEASON');
                $sheet->setCellValue('L1','PSD');
                $sheet->setCellValue('M1','PROMISE_DATE');
                $sheet->setCellValue('N1','BATCH_CODE');
                $sheet->setCellValue('O1','UPDATED_AT');
                $sheet->setCellValue('P1','WAREHOUSE_PACKING');

            $row=2;
            foreach ($material_cancel_order as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->po_buyer);
                $sheet->setCellValue('B'.$row,$i->brand);
                $sheet->setCellValue('C'.$row,$i->destination);
                $sheet->setCellValue('D'.$row,$i->lc_date);
                $sheet->setCellValue('E'.$row,$i->item_code);
                $sheet->setCellValue('F'.$row,$i->style);
                $sheet->setCellValue('G'.$row,$i->article_no);
                $sheet->setCellValue('H'.$row,$i->qty_required);
                $sheet->setCellValue('I'.$row,$i->qty_allocation);
                $sheet->setCellValue('J'.$row,$i->qty_outstanding);
                $sheet->setCellValue('K'.$row,$i->season);
                $sheet->setCellValue('L'.$row,$i->psd);
                $sheet->setCellValue('M'.$row,$i->promise_date);
                $sheet->setCellValue('N'.$row,$i->batch_code);
                $sheet->setCellValue('O'.$row,$i->updated_at);
                $sheet->setCellValue('P'.$row,$i->warehouse_name);
                $row++;
            }
            });

        })
        ->export('csv');

    }

}
