<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Barcode;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\MaterialStock;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\SummaryHandoverMaterial;
use App\Models\HistoryMaterialStockOpname;
use App\Models\MaterialRollHandoverFabric;


class FabricMaterialOutNonCuttingController extends Controller
{
    public function index()
    {
        // return view('errors.migration');
          
      $is_using_for_handover = MaterialStock::where([
          ['last_status','handover'],
          ['last_user_used_id',auth::user()->id],
      ])
      ->exists();

        if($is_using_for_handover)
        MaterialStock::where([
            ['last_status','handover'],
            ['last_user_used_id',auth::user()->id],
            ['is_allocated',false],
        ]) 
        ->update([
            'last_date_used' => null
            ,'last_status' => null
            ,'last_user_used_id' => null
        ]);

      return view('fabric_material_out_non_cutting.index');
    }

    public function create(Request $request)
    {
        $barcode        = strtoupper(trim($request->barcode));
        $warehouse_id   = $request->warehouse_id;

        $material_stock = MaterialStock::where([
            ['barcode_supplier',$barcode],
            ['warehouse_id',$warehouse_id],
            ['is_allocated',false],
            ['is_active',true],
        ])
        ->orderby('created_at','desc')
        ->first();
        
        if(!$material_stock) return response()->json('Barcde not found.', 422);
        
        if($material_stock->is_short_roll)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_short_roll) return response()->json('Actual yard is not same with barcode, please move to locator reject first',422);
        } 

        if($material_stock->is_reject_by_lot)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Roll cannot be used due of lot is not match,please move to locator reject first',422);
        } 

        if($material_stock->is_reject_by_rma)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_rma) return response()->json('cannot be used due of material is wrong,please move to locator reject first',422);
        } 

        if($material_stock->is_reject)
        {
            if(!$material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Roll cannot be used due of reject result by qc,please move to locator reject first',422);
        } 

        if($material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Barcode cannot used due reject by lot',422);
        if($material_stock->movement_to_locator_reject_date_from_rma) return response()->json('Barcode cannot used due reject by return material',422);
        if($material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Barcode cannot used due reject by qc',422);
        if(!$material_stock->approval_date) return response()->json('Barcode cannot used due approval is empty',422);
        if($material_stock->last_status == 'sto') return response()->json('Roll is in use for sto by '.$material_stock->userLastUsed->name.' at '.$material_stock->last_date_used,422);
        if($material_stock->last_status == 'relax') return response()->json('Roll is in use for rilex prosess by '.$material_stock->userLastUsed->name.' at '.$material_stock->last_date_used,422);
        if($material_stock->last_status == 'inspect_lab') return response()->json('Roll is in use by qc',422);
        //if($material_stock->inspect_lot_result == 'HOLD') return response()->json('Roll is in hold by qc due of lot inspection.',422);
        
        $is_outstanding_approval_exists = HistoryMaterialStockOpname::where('material_stock_id',$material_stock->id)
        ->where(function($query){
            $query->whereNull('approval_date')
            ->OrwhereNull('accounting_approval_date');
        })
        ->exists();
        
        if($is_outstanding_approval_exists) return response()->json('Roll cannot be STO due outstanding approval STO',422);

        $document_no        = strtoupper($material_stock->document_no);
        $item_code          = strtoupper($material_stock->item_code);
        $c_bpartner_id      = $material_stock->c_bpartner_id;
        $c_order_id         = $material_stock->c_order_id;
        $item_id            = $material_stock->item_id;
        $material_stock_id  = $material_stock->id;
        $locator_from_id    = $material_stock->locator_id;
        $locator_from_code  = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        $barcode            = $material_stock->barcode_supplier;
        $warehouse_id       = $material_stock->warehouse_id;
        $nomor_roll         = $material_stock->nomor_roll;
        $batch_number       = $material_stock->batch_number;
        $supplier_name      = strtoupper($material_stock->supplier_name);
        $no_packing_list    = $material_stock->no_packing_list;
        $no_invoice         = $material_stock->no_invoice;
        $color              = $material_stock->color;
        $begin_width        = $material_stock->begin_width;
        $middle_width       = $material_stock->middle_width;
        $end_width          = $material_stock->end_width;
        $actual_width       = $material_stock->actual_width;
        $actual_length      = $material_stock->actual_length;
        $load_actual        = $material_stock->load_actual;
        $uom                = trim($material_stock->uom);
        $po_detail_id       = $material_stock->po_detail_id;
        $stock              = $material_stock->stock;
        $available_qty      = $material_stock->available_qty;
        $reserved_qty       = $material_stock->reserved_qty;

        $obj                                = new stdClass();
        $obj->material_stock_id             = $material_stock_id;
        $obj->summary_handover_material_id  = null;
        $obj->locator_from_id               = $locator_from_id;
        $obj->locator_from_code             = $locator_from_code;
        $obj->barcode                       = $barcode;
        $obj->po_detail_id                  = $po_detail_id;
        $obj->warehouse_id                  = $warehouse_id;
        $obj->nomor_roll                    = $nomor_roll;
        $obj->batch_number                  = $batch_number;
        $obj->document_no                   = $document_no;
        $obj->item_code                     = $item_code;
        $obj->item_id                       = $item_id;
        $obj->supplier_name                 = $supplier_name;
        $obj->c_bpartner_id                 = $c_bpartner_id;
        $obj->no_packing_list               = $no_packing_list;
        $obj->no_invoice                    = $no_invoice;
        $obj->color                         = $color;
        $obj->begin_width                   = $begin_width;
        $obj->middle_width                  = $middle_width;
        $obj->end_width                     = $end_width;
        $obj->actual_width                  = $actual_width;
        $obj->actual_length                 = $actual_length;
        $obj->_actual_lot                   = $load_actual;
        $obj->actual_lot                    = $load_actual;
        $obj->uom                           = $uom;
        $obj->stock                         = $stock;
        $obj->_available_qty                = sprintf('%0.8f',$available_qty);
        $obj->available_qty                 = sprintf('%0.8f',$available_qty);
        $obj->_reserved_qty                 = sprintf('%0.8f',$reserved_qty);
        $obj->reserved_qty                  = sprintf('%0.8f',$reserved_qty);
        $obj->c_order_id                    = $c_order_id;
        $obj->qty_booking                   = 0;
        $obj->movement_date                 = Carbon::now()->toDateTimeString();
        $obj->new_barcode                   = 'BELUM DI PRINT';
        $obj->referral_code                 = '-1';
        $obj->sequence                      = '-1';
       
        $material_stock->last_date_used     = carbon::now();
        $material_stock->last_status        ='handover';
        $material_stock->last_user_used_id  =  auth::user()->id;
        $material_stock->save();
        
        return response()->json($obj, 200);
      
    }

    public function documentPicklist(Request $request)
    {
        $type           = $request->type;
        $c_order_id     = $request->c_order_id;
        $item_id        = $request->item_id;
        $c_bpartner_id  = $request->c_bpartner_id;
        $warehouse_id   = $request->warehouse_id;
        $q              = strtoupper($request->q);

        $lists = SummaryHandoverMaterial::select('id','handover_document_no','document_no','item_code','supplier_name','total_qty_handover','total_qty_outstanding','total_qty_supply','complete_date')
        ->where([
            ['c_order_id',$c_order_id],
            ['item_id',$item_id],
            ['c_bpartner_id',$c_bpartner_id],
            ['warehouse_id',$warehouse_id],
        ]);

        if($type == 'move_order') $lists = $lists->where('is_move_order',true);
        else if($type == 'handover') $lists = $lists->where('is_move_order',false);
        
        $lists = $lists->whereNotNull('handover_document_no')
        ->whereNull('deleted_at')
        ->where(db::raw('upper(handover_document_no)'),'LIKE',"%$q%")
        ->orderBy('handover_document_no','desc')
        ->orderBy('total_qty_outstanding','desc')
        ->limit(10)
        ->get();

        //['is_move_order',false],
        
        $date = date('Ym');
        $referral_code = MaterialMovementLine::where(DB::raw("to_char(created_at,'YYYYMM')"),$date)
        ->wherenotnull('document_no_kk')
        ->count();
        if($referral_code == null)
        {
            $sequence = 1;
        }else{
            $sequence = $referral_code+1;
        }
        $document_no_out = 'WMS-'.$date.'-'.sprintf("%03s", $sequence).'-FB';
            

        if($type ==  'move_order') return view('fabric_material_out_non_cutting._document_move_order_list',compact('lists','type','document_no_out'));
        else if($type == 'handover') return view('fabric_material_out_non_cutting._document_handover_list',compact('lists','type'));
    }

    public function removeLastUser(Request $request)
    {
        $c_order_id     = $request->c_order_id;
        $item_id        = $request->item_id;
        $warehouse_id   = $request->warehouse_id;
       
        $material_stock = MaterialStock::where([
            ['item_id',$item_id],
            ['c_order_id',$c_order_id],
            ['is_allocated',false],
            ['warehouse_id',$warehouse_id],
        ])
        ->update([
            'last_date_used'        => null
            ,'last_status'          => null
            ,'last_user_used_id'    => null
        ]);

        return response()->json(200);
    }

    public function store(Request $request)
    {
      $validator = Validator::make($request->all(), [
          'barcode_products' => 'required|not_in:[]'
      ]);

      if($validator->passes())
      {
        $_warehouse_id      = $request->warehouse_id;
        $insert_movement    = array();
        $return_print       = array();
        $source_locator     = array();
        $data_roll          = json_decode($request->barcode_products);
        // dd($data_roll);
        //$data_handover = json_decode($request->data_allocation);
        $handover = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','HANDOVER'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $move_order = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where([
                ['warehouse',$_warehouse_id],
                ['name','SUBCONT'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $date = date('Ym');
        $referral_code = MaterialMovement::where(DB::raw("to_char(created_at,'YYYYMM')"),$date)
        ->wherenotnull('document_no_kk')
        ->count();
        if($referral_code == null)
        {
            $sequence = 1;
        }else{
            $sequence = $referral_code+1;
        }
        $document_no_out = 'WMS-'.$date.'-'.sprintf("%03s", $sequence).'-FB';
       
        $array                  = array();
        $update_summary_stocks  = array();

        try 
        {
            DB::beginTransaction();
            
            foreach ($data_roll as $key => $value) 
            {
                $material_stock_id              = $value->material_stock_id;
                $summary_handover_material_id   = $value->summary_handover_material_id;
                $summary_handover_material      = SummaryHandoverMaterial::find($summary_handover_material_id);
                $locator_id                     = $value->locator_from_id;
                $document_no                    = strtoupper($value->document_no);
                $c_bpartner_id                  = $value->c_bpartner_id;
                $supplier_name                  = strtoupper($value->supplier_name);
                $c_order_id                     = $value->c_order_id;
                $no_invoice                     = $value->no_invoice;
                $no_packing_list                = isset($value->no_packing_list)? $value->no_packing_list : null;
                $item_code                      = strtoupper($value->item_code);
                $color                          = $value->color;
                $item_id                        = $value->item_id;
                $nomor_roll                     = $value->nomor_roll;
                $batch_number                   = $value->batch_number;
                $actual_lot                     = $value->actual_lot;
                $begin_width                    = $value->begin_width;
                $middle_width                   = $value->middle_width;
                $end_width                      = $value->end_width;
                $actual_length                  = $value->actual_length;
                $actual_width                   = $value->actual_width;
                $po_detail_id                   = $value->po_detail_id;
                $uom                            = $value->uom;
                $qty_handover                   = sprintf('%0.8f',$value->qty_booking);
                $reserved_qty                   = sprintf('%0.8f',$value->reserved_qty);
                $available_qty                  = sprintf('%0.8f',$value->available_qty);
                $movement_date                  = $value->movement_date;
                $handover_document_no           = $value->handover_document_no;
                $is_move_order                  = $value->is_move_order;
                $warehouse_id                   = $handover->z_column;
                // $is_subcont                     = $value->issubcont;
                $material_stock                 = MaterialStock::find($material_stock_id);
                
              

                if($is_move_order)
                {
                   
                    if($summary_handover_material )
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($summary_handover_material->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$summary_handover_material->no_kk;
                        if($summary_handover_material->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$summary_handover_material->no_bc;
                    }
                    
                    $note           = 'ROLL DIKELUARKAN KE '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper(auth::user()->name);
                    $status         = 'move-order';
                    // if($is_subcont)
                    // {
                        $destination    = $move_order->id;
                    // }else{
                    //     $destination    = null;
                    // }
                   

                    
                    // dd($destination);
                    //$receive_date   = carbon::now();
                }else
                {
                    if($summary_handover_material)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($summary_handover_material->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$summary_handover_material->no_kk;
                        if($summary_handover_material->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$summary_handover_material->no_bc;
                    }

                    $note           = 'PINDAH TANGAN NOMOR PT '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper(auth::user()->name);
                    $status         = 'out-handover';
                    $destination    = $handover->id;
                    //$receive_date   = null;
                }
            
                if($available_qty <= 0)
                {
                    $source_locator [] = $locator_id;
                    
                    $barcode_supplier   = $material_stock->barcode_supplier;
                    $sequence           = $material_stock->sequence;
                    $referral_code      = $material_stock->referral_code;
                }else
                {
                    $barcode_supplier   = $value->new_barcode;
                    $sequence           = $value->sequence;
                    $referral_code      = $value->referral_code;
                }

                $is_exists = MaterialRollHandoverFabric::where([
                    ['barcode','BELUM DI PRINT'],
                    ['c_order_id',$c_order_id],
                    ['uom',$uom],
                    ['nomor_roll',$nomor_roll],
                    ['batch_number',$batch_number],
                    ['actual_lot',$actual_lot],
                    ['actual_width',$actual_width],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['material_stock_id',$material_stock_id],
                    ['qty_handover',$qty_handover],
                    ['summary_handover_material_id',$summary_handover_material_id],
                    ['warehouse_from_id',$_warehouse_id],
                ])
                ->exists();

                if(!$is_exists && $material_stock)
                {
                    $new_material_roll_handover_fabric = MaterialRollHandoverFabric::FirstOrCreate([
                        'barcode'                       => $barcode_supplier,
                        'document_no'                   => $document_no,
                        'c_order_id'                    => $c_order_id,
                        'uom'                           => $uom,
                        'item_code'                     => $item_code,
                        'nomor_roll'                    => $nomor_roll,
                        'supplier_name'                 => $supplier_name,
                        'color'                         => $color,
                        'batch_number'                  => $batch_number,
                        'actual_lot'                    => $actual_lot,
                        'begin_width'                   => $begin_width,
                        'begin_width'                   => $middle_width,
                        'end_width'                     => $end_width,
                        'actual_width'                  => $actual_width,
                        'actual_length'                 => $actual_length,
                        'no_packing_list'               => $no_packing_list,
                        'no_invoice'                    => $no_invoice,
                        'po_detail_id'                  => $po_detail_id,
                        'item_id'                       => $item_id,
                        'material_stock_id'             => $material_stock_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'qty_handover'                  => $qty_handover,
                        'warehouse_from_id'             => $_warehouse_id,
                        'warehouse_to_id'               => ($is_move_order) ? null : $warehouse_id,
                        'sequence'                      => $sequence,
                        'referral_code'                 => $referral_code,
                        'summary_handover_material_id'  => $summary_handover_material_id,
                        'user_id'                       => auth::user()->id,
                    ]);

                    if($new_material_roll_handover_fabric->barcode == 'BELUM DI PRINT') $return_print [] = $new_material_roll_handover_fabric->id;
                    else {
                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id'     => $material_stock_id,
                            'from_location'         => $locator_id,
                            'to_destination'        => $destination,
                            'status'                => $status,
                            'uom'                   => $uom,
                            'qty'                   => $new_material_roll_handover_fabric->qty_handover,
                            'movement_date'         => $new_material_roll_handover_fabric->created_at,
                            'note'                  => $note.' (NOMOR SERI BARCODE '.$new_material_roll_handover_fabric->barcode.')',
                            'user_id'               => auth::user()->id
                        ]);
                    }

                    $insert_movement []                 = $new_material_roll_handover_fabric->id;
                    $array []                           = $summary_handover_material_id;
                    
                    $material_stock->summary_stock_fabric_id;
                    $material_stock->last_date_used     = null;
                    $material_stock->last_status        = null;
                    $material_stock->last_user_used_id  = null;
                    $material_stock->reserved_qty       = $reserved_qty;
                    $material_stock->available_qty      = $available_qty;

                    if($available_qty <=0)
                    {
                        $material_stock->locator_id     = null;
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                        $material_stock->deleted_at     = carbon::now();
                    }

                    $update_summary_stocks [] = $material_stock->summary_stock_fabric_id;

                    $material_stock->save();
                }
            }

            foreach ($array as $key => $summay_handover_fabric_id) 
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_handover_fabric('".$summay_handover_fabric_id."');"));
            }
            
            $material_roll_handovers = MaterialRollHandoverFabric::select('summary_handover_material_id',db::raw('sum(qty_handover) as total_qty_supply'))  
            ->whereIn('summary_handover_material_id',$array)
            ->groupby('summary_handover_material_id')
            ->get();

            foreach ($material_roll_handovers as $key => $material_roll_handover) 
            {
                $id                         = $material_roll_handover->summary_handover_material_id;
                $total_qty_supply           = sprintf('%0.8f',$material_roll_handover->total_qty_supply);
                $summary_handover_material  = SummaryHandoverMaterial::find($id);
                $total_qty_handover         = sprintf('%0.8f',$summary_handover_material->total_qty_handover);

                $new_total_outstanding      = $total_qty_handover - $total_qty_supply;

                if($new_total_outstanding <= 0) $summary_handover_material->complete_date = carbon::now();

                $summary_handover_material->total_qty_outstanding   = sprintf('%0.8f',$new_total_outstanding);
                $summary_handover_material->total_qty_supply        = $total_qty_supply;
                $summary_handover_material->document_no_out         = $document_no_out;
                // $summary_handover_material->is_subcont              = $is_subcont;
                $summary_handover_material->save();

            }

            foreach ($source_locator as $key => $value) 
            {
                $total_item_on_locator = MaterialStock::where('locator_id',$value)
                ->whereNull('deleted_at')
                ->count();

                $counter                = Locator::find($value);
                $counter->counter_in    = $total_item_on_locator;
                $counter->save();
            }

            $this->insertToMovementInventory($insert_movement);
            $this->updateSummaryStockFabric($update_summary_stocks);

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }

        if(count($return_print) == 0) return response()->json('no_need_print',200);
        else return response()->json($return_print,200);
        
        
      }else
      {
        return response()->json('data tidak ditemukan.',422);
      }
    }

    public function storeSummaryHandoverMaterial(Request $request)
    {
        $id             = $request->summary_handover_id;
        $c_bpartner_id  = $request->c_bpartner_id;
        $item_code      = $request->item_code;
        $document_no    = $request->document_no;
        $supplier_name  = $request->supplier_name;
        $no_pt_input    = strtoupper(trim($request->no_pt_input));
        $qty_handover   = $request->input_qty_handover;
        $is_move_order  = $request->is_move_order;
        $no_bc          = $request->no_bc;
        $c_order_id     = $request->c_order_id;
        $item_id        = $request->item_id;
        $no_kk          = $request->no_kk;
        $warehouse_id   = $request->warehouse_id;
        $no_surat_jalan = $request->no_surat_jalan;
        $tanggal_Kirim  = $request->tanggal_Kirim;
        // $is_subcont  = $request->subcont;
        if($is_move_order == 1)
        {
            try 
            {
                DB::beginTransaction();

                    $_summary_handover_material = SummaryHandoverMaterial::create([
                        'document_no'           => $document_no,
                        'supplier_name'         => $supplier_name,
                        'item_code'             => $item_code,
                        'uom'                   => 'YDS',
                        'c_bpartner_id'         => $c_bpartner_id,
                        'warehouse_id'          => $warehouse_id,
                        'total_qty_handover'    => $qty_handover,
                        'total_qty_outstanding' => $qty_handover,
                        'total_qty_supply'      => 0,
                        'handover_document_no'  => $no_pt_input,
                        'is_move_order'         => true,
                        'no_bc'                 => $no_bc,
                        'no_kk'                 => $no_kk,
                        'c_order_id'            => $c_order_id,
                        'item_id'               => $item_id,
                        'deleted_at'            => null,
                        'no_surat_jalan'        => $no_surat_jalan,
                        'tanggal_kirim'         => $tanggal_Kirim,
                    ]);

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $obj_summary_handover                               = new stdclass();
            $obj_summary_handover->summary_handover_material_id = $_summary_handover_material->id;
            $obj_summary_handover->document_no                  = $_summary_handover_material->document_no;
            $obj_summary_handover->supplier_name                = $_summary_handover_material->supplier_name;
            $obj_summary_handover->item_code                    = $_summary_handover_material->item_code;
            $obj_summary_handover->c_bpartner_id                = $_summary_handover_material->c_bpartner_id;
            $obj_summary_handover->total_qty_outstanding        = $_summary_handover_material->total_qty_outstanding;
            $obj_summary_handover->total_qty_supply             = 0;
            $obj_summary_handover->uom                          = 'YDS';
            $obj_summary_handover->is_mover_oder                = true;
            $obj_summary_handover->handover_document_no         = $_summary_handover_material->handover_document_no;
            $obj_summary_handover->c_order_id                   = $_summary_handover_material->c_order_id;
            $obj_summary_handover->item_id                      = $_summary_handover_material->item_id;
            $obj_summary_handover->warehouse_id                 = $_summary_handover_material->warehouse_id;
            // if($is_subcont == 'Y')
            // {
            //     $obj_summary_handover->issubcont                    = true;
            // }else {
            //     $obj_summary_handover->issubcont                    = false;
            // }
            
        }else
        {
            $summary_handover_material = SummaryHandoverMaterial::find($id);

            if($summary_handover_material)
            {
                $obj_summary_handover                               = new stdclass();
                $obj_summary_handover->summary_handover_material_id = $summary_handover_material->id;
                $obj_summary_handover->document_no                  = $document_no;
                $obj_summary_handover->supplier_name                = $supplier_name;
                $obj_summary_handover->item_code                    = $item_code;
                $obj_summary_handover->c_bpartner_id                = $c_bpartner_id;
                $obj_summary_handover->total_qty_outstanding        = sprintf('%0.8f',$summary_handover_material->total_qty_outstanding);
                $obj_summary_handover->total_qty_supply             = sprintf('%0.8f',$summary_handover_material->total_qty_supply);
                $obj_summary_handover->uom                          = $summary_handover_material->uom;
                $obj_summary_handover->is_move_order                = $summary_handover_material->is_move_order;
                $obj_summary_handover->handover_document_no         = $summary_handover_material->handover_document_no;
                $obj_summary_handover->c_order_id                   = $summary_handover_material->c_order_id;
                $obj_summary_handover->item_id                      = $summary_handover_material->item_id;
                $obj_summary_handover->warehouse_id                 = $summary_handover_material->warehouse_id;
            }else
            {
                $is_exists = SummaryHandoverMaterial::where([
                        ['c_order_id',$c_order_id],
                        ['item_id',$item_id],
                        ['handover_document_no',$no_pt_input],
                        ['total_qty_handover',$qty_handover],
                        ['warehouse_id',$warehouse_id],
                        ['is_move_order',false],
                ])
                ->whereNull('deleted_at')
                ->whereNull('complete_date')
                ->exists();

                if(!$is_exists)
                {
                    try 
                    {
                        DB::beginTransaction();
                        $_summary_handover_material = SummaryHandoverMaterial::FirstorCreate([
                            'document_no'           => $document_no,
                            'supplier_name'         => $supplier_name,
                            'item_id'               => $item_id,
                            'c_order_id'            => $c_order_id,
                            'item_code'             => $item_code,
                            'uom'                   => 'YDS',
                            'c_bpartner_id'         => $c_bpartner_id,
                            'warehouse_id'          => $warehouse_id,
                            'total_qty_handover'    => $qty_handover,
                            'total_qty_outstanding' => $qty_handover,
                            'total_qty_supply'      => 0,
                            'handover_document_no'  => $no_pt_input,
                            'is_move_order'         => false,
                            'no_bc'                 => $no_bc,
                            'no_kk'                 => $no_kk,
                            'deleted_at'            => null,
                            'no_surat_jalan'        => $no_surat_jalan,
                            'tanggal_kirim'         => $tanggal_Kirim,
                        ]);
                        DB::commit();
                    } catch (Exception $e) 
                    {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }

                    $obj_summary_handover                               = new stdclass();
                    $obj_summary_handover->summary_handover_material_id = $_summary_handover_material->id;
                    $obj_summary_handover->document_no                  = $_summary_handover_material->document_no;
                    $obj_summary_handover->supplier_name                = $_summary_handover_material->supplier_name;
                    $obj_summary_handover->item_code                    = $_summary_handover_material->item_code;
                    $obj_summary_handover->c_bpartner_id                = $_summary_handover_material->c_bpartner_id;
                    $obj_summary_handover->total_qty_outstanding        = $_summary_handover_material->total_qty_outstanding;
                    $obj_summary_handover->total_qty_supply             = 0;
                    $obj_summary_handover->uom                          = 'YDS';
                    $obj_summary_handover->is_move_order                = false;
                    $obj_summary_handover->handover_document_no         = $_summary_handover_material->handover_document_no;
                    $obj_summary_handover->c_order_id                   = $_summary_handover_material->c_order_id;
                    $obj_summary_handover->item_id                      = $_summary_handover_material->item_id;
                    $obj_summary_handover->warehouse_id                 = $_summary_handover_material->warehouse_id;
                }
                
            }
        }
        
       
        return response()->json($obj_summary_handover,200);
    }

    public function printBarcode(Request $request)
    {

        $list_barcodes       = json_decode($request->list_barcodes);
        $material_handovers  = MaterialRollHandoverFabric::wherein('id',$list_barcodes)->get();

        foreach ($material_handovers as $key => $value) 
        {
            
            if($value->barcode == 'BELUM DI PRINT')
            {

                $_warehouse_id          = $value->summaryHandoverMaterial->warehouse_id;
                $handover_document_no   = $value->summaryHandoverMaterial->handover_document_no;

                $handover       = Locator::with('area')
                ->whereHas('area',function ($query) use ($_warehouse_id)
                {
                    $query->where([
                        ['warehouse',$_warehouse_id],
                        ['name','HANDOVER'],
                        ['is_destination',true]
                    ]);

                })
                ->first();

                $inventory  = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($_warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$_warehouse_id);
                })
                ->first();
                
                if($value->summaryHandoverMaterial->is_move_order)
                {
                    if($value->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($value->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$value->summaryHandoverMaterial->no_kk;
                        if($value->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$value->summaryHandoverMaterial->no_bc;
                    }
                    $note           = 'ROLL DIKELUARKAN KE '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper(auth::user()->name);
                    
                    $status         = 'move-order';
                    $destination    = null;
                }else
                {
                    if($value->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($value->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$value->summaryHandoverMaterial->no_kk;
                        if($value->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$value->summaryHandoverMaterial->no_bc;
                    }
                    $note           = 'PINDAH TANGAN NOMOR PT '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper(auth::user()->name);
                   
                    $status         = 'out-handover';
                    $destination    = $handover->id;
                }

                $get_barcode        = $this->randomCode($value->materialStock->barcode_supplier);
                $new_barcode        = $get_barcode->barcode;
                $new_referral_code  = $get_barcode->referral_code;
                $new_sequence       = $get_barcode->sequence;
                
                $value->barcode         = $new_barcode;
                $value->referral_code   = $new_referral_code;
                $value->sequence        = $new_sequence;

                MovementStockHistory::FirstOrCreate([
                    'material_stock_id'     => $value->material_stock_id,
                    'from_location'         => $inventory->id,
                    'to_destination'        => $destination,
                    'status'                => $status,
                    'uom'                   => $value->uom,
                    'qty'                   => $value->qty_handover,
                    'movement_date'         => $value->created_at,
                    'note'                  => $note.' (NOMOR SERI BARCODE '.$new_barcode.')',
                    'user_id'               => auth::user()->id
                ]);

                $value->save();
            }
        }

        
        //return response()->json($material_handover);
        return view('fabric_material_out_non_cutting.print_barcode',compact('material_handovers'));

    }

    static function insertToMovementInventory($insert_movements)
    {
        try
        {
            $data = MaterialRollHandoverFabric::whereIn('id',$insert_movements)->get();

            $date = date('Ym');
            $referral_code = MaterialMovement::where(DB::raw("to_char(created_at,'YYYYMM')"),$date)
            ->wherenotnull('document_no_kk')
            ->count();
            if($referral_code == null)
            {
                $sequence = 1;
            }else{
                $sequence = $referral_code+1;
            }
            $document_no_out = 'WMS-'.$date.'-'.sprintf("%03s", $sequence).'-FB';

            foreach ($data as $key => $datum) 
            {
                $warehouse_id           = $datum->warehouse_from_id;
                $material_stock_id      = $datum->material_stock_id;
                $created_at             = $datum->created_at;
                $is_move_order          = $datum->summaryHandoverMaterial->is_move_order;
                // $is_subcont          = $datum->summaryHandoverMaterial->is_subcont;
                $handover_document_no   = $datum->summaryHandoverMaterial->handover_document_no;
                $qty_handover           = sprintf('%0.8f',$datum->qty_handover);
                $user_id                = $datum->user_id;
                
                if($is_move_order)
                {
                    if($datum->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($datum->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$datum->summaryHandoverMaterial->no_kk;
                        if($datum->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$datum->summaryHandoverMaterial->no_bc;
                    }
                    
                    $note           = 'PROSES INTEGRASI UNTUK MOVE ORDER, ROLL DIKELUARKAN KE '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper($datum->user->name);
                }else
                {
                    if($datum->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($datum->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$datum->summaryHandoverMaterial->no_kk;
                        if($datum->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$datum->summaryHandoverMaterial->no_bc;
                    }

                    $note           = 'PROSES INTEGRASI UNTUK PINDAH TANGAN, PINDAH TANGAN NOMOR PT '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper($datum->user->name);
                }

                $inventory_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $subcont_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','SUBCONT'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','SUBCONT');
                })
                ->first();
                // dd($subcont_erp);
    
                $material_stock     = MaterialStock::find($material_stock_id);
                $po_detail_id       = $material_stock->po_detail_id;
                $item_code          = $material_stock->item_code;
                $item_id            = $material_stock->item_id;
                $c_bpartner_id      = $material_stock->c_bpartner_id;
                $supplier_name      = $material_stock->supplier_name;
                $document_no        = $material_stock->document_no;
                $nomor_roll         = $material_stock->nomor_roll;
                $uom                = $material_stock->uom;

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $c_order_id             = 'FREE STOCK';
                    $no_packing_list        = '-';
                    $no_invoice             = '-';
                    $c_orderline_id         = '-';
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();

                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : '-');
                }

            if(!$is_move_order)
            {

                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$inventory_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','integration-to-inventory-erp'],
                    ])
                    ->first();

                if(!$is_movement_exists)
                {
                    $material_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     =>$inventory_erp->area->erp_id,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $created_at,
                        'updated_at'            => $created_at,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_movement_exists->updated_at = $created_at;
                    $is_movement_exists->save();

                    $material_movement_id = $is_movement_exists->id;
                }

                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['material_stock_id',$material_stock_id],
                    ['qty_movement',-1*$qty_handover],
                    ['date_movement',$created_at],
                    ['is_integrate',false],
                    ['is_active',true],
                ])
                ->exists();

                if(!$is_material_movement_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_stock_id'             => $material_stock_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => 1,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => -1*$qty_handover,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => $nomor_roll,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => $note,
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }
            }else{

                // if($is_subcont == true)
                // {
                    $is_movement_exists = MaterialMovement::where([
                        ['from_location',$inventory_erp->id],
                        ['to_destination',$subcont_erp->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$subcont_erp->area->erp_id],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status','out-subcont'],
                        ['document_no_out', $document_no_out],
                        ['document_no_kk',$datum->summaryHandoverMaterial->no_kk],
                        ['document_type', 'subcont']
                    ])
                    ->first();
    
                    if(!$is_movement_exists)
                    {
                        $material_movement = MaterialMovement::firstOrCreate([
                            'from_location'         => $inventory_erp->id,
                            'to_destination'        => $subcont_erp->id,
                            'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                            'to_locator_erp_id'     => $subcont_erp->area->erp_id,
                            'is_integrate'          => false,
                            'is_active'             => true,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'status'                => 'out-subcont',
                            'created_at'            => $created_at,
                            'updated_at'            => $created_at,
                            'document_no_out'       => $document_no_out,
                            'document_no_kk'        => $datum->summaryHandoverMaterial->no_kk,
                            'document_type'         => 'subcont'
                        ]);
    
                        $material_movement_id = $material_movement->id;
                    }else
                    {
                        $is_movement_exists->updated_at = $created_at;
                        $is_movement_exists->save();
    
                        $material_movement_id = $is_movement_exists->id;
                    }

                // }else {
                //     $is_movement_exists = MaterialMovement::where([
                //         ['from_location',$inventory_erp->id],
                //         ['to_destination',$inventory_erp->id],
                //         ['from_locator_erp_id',$inventory_erp->area->erp_id],
                //         ['to_locator_erp_id',$inventory_erp->area->erp_id],
                //         ['is_integrate',false],
                //         ['is_active',true],
                //         ['no_packing_list',$no_packing_list],
                //         ['no_invoice',$no_invoice],
                //         ['status','out'],
                //     ])
                //     ->first();
    
                //     if(!$is_movement_exists)
                //     {
                //         $material_movement = MaterialMovement::firstOrCreate([
                //             'from_location'         => $inventory_erp->id,
                //             'to_destination'        => $inventory_erp->id,
                //             'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                //             'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                //             'is_integrate'          => false,
                //             'is_active'             => true,
                //             'no_packing_list'       => $no_packing_list,
                //             'no_invoice'            => $no_invoice,
                //             'status'                => 'out',
                //             'created_at'            => $created_at,
                //             'updated_at'            => $created_at,
                //         ]);
    
                //         $material_movement_id = $material_movement->id;
                //     }else
                //     {
                //         $is_movement_exists->updated_at = $created_at;
                //         $is_movement_exists->save();
    
                //         $material_movement_id = $is_movement_exists->id;
                //     }
                // }

                
             

                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['material_stock_id',$material_stock_id],
                    ['qty_movement',$qty_handover],
                    ['date_movement',$created_at],
                    ['is_integrate',false],
                    ['is_active',true],
                   
                ])
                ->exists();

                if(!$is_material_movement_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_stock_id'             => $material_stock_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => 1,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $qty_handover,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => $nomor_roll,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => $note,
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }

            }

            }

        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function randomCode($barcode)
    {
        $sequence = Barcode::where('referral_code',$barcode)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $barcode.'-'.$sequence,
            'referral_code' => $barcode,
            'sequence'      => $sequence,
        ]);

        $obj                = new stdClass();
        $obj->barcode       = $barcode.'-'.$sequence;
        $obj->referral_code = $barcode;
        $obj->sequence      = $sequence;

        return $obj;
    }

    static function updateSummaryStockFabric($update_summary_stock_fabrics)
    {
        try 
        {
            DB::beginTransaction();
            
            $summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$update_summary_stock_fabrics)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($summary_stocks as $key => $summary_stock) 
            {
                $id                     = $summary_stock->summary_stock_fabric_id;
                $total_stock            = sprintf('%0.8f',$summary_stock->total_stock);
                $total_reserved_qty     = sprintf('%0.8f',$summary_stock->total_reserved);
                $total_available_qty    = sprintf('%0.8f',$summary_stock->total_available);
                

                $summary_stock_fabric_data = SummaryStockFabric::find($id);
                if($summary_stock_fabric_data)
                {
                    $summary_stock_fabric_data->stock           = $total_stock; 
                    $summary_stock_fabric_data->reserved_qty    = $total_reserved_qty; 
                    $summary_stock_fabric_data->available_qty   = $total_available_qty; 
                    $summary_stock_fabric_data->save();
                }
            
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function generateDocumentOut()
    {
        $date = date('Ym');
        $referral_code = MaterialMovementLine::where(DB::raw("to_char(created_at,'YYYYMM')"),$date)
        ->wherenotnull('document_no_kk')
        ->count();
        if($referral_code == null)
        {
            $sequence = 1;
        }else{
            $sequence = $referral_code+1;
        }
        $document_no_out = 'WMS-'.$date.'-'.sprintf("%03s", $sequence).'-FB';

        $dex = new response($document_no_out);
        return $dex;
    }
}
