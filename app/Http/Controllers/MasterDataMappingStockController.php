<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use stdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\MappingStocks;

class MasterDataMappingStockController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_mapping_stock.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $mapping_stock = MappingStocks::orderBy('created_at','desc');

            return DataTables::of($mapping_stock)
            ->addColumn('action', function($mapping_stock) 
            {
                return view('master_data_mapping_stock._action', [
                    'model'         => $mapping_stock,
                    'edit'          => route('masterDataMappingStock.edit',$mapping_stock->id),
                    'delete'        => route('masterDataMappingStock.delete', $mapping_stock->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create(Request $request)
    {
        return view('master_data_mapping_stock.create');
    }

    public function edit(Request $request,$id)
    {
        $mapping_stock = MappingStocks::find($id);
        return view('master_data_mapping_stock.edit',compact('mapping_stock'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'document_number' => 'required',
            'type_stock_erp_code' => 'required',
            'type_stock' => 'required'
        ]);

        if(MappingStocks::where('document_number',strtoupper($request->document_number))->exists())
            return response()->json(['message' => 'Data already exists.'
            ], 422);

        MappingStocks::create([
            'document_number' => strtoupper($request->document_number),
            'type_stock_erp_code' => strtoupper($request->type_stock_erp_code),
            'type_stock' =>  strtoupper($request->type_stock)
        ]);
        
        Session::flash('flag', 'success');
        return response()->json('success', 200);
    }

    public function update(Request $request,$id)
    {
        $this->validate($request, [
            'document_number' => 'required',
            'type_stock_erp_code' => 'required',
            'type_stock' => 'required'
        ]);

        if(MappingStocks::where([
            ['document_number',strtoupper($request->document_number)],
            ['id','!=',$id],
        ])->exists())
            return response()->json(['message' => 'Data already exists.'
            ], 422);

        $mapping_stock                      = MappingStocks::find($id);
        $mapping_stock->document_number     =  strtoupper($request->document_number);
        $mapping_stock->type_stock_erp_code =  strtoupper($request->type_stock_erp_code);
        $mapping_stock->type_stock          =  strtoupper($request->type_stock);
        $mapping_stock->save();
        
        Session::flash('flag', 'success_2');
        return response()->json('success', 200);
    }

    public function destroy(Request $request)
    {
        MappingStocks::where('id',$request->id)->delete();
    }
}
