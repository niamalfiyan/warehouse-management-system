<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\Barcode;
use App\Models\Criteria;
use App\Models\Temporary;
use App\Models\PurchaseItem;
use App\Models\AutoAllocation;
use App\Models\MaterialOther;
use App\Models\ReroutePoBuyer;
use App\Models\MaterialArrival;
use App\Models\MaterialBacklog;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\MasterDataReroutePoBuyerController as MasterDataReroute;
//use App\Http\Controllers\MaterialSwitchAccessoriesController as MaterialSwitch;
use App\Http\Controllers\AccessoriesMaterialOutController as MaterialOut;


class AccessoriesBarcodeMaterialPreparationController extends Controller
{
    public function index()
    {
        return view('errors.migration');
        // return view('accessories_barcode_material_preparation.index');
    }
    
    public function critriaPicklist(request $request)
    {
        $document_no    = trim(strtoupper($request->document_no));
        $item_code      = trim(strtoupper($request->item_code));
        $_warehouse_id      = $request->warehouse_id;

        $lists = MaterialArrival::select('c_bpartner_id','supplier_name','document_no','item_code','prepared_status')
        ->where(function($query){
            $query->where('prepared_status','NEED PREPARED')
            ->orwhere('prepared_status','NEED PREPARED TOP & BOTTOM');
        })
        ->where(function($query) use($_warehouse_id)
        {
            $query->where('po_buyer','<>','')
            ->where('warehouse_id',$_warehouse_id);
        });

        if($document_no) $lists = $lists->where('document_no','LIKE',"%$document_no%");
        if($item_code) $lists = $lists->where('item_code','LIKE',"%$item_code%");

        $lists = $lists->groupby('c_bpartner_id','supplier_name','document_no','item_code','prepared_status')
        ->paginate('10');

        return view('accessories_barcode_material_preparation._criteria_list')
        ->with('lists', $lists);
    }

    public function store(request $request)
    {
        $_c_bpartner_id         = trim($request->c_bpartner_id);
        $_document_no           = trim($request->document_no);
        $_item_code             = trim($request->item_code);
        $_status                = trim($request->status);
        $_warehouse             = $request->warehouse;
        $movement_date          = Carbon::now()->toDateTimeString();
        $concatenate            = '';
        $insert_cancel_order    = array();
        $insert_cancel_item     = array();

        if($_status ==  'NEED PREPARED TOP') $_status = 'NEED PREPARED TOP & BOTTOM';
        else  $_status = $_status;

        if (strpos($_document_no, "POAR") !== false) $is_poar = true;
        else $is_poar = false;
        
        $array = array();
        
        if(!$_document_no && !$_item_code) return response()->json('error',400); 
        
        if($_status == 'NEED PREPARED TOP & BOTTOM')
        {
            $material_arrivals = MaterialArrival::select('po_detail_id','item_id','c_bpartner_id','document_no','c_order_id','warehouse_id','uom','po_buyer',db::raw('sum(qty_upload) as total_receiving'))
            ->where([
                ['warehouse_id',$_warehouse],
                ['prepared_status',$_status],
                ['document_no',$_document_no],
                ['item_code',$_item_code],
                ['c_bpartner_id',$_c_bpartner_id],
            ])
            ->groupby('item_id','c_bpartner_id','document_no','c_order_id','warehouse_id','uom','po_buyer','po_detail_id')
            ->get();
        }else
        {
            $material_arrivals = MaterialArrival::select('po_detail_id','item_id','c_bpartner_id','document_no','c_order_id','warehouse_id','uom',db::raw('sum(qty_upload) as total_receiving'))
            ->where([
                ['warehouse_id',$_warehouse],
                ['prepared_status',$_status],
                ['document_no',$_document_no],
                ['item_code',$_item_code],
                ['c_bpartner_id',$_c_bpartner_id],
            ])
            ->groupby('item_id','c_bpartner_id','document_no','c_order_id','warehouse_id','uom','po_detail_id')
            ->get();
        }

        foreach ($material_arrivals as $key_arrival => $material_arrival)
        {
            $po_detail_id           = $material_arrival->po_detail_id;
            $warehouse_id           = $material_arrival->warehouse_id;
            $materail_preparations  = MaterialPreparation::where([
                ['po_detail_id',$po_detail_id],
                ['warehouse',$warehouse_id],
                ['is_reroute',false],
            ])
            ->whereIn('auto_allocation_id',function ($query)
            {
                $query->select('id')
                ->from('auto_allocations')
                ->where('is_allocation_purchase', true);
            })
            ->get();

            foreach ($materail_preparations as $key_purchase => $material_preparation)
            {
                $user = auth::user();
                
                $barcode                    = $material_preparation->barcode;
                $po_buyer                   = $material_preparation->po_buyer;
                $item_id                    = $material_preparation->item_id;
                $item_code                  = $material_preparation->item_code;
                $document_no                = $material_preparation->document_no;
                $referral_code              = $material_preparation->referral_code;
                $sequence                   = $material_preparation->sequence;
                $material_preparation_id    = $material_preparation->id;
                $job_order                  = $material_preparation->job_order;
                $uom_conversion             = $material_preparation->uom_conversion;
                $style                      = $material_preparation->style;
                $article_no                 = $material_preparation->article_no;
                $qty_conversion             = $material_preparation->qty_conversion;
                $auto_allocation_id         = $material_preparation->auto_allocation_id;

                $auto_allocation            = AutoAllocation::find($auto_allocation_id);
                $old_po_buyer               = ($auto_allocation ? $auto_allocation->old_po_buyer : null);
                
                if($old_po_buyer != $po_buyer)  $show_po_buyer   = $po_buyer.' (po buyer ini reroute dari po '.$old_po_buyer.')'; 
                else    $show_po_buyer   = $po_buyer; 

                $is_others_already_out              = $this->checkOthersIsCheckout($po_buyer); 
                
                $material_requirements = MaterialRequirement::where([
                    ['item_id',$item_id],
                    ['po_buyer',$po_buyer],
                    ['style',$style],
                    ['article_no',$article_no],
                ])
                ->first();

                $master_po_buyer  = PoBuyer::where('po_buyer',$po_buyer)->first();
                $cancel_date      = ($master_po_buyer ? $master_po_buyer->cancel_date:null);  

                if($cancel_date)
                {
                    $is_po_buyer_cancel = true;
                    $is_cancel_item     = false;
                }else
                {
                    if(!$material_requirements)
                    {
                        $is_item_exits = MaterialRequirement::where([
                            ['item_id',$item_id],
                            ['po_buyer',$po_buyer]
                        ])
                        ->exists();

                        $is_po_buyer_cancel = false;
                        $is_cancel_item     = ($is_item_exits ? false : true);
                    }else
                    {
                        $is_po_buyer_cancel = false;
                        $is_cancel_item     = false;
                    }
                }
                
                if($material_preparation->first_print_date)
                {
                    $_note = 'ALREADY PRINT ON '.$material_preparation->first_print_date->format('d/m/y H:m:s');
                    //if($check_is_reroute) $_note .= ', PO INI REROUTE. PO ASAL '.$po_buyer;
                    if($old_po_buyer != $po_buyer)  $_note   .= ', (PO BUYER INI REROUTE DARI PO '.$old_po_buyer.')'; 
                    if($is_others_already_out) $_note .= ', DAN BEBERAPA ITEM UNTUK PO INI SUDAH SUPPLAI.';
                    if($user->hasRole(['admin-ict-acc','supervisor'])) $show_selected = true;
                    else $show_selected = false;

                    $selected      = false;
                }else
                {
                    $show_selected = true;
                    $selected      = true;
                    $_note = 'READY TO PRINT';
                }
        
                if($material_requirements)
                {
                    if($material_requirements->warehouse_id)
                    {
                        $warehouse_place = $material_requirements->warehouse_id;
                        if($warehouse_place == '1000007') $_warehouse_place = '1000002';
                        else if($warehouse_place == '1000010') $_warehouse_place = '1000013';
                        else $_warehouse_place = $warehouse_id;
        
                        if($_warehouse_place != $warehouse_id) $is_handover = true;
                        else $is_handover = false;
                    } else
                    {
                        $is_handover = false;
                    }
                }
                else
                {
                    $is_handover = false;
                }


                if($is_handover) $_note .= ', PO INI BUTUH HANDOVER SILAHKAN DISCAN HANDOVER';
                if($material_preparation->is_reduce) $_note .= ', PO INI REDUCE SILAHKAN DI SCAN ADJUSTMENT';

                $obj                            = new stdClass();
                $obj->selected                  = $selected;
                $obj->show_selected             = $show_selected;
                $obj->material_preparation_id   = $material_preparation_id;
                $obj->document_no               = $document_no;
                $obj->item_code                 = $item_code;
                $obj->show_po_buyer             = $show_po_buyer;
                $obj->po_buyer                  = $po_buyer;
                $obj->style                     = $style;
                $obj->article_no                = $article_no;
                $obj->uom_conversion            = $uom_conversion;
                $obj->qty_need                  = ($material_requirements ? $material_requirements->qty_required : '0');
                $obj->qty_conversion            = $qty_conversion;
                $obj->job_order                 = $job_order;
                $obj->note                      = $_note;
                $obj->is_po_buyer_cancel        = $is_po_buyer_cancel;
                $obj->is_cancel_item            = $is_cancel_item;
                $obj->is_error                  = false;
                $obj->is_full                   = false;
                $obj->is_remaining              = false;
                $obj->is_other_buyer_checkout   = $is_others_already_out;
                $obj->is_backlog                = false;
                $obj->order_by                  = 3;
                $obj->movement_date             = $movement_date;
                $obj->is_handover               = $is_handover;
                $array[]                        = $obj;
            }
        }
        
        return response()->json($array);
    }

    public function barcode(request $request)
    {

        $list_barcodes  = json_decode($request->list_barcodes);
        $items          = MaterialPreparation::orderby('po_buyer','item_code')->whereIn('material_preparations.id',$list_barcodes)->get();
        
        return view('accessories_barcode_material_preparation.barcode', compact('items'));
    }

    public function updateBarcodeStatus(request $request)
    {
        $return_print   = '';
        
        $validator = Validator::make($request->all(), [
            'material_preparations' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $items                          = json_decode($request->material_preparations);
            $print_barcodes                 = array();
            $movement_date                  = carbon::now();
            $delete_material_preperations   = array();
            $insert_cancel_order            = array();
            $insert_cancel_item             = array();
            $_warehouse_id                  = $request->warehouse_id;


            $freestock_location = Locator::with('area')
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','FREE STOCK');
            })
            ->where('rack','PRINT BARCODE')
            ->first();

            $receiving_location = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','RECEIVING');
            })
            ->first();

            $cancel_order_destination = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where([
                    ['warehouse',$_warehouse_id],
                    ['name','CANCEL ORDER'],
                    ['is_destination',true]
                ]);
    
            })
            ->first();

            $cancel_item_destination = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where([
                    ['warehouse',$_warehouse_id],
                    ['name','CANCEL ITEM'],
                    ['is_destination',true]
                ]);
            })
            ->first();

            $missing_item_from = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where([
                    ['warehouse',$_warehouse_id],
                    ['name','MISSING ITEM'],
                    ['is_destination',false]
                ]);
    
            })
            ->first();
            
            try 
            {
                DB::beginTransaction();
                
                foreach ($items as $key => $value) 
                {
                    $selected                       = $value->selected;
                    $material_preparation_id        = $value->material_preparation_id;
                    $is_error                       = $value->is_error;
                    $is_po_buyer_cancel             = $value->is_po_buyer_cancel;
                    $is_cancel_item                 = $value->is_cancel_item;
                    $is_full                        = $value->is_full;
                    
                    if($selected == true)
                    {
                        if(!$is_error && $is_full == false)
                        {
                            if($material_preparation_id)
                            {
                                $material_preparation                   = MaterialPreparation::find($material_preparation_id);
                                if($material_preparation->first_print_date)
                                {
                                    $reprint_counter                        = $material_preparation->reprint_counter;
                                    $material_preparation->reprint_user_id  = auth::user()->id;
                                    $material_preparation->reprint_date     = carbon::now();
                                    $material_preparation->reprint_counter  = $reprint_counter+1;
                                }else
                                {
                                    if($material_preparation->barcode == 'BELUM DI PRINT')
                                    {
                                        $get_barcode                            = $this->randomCode();
                                        $barcode                                = $get_barcode->barcode;
                                        $referral_code                          = $get_barcode->referral_code;
                                        $sequence                               = $get_barcode->sequence;

                                        $auto_allocation_id                     = $material_preparation->auto_allocation_id;
                                        $auto_allocation                        = AutoAllocation::find($auto_allocation_id);
                                        $is_reroute                             = $auto_allocation->is_reroute;
                                        $old_po_buyer                           = $auto_allocation->old_po_buyer;
        
                                        if($is_reroute)
                                        {
                                            $reroute_destination = Locator::with('area') 
                                            ->whereHas('area',function ($query) use ($material_preparation)
                                            {
                                                $query->where([
                                                    ['warehouse',$material_preparation->warehouse],
                                                    ['name','REROUTE'],
                                                    ['is_destination',true]
                                                ]);
                                
                                            })
                                            ->first();
        
                                            $supplier_location = Locator::with('area')
                                            ->whereHas('area',function ($query) use ($material_preparation)
                                            {
                                                $query->where('is_destination',false);
                                                $query->where('is_active',true);
                                                $query->where('warehouse',$material_preparation->warehouse);
                                                $query->where('name','SUPPLIER');
                                            })
                                            ->first();
        
                                            $system                     = User::where([
                                                ['name','system'],
                                                ['warehouse',$material_preparation->warehouse]
                                            ])
                                            ->first();
        
                                            $is_old_exists = MaterialPreparation::where([
                                                ['material_stock_id',$material_preparation->material_stock_id],
                                                ['auto_allocation_id',$material_preparation->auto_allocation_id],
                                                ['c_order_id',$material_preparation->c_order_id],
                                                ['po_buyer',$old_po_buyer],
                                                ['item_id',$material_preparation->item_id],
                                                ['uom_conversion',$material_preparation->uom_conversion],
                                                ['warehouse',$material_preparation->warehouse],
                                                ['style',$material_preparation->style],
                                                ['article_no',$material_preparation->article_no],
                                                ['qty_conversion',$material_preparation->qty_conversion],
                                                ['is_backlog',$material_preparation->is_backlog],
                                            ]);
        
                                            if($material_preparation->po_detail_id) $is_old_exists = $is_old_exists->where('po_detail_id',$material_preparation->po_detail_id);
        
                                            $is_old_exists = $is_old_exists->exists();
        
                                            if(!$is_old_exists)
                                            {
                                                $old_material_preparation = MaterialPreparation::FirstOrCreate([
                                                    'material_stock_id'         => $material_preparation->material_stock_id,
                                                    'auto_allocation_id'        => $material_preparation->auto_allocation_id,
                                                    'po_detail_id'              => $material_preparation->po_detail_id,
                                                    'barcode'                   => 'REROUTE_'.$barcode,
                                                    'referral_code'             => $referral_code,
                                                    'sequence'                  => $sequence,
                                                    'item_id'                   => $material_preparation->item_id,
                                                    'c_order_id'                => $material_preparation->c_order_id,
                                                    'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                                    'supplier_name'             => $material_preparation->supplier_name,
                                                    'document_no'               => $material_preparation->document_no,
                                                    'po_buyer'                  => $old_po_buyer,
                                                    'uom_conversion'            => $material_preparation->uom_conversion,
                                                    'qty_conversion'            => $material_preparation->qty_conversion,
                                                    'qty_reconversion'          => $material_preparation->qty_reconversion,
                                                    'job_order'                 => $material_preparation->job_order,
                                                    'style'                     => $material_preparation->style,
                                                    '_style'                    => $material_preparation->_style,
                                                    'article_no'                => $material_preparation->article_no,
                                                    'warehouse'                 => $material_preparation->warehouse,
                                                    'item_code'                 => $material_preparation->item_code,
                                                    'item_desc'                 => $material_preparation->item_desc,
                                                    'category'                  => $material_preparation->category,
                                                    'is_backlog'                => $material_preparation->is_backlog,
                                                    'total_carton'              => 1,
                                                    'type_po'                   => 2,
                                                    'user_id'                   => $material_preparation->user_id,
                                                    'last_status_movement'      => 'reroute',
                                                    'is_reroute'                => true,
                                                    'is_need_to_be_switch'      => false,
                                                    'last_locator_id'           => $reroute_destination->id, 
                                                    'last_movement_date'        => $material_preparation->last_movement_date,
                                                    'created_at'                => $material_preparation->created_at,
                                                    'updated_at'                => $material_preparation->updated_at,
                                                    'first_print_date'          => carbon::now(),
                                                    'deleted_at'                => $material_preparation->created_at,
                                                    'last_user_movement_id'     => $material_preparation->last_user_movement_id,
                                                    'is_stock_already_created'  => $material_preparation->is_stock_already_created,
                                                    'is_reduce'                 => $material_preparation->is_reduce
                                                ]);

                                                if($old_material_preparation->po_detail_id == 'FREE STOCK' || $old_material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                                {
                                                    $no_packing_list        = '-';
                                                    $no_invoice             = '-';
                                                    $c_orderline_id         = '-';
                                                }else
                                                {
                                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                                    ->whereNull('material_roll_handover_fabric_id')
                                                    ->where('po_detail_id',$old_material_preparation->po_detail_id)
                                                    ->first();
                                                    
                                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                                }
                                                
                                                $is_old_material_movement_exists = MaterialMovement::where([
                                                    ['from_location',$supplier_location->id],
                                                    ['to_destination',$receiving_location->id],
                                                    ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                    ['po_buyer',$old_material_preparation->po_buyer],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                    ['no_packing_list',$no_packing_list],
                                                    ['no_invoice',$no_invoice],
                                                    ['status','receive'],
                                                ])
                                                ->first();

                                                if(!$is_old_material_movement_exists)
                                                {
                                                    $old_receive_material_movement = MaterialMovement::FirstOrCreate([
                                                        'from_location'         => $supplier_location->id,
                                                        'to_destination'        => $receiving_location->id,
                                                        'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                        'is_integrate'          => false,
                                                        'is_active'             => true,
                                                        'po_buyer'              => $old_material_preparation->po_buyer,
                                                        'created_at'            => $old_material_preparation->created_at,
                                                        'updated_at'            => $old_material_preparation->updated_at,
                                                        'status'                => 'receive'
                                                    ]);

                                                    $old_material_movement_id = $old_receive_material_movement->id;
                                                }else
                                                {
                                                    $is_old_material_movement_exists->updated_at = $old_material_preparation->updated_at;
                                                    $is_old_material_movement_exists->save();

                                                    $old_material_movement_id = $is_old_material_movement_exists->id;
                                                }
        
                                                $is_old_material_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                ->where([
                                                    ['material_movement_id',$old_material_movement_id],
                                                    ['material_preparation_id',$old_material_preparation->id],
                                                    ['item_id',$old_material_preparation->item_id],
                                                    ['qty_movement',$old_material_preparation->qty_conversion],
                                                    ['date_movement',$old_material_preparation->created_at],
                                                    ['warehouse_id',$old_material_preparation->warehouse],
                                                    ['is_active',true],
                                                    ['is_integrate',false],
                                                ])
                                                ->exists();

                                                if(!$is_old_material_line_exists)
                                                {
                                                    $new_old_material_movement_line = MaterialMovementLine::FirstOrCreate([
                                                        'material_movement_id'        => $old_material_movement_id,
                                                        'material_preparation_id'     => $old_material_preparation->id,
                                                        'item_id'                     => $old_material_preparation->item_id,
                                                        'item_id_source'              => $old_material_preparation->item_id_source,
                                                        'item_code'                   => $old_material_preparation->item_code,
                                                        'type_po'                     => 2,
                                                        'c_order_id'                  => $old_material_preparation->c_order_id,
                                                        'c_bpartner_id'               => $old_material_preparation->c_bpartner_id,
                                                        'supplier_name'               => $old_material_preparation->supplier_name,
                                                        'uom_movement'                => $old_material_preparation->uom_conversion,
                                                        'qty_movement'                => $old_material_preparation->qty_conversion,
                                                        'date_movement'               => $old_material_preparation->created_at,
                                                        'date_receive_on_destination' => $old_material_preparation->created_at,
                                                        'created_at'                  => $old_material_preparation->created_at,
                                                        'updated_at'                  => $old_material_preparation->created_at,
                                                        'warehouse_id'                => $old_material_preparation->warehouse,
                                                        'c_orderline_id'              => $c_orderline_id,
                                                        'document_no'                 => $old_material_preparation->document_no,
                                                        'is_active'                   => true,
                                                        'is_integrate'                => false,
                                                        'nomor_roll'                  => '-',
                                                        'note'                        => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                        'user_id'                     => $old_material_preparation->user_id,
                                                        'created_at'                  => $old_material_preparation->created_at,
                                                        'updated_at'                  => $old_material_preparation->created_at,
                                                    ]);

                                                    $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_line->id;
                                                    $old_material_preparation->save();
                                                }
                                                
                                                $reroute_date = carbon::now();
                                                $is_old_reroute_movement_exists = MaterialMovement::where([
                                                    ['from_location',$receiving_location->id],
                                                    ['to_destination',$reroute_destination->id],
                                                    ['from_locator_erp_id',$receiving_location->area->erp_id],
                                                    ['to_locator_erp_id',$reroute_destination->area->erp_id],
                                                    ['po_buyer',$old_material_preparation->po_buyer],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                    ['no_packing_list',$no_packing_list],
                                                    ['no_invoice',$no_invoice],
                                                    ['status','reroute'],
                                                ])
                                                ->first();

                                                if(!$is_old_reroute_movement_exists)
                                                {
                                                    $old_reroute_material_movement = MaterialMovement::FirstOrCreate([
                                                        'from_location'             => $receiving_location->id,
                                                        'to_destination'            => $reroute_destination->id,
                                                        'from_locator_erp_id'       => $receiving_location->area->erp_id,
                                                        'to_locator_erp_id'         => $reroute_destination->area->erp_id,
                                                        'is_integrate'              => false,
                                                        'is_active'                 => true,
                                                        'po_buyer'                  => $old_material_preparation->po_buyer,
                                                        'created_at'                => $reroute_date,
                                                        'updated_at'                => $reroute_date,
                                                        'status'                    => 'reroute',
                                                        'no_packing_list'           => $no_packing_list,
                                                        'no_invoice'                => $no_invoice,
                                                    ]);

                                                    $old_reroute_movement_id = $old_reroute_material_movement->id;
                                                }else
                                                {
                                                    $is_old_reroute_movement_exists->updated_at = $reroute_date;
                                                    $is_old_reroute_movement_exists->save();

                                                    $old_reroute_movement_id = $is_old_reroute_movement_exists->id;
                                                }
                                                
        
                                                $is_old_reroute_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                ->where([
                                                    ['material_movement_id',$old_reroute_movement_id],
                                                    ['material_preparation_id',$old_material_preparation->id],
                                                    ['item_id',$old_material_preparation->item_id],
                                                    ['warehouse_id',$old_material_preparation->warehouse],
                                                    ['qty_movement',$old_material_preparation->qty_conversion],
                                                    ['date_movement',$reroute_date],
                                                    ['is_active',true],
                                                    ['is_integrate',false],
                                                ])
                                                ->exists();

                                                if(!$is_old_reroute_movement_line_exists)
                                                {
                                                    $new_old_material_movement_line_reroute = MaterialMovementLine::FirstOrCreate([
                                                        'material_movement_id'          => $old_reroute_movement_id,
                                                        'material_preparation_id'       => $old_material_preparation->id,
                                                        'item_id'                       => $old_material_preparation->item_id,
                                                        'item_id_source'                => $old_material_preparation->item_id_source,
                                                        'item_code'                     => $old_material_preparation->item_code,
                                                        'warehouse_id'                  => $old_material_preparation->warehouse,
                                                        'c_order_id'                    => $old_material_preparation->c_order_id,
                                                        'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                        'supplier_name'                 => $old_material_preparation->supplier_name,
                                                        'c_orderline_id'                => $c_orderline_id,
                                                        'type_po'                       => 2,
                                                        'qty_movement'                  => $old_material_preparation->qty_conversion,
                                                        'date_receive_on_destination'   => $reroute_date,
                                                        'date_movement'                 => $reroute_date,
                                                        'created_at'                    => $reroute_date,
                                                        'updated_at'                    => $reroute_date,
                                                        'is_active'                     => true,
                                                        'is_integrate'                  => false,
                                                        'nomor_roll'                    => '-',
                                                        'document_no'                   => $old_material_preparation->document_no,
                                                        'note'                          => 'REROUTE KE PO BUYER '.$material_preparation->po_buyer,
                                                        'user_id'                       => $system->id,
                                                        'created_at'                    => $reroute_date,
                                                        'updated_at'                    => $reroute_date,
                                                    ]);

                                                    $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_line_reroute->id;
                                                    $old_material_preparation->save();
                                                }

                                                $old_detail_material_preparation = DetailMaterialPreparation::where('material_preparation_id',$material_preparation->id)->first();
                                                DetailMaterialPreparation::FirstOrCreate([
                                                    'material_preparation_id'   => $old_material_preparation->id,
                                                    'material_arrival_id'       => ($old_detail_material_preparation ? $old_detail_material_preparation->material_arrival_id : null),
                                                    'user_id'                   => $old_material_preparation->user_id
                                                ]);
        
                                            }
                                        }
                                        
                                        if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                                        {
                                            $no_packing_list        = '-';
                                            $no_invoice             = '-';
                                            $c_orderline_id         = '-';
                                        }else
                                        {
                                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                            ->whereNull('material_roll_handover_fabric_id')
                                            ->where('po_detail_id',$material_preparation->po_detail_id)
                                            ->first();

                                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                        }
                                            
                                        $material_preparation->last_status_movement  = 'print';
                                        $material_preparation->last_movement_date    = $movement_date;
                                        $material_preparation->last_locator_id       = $freestock_location->id;
                                        $material_preparation->barcode               = $barcode;
                                        $material_preparation->referral_code         = $referral_code;
                                        $material_preparation->sequence              = $sequence;
        
                                        $is_material_movement_exists = MaterialMovement::where([
                                            ['from_location',$receiving_location->id],
                                            ['to_destination',$freestock_location->id],
                                            ['from_locator_erp_id',$receiving_location->area->erp_id],
                                            ['to_locator_erp_id',$freestock_location->area->erp_id],
                                            ['po_buyer',$material_preparation->po_buyer],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                            ['no_packing_list',$no_packing_list],
                                            ['no_invoice',$no_invoice],
                                            ['status','print'],
                                        ])
                                        ->first();

                                        if(!$is_material_movement_exists)
                                        {
                                            $material_movement = MaterialMovement::FirstOrCreate([
                                                'from_location'             => $receiving_location->id,
                                                'to_destination'            => $freestock_location->id,
                                                'from_locator_erp_id'       => $receiving_location->area->erp_id,
                                                'to_locator_erp_id'         => $freestock_location->area->erp_id,
                                                'is_integrate'              => false,
                                                'is_active'                 => true,
                                                'po_buyer'                  => $material_preparation->po_buyer,
                                                'created_at'                => $movement_date,
                                                'updated_at'                => $movement_date,
                                                'status'                    => 'print',
                                                'no_packing_list'           => $no_packing_list,
                                                'no_invoice'                => $no_invoice,
                                            ]);

                                            $material_movement_id = $material_movement->id;
                                        }else
                                        {
                                            $is_material_movement_exists->updated_at = $movement_date;
                                            $is_material_movement_exists->save();

                                            $material_movement_id = $is_material_movement_exists->id;
                                        }

                                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                        ->where([
                                            ['material_movement_id',$material_movement_id],
                                            ['material_preparation_id',$material_preparation->id],
                                            ['item_id',$material_preparation->item_id],
                                            ['qty_movement',$material_preparation->qty_conversion],
                                            ['warehouse_id',$material_preparation->warehouse],
                                            ['date_movement',$movement_date],
                                            ['is_integrate',false],
                                            ['is_active',true],
                                        ])
                                        ->exists();

                                        if(!$is_material_movement_line_exists)
                                        {
                                            $new_material_movement_line = MaterialMovementLine::FirstOrCreate([
                                                'material_movement_id'          => $material_movement_id,
                                                'material_preparation_id'       => $material_preparation->id,
                                                'c_order_id'                    => $material_preparation->c_order_id,
                                                'document_no'                   => $material_preparation->document_no,
                                                'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                'supplier_name'                 => $material_preparation->supplier_name,
                                                'document_no'                   => $material_preparation->document_no,
                                                'nomor_roll'                    => '-',
                                                'item_id'                       => $material_preparation->item_id,
                                                'item_code'                     => $material_preparation->item_code,
                                                'type_po'                       => $material_preparation->type_po,
                                                'warehouse_id'                  => $material_preparation->warehouse,
                                                'uom_movement'                  => $material_preparation->uom_conversion,
                                                'qty_movement'                  => $material_preparation->qty_conversion,
                                                'date_movement'                 => $movement_date,
                                                'date_receive_on_destination'   => $movement_date,
                                                'created_at'                    => $movement_date,
                                                'updated_at'                    => $movement_date,
                                                'is_integrate'                  => false,
                                                'is_active'                     => true,
                                                'user_id'                       => Auth::user()->id
                                            ]);

                                            $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                        }

                                        if($is_po_buyer_cancel || $is_cancel_item)
                                        {
                                            $is_material_movement_cancel_exists = MaterialMovement::where([
                                                ['from_location',$freestock_location->id],
                                                ['to_destination',$missing_item_from->id],
                                                ['from_locator_erp_id',$freestock_location->area->erp_id],
                                                ['to_locator_erp_id',$missing_item_from->area->erp_id],
                                                ['po_buyer',$material_preparation->po_buyer],
                                                ['is_integrate',false],
                                                ['is_active',true],
                                                ['no_packing_list',$no_packing_list],
                                                ['no_invoice',$no_invoice],
                                                ['status','in'],
                                            ])
                                            ->first();

                                            if(!$is_material_movement_cancel_exists)
                                            {
                                                $material_movement_in = MaterialMovement::FirstOrCreate([
                                                    'from_location'         => $freestock_location->id,
                                                    'to_destination'        => $missing_item_from->id,
                                                    'from_locator_erp_id'   => $freestock_location->area->erp_id,
                                                    'to_locator_erp_id'     => $missing_item_from->area->erp_id,
                                                    'is_integrate'          => false,
                                                    'is_active'             => true,
                                                    'po_buyer'              => $material_preparation->po_buyer,
                                                    'status'                => 'in',
                                                    'no_packing_list'       => $no_packing_list,
                                                    'no_invoice'            => $no_invoice,
                                                    'created_at'            => $movement_date,
                                                    'updated_at'            => $movement_date,
                                                ]);

                                                $material_movement_in_id = $material_movement_in->id;
                                            }else
                                            {
                                                $is_material_movement_cancel_exists->updated_at = $movement_date;
                                                $is_material_movement_cancel_exists->save();

                                                $material_movement_in_id = $is_material_movement_cancel_exists->id;
                                            }

                                            $is_material_movement_line_in_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                            ->where([
                                                ['material_movement_id',$material_movement_in_id],
                                                ['material_preparation_id',$material_preparation->id],
                                                ['item_id',$material_preparation->item_id],
                                                ['warehouse_id',$material_preparation->warehouse],
                                                ['qty_movement',$material_preparation->qty_movement],
                                                ['date_movement',$movement_date],
                                                ['is_integrate',false],
                                                ['is_active',true],
                                            ])
                                            ->exists();

                                            if(!$is_material_movement_line_in_exists)
                                            {
                                                $new_material_movement_line_in = MaterialMovementLine::FirstOrCreate([
                                                    'material_movement_id'          => $material_movement_in_id,
                                                    'material_preparation_id'       => $material_preparation->id,
                                                    'item_id'                       => $material_preparation->item_id,
                                                    'item_id_source'                => $material_preparation->item_id_source,
                                                    'warehouse_id'                  => $material_preparation->warehouse,
                                                    'item_code'                     => $material_preparation->item_code,
                                                    'type_po'                       => $material_preparation->type_po,
                                                    'c_order_id'                    => $material_preparation->c_order_id,
                                                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                    'document_no'                   => $material_preparation->document_no,
                                                    'nomor_roll'                    => '-',
                                                    'supplier_name'                 => $material_preparation->supplier_name,
                                                    'c_orderline_id'                => $c_orderline_id,
                                                    'uom_movement'                  => $material_preparation->uom_conversion,
                                                    'qty_movement'                  => $material_preparation->qty_conversion,
                                                    'date_receive_on_destination'   => $movement_date,
                                                    'date_movement'                 => $movement_date,
                                                    'created_at'                    => $movement_date,
                                                    'updated_at'                    => $movement_date,
                                                    'is_integrate'                  => false,
                                                    'is_active'                     => true,
                                                    'user_id'                       => Auth::user()->id
                                                ]);

                                                $material_preparation->last_material_movement_line_id = $new_material_movement_line_in->id;
                                            }

                                            if($is_po_buyer_cancel)
                                            {
                                                $status_cancel          = 'cancel order';
                                                $to_destination         = $cancel_order_destination->id;
                                                $to_destination_erp_id  = $cancel_order_destination->area->erp_id;

                                                $delete_material_preperations [] = 
                                                [
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'is_from_cancel_or_reroute' => true,
                                                    'last_locator_id'           => $cancel_order_destination->id,
                                                    'last_movement_status'      => 'cancel order',
                                                    'last_movement_date'        => $movement_date,
                                                    'last_user_movement_id'     => Auth::user()->id,
                                                    'ict_log'                   => null,
                                                ];
                                                
                                                $insert_cancel_order [] = $material_preparation->id;
                                            }
                                            else if($is_cancel_item)
                                            {
                                                $status_cancel          = 'cancel item';
                                                $to_destination         = $cancel_item_destination->id;
                                                $to_destination_erp_id  = $cancel_item_destination->area->erp_id;

                                                $delete_material_preperations [] = 
                                                [
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'is_from_cancel_or_reroute' => true,
                                                    'last_locator_id'           => $cancel_item_destination->id,
                                                    'last_movement_status'      => 'cancel item',
                                                    'last_movement_date'        => $movement_date,
                                                    'last_user_movement_id'     => Auth::user()->id,
                                                    'ict_log'                   => null,
                                                ];

                                                $insert_cancel_item [] = $material_preparation->id;
                                            }

                                            $is_material_movement_out_cancel_exists = MaterialMovement::where([
                                                ['from_location',$missing_item_from->id],
                                                ['to_destination',$to_destination],
                                                ['from_locator_erp_id',$missing_item_from->area->erp_id],
                                                ['to_locator_erp_id',$to_destination_erp_id],
                                                ['po_buyer',$material_preparation->po_buyer],
                                                ['status',$status_cancel],
                                                ['is_integrate',false],
                                                ['is_active',true],
                                                ['no_packing_list',$no_packing_list],
                                                ['no_invoice',$no_invoice],
                                            ])
                                            ->first();

                                            if(!$is_material_movement_out_cancel_exists)
                                            {
                                                $material_movement_out_cancel = MaterialMovement::FirstOrCreate([
                                                    'from_location'         => $missing_item_from->id,
                                                    'to_destination'        => $to_destination,
                                                    'from_locator_erp_id'   => $missing_item_from->area->erp_id,
                                                    'to_locator_erp_id'     => $to_destination_erp_id,
                                                    'is_integrate'          => false,
                                                    'is_active'             => true,
                                                    'po_buyer'              => $material_preparation->po_buyer,
                                                    'status'                => $status_cancel,
                                                    'created_at'            => $movement_date,
                                                    'updated_at'            => $movement_date,
                                                    'no_invoice'            => $no_invoice,
                                                    'no_packing_list'       => $no_packing_list,
                                                ]);

                                                $material_movement_out_cancel_id = $material_movement_out_cancel->id;
                                            }else
                                            {
                                                $is_material_movement_out_cancel_exists->updated_at = $movement_date;
                                                $is_material_movement_out_cancel_exists->save();

                                                $material_movement_out_cancel_id = $is_material_movement_out_cancel_exists->id;
                                            }
                                           
                                            $is_material_line_cancel_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                            ->where([
                                                ['material_movement_id',$material_movement_out_cancel_id],
                                                ['material_preparation_id',$material_preparation->id],
                                                ['item_id',$material_preparation->item_id],
                                                ['warehouse_id',$material_preparation->warehouse],
                                                ['qty_movement',$material_preparation->qty_movement],
                                                ['date_movement',$material_preparation->date_movement],
                                                ['is_active',true],
                                                ['is_integrate',false],
                                            ])
                                            ->exists();
                                           
                                            if($is_material_line_cancel_exists)
                                            {
                                                $new_material_movement_cancel = MaterialMovementLine::FirstOrCreate([
                                                    'material_movement_id'          => $material_movement_out_cancel_id,
                                                    'material_preparation_id'       => $material_preparation->id,
                                                    'item_id'                       => $material_preparation->item_id,
                                                    'item_id_source'                => $material_preparation->item_id_source,
                                                    'warehouse_id'                  => $material_preparation->warehouse,
                                                    'item_code'                     => $material_preparation->item_code,
                                                    'type_po'                       => $material_preparation->type_po,
                                                    'c_order_id'                    => $material_preparation->c_order_id,
                                                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                    'document_no'                   => $material_preparation->document_no,
                                                    'nomor_roll'                    => '-',
                                                    'supplier_name'                 => $material_preparation->supplier_name,
                                                    'c_orderline_id'                => $c_orderline_id,
                                                    'uom_movement'                  => $material_preparation->uom_conversion,
                                                    'qty_movement'                  => $material_preparation->qty_conversion,
                                                    'date_receive_on_destination'   => $movement_date,
                                                    'date_movement'                 => $movement_date,
                                                    'created_at'                    => $movement_date,
                                                    'updated_at'                    => $movement_date,
                                                    'is_integrate'                  => false,
                                                    'is_active'                     => true,
                                                    'user_id'                       => Auth::user()->id
                                                ]);

                                                $material_preparation->last_materia_movement_line_id = $new_material_movement_cancel->id;
                                            }
                                        }

                                        Temporary::Create([
                                            'barcode'       => $material_preparation->po_buyer,
                                            'status'        => 'mrp',
                                            'user_id'       => Auth::user()->id,
                                            'created_at'    => Auth::user()->id,
                                            'updated_at'    => $movement_date,
        
                                        ]);
                                    }

                                    $material_preparation->first_print_date = $movement_date;
                                }
                               
                                $material_preparation->save();

                                $print_barcodes []                      = $material_preparation_id;
                            }
                        }
                    }
                }
                
                if(count($delete_material_preperations)) MaterialOut::deleteMaterialPreparation($delete_material_preperations,$_warehouse_id);
                if(count($insert_cancel_order)) MaterialOut::insertCancelOrder($insert_cancel_order);
                else if(count($insert_cancel_item)) MaterialOut::insertCancelItem($insert_cancel_item);

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json($print_barcodes,200);
        }else
        {
            return response()->json([
                'data tidak ditemukan.'
            ],422);
        }
        
    }
    
    static function randomCode()
    {
        $referral_code = '2P'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode' => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence' => $sequence,
        ]);

        $obj = new stdClass();
        $obj->barcode = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence = $sequence;
        return $obj;
    }

    static function checkOthersIsCheckout($po_buyer)
    {
        $is_exists = MaterialPreparation::where([
            ['po_buyer',$po_buyer],
            ['warehouse',auth::user()->warehouse]
        ])
        ->where(function($q){
            $q->Where('last_status_movement','out')
            ->orWhere('last_status_movement','out-handover');
        })
        ->exists();
        
        if($is_exists)  return true;
        else return false;
    }
}