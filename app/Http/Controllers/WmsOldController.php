<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\WmsOld;
use App\Models\Temporary;
use App\Models\MaterialReadyPreparation;

class WmsOldController extends Controller
{
    static function hourly_cron_insert(){
        $date_now = Carbon::now()->format('Y-m-d');
        WmsOld::where(db::raw("to_char(checkout_date,'yyyy-mm-dd')"),$date_now)->delete();
        
        $po_buyers = MaterialReadyPreparation::select('po_buyer')
        ->groupby('po_buyer')
        ->get();

        $movement_wms_olds = db::connection('wms_old')
        ->table('report1_v2')
        ->select('case as status','c_locator_id as locator','poreference as po_buyer',db::raw('
            case 
                when checkout_date is null then date
                else checkout_date
            end as movement_date')
            ,'style'
            ,'user'
            ,'componentvalue as item_code'
            ,'qtyrequired as qty_required')
        ->wherein(db::raw('trim(poreference)'),$po_buyers)
        ->where(db::raw("to_char(checkout_date,'yyyy-mm-dd')"),$date_now)
        ->wherein('case',['READY PREPARE','TELAH SUPLAI','RECEIVING'])
        ->orderBy('poreference','asc')
        ->get();

        foreach ($movement_wms_olds as $key => $value) {
            if(trim($value->locator) == '' || trim($value->locator) == null){
                $_temp = null;
            }else{
                $_temp = 'PREPARATION-'.trim($value->locator);
            }

            if(trim($value->status) == 'READY PREPARE')
                $_temp_status = 'in';
            
            if(trim($value->status) == 'TELAH SUPLAI')
                $_temp_status = 'out';

            if(trim($value->status) == 'RECEIVING')
                $_temp_status = 'receiving';
                
            WmsOld::create([
                'po_buyer'=>trim($value->po_buyer),
                'style'=>trim($value->style),
                'status'=>trim($_temp_status),
                'locator'=>trim($_temp),
                'user_name'=>trim($value->user),
                'item_code'=>trim($value->item_code),
                'item_code'=>trim($value->item_code),
                'checkout_date'=> trim($value->movement_date),
                'qty_in'=> sprintf('%0.8f', $value->qty_required),

            ]);

            Temporary::FirstorCreate([
                'barcode' => trim($value->po_buyer),
                'status' => 'mrp',
                'user_id' => 1
            ]);
        }
    }

    static function daily_cron_insert(){
        WmsOld::truncate();
        
        $po_buyers = MaterialReadyPreparation::select('po_buyer')
        ->groupby('po_buyer')
        ->get();

        
        $movement_wms_olds = db::connection('wms_old')
        ->table('report1_v2')
        ->select('case as status','c_locator_id as locator','poreference as po_buyer',db::raw('
            case 
                when checkout_date is null then date
                else checkout_date
            end as movement_date')
            ,'style'
            ,'user'
            ,'componentvalue as item_code'
            ,'qtyrequired as qty_required')
        ->wherein(db::raw('trim(poreference)'),$po_buyers)
        ->wherein('case',['READY PREPARE','TELAH SUPLAI','RECEIVING'])
        ->orderBy('poreference','asc')
        ->get();

        foreach ($movement_wms_olds as $key => $value) {
            if(trim($value->locator) == '' || trim($value->locator) == null){
                $_temp = null;
            }else{
                $_temp = 'PREPARATION-'.trim($value->locator);
            }

            if(trim($value->status) == 'READY PREPARE')
                $_temp_status = 'in';
            
            if(trim($value->status) == 'TELAH SUPLAI')
                $_temp_status = 'out';

            if(trim($value->status) == 'RECEIVING')
                $_temp_status = 'receiving';
                
            WmsOld::create([
                'po_buyer'=>trim($value->po_buyer),
                'style'=>trim($value->style),
                'status'=>trim($_temp_status),
                'locator'=>trim($_temp),
                'user_name'=>trim($value->user),
                'item_code'=>trim($value->item_code),
                'item_code'=>trim($value->item_code),
                'checkout_date'=> trim($value->movement_date),
                'qty_in'=> sprintf('%0.8f', $value->qty_required),

            ]);

            Temporary::FirstorCreate([
                'barcode' => trim($value->po_buyer),
                'status' => 'mrp',
                'user_id' => 1
            ]);
        }
    }
}
