<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\Permission;

class MasterDataPermissionController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('master_data_permission.index',compact('flag'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $permissions = Permission::orderBy('created_at','desc');

            return DataTables::of($permissions)
            ->addColumn('action', function($permissions) 
            {
                return view('master_data_permission._action', [
                    'model' => $permissions,
                    'edit' => route('masterDataPermission.edit',$permissions->id),
                    'delete' => route('masterDataPermission.delete',$permissions->id),
                ]);
            })
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_permission.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Permission::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Permission Name already exists.'
                ], 422);

        try 
        {
            DB::beginTransaction();

            Permission::FirstOrCreate([
                'name' => str_slug($request->name),
                'display_name' => $request->name,
                'description' => $request->description
            ]);

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        Session::flash('flag', 'success');
        return response()->json('success', 200);
    }

    public function edit($id)
    {
        $permission = Permission::find($id);
        return view('master_data_permission.edit',compact('permission'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3'
        ]);

        if(Permission::where('name',str_slug($request->name))->where('id','!=',$request->id)->exists())
            return response()->json(['message' => 'Permission Name already exists.'
                ], 422);

        try 
        {
            DB::beginTransaction();
            
            $permission                 = Permission::find($request->id);
            $permission->name           = str_slug($request->name);
            $permission->display_name   = $request->name;
            $permission->description    = $request->description;
            $permission->save();


            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        Session::flash('flag', 'success_2');
        return response()->json('success', 200);
    }

    public function destroy($id){
        $permission = Permission::findorFail($id)
                ->delete();
        return response()->json(200);
    }

    public function picklist(Request $request){
		$query = $request->input('q');
		$state = $request->state;
		$items = Permission::orderBy('display_name')
            ->Where('display_name','like',"%$query%")
            ->orWhere('name','like',"%$query%");


		return view('modal_lov._permission_list')
			->with('items', $items->paginate(5));
	}
}
