<?php  namespace App\Http\Controllers;

use Hash;
use DB;
use Auth;
use File;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use IntlDateFormatter;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Intervention\Image\ImageManagerStatic as Image;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\Temporary;
use App\Models\PurchaseItem;
use App\Models\HistoryAutoAllocations;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialArrival;
use App\Models\MaterialPlanningFabric;
use App\Models\DetailMaterialRequirement;
use App\Models\MaterialRequirementPerPart;
use App\Models\MaterialPlanningFabricPiping;

use App\Http\Controllers\FabricMaterialPlanningController as PlanningFabric;


class HomeController extends Controller
{

    public function accountSetting(Request $request,$id)
    {
        $obj                = new stdClass();
        $obj->id            = auth::user()->id;
        $obj->name          = auth::user()->name;
        $obj->email         = auth::user()->email;
        $obj->department    = auth::user()->department;

        if(auth::user()->warehouse =='1000013')  $obj->warehouse_name = 'Accessories Aoi 2'; 
        else if(auth::user()->warehouse =='1000002') $obj->warehouse_name = 'Accessories Aoi 1'; 
        else if(auth::user()->warehouse =='1000011') $obj->warehouse_name = 'Fabric Aoi 2'; 
        else if(auth::user()->warehouse =='1000001') $obj->warehouse_name = 'Fabric Aoi 1'; 

        return view('account_setting',compact('obj'));
    }

    public function updateAccountSetting(Request $request,$id)
    {
        $avatar = Config::get('storage.avatar');
        if (!File::exists($avatar)) File::makeDirectory($avatar, 0777, true);

        $name               = strtolower(trim($request->name));
        $email              = strtolower(trim($request->email));
        $old_password       = $request->old_password;
        $password           = $request->new_password;
        $retype_password    = $request->retype_password;
        $user               = User::find($id);

        if($email)
        {
            $is_email_exists = User::where([
                ['email',$email],
                ['id','!=',$id],
            ])
            ->whereNull('deleted_at')
            ->exists();
            
            if($is_email_exists) return response()->json(['message' => 'Email '.$email.' already used by other user'],422); 
            
        }

        $image = null;
        if($password)
        {
            if (!Hash::check($old_password, $user->password)) return response()->json('Old password is wrong',422);  
            if ($password != $retype_password)  return response()->json('Password is not match',422); 
        }

       
        if ($request->hasFile('photo')) 
        {
            if ($request->file('photo')->isValid()) 
            {
                $old_file = $avatar . '/' . $user->photo;
                
                if(File::exists($avatar)) File::delete($old_file);
                
                $file = $request->file('photo');
                $now = Carbon::now()->format('u');
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $image = $filename . '_' . $now . '.' . $file->getClientOriginalExtension();

                //Resize Function
                $image_resize = Image::make($file->getRealPath());
                $height = $image_resize->height();
                $width = $image_resize->width();

                $newWidth = 80;
                $newHeight = 80;
                $image_resize->resize($newWidth, $newHeight);
                $image_resize->save($avatar.'/'.$image);
            }
        }

        if ($password) $user->password = bcrypt($password);
        if ($image) $user->photo = $image;

        $user->name     = $name;
        $user->email    = $email;
        $user->save();

        return response()->json(200); 
    }

    public function showAvatar($filename)
    {
        $path = Config::get('storage.avatar');
        $resp = response()->download($path . '/' . $filename);
        $resp->headers->set('Content-Disposition', 'inline');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function MRP(Request $request, Builder $htmlBuilder){
        $po_buyer = trim($request->po_buyer);
        if($request->ajax()){ 
            $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
				'".$po_buyer."'
				);"
			));;
            
            return DataTables::of($mrp)
            ->editColumn('qty_need',function ($mrp){
                return number_format($mrp->qty_need, 4, '.', ',');
            })
            ->editColumn('qty_available',function ($mrp){
                return number_format($mrp->qty_available, 4, '.', ',');
            })
             ->EditColumn('last_status_movement',function ($mrp){
                if($mrp->last_status_movement == 'receiving') return '<span class="label label-info">BARANG DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'not receive') return '<span class="label label-default">BARANG BELUM DITERIMA DIGUDANG</span>';
                elseif($mrp->last_status_movement == 'in') return '<span class="label label-info">MATERIAL SIAP SUPLAI</span>';
                elseif($mrp->last_status_movement == 'expanse') return '<span class="label label-warning">EXPENSE</span>';
                elseif($mrp->last_status_movement == 'out') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI</span>';
                elseif($mrp->last_status_movement == 'out-handover') return '<span class="label label-success">MATERIAL PINDAH TANGAN</span>';
                elseif($mrp->last_status_movement == 'out-subcont') return '<span class="label label-success">MATERIAL SUDAH DISUPLAI KE SUBCONT</span>';
                elseif($mrp->last_status_movement == 'reject') return '<span class="label label-danger">REJECT</span>';
                elseif($mrp->last_status_movement == 'change') return '<span class="label" style="background-color:black">MATERIAL PINDAH LOKASI RAK</span>';
                elseif($mrp->last_status_movement == 'hold') return '<span class="label" style="background-color:#e6e600">MATERIAL DITAHAN QC UNTUK PENGECEKAN</span>';
                elseif($mrp->last_status_movement == 'adjustment' || $mrp->last_status_movement == 'adjusment') return '<span class="label" style="background-color:#838606">PENYESUAIAN QTY</span>';
                elseif($mrp->last_status_movement == 'print') return '<span class="label" style="background-color:#cc00ff">PRINT BARCODE</span>';
                elseif($mrp->last_status_movement == 'reroute') return '<span class="label" style="background-color:#ff6666">REROUTE</span>';
                elseif($mrp->last_status_movement == 'cancel item') return '<span class="label" style="background-color:#ff6666">CANCEL ITEM</span>';
                elseif($mrp->last_status_movement == 'cancel order') return '<span class="label" style="background-color:#ff6666">CANCEL ORDER</span>';
                elseif($mrp->last_status_movement == 'check') return '<span class="label" style="background-color:#ffff00;color:black">MATERIAL DALAM PENGECEKAN QC</span>';
                elseif($mrp->last_status_movement == 'out-cutting') return '<span class="label" style="background-color:#e600e6">MATERIAL SUDAH DISUPLAI KE CUTTING</span>';    
                elseif($mrp->last_status_movement == 'relax') return '<span class="label" style="background-color:#e65c00">RELAX CUTTING</span>';   
                elseif($mrp->last_status_movement == 'cancel backlog') return '<span class="label" style="background-color:#b30000">CANCEL BACKLOG</span>'; 
                else return '<span class="label">'.$mrp->last_status_movement.'</span>';   
            })
            ->setRowAttr([
                'style' => function($mrp) use($po_buyer){
                    $check_po_buyer_is_cancel = PoBuyer::where('po_buyer',$po_buyer)
                    ->whereNotNull('cancel_date')
                    ->exists();

                    if($check_po_buyer_is_cancel){
                        return  'background-color: #f44336;color:white';
                    }

                    if($mrp->warehouse == 'WAREHOUSE AOI 2'){
                        return  'background-color: #ff99ff;color:black';
                    }
                },
            ])
             ->rawColumns(['style','last_status_movement'])
            ->make(true);
        }

       $html = $htmlBuilder
        ->addColumn(['data'=>'statistical_date', 'name'=>'statistical_date', 'title'=>'TANGGAL PO DD','orderable' => false])
        ->addColumn(['data'=>'style', 'name'=>'style', 'title'=>'STYLE','orderable' => false])
        ->addColumn(['data'=>'article_no', 'name'=>'article_no', 'title'=>'ARTICLE NO','orderable' => false])
        ->addColumn(['data'=>'po_buyer', 'name'=>'po_buyer', 'title'=>'NO. PO BUYER','orderable' => false])
        ->addColumn(['data'=>'category', 'name'=>'category', 'title'=>'KATEGORI ITEM','orderable' => false])
        ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM','orderable' => false])
        ->addColumn(['data'=>'qty_need', 'name'=>'qty_need', 'title'=>'QTY NEED','orderable' => false])
        ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM','orderable' => false])
        ->addColumn(['data'=>'last_status_movement', 'name'=>'last_status_movement', 'title'=>'STATUS TERAKHIR','orderable' => false])
        ->addColumn(['data'=>'warehouse', 'name'=>'warehouse', 'title'=>'WAREHOUSE','orderable' => false])
        ->addColumn(['data'=>'last_locator_code', 'name'=>'last_locator_code', 'title'=>'LOKASI TERAKHIR','orderable' => false])
        ->addColumn(['data'=>'last_movement_date', 'name'=>'last_movement_date', 'title'=>'TANGGAL PERPINDAHAN TERAKHIR','orderable' => false])
        ->addColumn(['data'=>'last_user_movement_name', 'name'=>'last_user_movement_name', 'title'=>'USER YANG MELAKUKAN PERPINDAHAN TERAKHIR','orderable' => false])
        ->addColumn(['data'=>'qty_available', 'name'=>'qty_available', 'title'=>'QTY AVAILABLE','orderable' => false])
        ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'PO SUPPLIER','orderable' => false])
        ->addColumn(['data'=>'remark', 'name'=>'remark', 'title'=>'REMARK','orderable' => false])
        ->addColumn(['data'=>'system_log', 'name'=>'system_log', 'title'=>'SYSTEM LOG','orderable' => false]);


        return view('home.mrp')
        ->with(compact('html','po_buyer'));
    }

    public function exportMrpReport(Request $request){
        $po_buyer = trim($request->_po_buyer);
       
        if(!$po_buyer)
            return response()->json(400);
        
        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            );"
        ));;
       
        return Excel::create('LAPORAN_MRP',function ($excel) use($mrp){
            $excel->sheet('main', function($sheet) use($mrp) {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','TANGGAL PO DD');
                $sheet->setCellValue('C1','STYLE');
                $sheet->setCellValue('D1','ARTICLE NO');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','KATEGORI ITEM');
                $sheet->setCellValue('G1','KODE ITEM');
                $sheet->setCellValue('H1','QTY NEED');
                $sheet->setCellValue('I1','UOM');
                $sheet->setCellValue('J1','STATUS TERAKHIR');
                $sheet->setCellValue('K1','WAREHOUSE');
                $sheet->setCellValue('L1','LOKASI TERAKHIR');
                $sheet->setCellValue('M1','TANGGAL PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('N1','USER YANG MELAKUKAN PERPINDAHAN TERAKHIR');
                $sheet->setCellValue('O1','QTY AVAILABLE');
                $sheet->setCellValue('P1','PO SUPPLIER');
                $sheet->setCellValue('Q1','REMARK');
                $sheet->setCellValue('R1','SYSTEM LOG');
               
                $index = 0;
                foreach ($mrp as $key => $value) {
                    $row = $index + 2;
                    $index++;
                    
                    $sheet->setCellValue('A'.$row, $index);
                    $sheet->setCellValue('B'.$row, $value->statistical_date);
                    $sheet->setCellValue('C'.$row, $value->style);
                    $sheet->setCellValue('D'.$row, $value->article_no);
                    $sheet->setCellValue('E'.$row, $value->po_buyer);
                    $sheet->setCellValue('F'.$row, $value->category);
                    $sheet->setCellValue('G'.$row, $value->item_code);
                    $sheet->setCellValue('H'.$row, $value->qty_need);
                    $sheet->setCellValue('I'.$row, $value->uom);
                    $sheet->setCellValue('J'.$row, $value->last_status_movement);
                    $sheet->setCellValue('K'.$row, $value->warehouse);
                    $sheet->setCellValue('L'.$row, $value->last_locator_code);
                    $sheet->setCellValue('M'.$row, $value->last_movement_date);
                    $sheet->setCellValue('N'.$row, $value->last_user_movement_name);
                    $sheet->setCellValue('O'.$row, $value->qty_available);
                    $sheet->setCellValue('P'.$row, $value->document_no);
                    $sheet->setCellValue('Q'.$row, $value->remark);
                    $sheet->setCellValue('R'.$row, $value->system_log);
                }
            });
            
        })
        ->export('csv');
    }

    public function printMrpReport(Request $request){
        $po_buyer = trim($request->_print_po_buyer);
       
        if(!$po_buyer)
            return response()->json('silahkan masukan po buyer terlebih dahulu');
        
        $mrp = DB::select(db::raw("SELECT * FROM get_mrp_cte(
            '".$po_buyer."'
            );"
        ));;
       

        return view('report.mrp_printpreview', [
            'mrp' => $mrp
        ]);
    }
   
    public function exportAllMrpReport(){
        $filename = 'Report_MRP';
        $file = Config::get('storage.mrp') . '/' . e($filename).'.csv';

        if(!file_exists($file))
            return 'file yang anda cari tidak ditemukan';  
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function printall(){
        $items = MaterialPreparation::select('material_arrivals.supplier_name','material_preparations.po_buyer','material_preparations.job_order','material_preparations.uom_conversion','material_preparations.style',
            'material_preparations.barcode', 'material_preparations.item_code','material_preparations.item_desc','material_arrivals.document_no','material_preparations.total_carton',
            'material_preparations.article_no','material_preparations.qty_conversion',db::raw('coalesce(uom_conversions.multiplyrate,1) as multiplyrate'),db::raw('case when uom_conversions.uom_from is not null then uom_conversions.uom_from else material_preparations.uom_conversion end as uom_reconversion')
        )
        ->leftjoin('detail_material_preparations','detail_material_preparations.material_preparation_id','material_preparations.id')
        ->leftjoin('material_arrivals','material_arrivals.id','detail_material_preparations.material_arrival_id')
        ->leftjoin('uom_conversions',[
            ['uom_conversions.item_code','material_preparations.item_code'],
            ['uom_conversions.category','material_preparations.category'],
            ['uom_conversions.uom_to','matexrial_preparations.uom_conversion']
        ])
        ->whereRaw('
            to_char(material_preparations.created_at,\'yyyymmdd\') = \'20180713\'
            and material_preparations.barcode like \'2%\'
            and material_preparations.warehouse = \'1000013\'
        ')
        ->groupby(
            'material_preparations.po_buyer','material_preparations.job_order','material_preparations.uom_conversion','material_preparations.style',
            'material_preparations.barcode', 'material_preparations.item_code','material_preparations.item_desc','material_arrivals.document_no','material_preparations.total_carton',
            'material_preparations.article_no','material_preparations.qty_conversion','uom_conversions.multiplyrate','uom_conversions.uom_from',
            'material_arrivals.supplier_name'   
        )
        ->get();
        //return response()->json($items->count());
        
        return view('home.barcode_preparation', compact('items'));
    }

    public function materialRequirementIndex(Request $request, Builder $htmlBuilder){
        if ($request->ajax()) { 
             if($request->has('_po_buyer')){
                $_po_buyer = $request->_po_buyer;
                $material_requirements = MaterialRequirement::whereIn('po_buyer',$_po_buyer)
                ->orderby('statistical_date','desc');
            }else{
                $po_buyer = $request->po_buyer;
                $material_requirements = MaterialRequirement::orderby('statistical_date','desc')
                ->where('po_buyer',$po_buyer);
            }
            return DataTables::of($material_requirements)
            ->make(true);
        }

        $html = $htmlBuilder
        ->addColumn(['data'=>'lc_date', 'name'=>'lc_date', 'title'=>'LC DATE'])
        ->addColumn(['data'=>'promise_date', 'name'=>'statistical_date', 'title'=>'PROMISE DATE'])
        ->addColumn(['data'=>'statistical_date', 'name'=>'statistical_date', 'title'=>'STATISTICAL DATE'])
        ->addColumn(['data'=>'season', 'name'=>'season', 'title'=>'SEASON'])
        ->addColumn(['data'=>'po_buyer', 'name'=>'po_buyer', 'title'=>'PO BUYER'])
        ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'ITEM CODE'])
        ->addColumn(['data'=>'item_desc', 'name'=>'item_desc', 'title'=>'ITEM DESC'])
        ->addColumn(['data'=>'category', 'name'=>'category', 'title'=>'CATEGORY'])
        ->addColumn(['data'=>'job_order', 'name'=>'job_order', 'title'=>'JOB ORDER'])
        ->addColumn(['data'=>'style', 'name'=>'style', 'title'=>'STYLE'])
        ->addColumn(['data'=>'article_no', 'name'=>'article_no', 'title'=>'ARTICLE NO'])
        ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
        ->addColumn(['data'=>'warehouse_name', 'name'=>'warehouse_name', 'title'=>'WAREHOUSE PACKING'])
        ->addColumn(['data'=>'qty_required', 'name'=>'qty_required', 'title'=>'QTY','searchable'=>false]);
        
        return view('home.material_requirement',compact('html'));
    }

    public function materialRequirementIndexExport(){
         return Excel::create('upload_buyer',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
            
            });
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function materialRequirementImport(Request $request){
        $array = array();
        $obj = new stdClass();
        $po_buyer = [];
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);
          
            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $movement_date  = carbon::now()->toDateTimeString();

            if(!empty($data) && $data->count())
            {
               
                foreach ($data as $key => $value) 
                {
                    $is_cancel = PoBuyer::where('po_buyer',trim($value->po_buyer))
                    ->whereNotNull('cancel_user_id')
                    ->exists();
                    
                    if(!$is_cancel){
                        $po_buyer[] = trim($value->po_buyer);
                        $this->getPoBuyer(trim($value->po_buyer));
                        $this->getSummaryBuyerPerPart(trim($value->po_buyer));
                        $this->getSummaryBuyer(trim($value->po_buyer));
                        //$this->getPurchaseItems(trim($value->po_buyer));
                        $this->insertAllocationFabric(trim($value->po_buyer));
                        $this->getPlanningFabric(trim($value->po_buyer));
                        PlanningFabric::getAllocationFabricPerBuyer([$value->po_buyer]);

                        Temporary::Create([
                            'barcode'       => $value->po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => 1,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);
                    }
                }
            }
            
            return route('home.materialRequirementIndex',[
                '_po_buyer'=>$po_buyer
            ]);
        }
    }

    public function materialRequirementStore(Request $request)
    {
        $po_buyer       = $request->po_buyer;
        $movement_date  = carbon::now()->toDateTimeString();

         $is_cancel = PoBuyer::where('po_buyer',$po_buyer)
        ->whereNotNull('cancel_user_id')
        ->exists();

        if($is_cancel) return response()->json('PO BUYER INI CANCEL','422');


        $this->getPoBuyer($po_buyer);
        $this->getSummaryBuyerPerPart($po_buyer);
        $this->getSummaryBuyer($po_buyer);
        //$this->getPurchaseItems($po_buyer);
        $this->insertAllocationFabric($po_buyer);
        $this->getPlanningFabric(trim($po_buyer));
        PlanningFabric::getAllocationFabricPerBuyer([$po_buyer]);

        Temporary::Create([
            'barcode'       => $po_buyer,
            'status'        => 'mrp',
            'user_id'       => 1,
            'created_at'    => $movement_date,
            'updated_at'    => $movement_date,
        ]);
        return response()->json($po_buyer);
    }

    public function soapInternalUse()
    {
        try {
            $movement_date = carbon::now()->toDateTimeString();
           

            /*$data   = db::select(db::raw("
                select material_movements.id as material_movement_id,material_movement_lines.id as material_movement_line_id,'1000070' as from_locator_erp_id,'1000023' as to_locator_erp_id,item_id,item_code,material_movement_lines.qty_movement as qty,nomor_roll
                from material_movements 
                join material_movement_lines on material_movement_lines.material_movement_id = material_movements.id
                where material_movement_lines.id in ('00a2cf70-2ec3-11eb-891b-3b9edc1b10e5',
                '00bb0910-2df7-11eb-bd6e-850a1b01337d',
                '0149a910-2eb9-11eb-94c8-c9bfbc52fb95',
                '01c81de0-2ec3-11eb-a0a8-11c5518beddd',
                '029fc510-1297-11eb-ab8c-032072f5bc32',
                '030e9720-2ec9-11eb-9dfb-57d73c568a5a',
                '03429f70-2ec3-11eb-9776-75d256d0a09e',
                '03ba3d40-2ec3-11eb-b4a3-01a5d480ad63',
                '04730c60-2ec3-11eb-b731-5f970c82cecd',
                '04981f60-1372-11eb-a19d-af74ca363a4a')
            "));*/

            $item_1                                 = new stdClass();
            $item_1->material_movement_line_id      = carbon::now()->format('u').'1';
            $item_1->item_id                        = '1000116';
            $item_1->qty                            = 410;

            $item_2                                 = new stdClass();
            $item_2->material_movement_line_id      = carbon::now()->format('u').'2';
            $item_2->item_id                        = '1002282';
            $item_2->qty                            = 90;
            
            $item_3                                 = new stdClass();
            $item_3->material_movement_line_id      = carbon::now()->format('u').'3';
            $item_3->item_id                        = '1002322';
            $item_3->qty                            = 60;

            $item_4                                 = new stdClass();
            $item_4->material_movement_line_id      = carbon::now()->format('u').'4';
            $item_4->item_id                        = '1049465';
            $item_4->qty                            = 410;
            
            $item_5                                 = new stdClass();
            $item_5->material_movement_line_id      = carbon::now()->format('u').'5';
            $item_5->item_id                        = '1049526';
            $item_5->qty                            = 220;

            $item_6                                 = new stdClass();
            $item_6->material_movement_line_id      = carbon::now()->format('u').'6';
            $item_6->item_id                        = '1049549';
            $item_6->qty                            = 240;

            $item_7                                 = new stdClass();
            $item_7->material_movement_line_id      = carbon::now()->format('u').'7';
            $item_7->item_id                        = '1049561';
            $item_7->qty                            = 210;

            $data = array(
                $item_1,
                $item_2,
                $item_3,
                $item_4,
                $item_5,
                $item_6,
                $item_7,
            );

            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">';
            $xml .= '<soapenv:Header/>';
            $xml .= '<soapenv:Body>';
            $xml .= '<_0:compositeOperation>';
            $xml .= '<_0:CompositeRequest>';

            $xml .= '<_0:ADLoginRequest>';
            $xml .= '<_0:user>AOIAdmin</_0:user>';
            $xml .= '<_0:pass>aoiadmin</_0:pass>';
            $xml .= '<_0:lang>EN</_0:lang>';
            $xml .= '<_0:ClientID>1000000</_0:ClientID>';
            $xml .= '<_0:RoleID>1000006</_0:RoleID>';
            $xml .= '<_0:OrgID>1000003</_0:OrgID>';
            $xml .= '<_0:WarehouseID>1000026</_0:WarehouseID>';
            $xml .= '<_0:stage>0</_0:stage>';
            $xml .= '</_0:ADLoginRequest>';
            $xml .= '<_0:serviceType>composite_internaluse</_0:serviceType>';
            $xml .= '<_0:operations>';
           
            // INSERT HEADER
            $xml .= '<_0:operation preCommit="false" postCommit="false">';
            $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
            $xml .= '<_0:ModelCRUD>';
            $xml .= '<_0:serviceType>internaluse_inventory</_0:serviceType>';
            $xml .= '<_0:DataRow>';
            $xml .= '<_0:field column="M_Warehouse_ID">';
            $xml .= '<_0:val>1000026</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="MovementDate">';
            $xml .= '<_0:val>'.$movement_date.'</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="C_DocType_ID">';
            $xml .= '<_0:val>1000167</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="Comments">';
            $xml .= '<_0:val>Testing header WMS</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="Description">';
            $xml .= '<_0:val>manual 3</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '</_0:DataRow>';
            $xml .= '</_0:ModelCRUD>';
            $xml .= '</_0:operation>';
            // END OF INSERT HEADER

            foreach ($data as $key => $value) 
            {
                // INSERT LINE
                $xml .= '<_0:operation preCommit="false" postCommit="false">';
                $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
                $xml .= '<_0:ModelCRUD>';
                $xml .= '<_0:serviceType>internaluseline_inventory</_0:serviceType>';
                $xml .= '<_0:DataRow>';
                $xml .= '<_0:field column="M_Product_ID">';
                $xml .= '<_0:val>'.$value->item_id.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="M_Inventory_ID">';
                $xml .= '<_0:val>@M_Inventory.M_Inventory_ID</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="M_Locator_ID">';
                $xml .= '<_0:val>1000006</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="QtyInternalUse">';
                $xml .= '<_0:val>'.-1*$value->qty.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="C_Charge_ID">';
                $xml .= '<_0:val>1000001</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="AD_Org_ID">';
                $xml .= '<_0:val>1000003</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="Comments">';
                $xml .= '<_0:val>Testing line WMS</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="Description">';
                $xml .= '<_0:val>'.$value->material_movement_line_id.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '</_0:DataRow>';
                $xml .= '</_0:ModelCRUD>';
                $xml .= '</_0:operation>';
                // END OF INSERT HEADER
            }
            


            //INSERT DOC STATUS 
            $xml .= '<_0:operation preCommit="false" postCommit="false">';
            $xml .= '<_0:TargetPort>setDocAction</_0:TargetPort>';
            $xml .= '<_0:ModelSetDocAction>';
            $xml .= '<_0:serviceType>prepare_internaluse</_0:serviceType>';
            $xml .= '<_0:tableName>M_Inventory</_0:tableName>';
            $xml .= '<_0:recordIDVariable>@M_Inventory.M_Inventory_ID</_0:recordIDVariable>';
            $xml .= '<_0:docAction>CO</_0:docAction>';
            $xml .= '</_0:ModelSetDocAction>';
            $xml .= '</_0:operation>';
            //END OF DOC STATUS

            $xml .= '</_0:operations>';
            $xml .= '</_0:CompositeRequest>';
            $xml .= '</_0:compositeOperation>';
            $xml .= '</soapenv:Body>';
            $xml .= '</soapenv:Envelope>';
        
            //return response($xml, 200)->header('Content-Type', 'application/xml');
            
            $headers = array(
                "POST /package/package_1.3/packageservices.asmx HTTP/1.1",
                "Host: privpakservices.schenker.nu",
                "Content-Type: application/soap+xml; charset=utf-8",
                "Content-Length: ".strlen($xml)
                ); 
            $url = 'http://bimadev.bbi-apparel.com/ADInterface/services/compositeInterface';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            $response = curl_exec($ch); 
            curl_close($ch);
           
            if(strpos($response, 'IsRolledBack="true"') !== false){
                echo "rollback!";
            } else{
                echo 'commit';
            }

            echo $response;
           // dd(is_string($response));
            //dd(is_string($response));
            //return response($response, 200)->header('Content-Type', 'application/xml');
            
        } catch (Exception $ex) {
            echo $ex;
        }
        


        //response()->xml($xml);
        //dd($xml);
        //return response($xml, 200)->header('Content-Type', 'application/xml');
    }

    public function soapMovement()
    {
        try {
            $movement_date = carbon::now()->toDateTimeString();
           

            $data   = db::select(db::raw("
                select material_movements.id as material_movement_id,material_movement_lines.id as material_movement_line_id,'1000070' as from_locator_erp_id,'1000023' as to_locator_erp_id,item_id,item_code,material_movement_lines.qty_movement as qty,nomor_roll
                from material_movements 
                join material_movement_lines on material_movement_lines.material_movement_id = material_movements.id
                where material_movement_lines.id in ('00a2cf70-2ec3-11eb-891b-3b9edc1b10e5',
                '00bb0910-2df7-11eb-bd6e-850a1b01337d',
                '0149a910-2eb9-11eb-94c8-c9bfbc52fb95',
                '01c81de0-2ec3-11eb-a0a8-11c5518beddd',
                '029fc510-1297-11eb-ab8c-032072f5bc32',
                '030e9720-2ec9-11eb-9dfb-57d73c568a5a',
                '03429f70-2ec3-11eb-9776-75d256d0a09e',
                '03ba3d40-2ec3-11eb-b4a3-01a5d480ad63',
                '04730c60-2ec3-11eb-b731-5f970c82cecd',
                '04981f60-1372-11eb-a19d-af74ca363a4a')
            "));

            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">';
            $xml .= '<soapenv:Header/>';
            $xml .= '<soapenv:Body>';
            $xml .= '<_0:compositeOperation>';
            $xml .= '<_0:CompositeRequest>';

            // user authentications
            $xml .= '<_0:ADLoginRequest>';
            $xml .= '<_0:user>AOIAdmin</_0:user>';
            $xml .= '<_0:pass>aoiadmin</_0:pass>';
            $xml .= '<_0:lang>EN</_0:lang>';
            $xml .= '<_0:ClientID>1000000</_0:ClientID>';
            $xml .= '<_0:RoleID>1000006</_0:RoleID>';
            $xml .= '<_0:OrgID>1000003</_0:OrgID>';
            $xml .= '<_0:WarehouseID>1000026</_0:WarehouseID>';
            $xml .= '<_0:stage>0</_0:stage>';
            $xml .= '</_0:ADLoginRequest>';
            // end user authentications
            $xml .= '<_0:serviceType>composite_internaluse</_0:serviceType>';

            $xml .= '<_0:operations>';

            // insert header movement
            $xml .= '<_0:operation preCommit="false" postCommit="false">';
            $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
            $xml .= '<_0:ModelCRUD>';
            $xml .= '<_0:serviceType>inventorymove_aoi</_0:serviceType>';
            $xml .= '<_0:DataRow>';
            $xml .= '<_0:field column="MovementDate">';
            $xml .= '<_0:val>'.$movement_date.'</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="C_DocType_ID">';
            $xml .= '<_0:val>1000163</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="Comments">';
            $xml .= '<_0:val>Testing header movement reject WMS</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '<_0:field column="Description">';
            $xml .= '<_0:val>6d29df10-7e15-11ea-9001-a10cf4d42f4e - test 2</_0:val>';
            $xml .= '</_0:field>';
            $xml .= '</_0:DataRow>';
            $xml .= '</_0:ModelCRUD>';
            $xml .= '</_0:operation>';
            // end header movement

            foreach ($data as $key => $value) 
            {
                // insert line movement
                $xml .= '<_0:operation preCommit="false" postCommit="false">';
                $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
                $xml .= '<_0:ModelCRUD>';
                $xml .= '<_0:serviceType>inventorymoveline_aoi</_0:serviceType>';
                $xml .= '<_0:DataRow>';
                $xml .= '<_0:field column="M_Movement_ID">';
                $xml .= '<_0:val>@M_Movement.M_Movement_ID</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="M_Product_ID">';
                $xml .= '<_0:val>'.$value->item_id.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="MovementQty">';
                $xml .= '<_0:val>'.$value->qty.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="M_Locator_ID">';
                $xml .= '<_0:val>1000006</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="M_LocatorTo_ID">';
                $xml .= '<_0:val>1000009</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="Comments">';
                $xml .= '<_0:val>Testing movement line WMS no roll '.$value->nomor_roll.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '<_0:field column="Description">';
                $xml .= '<_0:val>'.$value->material_movement_line_id.'</_0:val>';
                $xml .= '</_0:field>';
                $xml .= '</_0:DataRow>';
                $xml .= '</_0:ModelCRUD>';
                $xml .= '</_0:operation>';
                // end line movement
            }
            

            // insert doc action 
            $xml .= '<_0:operation preCommit="false" postCommit="false">';
            $xml .= '<_0:TargetPort>setDocAction</_0:TargetPort>';
            $xml .= '<_0:ModelSetDocAction>';
            $xml .= '<_0:serviceType>prepare_moveinventory_aoi</_0:serviceType>';
            $xml .= '<_0:tableName>M_Movement</_0:tableName>';
            $xml .= '<_0:recordIDVariable>@M_Movement.M_Movement_ID</_0:recordIDVariable>';
            $xml .= '<_0:docAction>CO</_0:docAction>';
            $xml .= '</_0:ModelSetDocAction>';
            $xml .= '</_0:operation>';
            // end of doc action

            $xml .= '</_0:operations>';

            $xml .= '</_0:CompositeRequest>';
            $xml .= '</_0:compositeOperation>';
            $xml .= '</soapenv:Body>';
            $xml .= '</soapenv:Envelope>';
        
            //return response($xml, 200)->header('Content-Type', 'application/xml');
            
            $headers = array(
                "POST /package/package_1.3/packageservices.asmx HTTP/1.1",
                "Host: privpakservices.schenker.nu",
                "Content-Type: application/soap+xml; charset=utf-8",
                "Content-Length: ".strlen($xml)
                ); 
            $url = 'http://bimadev.bbi-apparel.com/ADInterface/services/compositeInterface';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            $response = curl_exec($ch); 
            curl_close($ch);
           
            if(strpos($response, 'IsRolledBack="true"') !== false){
                echo "rollback!";
            } else{
                echo 'commit';
            }

            echo $response;
           // dd(is_string($response));
            //dd(is_string($response));
            //return response($response, 200)->header('Content-Type', 'application/xml');
            
        } catch (Exception $ex) {
            echo $ex;
        }
        


        //response()->xml($xml);
        //dd($xml);
        //return response($xml, 200)->header('Content-Type', 'application/xml');
    }

    public function soapCompleteMRC()
    {
        try {
            $movement_date = carbon::now()->toDateTimeString();
            $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">';
            $xml .= '<soapenv:Header/>';
            $xml .= '<soapenv:Body>';
            $xml .= '<_0:setDocAction>';
            $xml .= '<_0:ModelSetDocActionRequest>';
            $xml .= '<_0:ModelSetDocAction>';
            $xml .= '<_0:serviceType>prepare_materialreceipt_aoi</_0:serviceType>';
            $xml .= '<_0:tableName>M_InOut</_0:tableName>';
            $xml .= '<_0:recordID>1000011</_0:recordID>'; // ini mr id nya saat ini ambil dari kolom m_inout_id
            $xml .= '<_0:docAction>CO</_0:docAction>';
            $xml .= '</_0:ModelSetDocAction>';
            $xml .= '<_0:ADLoginRequest>';
            $xml .= '<_0:user>AOIAdmin</_0:user>';
            $xml .= '<_0:pass>aoiadmin</_0:pass>';
            $xml .= '<_0:lang>EN</_0:lang>';
            $xml .= '<_0:ClientID>1000000</_0:ClientID>';
            $xml .= '<_0:RoleID>1000006</_0:RoleID>';
            $xml .= '<_0:OrgID>1000003</_0:OrgID>';
            $xml .= '<_0:WarehouseID>1000053</_0:WarehouseID>';
            $xml .= '<_0:stage>0</_0:stage>';
            $xml .= '</_0:ADLoginRequest>';
            $xml .= '</_0:ModelSetDocActionRequest>';
            $xml .= '</_0:setDocAction>';
            $xml .= '</soapenv:Body>';
            $xml .= '</soapenv:Envelope>';
            
        
            //return response($xml, 200)->header('Content-Type', 'application/xml');
            
            $headers = array(
                "POST /package/package_1.3/packageservices.asmx HTTP/1.1",
                "Host: privpakservices.schenker.nu",
                "Content-Type: application/soap+xml; charset=utf-8",
                "Content-Length: ".strlen($xml)
                ); 
            $url = 'http://bimadev.bbi-apparel.com/ADInterface/services/ModelADService';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            $response = curl_exec($ch); 
            curl_close($ch);
           
            if(strpos($response, 'IsRolledBack="true"') !== false){
                echo "rollback!";
            } else{
                echo 'commit';
            }

            echo $response;
            
        } catch (Exception $ex) {
            echo $ex;
        }
        


        //response()->xml($xml);
        //dd($xml);
        //return response($xml, 200)->header('Content-Type', 'application/xml');
    }

    public function soapOutIssue()
    {
        $pp_orders = ['1000002','1000003','1000004','1000006'];

        $line_1 = new StdClass();
        $line_1->pp_order_id              = '1000002';
        $line_1->pp_order_bomline_id      = '1000005';
        $line_1->s_resource_id            = '1000000';
        $line_1->m_warehouse_id           = '1000026';
        $line_1->pp_order_workflow_id     = '1000001';
        $line_1->bom_product_id           = '1049526';
        $line_1->m_product_id             = '1012441';
        $line_1->qtyrequired              = '100';

        $line_2 = new StdClass();
        $line_2->pp_order_id              = '1000002';
        $line_2->pp_order_bomline_id      = '1000006';
        $line_2->s_resource_id            = '1000000';
        $line_2->m_warehouse_id           = '1000026';
        $line_2->pp_order_workflow_id     = '1000001';
        $line_2->bom_product_id           = '1049465';
        $line_2->m_product_id             = '1012441';
        $line_2->qtyrequired              = '200';

        $line_3 = new StdClass();
        $line_3->pp_order_id              = '1000002';
        $line_3->pp_order_bomline_id      = '1000007';
        $line_3->s_resource_id            = '1000000';
        $line_3->m_warehouse_id           = '1000026';
        $line_3->pp_order_workflow_id     = '1000001';
        $line_3->bom_product_id           = '1049561';
        $line_3->m_product_id             = '1012441';
        $line_3->qtyrequired              = '100';

        $line_4 = new StdClass();
        $line_4->pp_order_id              = '1000002';
        $line_4->pp_order_bomline_id      = '1000008';
        $line_4->s_resource_id            = '1000000';
        $line_4->m_warehouse_id           = '1000026';
        $line_4->pp_order_workflow_id     = '1000001';
        $line_4->bom_product_id           = '1049549';
        $line_4->m_product_id             = '1012441';
        $line_4->qtyrequired              = '100';

        $line_5 = new StdClass();
        $line_5->pp_order_id              = '1000002';
        $line_5->pp_order_bomline_id      = '1000009';
        $line_5->s_resource_id            = '1000000';
        $line_5->m_warehouse_id           = '1000026';
        $line_5->pp_order_workflow_id     = '1000001';
        $line_5->bom_product_id           = '1000116';
        $line_5->m_product_id             = '1012441';
        $line_5->qtyrequired              = '200';

        $line_6 = new StdClass();
        $line_6->pp_order_id              = '1000003';
        $line_6->pp_order_bomline_id      = '1000014';
        $line_6->s_resource_id            = '1000000';
        $line_6->m_warehouse_id           = '1000026';
        $line_6->pp_order_workflow_id     = '1000002';
        $line_6->bom_product_id           = '1000116';
        $line_6->m_product_id             = '1012441';
        $line_6->qtyrequired              = '10';

        $line_7 = new StdClass();
        $line_7->pp_order_id              = '1000003';
        $line_7->pp_order_bomline_id      = '1000010';
        $line_7->s_resource_id            = '1000000';
        $line_7->m_warehouse_id           = '1000026';
        $line_7->pp_order_workflow_id     = '1000002';
        $line_7->bom_product_id           = '1049526';
        $line_7->m_product_id             = '1012441';
        $line_7->qtyrequired              = '20';

        $line_8 = new StdClass();
        $line_8->pp_order_id              = '1000003';
        $line_8->pp_order_bomline_id      = '1000011';
        $line_8->s_resource_id            = '1000000';
        $line_8->m_warehouse_id           = '1000026';
        $line_8->pp_order_workflow_id     = '1000002';
        $line_8->bom_product_id           = '1049465';
        $line_8->m_product_id             = '1012441';
        $line_8->qtyrequired              = '10';

        $line_9 = new StdClass();
        $line_9->pp_order_id              = '1000003';
        $line_9->pp_order_bomline_id      = '1000012';
        $line_9->s_resource_id            = '1000000';
        $line_9->m_warehouse_id           = '1000026';
        $line_9->pp_order_workflow_id     = '1000002';
        $line_9->bom_product_id           = '1049561';
        $line_9->m_product_id             = '1012441';
        $line_9->qtyrequired              = '10';

        $line_10 = new StdClass();
        $line_10->pp_order_id              = '1000003';
        $line_10->pp_order_bomline_id      = '1000013';
        $line_10->s_resource_id            = '1000000';
        $line_10->m_warehouse_id           = '1000026';
        $line_10->pp_order_workflow_id     = '1000002';
        $line_10->bom_product_id           = '1049526';
        $line_10->m_product_id             = '1012441';
        $line_10->qtyrequired              = '40';

        $line_11 = new StdClass();
        $line_11->pp_order_id              = '1000004';
        $line_11->pp_order_bomline_id      = '1000015';
        $line_11->s_resource_id            = '1000000';
        $line_11->m_warehouse_id           = '1000026';
        $line_11->pp_order_workflow_id     = '1000002';
        $line_11->bom_product_id           = '1049526';
        $line_11->m_product_id             = '1012441';
        $line_11->qtyrequired              = '100';

        $line_12 = new StdClass();
        $line_12->pp_order_id              = '1000004';
        $line_12->pp_order_bomline_id      = '1000016';
        $line_12->s_resource_id            = '1000000';
        $line_12->m_warehouse_id           = '1000026';
        $line_12->pp_order_workflow_id     = '1000002';
        $line_12->bom_product_id           = '1049465';
        $line_12->m_product_id             = '1012441';
        $line_12->qtyrequired              = '200';

        $line_13 = new StdClass();
        $line_13->pp_order_id              = '1000004';
        $line_13->pp_order_bomline_id      = '1000017';
        $line_13->s_resource_id            = '1000000';
        $line_13->m_warehouse_id           = '1000026';
        $line_13->pp_order_workflow_id     = '1000002';
        $line_13->bom_product_id           = '1049561';
        $line_13->m_product_id             = '1012441';
        $line_13->qtyrequired              = '100';

        $line_14 = new StdClass();
        $line_14->pp_order_id              = '1000004';
        $line_14->pp_order_bomline_id      = '1000018';
        $line_14->s_resource_id            = '1000000';
        $line_14->m_warehouse_id           = '1000026';
        $line_14->pp_order_workflow_id     = '1000002';
        $line_14->bom_product_id           = '1049549';
        $line_14->m_product_id             = '1012441';
        $line_14->qtyrequired              = '100';

        $line_15 = new StdClass();
        $line_15->pp_order_id              = '1000004';
        $line_15->pp_order_bomline_id      = '1000019';
        $line_15->s_resource_id            = '1000000';
        $line_15->m_warehouse_id           = '1000026';
        $line_15->pp_order_workflow_id     = '1000002';
        $line_15->bom_product_id           = '1000116';
        $line_15->m_product_id             = '1012441';
        $line_15->qtyrequired              = '200';

        $line_16 = new StdClass();
        $line_16->pp_order_id              = '1000004';
        $line_16->pp_order_bomline_id      = '1000015';
        $line_16->s_resource_id            = '1000000';
        $line_16->m_warehouse_id           = '1000026';
        $line_16->pp_order_workflow_id     = '1000002';
        $line_16->bom_product_id           = '1049526';
        $line_16->m_product_id             = '1012441';
        $line_16->qtyrequired              = '100';

        $line_17 = new StdClass();
        $line_17->pp_order_id              = '1000004';
        $line_17->pp_order_bomline_id      = '1000016';
        $line_17->s_resource_id            = '1000000';
        $line_17->m_warehouse_id           = '1000026';
        $line_17->pp_order_workflow_id     = '1000002';
        $line_17->bom_product_id           = '1049465';
        $line_17->m_product_id             = '1012441';
        $line_17->qtyrequired              = '200';

        $line_18 = new StdClass();
        $line_18->pp_order_id              = '1000004';
        $line_18->pp_order_bomline_id      = '1000017';
        $line_18->s_resource_id            = '1000000';
        $line_18->m_warehouse_id           = '1000026';
        $line_18->pp_order_workflow_id     = '1000002';
        $line_18->bom_product_id           = '1049561';
        $line_18->m_product_id             = '1012441';
        $line_18->qtyrequired              = '100';

        $line_19 = new StdClass();
        $line_19->pp_order_id              = '1000004';
        $line_19->pp_order_bomline_id      = '1000018';
        $line_19->s_resource_id            = '1000000';
        $line_19->m_warehouse_id           = '1000026';
        $line_19->pp_order_workflow_id     = '1000002';
        $line_19->bom_product_id           = '1049549';
        $line_19->m_product_id             = '1012441';
        $line_19->qtyrequired              = '100';

        $line_20 = new StdClass();
        $line_20->pp_order_id              = '1000004';
        $line_20->pp_order_bomline_id      = '1000019';
        $line_20->s_resource_id            = '1000000';
        $line_20->m_warehouse_id           = '1000026';
        $line_20->pp_order_workflow_id     = '1000002';
        $line_20->bom_product_id           = '1000116';
        $line_20->m_product_id             = '1012441';
        $line_20->qtyrequired              = '200';

        $lines = array(
            $line_1,
            $line_2,
            $line_3,
            $line_4,
            $line_5,
            $line_6,
            $line_7,
            $line_8,
            $line_9,
            $line_10,
            $line_11,
            $line_12,
            $line_13,
            $line_14,
            $line_15,
            $line_16,
            $line_17,
            $line_18,
            $line_19,
            $line_20,
        );
        
        foreach ($pp_orders as $key => $pp_order) {
            try {
                $movement_date = carbon::now()->toDateTimeString();
                $xml = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:_0="http://idempiere.org/ADInterface/1_0">';
                $xml .= '<soapenv:Header/>';
                $xml .= '<soapenv:Body>';
                $xml .= '<_0:compositeOperation>';
                $xml .= '<_0:CompositeRequest>';
                $xml .= '<_0:ADLoginRequest>';
                $xml .= '<_0:user>AOIAdmin</_0:user>';
                $xml .= '<_0:pass>aoiadmin</_0:pass>';
                $xml .= '<_0:lang>EN</_0:lang>';
                $xml .= '<_0:ClientID>1000000</_0:ClientID>';
                $xml .= '<_0:RoleID>1000006</_0:RoleID>';
                $xml .= '<_0:OrgID>1000003</_0:OrgID>';
                $xml .= '<_0:WarehouseID>1000026</_0:WarehouseID>';
                $xml .= '<_0:stage>0</_0:stage>';
                $xml .= '</_0:ADLoginRequest>';
                $xml .= '<_0:serviceType>Composite_AOI</_0:serviceType>';
                $xml .= '<_0:operations>';
                
                //header grouping
                $xml .= '<_0:operation preCommit="false" postCommit="false">';
                $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
                $xml .= '<_0:ModelCRUD>';
                $xml .= '<_0:serviceType>create_costcollectorgroup_aoi</_0:serviceType>';
                $xml .= '<_0:DataRow>';
                $xml .= '<_0:field column="AD_Org_ID"><_0:val>1000003</_0:val></_0:field>';
                $xml .= '<_0:field column="C_DocTypeTarget_ID"><_0:val>1000188</_0:val></_0:field>';
                $xml .= '<_0:field column="C_DocType_ID"><_0:val>1000188</_0:val></_0:field>';
                $xml .= '<_0:field column="PP_Order_ID"><_0:val>'.$pp_order.'</_0:val></_0:field>';
                $xml .= '<_0:field column="DateAcct"><_0:val>'.$movement_date.'</_0:val></_0:field>'; // supply date
                $xml .= '</_0:DataRow>';
                $xml .= '</_0:ModelCRUD>';
                $xml .= '</_0:operation>';
    
                foreach ($lines as $key => $line) {
                    if($line->pp_order_id == $pp_order)
                    {
                        //Cost Collector ( ini bisa berulang )
                        $xml .= '<_0:operation preCommit="false" postCommit="false">';
                        $xml .= '<_0:TargetPort>createData</_0:TargetPort>';
                        $xml .= '<_0:ModelCRUD>';
                        $xml .= '<_0:serviceType>create_costcollector_aoi</_0:serviceType>';
                        $xml .= '<_0:DataRow>';
                        $xml .= '<_0:field column="PP_Cost_CollectorGroup_ID"><_0:val>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:val></_0:field>';
                        $xml .= '<_0:field column="AD_Org_ID"><_0:val>@PP_Cost_CollectorGroup.AD_Org_ID</_0:val></_0:field>';
                        $xml .= '<_0:field column="MovementDate"><_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val></_0:field>';
                        $xml .= '<_0:field column="DateAcct"><_0:val>@PP_Cost_CollectorGroup.DateAcct</_0:val></_0:field>';
                        $xml .= '<_0:field column="PP_Order_ID"><_0:val>@PP_Cost_CollectorGroup.PP_Order_ID</_0:val></_0:field>';
                        $xml .= '<_0:field column="C_DocTypeTarget_ID"><_0:val>1000177</_0:val></_0:field>';
                        $xml .= '<_0:field column="C_DocType_ID"><_0:val>1000177</_0:val></_0:field>';
                        $xml .= '<_0:field column="PP_Order_Workflow_ID"><_0:val>1000001</_0:val></_0:field>';
                        $xml .= '<_0:field column="S_Resource_ID"><_0:val>'.$line->s_resource_id.'</_0:val></_0:field>';
                        $xml .= '<_0:field column="M_Warehouse_ID"><_0:val>'.$line->m_warehouse_id.'</_0:val></_0:field>';
                        $xml .= '<_0:field column="CostCollectorType"><_0:val>110</_0:val></_0:field>';
                        $xml .= '<_0:field column="M_Locator_ID"><_0:val>1000006</_0:val></_0:field>';
                        $xml .= '<_0:field column="PP_Order_BOMLine_ID"><_0:val>'.$line->pp_order_bomline_id.'</_0:val></_0:field>';
                        $xml .= '<_0:field column="M_Product_ID"><_0:val>'.$line->m_product_id.'</_0:val></_0:field>';
                        $xml .= '<_0:field column="MovementQty"><_0:val>'.$line->qtyrequired.'</_0:val></_0:field>';
                        $xml .= '</_0:DataRow>';
                        $xml .= '</_0:ModelCRUD>';
                        $xml .= '</_0:operation>';
                    }
                }

                //doc action
                $xml .= '<_0:operation preCommit="false" postCommit="false">';
                $xml .= '<_0:TargetPort>setDocAction</_0:TargetPort>';
                $xml .= '<_0:ModelSetDocAction>';
                $xml .= '<_0:serviceType>docaction_cost_collectorgroup_aoi</_0:serviceType>';
                $xml .= '<_0:tableName>PP_Cost_CollectorGroup</_0:tableName>';
                $xml .= '<_0:recordIDVariable>@PP_Cost_CollectorGroup.PP_Cost_CollectorGroup_ID</_0:recordIDVariable>';
                $xml .= '<_0:docAction>PR</_0:docAction>';
                $xml .= '</_0:ModelSetDocAction>';
                $xml .= '</_0:operation>';
    
                $xml .= '</_0:operations>';
                $xml .= '</_0:CompositeRequest>';
                $xml .= '</_0:compositeOperation>';
                $xml .= '</soapenv:Body>';
                $xml .= '</soapenv:Envelope>';
                
            
                //return response($xml, 200)->header('Content-Type', 'application/xml');
                
                $headers = array(
                    "POST /package/package_1.3/packageservices.asmx HTTP/1.1",
                    "Host: privpakservices.schenker.nu",
                    "Content-Type: application/soap+xml; charset=utf-8",
                    "Content-Length: ".strlen($xml)
                    ); 
    
                $url = 'http://bimadev.bbi-apparel.com/ADInterface/services/compositeInterface';
    
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HEADER, true);
                $response = curl_exec($ch); 
                curl_close($ch);
               
                if(strpos($response, 'IsRolledBack="true"') !== false){
                    echo "rollback!";
                } else{
                    echo 'commit';
                }
    
                echo $response;
                
            } catch (Exception $ex) {
                echo $ex;
            }
        }
    }

    static function getPoBuyer($po_buyer)
    {
        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();
        $data =  DB::connection('erp') //dev_erp //erp
        ->table('wms_lc_buyers')
        ->select('kst_lcdate as lc_date',
                'poreference as po_buyer',
                'kst_season as season'
                ,'kst_statisticaldate as statistical_date'
                ,'kst_statisticaldate2 as statistical_date_2'
                ,'ordertype as order_type'
                ,'brand as brand'
                ,'datepromised as promise_date'
                ,'so_id')
        ->where('poreference',$po_buyer)
        ->first();
        
        $_po_buyer = PoBuyer::select('po_buyer')
        ->where('po_buyer',$po_buyer)
        ->groupby('po_buyer')
        ->exists();
        
        if($data){
            if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
            else $brand = $data->brand;

            if($data->order_type == 'MTF'){
                $type_stock = 'MTFC';
                $type_stock_erp_code = '4';
            }else if($data->order_type == 'SLT'){
                $type_stock = 'SLT';
                $type_stock_erp_code = '1';
            }else if($data->order_type == 'SR'){
                $type_stock = 'SR';
                $type_stock_erp_code = '3';
            }else{
                if($brand == 'NEW BALANCE'){
                    $type_stock = null;
                    $type_stock_erp_code = null;
                }else{
                    $type_stock = 'REGULAR';
                    $type_stock_erp_code = '2';
                }
            }

            if($data->statistical_date_2) $statistical_date = $data->statistical_date_2;
            else  $statistical_date = $data->statistical_date;
        }else{
            $statistical_date = null;
            $type_stock = null;
            $type_stock_erp_code = null;
        }

        if(!$_po_buyer){
            if($data){
                if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $data->brand;

                if($data->order_type == 'MTF'){
                    $type_stock = 'MTFC';
                    $type_stock_erp_code = '4';
                }else if($data->order_type == 'SLT'){
                    $type_stock = 'SLT';
                    $type_stock_erp_code = '1';
                }else if($data->order_type == 'SR'){
                    $type_stock = 'SR';
                    $type_stock_erp_code = '3';
                }else{
                    if($brand == 'NEW BALANCE'){
                        $type_stock = null;
                        $type_stock_erp_code = null;
                    }else{
                        $type_stock = 'REGULAR';
                        $type_stock_erp_code = '2';
                    }
                }
                
                PoBuyer::FirstorCreate([
                    'po_buyer' => $data->po_buyer,
                    'lc_date' => $data->lc_date,
                    'statistical_date' => $statistical_date,
                    'promise_date' => $data->promise_date,
                    'season' => $data->season,
                    'brand' => $brand,
                    'type_stock' => $type_stock,
                    'type_stock_erp_code' => $type_stock_erp_code,
                    'so_id' => $data->so_id,
                ]);
                
                if($statistical_date){
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update(['promise_date' => $statistical_date]);
                    PurchaseItem::where('po_buyer',$data->po_buyer)->update(['promise_date' => $statistical_date]);
                }else{
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update(['promise_date' => $data->promise_date]);
                    PurchaseItem::where('po_buyer',$data->po_buyer)->update(['promise_date' => $data->promise_date]);
                }
                
            }
        }else{
            if($data){
                if($data->brand == 'NEW BALANC') $brand ='NEW BALANCE';
                else $brand = $data->brand;

                if($data->order_type == 'MTF'){
                    $type_stock = 'MTFC';
                    $type_stock_erp_code = '4';
                }else if($data->order_type == 'SLT'){
                    $type_stock = 'SLT';
                    $type_stock_erp_code = '1';
                }else if($data->order_type == 'SR'){
                    $type_stock = 'SR';
                    $type_stock_erp_code = '3';
                }else{
                    if($brand == 'NEW BALANCE'){
                        $type_stock = null;
                        $type_stock_erp_code = null;
                    }else{
                        $type_stock = 'REGULAR';
                        $type_stock_erp_code = '2';
                    }
                }

                PoBuyer::where('po_buyer',$data->po_buyer)
                ->update([
                    'promise_date' => $data->promise_date
                    ,'statistical_date' => $statistical_date
                    , 'lc_date' => $data->lc_date
                    , 'season' => $data->season
                    ,'brand' => $brand
                    ,'season' => $data->season
                    ,'type_stock' => $type_stock
                    ,'type_stock_erp_code' => $type_stock_erp_code
                ]);
                if($statistical_date){
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update(['promise_date' => $statistical_date]);
                    PurchaseItem::where('po_buyer',$data->po_buyer)->update(['promise_date' => $statistical_date]);
                }else{
                    AutoAllocation::where('po_buyer',$data->po_buyer)->update(['promise_date' => $data->promise_date]);
                    PurchaseItem::where('po_buyer',$data->po_buyer)->update(['promise_date' => $data->promise_date]);
                }
            }
        }

        if(!$is_exist){
            $material_requirements = DB::connection('erp') //dev_erp //erp
            ->table('wms_material_requirements_detail_v')
            ->select('style',
                    'item_code',
                    'item_desc',
                    'uom',
                    'po_buyer',
                    'job_order',
                    'qty_required',
                    'article_no',
                    'is_piping',
                    'season',
                    'garment_size',
                    'part_no',
                    'warehouse_id',
                    'warehouse_name',
                    'city_name',
                    'component',
                    'category')
            ->where('po_buyer',$po_buyer)
            ->get();

            if($material_requirements->count() > 0){
                DetailMaterialRequirement::where('po_buyer',$po_buyer)->delete();
                
                foreach ($material_requirements as $key2 => $material_requirement) {
                    DetailMaterialRequirement::create([
                        'season' => $material_requirement->season,
                        'po_buyer' => $material_requirement->po_buyer,
                        'article_no' => $material_requirement->article_no,
                        'item_code' => $material_requirement->item_code,
                        'item_desc' => $material_requirement->item_desc,
                        'uom' => $material_requirement->uom,
                        'style' => $material_requirement->style,
                        'category' => $material_requirement->category,
                        'job_order' => $material_requirement->job_order,
                        'statistical_date' => $statistical_date,
                        'lc_date' => ($data)? $data->lc_date : null,
                        'promise_date' =>($data)? $data->promise_date : null,
                        'is_piping' => $material_requirement->is_piping,
                        'garment_size' => $material_requirement->garment_size,
                        'part_no' => $material_requirement->part_no,
                        'qty_required' => sprintf('%0.8f',$material_requirement->qty_required),
                        'warehouse_id' => $material_requirement->warehouse_id,
                        'warehouse_name' => $material_requirement->warehouse_name,
                        'component' => $material_requirement->component,
                        'destination_name' => $material_requirement->city_name
                    ]);
                }
            }
            
        }
        
        return response()->json(200);
    }

    static function getPlanningFabric($_po_buyer){
        $check_csi_on_wms =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_date(
            '".$_po_buyer."'
            )"
        )); 

        foreach ($check_csi_on_wms as $key => $value) {
            $_planning_date = $value->planning_date;
            $item_code = $value->item_code;
            $_warehouse_id = $value->warehouse_id;

            if($_warehouse_id == '1000012') $warehouse_id = '1000011';
            else if($_warehouse_id == '1000005') $warehouse_id = '1000001';

            $pos = strpos($_planning_date, ',');
            if ($pos === false){
                $planning_date = [$_planning_date];
            } else {
                $planning_date = explode(',',$_planning_date);
            }

            $material_planning_fabrics = MaterialPlanningFabric::where([
                ['po_buyer',$_po_buyer],
                [db::raw('upper(item_code)'),$item_code],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->get();

            foreach ($material_planning_fabrics as $key_2 => $material_planning_fabric) {
                $planning_date_wms = $material_planning_fabric->planning_date;
                if(!in_array($planning_date_wms, $planning_date)){
                    $material_planning_fabric->deleted_at = carbon::now();
                    $material_planning_fabric->remark = 'CANCEL PLANNING';
                    $material_planning_fabric->save();
                    MaterialPlanningFabricPiping::where('material_planning_fabric_id',$material_planning_fabric->id)->update(['deleted_at' => carbon::now()]);;
                }

                
            }
        }
        
        $cutting_instructions =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_buyer_all_warehouse(
            '".$_po_buyer."'
            )order by datestartschedule asc;"
        )); 

        $preparation_fabrics = array();
        foreach ($cutting_instructions as $key => $cutting_instruction) {
            $item_code = strtoupper($cutting_instruction->item_code);
            $_warehouse_id = $cutting_instruction->m_warehouse_id;
            $style = $cutting_instruction->style;
            $planning_date = $cutting_instruction->datestartschedule;
            $article_no = $cutting_instruction->article_no;
            $job_order = $cutting_instruction->job_order;
            $uom = $cutting_instruction->uom;
            $color = $cutting_instruction->color;
            $qty_consumtion = sprintf('%0.8f',$cutting_instruction->qty_consumtion);

            if($_warehouse_id == '1000012') $warehouse_id = '1000011';
            else if($_warehouse_id == '1000005') $warehouse_id = '1000001';

            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            $is_planning_exists = MaterialPlanningFabric::where([
                ['planning_date',$planning_date],
                ['po_buyer',$_po_buyer],
                [db::raw('upper(item_code)'),$item_code],
                ['style',$style],
                ['article_no',$article_no],
                //['qty_consumtion',$qty_consumtion],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->first();
            
            if(!$is_planning_exists){
                $material_planning_fabric = MaterialPlanningFabric::firstorcreate([
                    'po_buyer' => $_po_buyer,
                    'item_code' => $item_code,
                    'color' => $color,
                    'planning_date' => $planning_date,
                    'style' => $style,
                    'article_no' => $article_no,
                    'job_order' => $job_order,
                    'uom' => $uom,
                    'qty_consumtion' => $qty_consumtion,
                    'warehouse_id' => $warehouse_id,
                    'user_id' => $system->id
                ]);

                $preparation_fabrics [] = $material_planning_fabric->id;
            }else{
                $is_planning_exists->qty_consumtion = $qty_consumtion;
                $is_planning_exists->save();
                $preparation_fabrics [] = $is_planning_exists->id;
            }
        }

       //MaterialPlanning::insert_material_planning_piping($preparation_fabrics);
    }

    static function getSummaryBuyer($po_buyer){
        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();

        if(!$is_exist){
            $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement(
                '".$po_buyer."'
                );"
            ));

            $_po_buyer = PoBuyer::select('lc_date','statistical_date','promise_date')
            ->where('po_buyer',$po_buyer)
            ->groupby('lc_date','statistical_date','promise_date')
            ->first();

            if(count($material_requirements) > 0){
                MaterialRequirement::where([
                    ['po_buyer',$po_buyer],
                    ['is_upload_manual',false],
                ])
                ->delete();

                foreach ($material_requirements as $key2 => $material_requirement) {
                    $item = Item::select('item_desc')
                    ->where('item_code',$material_requirement->item_code)
                    ->groupby('item_desc')
                    ->first();

                    $_style = explode('::',$material_requirement->job_order)[0];

                    if($material_requirement->item_desc)
                        $_item_desc = $material_requirement->item_desc;
                    else
                        $_item_desc = $item->item_desc;
                    
                    MaterialRequirement::create([
                        'lc_date' => ($_po_buyer)? $_po_buyer->lc_date : null,
                        'season' => $material_requirement->season,
                        'po_buyer' => $material_requirement->po_buyer,
                        'article_no' => $material_requirement->article_no,
                        'item_code' => $material_requirement->item_code,
                        'item_desc' => $_item_desc,
                        'uom' => $material_requirement->uom,
                        'style' => $material_requirement->style,
                        '_style' => $_style,
                        'category' => $material_requirement->category,
                        'job_order' => $material_requirement->job_order,
                        'statistical_date' => ($_po_buyer)? $_po_buyer->statistical_date : null,
                        'promise_date' => ($_po_buyer)? $_po_buyer->promise_date : null,
                        'qty_required' => sprintf('%0.8f',$material_requirement->qty_required),
                        'warehouse_id' => $material_requirement->warehouse_id,
                        'warehouse_name' => $material_requirement->warehouse_name,
                        'component' => $material_requirement->component,
                        'destination_name' => $material_requirement->destination_name
                    ]);
                }
            }
        }
            

        
       

       
    }

    static function getSummaryBuyerPerPart($po_buyer)
    {
        $is_exist = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->exists();

        if(!$is_exist){
            $material_requirements = DB::select(db::raw("SELECT * FROM get_summary_material_requirement_per_part(
                '".$po_buyer."'
                );"
            ));

            $_po_buyer = PoBuyer::select('statistical_date','promise_date')
            ->where('po_buyer',$po_buyer)
            ->groupby('statistical_date','promise_date')
            ->first();

            if(count($material_requirements) > 0){
                MaterialRequirementPerPart::where('po_buyer',$po_buyer)->delete();

                foreach ($material_requirements as $key2 => $material_requirement) {
                    MaterialRequirementPerPart::create([
                        'po_buyer' => $material_requirement->po_buyer,
                        'item_code' => $material_requirement->item_code,
                        'article_no' => $material_requirement->article_no,
                        'is_piping' => $material_requirement->is_piping,
                        'style' => $material_requirement->style,
                        'category' => $material_requirement->category,
                        'uom' => $material_requirement->uom,
                        'part_no' => $material_requirement->part_no,
                        'qty_required' => sprintf('%0.8f',$material_requirement->qty_required),
                        'statistical_date' => ($_po_buyer)?$_po_buyer->statistical_date : null,
                        'promise_date' => ($_po_buyer)? $_po_buyer->promise_date : null,
                        'lc_date' => $material_requirement->lc_date
                    ]);
                }
            }
        }
    }

    static function getPurchaseItems($_po_buyer)
    {
        $purchase_items = DB::connection('erp') //dev_erp //erp
        ->table('rma_wms_planning_v2')
        ->select('document_no',
                'c_order_id',
                'c_bpartner_id',
                'supplier_name',
                'po_buyer',
                'm_warehouse_id as warehouse',
                'item_code',
                'uom_pr as uom',
                'uom_po as uom_po',
                'category',
                db::raw('sum(qty_pr) as qty'),
                db::raw('sum(qtyordered) as qty_order'))
        ->where('po_buyer',$_po_buyer)
        ->where('document_no','NOT LIKE',"%^%")
        ->orderBy('po_buyer','desc')
        ->groupby('document_no',
        'c_order_id',
        'c_bpartner_id',
        'supplier_name',
        'po_buyer',
        'm_warehouse_id',
        'item_code',
        'uom_pr',
        'uom_po',
        'category')
        ->get();

        $__po_buyer = PoBuyer::where(db::raw('po_buyer'),$_po_buyer)->first();
        $lc_date = ($__po_buyer)? $__po_buyer->lc_date : null;
        if($__po_buyer){
            if($__po_buyer->statistical_date) $promise_date = ($__po_buyer)? $__po_buyer->statistical_date : null;
            else $promise_date = ($__po_buyer)? $__po_buyer->promise_date : null;
        }else{
            $promise_date = null;
        }
        
        $season = ($__po_buyer)? $__po_buyer->season : null;
        $type_stock_erp_code = ($__po_buyer)? $__po_buyer->type_stock_erp_code : null;
        if($type_stock_erp_code == 1) $type_stock = 'SLT';
        else if($type_stock_erp_code == 2) $type_stock = 'REGULER';
        else if($type_stock_erp_code == 3) $type_stock = 'PR/SR';
        else if($type_stock_erp_code == 4) $type_stock = 'MTFC';
        else $type_stock = null;
        
        $cancel_date = ($__po_buyer)? $__po_buyer->cancel_date : null;
        if($cancel_date != null) $status_po_buyer = 'cancel';
        else $status_po_buyer = 'active';

        $array = array();
        foreach ($purchase_items as $key => $purchase_item)
        {
            $c_order_id = $purchase_item->c_order_id;
            $document_no = strtoupper($purchase_item->document_no);
            $c_bpartner_id = $purchase_item->c_bpartner_id;
            $supplier_name = strtoupper($purchase_item->supplier_name);
            $po_buyer = $purchase_item->po_buyer;
            $item_code = strtoupper($purchase_item->item_code);
            $warehouse = $purchase_item->warehouse;
            $qty = sprintf('%0.8f',$purchase_item->qty);
            $qty_order = sprintf('%0.8f',$purchase_item->qty_order);
            $qty_moq = sprintf('%0.8f',$qty_order - $qty);
            $uom = $purchase_item->uom;
            $uom_po = $purchase_item->uom_po;
            $category = $purchase_item->category;

            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse]
            ])
            ->first();

            $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();

            $item = Item::select('item_desc','category','uom')
            ->where(db::raw('item_code'),$item_code)
            ->groupby('item_desc','category','uom')
            ->first();
            $item_desc = ($item)? strtoupper($item->item_desc): 'data not found';
            $uom = ($item)? strtoupper($item->uom): 'data not found';
            $category = ($item)? strtoupper($item->category): 'data not found';

            if($category == 'FB' && ($po_buyer != null || $po_buyer != ''))
            {
                if($lc_date >= '2019-08-15')// cut off mulai lc tanggal ini, user baru setuju mau maintain
                {   
                    if($warehouse == '1000011') $warehouse_name = 'FABRIC AOI 2';
                    else if($warehouse == '1000001') $warehouse_name = 'FABRIC AOI 1';

                    $is_exists = AutoAllocation::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(item_code)'),$item_code],
                        ['warehouse_id',$warehouse],
                        ['qty_allocation',$qty],
                        ['is_fabric',true],
                        //['is_upload_manual',false],
                        //['is_allocation_purchase',true],
                    ])
                    ->exists();

                    if(!$is_exists)
                    {
                        AutoAllocation::firstorcreate([
                            'type_stock_erp_code' => $type_stock_erp_code,
                            'type_stock' => $type_stock,
                            'lc_date' => $lc_date,
                            'promise_date' => $promise_date,
                            'season' => $season,
                            'document_no' => $document_no,
                            'c_bpartner_id' => $c_bpartner_id,
                            'supplier_name' => $supplier_name,
                            'po_buyer' => $po_buyer,
                            'item_code_source' => $item_code,
                            'item_code' => $item_code,
                            'item_desc' => $item_desc,
                            'category' => $category,
                            'uom' => $uom,
                            'warehouse_name' => $warehouse_name,
                            'warehouse_id' => $warehouse,
                            'qty_allocation' => $qty,
                            'qty_outstanding' => $qty,
                            'qty_allocated' => 0,
                            'status_po_buyer' => $status_po_buyer,
                            'is_fabric' => true,
                            'is_already_generate_form_booking' => false,
                            'is_upload_manual' => false,
                            'is_allocation_purchase' => true,
                            'user_id' => $system->id,
                            'so_id'   => $so_id->so_id
                        ]);
                    }
                }

                $purchase_item_id = null;
            }else{
                if($qty_moq > 0){
                    if($category == 'EL' || $category == 'TP' || $category == 'TH'){
                        $remark_moq = 'MATERIAL SEBENERNYA TERKENA MOQ AKAN TETAPI KARNA KATEGORINYA '.$category.' JADI TIDAK DI ANGGAP';
                        $is_moq = false;
                    }else if($c_bpartner_id == '1001054' || $c_bpartner_id == '1001164'){
                        $remark_moq = 'MATERIAL SEBENERNYA TERKENA MOQ AKAN TETAPI KARNA SUPPLIERNYA DARI  '.$supplier_name.' JADI TIDAK DI ANGGAP';
                        $is_moq = false;
                    }else if($category == 'LB'){
                        $array = [
                            '62752352-001A-00',
                            '62752352.BT-001A-00',
                            '62752352.TP-001A-00',
                            '62002447-001A-00',
                            '62002447.BT-001A-00',
                            '62002447.TP-001A-00',
                            '62550266-001A-00',
                            '62550266.BT-001A-00',
                            '62550266.TP-001A-00',
                            '62752351-001A-00',
                            '62752351.BT-001A-00',
                            '62752351.TP-001A-00'
                        ];
                        if(!in_array($item_code, $array)){
                            $remark_moq = 'MATERIAL SEBENERNYA TERKENA MOQ AKAN TETAPI KARNA ITEMNYA DARI  '.$item_code.' JADI TIDAK DI ANGGAP';
                            $is_moq = false;
                        }
                    }else{
                        $conversion = UomConversion::where([
                            ['item_code',$item_code],
                            ['uom_to',$uom],
                            ['uom_from',$uom_po]
                        ])
                        ->first();
                    
                        if($conversion) $multiplyrate = $conversion->multiplyrate;
                        else $multiplyrate = 1;

                        $diff = sprintf('%0.8f',$qty_moq * $multiplyrate);

                        if($diff > 10){
                            $remark_moq = 'MATERIAL INI TERKENA MOQ';
                            $is_moq = true;
                        }else{
                            $remark_moq = 'MATERIAL SEBENERNYA TERKENA MOQ AKAN TETAPI KARNA QTYNYA KURANG DARI 10, QTY MOQ NYA ADALAH'.$diff.'('.$uom_po.') JADI TIDAK DI ANGGAP';
                            $is_moq = false;
                        }
                        
                    }
                }else{
                    $remark_moq = null;
                    $is_moq = false;
                }

                $purchase_item = PurchaseItem::where([
                    [db::raw('upper(document_no)'),$document_no],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['po_buyer',$po_buyer],
                    [db::raw('upper(item_code)'),$item_code],
                    ['warehouse',$warehouse],
                    ['category',$category]
                ]) 
                ->first();
    
                if($purchase_item){
                    $purchase_item->c_order_id = $c_order_id;
                    $purchase_item->promise_date = $promise_date;
                    $purchase_item->supplier_name = $supplier_name;
                    $purchase_item->uom = $uom;
                    $purchase_item->qty = $qty;
                    $purchase_item->category = $category;
                    $purchase_item->status_po_buyer = $status_po_buyer;
                    $purchase_item->qty_order = $qty_order;
                    $purchase_item->qty_moq = $qty_moq;
                    if($purchase_item->counter_moq == 0) $purchase_item->credit_moq = $qty_moq;
                    $purchase_item->remark_moq = $remark_moq;
                    $purchase_item->is_moq = $is_moq;
                    $purchase_item->save();

                    $purchase_item_id = $purchase_item->id;
                }else{
                    $new_purchase = PurchaseItem::firstOrCreate([
                        'c_order_id' => $c_order_id,
                        'promise_date' => $promise_date,
                        'document_no' => $document_no,
                        'c_bpartner_id' => $c_bpartner_id,
                        'po_buyer' => $po_buyer,
                        'supplier_name' => $supplier_name,
                        'item_code' => $item_code,
                        'warehouse' => $warehouse,
                        'category' => $category,
                        'qty' => $qty,
                        'qty_order' => $qty_order,
                        'uom' => $uom,
                        'status_po_buyer' => $status_po_buyer,
                        'qty_moq' => $qty_moq,
                        'credit_moq' => $qty_moq,
                        'remark_moq' => $remark_moq,
                        'is_moq' => $is_moq,
                    ]);

                    $purchase_item_id = $new_purchase->id;
                }

            }

            $array [] = $purchase_item_id;
        }

        $data_moq = PurchaseItem::select('id','document_no','c_bpartner_id','item_code','uom','po_buyer','c_order_id','qty_moq','warehouse')
        ->whereIn('id',$array)
        ->where([
            ['is_moq',true],
            ['is_stock_moq_already_created',false],
            ['remark_moq','MATERIAL INI TERKENA MOQ'],
        ])
        ->groupby('id','document_no','c_bpartner_id','item_code','uom','po_buyer','c_order_id','qty_moq','warehouse')
        ->get();

        foreach ($data_moq as $key => $value) {
            $item = Item::where('item_code',$value->item_code)->first();
            $_po_buyer = PoBuyer::where('po_buyer',$value->po_buyer)->first();
            $po_supplier = PoSupplier::where([
                [db::raw('upper(document_no)'),strtoupper($value->document_no)],
                ['c_bpartner_id',$value->c_bpartner_id],
            ])
            ->first();

            $system = User::where([
                ['name','system'],
                ['warehouse',$value->warehouse]
            ])
            ->first();
            HistoryAutoAllocations::firstOrCreate([
                'purchase_item_id' => $value->id,
                'document_no_old' => $value->document_no,
                'c_bpartner_id_old' => $value->c_bpartner_id,
                'item_code_old' => $value->item_code,
                'uom_old' => $value->uom,
                'item_id_old' => ($item) ? $item->item_id : null,
                'item_source_id_old' => ($item) ? $item->item_id : null,
                'item_code_source_old' => $value->item_code,
                'po_buyer_old' => $value->po_buyer,
                'supplier_name_old' => ($po_supplier) ? ($po_supplier->supplier_id)? $po_supplier->supplier->supplier_name :null : null,
                'qty_allocated_old' => $value->qty_moq,
                'warehouse_id_old' => $value->warehouse,
                'type_stock_erp_code_old' => ($_po_buyer)? $_po_buyer->type_stock_erp_code : null,
                'type_stock_old' => ($_po_buyer)? $_po_buyer->type_stock : null,
    
                'note_code' => '2',
                'note' => 'MOQ DARI PEMBELIAN',
                'operator' => -1*$value->qty_moq,
                'remark' => 'MOQ DARI PEMBELIAN',
                'is_integrate' => false,
                'lc_date' => ($_po_buyer)? $_po_buyer->lc_date : null,
                'c_order_id' => $value->c_order_id,
                'user_id' => $system->id
            ]);

            PurchaseItem::where([
                ['c_bpartner_id',$value->c_bpartner_id],
                ['item_code',$value->item_code],
                ['po_buyer',$value->po_buyer],
                ['c_order_id',$value->c_order_id],
                ['qty_moq',$value->qty_moq],
                ['warehouse',$value->warehouse],
            ])
            ->update([
                'is_stock_moq_already_created' => true
            ]);
        }

    }

    private function insertAllocationFabric($po_buyer)
    {
        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['po_buyer',$po_buyer],
        ])
        ->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();

        foreach ($auto_allocations as $key_2 => $auto_allocation) {
            $auto_allocation_id = $auto_allocation->id;
            $document_no = $auto_allocation->document_no;
            $c_bpartner_id = $auto_allocation->c_bpartner_id;
            $po_buyer = $auto_allocation->po_buyer;
            $item_code = $auto_allocation->item_code;
            $qty_outstanding = sprintf('%0.8f',$auto_allocation->qty_outstanding);
            $qty_allocated = sprintf('%0.8f',$auto_allocation->qty_allocated);
            $warehouse_id = $auto_allocation->warehouse_id;
            $supplier = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
            $supplier_name = ($supplier)? $supplier->supplier_name : null;
            $supplier_code = ($supplier)? $supplier->supplier_code : null;
            
            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
            
            $material_requirements = MaterialRequirement::where([
                ['po_buyer',$po_buyer],
                [db::raw('upper(item_code)'),$item_code],
            ])
            ->get();

            if($qty_outstanding > 0){
                $count_mr = $material_requirements->count();
                foreach ($material_requirements as $key => $material_requirement) {
                    $lc_date = $material_requirement->lc_date;
                    $item_desc = strtoupper($material_requirement->item_desc);
                    $uom = $material_requirement->uom;
                    $category = $material_requirement->category;
                    $style = $material_requirement->style;
                    $job_order = $material_requirement->job_order;
                    $_style = explode('::',$job_order)[0];
                    $article_no = $material_requirement->article_no;
                    
                    $month_lc = $material_requirement->lc_date->format('m');
                    if($month_lc <= '02') $qty_required = sprintf("%0.2f",$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style));
                    else $qty_required = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style));
                    
                    if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                    else $__supplied = sprintf('%0.8f',$qty_outstanding);

                    $_warehouse_erp_id = $material_requirement->warehouse_id;

                    if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                    else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                    else $warehouse_production_id = $warehouse_id;
                    
                    if($qty_required > 0 && $qty_outstanding > 0){
                        if ($qty_outstanding/$__supplied >= 1) $_supply = $__supplied;
                        else $_supply = $qty_outstanding;

                        if($month_lc <= '02') $__supply = sprintf("%0.2f",$_supply);
                        else $__supply = sprintf('%0.8f',$_supply);
                    
                        $is_exists = AllocationItem::where([
                            ['auto_allocation_id',$auto_allocation_id],
                            ['item_code',$item_code],
                            ['style',$style],
                            ['po_buyer',$po_buyer],
                            ['qty_booking',$__supply],
                            ['is_additional',false],
                            ['is_from_allocation_fabric',true],
                            ['warehouse_inventory',$warehouse_id] // warehouse inventory
                        ])
                        ->where(function($query){
                            $query->where('confirm_by_warehouse','approved')
                            ->OrWhereNull('confirm_by_warehouse');
                        })
                        ->whereNull('deleted_at')
                        ->exists();
                        
                        if(!$is_exists && $_supply > 0){
                            $allocation_item = AllocationItem::FirstOrCreate([
                                'auto_allocation_id' => $auto_allocation_id,
                                'lc_date' => $lc_date,
                                'c_bpartner_id' => $c_bpartner_id,
                                'supplier_code' => $supplier_code,
                                'supplier_name' => $supplier_name,
                                'document_no' => $document_no,
                                'item_code' => $item_code,
                                'item_desc' => $item_desc,
                                'category' => $category,
                                'uom' => $uom,
                                'job' => $job_order,
                                'style' => $style,
                                '_style' => $_style,
                                'article_no' => $article_no,
                                'po_buyer' => $po_buyer,
                                'warehouse' => $warehouse_production_id,
                                'warehouse_inventory' => $warehouse_id,
                                'is_need_to_handover' => false,
                                'qty_booking' => $__supply,
                                'is_from_allocation_fabric' => true,
                                'user_id' => $system->id,
                                'confirm_by_warehouse' => 'approved',
                                'confirm_date' => Carbon::now(),
                                'remark' => 'ALLOCATION PURCHASE',
                                'confirm_user_id' => $system->id,
                                'deleted_at' => null
                            ]);

                            //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                            //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                            $qty_required -= $_supply;
                            $qty_outstanding -= $_supply;
                            $qty_allocated += $_supply;
                        }
                    }
                }
            }
            
            //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
            
            if(intVal($qty_outstanding)<= 0){
                $auto_allocation->is_already_generate_form_booking = true;
                $auto_allocation->generate_form_booking = carbon::now();
                $auto_allocation->qty_outstanding = 0;
            }else{
                $auto_allocation->qty_outstanding = sprintf('%0.8f',$qty_outstanding);
            
            }

            $auto_allocation->qty_allocated = sprintf('%0.8f',$qty_allocated);
            $auto_allocation->save();
            
        }
    }
}
