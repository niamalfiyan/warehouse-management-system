<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use stdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Item;
use App\Models\Scheduler;
use App\Models\MaterialCheck;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\SummaryHandoverMaterial;
use App\Models\MonitoringReceivingFabric;
use App\Models\MaterialRollHandoverFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparationFabric;

class MasterDataItemController extends Controller
{
    public function index()
    {
        $warehouse = auth::user()->warehouse;
        if($warehouse == '1000011' || $warehouse == '1000001') $selected_category = 'FB';
        else $selected_category = null; 

        $categories = Item::select('category')->groupby('category')->pluck('category','category')->all();
        return view('master_data_item.index',compact('categories','selected_category'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $category = $request->category;

            $items = Item::where('category','like',"%$category%")->orderby('updated_at','desc');

            return DataTables::of($items)
            ->addColumn('action',function($items){
                return view('master_data_item._action',[
                    'model'                 => $items,
                    'uomConversion'         => route('masterDataItem.dataUomConversion',$items->item_id),
            ]);
               
            })
            ->make(true);
        }
    }

    public function dataUomConversion(Request $request,$id)
    {
        if ($request->ajax())
        {
            $item_id = $id;

            $uom_conversions = UomConversion::where('item_id',$item_id)->orderby('updated_at','desc');

            return DataTables::of($uom_conversions)
            ->make(true);
        }
    }

    public function erpItemPickList(Request $request)
    {
        $q      = strtoupper($request->q);
        $lists  = DB::connection('erp')
        ->table('wms_master_items')
        ->where(db::raw('upper(item_code)'),'like',"%$q%")
        ->paginate(10);
        
        return view('master_data_item._erp_item_list',compact('lists'));
    }

    public function store(Request $request)
    {  
       $item_id =  $request->item_id;
       $data    = DB::connection('erp')
       ->table('wms_master_items')
       ->where('item_id',$item_id)
       ->first();

       $data_conversion = DB::connection('erp')
        ->table('adt_uom_conv')
        ->where('m_product_id',$item_id)
        ->first();
    
        $sync_date = Carbon::now();
       try 
       {
            DB::beginTransaction();

            $is_exits = Item::where('item_id',$data->item_id)->first();
            
            $_is_stock        = trim(strtoupper($data->isstock));

            if($_is_stock == 'Y') $is_stock = true;
            else $is_stock = false;


            if($is_exits)
            {
                $is_exits->item_code        = trim(strtoupper($data->item_code));
                $is_exits->item_desc        = trim(strtoupper($data->item_desc));
                $is_exits->color            = trim(strtoupper($data->color));
                $is_exits->category         = trim(strtoupper($data->category));
                $is_exits->uom              = trim(strtoupper($data->uom));
                $is_exits->upc              = trim(strtoupper($data->upc));
                $is_exits->composition      = trim(strtoupper($data->description));
                $is_exits->category_name    = trim(strtoupper($data->category_name));
                $is_exits->width            = trim(strtoupper($data->width));
                $is_exits->is_stock         = $is_stock;
                $is_exits->updated_at       = $sync_date;
                $is_exits->save();
            }else
            {
                Item::firstorCreate([
                    'item_id'           => trim(strtoupper($data->item_id)),
                    'item_code'         => trim(strtoupper($data->item_code)),
                    'item_desc'         => trim(strtoupper($data->item_desc)),
                    'category'          => trim(strtoupper($data->category)),
                    'uom'               => trim(strtoupper($data->uom)),
                    'color'             => trim(strtoupper($data->color)),
                    'upc'               => trim(strtoupper($data->upc)),
                    'composition'       => trim(strtoupper($data->description)),
                    'category_name'     => trim(strtoupper($data->category_name)),
                    'width'             => trim(strtoupper($data->width)),
                    'is_stock'          => $is_stock,
                    'created_at'        => $sync_date,
                    'updated_at'        => $sync_date
                ]);
            }
            
            if($data_conversion)
            {
                $is_uom_conversion_exists = UomConversion::where([
                    ['item_id',$data_conversion->m_product_id],
                    [db::raw('upper(uom_from)'),$data_conversion->uom_to],
                    [db::raw('upper(uom_to)'),$data_conversion->uomsymbol],
                ])
                ->first();
    
                if($is_uom_conversion_exists)
                {
                    $is_uom_conversion_exists->item_code       = $data_conversion->product;
                    $is_uom_conversion_exists->item_desc       = strtoupper($data_conversion->product_name);
                    $is_uom_conversion_exists->category        = strtoupper($data_conversion->value);
                    $is_uom_conversion_exists->uom_from        = strtoupper($data_conversion->uom_to);
                    $is_uom_conversion_exists->uom_to          = strtoupper($data_conversion->uomsymbol);
                    $is_uom_conversion_exists->dividerate      = $data_conversion->dividerate;
                    $is_uom_conversion_exists->multiplyrate    = $data_conversion->multiplyrate;
                    $is_uom_conversion_exists->save();
                }else
                {
                    UomConversion::firstorCreate([
                        'item_id'       => $data_conversion->m_product_id,
                        'item_code'     => $data_conversion->product,
                        'item_desc'     => $data_conversion->product_name,
                        'category'      => $data_conversion->value,
                        'uom_from'      => $data_conversion->uom_to,
                        'uom_to'        => $data_conversion->uomsymbol,
                        'dividerate'    => $data_conversion->dividerate,
                        'multiplyrate'  => $data_conversion->multiplyrate,
                    ]);
                }
            }
            
            DB::commit();

        
       } catch (Exception $e) 
       {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
       }

       $item = Item::where('item_id',$item_id)->first();

       
       MaterialPreparation::where('item_id',$item_id)
       ->update([
            'item_code' => $item->item_code,
            'item_desc' => $item->item_desc
       ]);
       
       MaterialRequirement::where('item_id',$item_id)
       ->update([
            'item_code' => $item->item_code,
            'item_desc' => $item->item_desc
       ]);
       
       SummaryStockFabric::where('item_id',$item_id)
       ->update([
            'item_code' => $item->item_code,
            'color'     => $item->color,
       ]);

       $auto_allocations = AutoAllocation::where('item_id_book',$item_id)->get();
       foreach ($auto_allocations as $key => $auto_allocation) 
       {
           $article_no = $auto_allocation->article_no;
           if($article_no) $item_code = $item->item_code.'|'.$article_no;
           else $item_code = $item->item_code;

           $auto_allocation->item_code = $item_code;
           $auto_allocation->item_code_book = $item->item_code;
           $auto_allocation->save();
       }

       AutoAllocation::where('item_id_source',$item_id)
       ->update([
            'item_code_source' => $item->item_code,
       ]);

       MaterialArrival::where('item_id',$item_id)
       ->update([
            'item_code' => $item->item_code,
            'item_desc' => $item->item_desc
       ]);

       MaterialStock::where('item_id',$item_id)
       ->update([
            'color'     => $item->color,
            'item_desc' => $item->item_desc,
            'item_code' => $item->item_code,
       ]);

       MaterialPreparationFabric::where('item_id_book',$item_id)
       ->update([
             'item_code' => $item->item_code,
       ]);

       MaterialPreparationFabric::where('item_id_source',$item_id)
       ->update([
             'item_code_source' => $item->item_code,
       ]);
      
       DetailMaterialPreparationFabric::whereIn('material_stock_id',function($query) use($item_id)
       {
            $query->select('id')
            ->from('material_stocks')
            ->where('item_id',$item_id);
       })
        ->update([
            'color'     => $item->color,
            'item_code' => $item->item_code,
            'item_id'   => $item->item_id,
        ]);

       MaterialCheck::whereNotNull('material_preparation_id')
       ->whereIn('material_preparation_id',function($query) use ($item_id) 
       {
            $query->select('id')
            ->from('material_preparations')
            ->where('item_id',$item_id);
       })
       ->update([
            'color'     => $item->color,
            'item_desc' => $item->item_desc,
            'item_code' => $item->item_code,
       ]);

       MaterialCheck::whereNotNull('material_stock_id')
       ->whereIn('material_stock_id',function($query) use ($item_id) 
       {
            $query->select('id')
            ->from('material_stocks')
            ->where('item_id',$item_id);
       })
       ->update([
            'color'     => $item->color,
            'item_desc' => $item->item_desc,
            'item_code' => $item->item_code,
       ]);

       MonitoringReceivingFabric::where('item_id',$item_id)
       ->update([
            'color'         => $item->color,
            'item_code'     => $item->item_code
       ]);

       SummaryHandoverMaterial::where('item_id',$item_id)
       ->update([
            'item_code'     => $item->item_code
       ]);

       MaterialRollHandoverFabric::where('item_id',$item_id)
       ->update([
            'color'         => $item->color,
            'item_code'     => $item->item_code
       ]);

       AllocationItem::whereNotNull('item_id_book')
       ->where('item_id_book',$item_id)
       ->update([
            'item_code'         => $item->item_code
       ]);

       AllocationItem::whereNotNull('item_id_source')
       ->where('item_id_source',$item_id)
       ->update([
            'item_code_source'  => $item->item_code
       ]);

       AllocationItem::whereNull('item_id_book')
       ->where(db::raw("upper(trim(allocation_items.item_code))"),$item->item_code)
       ->update([
            'item_id_book'      => $item->item_id,
            'item_code'         => $item->item_code,
       ]);

       AllocationItem::whereNull('item_id_source')
       ->where(db::raw("upper(trim(COALESCE(allocation_items.item_code_source, allocation_items.item_code)))"),$item->item_code)
       ->update([
            'item_id_source'    => $item->item_id,
            'item_code_source'  => $item->item_code,
       ]);

       return response()->json(200);
    }

    
}
