<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Mrp;
use App\Models\WmsOld;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Temporary;
use App\Models\PoBuyerLog;
use App\Models\ReroutePoBuyer;
use App\Models\RackAutomation;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;

class AccessoriesBarcodeCartonController extends Controller
{
    public function index()
    {
        return view('accessories_barcode_carton.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $barcode        = trim($request->barcode);
       
        $has_dash       = strpos($barcode, "-");
        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
        }

        $array = array();
        $array = $this->getDataBarcode($_barcode,$_warehouse_id);

        if(isset($array->original)) return response()->json($array->original,422);
        return response()->json($array,200);
    }

    private function getDataBarcode($barcode,$_warehouse_id)
    {
       
        if($barcode =='BELUM DI PRINT') return response()->json('Barcode not printed yet.',422);
        
        $material_preparation = MaterialPreparation::where([
            ['barcode',strtoupper($barcode)],
            ['warehouse',$_warehouse_id]
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found.',422);
        if($material_preparation->is_need_to_be_switch) return response()->json('Item is book to another Po buyer, please give it to admin to be prepared again(Item ini dipinjamkan untuk po buyer lain, silahkan berikan ke admin untuk di prepare ulang)..',422);
        if($material_preparation->is_moq) return response()->json('Item has MOQ on it, please make adjustments first(Item ini terkena moq, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->is_reduce) return response()->json('Qty for this item need REDUCE due reduce qty on bom, please make adjustments first(Item ini terkena reduce, silahkan lakukan adjustment terlebih dahulu).',422);
        if($material_preparation->qc_status == 'HOLD') return response()->json('The QC results for this item are HOLD, please contact QC to find out the actual results (Hasil qc item ini HOLD, silahkan hubungi QC untuk mengetahui hasil aktualnya).',422);
        if($material_preparation->qc_status == 'FULL REJECT') return response()->json('The QC results of this item are reject, the item is not used, please wait for a replacement (Hasil QC item ini ditolak, sudah tidak digunakan silahkan tunggu pengantinya).',422);

        if($material_preparation->warehouse != $_warehouse_id)
        {
            if($material_preparation->warehouse == 1000002) $_temp = 'Accessories AOI 1';
            else if($material_preparation->warehouse == 1000013) $_temp = 'Accessories AOI 2';

            return response()->json('Warehouse '.$_temp.', not same with your warehouse.',422);
        }

        if($material_preparation->last_status_movement == 'out' 
            || $material_preparation->last_status_movement == 'reroute' 
            || $material_preparation->last_status_movement == 'cancel order' 
            || $material_preparation->last_status_movement == 'cancel item') 

            return response()->json('Item already supplied to '.$material_preparation->locator->code,422);
        
      

        
        $document_no                = $material_preparation->document_no;
        $material_preparation_id    = $material_preparation->id;
        $po_buyer                   = $material_preparation->po_buyer;
        $item_id                    = $material_preparation->item_id;
        $item_code                  = $material_preparation->item_code;
        $item_desc                  = $material_preparation->item_desc;
        $category                   = $material_preparation->category;
        $type_po                    = $material_preparation->type_po;
        $job_order                  = $material_preparation->job_order;
        $total_carton               = $material_preparation->total_carton;
        $is_backlog                 = $material_preparation->is_backlog;
        $uom_conversion             = $material_preparation->uom_conversion;
        $qty_conversion             = $material_preparation->qty_conversion;
        $last_locator_id            = $material_preparation->last_locator_id;
        $last_status_movement       = $material_preparation->last_status_movement;
        $last_locator_code          = $material_preparation->lastLocator->code;
        $warehouse_user             = auth::user()->warehouse;
        $style                      = $material_preparation->style;
        $article_no                 = $material_preparation->article_no;
        
        $material_requirement = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id],
            ['style',$style],
            ['article_no',$article_no]
        ])
        ->first();

        $master_po_buyer               = PoBuyer::where('po_buyer',$po_buyer)->first();
        $is_po_buyer_cancel            = ($master_po_buyer?($master_po_buyer->cancel_date?true:false):false);
        
        $obj                            = new StdClass();
        $obj->material_preparation_id   = $material_preparation_id;
        $obj->item_code                 = $item_code;
        $obj->item_desc                 = $item_desc;
        $obj->category                  = $category;
        $obj->type_po                   = $type_po;
        $obj->uom_conversion            = $uom_conversion;
        $obj->job                       = $job_order;
        $obj->style                     = $style;
        $obj->article_no                = $article_no;
        $obj->po_buyer                  = $po_buyer;
        $obj->is_reroute                = false;
        $obj->qty_receive               = $qty_conversion;
        $obj->barcode                   = $barcode;
        $obj->total_carton              = $total_carton;
        $obj->last_locator_code         = $last_locator_code;
        $obj->is_po_buyer_cancel        = $is_po_buyer_cancel;
        $obj->season                    = ($material_requirement) ? $material_requirement->season : null;
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        return $obj;
    }

    public function store(Request $request)
    {
        $items              = json_decode($request->material_barcode_carton_accessories);
        $return_barcodes    = array();

        foreach ($items as $key => $item) 
        {
            $id                     = $item->material_preparation_id;
            $new_total_carton       = $item->total_carton;
            $material_preparation   = MaterialPreparation::find($id);

            $material_preparation->total_carton = $new_total_carton;
            $material_preparation->save();

            $return_barcodes [] = $id;
        }

        return response()->json($return_barcodes,200);
    }

    public function barcode(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $data           = json_decode($request->list_barcodes);
        $items          = MaterialPreparation::wherein('id',$data)
        ->where([
            ['warehouse',$warehouse_id],
            ['type_po',2],
        ])
        ->orderby('document_no','asc')
        ->orderby('item_code','asc')
        ->orderby('barcode','asc')
        ->get();
        
        return view('accessories_barcode_carton.barcode',compact('items'));
    }
}
