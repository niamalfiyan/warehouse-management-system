<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\PoBuyer;
use App\Models\ItemPackingList;
use App\Models\PurchaseOrderDetail;

class WebSupplierController extends Controller
{
    static function get_pobuyer(){
        $set_po_buyer = new PoBuyer();
        $insert = array();
        $update = array();

        $data = PurchaseOrderDetail::select('pobuyer','type_po')
        ->whereNotNull('pobuyer')
        ->where([
            ['isactive','=',true],
            ['is_locked','=',true],
            ['pobuyer','<>',''],
            [db::raw('to_char(create_date,\'YYYY\')'),'>=','2018']
        ])
        ->where(function ($query){
            $query->where('flag_wms_get_pobuyer',false)
                ->orWhere('flag_wms_get_pobuyer',null);
        })
        ->groupby('pobuyer','type_po')
        ->get();

        $col_id = ''; 
        $col_po_buyer = ''; 
        $col_type_po = ''; 
        $flag_insert = 0; 
        foreach ($data as $key => $value) {
           $has_many_po_buyer = strpos($value->pobuyer, ",");
            
           if($has_many_po_buyer == false){
                $col_id .= '\''.Uuid::generate(4). '\',';
                $col_po_buyer .= '\''.$value->pobuyer. '\',';
                $col_type_po .= '\''.$value->type_po. '\',';
           }else{
                $split = explode(',',$value->pobuyer);
                foreach ($split as $key_split => $split_value) {
                    $col_id.= '\''.Uuid::generate(4). '\',';;
                    $col_po_buyer .= '\''.trim($split_value). '\',';
                    $col_type_po .= '\''.$value->type_po. '\',';
                }
           }
           $flag_insert++;
           $update [] = $value->pobuyer;
        }
        
        if($flag_insert > 0){
            try{
                db::beginTransaction();
                $col_id = substr_replace($col_id, '', -1);
                $col_po_buyer = substr_replace($col_po_buyer, '', -1);
                $col_type_po = substr_replace($col_type_po, '', -1);
                
                DB::select(db::raw("INSERT INTO po_buyers(
                id,
                po_buyer,
                type_po, 
                created_at, 
                updated_at)
                SELECT 
                    unnest(array[" . $col_id . "]),  
                    unnest(array[" . $col_po_buyer . "]),
                    unnest(array[" . $col_type_po . "]),
                    now()::timestamp(0),
                    now()::timestamp(0)
                "));
                    
                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message); 
            }

            PurchaseOrderDetail::whereIn('pobuyer',$update)
            ->update(['flag_wms_get_pobuyer' => true]);
        }
        
    }
}
