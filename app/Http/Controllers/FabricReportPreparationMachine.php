<?php

namespace App\Http\Controllers;

use DB;
use Excel;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class FabricReportPreparationMachine extends Controller
{
    public function index()
    {
        return view('fabric_report_preparation_machine.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $warehouse_id           = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            // dd($start_date);

            $data = DB::table('mna_report_output_machine')
                    ->where('warehouse_id', $warehouse_id)
                    ->whereBetween('preparation_date',[$start_date,$end_date])
                    ->orderBy('preparation_date','desc')
                    ->orderBy('planning_date', 'asc')
                    ->get();
            // dd($data);

            return DataTables::of($data)
            ->editColumn('preparation_date',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->preparation_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('total_yard',function ($data)
            {
                return number_format($data->reserved_qty, 4, '.', ',');
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        
        $_start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d') : Carbon::today()->subDays(30)->format('Y-m-d');
        $_end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d') : Carbon::now()->format('Y-m-d');
    
        
        $warehouse_name     = ($warehouse_id == '1000001' ?'AOI-1':'AOI-2' );

            $data = DB::table('mna_report_output_machine')
            ->where('warehouse_id', $warehouse_id)
            ->whereBetween('preparation_date',[$start_date,$end_date])
            ->orderBy('preparation_date','desc')
            ->orderBy('planning_date', 'asc')
            ->get();
            
        
            

            $file_name = 'REPORT_OUTPUT_MACHINE'.$warehouse_name.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
      

        return Excel::create($file_name,function($excel) use ($data)
        {
            $excel->sheet('ACTIVE',function($sheet)use($data)
            {
                $sheet->setCellValue('A1','PREPARATION_DATE');
                $sheet->setCellValue('B1','USER PREPARATION');
                $sheet->setCellValue('C1','PLANNING_DATE');
                $sheet->setCellValue('D1','MACHINE');
                $sheet->setCellValue('E1','BARCODE');
                $sheet->setCellValue('F1','NO_ROLL');
                $sheet->setCellValue('G1','ITEM_CODE');
                $sheet->setCellValue('H1','ARTICLE_NO');
                $sheet->setCellValue('I1','STYLE');
                $sheet->setCellValue('J1','QTY');
                $sheet->setCellValue('K1','WAREHOUSE');
            
            $row=2;

           
            foreach ($data as $i) 
            {  


                if($i->warehouse_id == '1000001') $warehouse_name =  'Warehouse fabric AOI 1';
                elseif($i->warehouse_id == '1000011') $warehouse_name =  'Warehouse fabric AOI 2';

                $sheet->setCellValue('A'.$row,$i->preparation_date);
                $sheet->setCellValue('B'.$row,$i->name);
                $sheet->setCellValue('C'.$row,$i->planning_date);
                $sheet->setCellValue('D'.$row,$i->machine);
                $sheet->setCellValue('E'.$row,$i->barcode);
                $sheet->setCellValue('F'.$row,$i->nomor_roll);
                $sheet->setCellValue('G'.$row,$i->item_code);
                $sheet->setCellValue('H'.$row,$i->article_no);
                $sheet->setCellValue('I'.$row,$i->style);
                $sheet->setCellValue('J'.$row,$i->reserved_qty);
                $sheet->setCellValue('K'.$row,$warehouse_name);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}
