<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\PoBuyer;
use App\Models\User;
use App\Models\Supplier;
use App\Models\Locator;
use App\Models\MaterialArrival;
use App\Models\MappingStocks;
use App\Models\MaterialStock;
use App\Models\UomConversion;
use App\Models\AutoAllocation;
use App\Models\MaterialBacklog;
use App\Models\MaterialMovement;
use App\Models\MaterialAdjustment;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialStock;

//use App\Http\Controllers\MasterDataAutoAllocationController as AutoAllocation;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesBarcodeAdjustmentController extends Controller
{
    public function index()
    {
        return view('accessories_barcode_adjustment.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id  = $request->warehouse_id;
        $barcode        = trim(strtoupper($request->barcode));
        
        $has_dash       = strpos($barcode, "-");
        if($has_dash == false)
        {
            $_barcode = $barcode;
        }else
        {
            $split = explode('-',$barcode);
            $_barcode = $split[0];
        }

        $material_preparation = MaterialPreparation::where([
            ['barcode',$_barcode],
            ['warehouse',$_warehouse_id]
        ])
        ->whereNull('deleted_at')
        ->first();
        
        if(!$material_preparation) return response()->json('Barcode not found.',422);
        
        if($material_preparation->warehouse != $_warehouse_id)
        {
            if($material_preparation->warehouse == 1000002) $_temp = 'Accessories AOI 1';
            else if($material_preparation->warehouse == 1000013) $_temp = 'Accessories AOI 2';

            return response()->json('Warehouse '.$_temp.', not same with your warehouse.',422);
        }

        if($material_preparation->qc_status == 'FULL REJECT') return response()->json('The QC results of this item reject, this barcode is no longer active please wait for replacement.',422);
        
        if($material_preparation->is_from_barcode_bom)
        {
            $material_preparation_id    = $material_preparation->id;
            $auto_allocation_id         = $material_preparation->auto_allocation_id;
            $barcode                    = $material_preparation->barcode;
            $po_buyer                   = $material_preparation->po_buyer;
            $c_order_id                 = $material_preparation->c_order_id;
            $item_code                  = $material_preparation->item_code;
            $item_id                    = $material_preparation->item_id;
            $article_no                 = $material_preparation->article_no;
            $style                      = $material_preparation->style;
            $item_desc                  = $material_preparation->item_desc;
            $uom_conversion             = $material_preparation->uom_conversion;
            $category                   = $material_preparation->category;
            $qty_preparation            = $material_preparation->qty_conversion;
            $warehouse                  = $material_preparation->warehouse;
            $c_bpartner_id              = $material_preparation->c_bpartner_id;
            $uom_source                 = ($material_preparation->material_stock_id ? $material_preparation->materialStock->uom_source : $uom_conversion);
            $document_no                = strtoupper($material_preparation->document_no);

            // $is_po_buyer_cancel         = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();
            // if($is_po_buyer_cancel) return response()->json('Po buyer '.$po_buyer.' is cancel, please checkout to cancel order.',422);
           
            $material_requirement = MaterialRequirement::where([
                ['item_id',$item_id],
                ['po_buyer',$po_buyer],
                ['style',$style],
                ['article_no',$article_no],
            ])
            ->first();
                    
            if($material_requirement)
            {
                $uom_purchase = $material_requirement->uom;
                $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
            }else{
                $uom_purchase = $uom_conversion;
                $qty_required = 1;
            }

            $qty_adj                        = sprintf('%0.8f',$qty_preparation - $qty_required);
            $qty_after_adj                  = sprintf('%0.8f',$qty_preparation - $qty_adj);
            
            $obj                            = new StdClass();
            $obj->barcode                   = $barcode;
            $obj->material_preparation_id   = $material_preparation_id;
            $obj->auto_allocation_id        = $auto_allocation_id;
            $obj->document_no               = $document_no;
            $obj->item_code                 = $item_code;
            $obj->item_desc                 = $item_desc;
            $obj->category                  = $category;
            $obj->po_buyer                  = $po_buyer;
            $obj->uom_conversion            = $uom_conversion;
            $obj->qty_before_adj            = $qty_preparation;
            $obj->qty_need                  = $qty_required;
            $obj->qty_after_adj             = ($qty_adj < 0 )? null : sprintf('%0.8f',$qty_after_adj);
            $obj->qty_adj                   = sprintf('%0.8f',$qty_adj);
            $obj->warehouse                 = $warehouse;
            $obj->barcodes                  = [];

        }else
        {
            if(!$material_preparation->auto_allocation_id) return response()->json('Auto Allocation ID is not found, please contact ICT',422);

            $material_preparation_id    = $material_preparation->id;
            $auto_allocation_id         = $material_preparation->auto_allocation_id;
            $barcode                    = $material_preparation->barcode;
            $po_buyer                   = $material_preparation->po_buyer;
            $c_order_id                 = $material_preparation->c_order_id;
            $item_code                  = $material_preparation->item_code;
            $item_id                    = $material_preparation->item_id;
            $article_no                 = $material_preparation->article_no;
            $style                      = $material_preparation->style;
            $item_desc                  = $material_preparation->item_desc;
            $uom_conversion             = $material_preparation->uom_conversion;
            $category                   = $material_preparation->category;
            $qty_preparation            = $material_preparation->qty_conversion;
            $warehouse                  = $material_preparation->warehouse;
            $c_bpartner_id              = $material_preparation->c_bpartner_id;
            $uom_source                 = ($material_preparation->material_stock_id ? $material_preparation->materialStock->uom_source : $uom_conversion);
            $document_no                = strtoupper($material_preparation->document_no);
    
            
            // $is_po_buyer_cancel         = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();
            // if($is_po_buyer_cancel) return response()->json('Po buyer '.$po_buyer.' is cancel, please checkout to cancel order.',422);
           
            $allocation = AutoAllocation::find($auto_allocation_id);
            
            if($allocation)
            {
                $uom_purchase       = $allocation->uom;
                $qty_oustanding     = sprintf('%0.8f',$allocation->qty_oustanding);
                $qty_allocation     = sprintf('%0.8f',$allocation->qty_allocation);
                $qty_allocated      = sprintf('%0.8f',$allocation->qty_allocated);
                $qty_adjusment      = sprintf('%0.8f',$allocation->qty_adjusment);
                $total_qty_out      = sprintf('%0.8f',$allocation->getTotalQtyOut($allocation->id));
                $total_qty_prepare  = sprintf('%0.8f',$allocation->getTotalQtyPrepare($allocation->id));
                
                if($qty_oustanding == 0.00000000 && $total_qty_prepare == $qty_allocation)
                {
                    $allocation->is_reduce              = false;
                    $material_preparation->is_reduce    = false;
                    $material_preparation->save();

                    return response()->json('This barcode is not reduced, reduces flag is switch to non active',422);
                }else
                {
                    if($total_qty_out > 0 && $total_qty_out >= $qty_allocation && $qty_oustanding <= $qty_allocation) $qty_required = sprintf('%0.8f',$qty_preparation);
                    else $qty_required  = sprintf('%0.8f',$allocation->qty_allocation);
                }
                
            }else
            {
                $uom_purchase = $uom_conversion;
                $qty_required = 1;
            }
    
            $conversion = UomConversion::where([
                ['item_id',$item_id],
                ['uom_from',$uom_source],
                ['uom_to',$uom_conversion],
            ])
            ->first();
    
            if ($conversion) 
            {
                $dividerate     = $conversion->dividerate;
                $multiplyrate   = $conversion->multiplyrate;
            }else
            {
                $dividerate     = 1;
                $multiplyrate   = 1;
            }
    
            $qty_conversion = sprintf('%0.8f',$qty_required);
           
            $qty_adj                        = sprintf('%0.8f',$qty_preparation - $qty_conversion);
            $qty_after_adj                  = sprintf('%0.8f',$qty_preparation - $qty_adj);
            
            $obj                            = new StdClass();
            $obj->barcode                   = $barcode;
            $obj->material_preparation_id   = $material_preparation_id;
            $obj->auto_allocation_id        = $auto_allocation_id;
            $obj->document_no               = $document_no;
            $obj->item_code                 = $item_code;
            $obj->item_desc                 = $item_desc;
            $obj->category                  = $category;
            $obj->po_buyer                  = $po_buyer;
            $obj->uom_conversion            = $uom_conversion;
            $obj->qty_before_adj            = $qty_preparation;
            $obj->qty_need                  = $qty_conversion;
            $obj->qty_after_adj             = ($qty_adj < 0 )? null : sprintf('%0.8f',$qty_after_adj);
            $obj->qty_adj                   = sprintf('%0.8f',$qty_adj);
            $obj->warehouse                 = $warehouse;
            $obj->barcodes                  = [];
        }
        
        
        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'material_adjustments' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $material_adjustments   = json_decode($request->material_adjustments);
            $_warehouse_id          = $request->warehouse_id;
            $flag_insert            = 0;
            $rows_update            = '';
            $return_print           = array(); 
            $insert_moq             = array();
            $detail_material_stock_ids = array(); 
            

            $locator_adjustment_qc = Locator::whereHas('area',function($query) use ($_warehouse_id)
            {
                $query->where('name','ADJUSTMENT')
                ->where('warehouse',$_warehouse_id);
            })
            ->where('rack','ADJUSTMENT')
            ->first();

            $receiving_location = Locator::with('area')
            ->where('is_active',true)
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','RECEIVING');
            })
            ->first();

            $system = User::where([
                ['name','system'],
                ['warehouse',$_warehouse_id]
            ])
            ->first();
    
            $free_stock_destination = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where([
                    ['warehouse',$_warehouse_id],
                    ['name','FREE STOCK'],
                    ['is_destination',true]
                ]);
    
            })
            ->first();

            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            $movement_date = carbon::now()->toDateTimeString();
            try 
            {
                DB::beginTransaction();

                foreach ($material_adjustments as $key => $material_adjustment) 
                {
                    $barcodes               = $material_adjustment->barcodes;
                    $warehouse              = $material_adjustment->warehouse;
                    $auto_allocation_id     = $material_adjustment->auto_allocation_id;
                    $qty_adj                = sprintf('%0.8f',$material_adjustment->qty_adj);

                    if($material_adjustment->qty_adj > 0)
                    {
                        $material_preparations = MaterialPreparation::whereIn('barcode',$barcodes)
                        ->where('warehouse',$warehouse)
                        ->orderby('qty_conversion','asc')
                        ->get();

                        foreach ($material_preparations as $key => $value) 
                        {
                            if($qty_adj > 0)
                            {
                                $barcode                    = $value->barcode;
                                $material_preparation_id    = $value->id;
                                $po_detail_id               = $value->po_detail_id;
                                $po_buyer                   = $value->po_buyer;
                                $c_order_id                 = ($value->c_order_id)? $value->c_order_id : 'FREE STOCK';
                                $item_id                    = $value->item_id;
                                $item_id_source             = $value->item_id_source;
                                $item_code                  = $value->item_code;
                                $style                      = $value->style;
                                $article_no                 = $value->article_no;
                                $document_no                = strtoupper($value->document_no);
                                $item_desc                  = strtoupper($value->item_desc);
                                $supplier_name              = strtoupper($value->supplier_name);
                                $po_detail_id               = $value->po_detail_id;
                                $category                   = $value->category;
                                $last_locator_id            = $value->last_locator_id;
                                $last_locator_erp_id        = $value->lastLocator->erp_id;
                                $uom_conversion             = $value->uom_conversion;
                                $uom_source                 = ( $value->material_stock_id ? $value->materialStock->uom_source : $uom_conversion);
                                $qty_conversion             = sprintf('%0.8f',$value->qty_conversion);
                                $warehouse                  = $value->warehouse;
                                $c_bpartner_id              = $value->c_bpartner_id;

                                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();
                                    
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }
                                
                                if ($qty_adj/$qty_conversion >= 1) $_supply = $qty_conversion;
                                else $_supply = $qty_adj;

                                $new_qty_conversion = sprintf('%0.8f',$qty_conversion - $_supply);
                                
                                $conversion = $value->conversion($item_code,$category,$uom_source);
                                if($conversion) $multiplyrate = $conversion->multiplyrate;
                                else $multiplyrate = 1;

                                $material_movement_line = MaterialMovementLine::where([
                                    ['material_preparation_id',$material_preparation_id],
                                    ['is_active',true]
                                ])
                                ->first();

                                // masukin history
                                $is_movement_adjusment_exists = MaterialMovement::where([
                                    ['from_location',$last_locator_id],
                                    ['to_destination',$locator_adjustment_qc->id],
                                    ['from_locator_erp_id',$last_locator_erp_id],
                                    ['to_locator_erp_id',$locator_adjustment_qc->area->erp_id],
                                    ['po_buyer',$po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','adjusment'],
                                ])
                                ->first();
                                
                                if(!$is_movement_adjusment_exists)
                                {
                                    $movement_adjusment = MaterialMovement::firstOrCreate([
                                        'from_location'         => $last_locator_id,
                                        'to_destination'        => $locator_adjustment_qc->id,
                                        'from_locator_erp_id'   => $last_locator_erp_id,
                                        'to_locator_erp_id'     => $locator_adjustment_qc->area->erp_id,
                                        'po_buyer'              => $po_buyer,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'adjusment',
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
                                    ]);
            
                                    $movement_adjusment_id = $movement_adjusment->id;
                                }else
                                {
                                    $is_movement_adjusment_exists->updated_at = $movement_date;
                                    $is_movement_adjusment_exists->save();
            
                                    $movement_adjusment_id = $is_movement_adjusment_exists->id;
                                }

                                $is_material_movement_line_adjustment_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$movement_adjusment_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['qty_movement',sprintf('%0.8f',$new_qty_conversion)],
                                    ['item_id',$item_id],
                                    ['warehouse_id',$warehouse],
                                    ['date_movement',$movement_date],
                                ]) 
                                ->exists();

                                if(!$is_material_movement_line_adjustment_exists)
                                {
                                    $material_movement_line_adjustment = MaterialMovementLine::Create([
                                        'material_movement_id'          => $movement_adjusment_id,
                                        'material_preparation_id'       => $material_preparation_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'item_id_source'                => $item_id_source,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 2,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom_conversion,
                                        'qty_movement'                  => sprintf('%0.8f',$new_qty_conversion),
                                        'date_movement'                 => $movement_date,
                                        'created_at'                    => $movement_date,
                                        'updated_at'                    => $movement_date,
                                        'date_receive_on_destination'   => $movement_date,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $warehouse,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'ADJUSTMENT QTY SEBANYAK '.sprintf('%0.8f',$_supply).' [QTY AWAL NYA ADALAH '.$qty_conversion.']',
                                        'is_active'                     => true,
                                        'user_id'                       => auth::user()->id,
                                    ]);
                                }

                                // masukin intergasi buat adj erp
                                $is_movement_integration_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp->id],
                                    ['to_destination',$inventory_erp->id],
                                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['po_buyer',$po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();
                                
                                if(!$is_movement_integration_exists)
                                {
                                    $movement_integration = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp->id,
                                        'to_destination'        => $inventory_erp->id,
                                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                        'po_buyer'              => $po_buyer,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
                                    ]);
            
                                    $movement_integration_id = $movement_integration->id;
                                }else
                                {
                                    $is_movement_integration_exists->updated_at = $movement_date;
                                    $is_movement_integration_exists->save();
            
                                    $movement_integration_id = $is_movement_integration_exists->id;
                                }

                                $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$movement_integration_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['qty_movement',-1*sprintf('%0.8f',$_supply)],
                                    ['date_movement',$movement_date],
                                    ['item_id',$item_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['warehouse_id',$warehouse],
                                ]) 
                                ->exists();

                                if(!$is_material_movement_line_integration_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $movement_integration_id,
                                        'material_preparation_id'       => $material_preparation_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 2,
                                        'is_active'                     => true,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom_conversion,
                                        'qty_movement'                  => -1*sprintf('%0.8f',$_supply),
                                        'date_movement'                 => $movement_date,
                                        'created_at'                    => $movement_date,
                                        'updated_at'                    => $movement_date,
                                        'date_receive_on_destination'   => $movement_date,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $warehouse,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'ADJUSTMENT QTY SEBANYAK '.sprintf('%0.8f',$_supply).' [QTY AWAL NYA ADALAH '.$qty_conversion.']',
                                        'is_active'                     => true,
                                        'user_id'                       => auth::user()->id,
                                    ]);
                                }
                               
                                if($new_qty_conversion <=0)
                                {
                                    $locator_id         = $value->last_locator_id;
                                    $counter_out        = Locator::find($locator_id);
                                    $counter_minus      = $counter_out->counter_in;

                                    $value->deleted_at                          = $movement_date;
                                    $value->last_status_movement                = 'adjustment';
                                    $value->last_locator_id                     = $locator_adjustment_qc->id;
                                    $value->last_movement_date                  = $movement_date;
                                    $value->last_user_movement_id               = auth::user()->id;
                                    
                                    if($counter_minus > 0) $counter_out->counter_in = $counter_minus - 1;
                                    if($counter_minus == 0) $counter_out->counter_in = 0;
                                    $counter_out->save();
                                }
                                
                                $value->last_material_movement_line_id      = $material_movement_line_adjustment->id;
                                $value->qty_reconversion                    = sprintf('%0.8f',$new_qty_conversion * $multiplyrate);
                                $value->qty_conversion                      = sprintf('%0.8f',$new_qty_conversion);
                                $value->adjustment                          = sprintf('%0.8f',$_supply);
                                $value->is_moq                              = false;
                                $value->is_reduce                           = false;
                                $value->is_from_adjustment                  = true;
                                $value->adjustment_date                     = $movement_date;

                                // update klo dia handover
                                MaterialPreparation::where([
                                    ['id','!=',$value->id],
                                    ['barcode',$value->barcode],
                                    ['po_buyer',$value->po_buyer],
                                    ['item_id',$value->item_id],
                                ])
                                ->update([
                                    'qty_reconversion'  => sprintf('%0.8f',$new_qty_conversion * $multiplyrate),
                                    'qty_conversion'    => sprintf('%0.8f',$new_qty_conversion),
                                    'adjustment'        => sprintf('%0.8f',$_supply),
                                    'is_moq'            => false,
                                    'is_reduce'         => false,
                                ]);
                                
                                if($auto_allocation_id)
                                {
                                    $model_auto_allocation                  = AutoAllocation::find($auto_allocation_id);
                                    $qty_allocated                          = $model_auto_allocation->qty_allocated;
                                    $qty_outstanding                        = $model_auto_allocation->qty_outstanding;
                                    //$qty_adjustment                         = $model_auto_allocation->qty_adjustment;
                                    
                                    if($qty_outstanding < 0)
                                    {
                                        $new_qty_allocated                      = sprintf('%0.8f',$qty_allocated - $_supply);
                                        if($qty_outstanding < 0) $qty_outstanding = (-1)*$qty_outstanding;
                                        else $qty_outstanding = $qty_outstanding;

                                        $new_qty_outstanding                    = sprintf('%0.8f',$qty_outstanding - $_supply);
                                        //$new_qty_adjustment                     = sprintf('%0.8f',$qty_adjustment + $_supply);

                                        //$model_auto_allocation->qty_adjustment  = $new_qty_adjustment;
                                        $model_auto_allocation->qty_outstanding = $new_qty_outstanding;
                                        $model_auto_allocation->qty_allocated   = $new_qty_allocated;

                                        if($new_qty_allocated == $qty_allocated) $model_auto_allocation->is_reduce = false;
                                        $model_auto_allocation->save();
                                    }
                                    
                                }
                                
                                $value->save();
                                
                                $flag_insert++;
                                $return_print []            = $material_preparation_id;
                                
                                if($_supply > 0)
                                {
                                    $obj                        = new stdCLass();
                                    $obj->item_id               = $item_id;
                                    $obj->po_detail_id          = $po_detail_id;
                                    $obj->c_order_id            = $c_order_id;
                                    $obj->c_bpartner_id         = $c_bpartner_id;
                                    $obj->document_no           = $document_no;
                                    $obj->supplier_name         = $supplier_name;
                                    $obj->item_code             = $item_code;
                                    $obj->item_desc             = $item_desc;
                                    $obj->category              = $category;
                                    $obj->po_buyer              = $po_buyer;
                                    $obj->warehouse_id          = $warehouse;
                                    $obj->style                 = $style;
                                    $obj->article_no            = $article_no;
                                    $obj->uom                   = $uom_conversion;
                                    $obj->auto_allocation_id    = $auto_allocation_id;
                                    $obj->qty_adjusment = sprintf('%0.8f',$_supply);
                                    $insert_moq []      = $obj;
                                }
                                
                                $qty_adj -= $_supply;
                            }else
                            {
                                $value->is_moq              = false;
                                $value->save();
                            }
                        }
                    }else
                    {
                        $material_preparations = MaterialPreparation::whereIn('barcode',$barcodes)
                        ->where('warehouse',$warehouse)
                        ->get();
                        foreach ($material_preparations as $key => $value) 
                        {
                            $value->is_moq              = false;
                            $value->is_reduce           = false;
                            $value->save();
                            $return_print []            = $value->id;
                            //$value->is_moq              = false;
                            //$value->save();
                        }
                        
                    }
                }

                if(count($insert_moq))
                {
                    try 
                    {
                        DB::beginTransaction();
                        $upload_date                            =  Carbon::now()->toDateTimeString();
                        
                        foreach ($insert_moq as $key => $value)
                        {
                            $document_no                        = $value->document_no;
                            $po_detail_id                       = $value->po_detail_id;
                            $auto_allocation_id                 = $value->auto_allocation_id;
                            $item_id                            = $value->item_id;
                            $c_order_id                         = ($value->c_order_id)? $value->c_order_id : 'FREE STOCK';
                            $c_bpartner_id                      = $value->c_bpartner_id;
                            $document_no                        = $value->document_no;
                            $supplier_name                      = $value->supplier_name;
                            $item_code                          = $value->item_code;
                            $item_desc                          = $value->item_desc;
                            $category                           = $value->category;
                            $po_buyer                           = $value->po_buyer;
                            $style                              = $value->style;
                            $article_no                         = $value->article_no;
                            $warehouse_id                       = $value->warehouse_id;
                            $uom                                = $value->uom;
                            $qty_adjusment                      = sprintf('%0.8f',$value->qty_adjusment);

                            if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK') $supplier_code = 'FREE STOCK';
                            else
                            {
                                $supplier                       = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                if($supplier) $supplier_code    = $supplier->supplier_code;
                                else $supplier_code             = null;
                            }

                            if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK')
                            {
                                $c_bpartner_id                  = 'FREE STOCK';
                                $supplier_code                  = 'FREE STOCK';
                
                                $mapping_stock_id               = null;
                                $type_stock_erp_code            = '2';
                                $type_stock                     = 'REGULER';
                                
                            }else
                            {
                                
                                $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                if($_supplier) $supplier_code   = $_supplier->supplier_code;
                                else $supplier_code             = null;
                
                                $get_4_digit_from_beginning     = substr($document_no,0, 4);
                                $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                                
                                if($_mapping_stock_4_digt)
                                {
                                    $mapping_stock_id           = $_mapping_stock_4_digt->id;
                                    $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                                    $type_stock                 = $_mapping_stock_4_digt->type_stock;
                                }else
                                {
                                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                                    $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                    if($_mapping_stock_5_digt)
                                    {
                                        $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                                    }else
                                    {
                                        $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
                                        $mapping_stock_id       = null;
                                        $type_stock_erp_code    = ($_po_buyer)? $_po_buyer->type_stock_erp_code : NULL;
                                        $type_stock             = ($_po_buyer)? $_po_buyer->type_stock : null;
                                    }
                                    
                                }
                            }

                            $is_stock_already_created           = MaterialStock::where([
                                ['c_order_id',$c_order_id],
                                ['po_buyer',$po_buyer],
                                ['item_id',$item_id],
                                ['warehouse_id',$warehouse_id],
                                ['is_stock_on_the_fly',true],
                                ['is_running_stock',false],
                            ])
                            ->whereNull('deleted_at')
                            ->whereNotNull('po_buyer')
                            ->exists();

                            
                            $is_exists = MaterialStock::where([
                                'document_no'                   => $document_no,
                                'c_order_id'                    => $c_order_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'locator_id'                    => $free_stock_destination->id,
                                'po_buyer'                      => $po_buyer,
                                'item_id'                       => $item_id,
                                'item_code'                     => $item_code,
                                'type_po'                       => 2,
                                'warehouse_id'                  => $warehouse_id,
                                'source'                        => 'ADJ QTY',
                                'uom'                           => $uom,
                                'is_material_others'            => true,
                                'is_closing_balance'            => false,
                                'is_stock_already_created'      => $is_stock_already_created,
                                'type_stock_erp_code'           => $type_stock_erp_code,
                                'type_stock'                    => $type_stock,
                            ])
                            ->whereNull('deleted_at');

                            if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
                            if($auto_allocation_id) $is_exists = $is_exists->where('auto_allocation_id',$auto_allocation_id);
                            
                            $is_exists = $is_exists->first();

                            if(!$is_exists)
                            {
                                $material_stock = MaterialStock::FirstorCreate([
                                    'type_stock_erp_code'       => $type_stock_erp_code,
                                    'type_stock'                => $type_stock,
                                    'po_detail_id'              => $po_detail_id,
                                    'auto_allocation_id'        => $auto_allocation_id,
                                    'c_order_id'                => $c_order_id,
                                    'c_bpartner_id'             => $c_bpartner_id,
                                    'supplier_code'             => $supplier_code,
                                    'supplier_name'             => $supplier_name,
                                    'document_no'               => $document_no,
                                    'po_buyer'                  => $po_buyer,
                                    'locator_id'                => $free_stock_destination->id,
                                    'item_id'                   => $item_id,
                                    'item_code'                 => $item_code,
                                    'item_desc'                 => $item_desc,
                                    'category'                  => $category,
                                    'type_po'                   => 2,
                                    'warehouse_id'              => $warehouse_id,
                                    'uom'                       => $uom,
                                    'qty_carton'                => 1,
                                    'stock'                     => sprintf('%0.8f',$qty_adjusment),
                                    'reserved_qty'              => 0,
                                    'available_qty'             => sprintf('%0.8f',$qty_adjusment),
                                    'is_active'                 => true,
                                    'is_material_others'        => true,
                                    'is_closing_balance'        => false,
                                    'is_stock_already_created'  => $is_stock_already_created,
                                    'source'                    => 'ADJ QTY',
                                    'user_id'                   => Auth::user()->id,
                                    'created_at'                => $upload_date,
                                    'updated_at'                => $upload_date,
                                    'deleted_at'                => null,
                                ]);

                                $material_stock_id          = $material_stock->id;
                                $detail_material_stock  = DetailMaterialStock::FirstOrCreate([
                                    'material_stock_id' => $material_stock_id,
                                    'remark'            => strtoupper('ADJUTSMENT STOCK DARI PO BUYER '.$material_stock->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                                    'uom'               => $uom,
                                    'qty'               => sprintf('%0.8f',$qty_adjusment),
                                    'user_id'           => $material_stock->user_id,
                                    'created_at'        => $material_stock->created_at,
                                    'updated_at'        => $material_stock->created_at
                                ]);

                                $detail_material_stock_ids [] = $detail_material_stock->id;

                            }else
                            {
                               
                                $material_stock_id          = $is_exists->id;
                                $detail_material_stock = DetailMaterialStock::FirstOrCreate([
                                    'material_stock_id' => $material_stock_id,
                                    'remark'            => strtoupper('ADJUTSMENT STOCK DARI PO BUYER '.$is_exists->po_buyer.', STYLE '.$style.' DAN ARTICLE '.$article_no),
                                    'uom'               => $uom,
                                    'qty'               => sprintf('%0.8f',$qty_adjusment),
                                    'user_id'           => Auth::user()->id,
                                    'created_at'        => $upload_date,
                                    'updated_at'        => $upload_date
                                ]);

                                $detail_material_stock_ids [] = $detail_material_stock->id;

                            }
                            
                            $return_print []                    = $material_stock_id;
                        }

                        DB::commit();
                    } catch (Exception $e) 
                    {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }

                    //approve stock
                    foreach($detail_material_stock_ids as $key => $detail_material_stock_id)
                    {
                        HistoryStock::itemApprove($detail_material_stock_id, 'detail_material_stocks');
                    }

                }
                
                
                $_return_print = '';
                foreach (array_unique($return_print) as $key => $value) 
                {
                    $_return_print .= $value.',';
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            

            return response()->json([
                'data_print' => substr($_return_print,0,-1)
            ],200);

        }else{
            return response()->json('Please scan barcode first.',422);
        }
    }

    public function printout(request $request)
    {
        $array = array();
        $id = trim($request->id,'"');
        $splits =  explode(',',$id);
        
        
        foreach ($splits as $key => $value) 
        {
            $array [] = $value;
        }
       
        $items          = MaterialPreparation::whereIn('id',$array)->get();
        $item_stocks    = MaterialStock::whereIn('id',$array)->whereNull('deleted_at')->get();
    
        return view('accessories_barcode_adjustment.barcode', compact('items','item_stocks'));
    }
}
