<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\Item;
use App\Models\Locator;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\MaterialStock;
use App\Models\MaterialSaving;
use App\Models\MaterialArrival;
use App\Models\AllocationItem;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialMovementPerSize;
use App\Models\BarcodePreparationFabric;
use App\Models\MonitoringReceivingFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\MaterialRequirementPerSize;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPreparationFabric;

class FabricMaterialOutCuttingController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_material_out_cutting.index');
        // return view('errors.migration');
    }

    public function create(Request $request)
    {
        $_warehouse_id                      = $request->warehouse_id;
        $barcode                            = strtoupper(trim($request->barcode));

        $detail_material_preparation_fabric = DetailMaterialPreparationFabric::where([
            ['barcode',$barcode],
            ['warehouse_id',$_warehouse_id],
        ])
        ->whereNull('deleted_at')
        ->orderby('created_at','desc')
        ->first();

        if($detail_material_preparation_fabric)
        {
            if($detail_material_preparation_fabric->deleted_at) return response()->json('Barcode already supplied.', 422);
            
            $detail_material_preparation_fabric_id  = $detail_material_preparation_fabric->id;
            $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
            $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
            $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
            $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
            $po_detail_id                           = $detail_material_preparation_fabric->materialStock->po_detail_id;
            $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
            $locator_from_code                      = $detail_material_preparation_fabric->locator->code;
            $barcode                                = $detail_material_preparation_fabric->barcode;
            $reserved_qty                           = sprintf('%0.8f',$detail_material_preparation_fabric->reserved_qty);
            $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
            $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
            $c_order_id                             = $detail_material_preparation_fabric->materialStock->c_order_id;
            $item_id                                = $detail_material_preparation_fabric->materialStock->item_id;
            $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
            $item                                   = Item::where('item_id', $item_id)->first();
            $is_required_lot                        = $item->required_lot;
            $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
            $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
            $color                                  = $detail_material_preparation_fabric->materialStock->color;
            $actual_width                           = $detail_material_preparation_fabric->materialStock->actual_width;
            $is_additional                          = $detail_material_preparation_fabric->materialPreparationFabric->is_from_additional;
            $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;
            $is_piping                              = $detail_material_preparation_fabric->materialPreparationFabric->is_piping;
            $is_receive_piping                      = $detail_material_preparation_fabric->receive_piping_user_id;
            $ci_approval_status                     = $detail_material_preparation_fabric->ci_approval_status;

            if ($is_piping == true and $warehouse_id =='1000011' and $is_receive_piping == null) return response()->json('Barcode Belum Receive Piping.', 422);
            //lock cutting intruction approval activekan kalau trialnya sudah selesai
            //if($ci_approval_status != 'approved') return response()->json('CI belum di approve.', 422);
            if($is_piping)
            {
                $distribution = Locator::whereHas('area',function($query) use ($_warehouse_id)
                {
                    $query->where([
                        ['name','DISTRIBUTION'],
                        ['warehouse',$_warehouse_id],
                    ]);
                })
                ->first();

                $destination_id     = $distribution->id;
                $destination_name   = trim($distribution->code);
            }else
            {
                $cutting = Locator::whereHas('area',function($query) use ($_warehouse_id)
                {
                    $query->where([
                        ['name','CUTTING'],
                        ['warehouse',$_warehouse_id],
                    ]);
                })
                ->first();

                $destination_id     = $cutting->id;
                $destination_name   = trim($cutting->code);
            }

            if ($is_required_lot==false)
            {
                $actual_lot                         = "Not Required Lot";
                $is_already_have_actual_lot         = false;
            }else
            {
                $actual_lot                         = $detail_material_preparation_fabric->materialStock->load_actual;
                $is_already_have_actual_lot         = ($detail_material_preparation_fabric->materialStock->load_actual)? false:true;
            }

            $uom                                    = trim($detail_material_preparation_fabric->materialStock->uom);
            $_planning_date                         = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date) ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->todateTimeString() : $detail_material_preparation_fabric->materialPreparationFabric->planning_date;
            $planning_date                          = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->format('d/M/Y') : $detail_material_preparation_fabric->materialPreparationFabric->planning_date);
            $additional_note                        = ($detail_material_preparation_fabric->materialPreparationFabric->additional_note ? $detail_material_preparation_fabric->materialPreparationFabric->additional_note : null);

            $obj                                    = new stdClass();
            $obj->id                                = $detail_material_preparation_fabric_id;
            $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
            $obj->_planning_date                    = $_planning_date;
            $obj->planning_date                     = $planning_date;
            $obj->additional_note                   = $additional_note;
            $obj->material_stock_id                 = $material_stock_id;
            $obj->locator_from_id                   = $last_locator_id;
            $obj->locator_from_code                 = $locator_from_code;
            $obj->c_order_id                        = $c_order_id;
            $obj->item_id                           = $item_id;
            $obj->barcode                           = $barcode;
            $obj->reserved_qty                      = $reserved_qty;
            $obj->warehouse_id                      = $warehouse_id;
            $obj->nomor_roll                        = $nomor_roll;
            $obj->batch_number                      = $batch_number;
            $obj->document_no                       = $document_no;
            $obj->item_code                         = $item_code;
            $obj->supplier_name                     = $supplier_name;
            $obj->c_bpartner_id                     = $c_bpartner_id;
            $obj->article_no                        = $article_no;
            $obj->color                             = $color;
            $obj->actual_width                      = $actual_width;
            $obj->actual_lot                        = $actual_lot;
            $obj->uom                               = $uom;
            $obj->po_detail_id                      = $po_detail_id;
            $obj->is_already_have_actual_lot        = $is_already_have_actual_lot;
            $obj->movement_date                     = Carbon::now()->toDateTimeString();
            $obj->destination_id                    = $destination_id;
            $obj->destination_name                  = $destination_name;
            $obj->qty_saving                        = null;
            $obj->is_error                          = false;
            $obj->error_note                        = null;
            $obj->remark_ict                        = null;
            $obj->is_for_handover                   = false;
            $obj->is_for_material_saving            = false;
            $obj->is_additional                     = $is_additional;
           
            return response()->json($obj, 200);
        }else
        {
            return response()->json('Barcode not found', 422);
        }
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $_warehouse_id              = $request->warehouse_id;
            $items                      = json_decode($request->barcode_products);
            $insert_material_savings    = array();
            $array_locator_old          = array();
           
            $inventory_erp              = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            try
            {
                db::beginTransaction();

                foreach ($items as $key => $value) 
                {
                    $barcode_preparation_fabric_id                              = $value->id;
                    $material_preparation_fabric_id                             = $value->material_preparation_fabric_id;
                    $locator_from_id                                            = $value->locator_from_id;
                    $is_error                                                   = $value->is_error;
                    $remark_ict                                                 = $value->remark_ict;
                    $movement_date                                              = $value->movement_date;
                    $material_stock_id                                          = $value->material_stock_id;
                    $warehouse_id                                               = $value->warehouse_id;
                    $po_detail_id                                               = $value->po_detail_id;
                    $nomor_roll                                                 = $value->nomor_roll;
                    $item_id                                                    = $value->item_id;
                    $item_code                                                  = $value->item_code;
                    $document_no                                                = $value->document_no;
                    $c_order_id                                                 = $value->c_order_id;
                    $c_bpartner_id                                              = $value->c_bpartner_id;
                    $supplier_name                                              = $value->supplier_name;
                    $additional_note                                            = $value->additional_note;
                    $_planning_date                                             = $value->_planning_date;
                    $planning_date                                              = $value->planning_date;
                    $uom                                                        = $value->uom;
                    $is_additional                                              = $value->is_additional;
                    $reserved_qty                                               = sprintf('%0.8f',$value->reserved_qty);
                    $qty_saving                                                 = sprintf('%0.8f',$value->qty_saving);
                    $destination_name                                           = $value->destination_name;
                    $destination_id                                             = $value->destination_id;
                    $barcode                                                    = $value->barcode;
                    $array_locator_old []                                       = $locator_from_id;

                    if($is_error == false)
                    {
                        $detail_material_preparation_fabric                         = DetailMaterialPreparationFabric::find($barcode_preparation_fabric_id);
                    

                        if($is_additional) $note = 'BARCODE '.$barcode.' DENGAN QTY '.$reserved_qty.', UNTUK ADDITIONAL, SUDAH DI KELUARKAN KE '.$destination_name;
                        else $note = 'BARCODE '.$barcode.' DENGAN QTY '.$reserved_qty.', UNTUK PCD '.$planning_date.', SUDAH DI KELUARKAN KE '.$destination_name;
                        
                        /* 
                        
                            ini harusnya ga ada, semua yg out harus ada po buyernya baik itu additional / gak. 
                            charis
                            
                            if($is_additional || $detail_material_preparation_fabric->detailPoBuyerPerRoll()->count() == 0)
                            {
                                if(in_array($po_detail_id,['FREE STOCK','FREE STOCK-CELUP']) )
                                {
                                    $no_packing_list        = null;
                                    $no_invoice             = null;
                                    $c_orderline_id         = null;
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();
                                    
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : null);
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : null);
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : null);
                                }

                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp->id],
                                    ['to_destination',$inventory_erp->id],
                                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();

                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp->id,
                                        'to_destination'        => $inventory_erp->id,
                                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $movement_date,
                                        'updated_at'            => $movement_date,
                                    ]);

                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->updated_at = $movement_date;
                                    $is_movement_exists->save();

                                    $material_movement_id = $is_movement_exists->id;
                                }

                                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['item_id',$item_id],
                                    ['warehouse_id',$warehouse_id],
                                    ['material_stock_id',$material_stock_id],
                                    ['qty_movement',-1*$reserved_qty],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['date_movement',$movement_date],
                                ])
                                ->exists();
                
                                if(!$is_material_movement_line_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_id,
                                        'material_stock_id'             => $material_stock_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => 1,
                                        'is_integrate'                  => false,
                                        'uom_movement'                  => $uom,
                                        'qty_movement'                  => -1*$reserved_qty,
                                        'date_movement'                 => $movement_date,
                                        'created_at'                    => $movement_date,
                                        'updated_at'                    => $movement_date,
                                        'date_receive_on_destination'   => $movement_date,
                                        'nomor_roll'                    => $nomor_roll,
                                        'warehouse_id'                  => $warehouse_id,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'ROLL DIKELUARKAN UNTUK ADDITIONAL UNTUK KEGUNAAN '.$additional_note,
                                        'is_active'                     => true,
                                        'user_id'                       => auth::user()->id,
                                    ]);
                                }
                            }

                        */
                        
                        MovementStockHistory::create([
                            'material_stock_id'         => $material_stock_id,
                            'from_location'             => $locator_from_id,
                            'to_destination'            => $destination_id,
                            'status'                    => 'out-cutting',
                            'uom'                       => $uom,
                            'qty'                       => $reserved_qty,
                            'movement_date'             => $movement_date,
                            'note'                      => $note,
                            'user_id'                   => auth::user()->id
                        ]);

                        $detail_material_preparation_fabric->last_status_movement   = 'out';
                        $detail_material_preparation_fabric->remark_ict             = $remark_ict;
                        $detail_material_preparation_fabric->qty_saving             = $qty_saving;
                        $detail_material_preparation_fabric->last_locator_id        = $destination_id;
                        $detail_material_preparation_fabric->last_movement_date     = $movement_date;
                        $detail_material_preparation_fabric->movement_out_date      = $movement_date;
                        $detail_material_preparation_fabric->deleted_at             = $movement_date;
                        $detail_material_preparation_fabric->remark                 = 'SUDAH SUPPLAI KE '.$destination_name;
                        $detail_material_preparation_fabric->last_user_movement_id  = auth::user()->id;
                        $detail_material_preparation_fabric->save();

                    }
                }

                $locator = Locator::whereIn('id',$array_locator_old)->get();
                foreach ($locator as $key => $value) 
                {
                    $count_item_on_locator = MaterialStock::where([
                        ['locator_id',$value->id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();
            
                    $value->counter_in = $count_item_on_locator;
                    if($count_item_on_locator == 0) $value->last_po_buyer = null;
                    
                    $value->save();
                }

                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
            }
            

            

        }else
        {
            return response()->json('Please scan barcode first.',400); 
        }
        
        return response()->json(200);
    }

    public function downloadFormUpload()
    {
        return Excel::create('form_upload',function ($excel) 
        {
            $excel->sheet('active', function($sheet)
            {
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','REMARK_ICT');
            });
        })
        ->export('xlsx');
    }

    public function uploadFormUpload(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $_warehouse_id   = $request->upload_warehouse_id;
            $movement_date  = Carbon::now()->toDateTimeString();

            foreach ($data as $key => $value) 
            {
                $remark_ict = strtoupper(trim($value->remark_ict));
                $id         = $value->id;
                $detail_material_preparation_fabric = DetailMaterialPreparationFabric::where([
                    ['id',$id],
                    ['warehouse_id',$_warehouse_id],
                ])
                ->orderby('created_at','desc')
                ->first();

                if($detail_material_preparation_fabric)
                {
                    $detail_material_preparation_fabric_id = $detail_material_preparation_fabric->id;
                    $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
                    $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
                    $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
                    $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
                    $po_detail_id                           = $detail_material_preparation_fabric->materialStock->po_detail_id;
                    $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
                    $locator_from_code                      = $detail_material_preparation_fabric->locator->code;
                    $barcode                                = $detail_material_preparation_fabric->barcode;
                    $reserved_qty                           = sprintf('%0.8f',$detail_material_preparation_fabric->reserved_qty);
                    $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
                    $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
                    $c_order_id                             = $detail_material_preparation_fabric->materialStock->c_order_id;
                    $item_id                                = $detail_material_preparation_fabric->materialStock->item_id;
                    $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
                    $item                                   = Item::where('item_id', $item_id)->first();
                    $is_required_lot                        = $item->required_lot;
                    $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
                    $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
                    $color                                  = $detail_material_preparation_fabric->materialStock->color;
                    $actual_width                           = $detail_material_preparation_fabric->materialStock->actual_width;
                    $is_additional                          = $detail_material_preparation_fabric->materialPreparationFabric->is_from_additional;
                    $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;
                    $is_piping                              = $detail_material_preparation_fabric->materialPreparationFabric->is_piping;
                    $is_receive_piping                      = $detail_material_preparation_fabric->receive_piping_user_id;

                    if($is_piping)
                    {
                        $distribution = Locator::whereHas('area',function($query) use ($warehouse_id)
                        {
                            $query->where([
                                ['name','DISTRIBUTION'],
                                ['warehouse',$warehouse_id],
                            ]);
                        })
                        ->first();

                        $destination_id     = $distribution->id;
                        $destination_name   = trim($distribution->code);
                    }else
                    {
                        $cutting = Locator::whereHas('area',function($query) use ($warehouse_id)
                        {
                            $query->where([
                                ['name','CUTTING'],
                                ['warehouse',$warehouse_id],
                            ]);
                        })
                        ->first();

                        $destination_id     = $cutting->id;
                        $destination_name   = trim($cutting->code);
                    }

                    if ($is_required_lot==false)
                    {
                        $actual_lot                         = "Not Required Lot";
                        $is_already_have_actual_lot         = false;
                    }else
                    {
                        $actual_lot                         = $detail_material_preparation_fabric->materialStock->load_actual;
                        $is_already_have_actual_lot         = ($detail_material_preparation_fabric->materialStock->load_actual)? false:true;
                    }

                    $uom                                    = trim($detail_material_preparation_fabric->materialStock->uom);
                    $_planning_date                         = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date) ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->todateTimeString() : $detail_material_preparation_fabric->materialPreparationFabric->planning_date;
                    $planning_date                          = ($detail_material_preparation_fabric->materialPreparationFabric->planning_date ? $detail_material_preparation_fabric->materialPreparationFabric->planning_date->format('d/M/Y') : $detail_material_preparation_fabric->materialPreparationFabric->planning_date);
                    $additional_note                        = ($detail_material_preparation_fabric->materialPreparationFabric->additional_note ? $detail_material_preparation_fabric->materialPreparationFabric->additional_note : null);

                    
                    if($detail_material_preparation_fabric->deleted_at)
                    {
                        $obj                                    = new stdClass();
                        $obj->id                                = $detail_material_preparation_fabric_id;
                        $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
                        $obj->_planning_date                    = $_planning_date;
                        $obj->planning_date                     = $planning_date;
                        $obj->additional_note                   = $additional_note;
                        $obj->material_stock_id                 = $material_stock_id;
                        $obj->locator_from_id                   = $last_locator_id;
                        $obj->locator_from_code                 = $locator_from_code;
                        $obj->c_order_id                        = $c_order_id;
                        $obj->item_id                           = $item_id;
                        $obj->barcode                           = $barcode;
                        $obj->reserved_qty                      = $reserved_qty;
                        $obj->warehouse_id                      = $warehouse_id;
                        $obj->nomor_roll                        = $nomor_roll;
                        $obj->batch_number                      = $batch_number;
                        $obj->document_no                       = $document_no;
                        $obj->item_code                         = $item_code;
                        $obj->supplier_name                     = $supplier_name;
                        $obj->c_bpartner_id                     = $c_bpartner_id;
                        $obj->article_no                        = $article_no;
                        $obj->color                             = $color;
                        $obj->actual_width                      = $actual_width;
                        $obj->actual_lot                        = $actual_lot;
                        $obj->uom                               = $uom;
                        $obj->po_detail_id                      = $po_detail_id;
                        $obj->is_already_have_actual_lot        = $is_already_have_actual_lot;
                        $obj->movement_date                     = $movement_date;
                        $obj->destination_id                    = $destination_id;
                        $obj->destination_name                  = $destination_name;
                        $obj->qty_saving                        = null;
                        $obj->is_error                          = true;
                        $obj->error_note                        = 'Barcode already supplied.';
                        $obj->remark_ict                        = $remark_ict;
                        $obj->is_for_handover                   = false;
                        $obj->is_for_material_saving            = false;
                        $obj->is_additional                     = $is_additional;
                        $array [] = $obj;
                    }else
                    {
                        if ($is_piping == true and $warehouse_id =='1000011' and $is_receive_piping == null)
                        {
                            $obj                                    = new stdClass();
                            $obj->id                                = $detail_material_preparation_fabric_id;
                            $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
                            $obj->_planning_date                    = $_planning_date;
                            $obj->planning_date                     = $planning_date;
                            $obj->additional_note                   = $additional_note;
                            $obj->material_stock_id                 = $material_stock_id;
                            $obj->locator_from_id                   = $last_locator_id;
                            $obj->locator_from_code                 = $locator_from_code;
                            $obj->c_order_id                        = $c_order_id;
                            $obj->item_id                           = $item_id;
                            $obj->barcode                           = $barcode;
                            $obj->reserved_qty                      = $reserved_qty;
                            $obj->warehouse_id                      = $warehouse_id;
                            $obj->nomor_roll                        = $nomor_roll;
                            $obj->batch_number                      = $batch_number;
                            $obj->document_no                       = $document_no;
                            $obj->item_code                         = $item_code;
                            $obj->supplier_name                     = $supplier_name;
                            $obj->c_bpartner_id                     = $c_bpartner_id;
                            $obj->article_no                        = $article_no;
                            $obj->color                             = $color;
                            $obj->actual_width                      = $actual_width;
                            $obj->actual_lot                        = $actual_lot;
                            $obj->uom                               = $uom;
                            $obj->po_detail_id                      = $po_detail_id;
                            $obj->is_already_have_actual_lot        = $is_already_have_actual_lot;
                            $obj->movement_date                     = $movement_date;
                            $obj->destination_id                    = $destination_id;
                            $obj->destination_name                  = $destination_name;
                            $obj->qty_saving                        = null;
                            $obj->is_error                          = true;
                            $obj->error_note                        = 'Barcode Belum Receive Piping';
                            $obj->remark_ict                        = $remark_ict;
                            $obj->is_for_handover                   = false;
                            $obj->is_for_material_saving            = false;
                            $obj->is_additional                     = $is_additional;
                            $array [] = $obj;
                        }else
                        {
                            $obj                                    = new stdClass();
                            $obj->id                                = $detail_material_preparation_fabric_id;
                            $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
                            $obj->_planning_date                    = $_planning_date;
                            $obj->planning_date                     = $planning_date;
                            $obj->additional_note                   = $additional_note;
                            $obj->material_stock_id                 = $material_stock_id;
                            $obj->locator_from_id                   = $last_locator_id;
                            $obj->locator_from_code                 = $locator_from_code;
                            $obj->c_order_id                        = $c_order_id;
                            $obj->item_id                           = $item_id;
                            $obj->barcode                           = $barcode;
                            $obj->reserved_qty                      = $reserved_qty;
                            $obj->warehouse_id                      = $warehouse_id;
                            $obj->nomor_roll                        = $nomor_roll;
                            $obj->batch_number                      = $batch_number;
                            $obj->document_no                       = $document_no;
                            $obj->item_code                         = $item_code;
                            $obj->supplier_name                     = $supplier_name;
                            $obj->c_bpartner_id                     = $c_bpartner_id;
                            $obj->article_no                        = $article_no;
                            $obj->color                             = $color;
                            $obj->actual_width                      = $actual_width;
                            $obj->actual_lot                        = $actual_lot;
                            $obj->uom                               = $uom;
                            $obj->po_detail_id                      = $po_detail_id;
                            $obj->is_already_have_actual_lot        = $is_already_have_actual_lot;
                            $obj->movement_date                     = $movement_date;
                            $obj->destination_id                    = $destination_id;
                            $obj->destination_name                  = $destination_name;
                            $obj->qty_saving                        = null;
                            $obj->is_error                          = false;
                            $obj->error_note                        = null;
                            $obj->remark_ict                        = $remark_ict;
                            $obj->is_for_handover                   = false;
                            $obj->is_for_material_saving            = false;
                            $obj->is_additional                     = $is_additional;
                            $array [] = $obj;
                        }
                    }
                }else
                {
                    $obj                                    = new stdClass();
                    $obj->id                                = null;
                    $obj->material_preparation_fabric_id    = null;
                    $obj->_planning_date                    = null;
                    $obj->planning_date                     = null;
                    $obj->additional_note                   = null;
                    $obj->material_stock_id                 = null;
                    $obj->locator_from_id                   = null;
                    $obj->locator_from_code                 = null;
                    $obj->c_order_id                        = null;
                    $obj->item_id                           = null;
                    $obj->barcode                           = null;
                    $obj->reserved_qty                      = null;
                    $obj->warehouse_id                      = null;
                    $obj->nomor_roll                        = null;
                    $obj->batch_number                      = null;
                    $obj->document_no                       = null;
                    $obj->item_code                         = null;
                    $obj->supplier_name                     = null;
                    $obj->c_bpartner_id                     = null;
                    $obj->article_no                        = null;
                    $obj->color                             = null;
                    $obj->actual_width                      = null;
                    $obj->actual_lot                        = null;
                    $obj->uom                               = null;
                    $obj->po_detail_id                      = null;
                    $obj->is_already_have_actual_lot        = null;
                    $obj->movement_date                     = null;
                    $obj->destination_id                    = null;
                    $obj->destination_name                  = null;
                    $obj->qty_saving                        = null;
                    $obj->is_error                          = true;
                    $obj->error_note                        = 'ID '.$id.' not found.';
                    $obj->remark_ict                        = $remark_ict;
                    $obj->is_for_handover                   = null;
                    $obj->is_for_material_saving            = null;
                    $obj->is_additional                     = null;
                    $array [] = $obj;
                }
            }

            return response()->json($array,200);
        }
        
    }


}