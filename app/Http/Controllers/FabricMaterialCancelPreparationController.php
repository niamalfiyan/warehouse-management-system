<?php namespace App\Http\Controllers;

use DB;
use Auth;
use stdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Locator;
use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPreparationFabric;

use App\Http\Controllers\AccessoriesMaterialReverseOutController as ReverseOut;

class FabricMaterialCancelPreparationController extends Controller
{
    public function index()
    {
        return view('fabric_material_cancel_preparation.index');
    }

    public function create(Request $request)
    {
        $warehouse_id                       = $request->warehouse_id;
        $barcode                            = strtoupper($request->barcode);

        $detail_material_preparation_fabric = DetailMaterialPreparationFabric::where([
            ['barcode',$barcode],
            ['is_closing',false],
            ['warehouse_id',$warehouse_id],
          ])
        ->first();

        if($detail_material_preparation_fabric)
        {
            $detail_material_preparation_fabric_id  = $detail_material_preparation_fabric->id;
            $material_stock_id                      = $detail_material_preparation_fabric->material_stock_id;
            $material_preparation_fabric_id         = $detail_material_preparation_fabric->material_preparation_fabric_id;
            $summary_stock_fabric_id                = $detail_material_preparation_fabric->materialStock->summary_stock_fabric_id;
            $document_no                            = strtoupper($detail_material_preparation_fabric->materialStock->document_no);
            $batch_number                           = $detail_material_preparation_fabric->materialStock->batch_number;
            $barcode                                = $detail_material_preparation_fabric->barcode;
            $reserved_qty                           = $detail_material_preparation_fabric->reserved_qty;
            $warehouse_id                           = $detail_material_preparation_fabric->warehouse_id;
            $nomor_roll                             = $detail_material_preparation_fabric->materialStock->nomor_roll;
            $item_code                              = strtoupper($detail_material_preparation_fabric->materialStock->item_code);
            $supplier_name                          = $detail_material_preparation_fabric->materialStock->supplier_name;
            $c_bpartner_id                          = $detail_material_preparation_fabric->materialStock->c_bpartner_id;
            $color                                  = $detail_material_preparation_fabric->materialStock->color;
            $begin_width                            = $detail_material_preparation_fabric->begin_width;
            $middle_width                           = $detail_material_preparation_fabric->middle_width;
            $end_width                              = $detail_material_preparation_fabric->end_width;
            $actual_width                           = $detail_material_preparation_fabric->actual_width;
            $actual_lot                             = $detail_material_preparation_fabric->materialStock->load_actual;
            $uom                                    = trim($detail_material_preparation_fabric->uom);
            $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
            $article_no                             = $detail_material_preparation_fabric->materialPreparationFabric->article_no;
            
            $obj                                    = new stdClass();
            $obj->id                                = $detail_material_preparation_fabric_id;
            $obj->material_stock_id                 = $material_stock_id;
            $obj->summary_stock_fabric_id           = $summary_stock_fabric_id;
            $obj->material_preparation_fabric_id    = $material_preparation_fabric_id;
            $obj->last_locator_id                   = $last_locator_id;
            $obj->barcode                           = $barcode;
            $obj->total_qty_preparation             = $reserved_qty;
            $obj->warehouse_id                      = $warehouse_id;
            $obj->nomor_roll                        = $nomor_roll;
            $obj->batch_number                      = $batch_number;
            $obj->document_no                       = $document_no;
            $obj->item_code                         = $item_code;
            $obj->supplier_name                     = $supplier_name;
            $obj->c_bpartner_id                     = $c_bpartner_id;
            $obj->article_no                        = $article_no;
            $obj->color                             = $color;
            $obj->actual_width                      = $actual_width;
            $obj->begin_width                       = $begin_width;
            $obj->middle_width                      = $middle_width;
            $obj->end_width                         = $end_width;
            $obj->actual_lot                        = $actual_lot;
            $obj->uom                               = $uom;
            $obj->movement_date                     = Carbon::now()->toDateTimeString();
           
            return response()->json($obj, 200);
        }else
        {
            return response()->json('Barcode not found.', 422);
        }
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'items' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $warehouse_id       = $request->warehouse_id;
            $items              = json_decode($request->items);
            
            $this->doStore($warehouse_id,$items);
            
            return response()->json(200);
        }else
        {
            return response()->json('Please scan barcode first.',400); 
        }
    }

    static function doStore($warehouse_id,$items)
    {
        $inventory  = Locator::where('rack','INVENTORY')
        ->whereHas('area',function ($query) use ($warehouse_id)
        {
            $query->where('name','INVENTORY')
            ->where('warehouse',$warehouse_id);
        })
        ->first();

        $inventory_erp = Locator::with('area')
        ->where([
            ['is_active',true],
            ['rack','ERP INVENTORY'],
        ])
        ->whereHas('area',function ($query) use($warehouse_id)
        {
            $query->where('is_active',true);
            $query->where('warehouse',$warehouse_id);
            $query->where('name','ERP INVENTORY');
        })
        ->first();

        $locator_source                 = array();
        $locator_to                     = array();
        $delete_material_movement_lines = array();
        $summary_stock_fabrics          = array();
        $material_preparation_fabrics   = array();
        $concatenate                    = '';

        try 
        {
            DB::beginTransaction();
        
            foreach ($items as $key => $item) 
            {
                $detail_material_preparation_fabric_id  = $item->id;
                $material_stock_id                      = $item->material_stock_id;
                $material_preparation_fabric_id         = $item->material_preparation_fabric_id;
                $movement_date                          = $item->movement_date;
                
                $material_preparation_fabric            = MaterialPreparationFabric::find($material_preparation_fabric_id);
                $planning_date                          = $material_preparation_fabric->planning_date;
                $is_additonal                           = $material_preparation_fabric->is_from_additional;

                $detail_material_preparation_fabric     = DetailMaterialPreparationFabric::find($detail_material_preparation_fabric_id);
                $last_locator_id                        = $detail_material_preparation_fabric->last_locator_id;
                $movement_out_date                      = $detail_material_preparation_fabric->movement_out_date;
                $barcode                                = $detail_material_preparation_fabric->barcode;
                $sequence                               = $detail_material_preparation_fabric->sequence;
                $referral_code                          = $detail_material_preparation_fabric->referral_code;
                $qty_preparation                        = sprintf('%0.8f',$detail_material_preparation_fabric->reserved_qty);
                
                $material_stock                         = MaterialStock::find($material_stock_id);
                $reserved_qty                           = sprintf('%0.8f',$material_stock->reserved_qty);
                $qty_order                              = sprintf('%0.8f',$material_stock->qty_order);
                $stock                                  = sprintf('%0.8f',$material_stock->stock);
                $barcode_parent                         = $material_stock->barcode_supplier;
                $item_id                                = $material_stock->item_id;
                $po_detail_id                           = $material_stock->po_detail_id;
                $no_packing_list                        = $material_stock->no_packing_list;
                $no_invoice                             = $material_stock->no_invoice;
                $c_order_id                             = $material_stock->c_order_id;
                $batch_number                           = $material_stock->batch_number;
                $nomor_roll                             = $material_stock->nomor_roll;
                $uom                                    = $material_stock->uom;
                $document_no                            = $material_stock->document_no;
                $c_bpartner_id                          = $material_stock->c_bpartner_id;
                $warehouse_id                           = $material_stock->warehouse_id;
                $summary_stock_fabric_id                = $material_stock->summary_stock_fabric_id;
                $item_code                              = $material_stock->item_code;
                $item_desc                              = $material_stock->item_desc;
                $color                                  = $material_stock->color;
                $upc                                    = $material_stock->upc;
                $category                               = $material_stock->category;
                $supplier_name                          = $material_stock->supplier_name;
                $supplier_code                          = $material_stock->supplier_code;
                $load_actual                            = $material_stock->load_actual;
                $material_roll_handover_fabric_id       = $material_stock->material_roll_handover_fabric_id;
                $summary_handover_material_id           = $material_stock->summary_handover_material_id;
                $c_order_id                             = $material_stock->c_order_id;
                $jenis_po                               = $material_stock->jenis_po;
                $qc_result                              = $material_stock->qc_result;
                $po_detail_id                           = $material_stock->po_detail_id;
                $begin_width                            = $material_stock->begin_width;
                $middle_width                           = $material_stock->middle_width;
                $end_width                              = $material_stock->end_width;
                $actual_width                           = $material_stock->actual_width;
                $actual_length                          = $material_stock->actual_length;
                $different_yard                         = $material_stock->different_yard;
                $inspect_lab_date                       = $material_stock->inspect_lab_date;
                $user_lab_id                            = $material_stock->user_lab_id;
                $inspect_lab_remark                     = $material_stock->inspect_lab_remark;
                $is_from_handover                       = $material_stock->is_from_handover;
                $inspect_lot_result                     = $material_stock->inspect_lot_result;
                $monitoring_receiving_fabric_id         = $material_stock->monitoring_receiving_fabric_id;
                $upc_item                               = $material_stock->upc_item;
                $type_stock_erp_code                    = $material_stock->type_stock_erp_code;
                $type_stock                             = $material_stock->type_stock;
                $new_qty_order                          = sprintf('%0.8f',$qty_order - $qty_preparation);

               
                
                if($is_additonal) $note_cancel = ' YANG DIGUNAKAN UNTUK ADDITONAL ';
                else $note_cancel = ' YANG DIGUNAKAN UNTUK PLANNING '.$planning_date->format('d/M/y');

                $material_stock->qty_order   = $new_qty_order;

                // buat nambah stock
                //insert new roll
                $is_stock_exists = MaterialStock::where([
                    ['locator_id',$inventory->id],
                    ['summary_stock_fabric_id',$summary_stock_fabric_id],
                    ['barcode_supplier',$barcode],
                    ['item_id',$item_id],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['c_order_id',$c_order_id],
                    ['batch_number',$batch_number],
                    ['nomor_roll',$nomor_roll],
                    ['document_no',$document_no],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse_id',$warehouse_id],
                    ['stock',$qty_preparation],
                    ['is_material_others',true],
                    ['is_roll_cancel',true],
                    ['type_po',1],
                    ['source','CANCEL PREPARATION '.$note_cancel.', INDUK '.$barcode_parent],
                ])
                ->first();

                //dd($is_stock_exists);
                if(!$is_stock_exists)
                {

                    $new_material_stock = MaterialStock::UpdateOrCreate([
                        'locator_id'                            => $inventory->id,
                        'barcode_supplier'                      => $barcode,
                        'referral_code'                         => $referral_code,
                        'sequence'                              => $sequence,
                        'monitoring_receiving_fabric_id'        => $monitoring_receiving_fabric_id,
                        'po_detail_id'                          => $po_detail_id,
                        'item_code'                             => $item_code,
                        'item_desc'                             => $item_desc,
                        'user_lab_id'                           => $user_lab_id,
                        'color'                                 => $color,
                        'category'                              => $category,
                        'no_packing_list'                       => $no_packing_list,
                        'inspect_lab_remark'                    => $inspect_lab_remark,
                        'no_invoice'                            => $no_invoice,
                        'load_actual'                           => $load_actual,
                        'qc_result'                             => $qc_result,
                        'is_from_handover'                      => $is_from_handover,
                        'upc_item'                              => $upc_item,
                        'actual_length'                         => $actual_length,
                        'type_po'                               => 1,
                        'c_order_id'                            => $c_order_id,
                        'item_id'                               => $item_id,
                        'begin_width'                           => $begin_width,
                        'middle_width'                          => $middle_width,
                        'end_width'                             => $end_width,
                        'actual_width'                          => $actual_width,
                        'inspect_lab_date'                      => $inspect_lab_date,
                        'inspect_lot_result'                    => $inspect_lot_result,
                        'qty_order'                             => $qty_preparation,
                        'qty_arrival'                           => $qty_preparation,
                        'type_stock_erp_code'                   => $type_stock_erp_code,
                        'type_stock'                            => $type_stock,
                        'summary_stock_fabric_id'               => $summary_stock_fabric_id,
                        'stock'                                 => $qty_preparation,
                        'available_qty'                         => $qty_preparation,
                        'batch_number'                          => $batch_number,
                        'is_roll_cancel'                        => true,
                        'nomor_roll'                            => $nomor_roll,
                        'uom'                                   => $uom,
                        'different_yard'                        => $different_yard,
                        'source'                                => 'CANCEL PREPARATION '.$note_cancel.', INDUK '.$barcode_parent,
                        'is_active'                             => true,
                        'is_material_others'                    => true,
                        'supplier_name'                         => $supplier_name,
                        'document_no'                           => $document_no,
                        'c_bpartner_id'                         => $c_bpartner_id,
                        'supplier_code'                         => $supplier_code,
                        'warehouse_id'                          => $warehouse_id,
                        'material_roll_handover_fabric_id'      => $material_roll_handover_fabric_id,
                        'summary_handover_material_id'          => $summary_handover_material_id,
                        'jenis_po'                              => $jenis_po,
                        'user_id'                               => auth::user()->id,
                        'approval_user_id'                      => auth::user()->id,
                        'approval_date'                         => $movement_date,
                        'created_at'                            => $movement_date,
                        'updated_at'                            => $movement_date,
                    ]);

                    MovementStockHistory::UpdateOrCreate([
                        'material_stock_id'     => $new_material_stock->id,
                        'from_location'         => $last_locator_id,
                        'to_destination'        => $inventory->id,
                        'status'                => 'cancel-preparation',
                        'uom'                   => $uom,
                        'qty'                   => $qty_preparation,
                        'movement_date'         => $movement_date,
                        'note'                  => 'DIDAPATKAN DARI CANCEL PREPARATION, BARCODE INDUK '.$barcode_parent.$note_cancel,
                        'user_id'               => auth::user()->id
                    ]);

                    $concatenate                            .= "'".$new_material_stock->id."',";
                    $material_stock->save();
                }else
                {
                    $locator_source []  = $is_stock_exists->locator_id;
                    $stocks             = sprintf('%0.8f',$is_stock_exists->stock);
                    $reserved_qty       = sprintf('%0.8f',$is_stock_exists->reserved_qty);
                    $new_stocks         = sprintf('%0.8f',$stocks + $qty_preparation);
                    $new_available_qty  = sprintf('%0.8f',$stocks - $reserved_qty);

                    if($new_available_qty > 0)
                    {
                        $is_stock_exists->is_allocated  = false;
                        $is_stock_exists->deleted_at    = null;
                        $is_stock_exists->is_active     = true;
                    }

                    $is_stock_exists->stock         = $new_stocks;
                    $is_stock_exists->available_qty = $new_available_qty;

                    MovementStockHistory::FirstOrCreate([
                        'material_stock_id'     => $is_stock_exists->id,
                        'from_location'         => $is_stock_exists->locator_id,
                        'to_destination'        => $inventory->id,
                        'status'                => 'cancel-preparation',
                        'uom'                   => $uom,
                        'qty'                   => $qty_preparation,
                        'movement_date'         => $movement_date,
                        'note'                  => 'DIDAPATKAN DARI CANCEL PREPARATION, '.$note_cancel,
                        'user_id'               => auth::user()->id
                    ]);

                    $is_stock_exists->locator_id = $inventory->id;
                    $is_stock_exists->save();


                }

                if(!$is_additonal && $detail_material_preparation_fabric->detailPoBuyerPerRoll()->count() != 0)
                {
                    // buat hapus yang preparationnya
                    $material_preparations = MaterialPreparation::whereIn('id',function($query) use($detail_material_preparation_fabric_id)
                    {
                        $query->select('material_preparation_id')
                        ->from('detail_material_preparations')
                        ->whereNotNull('detail_material_preparation_fabric_id')
                        ->where('detail_material_preparation_fabric_id',$detail_material_preparation_fabric_id)
                        ->groupby('material_preparation_id');
                    })
                    ->get();

                    foreach ($material_preparations as $key => $material_preparation) 
                    {
                        if($material_preparation->last_status_movement == 'out-cutting' || $material_preparation->last_status_movement == 'out-distribution')
                        {
                            $last_material_movement_line_id = $material_preparation->last_material_movement_line_id;
                            
                            if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                                $c_orderline_id         = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->where('po_detail_id',$material_preparation->po_detail_id)
                                ->first();
                                
                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            }
                            
                            $_item_id_source = ($material_preparation->item_id_book != $material_preparation->item_id_source ? $material_preparation->item_id_source : null);

                            $is_movement_reverse_exists = MaterialMovement::where([
                                ['from_location',$material_preparation->last_locator_id],
                                ['to_destination',$inventory->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$material_preparation->po_buyer],
                                ['is_integrate',false],
                                ['is_complete_erp',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','reverse'],
                            ])
                            ->first();

                            if($is_movement_reverse_exists)
                            {
                                $is_movement_reverse_exists->updated_at     = $movement_date;
                                $is_movement_reverse_exists->save();
                                
                                $movement_reverse_id                        = $is_movement_reverse_exists->id;

                            }else
                            {
                                $movement_reverse = MaterialMovement::FirstOrCreate([
                                    'from_location'             => $material_preparation->last_locator_id,
                                    'to_destination'            => $inventory->id,
                                    'from_locator_erp_id'       => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'         => $inventory_erp->area->erp_id,
                                    'po_buyer'                  => $material_preparation->po_buyer,
                                    'is_complete_erp'           => false,
                                    'is_integrate'              => false ,
                                    'is_active'                 => true,
                                    'status'                    => 'reverse',
                                    'no_packing_list'           => $no_packing_list,
                                    'no_invoice'                => $no_invoice,
                                    'created_at'                => $movement_date,
                                    'updated_at'                => $movement_date,
                                ]);

                                $movement_reverse_id = $movement_reverse->id;
                            }

                            $is_line_reverse_exists = MaterialMovementLine::where([
                                ['material_movement_id',$movement_reverse_id],
                                ['material_preparation_id',$material_preparation->id],
                                ['item_id',$material_preparation->item_id],
                                ['warehouse_id',$material_preparation->warehouse],
                                ['qty_movement',$material_preparation->qty_conversion],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['date_movement',$movement_date],
                            ]) 
                            ->exists();

                            if(!$is_line_reverse_exists)
                            {
                                $new_material_movement_line_reverse = MaterialMovementLine::Create([
                                    'material_movement_id'                      => $movement_reverse_id,
                                    'material_preparation_id'                   => $material_preparation->id,
                                    'item_code'                                 => $material_preparation->item_code,
                                    'item_id'                                   => $material_preparation->item_id,
                                    'c_order_id'                                => $material_preparation->c_order_id,
                                    'c_orderline_id'                            => $c_orderline_id,
                                    'c_bpartner_id'                             => $material_preparation->c_bpartner_id,
                                    'supplier_name'                             => $material_preparation->supplier_name,
                                    'type_po'                                   => 1,
                                    'is_integrate'                              => false,
                                    'is_inserted_to_material_movement_per_size' => false,
                                    'uom_movement'                              => $material_preparation->uom_conversion,
                                    'qty_movement'                              => $material_preparation->qty_conversion,
                                    'date_movement'                             => $movement_date,
                                    'created_at'                                => $movement_date,
                                    'updated_at'                                => $movement_date,
                                    'date_receive_on_destination'               => $movement_date,
                                    'nomor_roll'                                => '-',
                                    'warehouse_id'                              => $material_preparation->warehouse,
                                    'document_no'                               => $material_preparation->document_no,
                                    'note'                                      => 'MOVEMENT DI KEMBALIKAN KE INVENTORY',
                                    'is_active'                                 => true,
                                    'user_id'                                   => auth::user()->id,
                                ]);

                                $material_preparation->last_material_movement_line_id    = $new_material_movement_line_reverse->id;
                            }

                            Temporary::Create([
                                'barcode'       => $material_preparation->po_buyer,
                                'status'        => 'mrp',
                                'user_id'       => Auth::user()->id,
                                'created_at'    => $movement_date,
                                'updated_at'    => $movement_date,
                            ]);

                            $material_preparation->last_user_movement_id    = Auth::user()->id;
                            $material_preparation->last_locator_id          = $inventory->id;
                            $material_preparation->last_status_movement     = 'reverse';
                            $material_preparation->last_movement_date       = $movement_date;
                            $material_preparation->cancel_planning          = $movement_date;
                            $material_preparation->save();

                            $obj_line                               = new stdClass();
                            $obj_line->material_movement_line_id    = $last_material_movement_line_id;
                            $obj_line->movement_date                = $movement_date;
                            $delete_material_movement_lines []      = $obj_line;

                        }else
                        {
                            Temporary::Create([
                                'barcode'       => $material_preparation->po_buyer,
                                'status'        => 'mrp',
                                'user_id'       => Auth::user()->id,
                                'created_at'    => $movement_date,
                                'updated_at'    => $movement_date,
                            ]);

                            $material_preparation->delete();                            
                        }
                    }
                }else
                {
                    if($movement_out_date)
                    {
                        if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$po_detail_id)
                            ->first();
                            
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        }

                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp->id],
                            ['to_destination',$inventory_erp->id],
                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
                        
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp->id,
                                'to_destination'        => $inventory_erp->id,
                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $movement_date,
                                'updated_at'            => $movement_date,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->updated_at = $movement_date;
                            $is_movement_exists->save();

                            $material_movement_id = $is_movement_exists->id;
                        }

                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['warehouse_id',$warehouse_id],
                            ['item_id',$item_id],
                            ['qty_movement',$qty_preparation],
                            ['date_movement',$movement_date],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => $qty_preparation,
                                'date_movement'                 => $movement_date,
                                'created_at'                    => $movement_date,
                                'updated_at'                    => $movement_date,
                                'date_receive_on_destination'   => $movement_date,
                                'nomor_roll'                    => $nomor_roll,
                                'warehouse_id'                  => $warehouse_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI UNTUK CANCEL PREPARATION ADDITIONAL FABRIC DILAKUKAN OLEH '.auth::user()->name,
                                'is_active'                     => true,
                                'user_id'                       => auth::user()->id,
                            ]);
                        }
                    }
                }


                $summary_stock_fabrics []               = $summary_stock_fabric_id;
                $material_preparation_fabrics []        = $material_preparation_fabric_id;


                
                $detail_material_preparation_fabric->delete();
                
            }

            $total_item_on_locator  = MaterialStock::where('locator_id',$inventory->id)
            ->whereNull('deleted_at')
            ->count();

            $counter                = Locator::find($inventory->id);
            $counter->counter_in    = $total_item_on_locator;
            $counter->save();

            foreach ($locator_source as $key => $value) 
            {
                $total_item_on_locator = MaterialStock::where('locator_id',$value)
                ->whereNull('deleted_at')
                ->count();

                $counter                = Locator::find($value);
                $counter->counter_in    = $total_item_on_locator;
                $counter->save();
            }

            if(count($delete_material_movement_lines) > 0)  ReverseOut::deleteMaterialMovementLines($delete_material_movement_lines);
           
            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_stock_fabric(array[".$concatenate ."]);" ));
            }

            //FabricMaterialCancelPreparationController::updateSummaryStockFabric($summary_stock_fabrics);
            FabricMaterialCancelPreparationController::updateMaterialPreparationFabrics($material_preparation_fabrics);
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    
    static function updateSummaryStockFabric($summary_stock_fabric_ids)
    {
        $summary_stock_fabrics = DB::table('summary_vs_stock_v')
        ->whereIn('id',$summary_stock_fabric_ids)
        ->get();
        
        try 
        {
          DB::beginTransaction();
         
          foreach ($summary_stock_fabrics as $key => $value) 
          {
            $summary_stock_fabric_id              = $value->id;
            $total_stock                          = sprintf('%0.8f',$value->detail_total_stock);
            $total_reserved_qty                   = sprintf('%0.8f',$value->detail_total_reserved_qty);
            $total_available_qty                  = sprintf('%0.8f',$value->detail_total_available_qty);
            
            $summary_stock_fabric                 = SummaryStockFabric::find($summary_stock_fabric_id);
            $summary_stock_fabric->stock          = $total_stock; 
            $summary_stock_fabric->reserved_qty   = $total_reserved_qty; 
            $summary_stock_fabric->available_qty  = $total_available_qty; 
            $summary_stock_fabric->save();
          }
          
          DB::commit();
        } catch (Exception $e) 
        {
          DB::rollBack();
          $message = $e->getMessage();
          ErrorHandler::db($message);
        }
    }

    static function updateMaterialPreparationFabrics($material_preparation_fabric_ids)
    {
        
        foreach ($material_preparation_fabric_ids as $key => $material_preparation_fabric_id) 
        {
            try 
            {
                DB::beginTransaction();
    
                $id                                                 = $material_preparation_fabric_id;
                $material_preparation_fabric                        = MaterialPreparationFabric::find($id);
                $total_reserved_qty                                 = sprintf('%0.8f',$material_preparation_fabric->total_reserved_qty);
                
                if($total_reserved_qty > 0)
                {
                    $total_qty_relax                                    = sprintf('%0.8f',DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)->sum('reserved_qty'));
                    $total_qty_outstanding                              = sprintf('%0.8f',$total_reserved_qty - $total_qty_relax);
                    
                }else
                {
                    $total_qty_relax                                    = sprintf('%0.8f',DetailMaterialPreparationFabric::where('material_preparation_fabric_id',$id)->sum('reserved_qty'));
                    $total_qty_outstanding                              = '0';
                }
                
                $material_preparation_fabric->total_qty_rilex       = $total_qty_relax;
                $material_preparation_fabric->total_qty_outstanding = $total_qty_outstanding;
                $material_preparation_fabric->remark_planning       = null;
                $material_preparation_fabric->save();
    
                DB::commit();
    
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }
}
