<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;

class FabricMaterialRejectController extends Controller
{
    public function index()
    {
        return view('fabric_material_reject.index');
    }

    public function create(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $barcode        = trim(strtoupper($request->barcode));

        $material_stock = MaterialStock::where([
            ['barcode_supplier',$barcode],
            ['warehouse_id',$warehouse_id],
            ['is_allocated',false],
        ])
        ->whereNull('deleted_at')
        ->first();

        if(!$material_stock) return response()->json('Barcode not found',422); 

        if ($material_stock->is_reject_by_lot && !$material_stock->movement_to_locator_reject_date_from_lot)
        {
            $obj                    = new stdClass();
            $obj->id                = $material_stock->id;
            $obj->item_id           = $material_stock->item_id;
            $obj->c_order_id        = $material_stock->c_order_id;
            $obj->barcode           = $material_stock->barcode_supplier;
            $obj->document_no       = $material_stock->document_no;
            $obj->item_code         = $material_stock->item_code;
            $obj->batch_number      = $material_stock->batch_number;
            $obj->nomor_roll        = $material_stock->nomor_roll;
            $obj->warehouse_id      = $material_stock->warehouse_id;
            $obj->uom               = $material_stock->uom;
            $obj->qty_reject        = sprintf('%0.8f',$material_stock->qty_reject_non_by_inspect);
            $obj->movement_date     = carbon::now()->toDateTimeString();
            $obj->status            = 'is_reject_by_lot';
        }else if($material_stock->is_reject_by_rma && !$material_stock->movement_to_locator_reject_date_from_rma)
        {
            $obj                    = new stdClass();
            $obj->id                = $material_stock->id;
            $obj->item_id           = $material_stock->item_id;
            $obj->c_order_id        = $material_stock->c_order_id;
            $obj->barcode           = $material_stock->barcode_supplier;
            $obj->document_no       = $material_stock->document_no;
            $obj->item_code         = $material_stock->item_code;
            $obj->batch_number      = $material_stock->batch_number;
            $obj->nomor_roll        = $material_stock->nomor_roll;
            $obj->warehouse_id      = $material_stock->warehouse_id;
            $obj->uom               = $material_stock->uom;
            $obj->qty_reject        = sprintf('%0.8f',$material_stock->qty_reject_non_by_inspect);
            $obj->movement_date     = carbon::now()->toDateTimeString();
            $obj->status            = 'is_reject_by_rma';
        }else if($material_stock->is_reject && !$material_stock->movement_to_locator_reject_date_from_inspect)
        {
            $obj                    = new stdClass();
            $obj->id                = $material_stock->id;
            $obj->item_id           = $material_stock->item_id;
            $obj->c_order_id        = $material_stock->c_order_id;
            $obj->barcode           = $material_stock->barcode_supplier;
            $obj->document_no       = $material_stock->document_no;
            $obj->item_code         = $material_stock->item_code;
            $obj->batch_number      = $material_stock->batch_number;
            $obj->nomor_roll        = $material_stock->nomor_roll;
            $obj->warehouse_id      = $material_stock->warehouse_id;
            $obj->uom               = $material_stock->uom;
            $obj->qty_reject        = sprintf('%0.8f',$material_stock->qty_reject_by_inspect);
            $obj->movement_date     = carbon::now()->toDateTimeString();
            $obj->status            = 'is_reject';
        }else if($material_stock->is_short_roll && !$material_stock->movement_to_locator_reject_date_from_short_roll)
        {
            $obj                    = new stdClass();
            $obj->id                = $material_stock->id;
            $obj->item_id           = $material_stock->item_id;
            $obj->c_order_id        = $material_stock->c_order_id;
            $obj->barcode           = $material_stock->barcode_supplier;
            $obj->document_no       = $material_stock->document_no;
            $obj->item_code         = $material_stock->item_code;
            $obj->batch_number      = $material_stock->batch_number;
            $obj->nomor_roll        = $material_stock->nomor_roll;
            $obj->warehouse_id      = $material_stock->warehouse_id;
            $obj->uom               = $material_stock->uom;
            $obj->qty_reject        = sprintf('%0.8f',$material_stock->qty_reject_by_short_roll);
            $obj->movement_date     = carbon::now()->toDateTimeString();
            $obj->status            = 'is_short_roll';
        }else
        {
            return response()->json('This roll is not reject.',422); 
        }
    
        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'barcode_products'      => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            try 
            {
                DB::beginTransaction();

                $items          = json_decode($request->barcode_products);
                $update_summary = array();
                $warehouse_id   = $request->warehouse_id;

                $reject_locator = Locator::whereHas('area',function($query) use ($warehouse_id)
                {
                    $query->where([
                        ['name','REJECT'],
                        ['is_destination',true],
                        ['warehouse',$warehouse_id],
                    ]);
                })
                ->first();

                
               
                foreach ($items as $key => $value) 
                {
                    $id                     = $value->id;
                    $movement_date          = $value->movement_date;
                    $status                 = $value->status;
                    $qty_reject             = sprintf('%0.8f',$value->qty_reject);

                    $material_stock         = MaterialStock::find($id);
                    $summary_stock_fabric_id= $material_stock->summary_stock_fabric_id;
                    $locator_id             = $material_stock->locator_id;
                    $uom                    = $material_stock->uom;
                    $is_short_roll          = $material_stock->is_short_roll;
                    $available_qty          = sprintf('%0.8f',$material_stock->available_qty);
                    $reserved_qty           = sprintf('%0.8f',$material_stock->reserved_qty);
                    $new_available_qty      = sprintf('%0.8f',$available_qty - $qty_reject);
                    $new_reserved_qty       = sprintf('%0.8f',$reserved_qty + $qty_reject);
                    
                    if($new_available_qty <= 0)
                    {
                        $new_locator_id    = $reject_locator->id;
                        $is_allocated      = true;
                        $is_active         = false;
                        $deleted_at        = $movement_date;
                        $new_available_qty = 0;
                    }else
                    {
                        $new_locator_id = $locator_id;
                        $is_allocated   = false;
                        $is_active      = true;
                        $deleted_at     = null;
                    }

                    if($status == 'is_reject')
                    {
                        $msg                                                                = 'REJECT BY QC ';
                        $material_stock->movement_to_locator_reject_date_from_inspect       = $movement_date;
                    }else if($status == 'is_reject_by_rma')
                    {
                        $msg                                                                = 'REJECT BY RETURN MATERIAL ';
                        $material_stock->movement_to_locator_reject_date_from_rma           = $movement_date;
                    }else if($status == 'is_reject_by_lot')
                    {
                        $msg                                                                = 'REJECT BY LOT NOT MATCH ';
                        $material_stock->movement_to_locator_reject_date_from_lot           = $movement_date;
                    }else if($status == 'is_short_roll')
                    {
                        $msg                                                                = 'REJECT BY SHORT ROLL ';
                        $material_stock->movement_to_locator_reject_date_from_short_roll    = $movement_date;
                    }

                    $material_stock->locator_id                             = $new_locator_id;
                    $material_stock->is_allocated                           = $is_allocated;
                    $material_stock->is_active                              = $is_active;
                    $material_stock->deleted_at                             = $deleted_at;
                    $material_stock->available_qty                          = $new_available_qty;
                    $material_stock->reserved_qty                           = $new_reserved_qty;
                    $material_stock->save();

                    MovementStockHistory::create([
                        'material_stock_id'     => $material_stock->id,
                        'from_location'         => $locator_id,
                        'to_destination'        => $reject_locator->id,
                        'status'                => 'reject',
                        'uom'                   => $uom,
                        'qty'                   => $new_available_qty,
                        'note'                  => $msg.' [ AWAL QTYNYA ADALAH '.$available_qty.' LALU DI REJECT '.$qty_reject.' ]',
                        'movement_date'         => $movement_date,
                        'user_id'               => auth::user()->id
                    ]);;

                    $total_item_on_locator  = MaterialStock::where('locator_id',$locator_id)->whereNull('deleted_at')->count();
                    
                    $counter                = Locator::find($locator_id);
                    if($counter)
                    {
                        $counter->counter_in    = $total_item_on_locator;
                        $counter->save();
                    }

                    $update_summary [] = $summary_stock_fabric_id;
                    
                }

                $this->updateSummaryStockFabric($update_summary);
                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
           
            return response()->json(200);
        }else
        {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }

    }

    static function updateSummaryStockFabric($update_summary)
    {
        $summary_stock_fabrics  = SummaryStockFabric::whereIn('id',$update_summary)->get();
        

        foreach ($summary_stock_fabrics as $key => $summary_stock_fabric) 
        {
            $detail                 = DB::table('summary_vs_stock_v')->where('id',$summary_stock_fabric->id) ->first();
            
            if($detail)
            {
                $total_stock         = sprintf('%0.8f',$detail->detail_total_stock);
                $total_reserved_qty  = sprintf('%0.8f',$detail->detail_total_reserved_qty);
                $total_available_qty = sprintf('%0.8f',$detail->detail_total_available_qty);
                $total_arrival_qty   = sprintf('%0.8f',$detail->detail_total_arrival_qty);

                $summary_stock_fabric->stock         = $total_stock;
                $summary_stock_fabric->reserved_qty  = $total_reserved_qty;
                $summary_stock_fabric->available_qty = $total_available_qty;
                $summary_stock_fabric->qty_order     = $total_arrival_qty;
                $summary_stock_fabric->save();
            }
        }
    }
}
