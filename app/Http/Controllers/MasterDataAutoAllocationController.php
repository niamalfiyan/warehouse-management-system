<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\PoSupplier;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\HistoryAutoAllocations;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\MaterialStockPerLot;

use App\Http\Controllers\AllocationFabricController as AllocationFabric;
use App\Http\Controllers\AccessoriesMaterialOutController as MaterialOut;
use App\Http\Controllers\FabricMaterialPlanningController as PlanningFabric;
use App\Http\Controllers\MasterDataReroutePoBuyerController as MasterDataReroute;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Http\Controllers\FabricReportMaterialMonitoringController as Dashboard;

class MasterDataAutoAllocationController extends Controller
{
    public function index()
    {
        $flag           = Session::get('flag');
        $active_tab     = 'active';
        $warehouse_id   = auth::user()->warehouse;

        return view('master_data_auto_allocation.index',compact('flag','active_tab'));
    }

    public function activeAllocation(Request $request)
    {
        if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))
        {
            $warehouse_id = ['1000011','1000001'];
            $auto_allocations = db::table('auto_allocations_v')
            ->where('is_additional',false)
            ->whereIn('warehouse_id',$warehouse_id)
            ->orderby('status_po_buyer','asc')
            ->orderby('qty_outstanding','desc')
            ->take(100);
        }else if(auth::user()->hasRole(['mm-staff-acc','admin-ict-acc', 'finance-account-acc']))
        {
            $warehouse_id = ['1000013','1000002'];
            if(auth::user()->hasRole(['admin-ict-acc', 'mm-staff-acc', 'finance-account-acc']))
            {
                $auto_allocations = db::table('auto_allocations_v')
                ->where('is_additional',false)
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderby('status_po_buyer','asc')
                ->orderby('qty_outstanding','desc')
                ->take(100);
            }else
            {
                $auto_allocations = db::table('auto_allocations_v')
                ->where('is_allocation_purchase',false)
                ->where('is_additional',false)
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderby('status_po_buyer','asc')
                ->orderby('qty_outstanding','desc')
                ->take(100);;
            }
            
            
        }
        else
        {
            $warehouse_id = [auth::user()->warehouse];
            if($warehouse_id == '1000011' or $warehouse_id == '1000001')
            {
                $auto_allocations = db::table('auto_allocations_v')
                ->where([
                    ['is_additional',false],
                ])
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderby('status_po_buyer','asc')
                ->orderby('qty_outstanding','desc')
                ->take(100);

            }
            {
                $auto_allocations = db::table('auto_allocations_v')
                ->where([
                    ['is_additional',false],
                    ['is_allocation_purchase',false],
                ])
                ->whereIn('warehouse_id',$warehouse_id)
                ->orderby('status_po_buyer','asc')
                ->orderby('qty_outstanding','desc')
                ->take(100);
            }
            
        }
        
        return DataTables::of($auto_allocations)
        ->editColumn('status_po_buyer',function($auto_allocations)
        {
            if($auto_allocations->status_po_buyer == 'active') return '<span class="label label-info">ACTIVE</span>';
            else if($auto_allocations->status_po_buyer == 'cancel') return '<span class="label label-danger">CANCEL</span>';
            else return $auto_allocations->status_po_buyer;
        })
        ->editColumn('lc_date',function($auto_allocations)
        {
            return ($auto_allocations->lc_date ? Carbon::createFromFormat('Y-m-d', $auto_allocations->lc_date)->format('d/M/Y') : null);
        })
        ->addColumn('date',function($auto_allocations)
        {
            return '<br><b>Created Date</b><br/>'.($auto_allocations->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->created_at)->format('d/M/Y H:i:s') : null ).
            '<br><b>Promise Date</b><br/>'.($auto_allocations->promise_date ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->promise_date)->format('d/M/Y') : null);
        })
        ->addColumn('additional',function($auto_allocations)
        {
            return '<br><b>Additional</b><br/>'.($auto_allocations->is_additional ? 'True' : 'False').
            '<br><b>Remark Additional</b><br/>'.($auto_allocations->is_additional ? $auto_allocations->remark_additional : null);
        })
        ->addColumn('source_supplier',function($auto_allocations)
        {
            return '<b>Po Suppliers</b><br/>'.$auto_allocations->document_no.
            '<br><b>Supplier Name</b><br/>'.$auto_allocations->supplier_name;
        })
        ->addColumn('allocating_to',function($auto_allocations)
        {
            return '<b>Po Buyer</b><br/>'.$auto_allocations->po_buyer.
            '<br/><b>Old Po Buyer</b><br/>'.$auto_allocations->old_po_buyer.
            '<br><b>Type Stock Buyer</b><br/>'.$auto_allocations->type_stock;
        })
        ->editColumn('item',function($auto_allocations)
        {
            return '<b>Item Source</b><br/>'.$auto_allocations->item_code_source.' ('.$auto_allocations->category.')'.
            '<br><b>Item Book</b><br/>'.$auto_allocations->item_code.' ('.$auto_allocations->category.')';
        })
        ->addColumn('quantity',function($auto_allocations)
        {
            if($auto_allocations->qty_outstanding < 0) $remark = ' <br/>  <br/> ALOKASI KELEBIHAN '.-1*$auto_allocations->qty_outstanding;
            else $remark = null;

            return '<b>Allocation</b><br/>'.number_format($auto_allocations->qty_allocation, 4, '.', ',').' ('.$auto_allocations->uom.')'.
            '<br><b>Outstanding</b><br/>'.number_format($auto_allocations->qty_outstanding, 4, '.', ',').' ('.$auto_allocations->uom.')'.$remark.
            '<br><b>Allocated</b><br/>'.number_format($auto_allocations->qty_allocated, 4, '.', ',').' ('.$auto_allocations->uom.')';
        })
        ->addColumn('action',function($auto_allocations)
        {
            if(auth::user()->hasRole(['admin-ict-fabric','admin-ict-febric','admin-ict-acc','mm-staff'])) {
                return view('master_data_auto_allocation._action',[
                    'model'                 => $auto_allocations,
                    'edit'                  => route('masterDataAutoAllocation.edit',['id' => $auto_allocations->id]),
                    'delete'                => route('masterDataAutoAllocation.delete',['id' => $auto_allocations->id]),
                    'cancelAutoAllocation'  => route('masterDataAutoAllocation.cancelAllocation',['id' => $auto_allocations->id]),
                    'allocatingStock'       => route('masterDataAutoAllocation.allocatingPoBuyer',['id' => $auto_allocations->id]),
                    'recalculate'           => route('masterDataAutoAllocation.recalculate',$auto_allocations->id),
                    'historyModal'          => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
                ]);
            }else if(auth::user()->hasRole(['mm-staff-acc']))
            {
                if($auto_allocations->category == 'CT')
                {
                    return view('master_data_auto_allocation._action',[
                        'model'                 => $auto_allocations,
                        'edit'                  => route('masterDataAutoAllocation.edit',['id' => $auto_allocations->id]),
                        'delete'                => route('masterDataAutoAllocation.delete',['id' => $auto_allocations->id]),
                        'cancelAutoAllocation'  => route('masterDataAutoAllocation.cancelAllocation',['id' => $auto_allocations->id]),
                        'allocatingStock'       => route('masterDataAutoAllocation.allocatingPoBuyer',['id' => $auto_allocations->id]),
                        'recalculate'           => route('masterDataAutoAllocation.recalculate',$auto_allocations->id),
                        'historyModal'          => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
                    ]);
                }
                else
                {
                    return view('master_data_auto_allocation._action',[
                        'model'                 => $auto_allocations,
                        'edit'                  => route('masterDataAutoAllocation.edit',['id' => $auto_allocations->id]),
                        'delete'                => route('masterDataAutoAllocation.delete',['id' => $auto_allocations->id]),
                        'allocatingStock'       => route('masterDataAutoAllocation.allocatingPoBuyer',['id' => $auto_allocations->id]),
                        'recalculate'           => route('masterDataAutoAllocation.recalculate',$auto_allocations->id),
                        'historyModal'          => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
                    ]);

                }
            }else
            {
                return view('_action');
            }
           
        })
        ->setRowAttr([
            'style' => function($auto_allocations) {
                if($auto_allocations->qty_outstanding == 0) return  'background-color: #d9ffde;';
                if($auto_allocations->qty_outstanding < 0) return  'background-color: #fff99e;';
            },
        ])
        ->rawColumns(['user','additional','date','status_po_buyer','allocating_to','item','source_supplier','quantity','action'])
        ->make(true);
    }

    public function additionalAllocation(Request $request)
    {
        if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))$warehouse_id = ['1000011','1000001'];
        else if(auth::user()->hasRole(['mm-staff-acc','admin-ict-acc']))$warehouse_id = ['1000013','1000002'];
        else $warehouse_id = [auth::user()->warehouse];
        
        $auto_allocations = db::table('auto_allocations_v')
        ->whereIn('warehouse_id',$warehouse_id)
        ->where('is_additional',true)
        ->orderby('status_po_buyer','asc')
        ->orderby('qty_outstanding','desc');

        return DataTables::of($auto_allocations)
        ->editColumn('status_po_buyer',function($auto_allocations)
        {
            if($auto_allocations->status_po_buyer == 'active') return '<span class="label label-info">ACTIVE</span>';
            else if($auto_allocations->status_po_buyer == 'cancel') return '<span class="label label-danger">CANCEL</span>';
            else return $auto_allocations->status_po_buyer;
        })
        ->editColumn('lc_date',function($auto_allocations)
        {
            return ($auto_allocations->lc_date ? Carbon::createFromFormat('Y-m-d', $auto_allocations->lc_date)->format('d/M/Y') : null);
        })
        ->addColumn('date',function($auto_allocations)
        {
            return '<br><b>Created Date</b><br/>'.($auto_allocations->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->created_at)->format('d/M/Y H:i:s') : null ).
            '<br><b>Promise Date</b><br/>'.($auto_allocations->promise_date ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->promise_date)->format('d/M/Y') : null);
        })
        ->addColumn('additional',function($auto_allocations)
        {
            return '<br><b>Additional</b><br/>'.($auto_allocations->is_additional ? 'True' : 'False').
            '<br><b>Remark Additional</b><br/>'.($auto_allocations->is_additional ? $auto_allocations->remark_additional : null);
        })
        ->addColumn('source_supplier',function($auto_allocations)
        {
            return '<b>Po Suppliers</b><br/>'.$auto_allocations->document_no.
            '<br><b>Supplier Name</b><br/>'.$auto_allocations->supplier_name;
        })
        ->addColumn('allocating_to',function($auto_allocations)
        {
            return '<b>Po Buyer</b><br/>'.$auto_allocations->po_buyer.
            '<br/><b>Old Po Buyer</b><br/>'.$auto_allocations->old_po_buyer.
            '<br><b>Type Stock Buyer</b><br/>'.$auto_allocations->type_stock;
        })
        ->editColumn('item',function($auto_allocations)
        {
            return '<b>Item Source</b><br/>'.$auto_allocations->item_code_source.' ('.$auto_allocations->category.')'.
            '<br><b>Item Book</b><br/>'.$auto_allocations->item_code.' ('.$auto_allocations->category.')';
        })
        ->addColumn('quantity',function($auto_allocations)
        {
            return '<b>Allocation</b><br/>'.number_format($auto_allocations->qty_allocation, 4, '.', ',').' ('.$auto_allocations->uom.')'.
            '<br><b>Outstanding</b><br/>'.number_format($auto_allocations->qty_outstanding, 4, '.', ',').' ('.$auto_allocations->uom.')'.
            '<br><b>Allocated</b><br/>'.number_format($auto_allocations->qty_allocated, 4, '.', ',').' ('.$auto_allocations->uom.')';
        })
        ->addColumn('action',function($auto_allocations)
        {
            if(auth::user()->hasRole(['admin-ict-fabric','admin-ict-febric','admin-ict-acc','mm-staff'])) {
                return view('master_data_auto_allocation._action',[
                    'model'                 => $auto_allocations,
                    'edit'                  => route('masterDataAutoAllocation.edit',['id' => $auto_allocations->id]),
                    'delete'                => route('masterDataAutoAllocation.delete',['id' => $auto_allocations->id]),
                    'cancelAutoAllocation'  => route('masterDataAutoAllocation.cancelAllocation',['id' => $auto_allocations->id]),
                    'allocatingStock'       => route('masterDataAutoAllocation.allocatingPoBuyer',['id' => $auto_allocations->id]),
                    'recalculate'           => route('masterDataAutoAllocation.recalculate',$auto_allocations->id),
                    'historyModal'          => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
                ]);
            }else if(auth::user()->hasRole(['mm-staff-acc']))
            {
                return view('master_data_auto_allocation._action',[
                    'model'                 => $auto_allocations,
                    'edit'                  => route('masterDataAutoAllocation.edit',['id' => $auto_allocations->id]),
                    'delete'                => route('masterDataAutoAllocation.delete',['id' => $auto_allocations->id]),
                    'allocatingStock'       => route('masterDataAutoAllocation.allocatingPoBuyer',['id' => $auto_allocations->id]),
                    'recalculate'           => route('masterDataAutoAllocation.recalculate',$auto_allocations->id),
                    'historyModal'          => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
                ]);
            }else
            {
                return view('_action');
            }
           
        })
        ->setRowAttr([
            'style' => function($auto_allocations) {
                if($auto_allocations->qty_outstanding == 0) return  'background-color: #d9ffde;';
            },
        ])
        ->rawColumns(['user','additional','date','status_po_buyer','allocating_to','item','source_supplier','quantity','action'])
        ->make(true);
    }

    public function inactiveAllocation(Request $request)
    {
        if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))$warehouse_id = ['1000011','1000001'];
        else if(auth::user()->hasRole(['mm-staff-acc','admin-ict-acc']))$warehouse_id = ['1000013','1000002'];
        else $warehouse_id = [auth::user()->warehouse];
        
        $auto_allocations = db::table('auto_allocations_inactive_v')
        ->whereIn('warehouse_id',$warehouse_id)
        ->orderby('status_po_buyer','asc')
        ->orderby('qty_outstanding','desc');
        
        return DataTables::of($auto_allocations)
        ->editColumn('status_po_buyer',function($auto_allocations)
        {
            if($auto_allocations->status_po_buyer == 'active') return '<span class="label label-info">ACTIVE</span>';
            else if($auto_allocations->status_po_buyer == 'cancel') return '<span class="label label-danger">CANCEL</span>';
            else return $auto_allocations->status_po_buyer;
        })
        ->editColumn('lc_date',function($auto_allocations)
        {
            return ($auto_allocations->lc_date ? Carbon::createFromFormat('Y-m-d', $auto_allocations->lc_date)->format('d/M/Y') : null);
        })
        ->addColumn('date',function($auto_allocations)
        {
            return '<br><b>Created Date</b><br/>'.($auto_allocations->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->created_at)->format('d/M/Y H:i:s') : null ).
            '<br><b>Promise Date</b><br/>'.($auto_allocations->promise_date ? Carbon::createFromFormat('Y-m-d H:i:s', $auto_allocations->promise_date)->format('d/M/Y') : null);
        })
        ->addColumn('additional',function($auto_allocations)
        {
            return '<br><b>Additional</b><br/>'.($auto_allocations->is_additional ? 'True' : 'False').
            '<br><b>Remark Additional</b><br/>'.($auto_allocations->is_additional ? $auto_allocations->remark_additional : null);
        })
        ->addColumn('source_supplier',function($auto_allocations)
        {
            return '<b>Po Suppliers</b><br/>'.$auto_allocations->document_no.
            '<br><b>Supplier Name</b><br/>'.$auto_allocations->supplier_name;
        })
        ->addColumn('allocating_to',function($auto_allocations)
        {
            return '<b>Po Buyer</b><br/>'.$auto_allocations->po_buyer.
            '<br/><b>Old Po Buyer</b><br/>'.$auto_allocations->old_po_buyer.
            '<br><b>Type Stock Buyer</b><br/>'.$auto_allocations->type_stock;
        })
        ->editColumn('item',function($auto_allocations)
        {
            return '<b>Item Source</b><br/>'.$auto_allocations->item_code_source.' ('.$auto_allocations->category.')'.
            '<br><b>Item Book</b><br/>'.$auto_allocations->item_code.' ('.$auto_allocations->category.')';
        })
        ->addColumn('quantity',function($auto_allocations)
        {
            return '<b>Allocation</b><br/>'.number_format($auto_allocations->qty_allocation, 4, '.', ',').' ('.$auto_allocations->uom.')'.
            '<br><b>Outstanding</b><br/>'.number_format($auto_allocations->qty_outstanding, 4, '.', ',').' ('.$auto_allocations->uom.')'.
            '<br><b>Allocated</b><br/>'.number_format($auto_allocations->qty_allocated, 4, '.', ',').' ('.$auto_allocations->uom.')';
        })
        ->addColumn('action',function($auto_allocations){
            return view('master_data_auto_allocation._action',[
                    'model'             => $auto_allocations,
                    'historyModal'      => route('masterDataAutoAllocation.history',['id' => $auto_allocations->id]),
            ]);
        })
        ->rawColumns(['user','date','status_po_buyer','additional','allocating_to','item','source_supplier','quantity','action'])
        ->make(true);
    }

    public function history($id)
    {
        //return view('errors.503');
        $history_auto_allocations = HistoryAutoAllocations::where('auto_allocation_id',$id)
        ->orderby('created_at','asc')
        ->get();

        return view('master_data_auto_allocation._history_table',compact('history_auto_allocations'));
    }

    public function create()
    {
        //return view('errors.503');
        return view('master_data_auto_allocation.create');
    }

    public function deleteFromBulk()
    {
        return view('master_data_auto_allocation.delete');
    }

    public function destroy(Request $request)
    {
        $id                     = $request->id;
        $auto_allocation        = AutoAllocation::find($id);
        
        $document_no            = strtoupper($auto_allocation->document_no);
        $c_bpartner_id          = $auto_allocation->c_bpartner_id;
        $warehouse_id           = $auto_allocation->warehouse_id;
        $type_stock_erp_code    = $auto_allocation->type_stock_erp_code;
        $type_stock             = $auto_allocation->type_stock;
        $uom                    = $auto_allocation->uom;
        $lc_date                = $auto_allocation->lc_date;
        $item_code_source       = $auto_allocation->item_code_source;
        $item_code_book         = $auto_allocation->item_code_book;
        $po_buyer               = $auto_allocation->po_buyer;
        $qty_allocated          = $auto_allocation->qty_allocated;
        $is_integrate           = ($auto_allocation->is_upload_manual) ? true: false; 

        if($qty_allocated > 0)  return response()->json('This allocation cannot be deleted cause already allocated, please contact ICT to further action',422);

        $_supplier              = PoSupplier::where('document_no',$document_no)->first();
        $c_order_id             = ($_supplier)?$_supplier->c_order_id : 'FREE STOCK';

        $item_source            = Item::where(db::raw('item_code'),$item_code_source)->first();
        $item_book              = Item::where(db::raw('item_code'),$item_code_book)->first();
        
        $item_source_id         = ($item_source) ? $item_source->item_id : null;
        $item_book_id           = ($item_book) ? $item_book->item_id : null;

        $supplier_name          = ($c_bpartner_id == 'FREE STOCK ' && $document_no == 'FREE STOCK')? 'FREE STOCK': $auto_allocation->supplier_name;
        $month_lc_date          = ($auto_allocation->lc_date)? $auto_allocation->lc_date->format('m') : '05';
        $qty_allocation         = ($month_lc_date <= 02)? sprintf("%0.2f",$auto_allocation->qty_outstanding) :  sprintf('%0.8f',$auto_allocation->qty_outstanding);
        $operator               = -1*$qty_allocation;
        $note                   = 'TERJADI PENGHAPUSAN ALOKASI';
        $note_code              = '2';

        HistoryAutoAllocations::Create([
            'auto_allocation_id'        => $id,
            'document_no_old'           => $document_no,
            'c_bpartner_id_old'         => $c_bpartner_id,
            'item_code_old'             => $item_code_book,
            'uom_old'                   => $uom,
            'item_id_old'               => $item_book_id,
            'item_source_id_old'        => $item_source_id,
            'item_code_source_old'      => $item_code_source,
            'po_buyer_old'              => $po_buyer,
            'supplier_name_old'         => $supplier_name,
            'qty_allocated_old'         => $qty_allocation,
            'warehouse_id_old'          => $warehouse_id,
            'type_stock_erp_code_old'   => $type_stock_erp_code,
            'type_stock_old'            => $type_stock,

            'document_no_new'           => $document_no,
            'item_code_new'             => $item_code_book,
            'item_id_new'               => $item_book_id,
            'item_source_id_new'        => $item_source_id,
            'item_code_source_new'      => $item_code_source,
            'uom_new'                   => $uom,
            'c_bpartner_id_new'         => $c_bpartner_id,
            'supplier_name_new'         => $supplier_name,
            'qty_allocated_new'         => $qty_allocation,
            'warehouse_id_new'          => $warehouse_id,
            'po_buyer_new'              => $po_buyer,
            'type_stock_erp_code_new'   => $type_stock_erp_code,
            'type_stock_new'            => $type_stock,

            'note_code'                 => $note_code,
            'note'                      => $note,
            'operator'                  => $operator,
            'remark'                    => strtoupper($request->cancel_reason),
            'is_integrate'              => $is_integrate,
            'lc_date'                   => $lc_date,
            'c_order_id'                => $c_order_id,
            'user_id'                   => auth::user()->id
        ]);

        $auto_allocation->update_user_id = auth::user()->id;
        $auto_allocation->remark = strtoupper($request->cancel_reason);
        $auto_allocation->deleted_at = carbon::now();
        $auto_allocation->save();

        return response()->json(200);
    }

    public function edit(Request $request)
    {
       $auto_allocation = AutoAllocation::where('id',$request->id)
        ->whereNull('deleted_at')
        ->first();

        return response()->json($auto_allocation, 200);
    }

    public function recalculate($id)
    {
        
        $this->doRecalculate($id);
        return response()->json('success',200);
    }

    public function update(Request $request)
    {
        $id                     = $request->id;
        $booking_number_new     = strtoupper($request->booking_number);
        $document_no_new        = strtoupper($request->document_no);
        $po_suppliers           = PoSupplier::where('document_no',$document_no_new)->first();;
        $c_order_id_new         = $po_suppliers->c_order_id;
        $c_bpartner_id_new      = $request->c_bpartner_id;
        $type_stock_erp_new     = $request->type_stock_erp;
        $type_stock_new         = $request->type_stock;
        $warehouse_id_new       = $request->warehouse_id;
        $cancel_reason          = strtoupper($request->cancel_reason);
        $item_code_source_new   = strtoupper($request->item_code_source);
        $_item_code_book_new    = strtoupper($request->item_code);
        $qty_allocation_new     = sprintf('%0.8f',$request->qty_allocation);
        $qty_outstanding_new    = sprintf('%0.8f',$request->qty_outstanding);
        $update_allocations_acc = array();
        $update_allocations_fab = array();

        if($warehouse_id_new == '1000001' || $warehouse_id_new == '1000011') $is_fabric_new = true;
        else $is_fabric_new = false;

        if($warehouse_id_new == '1000013') $warehouse_name_new = 'ACCESSORIES AOI 2';
        elseif($warehouse_id_new == '1000002') $warehouse_name_new = 'ACCESSORIES AOI 1';
        elseif($warehouse_id_new == '1000001') $warehouse_name_new = 'FABRIC AOI 1';
        elseif($warehouse_id_new == '1000011') $warehouse_name_new = 'FABRIC AOI 2';

        $pos = strpos($_item_code_book_new, '|');
        if($pos === false)
        {
            $item_code_book_new = $_item_code_book_new;
            $article_no_new     = null;
        }else
        {
            $split = explode('|',$_item_code_book_new);
            $item_code_book_new = $split[0];
            $article_no_new = $split[1];
        }

        $supplier                       = Supplier::where('c_bpartner_id',$c_bpartner_id_new)->first();
        $supplier_name_new              = ($supplier)? $supplier->supplier_name : 'FREE STOCK';
        $c_bpartner_id_new              = ($supplier)? $supplier->c_bpartner_id : 'FREE STOCK';

        $auto_allocation                = AutoAllocation::find($id);
        $month_lc_date                  = $auto_allocation->lc_date->format('m');
        $document_no_old                = strtoupper($auto_allocation->document_no);
        $supplier_name_old              = ($document_no_old == 'FREE STOCK') ? 'FREE STOCK' : $auto_allocation->supplier_name;
        $user_id_old                    = $auto_allocation->user_id;
        $document_allocation_number_old = $auto_allocation->document_allocation_number;
        $season_old                     = $auto_allocation->season;
        $po_buyer_old                   = $auto_allocation->po_buyer;
        $item_desc_old                  = $auto_allocation->item_desc;
        $lc_date_old                    = $auto_allocation->lc_date;
        $uom_old                        = $auto_allocation->uom;
        $po_buyer_old_old               = $auto_allocation->old_po_buyer;
        $status_po_buyer_old            = $auto_allocation->status_po_buyer;
        $promise_date_old               = $auto_allocation->promise_date;
        $is_fabric_old                  = $auto_allocation->is_fabric;
        $category_old                   = $auto_allocation->category;
        $is_allocation_purchase_old     = $auto_allocation->is_allocation_purchase;
        $warehouse_name_old             = $auto_allocation->warehouse_name;
        $c_bpartner_id_old              = $auto_allocation->c_bpartner_id;
        $warehouse_id_old               = $auto_allocation->warehouse_id;
        $item_code_source_old           = $auto_allocation->item_code_source;
        $item_code_book_old             = $auto_allocation->item_code_book;
        $type_stock_erp_code_old        = $auto_allocation->type_stock_erp_code;
        $type_stock_old                 = $auto_allocation->type_stock;
        $c_order_id_old                 = $auto_allocation->c_order_id;
        $item_id_book_old               = $auto_allocation->item_id_book;
        $item_id_source_old             = $auto_allocation->item_id_source;
        $qty_allocation_old             = sprintf('%0.8f',$auto_allocation->qty_allocation);
        $qty_outstanding_old            = sprintf('%0.8f',$auto_allocation->qty_outstanding);
        $qty_allocated_old              = sprintf('%0.8f',$auto_allocation->qty_allocated);
        $_item_code_book_old            = $auto_allocation->item_code;
        $is_additional                  = $auto_allocation->is_additional;
        $remark_additional              = $auto_allocation->remark_additional;
        $so_id                          = PoBuyer::select('so_id')->where('po_buyer', $po_buyer_old)->first();


        $pos = strpos($_item_code_book_old, '|');
        if ($pos === false)
        {
            $item_code_book_old = $_item_code_book_old;
            $article_no_old     = null;
        }else 
        {
            $split = explode('|',$_item_code_book_old);
            $item_code_book_old = $split[0];
            $article_no_old     = $split[1];
        }

        //ITEM SOURCE NEW
        $item_source_new        = Item::where(db::raw('item_code'),$item_code_source_new)->first();
        $item_source_new_id     = ($item_source_new)? $item_source_new->item_id : null;
        $item_source_new_desc   = ($item_source_new)? $item_source_new->item_desc : null;
        $category_new           = ($item_source_new)? $item_source_new->category : null;
        $uom_new                = ($item_source_new)? $item_source_new->uom : null;

        //ITEM BOOK NEW
        $item_book_new          = Item::where(db::raw('item_code'),$item_code_book_new)->first();
        $item_book_new_id       = ($item_book_new)? $item_book_new->item_id : null;
        
        //ITEM SOURCE OLD
        $item_source_old        = Item::where(db::raw('item_code'),$item_code_source_old)->first();
        $item_source_old_id     = ($item_source_old)? $item_source_old->item_id : null;

        //ITEM BOOK OLD
        $item_old               = Item::where(db::raw('item_code'),$item_code_book_old)->first();
        $item_old_id            = ($item_old)  ? $item_old->item_id : null;

        $is_system              = User::where([
            ['id',$user_id_old],
            ['name','system'],
        ])
        ->exists();

        $movement_date = Carbon::now();

        try 
        {
            DB::beginTransaction();

            if($item_code_source_old != $item_code_source_new 
            || $warehouse_id_new != $warehouse_id_old 
            || $c_bpartner_id_new != $c_bpartner_id_old 
            || $document_no_old != $document_no_new 
            || $qty_allocation_new != $qty_outstanding_old)
            {
                if($qty_allocated_old > 0)
                {
                    // insert new allocation
                    $new_allocation_based_on_qty_allocated = AutoAllocation::firstorcreate([
                        'document_allocation_number'        => $document_allocation_number_old,
                        'lc_date'                           => $lc_date_old,
                        'season'                            => $season_old,
                        'document_no'                       => $document_no_old,
                        'c_bpartner_id'                     => $c_bpartner_id_old,
                        'supplier_name'                     => $supplier_name_old,
                        'po_buyer'                          => $po_buyer_old,
                        'old_po_buyer'                      => $po_buyer_old_old,
                        'item_code_source'                  => $item_code_source_old,
                        'item_code'                         => $_item_code_book_old,
                        'item_code_book'                    => $item_code_book_old,
                        'article_no'                        => $article_no_old,
                        'item_desc'                         => $item_desc_old,
                        'is_allocation_purchase'            => $is_allocation_purchase_old,
                        'category'                          => $category_old,
                        'uom'                               => $uom_old,
                        'warehouse_name'                    => $warehouse_name_old,
                        'warehouse_id'                      => $warehouse_id_old,
                        'qty_allocation'                    => $qty_allocated_old,
                        'qty_outstanding'                   => 0,
                        'qty_allocated'                     => $qty_allocated_old,
                        'is_fabric'                         => $is_fabric_old,
                        'c_order_id'                        => $c_order_id_old,
                        'item_id_book'                      => $item_id_book_old,
                        'item_id_source'                    => $item_id_source_old,
                        'type_stock_erp_code'               => $type_stock_erp_code_old,
                        'type_stock'                        => $type_stock_old,
                        'is_already_generate_form_booking'  => true,
                        'is_upload_manual'                  => true,
                        'promise_date'                      => $promise_date_old,
                        'status_po_buyer'                   => $status_po_buyer_old,
                        'is_additional'                     => $is_additional,
                        'remark_additional'                 => $remark_additional,
                        'user_id'                           => auth::user()->id,
                        'created_at'                        => carbon::now(),
                        'updated_at'                        => carbon::now(),
                        'so_id'                             => $so_id
                    ]);

                    MaterialPreparation::where('auto_allocation_id',$id)->update([
                        'auto_allocation_id' => $new_allocation_based_on_qty_allocated->id
                    ]);

                    //insert history new allocation 
                    HistoryAutoAllocations::FirstOrCreate([
                        'auto_allocation_id'        => $new_allocation_based_on_qty_allocated->id,
                        'document_no_old'           => $document_no_old,
                        'c_bpartner_id_old'         => $c_bpartner_id_old,
                        'supplier_name_old'         => $supplier_name_old,
                        'po_buyer_old'              => $po_buyer_old,
                        'item_id_old'               => $item_old_id,
                        'item_code_old'             => $item_code_book_old,
                        'item_source_id_old'        => $item_source_old_id,
                        'item_code_source_old'      => $item_code_source_old,
                        'uom_old'                   => $uom_old,
                        'qty_allocated_old'         => $qty_allocation_old,
                        'warehouse_id_old'          => $warehouse_id_old,
                        'type_stock_erp_code_old'   => $type_stock_erp_code_old,
                        'type_stock_old'            => $type_stock_old,
        
                        'document_no_new'           => $document_no_old,
                        'c_bpartner_id_new'         => $c_bpartner_id_old,
                        'supplier_name_new'         => $supplier_name_old,
                        'po_buyer_new'              => $po_buyer_old,
                        'item_id_new'               => $item_old_id,
                        'item_code_new'             => $item_code_book_old,
                        'item_source_id_new'        => $item_source_old_id,
                        'item_code_source_new'      => $item_code_source_old,
                        'uom_new'                   => $uom_old,
                        'qty_allocated_new'         => $qty_allocated_old,
                        'warehouse_id_new'          => $warehouse_id_old,
                        'type_stock_erp_code_new'   => $type_stock_erp_code_old,
                        'type_stock_new'            => $type_stock_old,
                        
                        'remark'                    => $cancel_reason,
                        'is_integrate'              => true,
                        'user_id'                   => auth::user()->id
                    ]);

                    if($warehouse_id_new == '1000001' || $warehouse_id_new == '1000011') $update_allocations_fab [] = $new_allocation_based_on_qty_allocated->id;
                    else $update_allocations_acc[] = $new_allocation_based_on_qty_allocated->id;
                    
                    $spesific_id = $new_allocation_based_on_qty_allocated->id;
                }
                
                if($qty_outstanding_new > 0.0000)
                {
                    $new_allocation = AutoAllocation::firstorcreate([
                        'document_allocation_number'        => $booking_number_new,
                        'lc_date'                           => $lc_date_old,
                        'season'                            => $season_old,
                        'document_no'                       => $document_no_new,
                        'c_bpartner_id'                     => $c_bpartner_id_new,
                        'supplier_name'                     => $supplier_name_new,
                        'po_buyer'                          => $po_buyer_old,
                        'old_po_buyer'                      => $po_buyer_old_old,
                        'c_order_id'                        => $c_order_id_new,
                        'item_code_book'                    => $item_code_book_new,
                        'item_code_source'                  => $item_code_source_new,
                        'item_code'                         => $_item_code_book_new,
                        'item_id_book'                      => $item_book_new_id,
                        'item_id_source'                    => $item_source_new_id,
                        'item_desc'                         => $item_source_new_desc,
                        'article_no'                        => $article_no_new,
                        'category'                          => $category_new,
                        'uom'                               => $uom_new,
                        'warehouse_name'                    => $warehouse_name_new,
                        'warehouse_id'                      => $warehouse_id_new,
                        'qty_allocation'                    => $qty_outstanding_new,
                        'qty_outstanding'                   => $qty_outstanding_new,
                        'qty_allocated'                     => 0,
                        'is_fabric'                         => $is_fabric_new,
                        'is_allocation_purchase'            => $is_allocation_purchase_old,
                        'type_stock_erp_code'               => $type_stock_erp_code_old,
                        'type_stock'                        => $type_stock_old,
                        'is_already_generate_form_booking'  => false,
                        'is_upload_manual'                  => true,
                        'promise_date'                      => $promise_date_old,
                        'status_po_buyer'                   => $status_po_buyer_old,
                        'user_id'                           => auth::user()->id,
                        'created_at'                        => carbon::now(),
                        'updated_at'                        => carbon::now(),
                        'so_id'                             => $so_id
                    ]);
    
                    HistoryAutoAllocations::FirstOrCreate([
                        'auto_allocation_id'        => $new_allocation->id,
                        'document_no_old'           => $document_no_old,
                        'c_bpartner_id_old'         => $c_bpartner_id_old,
                        'supplier_name_old'         => $supplier_name_old,
                        'po_buyer_old'              => $po_buyer_old,
                        'item_id_old'               => $item_old_id,
                        'item_code_old'             => $item_code_book_old,
                        'item_source_id_old'        => $item_source_old_id,
                        'item_code_source_old'      => $item_code_source_old,
                        'uom_old'                   => $uom_old,
                        'qty_allocated_old'         => $qty_allocation_old,
                        'warehouse_id_old'          => $warehouse_id_old,
                        'type_stock_erp_code_old'   => $type_stock_erp_code_old,
                        'type_stock_old'            => $type_stock_old,
    
                        'document_no_new'           => $document_no_new,
                        'c_bpartner_id_new'         => $c_bpartner_id_new,
                        'supplier_name_new'         => $supplier_name_new,
                        'po_buyer_new'              => $po_buyer_old,
                        'item_id_new'               => $item_book_new_id,
                        'item_code_new'             => $item_code_book_new,
                        'item_source_id_new'        => $item_source_new_id,
                        'item_code_source_new'      => $item_code_source_new,
                        'uom_new'                   => $uom_new,
                        'qty_allocated_new'         => $qty_outstanding_new,
                        'warehouse_id_new'          => $warehouse_id_new,
                        'type_stock_erp_code_new'   => $type_stock_erp_code_old,
                        'type_stock_new'            => $type_stock_old,
                        
                        'remark'                    => $cancel_reason,
                        'is_integrate'              => true,
                        'user_id'                   => auth::user()->id
                    ]);
                    
                    if($warehouse_id_new == '1000001' || $warehouse_id_new == '1000011') $update_allocations_fab [] = $new_allocation->id;
                    else $update_allocations_acc[] = $new_allocation->id;

                    $spesific_id = $new_allocation->id;
                }
                
                if($is_system)
                {
                    HistoryAutoAllocations::FirstOrCreate([
                        'auto_allocation_id'        => $auto_allocation->id,
                        'document_no_old'           => $document_no_old,
                        'c_bpartner_id_old'         => $c_bpartner_id_old,
                        'supplier_name_old'         => $supplier_name_old,
                        'po_buyer_old'              => $po_buyer_old,
                        'item_id_old'               => $item_old_id,
                        'item_code_old'             => $item_code_book_old,
                        'item_source_id_old'        => $item_source_old_id,
                        'item_code_source_old'      => $item_code_source_old,
                        'uom_old'                   => $uom_old,
                        'qty_allocated_old'         => $qty_allocation_old,
                        'warehouse_id_old'          => $warehouse_id_old,
                        'type_stock_erp_code_old'   => $type_stock_erp_code_old,
                        'type_stock_old'            => $type_stock_old,
        
                        'document_no_new'           => $document_no_old,
                        'c_bpartner_id_new'         => $c_bpartner_id_old,
                        'supplier_name_new'         => $supplier_name_old,
                        'po_buyer_new'              => $po_buyer_old,
                        'item_id_new'               => $item_old_id,
                        'item_code_new'             => $item_code_book_old,
                        'item_source_id_new'        => $item_source_old_id,
                        'item_code_source_new'      => $item_code_source_old,
                        'uom_new'                   => $uom_old,
                        'qty_allocated_new'         => $qty_allocation_old,
                        'warehouse_id_new'          => $warehouse_id_old,
                        'type_stock_erp_code_new'   => $type_stock_erp_code_old,
                        'type_stock_new'            => $type_stock_old,
                        
                        'remark'                    => 'ALOKASI DARI ERP DI RUBAH',
                        'is_integrate'              => true,
                        'user_id'                   => auth::user()->id
                    ]);

                    $auto_allocation->qty_outstanding   = '0';
                    $auto_allocation->qty_allocated     = '0';
                    $auto_allocation->deleted_at        = carbon::now();
                    $auto_allocation->save();

                    $allocation_items = AllocationItem::where('auto_allocation_id', $id)->delete();
                }else
                {
                    // $auto_allocation->delete();
                    $delete = AutoAllocation::where('id', $id)->delete();
                }
                
            }else
            {
                if($qty_outstanding_new == 0.00000000)
                {
                    $auto_allocation->remark          = $cancel_reason;
                    $auto_allocation->qty_allocation  = $qty_allocation_new;
                    $auto_allocation->qty_outstanding = $qty_outstanding_new;

                    HistoryAutoAllocations::FirstOrCreate([
                        'auto_allocation_id'        => $auto_allocation->id,
                        'document_no_old'           => $document_no_old,
                        'c_bpartner_id_old'         => $c_bpartner_id_old,
                        'supplier_name_old'         => $supplier_name_old,
                        'po_buyer_old'              => $po_buyer_old,
                        'item_id_old'               => $item_old_id,
                        'item_code_old'             => $item_code_book_old,
                        'item_source_id_old'        => $item_source_old_id,
                        'item_code_source_old'      => $item_code_source_old,
                        'uom_old'                   => $uom_old,
                        'qty_allocated_old'         => $qty_allocation_old,
                        'warehouse_id_old'          => $warehouse_id_old,
                        'type_stock_erp_code_old'   => $type_stock_erp_code_old,
                        'type_stock_old'            => $type_stock_old,
        
                        'document_no_new'           => $document_no_old,
                        'c_bpartner_id_new'         => $c_bpartner_id_old,
                        'supplier_name_new'         => $supplier_name_old,
                        'po_buyer_new'              => $po_buyer_old,
                        'item_id_new'               => $item_old_id,
                        'item_code_new'             => $item_code_book_old,
                        'item_source_id_new'        => $item_source_old_id,
                        'item_code_source_new'      => $item_code_source_old,
                        'uom_new'                   => $uom_old,
                        'qty_allocated_new'         => $qty_allocated_old,
                        'warehouse_id_new'          => $warehouse_id_old,
                        'type_stock_erp_code_new'   => $type_stock_erp_code_old,
                        'type_stock_new'            => $type_stock_old,
                        
                        'remark'                    => $cancel_reason,
                        'is_integrate'              => true,
                        'user_id'                   => auth::user()->id
                    ]);
                }
                $spesific_id                                    = $auto_allocation->id;
                $auto_allocation->document_allocation_number    = $booking_number_new;
                $auto_allocation->save();
            }


            if(count($update_allocations_acc))
            {
                $this->doRecalculate($spesific_id);
                if(count($update_allocations_acc) > 0)
                {
                    $material_stocks = MaterialStock::where([
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                        ['is_stock_on_the_fly',false],
                    ])
                    ->whereNotNull('approval_date')
                    ->whereNull('deleted_at')
                    ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($update_allocations_acc) 
                    {
                        $query->select('c_order_id','item_id_source','warehouse_id')
                        ->from('auto_allocations')
                        ->whereIn('id',$update_allocations_acc)                    
                        ->groupby('c_order_id','item_id_source','warehouse_id');
                    })
                    ->get();
    
                    foreach ($material_stocks as $key => $material_stock )
                    {
                        if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, auth::user()->id, $movement_date);
                    }
                }
                //$this->updateAllocationAccessoriesItems($update_allocations_acc,$spesific_id);
            } 
            else if(count($update_allocations_fab))
            {
                $this->doRecalculate($spesific_id);
                $this->updateAllocationFabricItems($update_allocations_fab,$spesific_id);
            }
            
            // $data_response = ['status' => 200];

            DB::commit();
            return response()->json(200);

        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
            return response()->json(500);
            // $data_response = ['status' => 500];
        }

        // return response()->json(['data'=>$data_response]);
    }

    public function store(Request $request)
    {
        //return view('errors.503');
        $validator = Validator::make($request->all(), [
            'new_auto_allocations' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $new_auto_allocations = json_decode($request->new_auto_allocations);

            if(Session::has('flag')) Session::forget('flag');

            $update_allocations_acc = array();
            $update_allocations_fab = array();
            $movement_date          = carbon::now();

            try 
            {
                DB::beginTransaction();

                foreach ($new_auto_allocations as $key => $new_auto_allocation)
                {
                    $id                     = $new_auto_allocation->id;
                    $c_order_id             = $new_auto_allocation->c_order_id;
                    $type_stock             = $new_auto_allocation->type_stock;
                    $type_stock_erp_code    = $new_auto_allocation->type_stock_erp_code;
                    $booking_number         = $new_auto_allocation->booking_number;
                    $lc_date                = $new_auto_allocation->lc_date;
                    $season                 = $new_auto_allocation->season;
                    $c_bpartner_id          = $new_auto_allocation->c_bpartner_id;
                    $supplier_name          = $new_auto_allocation->supplier_name;
                    $document_no            = $new_auto_allocation->document_no;
                    $item_id                = $new_auto_allocation->item_id;
                    $item_code_source       = $new_auto_allocation->item_code_source;
                    $item_code_book         = $new_auto_allocation->item_code;
                    $item_desc              = $new_auto_allocation->item_desc;
                    $category               = $new_auto_allocation->category;
                    $po_buyer               = $new_auto_allocation->po_buyer;
                    $old_po_buyer           = $new_auto_allocation->old_po_buyer;
                    $uom                    = $new_auto_allocation->uom;
                    $warehouse_name         = $new_auto_allocation->warehouse_name;
                    $warehouse_id           = $new_auto_allocation->warehouse;
                    $error_upload           = $new_auto_allocation->error_upload;
                    $reason                 = strtoupper(trim($new_auto_allocation->reason));
                    $order_by               = $new_auto_allocation->order_by;
                    $remark                 = $new_auto_allocation->remark;
                    $article_no             = $new_auto_allocation->article_no;
                    $promise_date           = $new_auto_allocation->promise_date;
                    $is_additional          = $new_auto_allocation->is_additional;
                    $remark_additional      = $new_auto_allocation->remark_additional;
                    $so_id                  = PoBuyer::where('po_buyer', $po_buyer)->first();

                    //ITEM SOURCE NEW
                    $item_source_new        = Item::where(db::raw('item_code'),$item_code_source)->first();
                    $item_source_new_id     = ($item_source_new)? $item_source_new->item_id : null;
                    $item_source_new_desc   = ($item_source_new)? $item_source_new->item_desc : null;
                    $category_new           = ($item_source_new)? $item_source_new->category : null;
                    $uom_new                = ($item_source_new)? $item_source_new->uom : null;

                    //ITEM BOOK NEW
                    $item_new       = Item::where(db::raw('item_code'),$item_code_book)->first();
                    $item_id_new    = ($item_new)? $item_new->item_id : null;

                    $qty_allocation = sprintf('%0.8f',$new_auto_allocation->qty_allocation);
                    if($warehouse_id == '1000001' || $warehouse_id == '1000011')
                    {
                        $qty_outstanding = sprintf('%0.8f',$new_auto_allocation->qty_allocation);
                    }else{
                        $qty_outstanding = sprintf('%0.8f',$new_auto_allocation->qty_outstanding);
                    } 
                    
                    $pos_new = strpos($item_code_book, '|');
                    if ($pos_new === false)
                    {
                        $_item_code_book_new = $item_code_book;
                    } else 
                    {
                        $split = explode('|',$item_code_book);
                        $_item_code_book_new = $split[0];
                    }
                    
                    if(!$error_upload)
                    {
                        if($warehouse_id == '1000013' || $warehouse_id == '1000002') $is_fabric = false;
                        else $is_fabric = true;
                        
                        if($is_additional)
                        {
                            $auto_allocation = AutoAllocation::FirstOrCreate([
                                'document_allocation_number'        => $booking_number,
                                'lc_date'                           => $lc_date,
                                'c_order_id'                        => $c_order_id,
                                'season'                            => $season,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $po_buyer,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code_source,
                                'item_id_source'                    => $item_source_new_id,
                                'item_id_book'                      => $item_id_new,
                                'item_code'                         => $item_code_book,
                                'item_code_book'                    => $_item_code_book_new,
                                'article_no'                        => $article_no,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty_allocation,
                                'qty_outstanding'                   => $qty_allocation,
                                'qty_allocated'                     => 0,
                                'is_fabric'                         => $is_fabric,
                                'remark'                            => 'UPLOAD MANUAL'.($reason ? ', '.$reason : null),
                                'remark_update'                     => ($reason ? $reason : null),
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => true,
                                'is_additional'                     => $is_additional,
                                'remark_additional'                 => $remark_additional,
                                'type_stock'                        => $type_stock,
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'promise_date'                      => $promise_date,
                                'status_po_buyer'                   => 'active',
                                'user_id'                           => auth::user()->id,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'so_id'                             => $so_id->so_id
                            ]);
            
                            $note = 'TERJADI PENAMBAHAN ALOKASI';
                            $note_code = '1';
            
                            HistoryAutoAllocations::FirstOrCreate([
                                'auto_allocation_id'        => $auto_allocation->id,
                                'c_bpartner_id_new'         => $c_bpartner_id,
                                'supplier_name_new'         => $supplier_name,
                                'document_no_new'           => $document_no,
                                'item_id_new'               => $item_id_new,
                                'item_code_new'             => $item_code_book,
                                'item_source_id_new'        => $item_id,
                                'item_code_source_new'      => $item_code_source,
                                'po_buyer_new'              => $po_buyer,
                                'uom_new'                   => $uom,
                                'qty_allocated_new'         => $qty_allocation,
                                'warehouse_id_new'          => $warehouse_id,
                                'type_stock_new'            => $type_stock,
                                'type_stock_erp_code_new'   => $type_stock_erp_code,
                                'note_code'                 => $note_code,
                                'note'                      => $note,
                                'operator'                  => $qty_allocation,
                                'remark'                    => $remark_additional,
                                'is_integrate'              => true,
                                'user_id'                   => auth::user()->id
                            ]);
                            
                            if($is_fabric)
                            {
                                //$update_allocations_fab [] = $auto_allocation->id;
                                Temporary::FirstOrCreate([
                                    'barcode'               => $auto_allocation->id,
                                    'status'                => 'update_auto_allocation_fabric',
                                    'user_id'               => $auto_allocation->user_id,
                                    'created_at'            => $auto_allocation->created_at,
                                    'updated_at'            => $auto_allocation->updated_at,
                                ]);

                            } else {
                                $update_allocations_acc [] = $auto_allocation->id;
                            }
                        }else
                        {
                            $is_exists = AutoAllocation::where([
                                ['lc_date',$lc_date],
                                ['season',$season],
                                [db::raw('upper(document_no)'),$document_no],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['po_buyer',$po_buyer],
                                [db::raw('upper(item_code_source)'),$item_code_source],
                                [db::raw('upper(item_code)'),$item_code_book],
                                ['warehouse_id',$warehouse_id],
                                ['qty_allocation',$qty_allocation],
                                ['is_fabric',$is_fabric],
                                //['is_upload_manual',true],
                                //['type_stock_erp_code',$type_stock_erp_code],
                            ])
                            ->whereNull('deleted_at');
    
                            if($booking_number) $is_exists = $is_exists->where('document_allocation_number',$booking_number);
                            if($type_stock_erp_code) $is_exists = $is_exists->where('type_stock_erp_code',$type_stock_erp_code);
                            $is_exists = $is_exists->exists();
                
                            if(!$is_exists)
                            {
                                $auto_allocation = AutoAllocation::FirstOrCreate([
                                    'document_allocation_number'        => $booking_number,
                                    'lc_date'                           => $lc_date,
                                    'season'                            => $season,
                                    'document_no'                       => $document_no,
                                    'c_order_id'                        => $c_order_id,
                                    'c_bpartner_id'                     => $c_bpartner_id,
                                    'supplier_name'                     => $supplier_name,
                                    'po_buyer'                          => $po_buyer,
                                    'old_po_buyer'                      => $old_po_buyer,
                                    'item_code_source'                  => $item_code_source,
                                    'item_id_book'                      => $item_id_new,
                                    'item_id_source'                    => $item_source_new_id,
                                    'item_code'                         => $item_code_book,
                                    'item_code_book'                    => $_item_code_book_new,
                                    'article_no'                        => $article_no,
                                    'item_desc'                         => $item_desc,
                                    'category'                          => $category,
                                    'uom'                               => $uom,
                                    'warehouse_name'                    => $warehouse_name,
                                    'warehouse_id'                      => $warehouse_id,
                                    'qty_allocation'                    => $qty_allocation,
                                    'qty_outstanding'                   => $qty_allocation,
                                    'qty_allocated'                     => 0,
                                    'is_fabric'                         => $is_fabric,
                                    'remark'                            => 'UPLOAD MANUAL'.($reason ? ', '.$reason : null),
                                    'remark_update'                     => ($reason ? $reason : null),
                                    'is_already_generate_form_booking'  => false,
                                    'is_upload_manual'                  => true,
                                    'type_stock'                        => $type_stock,
                                    'type_stock_erp_code'               => $type_stock_erp_code,
                                    'promise_date'                      => $promise_date,
                                    'deleted_at'                        => null,
                                    'status_po_buyer'                   => 'active',
                                    'created_at'                        => $movement_date,
                                    'updated_at'                        => $movement_date,
                                    'user_id'                           => auth::user()->id,
                                    'so_id'                             => $so_id->so_id
                                ]);
                
                                $note = 'TERJADI PENAMBAHAN ALOKASI';
                                $note_code = '1';
                
                                HistoryAutoAllocations::FirstOrCreate([
                                    'auto_allocation_id'        => $auto_allocation->id,
                                    'c_bpartner_id_new'         => $c_bpartner_id,
                                    'supplier_name_new'         => $supplier_name,
                                    'document_no_new'           => $document_no,
                                    'item_id_new'               => $item_id,
                                    'item_code_new'             => $item_code_book,
                                    'item_source_id_new'        => $item_source_new_id,
                                    'item_code_source_new'      => $item_code_source,
                                    'po_buyer_new'              => $po_buyer,
                                    'uom_new'                   => $uom,
                                    'qty_allocated_new'         => $qty_allocation,
                                    'warehouse_id_new'          => $warehouse_id,
                                    'type_stock_new'            => $type_stock,
                                    'type_stock_erp_code_new'   => $type_stock_erp_code,
                                    'note_code'                 => $note_code,
                                    'note'                      => $note,
                                    'operator'                  => $qty_allocation,
                                    'is_integrate'              => true,
                                    'user_id'                   => auth::user()->id
                                ]);
                                
                                if($is_fabric)
                                {
                                    //$update_allocations_fab [] = $auto_allocation->id;
                                    Temporary::FirstOrCreate([
                                        'barcode'               => $auto_allocation->id,
                                        'status'                => 'update_auto_allocation_fabric',
                                        'user_id'               => $auto_allocation->user_id,
                                        'created_at'            => $auto_allocation->created_at,
                                        'updated_at'            => $auto_allocation->updated_at,
                                    ]);

                                } else {
                                    $update_allocations_acc [] = $auto_allocation->id;
                                }
        
                                 //close alokasi ila untuk keperluan dashboard
                                $auto_allocation_ila = AutoAllocation::where('po_buyer', $po_buyer)
                                ->where('item_code', $item_code_book)
                                ->where('document_no', 'like', '%S-%')
                                ->whereNull('deleted_at')
                                ->first();

                                if($auto_allocation_ila)
                                {
                                        $auto_allocation_ila->qty_in_house = $auto_allocation_ila->qty_allocation;
                                        $auto_allocation_ila->save();

                                        //alokasi yang ori ilangin 
                                        $auto_allocation_ori = AutoAllocation::where('po_buyer', $po_buyer)
                                        ->where('item_code', $item_code_book)
                                        ->where('document_no', 'not like', '%S-%')
                                        ->whereNull('deleted_at')
                                        ->first();

                                        if($auto_allocation_ori)
                                        {
                                        $auto_allocation_ori->deleted_at = $movement_date;
                                        $auto_allocation_ori->save();
                                        }
                                }
        
                            }
                        }
                    }
                
        
                }

                if(count($update_allocations_acc) > 0)
                {
                    $material_stocks = MaterialStock::where([
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                        ['is_stock_on_the_fly',false],
                    ])
                    ->whereNotNull('approval_date')
                    ->whereNull('deleted_at')
                    ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($update_allocations_acc) 
                    {
                        $query->select('c_order_id','item_id_source','warehouse_id')
                        ->from('auto_allocations')
                        ->whereIn('id',$update_allocations_acc)                    
                        ->groupby('c_order_id','item_id_source','warehouse_id');
                    })
                    ->get();
    
                    foreach ($material_stocks as $key => $material_stock )
                    {
                        if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, auth::user()->id, $movement_date);
                    }
                }
                
                //if(count($update_allocations_acc)) $this->updateAllocationAccessoriesItems($update_allocations_acc);
                //if(count($update_allocations_fab)) $this->updateAllocationFabricItems($update_allocations_fab);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
        
        Session::flash('flag', 'success');
        return response()->json(200);
    }

    public function allocatingPoBuyer(Request $request)
    {
        //return view('errors.503');
        if(auth::user()->warehouse == '1000011' || auth::user()->warehouse == '1000001' )
        {
            $auto_allocation_id = [$request->id];
            $this->updateAllocationFabricItems($auto_allocation_id,$request->id);
        }else
        {
            $auto_allocation_id = [$request->id];
            $is_purchase_item_stock = AutoAllocation::where('id',$request->id)
            ->where('is_allocation_purchase',true)
            ->exists();

            // dd($is_purchase_item_stock);

            $material_stocks = MaterialStock::where([
                ['is_allocated',false],
                ['is_closing_balance',false],
                ['is_stock_on_the_fly',false],
            ])
            ->whereNotNull('approval_date')
            ->whereNull('deleted_at')
            ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($auto_allocation_id) 
            {
                $query->select('c_order_id','item_id_source','warehouse_id')
                ->from('auto_allocations')
                ->where('id',$auto_allocation_id)                    
                ->groupby('c_order_id','item_id_source','warehouse_id');
            })
            ->get();

            foreach ($material_stocks as $key => $material_stock )
            {
                if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, auth::user()->id, carbon::now());
            }
        } 
    }

    public function cancelAllocation(Request $request)
    {
        //return view('errors.503');
        if(auth::user()->hasRole(['admin-ict-fabric','admin-ict-acc','mm-staff','mm-staff-acc']))
        {
            $auto_allocation_id     = $request->id;
            $cancel_reason          = strtoupper($request->cancel_reason);
            $this->doCancellation($auto_allocation_id,$cancel_reason,auth::user()->id);
        }else
        {
            return response()->json('BRO DAN SIST, ROLE MU BUKAN ICT. SILAHKAN APPLY JADI ICT DLU SUPAYA DAPAT ROLE INI. ',422);
        }
        
    }

    public function getPoSupplierPickList(Request $request)
    {
        $q           = trim(strtoupper($request->q));

        if(auth::user()->warehouse == '1000013' || auth::user()->warehouse == '1000002' )
        {
            $lists      = db::table('po_suppliers_v')
            ->where('document_no','like',"%$q%")
            ->paginate(10);
        }else if(auth::user()->warehouse == '1000011' || auth::user()->warehouse == '1000001' )
        {
            $lists      = db::table('po_supplier_fabric_v')
            ->where('document_no','like',"%$q%")
            ->paginate(10);
        }

        return view('master_data_auto_allocation._document_no_list',compact('lists'));
    }

    public function getItemPicklist(Request $request)
    {
        
        if(auth::user()->hasRole(['mm-staff','admin-ict-fabric']))$warehouse_id = ['1000011','1000001'];
        else if(auth::user()->hasRole(['mm-staff-acc','admin-ict-acc']))$warehouse_id = ['1000013','1000002'];
        else $warehouse_id = [auth::user()->warehouse];

        
        $q = strtoupper($request->q);
        $lists = Item::where(function($query) use($q){
            $query->where(db::raw('upper(item_code)'),'like',"%$q%")
            ->orWhere(db::raw('upper(item_desc)'),'like',"%$q%");
        });

        if(auth::user()->warehouse == '1000011' || auth::user()->warehouse == '1000001') $lists = $lists->where('category','FB');
        else $lists = $lists->where('category','!=','FB');
        
        $lists = $lists->paginate(10);

        return view('master_data_auto_allocation._item_list',compact('lists'));
    }

    public function getPoBuyerPickList(Request $request)
    {
        $item_code  = strtoupper($request->item_code);
        $type_stock = strtoupper($request->type_stock);
        $q          = strtoupper($request->q);
        $page       = 'auto_allocation';

        $lists = PoBuyer::select('po_buyer','type_stock','type_stock_erp_code','brand','season',db::raw("to_char(lc_date,'yyyy-mm-dd') as lc_date"),db::raw("to_char(statistical_date,'yyyy-mm-dd') as statistical_date"),db::raw("to_char(promise_date,'yyyy-mm-dd') as promise_date"));
        
        //if($type_stock) $lists = $lists->where('type_stock',$type_stock);

        $lists = $lists->where(function($query) use($q){
            $query->where(db::raw('upper(po_buyer)'),'like',"%$q%");
        })
        ->whereIn('po_buyer',function($query) use ($item_code){
            $query->select('po_buyer')
            ->from('material_requirements')
            ->where('item_code',$item_code)
            ->groupby('po_buyer');
        })
        ->groupby('po_buyer','type_stock','brand','type_stock_erp_code','season',db::raw("to_char(lc_date,'yyyy-mm-dd')"),db::raw("to_char(promise_date,'yyyy-mm-dd')"),db::raw("to_char(statistical_date,'yyyy-mm-dd')"))
        ->paginate(10);

        return view('modal_lov._buyer_list',compact('lists','page'));
    }

    
    public function downloadFormBooking(Request $request)
    {
        //return view('errors.503');
        $filename = 'report_form_booking_acc';
        $file = Config::get('storage.report') . '/' . e($filename).'.csv';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;

        
       /*$ $validator =  Validator::make($request->all(), [
            'lc_date' => 'required'
        ]);

        if ($validator->passes()){
            
            
            lc_date = Carbon::createFromFormat('d/m/Y', $request->lc_date)->format('Y-m-d');
            $allocation_items = AllocationItem::where([
                ['lc_date',$lc_date],
                ['remark','AUTO ALLOCATION'],
            ]);

            if(auth::user()->hasRole('mm-staff')){
                $allocation_items = $allocation_items->whereIn('warehouse_inventory',['1000011','1000001']);
            }else if(auth::user()->hasRole('mm-staff-acc')){
                $allocation_items = $allocation_items->whereIn('warehouse',['1000013','1000002']);
            }

            $allocation_items = $allocation_items
            ->whereNull('deleted_at')
            ->where(function($query){
                $query->where('confirm_by_warehouse','approved')
                ->OrWhereNull('confirm_by_warehouse');
            })
            ->whereNotNull('auto_allocation_id')
            ->get();

            return Excel::create('FORM_BOOKING'.$lc_date,function ($excel) use($allocation_items){
                $excel->sheet('active', function($sheet) use($allocation_items){
                    $sheet->setCellValue('A1','TANGGAL LC');
                    $sheet->setCellValue('B1','KODE SUPPLIER');
                    $sheet->setCellValue('C1','NAMA SUPPLIER');
                    $sheet->setCellValue('D1','NO. PO SUPPLIER');
                    $sheet->setCellValue('E1','PO BUYER ASAL');
                    $sheet->setCellValue('F1','PO BUYER TUJUAN');
                    $sheet->setCellValue('G1','KODE ITEM');
                    $sheet->setCellValue('H1','DEKSRIPSI ITEM');
                    $sheet->setCellValue('I1','KETEGORI');
                    $sheet->setCellValue('J1','STYLE');
                    $sheet->setCellValue('K1','ARTICLE NO');
                    $sheet->setCellValue('L1','UOM');
                    $sheet->setCellValue('M1','QTY BOOKING');
                    $sheet->setCellValue('N1','GUDANG PENYIMPANAN');
                    $sheet->setCellValue('O1','ALOKASI FABRIC');
                    $sheet->setCellValue('P1','LOKASI');
                    $sheet->setCellValue('Q1','IS_ALREADY_PRINT');
                    
                    $index = 0;
                    foreach ($allocation_items as $key => $allocation_item) {
                        $row = $index + 2;
                        $index++;
                        $lc_date = ($allocation_item->lc_date)? $allocation_item->lc_date->format('Y-m-d') : null;
                        $supplier_code = $allocation_item->supplier_code;
                        $supplier_name = $allocation_item->supplier_name;
                        $document_no = $allocation_item->document_no;
                        $po_buyer_source = $allocation_item->po_buyer_source;
                        $po_buyer = $allocation_item->po_buyer;
                        $item_code = $allocation_item->item_code;
                        $item_desc = $allocation_item->item_desc;
                        $category = $allocation_item->category;
                        $style = $allocation_item->style;
                        $article_no = $allocation_item->article_no;
                        $uom = $allocation_item->uom;
                        $qty_booking = $allocation_item->qty_booking;
                        $is_from_allocation_fabric = $allocation_item->is_from_allocation_fabric;
                        $material_preparation_id = $allocation_item->material_preparation_id;

                        if($is_from_allocation_fabric){
                            $summary_stock_fabric_id = $allocation_item->summary_stock_fabric_id;
                            if($allocation_item->warehouse_inventory == '1000001') $warehouse_inventory = 'FAB-AOI-1';
                            else if($allocation_item->warehouse_inventory == '1000011') $warehouse_inventory = 'FAB-AOI-2';
                            else $warehouse_inventory = null;

                            $location = $allocation_item->getLocatorFabric($summary_stock_fabric_id);
                        }else{
                            if($allocation_item->warehouse == '1000002') $warehouse_inventory = 'ACC-AOI-1';
                            else if($allocation_item->warehouse == '1000013') $warehouse_inventory = 'ACC-AOI-2';
                            else $warehouse_inventory = null;

                            $location = ($allocation_item->materialStock->locator)? $allocation_item->materialStock->locator->code : null;
                        }
                        
                        if($material_preparation_id) $is_already_print = true;
                        else $is_already_print = false;

                        $sheet->setCellValue('A'.$row, $lc_date);
                        $sheet->setCellValue('B'.$row, $supplier_code);
                        $sheet->setCellValue('C'.$row, $supplier_name);
                        $sheet->setCellValue('D'.$row, $document_no);
                        $sheet->setCellValue('E'.$row, $po_buyer_source);
                        $sheet->setCellValue('F'.$row, $po_buyer);
                        $sheet->setCellValue('G'.$row, $item_code);
                        $sheet->setCellValue('H'.$row, $item_desc);
                        $sheet->setCellValue('I'.$row, $category);
                        $sheet->setCellValue('J'.$row, $style);
                        $sheet->setCellValue('K'.$row, $article_no);
                        $sheet->setCellValue('L'.$row, $uom);
                        $sheet->setCellValue('M'.$row, $qty_booking);
                        $sheet->setCellValue('N'.$row, $warehouse_inventory);
                        $sheet->setCellValue('O'.$row, $is_from_allocation_fabric);
                        $sheet->setCellValue('P'.$row, $location);
                        $sheet->setCellValue('Q'.$row, $is_already_print);
                        
                    }
                    
                });
            })
            ->export('xlsx');
            

           
           
        }else{
            return redirect()->action('AutoAllocationController@index')
            ->withErrors($validator);
        }*/
    }

    public function exportToExcel()
    {
        //return view('errors.503');
        $warehouse = auth::user()->warehouse;

        if($warehouse == '1000013' || $warehouse == '1000002') $filename = 'report_auto_allocation_acc.csv';
        else $filename = 'report_auto_allocation_fab.csv';

        $file = Config::get('storage.report') . '/' . e($filename);

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function exportToExcelPR()
    {
        //return view('errors.503');
        $warehouse = auth::user()->warehouse;

        if($warehouse == '1000013' || $warehouse == '1000002') $filename = 'report_allocation_pr_acc.csv';
        else $filename = 'report_allocation_pr_fab.csv';

        $file = Config::get('storage.report') . '/' . e($filename);

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function poSupplierAll()
    {
        //return view('errors.503');
        $filename = 'report_po_supplier';
        $file = Config::get('storage.report') . '/' . e($filename).'.csv';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
    
    public function exportFormInput()
    {
        $warehouse_id = auth::user()->warehouse;
        if($warehouse_id == '1000013' || $warehouse_id == '1000002')  $filename = 'form_auto_allocation_acc';
        else $filename = 'form_auto_allocation_fab';
        
        $file = Config::get('storage.auto_allocation') . '/' . e($filename).'.xlsx';

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function exportFormDeleteInput()
    {
        return Excel::create('delete_allocation',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('B1','REMARK_UPDATE');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadFormInput(Request $request)
    {
        //return view('errors.503');
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                //return response()->json($data);
                foreach ($data as $key => $value)
                {
                    $id                     = null;//$value->id;
                    $booking_number         = trim(strtoupper($value->booking_number));
                    
                    /*$document_no_old        = trim(strtoupper($value->no_po_supplier_old));
                    $item_code_source_old   = trim(strtoupper($value->item_code_source_old));
                    $item_code_book_old     = trim(strtoupper($value->item_code_book_old));
                    $po_buyer_old           = trim(strtoupper($value->po_buyer_old));
                    $qty_input_old          = sprintf('%0.8f',$value->qty_input_old);
                    $warehouse_name_old     = trim(strtoupper($value->warehouse_inventory_old));
                    $remark_additional_old  = trim(strtoupper($value->remark_additional_old));
                    $is_additional_old      = trim(strtoupper($value->is_additional_old));*/

                    $supplier_code_new      = trim(strtoupper($value->supplier_code_new));
                    $document_no_new        = trim(strtoupper($value->no_po_supplier_new));
                    $item_code_source_new   = trim(strtoupper($value->item_code_source_new));
                    $item_code_book_new     = trim(strtoupper($value->item_code_book_new));
                    $po_buyer_new           = trim(strtoupper($value->po_buyer_new));
                    $uom_from               = trim(strtoupper($value->uom));
                    $qty_input_new          = sprintf('%0.8f',$value->qty_input_new);
                    $warehouse_name_new     = trim(strtoupper($value->warehouse_inventory_new));
                    $remark_additional_new  = trim(strtoupper($value->remark_additional_new));
                    $is_additional_new      = trim(strtoupper($value->is_additional_new));
                    $reason                 = trim(strtoupper($value->reason_update));
                    

                    if($document_no_new && $item_code_book_new && $po_buyer_new && $uom_from && $qty_input_new && $warehouse_name_new)
                    {
                        $document_no        = $document_no_new;
                        $item_code_source   = $item_code_source_new;
                        $item_code_book     = $item_code_book_new;
                        $_po_buyer          = $po_buyer_new;
                        $qty_input          = $qty_input_new;
                        $warehouse_name     = $warehouse_name_new;
                        $remark_additional  = $remark_additional_new;
                        $_is_additional     = trim(strtoupper($value->is_additional_new));

                        $reroute            = ReroutePoBuyer::where('old_po_buyer',$_po_buyer)->first();
                        if($reroute)
                        {
                            $po_buyer       = $reroute->new_po_buyer;
                            $old_po_buyer   = $reroute->old_po_buyer;
                        }else
                        {
                            $po_buyer       = $_po_buyer;
                            $old_po_buyer   = $_po_buyer;
                        }

                        if(!$_is_additional || $_is_additional == 'FALSE' || $_is_additional == 'f') $is_additional = false;
                        else
                        {
                            if($_is_additional == 'TRUE' || $_is_additional == 't' || $_is_additional == true )$is_additional = true;
                            else $is_additional = false;
                        }

                        $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
                        $type_stock_erp_code    = ($_po_buyer)? $_po_buyer->type_stock_erp_code : null;
                        $type_stock             = ($_po_buyer)? $_po_buyer->type_stock : null;
                        $lc_date                = ($_po_buyer)? $_po_buyer->lc_date->format('Y-m-d') : null;
                        
                        if($_po_buyer)
                        {
                            if($_po_buyer->statistical_date) $promise_date  = $_po_buyer->statistical_date->format('Y-m-d');
                            else $promise_date                              = $_po_buyer->promise_date->format('Y-m-d');

                            $season =  $_po_buyer->season;
                        }else
                        {
                            $promise_date = null;
                            $season       = null; 
                        }

                        $pos = strpos($item_code_book, '|');
                        if ($pos === false)
                        {
                            $_item_code     = $item_code_book;
                            $_article_no    = null;
                        } else {
                            $split          = explode('|',$item_code_book);
                            $_item_code     = $split[0];
                            $_article_no    = $split[1];
                        }

                        $item_code_source = ($item_code_source)? $item_code_source :  $_item_code;
                        $master_item_book = Item::where(db::raw('item_code'),$_item_code)->first();
    
                        $item_desc          = ($master_item_book ? $master_item_book->item_desc : null);
                        $item_id            = ($master_item_book ? $master_item_book->item_id : null);
                        $category           = ($master_item_book ? $master_item_book->category : null);
                        $article_no         = $_article_no;
    
                        $master_item_source = Item::where(db::raw('item_code'),$item_code_source)->first();
    
                        if($master_item_source)
                        {
                            $item_id_source = ($master_item_source ? $master_item_source->item_id : null);
                            //jika uom tidak sama dengan master nya
                            if($master_item_source->uom != $uom_from)
                            {
                                $uom_conversion = UomConversion::where([
                                    ['item_id', $master_item_source->item_id],
                                    ['uom_from', $uom_from],
                                    ['uom_to', $master_item_source->uom],
                                ])
                                ->first();
                            
                                $dividerate     = ($uom_conversion ? $uom_conversion->dividerate : 1);
                                $qty_input      = sprintf('%0.8f', $qty_input * $dividerate);
                                $uom            = ($uom_conversion ? $uom_conversion->uom_to :$uom_from);
                            }else
                            {
                                $uom            = $uom_from;
                                $qty_input      = sprintf('%0.8f', $qty_input);
                            }
                        }

                        if($warehouse_name == 'ACCESSORIES AOI 2' || $warehouse_name == 'ACCESSORIES AOI-2' || $warehouse_name == 'AKSESORIS AOI-2' || $warehouse_name == 'AKSESORIS AOI 2' || $warehouse_name == 'ACC AOI 2' || $warehouse_name == 'ACC AOI 2') $warehouse_id = '1000013';
                        else if($warehouse_name == 'ACCESSORIES AOI 1' || $warehouse_name == 'ACCESSORIES AOI-1' || $warehouse_name == 'AKSESORIS AOI-1' || $warehouse_name == 'AKSESORIS AOI 1' || $warehouse_name == 'ACC AOI-1' || $warehouse_name == 'ACC AOI 1') $warehouse_id = '1000002';
                        else if($warehouse_name == 'FABRIC AOI 2' || $warehouse_name == 'FABRIC AOI-2' || $warehouse_name == 'FABRIK AOI-2' || $warehouse_name == 'FABRIK AOI 2' || $warehouse_name == 'FAB AOI-2' || $warehouse_name == 'FAB AOI 2') $warehouse_id = '1000011';
                        else if($warehouse_name == 'FABRIC AOI 1' || $warehouse_name == 'FABRIC AOI-1' || $warehouse_name == 'FABRIK AOI-1' || $warehouse_name == 'FABRIK AOI 1' || $warehouse_name == 'FAB AOI-1' || $warehouse_name == 'FAB AOI 1') $warehouse_id = '1000001';
                        else $warehouse_id = null;


                        if($warehouse_id == '1000011' || $warehouse_id == '1000001') $is_fabric = true;
                        $is_fabric = false;

                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                        {
                            $c_order_id         = 'FREE STOCK';
                            if($supplier_code_new)
                            {
                                $master_supplier = Supplier::where('supplier_code',$supplier_code_new)->first();
                                if($master_supplier)
                                {
                                    $c_bpartner_id      = $master_supplier->c_bpartner_id;
                                    $supplier_name      = $master_supplier->supplier_name;
                                }else
                                {
                                    $c_bpartner_id      = null;
                                    $supplier_name      = null;
                                }
                                
                            }else
                            {
                                $c_bpartner_id      = 'FREE STOCK';
                                $supplier_name      = 'FREE STOCK';
                            }
                            
                        }else
                        {   $po_supplier        = PoSupplier::where(db::raw("upper(document_no)"),$document_no)->first();
                            $c_order_id         = ($po_supplier ? $po_supplier->c_order_id : null);
                            $c_bpartner_id      = ($po_supplier ? $po_supplier->c_bpartner_id : null);
                            $supplier_name      = ($po_supplier ? $po_supplier->supplier_name : null);
                        }
                        
                        if($warehouse_id)
                        {
                            if($c_order_id)
                            {
                                if($item_id)
                                {
                                    if($master_item_source)
                                    {
                                        if($c_bpartner_id)
                                        {
                                            $is_bom_exists = MaterialRequirement::where([
                                                ['po_buyer',$po_buyer],
                                                ['item_id',$item_id],
                                            ])
                                            ->exists();
                    
                                            if($is_bom_exists)
                                            {
                                                if($is_additional)
                                                {
                                                    $obj                        = new stdClass();
                                                    $obj->id                    = $id;
                                                    $obj->c_order_id            = $c_order_id;
                                                    $obj->type_stock            = $type_stock;
                                                    $obj->type_stock_erp_code   = $type_stock_erp_code;
                                                    $obj->booking_number        = $booking_number;
                                                    $obj->lc_date               = $lc_date;
                                                    $obj->promise_date          = $promise_date;
                                                    $obj->season                = $season;
                                                    $obj->c_bpartner_id         = $c_bpartner_id;
                                                    $obj->supplier_name         = $supplier_name;
                                                    $obj->document_no           = $document_no;
                                                    $obj->item_id               = $item_id;
                                                    $obj->item_id_source        = $item_id_source;
                                                    $obj->item_code_source      = $item_code_source;
                                                    $obj->item_code             = $item_code_book;
                                                    $obj->item_desc             = $item_desc;
                                                    $obj->category              = $category;
                                                    $obj->po_buyer              = $po_buyer;
                                                    $obj->old_po_buyer          = $old_po_buyer;
                                                    $obj->uom                   = $uom;
                                                    $obj->article_no            = $article_no;
                                                    $obj->qty_allocation        = $qty_input;
                                                    $obj->qty_outstanding       = 0;
                                                    $obj->warehouse_name        = $warehouse_name;
                                                    $obj->warehouse             = $warehouse_id;
                                                    $obj->is_additional         = $is_additional;
                                                    $obj->remark_additional     = $remark_additional;
                                                    $obj->error_upload          = false;
                                                    $obj->reason                = $reason;
                                                    $obj->order_by              = 2;
                                                    $obj->remark                = 'READY TO INSERT';
                                                    $array []                   = $obj;
                                                }else
                                                {
                                                    $is_exists = AutoAllocation::where([
                                                        ['lc_date',$lc_date],
                                                        ['season',$season],
                                                        [db::raw('upper(document_no)'),$document_no],
                                                        ['c_bpartner_id',$c_bpartner_id],
                                                        ['po_buyer',$po_buyer],
                                                        ['item_id_book',$item_id],
                                                        ['item_id_source',$item_id_source],
                                                        ['warehouse_id',$warehouse_id],
                                                        ['qty_allocation',$qty_input],
                                                    ])
                                                    ->whereNull('deleted_at');
                            
                                                    if($booking_number) $is_exists = $is_exists->where('document_allocation_number',$booking_number);
                                                    if($type_stock_erp_code) $is_exists = $is_exists->where('type_stock_erp_code',$type_stock_erp_code);
                                                    $is_exists = $is_exists->exists();
                                                    
                                                    if(!$is_exists)
                                                    {
                                                        $obj                        = new stdClass();
                                                        $obj->id                    = $id;
                                                        $obj->c_order_id            = $c_order_id;
                                                        $obj->type_stock            = $type_stock;
                                                        $obj->type_stock_erp_code   = $type_stock_erp_code;
                                                        $obj->booking_number        = $booking_number;
                                                        $obj->lc_date               = $lc_date;
                                                        $obj->promise_date          = $promise_date;
                                                        $obj->season                = $season;
                                                        $obj->c_bpartner_id         = $c_bpartner_id;
                                                        $obj->supplier_name         = $supplier_name;
                                                        $obj->document_no           = $document_no;
                                                        $obj->item_id               = $item_id;
                                                        $obj->item_id_source        = $item_id_source;
                                                        $obj->item_code_source      = $item_code_source;
                                                        $obj->item_code             = $item_code_book;
                                                        $obj->item_desc             = $item_desc;
                                                        $obj->category              = $category;
                                                        $obj->po_buyer              = $po_buyer;
                                                        $obj->old_po_buyer          = $old_po_buyer;
                                                        $obj->uom                   = $uom;
                                                        $obj->article_no            = $article_no;
                                                        $obj->qty_allocation        = $qty_input;
                                                        $obj->qty_outstanding       = 0;
                                                        $obj->warehouse_name        = $warehouse_name;
                                                        $obj->warehouse             = $warehouse_id;
                                                        $obj->is_additional         = $is_additional;
                                                        $obj->remark_additional     = $remark_additional;
                                                        $obj->error_upload          = false;
                                                        $obj->reason                = $reason;
                                                        $obj->order_by              = 2;
                                                        $obj->remark                = 'READY TO INSERT';
                                                        $array []                   = $obj;
                                                    }else
                                                    {
                                                        $obj                        = new stdClass();
                                                        $obj->id                    = $id;
                                                        $obj->c_order_id            = null;
                                                        $obj->type_stock            = null;
                                                        $obj->type_stock_erp_code   = null;
                                                        $obj->booking_number        = $booking_number;
                                                        $obj->lc_date               = null;
                                                        $obj->promise_date          = null;
                                                        $obj->season                = null;
                                                        $obj->c_bpartner_id         = null;
                                                        $obj->supplier_name         = null;
                                                        $obj->document_no           = $document_no;
                                                        $obj->item_id               = null;
                                                        $obj->item_id_source        = null;
                                                        $obj->item_code_source      = $item_code_source;
                                                        $obj->item_code             = $item_code_book;
                                                        $obj->item_desc             = null;
                                                        $obj->category              = null;
                                                        $obj->po_buyer              = $po_buyer;
                                                        $obj->old_po_buyer          = $old_po_buyer;
                                                        $obj->uom                   = null;
                                                        $obj->article_no            = null;
                                                        $obj->qty_allocation        = $qty_input;
                                                        $obj->qty_outstanding       = null;
                                                        $obj->warehouse_name        = $warehouse_name;
                                                        $obj->warehouse             = null;
                                                        $obj->is_additional         = null;
                                                        $obj->remark_additional     = null;
                                                        $obj->error_upload          = true;
                                                        $obj->reason                = $reason;
                                                        $obj->order_by              = 0;
                                                        $obj->remark                = 'UPLOAD GAGAL, ALOKASI SUDAH DI NAIKAN';
                                                        $array []                   = $obj;
                                                    }
                                                }
                                            }else
                                            {
                                                $obj                        = new stdClass();
                                                $obj->id                    = $id;
                                                $obj->c_order_id            = null;
                                                $obj->type_stock            = null;
                                                $obj->type_stock_erp_code   = null;
                                                $obj->booking_number        = $booking_number;
                                                $obj->lc_date               = null;
                                                $obj->promise_date          = null;
                                                $obj->season                = null;
                                                $obj->c_bpartner_id         = null;
                                                $obj->supplier_name         = null;
                                                $obj->document_no           = $document_no;
                                                $obj->item_id               = null;
                                                $obj->item_id_source        = null;
                                                $obj->item_code_source      = $item_code_source;
                                                $obj->item_code             = $item_code_book;
                                                $obj->item_desc             = null;
                                                $obj->category              = null;
                                                $obj->po_buyer              = $po_buyer;
                                                $obj->old_po_buyer          = $old_po_buyer;
                                                $obj->uom                   = null;
                                                $obj->article_no            = null;
                                                $obj->qty_allocation        = $qty_input;
                                                $obj->qty_outstanding       = null;
                                                $obj->warehouse_name        = $warehouse_name;
                                                $obj->warehouse             = null;
                                                $obj->is_additional         = null;
                                                $obj->remark_additional     = null;
                                                $obj->error_upload          = true;
                                                $obj->reason                = $reason;
                                                $obj->order_by              = 0;
                                                $obj->remark                = 'UPLOAD GAGAL, ITEM TIDAK DI TEMUKAN DI BOM';
                                                $array []                   = $obj;
                                            }
                                        }else
                                        {
                                            $obj                        = new stdClass();
                                            $obj->id                    = $id;
                                            $obj->c_order_id            = null;
                                            $obj->type_stock            = null;
                                            $obj->type_stock_erp_code   = null;
                                            $obj->booking_number        = $booking_number;
                                            $obj->lc_date               = null;
                                            $obj->promise_date          = null;
                                            $obj->season                = null;
                                            $obj->c_bpartner_id         = null;
                                            $obj->supplier_name         = null;
                                            $obj->document_no           = $document_no;
                                            $obj->item_id               = null;
                                            $obj->item_id_source        = null;
                                            $obj->item_code_source      = $item_code_source;
                                            $obj->item_code             = $item_code_book;
                                            $obj->item_desc             = null;
                                            $obj->category              = null;
                                            $obj->po_buyer              = $po_buyer;
                                            $obj->old_po_buyer          = $old_po_buyer;
                                            $obj->uom                   = null;
                                            $obj->article_no            = null;
                                            $obj->qty_allocation        = $qty_input;
                                            $obj->qty_outstanding       = null;
                                            $obj->warehouse_name        = $warehouse_name;
                                            $obj->warehouse             = null;
                                            $obj->is_additional         = null;
                                            $obj->remark_additional     = null;
                                            $obj->error_upload          = true;
                                            $obj->reason                = $reason;
                                            $obj->order_by              = 0;
                                            $obj->remark                = 'UPLOAD GAGAL, KODE SUPPLIER / SUPPLIER TIDAK DITEMUKAN';
                                            $array []                   = $obj;
                                        }
                                    }else
                                    {
                                        $obj                        = new stdClass();
                                        $obj->id                    = $id;
                                        $obj->c_order_id            = null;
                                        $obj->type_stock            = null;
                                        $obj->type_stock_erp_code   = null;
                                        $obj->booking_number        = $booking_number;
                                        $obj->lc_date               = null;
                                        $obj->promise_date          = null;
                                        $obj->season                = null;
                                        $obj->c_bpartner_id         = null;
                                        $obj->supplier_name         = null;
                                        $obj->document_no           = $document_no;
                                        $obj->item_id               = null;
                                        $obj->item_id_source        = null;
                                        $obj->item_code_source      = $item_code_source;
                                        $obj->item_code             = $item_code_book;
                                        $obj->item_desc             = null;
                                        $obj->category              = null;
                                        $obj->po_buyer              = $po_buyer;
                                        $obj->old_po_buyer          = $old_po_buyer;
                                        $obj->uom                   = null;
                                        $obj->article_no            = null;
                                        $obj->qty_allocation        = $qty_input;
                                        $obj->qty_outstanding       = null;
                                        $obj->warehouse_name        = $warehouse_name;
                                        $obj->warehouse             = null;
                                        $obj->is_additional         = null;
                                        $obj->remark_additional     = null;
                                        $obj->error_upload          = true;
                                        $obj->reason                = $reason;
                                        $obj->order_by              = 0;
                                        $obj->remark                = 'UPLOAD GAGAL, MASTER ITEM SOURCE TIDAK DITEMUKAN';
                                        $array []                   = $obj;
                                    }
                                }else
                                {
                                    $obj                        = new stdClass();
                                    $obj->id                    = $id;
                                    $obj->c_order_id            = null;
                                    $obj->type_stock            = null;
                                    $obj->type_stock_erp_code   = null;
                                    $obj->booking_number        = $booking_number;
                                    $obj->lc_date               = null;
                                    $obj->promise_date          = null;
                                    $obj->season                = null;
                                    $obj->c_bpartner_id         = null;
                                    $obj->supplier_name         = null;
                                    $obj->document_no           = $document_no;
                                    $obj->item_id               = null;
                                    $obj->item_id_source        = null;
                                    $obj->item_code_source      = $item_code_source;
                                    $obj->item_code             = $item_code_book;
                                    $obj->item_desc             = null;
                                    $obj->category              = null;
                                    $obj->po_buyer              = $po_buyer;
                                    $obj->old_po_buyer          = $old_po_buyer;
                                    $obj->uom                   = null;
                                    $obj->article_no            = null;
                                    $obj->qty_allocation        = $qty_input;
                                    $obj->qty_outstanding       = null;
                                    $obj->warehouse_name        = $warehouse_name;
                                    $obj->warehouse             = null;
                                    $obj->is_additional         = null;
                                    $obj->remark_additional     = null;
                                    $obj->error_upload          = true;
                                    $obj->reason                = $reason;
                                    $obj->order_by              = 0;
                                    $obj->remark                = 'UPLOAD GAGAL, MASTER ITEM BOOK TIDAK DITEMUKAN';
                                    $array []                   = $obj;
                                }
                            }else
                            {
                                $obj                        = new stdClass();
                                $obj->id                    = $id;
                                $obj->c_order_id            = null;
                                $obj->type_stock            = null;
                                $obj->type_stock_erp_code   = null;
                                $obj->booking_number        = $booking_number;
                                $obj->lc_date               = null;
                                $obj->promise_date          = null;
                                $obj->season                = null;
                                $obj->c_bpartner_id         = null;
                                $obj->supplier_name         = null;
                                $obj->document_no           = $document_no;
                                $obj->item_id               = null;
                                $obj->item_id_source        = null;
                                $obj->item_code_source      = $item_code_source;
                                $obj->item_code             = $item_code_book;
                                $obj->item_desc             = null;
                                $obj->category              = null;
                                $obj->po_buyer              = $po_buyer;
                                $obj->old_po_buyer          = $old_po_buyer;
                                $obj->uom                   = null;
                                $obj->article_no            = null;
                                $obj->qty_allocation        = $qty_input;
                                $obj->qty_outstanding       = null;
                                $obj->warehouse_name        = $warehouse_name;
                                $obj->warehouse             = null;
                                $obj->is_additional         = null;
                                $obj->remark_additional     = null;
                                $obj->error_upload          = true;
                                $obj->reason                = $reason;
                                $obj->order_by              = 0;
                                $obj->remark                = 'UPLOAD GAGAL, MASTER SUPPLIER DI WMS TIDAK DITEMUKAN';
                                $array []                   = $obj;
                            }
                        }else
                        {
                            $obj                        = new stdClass();
                            $obj->id                    = $id;
                            $obj->c_order_id            = null;
                            $obj->type_stock            = null;
                            $obj->type_stock_erp_code   = null;
                            $obj->booking_number        = $booking_number;
                            $obj->lc_date               = null;
                            $obj->promise_date          = null;
                            $obj->season                = null;
                            $obj->c_bpartner_id         = null;
                            $obj->supplier_name         = null;
                            $obj->document_no           = $document_no;
                            $obj->item_id               = null;
                            $obj->item_id_source        = null;
                            $obj->item_code_source      = $item_code_source;
                            $obj->item_code             = $item_code_book;
                            $obj->item_desc             = null;
                            $obj->category              = null;
                            $obj->po_buyer              = $po_buyer;
                            $obj->old_po_buyer          = $old_po_buyer;
                            $obj->uom                   = null;
                            $obj->article_no            = null;
                            $obj->qty_allocation        = $qty_input;
                            $obj->qty_outstanding       = null;
                            $obj->warehouse_name        = $warehouse_name;
                            $obj->warehouse             = null;
                            $obj->is_additional         = null;
                            $obj->remark_additional     = null;
                            $obj->error_upload          = true;
                            $obj->reason                = $reason;
                            $obj->order_by              = 0;
                            $obj->remark                = 'UPLOAD GAGAL, WAREHOUSE TIDAK DITEMUKAN';
                            $array []                   = $obj;
                        }
                    }
                }
            }

            
        }

        return response()->json($array,'200');
        /*die();
        usort($array, function($a, $b) {
            return strcmp($a->order_by,$b->order_by);
        });
        return response()->json($array,'200');*/
    }

    public function deleteBulk(Request $request)
    {
        $array          = array();
        $po_suppliers   = array();
        $items          = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    $auto_allocation_id = $value->auto_allocation_id;
                    $remark_update = $value->remark_update;
                    
                    if($auto_allocation_id)
                    {
                        $auto_allocation                        = AutoAllocation::find($auto_allocation_id);
                        $auto_allocation_document_no            = strtoupper($auto_allocation->document_no);
                        $auto_allocation_c_bpartner_id          = $auto_allocation->c_bpartner_id;
                        $auto_allocation_warehouse_id           = $auto_allocation->warehouse_id;
                        $auto_allocation_type_stock_erp_code    = $auto_allocation->type_stock_erp_code;
                        $auto_allocation_type_stock             = $auto_allocation->type_stock;
                        $auto_allocation_uom                    = $auto_allocation->uom;
                        $auto_allocation_lc_date                = $auto_allocation->lc_date;
                        $auto_allocation_item_code_source       = $auto_allocation->item_code_source;
                        $auto_allocation_item_code_book         = $auto_allocation->item_code_book;
                        $auto_allocation_po_buyer               = $auto_allocation->po_buyer;
                        $auto_allocation_qty_allocated          = $auto_allocation->qty_allocated;
                        $auto_allocation_is_integrate           = ($auto_allocation->is_upload_manual) ? true: false; 

                        $_supplier                              = PoSupplier::where('document_no',$auto_allocation_document_no)->first();
                        $auto_allocation_c_order_id             = ($_supplier)?$_supplier->c_order_id : 'FREE STOCK';

                        $item_source                            = Item::where(db::raw('item_code'),$auto_allocation_item_code_source)->first();
                        $item_book                              = Item::where(db::raw('item_code'),$auto_allocation_item_code_book)->first();
                        
                        $auto_allocation_item_source_id         = ($item_source) ? $item_source->item_id : null;
                        $auto_allocation_item_book_id           = ($item_book) ? $item_book->item_id : null;

                        $auto_allocation_supplier_name          = ($auto_allocation_c_bpartner_id == 'FREE STOCK ' && $auto_allocation_document_no == 'FREE STOCK')? 'FREE STOCK': $auto_allocation->supplier_name;
                        $auto_allocation_month_lc_date          = ($auto_allocation->lc_date)? $auto_allocation->lc_date->format('m') : '05';
                        $auto_allocation_qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                        $auto_allocation_operator               = -1*$auto_allocation_qty_allocation;
                        $auto_allocation_note                   = 'TERJADI PENGHAPUSAN ALOKASI';
                        $auto_allocation_note_code              = '2';
                        $auto_allocation_is_fabric              = $auto_allocation->is_fabric;
                        $auto_allocation_deleted_at             = $auto_allocation->deleted_at;
                        $is_additional                          = $auto_allocation->is_additional;
                        
                        try
                        {
                            db::beginTransaction();
                            if($auto_allocation_deleted_at == null)
                            {
                                if($auto_allocation_is_fabric)
                                {
                                    $allocation_items                       = AllocationItem::where('auto_allocation_id',$auto_allocation_id)->get();
                        
                                    foreach ($allocation_items as $key_2 => $allocation_item) 
                                    {
                                        //kembalikan qty material stock per lot
                                        $material_stock_per_lot = MaterialStockPerLot::find($allocation_item->material_stock_per_lot_id);
                                        if($material_stock_per_lot)
                                        {
                                            $qty_available_lot       = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                                            $qty_reserved_lot        = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                                            $qty_available_lot                     += $allocation_item->qty_booking;
                                            $qty_reserved_lot                      -= $allocation_item->qty_booking;
                                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                            $material_stock_per_lot->save();
                                        }
                                        // next buat update stock nya
                                        $allocation_item_id     = $allocation_item->id;
                                        DetailMaterialPlanningFabric::where('allocation_item_id',$allocation_item_id)->delete();

                                        $material_stock_id                  = $allocation_item->material_stock_id;
                                        $po_buyer                           = $allocation_item->po_buyer;
                                        $style                              = $allocation_item->style;
                                        $article_no                         = $allocation_item->article_no;
                                        $lc_date                            = $allocation_item->lc_date;
                                        $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_booking);

                                        if($material_stock_id)
                                        {
                                            $material_stock                     = MaterialStock::find($material_stock_id);
                                            $stock                              = sprintf('%0.8f',$material_stock->stock);
                                            $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
                                            $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                                            $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);
                                            $new_reserverd_qty                  = $reserved_qty - $qty_booking;
                                            $new_available_qty                  = $stock - $new_reserverd_qty;
                                            $curr_type_stock_erp_code           = $material_stock->type_stock_erp_code;
                                            $curr_type_stock                    = $material_stock->type_stock;
    
                                            $material_stock->reserved_qty       = sprintf('%0.8f',$new_reserverd_qty);
                                            $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty);
    
                                            if($new_available_qty > 0)
                                            {
                                                $material_stock->is_allocated   = false;
                                                $material_stock->is_active      = true;
                                            }
    
                                            $remark = 'CANCEL ALLOCATION, UNTUK PO BUYER '.$po_buyer.', STYLE '.$style.', ARTICLE '.$article_no.', DAN QTY '.$qty_booking;

                                            HistoryStock::approved($material_stock_id
                                            ,$_new_available_qty
                                            ,$old_available_qty
                                            ,(-1*$qty_booking)
                                            ,$po_buyer
                                            ,$style
                                            ,$article_no
                                            ,$lc_date
                                            ,$remark
                                            ,auth::user()->name
                                            ,auth::user()->id
                                            ,$curr_type_stock_erp_code
                                            ,$curr_type_stock
                                            ,false
                                            ,true );
    
                                            $material_stock->save();
                                        }

                                        $allocation_item->delete();
                                        if($is_additional == true)
                                        {
                                            $material_preparation_fabric = MaterialPreparationFabric::where('auto_allocation_id', $auto_allocation_id)
                                                                           ->first();
                                            if($material_preparation_fabric)
                                            {                               
                                                $material_preparation_fabric->total_reserved_qty    = 0;
                                                $material_preparation_fabric->total_qty_outstanding = 0;
                                                $material_preparation_fabric->remark_planning       = 'please cancel this data due allocation is not longer exists';
                                                $material_preparation_fabric->save();
                                            }
                                        }
                                    }

                                    HistoryAutoAllocations::Create([
                                        'auto_allocation_id'        => $auto_allocation_id,
                                        'document_no_old'           => $auto_allocation_document_no,
                                        'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                                        'item_code_old'             => $auto_allocation_item_code_book,
                                        'uom_old'                   => $auto_allocation_uom,
                                        'item_id_old'               => $auto_allocation_item_book_id,
                                        'item_source_id_old'        => $auto_allocation_item_source_id,
                                        'item_code_source_old'      => $auto_allocation_item_code_source,
                                        'po_buyer_old'              => $auto_allocation_po_buyer,
                                        'supplier_name_old'         => $auto_allocation_supplier_name,
                                        'qty_allocated_old'         => $auto_allocation_qty_allocation,
                                        'warehouse_id_old'          => $auto_allocation_warehouse_id,
                                        'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                                        'type_stock_old'            => $auto_allocation_type_stock,
                            
                                        'document_no_new'           => $auto_allocation_document_no,
                                        'item_code_new'             => $auto_allocation_item_code_book,
                                        'item_id_new'               => $auto_allocation_item_book_id,
                                        'item_source_id_new'        => $auto_allocation_item_source_id,
                                        'item_code_source_new'      => $auto_allocation_item_code_source,
                                        'uom_new'                   => $auto_allocation_uom,
                                        'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                                        'supplier_name_new'         => $auto_allocation_supplier_name,
                                        'qty_allocated_new'         => $auto_allocation_qty_allocation,
                                        'warehouse_id_new'          => $auto_allocation_warehouse_id,
                                        'po_buyer_new'              => $auto_allocation_po_buyer,
                                        'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                                        'type_stock_new'            => $auto_allocation_type_stock,
                            
                                        'note_code'                 => $auto_allocation_note_code,
                                        'note'                      => $auto_allocation_note,
                                        'operator'                  => $auto_allocation_operator,
                                        'remark'                    => 'BULK DELETE',
                                        'is_integrate'              => $auto_allocation_is_integrate,
                                        'lc_date'                   => $auto_allocation_lc_date,
                                        'c_order_id'                => $auto_allocation_c_order_id,
                                        'user_id'                   => auth::user()->id
                                    ]);

                                    $auto_allocation->update_user_id                    = auth::user()->id;
                                    $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                                    $auto_allocation->qty_allocated                     = 0;
                                    $auto_allocation->is_already_generate_form_booking  = false;
                                    $auto_allocation->generate_form_booking             = null;
                                    $auto_allocation->remark                            = 'DELETE BULK';
                                    $auto_allocation->deleted_at                        = carbon::now();
                                    $auto_allocation->save();

                                    $obj                    = new stdClass();
                                    $obj->document_no       = $auto_allocation->document_no;
                                    $obj->item_code         = $auto_allocation->item_code_book;
                                    $obj->po_buyer          = $auto_allocation->po_buyer;
                                    $obj->uom               = $auto_allocation->uom;
                                    $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                    $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                    $obj->error_upload      = false;
                                    $obj->remark            = 'SUCCESS, DATA BERHASIL DI HAPUS';
                                    $po_suppliers []        = $auto_allocation->document_no;
                                    $items []               = $auto_allocation->item_code_book;
                                    $array []               = $obj;
                                }else
                                {
                                    if($auto_allocation->qty_allocated > 0)
                                    {
                                        if(auth::user()->hasRole(['admin-ict-acc']))
                                        {
                                            $allocation_items   = MaterialPreparation::where('auto_allocation_id',$auto_allocation_id)->get();
                        
                                            foreach ($allocation_items as $key_2 => $allocation_item) 
                                            {
                                                    $material_stock_id                  = $allocation_item->material_stock_id;
                                                    $po_buyer                           = $allocation_item->po_buyer;
                                                    $style                              = $allocation_item->style;
                                                    $article_no                         = $allocation_item->article_no;
                                                    $lc_date                            = $allocation_item->lc_date;
                                                    $material_preparation_id            = $allocation_item->id;
                                                    $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_conversion);

                                                    Temporary::Create([
                                                        'barcode'   => $po_buyer,
                                                        'status'    => 'mrp',
                                                        'user_id'   => Auth::user()->id
                                                    ]);
                                                    
                                                    $material_stock                     = MaterialStock::find($material_stock_id);
                                                    $stock                              = sprintf('%0.8f',$material_stock->stock);
                                                    $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
                                                    $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                                                    $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);
                                                    $new_reserverd_qty                  = $reserved_qty - $qty_booking;
                                                    $new_available_qty                  = $stock - $new_reserverd_qty;
                                                    $curr_type_stock_erp_code           = $material_stock->type_stock_erp_code;
                                                    $curr_type_stock                    = $material_stock->type_stock;
                                                    $is_running_stock                   = $allocation_item->materialStock->is_running_stock;
                                                    $is_integrate                       = ( $is_running_stock ? true : false);

                                                    $material_stock->reserved_qty       = sprintf('%0.8f',$new_reserverd_qty);
                                                    $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty);

                                                    if($new_available_qty > 0)
                                                    {
                                                        $material_stock->is_allocated   = false;
                                                        $material_stock->is_active      = true;
                                                    }
                                                    
                                                    $remark = 'CANCEL ALLOCATION, UNTUK PO BUYER '.$po_buyer.', STYLE '.$style.', ARTICLE '.$article_no.', DAN QTY '.$qty_booking;
                                                    HistoryStock::approved($material_stock_id
                                                    ,$_new_available_qty
                                                    ,$old_available_qty
                                                    ,(-1*$qty_booking)
                                                    ,$po_buyer
                                                    ,$style
                                                    ,$article_no
                                                    ,$lc_date
                                                    ,$remark
                                                    ,auth::user()->name
                                                    ,auth::user()->id
                                                    ,$curr_type_stock_erp_code
                                                    ,$curr_type_stock
                                                    ,$is_integrate
                                                    ,true );

                                                    $material_stock->save();
                                                    $allocation_item->delete();
                                            }

                                            HistoryAutoAllocations::Create([
                                                'auto_allocation_id'        => $auto_allocation_id,
                                                'document_no_old'           => $auto_allocation_document_no,
                                                'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                                                'item_code_old'             => $auto_allocation_item_code_book,
                                                'uom_old'                   => $auto_allocation_uom,
                                                'item_id_old'               => $auto_allocation_item_book_id,
                                                'item_source_id_old'        => $auto_allocation_item_source_id,
                                                'item_code_source_old'      => $auto_allocation_item_code_source,
                                                'po_buyer_old'              => $auto_allocation_po_buyer,
                                                'supplier_name_old'         => $auto_allocation_supplier_name,
                                                'qty_allocated_old'         => $auto_allocation_qty_allocation,
                                                'warehouse_id_old'          => $auto_allocation_warehouse_id,
                                                'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                                                'type_stock_old'            => $auto_allocation_type_stock,
                                    
                                                'document_no_new'           => $auto_allocation_document_no,
                                                'item_code_new'             => $auto_allocation_item_code_book,
                                                'item_id_new'               => $auto_allocation_item_book_id,
                                                'item_source_id_new'        => $auto_allocation_item_source_id,
                                                'item_code_source_new'      => $auto_allocation_item_code_source,
                                                'uom_new'                   => $auto_allocation_uom,
                                                'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                                                'supplier_name_new'         => $auto_allocation_supplier_name,
                                                'qty_allocated_new'         => $auto_allocation_qty_allocation,
                                                'warehouse_id_new'          => $auto_allocation_warehouse_id,
                                                'po_buyer_new'              => $auto_allocation_po_buyer,
                                                'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                                                'type_stock_new'            => $auto_allocation_type_stock,
                                    
                                                'note_code'                 => $auto_allocation_note_code,
                                                'note'                      => $auto_allocation_note,
                                                'operator'                  => $auto_allocation_operator,
                                                'remark'                    => 'BULK DELETE',
                                                'is_integrate'              => $auto_allocation_is_integrate,
                                                'lc_date'                   => $auto_allocation_lc_date,
                                                'c_order_id'                => $auto_allocation_c_order_id,
                                                'user_id'                   => auth::user()->id
                                            ]);

                                            $auto_allocation->update_user_id                    = auth::user()->id;
                                            $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                                            $auto_allocation->qty_allocated                     = 0;
                                            $auto_allocation->is_already_generate_form_booking  = false;
                                            $auto_allocation->generate_form_booking             = null;
                                            $auto_allocation->remark                            = 'DELETE BULK';
                                            $auto_allocation->remark_update                     = $remark_update;
                                            $auto_allocation->deleted_at                        = carbon::now();
                                            $auto_allocation->save();

                                            $obj                    = new stdClass();
                                            $obj->document_no       = $auto_allocation->document_no;
                                            $obj->item_code         = $auto_allocation->item_code_book;
                                            $obj->po_buyer          = $auto_allocation->po_buyer;
                                            $obj->uom               = $auto_allocation->uom;
                                            $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                            $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                            $obj->error_upload      = false;
                                            $obj->remark            = 'SUCCESS, DATA BERHASIL DI HAPUS';
                                            $array [] = $obj;
                                        }
                                        else if (auth::user()->hasRole(['mm-staff-acc']))
                                        {
                                            $allocation_items   = MaterialPreparation::where('auto_allocation_id',$auto_allocation_id)
                                            ->where('barcode', 'BELUM DI PRINT')->get();
                        
                                            if(count($allocation_items) > 0)
                                            {
                                                foreach ($allocation_items as $key_2 => $allocation_item) 
                                                {
                                                        $material_stock_id                  = $allocation_item->material_stock_id;
                                                        $po_buyer                           = $allocation_item->po_buyer;
                                                        $style                              = $allocation_item->style;
                                                        $article_no                         = $allocation_item->article_no;
                                                        $lc_date                            = $allocation_item->lc_date;
                                                        $material_preparation_id            = $allocation_item->id;
                                                        $qty_booking                        = sprintf('%0.8f',$allocation_item->qty_conversion);

                                                        Temporary::Create([
                                                            'barcode'   => $po_buyer,
                                                            'status'    => 'mrp',
                                                            'user_id'   => Auth::user()->id
                                                        ]);
                                                        
                                                        $material_stock                     = MaterialStock::find($material_stock_id);
                                                        $stock                              = sprintf('%0.8f',$material_stock->stock);
                                                        $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
                                                        $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                                                        $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);
                                                        $new_reserverd_qty                  = $reserved_qty - $qty_booking;
                                                        $new_available_qty                  = $stock - $new_reserverd_qty;
                                                        $curr_type_stock_erp_code           = $material_stock->type_stock_erp_code;
                                                        $curr_type_stock                    = $material_stock->type_stock;
                                                        $is_running_stock                   = $allocation_item->materialStock->is_running_stock;
                                                        $is_integrate                       = ( $is_running_stock ? true : false);

                                                        $material_stock->reserved_qty       = sprintf('%0.8f',$new_reserverd_qty);
                                                        $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty);

                                                        if($new_available_qty > 0)
                                                        {
                                                            $material_stock->is_allocated   = false;
                                                            $material_stock->is_active      = true;
                                                        }
                                                        
                                                        $remark = 'CANCEL ALLOCATION, UNTUK PO BUYER '.$po_buyer.', STYLE '.$style.', ARTICLE '.$article_no.', DAN QTY '.$qty_booking;
                                                        HistoryStock::approved($material_stock_id
                                                        ,$_new_available_qty
                                                        ,$old_available_qty
                                                        ,(-1*$qty_booking)
                                                        ,$po_buyer
                                                        ,$style
                                                        ,$article_no
                                                        ,$lc_date
                                                        ,$remark
                                                        ,auth::user()->name
                                                        ,auth::user()->id
                                                        ,$curr_type_stock_erp_code
                                                        ,$curr_type_stock
                                                        ,$is_integrate
                                                        ,true );

                                                        $material_stock->save();
                                                        $allocation_item->delete();
                                                }

                                                HistoryAutoAllocations::Create([
                                                    'auto_allocation_id'        => $auto_allocation_id,
                                                    'document_no_old'           => $auto_allocation_document_no,
                                                    'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                                                    'item_code_old'             => $auto_allocation_item_code_book,
                                                    'uom_old'                   => $auto_allocation_uom,
                                                    'item_id_old'               => $auto_allocation_item_book_id,
                                                    'item_source_id_old'        => $auto_allocation_item_source_id,
                                                    'item_code_source_old'      => $auto_allocation_item_code_source,
                                                    'po_buyer_old'              => $auto_allocation_po_buyer,
                                                    'supplier_name_old'         => $auto_allocation_supplier_name,
                                                    'qty_allocated_old'         => $auto_allocation_qty_allocation,
                                                    'warehouse_id_old'          => $auto_allocation_warehouse_id,
                                                    'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                                                    'type_stock_old'            => $auto_allocation_type_stock,
                                        
                                                    'document_no_new'           => $auto_allocation_document_no,
                                                    'item_code_new'             => $auto_allocation_item_code_book,
                                                    'item_id_new'               => $auto_allocation_item_book_id,
                                                    'item_source_id_new'        => $auto_allocation_item_source_id,
                                                    'item_code_source_new'      => $auto_allocation_item_code_source,
                                                    'uom_new'                   => $auto_allocation_uom,
                                                    'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                                                    'supplier_name_new'         => $auto_allocation_supplier_name,
                                                    'qty_allocated_new'         => $auto_allocation_qty_allocation,
                                                    'warehouse_id_new'          => $auto_allocation_warehouse_id,
                                                    'po_buyer_new'              => $auto_allocation_po_buyer,
                                                    'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                                                    'type_stock_new'            => $auto_allocation_type_stock,
                                                    'note_code'                 => $auto_allocation_note_code,
                                                    'note'                      => $auto_allocation_note,
                                                    'operator'                  => $auto_allocation_operator,
                                                    'remark'                    => 'BULK DELETE',
                                                    'is_integrate'              => $auto_allocation_is_integrate,
                                                    'lc_date'                   => $auto_allocation_lc_date,
                                                    'c_order_id'                => $auto_allocation_c_order_id,
                                                    'user_id'                   => auth::user()->id
                                                ]);

                                                $auto_allocation->update_user_id                    = auth::user()->id;
                                                $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                                                $auto_allocation->qty_allocated                     = 0;
                                                $auto_allocation->is_already_generate_form_booking  = false;
                                                $auto_allocation->generate_form_booking             = null;
                                                $auto_allocation->remark                            = 'DELETE BULK';
                                                $auto_allocation->remark_update                     = $remark_update;
                                                $auto_allocation->deleted_at                        = carbon::now();
                                                $auto_allocation->save();

                                                $obj                    = new stdClass();
                                                $obj->document_no       = $auto_allocation->document_no;
                                                $obj->item_code         = $auto_allocation->item_code_book;
                                                $obj->po_buyer          = $auto_allocation->po_buyer;
                                                $obj->uom               = $auto_allocation->uom;
                                                $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                                $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                                $obj->error_upload      = false;
                                                $obj->remark            = 'SUCCESS, DATA BERHASIL DI HAPUS';
                                                $array [] = $obj;

                                            }
                                            //data yang sudah diprint butuh approval
                                            else
                                            {
                                                $auto_allocation->is_request_delete = true;
                                                $auto_allocation->remark            = 'REQUEST DELETE';
                                                $auto_allocation->update_user_id    = auth::user()->id;
                                                $auto_allocation->remark_update     = 'REQUEST DELETE ALOKASI OLEH '.auth::user()->name.' '.carbon::now(). ' KARENA '.$remark_update;
                                                $auto_allocation->save();

                                                HistoryAutoAllocations::Create([
                                                    'auto_allocation_id'        => $auto_allocation_id,
                                                    'document_no_old'           => $auto_allocation_document_no,
                                                    'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                                                    'item_code_old'             => $auto_allocation_item_code_book,
                                                    'uom_old'                   => $auto_allocation_uom,
                                                    'item_id_old'               => $auto_allocation_item_book_id,
                                                    'item_source_id_old'        => $auto_allocation_item_source_id,
                                                    'item_code_source_old'      => $auto_allocation_item_code_source,
                                                    'po_buyer_old'              => $auto_allocation_po_buyer,
                                                    'supplier_name_old'         => $auto_allocation_supplier_name,
                                                    'qty_allocated_old'         => $auto_allocation_qty_allocation,
                                                    'warehouse_id_old'          => $auto_allocation_warehouse_id,
                                                    'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                                                    'type_stock_old'            => $auto_allocation_type_stock,
                                        
                                                    'document_no_new'           => $auto_allocation_document_no,
                                                    'item_code_new'             => $auto_allocation_item_code_book,
                                                    'item_id_new'               => $auto_allocation_item_book_id,
                                                    'item_source_id_new'        => $auto_allocation_item_source_id,
                                                    'item_code_source_new'      => $auto_allocation_item_code_source,
                                                    'uom_new'                   => $auto_allocation_uom,
                                                    'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                                                    'supplier_name_new'         => $auto_allocation_supplier_name,
                                                    'qty_allocated_new'         => $auto_allocation->qty_allocated,
                                                    'warehouse_id_new'          => $auto_allocation_warehouse_id,
                                                    'po_buyer_new'              => $auto_allocation_po_buyer,
                                                    'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                                                    'type_stock_new'            => $auto_allocation_type_stock,
                                        
                                                    'note_code'                 => $auto_allocation_note_code,
                                                    'note'                      => $auto_allocation_note,
                                                    'operator'                  => $auto_allocation_operator,
                                                    'remark'                    => 'REQUEST DELETE ALOKASI KARENA '. $remark_update,
                                                    'is_integrate'              => $auto_allocation_is_integrate,
                                                    'lc_date'                   => $auto_allocation_lc_date,
                                                    'c_order_id'                => $auto_allocation_c_order_id,
                                                    'user_id'                   => auth::user()->id
                                                ]);

                                                $obj                    = new stdClass();
                                                $obj->document_no       = $auto_allocation->document_no;
                                                $obj->item_code         = $auto_allocation->item_code_book;
                                                $obj->po_buyer          = $auto_allocation->po_buyer;
                                                $obj->uom               = $auto_allocation->uom;
                                                $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                                $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                                $obj->error_upload      = false;
                                                $obj->remark            = 'SUCCESS, DATA MASUK LIST APPROVAL';
                                                $array [] = $obj;

                                            }

                                        }
                                        else
                                        {
                                            $obj                    = new stdClass();
                                            $obj->document_no       = $auto_allocation->document_no;
                                            $obj->item_code         = $auto_allocation->item_code_book;
                                            $obj->po_buyer          = $auto_allocation->po_buyer;
                                            $obj->uom               = $auto_allocation->uom;
                                            $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                            $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                            $obj->error_upload      = true;
                                            $obj->remark            = 'ERROR, SUDAH ADA YANG TERALOKASI, SILAHKAN HUBUNGI ICT UNTUK DIHAPUS DATANYA';
                                            $array [] = $obj;
                                        }
                                        
                                    }else
                                    {
                                        HistoryAutoAllocations::Create([
                                            'auto_allocation_id'        => $auto_allocation_id,
                                            'document_no_old'           => $auto_allocation_document_no,
                                            'c_bpartner_id_old'         => $auto_allocation_c_bpartner_id,
                                            'item_code_old'             => $auto_allocation_item_code_book,
                                            'uom_old'                   => $auto_allocation_uom,
                                            'item_id_old'               => $auto_allocation_item_book_id,
                                            'item_source_id_old'        => $auto_allocation_item_source_id,
                                            'item_code_source_old'      => $auto_allocation_item_code_source,
                                            'po_buyer_old'              => $auto_allocation_po_buyer,
                                            'supplier_name_old'         => $auto_allocation_supplier_name,
                                            'qty_allocated_old'         => $auto_allocation_qty_allocation,
                                            'warehouse_id_old'          => $auto_allocation_warehouse_id,
                                            'type_stock_erp_code_old'   => $auto_allocation_type_stock_erp_code,
                                            'type_stock_old'            => $auto_allocation_type_stock,
                                
                                            'document_no_new'           => $auto_allocation_document_no,
                                            'item_code_new'             => $auto_allocation_item_code_book,
                                            'item_id_new'               => $auto_allocation_item_book_id,
                                            'item_source_id_new'        => $auto_allocation_item_source_id,
                                            'item_code_source_new'      => $auto_allocation_item_code_source,
                                            'uom_new'                   => $auto_allocation_uom,
                                            'c_bpartner_id_new'         => $auto_allocation_c_bpartner_id,
                                            'supplier_name_new'         => $auto_allocation_supplier_name,
                                            'qty_allocated_new'         => $auto_allocation_qty_allocation,
                                            'warehouse_id_new'          => $auto_allocation_warehouse_id,
                                            'po_buyer_new'              => $auto_allocation_po_buyer,
                                            'type_stock_erp_code_new'   => $auto_allocation_type_stock_erp_code,
                                            'type_stock_new'            => $auto_allocation_type_stock,
                                
                                            'note_code'                 => $auto_allocation_note_code,
                                            'note'                      => $auto_allocation_note,
                                            'operator'                  => $auto_allocation_operator,
                                            'remark'                    => 'BULK DELETE',
                                            'is_integrate'              => $auto_allocation_is_integrate,
                                            'lc_date'                   => $auto_allocation_lc_date,
                                            'c_order_id'                => $auto_allocation_c_order_id,
                                            'user_id'                   => auth::user()->id
                                        ]);

                                        $auto_allocation->update_user_id                    = auth::user()->id;
                                        $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                                        $auto_allocation->qty_allocated                     = 0;
                                        $auto_allocation->is_already_generate_form_booking  = false;
                                        $auto_allocation->generate_form_booking             = null;
                                        $auto_allocation->remark                            = 'DELETE BULK';
                                        $auto_allocation->deleted_at                        = carbon::now();
                                        $auto_allocation->save();

                                        $obj                    = new stdClass();
                                        $obj->document_no       = $auto_allocation->document_no;
                                        $obj->item_code         = $auto_allocation->item_code_book;
                                        $obj->po_buyer          = $auto_allocation->po_buyer;
                                        $obj->uom               = $auto_allocation->uom;
                                        $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                        $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                        $obj->error_upload      = false;
                                        $obj->remark            = 'SUCCESS, DATA BERHASIL DI HAPUS';
                                        $array [] = $obj;
                                    }
                                    
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->document_no       = $auto_allocation->document_no;
                                $obj->item_code         = $auto_allocation->item_code_book;
                                $obj->po_buyer          = $auto_allocation->po_buyer;
                                $obj->uom               = $auto_allocation->uom;
                                $obj->qty_allocation    = $auto_allocation->qty_allocation;
                                $obj->warehouse_name    = $auto_allocation->warehouse_name;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, DATA SUDAH TIDAK AKTIF';
                                $array [] = $obj;
                            }
                            db::commit();
                        }catch (Exception $ex){
                            db::rollback();
                            $message = $ex->getMessage();
                            ErrorHandler::db($message);
                        }
                    }else
                    {
                        $obj                    = new stdClass();
                        $obj->document_no       = null;
                        $obj->item_code         = null;
                        $obj->po_buyer          = null;
                        $obj->uom               = null;
                        $obj->qty_allocation    = null;
                        $obj->warehouse_name    = null;
                        $obj->error_upload      = true;
                        $obj->remark            = 'ERROR, ID '.$auto_allocation_id.' TIDAK TIDAK DITEMUKAN';
                        $array [] = $obj;
                    }
                    
                }
                
            }
        }

        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                ,'c_order_id'
                ,'article_no'
                ,'warehouse_id'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'uom'
                ,'planning_date'
                ,'is_piping'
                ,db::raw("sum(qty_booking) as total_booking"))
            ->whereIn('document_no',$po_suppliers)
            ->whereIn('item_code',$items)
            ->groupby('document_no'
                ,'warehouse_id'
                ,'uom'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'c_order_id'
                ,'article_no'
                ,'planning_date'
                ,'is_piping')
            ->get();

            $movement_date  = carbon::now()->toDateTimeString();
            
            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                $_article_no_preparation        = $detail_material_planning->article_no;
                $_document_no_preparation       = $detail_material_planning->document_no;
                $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                $_item_code                     = $detail_material_planning->item_code;
                $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                $_item_id_book_preparation      = $detail_material_planning->item_id_book;
                $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                $_style_preparation             = $detail_material_planning->_style;
                $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                $_is_piping_preparation         = $detail_material_planning->is_piping;
                $_uom                           = $detail_material_planning->uom;
                $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$_c_order_id_preparation],
                    ['item_id_book',$_item_id_book_preparation],
                    ['item_id_source',$_item_id_source_preparation],
                    ['_style',$_style_preparation],
                    ['article_no',$_article_no_preparation],
                    ['document_no',$_document_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_code',$_item_code],
                    ['item_code_source',$_item_code_source_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                    $curr_remark_planning   = $is_material_preparation_exists->remark_planning;

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                       
                        if($new_qty_oustanding < 0) $remark_planning = 'pengurangan qty alokasi';
                        else $remark_planning = 'penambahan qty alokasi';

                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning,
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                        $is_material_preparation_exists->remark_planning        = $remark_planning;
                        $is_material_preparation_exists->save();
                    }
                }
            }
            

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        return response()->json($array,'200');
    }

    static function doCancellation($auto_allocation_id,$cancel_reason,$user_id)
    {
        $auto_allocation        = AutoAllocation::find($auto_allocation_id);

        if($auto_allocation->is_upload_manual)
        {
            $is_integrate       = false;
        }else
        {
            if($auto_allocation->is_allocation_purchase)  $is_integrate = true;
            else $is_integrate = false;
        }
        
        if($auto_allocation->is_fabric)
        {
            $allocation_item_fabrics = AllocationItem::whereNotNull('auto_allocation_id')
            ->where('auto_allocation_id',$auto_allocation_id)
            ->get();

            try 
            {
                DB::beginTransaction();

                foreach ($allocation_item_fabrics as $key => $allocation_item_fabric) 
                {
                    //kembalikan qty material stock per lot
                    $material_stock_per_lot = MaterialStockPerLot::find($allocation_item_fabric->material_stock_per_lot_id);
                    if($material_stock_per_lot)
                    {
                        $qty_available_lot       = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                        $qty_reserved_lot        = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                        $qty_available_lot += $allocation_item_fabric->qty_booking;
                        $qty_reserved_lot  -= $allocation_item_fabric->qty_booking;
                        $material_stock_per_lot->qty_available  = $qty_available_lot;
                        $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                        $material_stock_per_lot->save();
                    }

                    DetailMaterialPlanningFabric::where('allocation_item_id',$allocation_item_fabric->id)->delete();
                    $allocation_item_fabric->delete();
                }
                
                $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                    ,'c_order_id'
                    ,'warehouse_id'
                    ,'_style'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'uom'
                    ,'planning_date'
                    ,'is_piping'
                    ,db::raw("sum(qty_booking) as total_booking"))
                ->where('document_no',$auto_allocation->document_no)
                ->where('item_code',$auto_allocation->item_code)
                ->groupby('document_no'
                    ,'warehouse_id'
                    ,'uom'
                    ,'item_code'
                    ,'c_order_id'
                    ,'item_code_source'
                    ,'item_id_book'
                    ,'item_id_source'
                    ,'article_no'
                    ,'_style'
                    ,'planning_date'
                    ,'is_piping')
                ->get();
    
                $movement_date  = carbon::now()->toDateTimeString();
                
                foreach ($detail_material_plannings as $key => $detail_material_planning) 
                {
                    
                    $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                    $_article_no_preparation        = $detail_material_planning->article_no;
                    $_document_no_preparation       = $detail_material_planning->document_no;
                    $_style_preparation             = $detail_material_planning->_style;
                    $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                    $_item_code                     = $detail_material_planning->item_code;
                    $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                    $_item_id_book                  = $detail_material_planning->item_id_book;
                    $_item_id_source                = $detail_material_planning->item_id_source;
                    $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                    $_is_piping_preparation         = $detail_material_planning->is_piping;
                    $_uom                           = $detail_material_planning->uom;
                    $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);
    
                    $is_material_preparation_exists = MaterialPreparationFabric::where([
                        ['item_id_book',$_item_id_book],
                        ['item_id_source',$_item_id_source],
                        ['c_order_id',$_c_order_id_preparation],
                        ['article_no',$_article_no_preparation],
                        ['warehouse_id',$_warehouse_id_preparation],
                        ['_style',$_style_preparation],
                        ['planning_date',$_planning_date_preparation],
                        ['is_piping',$_is_piping_preparation],
                    ])
                    ->first();
    
                    if($is_material_preparation_exists)
                    {
                        $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                        $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                        $curr_remark_planning   = $is_material_preparation_exists->remark_planning;
    
                        if($_total_booking_preparation != $total_reserved_qty)
                        {
                            $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                        
                            if($total_reserved_qty < $_total_booking_preparation) $remark_planning = 'pengurangan qty alokasi';
                            else $remark_planning = 'penambahan qty alokasi';
    
                            HistoryPreparationFabric::create([
                                'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                                'remark'                            => $remark_planning,
                                'qty_before'                        => $total_reserved_qty,
                                'qty_after'                         => $_total_booking_preparation,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                            ]);
    
                            $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                            $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                            $is_material_preparation_exists->remark_planning        = $remark_planning;
                            $is_material_preparation_exists->save();
                        }
                    }
                }
                
    
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }else
        {
            $material_preparations = MaterialPreparation::whereNotNull('auto_allocation_id')
            ->where('auto_allocation_id',$auto_allocation_id)
            ->whereNull('deleted_at')
            ->get();

            $movement_date  = carbon::now()->toDateTimeString();

            try 
            {
                DB::beginTransaction();

                if(Auth::check())
                {
                    $user_id = Auth::user()->id;
                    $user_name = auth::user()->name;
                }
                else
                {
                    $user_id = '91';
                    $user_name = 'system';
                }
                foreach ($material_preparations as $key => $material_preparation) 
                {
                    if(!$material_preparation->deleted_at)
                    {
                        $po_buyer   = $material_preparation->po_buyer;
                        $style      = $material_preparation->style;
                        $article_no = $material_preparation->article_no;
                        $lc_date    = $material_preparation->lc_date;
    
                        Temporary::Create([
                            'barcode'       => $po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => $user_id,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);
    
                        $material_stock             = MaterialStock::find($material_preparation->material_stock_id);
                        $material_preparation_id    = $material_preparation->id;
                        $stock                      = $material_stock->stock;
                        $reserved_qty               = $material_stock->reserved_qty;
                        $old_available_qty          = $material_stock->available_qty;
                        $_new_available_qty         = $old_available_qty + $material_preparation->qty_conversion;
                        $new_reserverd_qty          = $reserved_qty - $material_preparation->qty_conversion;
                        $new_available_qty          = $stock - $new_reserverd_qty;
                        $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
                        $curr_type_stock            = $material_stock->type_stock;

                        $material_stock->reserved_qty   = $new_reserverd_qty;
                        $material_stock->available_qty  = $new_available_qty;

                    if($new_available_qty > 0)
                    {
                        $material_stock->is_allocated = false;
                        $material_stock->is_active = true;
                    }

                    $remark = 'CANCEL ALLOCATION, UNTUK PO BUYER '.$po_buyer.', STYLE '.$style.', ARTICLE '.$article_no.', DAN QTY '.$material_preparation->qty_conversion;
                    
                    HistoryStock::approved($material_preparation->material_stock_id
                        ,$_new_available_qty
                        ,$old_available_qty
                        ,(-1*$material_preparation->qty_conversion)
                        ,$po_buyer
                        ,$style
                        ,$article_no
                        ,$lc_date
                        ,$remark
                        ,$user_name
                        ,$user_id
                        ,$curr_type_stock_erp_code
                        ,$curr_type_stock
                        ,$is_integrate
                        ,true
                    );
                    
                    $material_stock->save();
                    //tambahin di line status cancel allocation
                    //note = alokasi ini di hapus karena .... (barcode yg di hapus)
                    $material_preparation->delete();
    
                    }
                }
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

        $item = Item::where('item_code',$auto_allocation->item_code_source)->first();

        HistoryAutoAllocations::FirstOrCreate([
            'auto_allocation_id'        => $auto_allocation_id,
            'document_no_old'           => $auto_allocation->document_no,
            'c_bpartner_id_old'         => $auto_allocation->c_bpartner_id,
            'supplier_name_old'         => $auto_allocation->supplier_name,
            'qty_allocated_old'         => $auto_allocation->qty_allocation,
            'po_buyer_old'              => $auto_allocation->po_buyer,
            'item_id_old'               => $item->item_id,
            'item_code_old'             => $auto_allocation->item_code,
            'warehouse_id_old'          => $auto_allocation->warehouse_id,

            'document_no_new'           => $auto_allocation->document_no,
            'c_bpartner_id_new'         => $auto_allocation->c_bpartner_id,
            'supplier_name_new'         => $auto_allocation->supplier_name,
            'qty_allocated_new'         => $auto_allocation->qty_allocation,
            'po_buyer_new'              => $auto_allocation->po_buyer,
            'item_id_new'               => $item->item_id,
            'item_code_new'             => $auto_allocation->item_code,
            'warehouse_id_new'          => $auto_allocation->warehouse_id,
            
            'remark'                    => $cancel_reason.' (DULU SUDAH DI ALOKASI SEBESAR '.$auto_allocation->qty_allocated.')',
            'is_integrate'              => true,
            'user_id'                   => $user_id
        ]);

        $auto_allocation->update_user_id                    = $user_id;
        $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
        $auto_allocation->qty_allocated                     = 0;
        $auto_allocation->is_already_generate_form_booking  = false;
        $auto_allocation->generate_form_booking             = null;
        $auto_allocation->qty_in_house                     = null;
        $auto_allocation->qty_on_ship                      = null;
        $auto_allocation->eta_actual                       = null;
        $auto_allocation->etd_actual                       = null;
        $auto_allocation->eta_date                         = null;
        $auto_allocation->etd_date                         = null;
        $auto_allocation->mrd                              = null;
        $auto_allocation->dd_pi                            = null;
        $auto_allocation->qty_mrd                          = null;
        $auto_allocation->receive_date                     = null;
        $auto_allocation->no_invoice                       = null;
        $auto_allocation->save();
    }
    
    static function updateAllocationFabricItems($update_auto_allocations,$spesific_id = null)
    {
        //return view('errors.503');
        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['status_po_buyer','active']
        ])
        ->whereIn('id',$update_auto_allocations);

        if($spesific_id) $auto_allocations = $auto_allocations->where('id',$spesific_id);

        $auto_allocations = $auto_allocations->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            $buyers         = array();
            $movement_date  = carbon::now()->todatetimestring();

            foreach ($auto_allocations as $key_2 => $auto_allocation) 
            {
                if($auto_allocation->is_upload_manual)
                {
                    $allocation_source = 'ALLOCATION MANUAL';
                    Temporary::create([
                        'barcode'       => $auto_allocation->id,
                        'status'        => 'insert_stock_fabric',
                        'user_id'       => '1',
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);
                }else
                {
                    if($auto_allocation->is_allocation_purchase) $allocation_source = 'ALLOCATION PURCHASE';
                    else $allocation_source = 'AUTO ALLOCATION';
                }

                $user_id                = $auto_allocation->user_id;
                $auto_allocation_id     = $auto_allocation->id;
                $c_order_id             = $auto_allocation->c_order_id;
                $document_no            = $auto_allocation->document_no;
                $c_bpartner_id          = $auto_allocation->c_bpartner_id;
                $new_po_buyer           = $auto_allocation->po_buyer;
                $old_po_buyer           = $auto_allocation->old_po_buyer;
                $item_code              = $auto_allocation->item_code;
                $item_id_book           = $auto_allocation->item_id_book;
                $item_id_source         = $auto_allocation->item_id_source;
                $item_code_source       = $auto_allocation->item_code_source;
                $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                $warehouse_id           = $auto_allocation->warehouse_id;
                $remark_update          = $auto_allocation->remark_update;
                $is_additional          = $auto_allocation->is_additional;
                $supplier               = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
                
                $_po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();

                // if($is_additional == false)
                // {

                    $material_requirements = MaterialRequirement::where([
                        ['po_buyer',$new_po_buyer],
                        [db::raw('upper(item_id)'),$item_id_book],
                    ])
                    ->get();

                    // $total_need = MaterialRequirement::where([
                    //     ['po_buyer',$new_po_buyer],
                    //     [db::raw('upper(item_id)'),$item_id_book],
                    // ])
                    // ->sum('qty_required');

                    

                    //echo $qty_outstanding.'<br/>';
                    if($qty_outstanding > 0.0000)
                    {
                        $count_mr = count($material_requirements);
                        //AllocationItem::where('auto_allocation_id',$auto_allocation_id)->delete();
                        //dd($material_requirements);
                        if($count_mr > 0)
                        {
                            foreach ($material_requirements as $key => $material_requirement) 
                            {
                                $lc_date        = $material_requirement->lc_date;
                                $item_desc      = strtoupper($material_requirement->item_desc);
                                $uom            = $material_requirement->uom;
                                $category       = $material_requirement->category;
                                $style          = $material_requirement->style;
                                $job_order      = $material_requirement->job_order;
                                $_style         = explode('::',$job_order)[0];
                                $article_no     = $material_requirement->article_no;
                                
                                //$month_lc       = $material_requirement->lc_date->format('m');
                                if($is_additional)
                                {
                                    if(($key+1) != $count_mr) $qty_required = sprintf('%0.8f',1);
                                    else $qty_required = sprintf('%0.8f',$qty_outstanding);
                                }else
                                {
                                    $qty_bom             = $material_requirement->qty_required;
                                    $qty_allocation_item = $material_requirement->qtyAllocatedFabric($new_po_buyer,$item_code,$style,$article_no);
                                    // if($qty_bom - $qty_allocation + $qty_allocation_item > 0)
                                    // {
                                    //     $qty_required    = $qty_allocation - $qty_allocation_item;
                                    // }
                                    // else
                                    // {
                                        $qty_required    = $qty_bom - $qty_allocation_item;
                                    //}
                                }
                                
                                $__supplied = sprintf('%0.8f',$qty_required);

                                $_warehouse_erp_id = $material_requirement->warehouse_id;

                                if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                                else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                                else $warehouse_production_id = $warehouse_id;
                                
                                if($qty_required > 0.0000 && $qty_outstanding > 0.0000)
                                {
                                    if ($qty_outstanding/$__supplied >= 1) $qty_required = $__supplied;
                                    else $qty_required = $qty_outstanding;

                                    $__supply = sprintf('%0.8f',$qty_required);
                                
                                    if($is_additional)
                                    {
                                        $is_exists = false;
                                    }else
                                    {
                                        $is_exists = AllocationItem::where([
                                            ['auto_allocation_id',$auto_allocation_id],
                                            ['item_id_book',$item_id_book],
                                            ['item_id_source',$item_id_source],
                                            ['style',$style],
                                            ['po_buyer',$new_po_buyer],
                                            //['qty_booking',$__supply],
                                            ['is_additional',false],
                                            ['is_from_allocation_fabric',true],
                                            ['warehouse_inventory',$warehouse_id] // warehouse inventory
                                        ])
                                        ->where(function($query){
                                            $query->where('confirm_by_warehouse','approved')
                                            ->OrWhereNull('confirm_by_warehouse');
                                        })
                                        ->whereNull('deleted_at')
                                        ->exists();
                                    }
                                    
                                    if(!$is_exists && $qty_required > 0)
                                    {
                                        //jika jepang cina dimaksimalkan 2 lot
                                        if($auto_allocation->is_ordering_for_japan_china == 1)
                                        {
                                            //ambil lot dengan qty paling gede
                                            $max_qty_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                            ->whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['qty_available', '>', 0],
                                            ])
                                            ->groupBy('actual_lot')
                                            ->orderBy('total_qty', 'desc')
                                            ->first();

                                            $lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->actual_lot : null;
                                            $qty_lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->total_qty : 0;

                                            if($lot_max != null)
                                            {

                                                //cari lot sebelumnya 
                                                $before_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                                ->whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                    ['actual_lot', '<', $lot_max],
                                                ])
                                                ->groupBy('actual_lot')
                                                ->orderBy('total_qty', 'desc')
                                                ->first();

                                                $lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->actual_lot : null;
                                                $qty_lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->total_qty : 0;

                                                //cari lot setelahnya
                                                $after_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                                ->whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                    ['actual_lot', '>', $lot_max],
                                                ])
                                                ->groupBy('actual_lot')
                                                ->orderBy('total_qty', 'desc')
                                                ->first();

                                                $lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->actual_lot : null;
                                                $qty_lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->total_qty : 0;

                                                //list urutan lotnya 
                                                $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                ])
                                                ->orderBy(db::raw(" actual_lot ='".$lot_max."'"), 'desc')
                                                ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                    actual_lot ='".$lot_before."'
                                                    else
                                                    actual_lot ='".$lot_after."'
                                                    end"), 'desc')
                                                ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                    actual_lot >'".$lot_before."'
                                                    else
                                                    actual_lot >'".$lot_after."'
                                                    end"), 'desc')
                                                ->get();    
                                            }
                                            else
                                            {
                                                $material_stock_per_lots = array();
                                            }

                                        }
                                        else
                                        {
                                            $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['qty_available', '>', 0],
                                            ])
                                            ->orderBy('actual_lot', 'asc')
                                            ->orderBy('qty_available', 'desc')
                                            ->get();

                                        }

                                        //$count_lot = 0;
                                        if(count($material_stock_per_lots) > 0)
                                        {
                                            //dd($material_stock_per_lots->pluck('id', 'qty_available'));
                                            //$ini_array = array();
                                                foreach ($material_stock_per_lots as $key_3 => $material_stock_per_lot) 
                                                {
                                                    if($qty_required > 0.0000)
                                                    {
                                                        $material_stock_per_lot_id = $material_stock_per_lot->id;
                                                        $qty_available_lot         = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                                                        $qty_reserved_lot          = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                                                        //cek apakah stock per lot cukup atau tidak 
                                                        if($qty_available_lot >= $qty_required )
                                                        {
                                                            // $ini_array [] = [
                                                            //     'qty_available_lot'         => $qty_available_lot,
                                                            //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            //     'qty_required'              => $qty_required,
                                                            //     'style'                     => $style
                                                            // ];
                                                            $qty_booking = $qty_required;
                                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                                'auto_allocation_id'        => $auto_allocation_id,
                                                                'lc_date'                   => $lc_date,
                                                                'c_bpartner_id'             => $c_bpartner_id,
                                                                'c_order_id'                => $c_order_id,
                                                                'supplier_code'             => $supplier_code,
                                                                'supplier_name'             => $supplier_name,
                                                                'document_no'               => $document_no,
                                                                'item_code'                 => $item_code,
                                                                'item_id_book'              => $item_id_book,
                                                                'item_code_source'          => $item_code_source,
                                                                'item_id_source'            => $item_id_source,
                                                                'item_desc'                 => $item_desc,
                                                                'category'                  => $category,
                                                                'uom'                       => $uom,
                                                                'job'                       => $job_order,
                                                                '_style'                    => $_style,
                                                                'style'                     => $style,
                                                                'article_no'                => $article_no,
                                                                'po_buyer'                  => $new_po_buyer,
                                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                                'warehouse'                 => $warehouse_production_id,
                                                                'warehouse_inventory'       => $warehouse_id,
                                                                'is_need_to_handover'       => false,
                                                                'qty_booking'               => $qty_booking,
                                                                'is_from_allocation_fabric' => true,
                                                                'is_additional'             => $is_additional,
                                                                'user_id'                   => $user_id,
                                                                'confirm_by_warehouse'      => 'approved',
                                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                                'confirm_date'              => Carbon::now(),
                                                                'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                                                'confirm_user_id'           => $system->id,
                                                                'deleted_at'                => null,
                                                                'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            ]);

                                                            //update available stock per lot
                                                            $qty_available_lot                     -= $qty_booking;
                                                            $qty_reserved_lot                      += $qty_booking;
                                                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                            $material_stock_per_lot->save();
                                                            //$__supply                               = $qty_booking;
                                                        }
                                                        else
                                                        {
                                                            //jika kurang dari qty booking pakai available yang tersedia
                                                            $qty_booking = $qty_available_lot;
                                                            // $ini_array [] = [
                                                            //     'qty_available_lot'         => $qty_available_lot,
                                                            //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            //     'qty_required'              => $qty_required,
                                                            //     'style'                     => $style
                                                            // ];

                                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                                'auto_allocation_id'        => $auto_allocation_id,
                                                                'lc_date'                   => $lc_date,
                                                                'c_bpartner_id'             => $c_bpartner_id,
                                                                'c_order_id'                => $c_order_id,
                                                                'supplier_code'             => $supplier_code,
                                                                'supplier_name'             => $supplier_name,
                                                                'document_no'               => $document_no,
                                                                'item_code'                 => $item_code,
                                                                'item_id_book'              => $item_id_book,
                                                                'item_code_source'          => $item_code_source,
                                                                'item_id_source'            => $item_id_source,
                                                                'item_desc'                 => $item_desc,
                                                                'category'                  => $category,
                                                                'uom'                       => $uom,
                                                                'job'                       => $job_order,
                                                                '_style'                    => $_style,
                                                                'style'                     => $style,
                                                                'article_no'                => $article_no,
                                                                'po_buyer'                  => $new_po_buyer,
                                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                                'warehouse'                 => $warehouse_production_id,
                                                                'warehouse_inventory'       => $warehouse_id,
                                                                'is_need_to_handover'       => false,
                                                                'qty_booking'               => $qty_booking,
                                                                'is_from_allocation_fabric' => true,
                                                                'is_additional'             => $is_additional,
                                                                'user_id'                   => $user_id,
                                                                'confirm_by_warehouse'      => 'approved',
                                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                                'confirm_date'              => Carbon::now(),
                                                                'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                                                'confirm_user_id'           => $system->id,
                                                                'deleted_at'                => null,
                                                                'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            ]);

                                                            //update available stock per lot
                                                            $qty_available_lot                     -= $qty_booking;
                                                            $qty_reserved_lot                      += $qty_booking;
                                                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                            $material_stock_per_lot->save();
                                                            //$__supply                               = $qty_booking;
                                                        }

                                                        //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                                        //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                                                        $qty_required       -= $qty_booking;
                                                        $qty_outstanding    -= $qty_booking;
                                                        $qty_allocated      += $qty_booking;
                                                        //$count_lot          += 1;
                                                        $buyers []           = $new_po_buyer;
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                                //dd($ini_array);

                                        }
                                        
                                        if($qty_required > 0.0000)
                                        {
                                            $__supply = $qty_required;

                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                'auto_allocation_id'        => $auto_allocation_id,
                                                'lc_date'                   => $lc_date,
                                                'c_bpartner_id'             => $c_bpartner_id,
                                                'c_order_id'                => $c_order_id,
                                                'supplier_code'             => $supplier_code,
                                                'supplier_name'             => $supplier_name,
                                                'document_no'               => $document_no,
                                                'item_code'                 => $item_code,
                                                'item_id_book'              => $item_id_book,
                                                'item_code_source'          => $item_code_source,
                                                'item_id_source'            => $item_id_source,
                                                'item_desc'                 => $item_desc,
                                                'category'                  => $category,
                                                'uom'                       => $uom,
                                                'job'                       => $job_order,
                                                '_style'                    => $_style,
                                                'style'                     => $style,
                                                'article_no'                => $article_no,
                                                'po_buyer'                  => $new_po_buyer,
                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                'warehouse'                 => $warehouse_production_id,
                                                'warehouse_inventory'       => $warehouse_id,
                                                'is_need_to_handover'       => false,
                                                'qty_booking'               => $__supply,
                                                'is_from_allocation_fabric' => true,
                                                'is_additional'             => $is_additional,
                                                'user_id'                   => $user_id,
                                                'confirm_by_warehouse'      => 'approved',
                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                'confirm_date'              => Carbon::now(),
                                                'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                                'confirm_user_id'           => $system->id,
                                                'deleted_at'                => null
                                        ]);

                                        $qty_required       -= $__supply;
                                        $qty_outstanding    -= $__supply;
                                        $qty_allocated      += $__supply;
                                        $buyers []           = $new_po_buyer;

                                        }
                                    }
                                    if($is_additional == true)
                                    {
                                        //tambahkan additional ke preparation 
                                        // $is_material_preparation_exists = MaterialPreparationFabric::where([
                                        //     ['c_order_id',$c_order_id],
                                        //     ['article_no',$article_no],
                                        //     ['warehouse_id',$warehouse_id],
                                        //     ['item_id_book',$item_id_book],
                                        //     ['item_id_source',$item_id_source],
                                        //     ['planning_date',Carbon::now()->format('Y-m-d')],
                                        //     ['is_piping',false],
                                        //     ['_style',$style],
                                        //     ['is_from_additional', false],
                                        //     ['total_reserved_qty', $qty_allocation]
                                        // ])
                                        // ->first();
                                
                                        // if(!$is_material_preparation_exists)
                                        // {

                                            $check_material_preparation_fab = db::table('material_preparation_fabrics')
                                            ->where('auto_allocation_id', $auto_allocation_id)
                                            ->where('document_no',$document_no)
                                            ->where('item_code', $item_code )
                                            ->where('po_buyer', $new_po_buyer)
                                            ->exists();

                                            if($check_material_preparation_fab==false){
                                            $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                                                'c_bpartner_id'         => $c_bpartner_id,
                                                'supplier_name'         => $supplier_name,
                                                'document_no'           => $document_no,
                                                'po_buyer'              => $new_po_buyer,    
                                                'item_code'             => $item_code,
                                                'item_code_source'      => $item_code_source,
                                                'c_order_id'            => $c_order_id,
                                                'item_id_book'          => $item_id_book,
                                                'item_id_source'        => $item_id_source,
                                                'uom'                   => 'YDS',
                                                'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                                                'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                                                'planning_date'         => Carbon::now()->format('Y-m-d'),
                                                'is_piping'             => false,
                                                'article_no'            => $article_no,
                                                '_style'                => $style,
                                                'is_from_additional'    => true,
                                                'is_balance_marker'     => false,
                                                'preparation_date'      => null,
                                                'preparation_by'        => null,
                                                'warehouse_id'          => $warehouse_id,
                                                'user_id'               => auth::user()->id,
                                                'created_at'            => $movement_date,
                                                'updated_at'            => $movement_date,
                                                'auto_allocation_id'    => $auto_allocation_id,
                                            ]);
                                        }

                                        // }
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                    
                    if(floatVal($qty_outstanding)<= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                        $auto_allocation->qty_outstanding                   = 0;
                    }else
                    {
                        $auto_allocation->qty_outstanding                   = sprintf('%0.8f',$qty_outstanding);
                    
                    }

                    $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                    $auto_allocation->save();

                }
                //additional
                // else
                // {
                    // $material_requirements = MaterialRequirement::where([
                    //     ['po_buyer',$new_po_buyer],
                    //     [db::raw('upper(item_id)'),$item_id_book],
                    // ])
                    // ->first();

                    // $job_order  = $material_requirement->job_order;
                    // $_style     = explode('::',$job_order)[0];
                    // $article_no = $material_requirement->article_no;

                    // HistoryAutoAllocations::FirstOrCreate([
                    //     'auto_allocation_id'        => $auto_allocation_id,
                    //     'c_bpartner_id_new'         => $c_bpartner_id,
                    //     'supplier_name_new'         => $supplier_name,
                    //     'document_no_new'           => $document_no,
                    //     'item_id_new'               => $item_id_book,
                    //     'item_code_new'             => $item_code,
                    //     'item_source_id_new'        => $item_id_source,
                    //     'item_code_source_new'      => $item_code_source,
                    //     'po_buyer_new'              => $po_buyer,
                    //     'uom_new'                   => 'YDS',
                    //     'qty_allocated_new'         => $qty_allocation,
                    //     'warehouse_id_new'          => $warehouse_id,
                    //     'type_stock_new'            => $type_stock,
                    //     'type_stock_erp_code_new'   => $type_stock_erp_code,
                    //     'note_code'                 => $note_code,
                    //     'note'                      => $note,
                    //     'operator'                  => $qty_allocation,
                    //     'remark'                    => $remark,
                    //     'is_integrate'              => true,
                    //     'user_id'                   => auth::user()->id
                    // ]);
            
                    //tambahan ke material preparation fabric
            
                    // $is_material_preparation_exists = MaterialPreparationFabric::where([
                    //     ['c_order_id',$c_order_id],
                    //     ['article_no',$article_no],
                    //     ['warehouse_id',$warehouse_id],
                    //     ['item_id_book',$item_id_book],
                    //     ['item_id_source',$item_id_source],
                    //     ['planning_date',$planning_date],
                    //     ['is_piping',false],
                    //     ['_style',$style],
                    //     ['is_additional', false],
                    //     ['total_reserved_qty', $qty_allocation]
                    // ])
                    // ->first();
            
                    // if(!$is_material_preparation_exists)
                    // {
                    //     $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                    //         'c_bpartner_id'         => $c_bpartner_id,
                    //         'supplier_name'         => $supplier_name,
                    //         'document_no'           => $document_no,
                    //         'po_buyer'              => $po_buyer,    
                    //         'item_code'             => $item_code,
                    //         'item_code_source'      => $item_code_source,
                    //         'c_order_id'            => $c_order_id,
                    //         'item_id_book'          => $item_id_book,
                    //         'item_id_source'        => $item_id_source,
                    //         'uom'                   => 'YDS',
                    //         'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                    //         'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                    //         'planning_date'         => $movement_date->format('Y-m-d'),
                    //         'is_piping'             => false,
                    //         'article_no'            => $article_no,
                    //         '_style'                => $style,
                    //         'is_from_additional'    => true,
                    //         'is_balance_marker'     => false,
                    //         'preparation_date'      => null,
                    //         'preparation_by'        => null,
                    //         'warehouse_id'          => $warehouse_id,
                    //         'user_id'               => auth::user()->id,
                    //         'created_at'            => $movement_date,
                    //         'updated_at'            => $movement_date,
                    //         'auto_allocation_id'    => $auto_allocation_id,
                    //     ]);
                    // }

               // }
                    
                //}

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
        }

            PlanningFabric::getAllocationFabricPerBuyer($buyers);

    }

    static function doRecalculate($id)
    {
        try
        {
            DB::beginTransaction();
            //return view('errors.503');
            $auto_allocation        = AutoAllocation::find($id);
            $c_order_id             = $auto_allocation->c_order_id;
            $item_id                = $auto_allocation->item_id_source;
            $warehouse_id           = $auto_allocation->warehouse_id;

            if($auto_allocation->status_po_buyer =='active' && !$auto_allocation->is_reduce)
            {
                if($auto_allocation->is_fabric)
                {
                    $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
    
                    $qty_allocated  = AllocationItem::where([
                        ['auto_allocation_id',$id],
                    ])
                    ->sum('qty_booking');
    
                    $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));
    
                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf('%0.8f',$new_oustanding);
    
                    $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                    $auto_allocation->save();

                    // $refresh = DB::select(db::raw("
                    // refresh materialized view monitoring_allocations_mv;"
                    // ));

                    Dashboard::dailyUpdateMonitoringMaterial($c_order_id, $item_id, $warehouse_id);
    
                }else 
                {
                    $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
    
                    $qty_allocated  = MaterialPreparation::where([
                        ['auto_allocation_id',$id],
                        ['last_status_movement','!=','adjustment'],
                        ['is_reroute',false],
                        ['is_from_handover',false],
                    ])
                    ->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));
    
                    $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));
    
                    if($new_oustanding <= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                    }else
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }
                
                    if ($new_oustanding == 0) $new_oustanding = '0';
                    else $new_oustanding = sprintf('%0.8f',$new_oustanding);
    
                    $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                    $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                    $auto_allocation->save();
    
                    
                }
            }
           
            
            DB::commit();
        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function syncAutoAllocationErp()
    {
        //return view('errors.503');
        $app_env        = Config::get('app.env');
        $movement_date  = carbon::now()->toDateTimeString();
        
        if($app_env == 'dev')
        {
            $allocation_on_erp = DB::connection('dev_erp')
            ->table('rma_report_free_stock_detail_wms') 
            ->select('product_value'
            ,'no_po'
            ,'supplier'
            ,'tgl_lc'
            ,'c_order_id'
            ,'m_product_id'
            ,'isfabric'
            ,'poreference'
            ,'m_warehouse_id'
            ,'c_bpartner_id'
            ,db::raw("sum(qty_alokasi) as qty_alokasi"))
            ->whereNotNull('poreference')
            ->whereNotNull('tgl_lc')
            ->where('poreference','0124247118')
            ->whereIn(db::raw("to_char(tgl_lc,'YYYY-MM-DD')"),['2019-09-26'])
            ->groupby('product_value'
            ,'no_po'
            ,'supplier'
            ,'tgl_lc'
            ,'isfabric'
            ,'c_order_id'
            ,'m_product_id'
            ,'poreference'
            ,'m_warehouse_id'
            ,'c_bpartner_id')
            ->get();
            
        }else
        {
            $allocation_on_erp = DB::connection('erp')
            ->table('rma_wms_planning_alokasi')
            ->where([
                ['isintegrate',false],
                ['status_lc','BO'],
                ['m_warehouse_id', '!=', '1000024'],
                //['po_buyer', 'not like', '%-S%']
            ])
            ->get();
        }

        try 
        {
    		DB::beginTransaction();
            
            foreach ($allocation_on_erp as $key => $value) 
            {
                $erp_allocation_id      = $value->uuid;
                $warehouse_id           = $value->m_warehouse_id;
                $_c_bpartner_id         = $value->c_bpartner_id;
                $c_order_id             = $value->c_order_id;
                $item_id                = $value->item_id;
                $item_code              = strtoupper($value->item_code);
                $document_no            = strtoupper($value->document_no);
                $supplier_name          = strtoupper($value->supplier_name);
                // $po_buyers         = PoBuyer::where('po_buyer', $value->po_buyer)->where('brand', 'ADIDAS')->first();
                // if($po_buyers)
                // {
                    $po_buyer               = trim(str_replace('-S', '',$value->po_buyer));
                    $po_sample              = strpos($value->po_buyer, '-S');
                // }
                // {
                //     $po_buyer               = $value->po_buyer;
                //     $po_sample              = false;
                // }
                $lc_date                = null;
                $promise_date           = null;
                $status_po_buyer        = null;
                $season                 = null;
                $item_desc              = null;
                $category               = null;
                $uom                    = strtoupper($value->uom);
                $qty_allocation         = sprintf('%0.8f',$value->qty);
                $master_data_po_buyer   = PoBuyer::where('po_buyer',$po_buyer)->first();
                $item                   = Item::where('item_id',$item_id)->first();
                
                if($_c_bpartner_id) $c_bpartner_id = $_c_bpartner_id;
                else $c_bpartner_id                 = 'FREE STOCK';

                if($c_order_id) $c_order_id   = $c_order_id;
                else $c_order_id              = 'FREE STOCK';

                if($master_data_po_buyer)
                {
                    $type_stock_erp_code                            = $master_data_po_buyer->type_stock_erp_code;
                    if($type_stock_erp_code == 1) $type_stock       = 'SLT';
                    elseif($type_stock_erp_code == 2) $type_stock   = 'REGULER';
                    elseif($type_stock_erp_code == 3) $type_stock   = 'PR/SR';
                    elseif($type_stock_erp_code == 4) $type_stock   = 'MTFC';
                    elseif($type_stock_erp_code == 5) $type_stock   = 'NB';
                    else $type_stock                                = null;

                    $lc_date                                        = $master_data_po_buyer->lc_date;
                    $promise_date                                   = ($master_data_po_buyer->statistical_date ? $master_data_po_buyer->statistical_date : $master_data_po_buyer->promise_date);
                    $cancel_date                                    = $master_data_po_buyer->cancel_date;
                    $season                                         = $master_data_po_buyer->season;
                    
                    if($cancel_date != null) $status_po_buyer       = 'cancel';
                    else $status_po_buyer                           = 'active';


                    if($value->isfabric == 'N')
                    {
                        $is_fabric                  = false;
                        $tgl_date                   = ($master_data_po_buyer->lc_date ? $master_data_po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($master_data_po_buyer->lc_date ? $master_data_po_buyer->lc_date->format('my')  : 'LC NOT FOUND');;

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';

                        $document_allocation_number = 'BOLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }else
                    {
                        $is_fabric                  = true;
                        $document_allocation_number = null;
                    }
                    
                }else
                {
                    $erp_master_po_buyer = DB::connection('erp')
                    ->table('wms_lc_buyers')
                    ->select('kst_lcdate as lc_date',
                            'poreference as po_buyer',
                            'kst_season as season'
                            ,'kst_statisticaldate as statistical_date'
                            ,'kst_statisticaldate2 as statistical_date_2'
                            ,'ordertype as order_type'
                            ,'brand as brand'
                            ,'datepromised as promise_date'
                            ,'so_id')
                    ->where('poreference', $po_buyer)
                    ->first();

                    if($erp_master_po_buyer)
                    {
                        if($erp_master_po_buyer->brand == 'NEW BALANC') $erp_brand ='NEW BALANCE';
                        else $erp_brand = $erp_master_po_buyer->brand;

                        if($erp_master_po_buyer->order_type == 'MTF')
                        {
                            $erp_type_stock                 = 'MTFC';
                            $erp_type_stock_erp_code        = '4';
                        }else if($erp_master_po_buyer->order_type == 'SLT')
                        {
                            $erp_type_stock                 = 'SLT';
                            $erp_type_stock_erp_code        = '1';
                        }else if($erp_master_po_buyer->order_type == 'SR')
                        {
                            $erp_type_stock                 = 'SR';
                            $erp_type_stock_erp_code        = '3';
                    }else if($erp_master_po_buyer->order_type == 'NB')
                        {
                            $erp_type_stock                 = 'NB';
                            $erp_type_stock_erp_code        = '5';
                        }else
                        {
                            if($erp_brand == 'ADIDAS')
                            {
                                $erp_type_stock             = 'REGULER';
                                $erp_type_stock_erp_code    = '2';
                            }else
                            {
                                $erp_type_stock             = null;
                                $erp_type_stock_erp_code    = null;
                            }
                        }

                        if($erp_master_po_buyer->statistical_date_2) $erp_statistical_date = $erp_master_po_buyer->statistical_date_2;
                        else  $erp_statistical_date = $erp_master_po_buyer->statistical_date;
                    }else
                    {
                        $erp_statistical_date       = null;
                        $erp_type_stock             = null;
                        $erp_type_stock_erp_code    = null;
                    }
                    if($erp_master_po_buyer == null)
                    {
                        echo $po_buyer. ' id ini error';
                    }

                    $new_data_po_buyer = PoBuyer::FirstorCreate([
                        'po_buyer'              => $erp_master_po_buyer->po_buyer,
                        'lc_date'               => $erp_master_po_buyer->lc_date,
                        'statistical_date'      => $erp_statistical_date,
                        'promise_date'          => $erp_master_po_buyer->promise_date,
                        'season'                => $erp_master_po_buyer->season,
                        'brand'                 => $erp_brand,
                        'type_stock'            => $erp_type_stock,
                        'type_stock_erp_code'   => $erp_type_stock_erp_code,
                        'so_id'                 => $erp_master_po_buyer->so_id,
                    ]);

                    $type_stock_erp_code                            = $new_data_po_buyer->type_stock_erp_code;
                    if($type_stock_erp_code == 1) $type_stock       = 'SLT';
                    elseif($type_stock_erp_code == 2) $type_stock   = 'REGULER';
                    elseif($type_stock_erp_code == 3) $type_stock   = 'PR/SR';
                    elseif($type_stock_erp_code == 4) $type_stock   = 'MTFC';
                    elseif($type_stock_erp_code == 5) $type_stock   = 'NB';
                    else $type_stock                                = null;

                    $lc_date                                        = $new_data_po_buyer->lc_date;
                    $promise_date                                   = ($new_data_po_buyer->statistical_date ? $new_data_po_buyer->statistical_date : $new_data_po_buyer->promise_date);
                    $cancel_date                                    = $new_data_po_buyer->cancel_date;
                    $season                                         = $new_data_po_buyer->season;
                    
                    if($cancel_date != null) $status_po_buyer       = 'cancel';
                    else $status_po_buyer                           = 'active';

                    if($value->isfabric == 'N')
                    {
                        $is_fabric                  = false;
                        $tgl_date                   = ($new_data_po_buyer->lc_date ? $new_data_po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($new_data_po_buyer->lc_date ? $new_data_po_buyer->lc_date->format('my')  : 'LC NOT FOUND');;

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';

                        $document_allocation_number = 'BOLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }else
                    {
                        $is_fabric                  = true;
                        $document_allocation_number = null;
                    }
                }
                
                
                if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI-1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI-2';
                else if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI-2';
                else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI-1';
               
                if($item)
                {
                    $item_desc = strtoupper(trim($item->item_desc));
                    $category   = strtoupper(trim($item->category));
                }
                $is_exists = null;
                //cek po shipment sample
                if ($po_sample === false )
                {
                    $is_exists = AutoAllocation::where([
                        ['lc_date',$lc_date],
                        ['season',$season],
                        [db::raw('upper(document_no)'),$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(item_code_source)'),$item_code],
                        [db::raw('upper(item_code)'),$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['qty_allocation',$qty_allocation],
                        ['is_fabric',$is_fabric],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ]);
                    if($erp_allocation_id) $is_exists = $is_exists->where('erp_allocation_id',$erp_allocation_id);
                    $is_exists = $is_exists->first();
                }
                else
                {
                    $is_exists = AutoAllocation::where([
                        ['lc_date',$lc_date],
                        ['season',$season],
                        [db::raw('upper(document_no)'),$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(item_code_source)'),$item_code],
                        [db::raw('upper(item_code)'),$item_code],
                        ['warehouse_id',$warehouse_id],
                        //['qty_allocation',$qty_allocation], BUAT NAMBAH QTY DARI PO SHIPMENT SAMPLE
                        ['is_fabric',$is_fabric],
                        ['type_stock_erp_code',$type_stock_erp_code],
                    ]);
                    $is_exists = $is_exists->first();
                }
                if(!$is_exists)
                {
                    if($category == 'BD' || $category == 'LP' || $category == 'MO')
                    {
                        $deleted_at = Carbon::now();
                        $remark     = 'KATEGORI INI DI ANGGAP SEBAGAI EXPENSE';
                    }else
                    {
                        $deleted_at = null;
                        $remark     = 'AUTO ALLOCATION';
                    }

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    $so_id     = PoBuyer::where('po_buyer', $po_buyer)->first();
                    $is_exists = AutoAllocation::firstorcreate([
                        'document_allocation_number'        => $document_allocation_number,
                        'type_stock_erp_code'               => $type_stock_erp_code,
                        'type_stock'                        => $type_stock,
                        'lc_date'                           => $lc_date,
                        'promise_date'                      => $promise_date,
                        'season'                            => $season,
                        'document_no'                       => $document_no,
                        'c_bpartner_id'                     => $c_bpartner_id,
                        'supplier_name'                     => $supplier_name,
                        'po_buyer'                          => $po_buyer,
                        'old_po_buyer'                      => $po_buyer,
                        'c_order_id'                        => $c_order_id,
                        'item_id_source'                    => $item_id,
                        'item_id_book'                      => $item_id,
                        'item_code_source'                  => $item_code,
                        'item_code'                         => $item_code,
                        'item_code_book'                    => $item_code,
                        'item_desc'                         => $item_desc,
                        'category'                          => $category,
                        'uom'                               => $uom,
                        'warehouse_name'                    => $warehouse_name,
                        'warehouse_id'                      => $warehouse_id,
                        'qty_allocation'                    => $qty_allocation,
                        'qty_outstanding'                   => $qty_allocation,
                        'qty_allocated'                     => 0,
                        'is_fabric'                         => $is_fabric,
                        'deleted_at'                        => $deleted_at,
                        'remark'                            => $remark,
                        'is_already_generate_form_booking'  => false,
                        'is_upload_manual'                  => false,
                        'is_allocation_purchase'            => false,
                        'status_po_buyer'                   => $status_po_buyer,
                        'user_id'                           => $system->id,
                        'created_at'                        => $movement_date,
                        'updated_at'                        => $movement_date,
                        'erp_allocation_id'                 => $erp_allocation_id,
                        'so_id'                             => $so_id->so_id
                    ]);

                    DB::connection('erp') //dev_erp //erp
                    ->table('rma_wms_planning_alokasi')
                    ->where('uuid',$erp_allocation_id)
                    ->update([
                        'isintegrate' => true,
                        'updated'     => $movement_date
                    ]);
                }else
                {
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();
                    $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                    $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                    $remark              = $is_exists->remark;

                    $is_exists->qty_allocation    = $old_qty_allocation + $qty_allocation;
                    $is_exists->qty_outstanding   = $old_qty_outstanding + $qty_allocation;
                    $is_exists->erp_allocation_id = $erp_allocation_id;
                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty_allocation;
                    $is_exists->user_id           = $system->id;
                    $is_exists->save();

                    DB::connection('erp') //dev_erp //erp
                    ->table('rma_wms_planning_alokasi')
                    ->where('uuid',$is_exists->erp_allocation_id)
                    ->update([
                        'isintegrate' => true,
                        'updated'     => $movement_date
                    ]);
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    static function updateAllocationReservedAccessoriesItems($update_auto_allocations,$spesific_id = null)
    {
        $confirm_date                           = Carbon::now();
        $recalculate                            = array();
        
        $supplier_location = Locator::with('area')
        ->whereHas('area',function ($query)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',auth::user()->warehouse);
            $query->where('name','SUPPLIER');
        })
        ->first();

        $receiving_location = Locator::with('area')
        ->whereHas('area',function ($query)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',auth::user()->warehouse);
            $query->where('name','RECEIVING');
        })
        ->first();
        
        try 
        {
    		DB::beginTransaction();
            $concatenate_preparation    = '';
            $material_stocks = MaterialStock::where([
                ['is_running_stock',true],
                ['is_allocated',false],
                ['is_closing_balance',false],
            ])
            ->whereNull('last_status')
            ->whereNotNull('approval_date')
            ->whereNull('deleted_at')
            ->whereIn(db::raw("(c_order_id,c_bpartner_id,item_id,warehouse_id)"),function($query) use($spesific_id) 
            {
                $query->select('c_order_id','c_bpartner_id','item_id','warehouse_id')
                ->from('auto_allocations')
                ->where('id',$spesific_id)
                ->groupby('c_order_id','c_bpartner_id','item_id','warehouse_id');
            })
            ->get();

            foreach ($material_stocks as $key => $material_stock)
            {
                $material_stock->last_status    = 'prepared';
                $material_stock->save();

                $document_no                    = strtoupper($material_stock->document_no);
                $item_code_source               = strtoupper($material_stock->item_code);
                $material_stock_id              = $material_stock->id;
                $po_detail_id                   = $material_stock->po_detail_id;
                $po_buyer_source                = $material_stock->po_buyer;
                $type_stock_erp_code            = $material_stock->type_stock_erp_code;
                $type_stock                     = $material_stock->type_stock;
                $uom_source                     = ( $material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);

                $supplier_name                  = strtoupper($material_stock->supplier_name);
                $c_bpartner_id                  = $material_stock->c_bpartner_id;
                $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                $supplier_code                  = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                $is_running_stock               = $material_stock->is_running_stock;
                $warehouse_id                   = $material_stock->warehouse_id;
                $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
                $available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                $is_bom_exists                  = 0;
                $c_order_id                     = $material_stock->c_order_id;

                if($c_order_id == 'FREE STOCK') $is_need_inserted_to_locator_free_stock_erp = true;
                else $is_need_inserted_to_locator_free_stock_erp = false;

                $material_arrival           = MaterialArrival::where([
                    ['po_detail_id',$po_detail_id],
                    ['warehouse_id',$warehouse_id],
                ])
                ->first();
                
                if (strpos($document_no, "POAR") !== false) $is_poar = true;
                else $is_poar = false;

                $prepared_status            = ($material_arrival ?$material_arrival->prepared_status : null);
                $material_arrival_id        = ($material_arrival ? $material_arrival->id : null);
                $is_moq                     = ($material_arrival ?$material_arrival->is_moq : null);
                $po_buyer                   = ($material_arrival ?$material_arrival->po_buyer : null);
                $no_packing_list            = ($material_arrival ?$material_arrival->no_packing_list : '-');
                $no_invoice                 = ($material_arrival ?$material_arrival->no_invoice : '-');
                $c_orderline_id             = ($material_arrival ?$material_arrival->c_orderline_id : '-');

                $system                         = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $conversion = UomConversion::where([
                    ['item_code',$item_code_source],
                    ['uom_from',$uom_source]
                ])
                ->first();
            
                if($conversion)
                {
                    $multiplyrate   = $conversion->multiplyrate;
                    $dividerate     = $conversion->dividerate;
                }else
                {
                    $dividerate     = 1;
                    $multiplyrate   = 1;
                }
                
                $auto_allocations               = AutoAllocation::where([
                    [db::raw('upper(document_no)'),$document_no],
                    [db::raw('upper(item_code_source)'),$item_code_source],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['warehouse_id',$warehouse_id],
                    ['status_po_buyer','active'],
                    ['is_fabric',false],
                    //['is_allocation_purchase',false],
                ]);
                
                if($prepared_status != 'NO NEED PREPARED') $auto_allocations = $auto_allocations->where('qty_outstanding','>',0);
                if($spesific_id)    $auto_allocations   = $auto_allocations->where('id',$spesific_id);

                if($is_running_stock) $auto_allocations = $auto_allocations->where('is_allocation_purchase',true);
                else $auto_allocations = $auto_allocations->where('is_allocation_purchase',false);

                //if($type_stock_erp_code) $auto_allocations = $auto_allocations->where('type_stock_erp_code',$type_stock_erp_code);
                
                $auto_allocations               = $auto_allocations->whereNull('deleted_at')
                ->orderby('promise_date','asc')
                ->orderby('qty_allocation','asc')
                ->get();
                    
                if($available_qty > 0)
                {
                    //echo $available_qty.'<br/>';
                    foreach ($auto_allocations as $key_2 => $auto_allocation) 
                    {
                        $auto_allocation_id     = $auto_allocation->id;
                        $is_reduce              = $auto_allocation->is_reduce;
                        $is_additional          = $auto_allocation->is_additional;
                        $remark_additional      = $auto_allocation->remark_additional;
                        $_item_code_book        = $auto_allocation->item_code;
                        $c_order_id             = $auto_allocation->c_order_id;
                        $item_id_book           = $auto_allocation->item_id_book;
                        $item_id_source         = $auto_allocation->item_id_source;
                        $status_po_buyer        = $auto_allocation->status_po_buyer;

                        if($auto_allocation->is_upload_manual)
                        {
                            $allocation_source  = 'ALLOCATION MANUAL';
                            $user               = $auto_allocation->user_id;
                            $username           = strtoupper($auto_allocation->user->name);
                            $is_integrate       = false;
                        }else
                        {
                            $allocation_source  = ($auto_allocation->is_allocation_purchase) ? 'ALLOCATION PURCHASE':'ALLOCATION ERP';
                            $user               = $system->id;
                            $username           = strtoupper($auto_allocation->user->name);
                            
                            if($auto_allocation->is_allocation_purchase)  $is_integrate = true;
                            else $is_integrate = true;
                        }

                        $pos                    = strpos($_item_code_book, '|');
                        if ($pos === false)
                        {
                            $item_code_book     = $_item_code_book;
                            $article_no         = null;
                        }else 
                        {
                            $split              = explode('|',$_item_code_book);
                            $item_code_book     = $split[0];
                            $article_no         = $split[1];
                        }

                        $new_po_buyer           = $auto_allocation->po_buyer;
                        $old_po_buyer           = $auto_allocation->old_po_buyer;
                        $qty_outstanding        = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$auto_allocation->qty_outstanding));//sprintf('%0.8f',$auto_allocation->qty_outstanding);
                        $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                        
                        $_po_buyer              = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                        
                        $material_requirements  = MaterialRequirement::where([
                            ['po_buyer',$new_po_buyer],
                            [db::raw('upper(item_code)'),$item_code_book],
                        ]);
                        if($article_no) 
                            $material_requirements  = $material_requirements->where('article_no',$article_no);
                            
                        $material_requirements      = $material_requirements->orderby('article_no','asc')
                        ->orderby('style','asc')
                        ->get();
                        
                        $count_mr = count($material_requirements);
                        if($count_mr > 0)
                        {
                            if ($available_qty/$qty_outstanding >= 1) $_supply = $qty_outstanding;
                            else $_supply   = $available_qty;

                            $qty_supply     = $_supply;

                            $__qty_outstanding = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$qty_outstanding));

                            if($__qty_outstanding > 0.0000)
                            {
                                foreach ($material_requirements as $key => $material_requirement) 
                                {
                                    $lc_date                = $material_requirement->lc_date;
                                    $item_desc              = strtoupper($material_requirement->item_desc);
                                    $uom                    = $material_requirement->uom;
                                    $category               = $material_requirement->category;
                                    $style                  = $material_requirement->style;
                                    $job_order              = $material_requirement->job_order;
                                    $article_no             = $material_requirement->article_no;
                                    $_style                 = explode('::',$job_order)[0];
                                    $qty_required_allocated = $material_requirement->qtyAllocatedAllocation($new_po_buyer,$item_code_book,$style,$article_no);
                                    
                                    if($is_additional) $qty_required    = 1;
                                    else
                                    {
                                        if($status_po_buyer == 'active')
                                        {
                                            if($is_poar) $qty_required   = $qty_outstanding;
                                            else $qty_required   = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$material_requirement->qty_required - $qty_required_allocated));
                                        } 
                                        else if($status_po_buyer == 'cancel') $qty_required   = $qty_outstanding;
                                    };
                            
                                    //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                    //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                    
                                    //echo $po_buyer.' '.$item_code.' '.$qty_supply.' '.$qty_required.'</br>';
                                    if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                                    else $__supplied = sprintf('%0.8f',$qty_supply);

                                    /*echo $new_po_buyer
                                    .' '.$qty_supply
                                    .' '.$style
                                    .' QTY REQUIRED '.$material_requirement->qty_required
                                    .' QTY ALLOCATED '.$qty_required_allocated
                                    .' QTY SUPPLIED '.$__supplied
                                    .'<br/>'
                                    .'<br/>';*/

                                    if($__supplied > 0.0000 && $qty_supply > 0.0000)
                                    {
                                        if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                        else $_supplied = $qty_supply;

                                        if($prepared_status == 'NO NEED PREPARED') $_supplied = sprintf('%0.8f',$available_qty);
                                        else $_supplied = $_supplied;

                                        $__supplied = sprintf('%0.8f',$_supplied);

                                        if($is_additional)
                                        {
                                            if($_supplied > 0.0000)
                                            {
                                                
    
                                                //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                                $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);
                                                
                                                $material_preparation = MaterialPreparation::Create([
                                                    'material_stock_id'                             => $material_stock_id,
                                                    'auto_allocation_id'                            => $auto_allocation_id,
                                                    'po_detail_id'                                  => $po_detail_id,
                                                    'barcode'                                       => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'referral_code'                                 => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'sequence'                                      => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : -1),
                                                    'item_id'                                       => $item_id_book,
                                                    'c_order_id'                                    => $c_order_id,
                                                    'c_bpartner_id'                                 => $c_bpartner_id,
                                                    'supplier_name'                                 => $supplier_name,
                                                    'document_no'                                   => $document_no,
                                                    'po_buyer'                                      => $new_po_buyer,
                                                    'uom_conversion'                                => $uom,
                                                    'qty_conversion'                                => $__supplied,
                                                    'qty_reconversion'                              => $qty_reconversion,
                                                    'job_order'                                     => $job_order,
                                                    'style'                                         => $style,
                                                    '_style'                                        => $_style,
                                                    'article_no'                                    => $article_no,
                                                    'warehouse'                                     => $warehouse_id,
                                                    'item_code'                                     => $item_code_book,
                                                    'item_desc'                                     => $item_desc,
                                                    'category'                                      => $category,
                                                    'is_additional'                                 => true,
                                                    'is_backlog'                                    => false,
                                                    'total_carton'                                  => 1,
                                                    'type_po'                                       => 2,
                                                    'user_id'                                       => auth::user()->id,
                                                    'last_status_movement'                          => 'receive',
                                                    'is_need_to_be_switch'                          => false,
                                                    'is_need_inserted_to_locator_free_stock_erp'    => $is_need_inserted_to_locator_free_stock_erp,
                                                    'last_locator_id'                               => $receiving_location->id,
                                                    'last_movement_date'                            => $confirm_date,
                                                    'created_at'                                    => $confirm_date,
                                                    'updated_at'                                    => $confirm_date,
                                                    'first_print_date'                              => null,
                                                    'deleted_at'                                    => null,
                                                    'last_user_movement_id'                         => auth::user()->id,
                                                    'is_stock_already_created'                      => false
                                                ]);

                                                $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;
                                                
                                                $is_materal_movement_exists = MaterialMovement::where([
                                                    ['from_location',$supplier_location->id],
                                                    ['to_destination',$receiving_location->id],
                                                    ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                    ['no_packing_list',$no_packing_list],
                                                    ['no_invoice',$no_invoice],
                                                    ['po_buyer',$new_po_buyer],
                                                    ['status','receive'],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                ])
                                                ->first();
                                
                                                if(!$is_materal_movement_exists)
                                                {
                                                    $material_movement = MaterialMovement::firstOrCreate([
                                                        'from_location'         => $supplier_location->i,
                                                        'to_destination'        => $receiving_location->id,
                                                        'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                        'po_buyer'              => $new_po_buyer,
                                                        'no_packing_list'       => $no_packing_list,
                                                        'no_invoice'            => $no_invoice,
                                                        'is_integrate'          => false,
                                                        'is_active'             => true,
                                                        'status'                => 'receive',
                                                        'created_at'            => $confirm_date,
                                                        'updated_at'            => $confirm_date,
                                                    ]);

                                
                                                    $material_movement_id = $material_movement->id;
                                                }else
                                                {
                                                    $is_materal_movement_exists->updated_at = $confirm_date;
                                                    $is_materal_movement_exists->save();
                                
                                                    $material_movement_id = $is_materal_movement_exists->id;
                                                }

                                                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                ->where([
                                                    'material_movement_id'      => $material_movement_id,
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'item_id'                   => $material_preparation->id->item_id,
                                                    'warehouse_id'              => $material_preparation->warehouse,
                                                    'type_po'                   => 2,
                                                    'date_movement'             => $confirm_date,
                                                    'is_integrate'              => false,
                                                    'is_active'                 => true,
                                                    'qty_movement'              => $material_preparation->qty_conversion,
                                                ])
                                                ->exists();

                                                if(!$is_line_exists)
                                                {
                                                    
                                                    $new_material_movement_line = MaterialMovementLine::FirstOrCreate([
                                                        'material_movement_id'          => $material_movement_id,
                                                        'material_preparation_id'       => $material_preparation->id,
                                                        'item_id'                       => $material_preparation->item_id,
                                                        'item_id_source'                => $material_preparation->item_id_source,
                                                        'c_order_id'                    => $material_preparation->c_order_id,
                                                        'c_orderline_id'                => $c_orderline_id,
                                                        'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                        'supplier_name'                 => $material_preparation->supplier_name,
                                                        'document_no'                   => $material_preparation->document_no,
                                                        'item_code'                     => $material_preparation->item_code,
                                                        'nomor_roll'                    => '-',
                                                        'type_po'                       => '2',
                                                        'uom_movement'                  => $material_preparation->uom_conversion,
                                                        'qty_movement'                  => $material_preparation->qty_conversion,
                                                        'warehouse_id'                  => $material_preparation->warehouse,
                                                        'date_movement'                 => $confirm_date,
                                                        'created_at'                    => $confirm_date,
                                                        'updated_at'                    => $confirm_date,
                                                        'date_receive_on_destination'   => $confirm_date,
                                                        'is_active'                     => true,
                                                        'is_integrate'                  => false,
                                                        'note'                          => 'BARANG SUDAH DITERIMA, DAN SISTEM MELAKUKAN PEMOTONGAN STOCK SESUAI DENGAN ALOKASINYA',
                                                        'user_id'                       => Auth::user()->id
                                                    ]);

                                                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                    $material_preparation->save();
                                                }
                    
                                                DetailMaterialPreparation::FirstOrCreate([
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'material_arrival_id'       => $material_arrival_id,
                                                    'user_id'                   => auth::user()->id
                                                ]);

                                                $qty_supply                 -= $_supplied;
                                                $qty_required               -= $_supplied;
                                                $qty_outstanding            -= $_supplied;
                                                $qty_allocated              += $_supplied;
                                                
                                                $old                        = $available_qty;
                                                $new                        = $available_qty - $_supplied;
                                                
                                                if($new <= 0) $new          = '0';
                                                else $new                   = $new;
    
                                                HistoryStock::approved($material_stock_id
                                                ,$new
                                                ,$old
                                                ,$__supplied
                                                ,$new_po_buyer
                                                ,$style
                                                ,$article_no
                                                ,$lc_date
                                                ,$allocation_source
                                                ,$username
                                                ,$user
                                                ,$type_stock_erp_code
                                                ,$type_stock
                                                ,$is_integrate
                                                ,true);
                                                
                                                $available_qty  -= $_supplied;
                                                $reserved_qty   += $_supplied;
    
                                                $is_bom_exists++;
                                            }
                                        }else
                                        {
                                            $is_exists = MaterialPreparation::where([
                                                ['material_stock_id',$material_stock_id],
                                                ['auto_allocation_id',$auto_allocation_id],
                                                ['c_order_id',$c_order_id],
                                                ['c_bpartner_id',$c_bpartner_id],
                                                ['po_buyer',$new_po_buyer],
                                                ['item_id',$item_id_book],
                                                ['uom_conversion',$uom],
                                                ['warehouse',$warehouse_id],
                                                ['style',$style],
                                                ['article_no',$article_no],
                                                ['qty_conversion',$__supplied],
                                                ['is_backlog',false],
                                                ['is_additional',false]
                                            ]);

                                            if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);

                                            $is_exists = $is_exists->exists();
                                            
                                            if(!$is_exists && $_supplied > 0.0000)
                                            {
                                                $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);

                                                $so_id                = PoBuyer::where('po_buyer', $new_po_buyer)->first();
                                                
                                                $material_preparation = MaterialPreparation::FirstOrCreate([
                                                    'material_stock_id'                             => $material_stock_id,
                                                    'auto_allocation_id'                            => $auto_allocation_id,
                                                    'po_detail_id'                                  => $po_detail_id,
                                                    'barcode'                                       => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'referral_code'                                 => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'sequence'                                      => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : -1),
                                                    'item_id'                                       => $item_id_book,
                                                    'c_order_id'                                    => $c_order_id,
                                                    'c_bpartner_id'                                 => $c_bpartner_id,
                                                    'supplier_name'                                 => $supplier_name,
                                                    'document_no'                                   => $document_no,
                                                    'po_buyer'                                      => $new_po_buyer,
                                                    'uom_conversion'                                => $uom,
                                                    'qty_conversion'                                => $__supplied,
                                                    'qty_reconversion'                              => $qty_reconversion,
                                                    'job_order'                                     => $job_order,
                                                    'style'                                         => $style,
                                                    '_style'                                        => $_style,
                                                    'article_no'                                    => $article_no,
                                                    'warehouse'                                     => $warehouse_id,
                                                    'item_code'                                     => $item_code_book,
                                                    'item_desc'                                     => $item_desc,
                                                    'category'                                      => $category,
                                                    'is_backlog'                                    => false,
                                                    'total_carton'                                  => 1,
                                                    'type_po'                                       => 2,
                                                    'user_id'                                       => auth::user()->id,
                                                    'last_status_movement'                          => 'receive',
                                                    'is_need_inserted_to_locator_free_stock_erp'    => $is_need_inserted_to_locator_free_stock_erp,
                                                    'is_need_to_be_switch'                          => false,
                                                    'last_locator_id'                               => $receiving_location->id,
                                                    'last_movement_date'                            => $confirm_date,
                                                    'created_at'                                    => $confirm_date,
                                                    'updated_at'                                    => $confirm_date,
                                                    'first_print_date'                              => null,
                                                    'deleted_at'                                    => null,
                                                    'last_user_movement_id'                         => auth::user()->id,
                                                    'is_stock_already_created'                      => false,
                                                    'is_reduce'                                     => $is_reduce,
                                                    'so_id'                                         => $so_id->so_id
                                                ]);

                                                $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;
                                                
                                                $is_materal_movement_exists = MaterialMovement::where([
                                                    ['from_location',$supplier_location->id],
                                                    ['to_destination',$receiving_location->id],
                                                    ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                    ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                    ['no_packing_list',$no_packing_list],
                                                    ['no_invoice',$no_invoice],
                                                    ['po_buyer',$new_po_buyer],
                                                    ['status','receive'],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                ])
                                                ->first();
                                
                                                if(!$is_materal_movement_exists)
                                                {
                                                    $material_movement = MaterialMovement::firstOrCreate([
                                                        'from_location'         => $supplier_location->i,
                                                        'to_destination'        => $receiving_location->id,
                                                        'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                        'po_buyer'              => $new_po_buyer,
                                                        'no_packing_list'       => $no_packing_list,
                                                        'no_invoice'            => $no_invoice,
                                                        'is_integrate'          => false,
                                                        'is_active'             => true,
                                                        'status'                => 'receive',
                                                        'created_at'            => $confirm_date,
                                                        'updated_at'            => $confirm_date,
                                                    ]);

                                
                                                    $material_movement_id = $material_movement->id;
                                                }else
                                                {
                                                    $is_materal_movement_exists->updated_at = $confirm_date;
                                                    $is_materal_movement_exists->save();
                                
                                                    $material_movement_id = $is_materal_movement_exists->id;
                                                }

                                                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                ->where([
                                                    'material_movement_id'      => $material_movement_id,
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'item_id'                   => $material_preparation->item_id,
                                                    'warehouse_id'              => $material_preparation->warehouse,
                                                    'type_po'                   => 2,
                                                    'date_movement'             => $confirm_date,
                                                    'is_integrate'              => false,
                                                    'is_active'                 => true,
                                                    'qty_movement'              => $material_preparation->qty_conversion,
                                                ])
                                                ->exists();

                                                if(!$is_line_exists)
                                                {
                                                    
                                                    $new_material_movement_line = MaterialMovementLine::FirstOrCreate([
                                                        'material_movement_id'          => $material_movement_id,
                                                        'material_preparation_id'       => $material_preparation->id,
                                                        'item_id'                       => $material_preparation->item_id,
                                                        'item_id_source'                => $material_preparation->item_id_source,
                                                        'c_order_id'                    => $material_preparation->c_order_id,
                                                        'c_orderline_id'                => $c_orderline_id,
                                                        'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                        'supplier_name'                 => $material_preparation->supplier_name,
                                                        'document_no'                   => $material_preparation->document_no,
                                                        'item_code'                     => $material_preparation->item_code,
                                                        'nomor_roll'                    => '-',
                                                        'type_po'                       => '2',
                                                        'uom_movement'                  => $material_preparation->uom_conversion,
                                                        'qty_movement'                  => $material_preparation->qty_conversion,
                                                        'warehouse_id'                  => $material_preparation->warehouse,
                                                        'date_movement'                 => $confirm_date,
                                                        'created_at'                    => $confirm_date,
                                                        'updated_at'                    => $confirm_date,
                                                        'date_receive_on_destination'   => $confirm_date,
                                                        'is_active'                     => true,
                                                        'is_integrate'                  => false,
                                                        'note'                          => 'BARANG SUDAH DITERIMA, DAN SISTEM MELAKUKAN PEMOTONGAN STOCK SESUAI DENGAN ALOKASINYA',
                                                        'user_id'                       => Auth::user()->id
                                                    ]);

                                                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                    $material_preparation->save();
                                                }

                                                DetailMaterialPreparation::FirstOrCreate([
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'material_arrival_id'       => $material_arrival_id,
                                                    'user_id'                   => auth::user()->id
                                                ]);
    
                                                //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
    
                                                $qty_supply             -= $_supplied;
                                                $qty_required           -= $_supplied;
                                                $qty_outstanding        -= $_supplied;
                                                $qty_allocated          += $_supplied;

                                                $old                    = $available_qty;
                                                $new                    = $available_qty - $_supplied;

                                                if($new <= 0) $new = '0';
                                                else $new = $new;
    
                                                HistoryStock::approved($material_stock_id
                                                ,$new
                                                ,$old
                                                ,$__supplied
                                                ,$new_po_buyer
                                                ,$style
                                                ,$article_no
                                                ,$lc_date
                                                ,$allocation_source
                                                ,$username
                                                ,$user
                                                ,$type_stock_erp_code
                                                ,$type_stock
                                                ,$is_integrate
                                                ,true);
                                                
                                                $available_qty  -= $_supplied;
                                                $reserved_qty   += $_supplied;
    
                                                $is_bom_exists++;

                                                $concatenate_preparation .= "'" .$material_preparation->barcode."',";
                                            }
                                        }
                                        
                                    }
                                }
                            }
                            
                            //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                            
                            if($qty_outstanding<=0)
                            {
                                $auto_allocation->is_upload_manual                  = true;
                                $auto_allocation->is_already_generate_form_booking  = true;
                                $auto_allocation->generate_form_booking             = carbon::now();
                            }

                            $recalculate[]                                          = $auto_allocation->id;
                            $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                            $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                            $auto_allocation->save();
                        }
                    }
                    
                }

                if($is_bom_exists > 0)
                {
                    if($available_qty <=0 )
                    {
                        $material_stock->is_allocated                           = true;
                        $material_stock->is_active                              = false;
                    }

                    $material_stock->available_qty                              = sprintf('%0.8f',$available_qty);
                    $material_stock->reserved_qty                               = sprintf('%0.8f',$reserved_qty);
                    
                }

                $material_stock->last_status                                    = null;
                $material_stock->save();
            }

            $concatenate_preparation = substr_replace($concatenate_preparation, '', -1);
            if($concatenate_preparation !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate_preparation."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate_preparation."]);"));
            }


            //recalculate
            foreach ($recalculate as $key => $value) 
            {
                $auto_allocation        = AutoAllocation::find($value);

                if(!$auto_allocation->is_reduce && $auto_allocation->status_po_buyer =='active')
                {
                    if($auto_allocation->is_fabric)
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);

                        $qty_allocated  = AllocationItem::where([
                            ['auto_allocation_id',$value],
                        ])
                        ->sum('qty_booking');

                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));

                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);

                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();

                    }else 
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);

                        $qty_allocated  = MaterialPreparation::where([
                            ['auto_allocation_id',$value],
                            //['last_status_movement','!=','out-handover'],
                            ['last_status_movement','!=','reroute'],
                            ['last_status_movement','!=','adjustment'],
                            ['is_from_handover',false],
                        ])
                        ->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));

                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);

                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();

                        
                    }
                }
                
            }
            
            DB::commit();
        } catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function updateAllocationAccessoriesItems($update_auto_allocations,$spesific_id = null)
    {
        $confirm_date                           = Carbon::now();
        
        $summary_stock_auto_allocations         = AutoAllocation::select('document_no','c_bpartner_id','item_code_source','warehouse_id','type_stock_erp_code')
        ->where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',false],
        ])
        ->whereIn('id',$update_auto_allocations);

        if($spesific_id) 
            $summary_stock_auto_allocations     = $summary_stock_auto_allocations->where('id',$spesific_id);

        $summary_stock_auto_allocations         = $summary_stock_auto_allocations->whereNull('deleted_at')
        ->groupby('document_no','c_bpartner_id','item_code_source','warehouse_id','type_stock_erp_code')
        ->get();

        try 
        {
    		DB::beginTransaction();
            $concatenate                            = '';
            $concatenate_preparation                = '';

            foreach ($summary_stock_auto_allocations as $key => $value) 
            {
                $document_no        = strtoupper($value->document_no);
                $_item_code         = strtoupper($value->item_code_source);

                $pos                = strpos($_item_code, '|');
                if ($pos === false)
                {
                    $item_code      = $_item_code;
                }else 
                {
                    $split          = explode('|',$_item_code);
                    $item_code      = $split[0];
                }
                
                $c_bpartner_id      = $value->c_bpartner_id;
                $warehouse_id       = $value->warehouse_id;
                $concatenate        .= "'" .$document_no.'_'.$item_code.'_'.$c_bpartner_id.'_'.$warehouse_id."'" .',';
            }

            
            $concatenate                            = substr_replace($concatenate, '', -1);
            if($concatenate != '')
            {
                $get_stocks                         = DB::select(db::raw("SELECT *  FROM get_stock_accessories(array[".$concatenate ."]);"));
                
                foreach ($get_stocks as $key => $value)
                {
                    $material_stock_id              = $value->id;
                    $material_stock = MaterialStock::where([
                        ['id',$material_stock_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('last_status')
                    ->whereNotNull('approval_date')
                    ->whereNull('deleted_at')
                    ->first();

                    $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                    ->where('material_stock_id',$material_stock_id)
                    ->exists();

                    if(!$has_outstanding_approval_exists)
                    {
                        if($material_stock)
                        {
                            $material_stock->last_status    = 'prepared';
                            $material_stock->save();
    
                            $document_no                    = strtoupper($material_stock->document_no);
                            $item_code_source               = strtoupper($material_stock->item_code);
                            $po_detail_id                   = $material_stock->po_detail_id;
                            $po_buyer_source                = $material_stock->po_buyer;
                            $type_stock_erp_code            = $material_stock->type_stock_erp_code;
                            $type_stock                     = $material_stock->type_stock;
                            $uom_source                     = ( $material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);
                            $c_order_id                     = $material_stock->c_order_id;
                            $item_id                        = $material_stock->item_id;
                            $supplier_name                  = strtoupper($material_stock->supplier_name);
                            $c_bpartner_id                  = $material_stock->c_bpartner_id;
                            $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                            $supplier_code                  = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                            $is_running_stock               = $material_stock->is_running_stock;
                            $warehouse_id                   = $value->warehouse_id;
                            $is_material_others             = $material_stock->is_material_others;
                            $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
                            $available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
                            $is_bom_exists                  = 0;
                            $material_arrival           = MaterialArrival::where([
                                ['po_detail_id',$po_detail_id],
                                ['warehouse_id',$warehouse_id],
                            ])
                            ->first();

                            $allocation_stock                       = Locator::with('area')
                            ->whereHas('area',function ($query) use ($warehouse_id)
                            {
                                $query->where('is_destination',false);
                                $query->where('is_active',true);
                                $query->where('warehouse',$warehouse_id);
                                $query->where('name','FREE STOCK');
                            })
                            ->where('rack','ALLOCATION STOCK')
                            ->first();
    
                            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                                $c_orderline_id         = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->where('po_detail_id',$po_detail_id)
                                ->first();

                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            }

                            if($is_material_others)
                            {
                                if($c_order_id == 'FREE STOCK')
                                {
                                    $is_need_inserted_to_locator_free_stock_erp = true;
                                    $is_need_inserted_to_locator_inventory_erp  = true;
                                }
                                else
                                {
                                    $is_need_inserted_to_locator_free_stock_erp = false;
                                    $is_need_inserted_to_locator_inventory_erp  = true;
                                }
                            }else
                            {
                                $is_need_inserted_to_locator_free_stock_erp = false;
                                $is_need_inserted_to_locator_inventory_erp  = false;
                            }
                            
                            if (strpos($document_no, "POAR") !== false) $is_poar = true;
                            else $is_poar = false;
    
                            $prepared_status            = ($material_arrival ?$material_arrival->prepared_status : null);
                            $material_arrival_id        = ($material_arrival ? $material_arrival->id : null);
                            $is_moq                     = ($material_arrival ?$material_arrival->is_moq : null);
                            $po_buyer                   = ($material_arrival ?$material_arrival->po_buyer : null);
    
                            $system                         = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();
    
                            $conversion = UomConversion::where([
                                ['item_id',$item_id],
                                ['uom_from',$uom_source]
                            ])
                            ->first();
                        
                            if($conversion)
                            {
                                $multiplyrate   = $conversion->multiplyrate;
                                $dividerate     = $conversion->dividerate;
                            }else
                            {
                                $dividerate     = 1;
                                $multiplyrate   = 1;
                            }
                            
                            $auto_allocations               = AutoAllocation::where([
                                ['c_order_id',$c_order_id],
                                ['item_id_source',$item_id],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['warehouse_id',$warehouse_id],
                                ['qty_outstanding','>',0],
                                ['status_po_buyer','active'],
                                ['is_fabric',false],
                                //['is_allocation_purchase',false],
                            ]);
                            
                            if($is_running_stock) $auto_allocations = $auto_allocations->where('is_allocation_purchase',true);
                            else $auto_allocations = $auto_allocations->where('is_allocation_purchase',false);
    
                            //if($type_stock_erp_code) $auto_allocations = $auto_allocations->where('type_stock_erp_code',$type_stock_erp_code);
                            if($spesific_id) 
                                $auto_allocations           = $auto_allocations->where('id',$spesific_id);
    
                            $auto_allocations               = $auto_allocations->whereNull('deleted_at')
                            ->orderby('promise_date','asc')
                            ->orderby('qty_allocation','asc')
                            ->get();
                                
                            if($available_qty > 0)
                            {
                                //echo $available_qty.'<br/>';
                                foreach ($auto_allocations as $key_2 => $auto_allocation) 
                                {
                                    $auto_allocation_id     = $auto_allocation->id;
                                    $is_additional          = $auto_allocation->is_additional;
                                    $remark_additional      = $auto_allocation->remark_additional;
                                    $_item_code_book        = $auto_allocation->item_code;
                                    $c_order_id             = $auto_allocation->c_order_id;
                                    $item_id_book           = $auto_allocation->item_id_book;
                                    $item_id_source         = $auto_allocation->item_id_source;
    
                                    if($item_id_book != $item_id_source) $_item_id_source = $item_id_source;
                                    else $_item_id_source = null;

                                    if($auto_allocation->is_upload_manual)
                                    {
                                        $allocation_source  = 'ALLOCATION MANUAL';
                                        $user               = $auto_allocation->user_id;
                                        $username           = strtoupper($auto_allocation->user->name);
                                        $is_integrate       = false;
                                    }else
                                    {
                                        $allocation_source  = ($auto_allocation->is_allocation_purchase) ? 'ALLOCATION PURCHASE':'ALLOCATION ERP';
                                        $user               = $system->id;
                                        $username           = strtoupper($auto_allocation->user->name);
                                        
                                        //if($auto_allocation->is_allocation_purchase)  $is_integrate = true;
                                        //else $is_integrate = true;
    
                                        $is_integrate       = true;
                                    }
    
                                    $pos                    = strpos($_item_code_book, '|');
                                    if ($pos === false)
                                    {
                                        $item_code_book     = $_item_code_book;
                                        $article_no         = null;
                                    }else 
                                    {
                                        $split              = explode('|',$_item_code_book);
                                        $item_code_book     = $split[0];
                                        $article_no         = $split[1];
                                    }
    
                                    $new_po_buyer           = $auto_allocation->po_buyer;
                                    $old_po_buyer           = $auto_allocation->old_po_buyer;
                                    $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                                    $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                                    
                                    $_po_buyer              = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                                    
                                    $material_requirements  = MaterialRequirement::where([
                                        ['po_buyer',$new_po_buyer],
                                        ['item_id',$item_id_book],
                                    ]);

                                    if($article_no)  $material_requirements  = $material_requirements->where('article_no',$article_no);
                                        
                                    $material_requirements      = $material_requirements->orderby('article_no','asc')
                                    ->orderby('style','asc')
                                    ->get();
                                    
                                    $count_mr = count($material_requirements);
                                    
                                    if($count_mr > 0 && $qty_outstanding > 0)
                                    {
                                        if ($available_qty/$qty_outstanding >= 1) $_supply = $qty_outstanding;
                                        else $_supply   = $available_qty;
    
                                        $qty_supply     = $_supply;
    
                                        if($qty_outstanding > 0.0000)
                                        {
                                            foreach ($material_requirements as $key => $material_requirement) 
                                            {
                                                $lc_date                = $material_requirement->lc_date;
                                                $item_desc              = strtoupper($material_requirement->item_desc);
                                                $uom                    = $material_requirement->uom;
                                                $category               = $material_requirement->category;
                                                $style                  = $material_requirement->style;
                                                $job_order              = $material_requirement->job_order;
                                                $article_no             = $material_requirement->article_no;
                                                $_style                 = explode('::',$job_order)[0];
                                                $qty_required_allocated = $material_requirement->qtyAllocatedAllocation($new_po_buyer,$item_code,$style,$article_no);
                                               
                                                if($is_additional) $qty_required    = 1;
                                                else  $qty_required                 = sprintf('%0.8f',$material_requirement->qty_required - $qty_required_allocated);
                                        
                                                //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                                //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                                
                                                //echo $po_buyer.' '.$item_code.' '.$qty_supply.' '.$qty_required.'</br>';
                                                if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                                                else $__supplied = sprintf('%0.8f',$qty_supply);
            
                                                /*echo $new_po_buyer
                                                .' '.$qty_supply
                                                .' '.$style
                                                .' QTY REQUIRED '.$material_requirement->qty_required
                                                .' QTY ALLOCATED '.$qty_required_allocated
                                                .' QTY SUPPLIED '.$__supplied
                                                .'<br/>'
                                                .'<br/>';*/
    
                                                if($__supplied > 0.0000 && $qty_supply > 0.0000)
                                                {
                                                    if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                                    else $_supplied = $qty_supply;
            
                                                    $__supplied = sprintf('%0.8f',$_supplied);
                                                    
                                                    if($is_additional)
                                                    {
                                                        if($_supplied > 0.0000)
                                                        {
                                                            
                                                            //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                                            $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);
                                                            $material_preparation = MaterialPreparation::Create([
                                                                'material_stock_id'                                 => $material_stock_id,
                                                                'auto_allocation_id'                                => $auto_allocation_id,
                                                                'po_detail_id'                                      => $po_detail_id,
                                                                'barcode'                                           => 'BELUM DI PRINT',
                                                                'referral_code'                                     => 'BELUM DI PRINT',
                                                                'sequence'                                          => 0,
                                                                'item_id'                                           => $item_id_book,
                                                                'item_id_source'                                    => $_item_id_source,
                                                                'c_order_id'                                        => $c_order_id,
                                                                'c_bpartner_id'                                     => $c_bpartner_id,
                                                                'supplier_name'                                     => $supplier_name,
                                                                'document_no'                                       => $document_no,
                                                                'po_buyer'                                          => $new_po_buyer,
                                                                'uom_conversion'                                    => $uom,
                                                                'qty_conversion'                                    => $__supplied,
                                                                'qty_reconversion'                                  => $qty_reconversion,
                                                                'job_order'                                         => $job_order,
                                                                'style'                                             => $style,
                                                                '_style'                                            => $_style,
                                                                'article_no'                                        => $article_no,
                                                                'warehouse'                                         => $warehouse_id,
                                                                'item_code'                                         => $item_code_book,
                                                                'item_desc'                                         => $item_desc,
                                                                'category'                                          => $category,
                                                                'is_additional'                                     => true,
                                                                'is_backlog'                                        => false,
                                                                'total_carton'                                      => 1,
                                                                'type_po'                                           => 2,
                                                                'user_id'                                           => auth::user()->id,
                                                                'last_status_movement'                              => 'allocation',
                                                                'is_need_inserted_to_locator_free_stock_erp'        => $is_need_inserted_to_locator_free_stock_erp,
                                                                'is_need_inserted_to_locator_inventory_erp'         => $is_need_inserted_to_locator_inventory_erp,
                                                                'is_need_to_be_switch'                              => false,
                                                                'last_locator_id'                                   => $allocation_stock->id,
                                                                'last_movement_date'                                => $confirm_date,
                                                                'created_at'                                        => $confirm_date,
                                                                'updated_at'                                        => $confirm_date,
                                                                'first_print_date'                                  => null,
                                                                'deleted_at'                                        => null,
                                                                'last_user_movement_id'                             => auth::user()->id,
                                                                'is_stock_already_created'                          => false
                                                            ]);
        
                                                            $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;
                                                            
                                                            $is_movement_header_exists = MaterialMovement::where([
                                                                ['from_location',$allocation_stock->id],
                                                                ['to_destination',$allocation_stock->area->erp_id],
                                                                ['from_locator_erp_id',$allocation_stock->id],
                                                                ['to_locator_erp_id',$allocation_stock->area->erp_id],
                                                                ['po_buyer',$material_preparation->po_buyer],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                                ['no_packing_list',$no_packing_list],
                                                                ['no_invoice',$no_invoice],
                                                                ['status','allocation'],
                                                            ])
                                                            ->first();
                                                            
                                                            if(!$is_movement_header_exists)
                                                            {
                                                                $material_movement = MaterialMovement::firstOrCreate([
                                                                    'from_location'         => $allocation_stock->id,
                                                                    'to_destination'        => $allocation_stock->id,
                                                                    'from_locator_erp_id'   => $allocation_stock->area->erp_id,
                                                                    'to_locator_erp_id'     => $allocation_stock->area->erp_id,
                                                                    'po_buyer'              => $material_preparation->po_buyer,
                                                                    'is_integrate'          => false,
                                                                    'is_active'             => true,
                                                                    'no_packing_list'       => $no_packing_list,
                                                                    'no_invoice'            => $no_invoice,
                                                                    'status'                => 'allocation',
                                                                    'created_at'            => $confirm_date,
                                                                    'updated_at'            => $confirm_date,
                                                                ]);
                                        
                                                                $material_movement_id = $material_movement->id;
                                                            }else
                                                            {
                                                                $is_movement_header_exists->updated_at = $confirm_date;
                                                                $is_movement_header_exists->save();
                                        
                                                                $material_movement_id = $is_movement_header_exists->id;
                                                            }

                                                            $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                            ->where([
                                                                ['material_movement_id',$material_movement_id],
                                                                ['material_preparation_id',$material_preparation->id],
                                                                ['qty_movement',sprintf('%0.8f',$material_preparation->qty_conversion)],
                                                                ['date_movement',$material_preparation->last_movement_date],
                                                            ]) 
                                                            ->exists();
                            
                                                            if(!$is_material_movement_line_exists)
                                                            {
                                                                $new_material_movement_line = MaterialMovementLine::Create([
                                                                    'material_movement_id'          => $material_movement_id,
                                                                    'material_preparation_id'       => $material_preparation->id,
                                                                    'item_code'                     => $material_preparation->item_code,
                                                                    'item_id'                       => $material_preparation->item_id,
                                                                    'item_id_source'                => $material_preparation->item_id_source,
                                                                    'c_order_id'                    => $material_preparation->c_order_id,
                                                                    'c_orderline_id'                => $c_orderline_id,
                                                                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                                    'supplier_name'                 => $material_preparation->supplier_name,
                                                                    'type_po'                       => 2,
                                                                    'is_integrate'                  => false,
                                                                    'uom_movement'                  => $material_preparation->uom_conversion,
                                                                    'qty_movement'                  => sprintf('%0.8f',$material_preparation->qty_conversion),
                                                                    'date_movement'                 => $material_preparation->last_movement_date,
                                                                    'created_at'                    => $material_preparation->last_movement_date,
                                                                    'updated_at'                    => $material_preparation->last_movement_date,
                                                                    'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                                                    'nomor_roll'                    => '-',
                                                                    'warehouse_id'                  => $material_preparation->warehouse,
                                                                    'document_no'                   => $material_preparation->document_no,
                                                                    'is_active'                     => true,
                                                                    'user_id'                       => $material_preparation->last_user_movement_id,
                                                                ]);

                                                                $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                                $material_preparation->save();
                                                            }

                                                            DetailMaterialPreparation::FirstOrCreate([
                                                                'material_preparation_id'   => $material_preparation->id,
                                                                'material_arrival_id'       => $material_arrival_id,
                                                                'user_id'                   => auth::user()->id
                                                            ]);
    
                                                            $qty_supply                 -= $_supplied;
                                                            $qty_required               -= $_supplied;
                                                            $qty_outstanding            -= $_supplied;
                                                            $qty_allocated              += $_supplied;
                                                            
                                                            $old                        = $available_qty;
                                                            $new                        = $available_qty - $_supplied;
                                                            
                                                            if($new <= 0) $new          = '0';
                                                            else $new                   = $new;
                
                                                            HistoryStock::approved($material_stock_id
                                                            ,$new
                                                            ,$old
                                                            ,$__supplied
                                                            ,$new_po_buyer
                                                            ,$style
                                                            ,$article_no
                                                            ,$lc_date
                                                            ,$allocation_source
                                                            ,$username
                                                            ,$user
                                                            ,$type_stock_erp_code
                                                            ,$type_stock
                                                            ,$is_integrate
                                                            ,true);
                                                            
                                                            $available_qty  -= $_supplied;
                                                            $reserved_qty   += $_supplied;
                
                                                            $is_bom_exists++;
                                                        }
                                                    }else
                                                    {
                                                        $is_exists = MaterialPreparation::where([
                                                            ['material_stock_id',$material_stock_id],
                                                            ['auto_allocation_id',$auto_allocation_id],
                                                            ['c_order_id',$c_order_id],
                                                            ['c_bpartner_id',$c_bpartner_id],
                                                            ['po_buyer',$new_po_buyer],
                                                            ['item_id',$item_id_book],
                                                            ['uom_conversion',$uom],
                                                            ['warehouse',$warehouse_id],
                                                            ['style',$style],
                                                            ['article_no',$article_no],
                                                            ['qty_conversion',$__supplied],
                                                            ['is_backlog',false],
                                                            ['is_additional',false]
                                                        ]);
        
                                                        if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
        
                                                        $is_exists = $is_exists->exists();
                                                        
                                                        if(!$is_exists && $_supplied > 0.0000)
                                                        {
                                                            $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);

                                                            $so_id                  = PoBuyer::where('po_buyer', $new_po_buyer)->first();

                                                            $material_preparation = MaterialPreparation::FirstOrCreate([
                                                                'material_stock_id'                                 => $material_stock_id,
                                                                'auto_allocation_id'                                => $auto_allocation_id,
                                                                'po_detail_id'                                      => $po_detail_id,
                                                                'barcode'                                           => 'BELUM DI PRINT',
                                                                'referral_code'                                     => 'BELUM DI PRINT',
                                                                'sequence'                                          => 0,
                                                                'item_id'                                           => $item_id_book,
                                                                'item_id_source'                                    => $_item_id_source,
                                                                'c_order_id'                                        => $c_order_id,
                                                                'c_bpartner_id'                                     => $c_bpartner_id,
                                                                'supplier_name'                                     => $supplier_name,
                                                                'document_no'                                       => $document_no,
                                                                'po_buyer'                                          => $new_po_buyer,
                                                                'uom_conversion'                                    => $uom,
                                                                'qty_conversion'                                    => $__supplied,
                                                                'qty_reconversion'                                  => $qty_reconversion,
                                                                'job_order'                                         => $job_order,
                                                                'style'                                             => $style,
                                                                '_style'                                            => $_style,
                                                                'article_no'                                        => $article_no,
                                                                'warehouse'                                         => $warehouse_id,
                                                                'item_code'                                         => $item_code_book,
                                                                'item_desc'                                         => $item_desc,
                                                                'category'                                          => $category,
                                                                'is_backlog'                                        => false,
                                                                'total_carton'                                      => 1,
                                                                'type_po'                                           => 2,
                                                                'user_id'                                           => auth::user()->id,
                                                                'last_status_movement'                              => 'allocation',
                                                                'is_need_inserted_to_locator_free_stock_erp'        => $is_need_inserted_to_locator_free_stock_erp,
                                                                'is_need_inserted_to_locator_inventory_erp'         => $is_need_inserted_to_locator_inventory_erp,
                                                                'is_need_to_be_switch'                              => false,
                                                                'last_locator_id'                                   => $allocation_stock->id,
                                                                'last_movement_date'                                => $confirm_date,
                                                                'created_at'                                        => $confirm_date,
                                                                'updated_at'                                        => $confirm_date,
                                                                'first_print_date'                                  => null,
                                                                'deleted_at'                                        => null,
                                                                'last_user_movement_id'                             => auth::user()->id,
                                                                'is_stock_already_created'                          => false,
                                                                'so_id'                                             => $so_id->so_id
                                                            ]);
        
                                                            $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;
                                                            
                                                            $is_movement_header_exists = MaterialMovement::where([
                                                                ['from_location',$allocation_stock->id],
                                                                ['to_destination',$allocation_stock->area->erp_id],
                                                                ['from_locator_erp_id',$allocation_stock->id],
                                                                ['to_locator_erp_id',$allocation_stock->area->erp_id],
                                                                ['po_buyer',$material_preparation->po_buyer],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                                ['no_packing_list',$no_packing_list],
                                                                ['no_invoice',$no_invoice],
                                                                ['status','allocation'],
                                                            ])
                                                            ->first();
                                                            
                                                            if(!$is_movement_header_exists)
                                                            {
                                                                $material_movement = MaterialMovement::firstOrCreate([
                                                                    'from_location'         => $allocation_stock->id,
                                                                    'to_destination'        => $allocation_stock->id,
                                                                    'from_locator_erp_id'   => $allocation_stock->area->erp_id,
                                                                    'to_locator_erp_id'     => $allocation_stock->area->erp_id,
                                                                    'po_buyer'              => $material_preparation->po_buyer,
                                                                    'is_integrate'          => false,
                                                                    'is_active'             => true,
                                                                    'no_packing_list'       => $no_packing_list,
                                                                    'no_invoice'            => $no_invoice,
                                                                    'status'                => 'allocation',
                                                                    'created_at'            => $confirm_date,
                                                                    'updated_at'            => $confirm_date,
                                                                ]);
                                        
                                                                $material_movement_id = $material_movement->id;
                                                            }else
                                                            {
                                                                $is_movement_header_exists->updated_at = $confirm_date;
                                                                $is_movement_header_exists->save();
                                        
                                                                $material_movement_id = $is_movement_header_exists->id;
                                                            }

                                                            $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                            ->where([
                                                                ['material_movement_id',$material_movement_id],
                                                                ['material_preparation_id',$material_preparation->id],
                                                                ['qty_movement',sprintf('%0.8f',$material_preparation->qty_conversion)],
                                                                ['date_movement',$material_preparation->last_movement_date],
                                                            ]) 
                                                            ->exists();
                            
                                                            if(!$is_material_movement_line_exists)
                                                            {
                                                                $new_material_movement_line = MaterialMovementLine::Create([
                                                                    'material_movement_id'          => $material_movement_id,
                                                                    'material_preparation_id'       => $material_preparation->id,
                                                                    'item_code'                     => $material_preparation->item_code,
                                                                    'item_id'                       => $material_preparation->item_id,
                                                                    'item_id_source'                => $material_preparation->item_id_source,
                                                                    'c_order_id'                    => $material_preparation->c_order_id,
                                                                    'c_orderline_id'                => $c_orderline_id,
                                                                    'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                                    'supplier_name'                 => $material_preparation->supplier_name,
                                                                    'type_po'                       => 2,
                                                                    'is_integrate'                  => false,
                                                                    'uom_movement'                  => $material_preparation->uom_conversion,
                                                                    'qty_movement'                  => sprintf('%0.8f',$material_preparation->qty_conversion),
                                                                    'date_movement'                 => $material_preparation->last_movement_date,
                                                                    'created_at'                    => $material_preparation->last_movement_date,
                                                                    'updated_at'                    => $material_preparation->last_movement_date,
                                                                    'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                                                    'nomor_roll'                    => '-',
                                                                    'warehouse_id'                  => $material_preparation->warehouse,
                                                                    'document_no'                   => $material_preparation->document_no,
                                                                    'is_active'                     => true,
                                                                    'user_id'                       => $material_preparation->last_user_movement_id,
                                                                ]);

                                                                $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                                $material_preparation->save();
                                                            }
        
                                                            DetailMaterialPreparation::FirstOrCreate([
                                                                'material_preparation_id'   => $material_preparation->id,
                                                                'material_arrival_id'       => $material_arrival_id,
                                                                'user_id'                   => auth::user()->id
                                                            ]);
                
                                                            //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                
                                                            $qty_supply             -= $_supplied;
                                                            $qty_required           -= $_supplied;
                                                            $qty_outstanding        -= $_supplied;
                                                            $qty_allocated          += $_supplied;
    
                                                            $old                    = $available_qty;
                                                            $new                    = $available_qty - $_supplied;
    
                                                            if($new <= 0) $new = '0';
                                                            else $new = $new;
                
                                                            HistoryStock::approved($material_stock_id
                                                            ,$new
                                                            ,$old
                                                            ,$__supplied
                                                            ,$new_po_buyer
                                                            ,$style
                                                            ,$article_no
                                                            ,$lc_date
                                                            ,$allocation_source
                                                            ,$username
                                                            ,$user
                                                            ,$type_stock_erp_code
                                                            ,$type_stock
                                                            ,$is_integrate
                                                            ,true);
                                                            
                                                            $available_qty  -= $_supplied;
                                                            $reserved_qty   += $_supplied;
                
                                                            $is_bom_exists++;
    
                                                            $concatenate_preparation .= "'" .$material_preparation->barcode."',";
                                                        }
                                                    }
                                                    
                                                }
                                            }
                                        }
                                        
                                        //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                                        
                                        if($qty_outstanding<=0)
                                        {
                                            $auto_allocation->is_upload_manual                  = true;
                                            $auto_allocation->is_already_generate_form_booking  = true;
                                            $auto_allocation->generate_form_booking             = carbon::now();
                                        }
            
                                        $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                                        $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                                        $auto_allocation->save();
                                    }
                                }
                                
                            }
    
                            if($is_bom_exists > 0)
                            {
                                if($available_qty <=0 )
                                {
                                    $material_stock->is_allocated                           = true;
                                    $material_stock->is_active                              = false;
                                }
    
                                $material_stock->available_qty                              = sprintf('%0.8f',$available_qty);
                                $material_stock->reserved_qty                               = sprintf('%0.8f',$reserved_qty);
                                
                            }
    
                            $material_stock->last_status                                    = null;
                            $material_stock->save();
                        }
                    }

                    
                   
                }
            }

            $concatenate_preparation = substr_replace($concatenate_preparation, '', -1);
            if($concatenate_preparation !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate_preparation."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate_preparation."]);"));
            }

            DB::commit();
        } catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function insertAllocationAccessoriesItems()
    {
        //return view('errors.503');
        $confirm_date                   = Carbon::now();
        $summary_stock_auto_allocations = AutoAllocation::select('document_no','c_bpartner_id','item_code_source','warehouse_id','type_stock_erp_code')
        ->where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',false],
           ])
        ->whereNull('deleted_at')
        ->groupby('document_no','c_bpartner_id','item_code_source','warehouse_id','type_stock_erp_code')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            $concatenate = '';
            foreach ($summary_stock_auto_allocations as $key => $value) 
            {
                $document_no        = strtoupper($value->document_no);
                $item_code          = strtoupper($value->item_code_source);
                $c_bpartner_id      = $value->c_bpartner_id;
                $warehouse_id       = $value->warehouse_id;
                $concatenate        .= "'" .$document_no.'_'.$item_code.'_'.$c_bpartner_id.'_'.$warehouse_id."'" .',';
                //$concatenate .= "'" .$document_no.'_'.$item_code.'_'.$c_bpartner_id.'_'.$warehouse_id.'_'.$type_stock_erp_code."'" .',';
            }

            $concatenate             = substr_replace($concatenate, '', -1);
            if($concatenate != '')
            {
                $get_stocks           = DB::select(db::raw("SELECT *  FROM get_stock_accessories(array[".$concatenate ."]);"));
                
                foreach ($get_stocks as $key => $value) 
                {
                    $material_stock_id  = $value->id;
                    $material_stock     = MaterialStock::where([
                        ['id',$material_stock_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                        ['is_running_stock',false],
                    ])
                    ->whereNull('last_status')
                    ->whereNotNull('approval_date')
                    ->whereNull('deleted_at')
                    ->first();

                    $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                    ->where('material_stock_id',$material_stock_id)
                    ->exists();

                    if(!$has_outstanding_approval_exists)
                    {
                        if($material_stock)
                        {
                            $material_stock->last_status = 'prepared';
                            $material_stock->save();

                            $document_no            = strtoupper($material_stock->document_no);
                            $item_code_source       = strtoupper($material_stock->item_code);
                            $po_buyer_source        = $material_stock->po_buyer;
                            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
                            $type_stock             = $material_stock->type_stock;
                            $po_detail_id           = $material_stock->po_detail_id;
                            $uom_source             = ($material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);
                            $c_bpartner_id          = $material_stock->c_bpartner_id;
                            $uom_stock              = ( $material_stock->uom_stock ? $material_stock->uom_stock : $material_stock->uom);
                            $supplier_name          = strtoupper($material_stock->supplier_name);
                            $_supplier              = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                            $supplier_code          = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                            $warehouse_id           = $value->warehouse_id;
                            $reserved_qty           = sprintf('%0.8f',$material_stock->reserved_qty);
                            $available_qty          = sprintf('%0.8f',$material_stock->available_qty);
                            $is_bom_exists          = 0;

                            $c_order_id             = $material_stock->c_order_id;
                            $is_material_others     = $material_stock->is_material_others;
                        
                            if($is_material_others)
                            {
                                if($c_order_id == 'FREE STOCK')
                                {
                                    $is_need_inserted_to_locator_free_stock_erp = true;
                                    $is_need_inserted_to_locator_inventory_erp  = true;
                                }
                                else
                                {
                                    $is_need_inserted_to_locator_free_stock_erp = false;
                                    $is_need_inserted_to_locator_inventory_erp  = true;
                                }
                            }else
                            {
                                $is_need_inserted_to_locator_free_stock_erp = false;
                                $is_need_inserted_to_locator_inventory_erp  = false;
                            }

                            $material_arrival       = MaterialArrival::where([
                                ['po_detail_id',$po_detail_id],
                                ['warehouse_id',$warehouse_id],
                            ])
                            ->whereNull('material_subcont_id')
                            ->first();

                            if (strpos($document_no, "POAR") !== false) $is_poar = true;
                            else $is_poar = false;
        
                            $prepared_status            = ($material_arrival ?$material_arrival->prepared_status : null);
                            $material_arrival_id        = ($material_arrival ? $material_arrival->id : null);
                            $is_moq                     = ($material_arrival ?$material_arrival->is_moq : null);
                            $po_buyer                   = ($material_arrival ?$material_arrival->po_buyer : null);

                            $allocation_stock                       = Locator::with('area')
                            ->whereHas('area',function ($query) use($warehouse_id)
                            {
                                $query->where('is_destination',false);
                                $query->where('is_active',true);
                                $query->where('warehouse',$warehouse_id);
                                $query->where('name','FREE STOCK');
                            })
                            ->where('rack','ALLOCATION STOCK')
                            ->first();
                            
                            $system = User::where([
                                ['name','system'],
                                ['warehouse',$warehouse_id]
                            ])
                            ->first();

                            $conversion = UomConversion::where([
                                ['item_code',$item_code],
                                ['uom_from',$uom_source]
                            ])
                            ->first();
                            
                        
                            if($conversion)
                            {
                                $multiplyrate   = $conversion->multiplyrate;
                                $dividerate     = $conversion->dividerate;
                            }else
                            {
                                $dividerate     = 1;
                                $multiplyrate   = 1;
                            }
                            
                            $auto_allocations = AutoAllocation::where([
                                [db::raw('upper(document_no)'),$document_no],
                                [db::raw('upper(item_code_source)'),$item_code_source],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['warehouse_id',$warehouse_id],
                                ['qty_outstanding','>',0],
                                ['is_additional',false],
                                ['is_fabric',false],
                                ['status_po_buyer','active'],
                                ['is_allocation_purchase',false],
                            ]);

                            //if($type_stock_erp_code) $auto_allocations = $auto_allocations->where('type_stock_erp_code',$type_stock_erp_code);
                            $auto_allocations = $auto_allocations->whereNull('deleted_at')
                            ->orderby('promise_date','asc')
                            ->orderby('qty_allocation','asc')
                            ->get();
                                
                            if($available_qty > 0)
                            {
                                //echo $available_qty.'<br/>';
                                foreach ($auto_allocations as $key_2 => $auto_allocation) 
                                {
                                    if($auto_allocation->is_upload_manual)
                                    {
                                        $allocation_source  = 'ALLOCATION MANUAL';
                                        $user               = $auto_allocation->user_id;
                                        $username           = strtoupper($auto_allocation->user->name);
                                        $is_integrate       = false;
                                    }else{
                                        if($auto_allocation->is_allocation_purchase) $allocation_source = 'ALLOCATION PURCHASE';
                                        else $allocation_source = 'ALLOCATION ERP';
                                        
                                        $user           = $system->id;
                                        $username       = strtoupper($auto_allocation->user->name);

                                        //if($auto_allocation->is_allocation_purchase)  $is_integrate = false;
                                        //else $is_integrate = true;
                                        $is_integrate   = true;
                                    }

                                    $_item_code_book = $auto_allocation->item_code;

                                    $pos = strpos($_item_code_book, '|');
                                    if ($pos === false)
                                    {
                                        $item_code_book = $_item_code_book;
                                        $article_no     = null;
                                    }else 
                                    {
                                        $split          = explode('|',$_item_code_book);
                                        $item_code_book = $split[0];
                                        $article_no     = $split[1];
                                    }

                                    $auto_allocation_id = $auto_allocation->id;
                                    $c_order_id         = $auto_allocation->c_order_id;
                                    $item_id_book       = $auto_allocation->item_id_book;
                                    $item_id_source     = $auto_allocation->item_id_source;
                                    $new_po_buyer       = $auto_allocation->po_buyer;
                                    $old_po_buyer       = $auto_allocation->old_po_buyer;
                                    $qty_outstanding    = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                                    $qty_allocated      = sprintf('%0.8f',$auto_allocation->qty_allocated);
                                
                                    $_po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                                    $material_requirements = MaterialRequirement::where([
                                        ['po_buyer',$new_po_buyer],
                                        [db::raw('upper(item_code)'),$item_code_book],
                                    ]);

                                    if($article_no) $material_requirements = $material_requirements->where('article_no',$article_no);
                                    
                                    $material_requirements = $material_requirements->orderby('article_no','asc')
                                    ->orderby('style','asc')
                                    ->get();
                                    
                                    $count_mr = $material_requirements->count();
                                        
                                    if($count_mr > 0 && $qty_outstanding > 0)
                                    {
                                        if ($available_qty/$qty_outstanding >= 1) $_supply = $qty_outstanding;
                                        else $_supply = $available_qty;

                                        $qty_supply = $_supply;

                                        if($qty_outstanding > 0)
                                        {
                                            foreach ($material_requirements as $key => $material_requirement) 
                                            {
                                                $lc_date        = $material_requirement->lc_date;
                                                $item_desc      = strtoupper($material_requirement->item_desc);
                                                $uom            = $material_requirement->uom;
                                                $category       = $material_requirement->category;
                                                $style          = $material_requirement->style;
                                                $job_order      = $material_requirement->job_order;
                                                $_style         = explode('::',$job_order)[0];
                                                $article_no     = $material_requirement->article_no;
                                                $qty_required   = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocatedAllocation($new_po_buyer,$item_code,$style,$article_no));
                                        
            
                                                //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                                //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                    
                                                //echo $po_buyer.' '.$item_code.' '.$is_exists.' '.$qty_supply.' '.$qty_required.'</br>';
                                                
                                                if(($key+1) != $count_mr) $__supplied   = sprintf('%0.8f',$qty_required);
                                                else $__supplied                        = sprintf('%0.8f',$qty_supply);
            
                                                if($__supplied > 0.0000 && $qty_supply > 0.0000)
                                                {
                                                    if ($qty_supply/$__supplied >= 1) $_supplied    = $__supplied;
                                                    else $_supplied                                 = $qty_supply;
            
                                                    $__supplied                                     = sprintf('%0.8f',$_supplied);
                                                
            
                                                    $is_exists = MaterialPreparation::where([
                                                        ['material_stock_id',$material_stock_id],
                                                        ['auto_allocation_id',$auto_allocation_id],
                                                        ['c_order_id',$c_order_id],
                                                        ['c_bpartner_id',$c_bpartner_id],
                                                        ['po_buyer',$new_po_buyer],
                                                        ['item_id',$item_id_book],
                                                        ['uom_conversion',$uom_stock],
                                                        ['warehouse',$warehouse_id],
                                                        ['style',$style],
                                                        ['article_no',$article_no],
                                                        ['qty_conversion',$__supplied],
                                                        ['is_backlog',false],
                                                        ['is_additional',false],
                                                    ]);

                                                    if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);

                                                    $is_exists = $is_exists->exists();

                                                    if(!$is_exists && $_supplied > 0.0000)
                                                    {
                                                    
                                                        $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);

                                                        $so_id                  = PoBuyer::where('po_buyer', $po_buyer)->first();

                                                        $material_preparation = MaterialPreparation::FirstOrCreate([
                                                            'material_stock_id'                                 => $material_stock_id,
                                                            'auto_allocation_id'                                => $auto_allocation_id,
                                                            'po_detail_id'                                      => $po_detail_id,
                                                            'barcode'                                           => 'BELUM DI PRINT',
                                                            'referral_code'                                     => 'BELUM DI PRINT',
                                                            'sequence'                                          => 0,
                                                            'item_id'                                           => $item_id_book,
                                                            'c_order_id'                                        => $c_order_id,
                                                            'c_bpartner_id'                                     => $c_bpartner_id,
                                                            'supplier_name'                                     => $supplier_name,
                                                            'document_no'                                       => $document_no,
                                                            'po_buyer'                                          => $new_po_buyer,
                                                            'uom_conversion'                                    => $uom,
                                                            'qty_conversion'                                    => $__supplied,
                                                            'qty_reconversion'                                  => $qty_reconversion,
                                                            'job_order'                                         => $job_order,
                                                            'style'                                             => $style,
                                                            '_style'                                            => $_style,
                                                            'article_no'                                        => $article_no,
                                                            'warehouse'                                         => $warehouse_id,
                                                            'item_code'                                         => $item_code_book,
                                                            'item_desc'                                         => $item_desc,
                                                            'category'                                          => $category,
                                                            'is_backlog'                                        => false,
                                                            'total_carton'                                      => 1,
                                                            'type_po'                                           => 2,
                                                            'user_id'                                           => $user,
                                                            'last_status_movement'                              => 'allocation',
                                                            'is_need_to_be_switch'                              => false,
                                                            'last_locator_id'                                   => $allocation_stock->id,
                                                            'last_movement_date'                                => $confirm_date,
                                                            'created_at'                                        => $confirm_date,
                                                            'updated_at'                                        => $confirm_date,
                                                            'is_need_inserted_to_locator_free_stock_erp'        => $is_need_inserted_to_locator_free_stock_erp,
                                                            'is_need_inserted_to_locator_inventory_erp'         => $is_need_inserted_to_locator_inventory_erp,
                                                            'first_print_date'                                  => null,
                                                            'deleted_at'                                        => null,
                                                            'last_user_movement_id'                             => $user,
                                                            'is_stock_already_created'                          => false,
                                                            'so_id'                                             => $so_id->so_id
                                                        ]);

                                                        $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;

                                                        $material_movement = MaterialMovement::FirstOrCreate([
                                                            'from_location'     => $allocation_stock->id,
                                                            'to_destination'    => $allocation_stock->id,
                                                            'is_active'         => true,
                                                            'po_buyer'          => $new_po_buyer,
                                                            'created_at'        => $confirm_date,
                                                            'updated_at'        => $confirm_date,
                                                            'status'            => 'allocation'
                                                        ]);
                            
                                                        MaterialMovementLine::FirstOrCreate([
                                                            'material_movement_id'      => $material_movement->id,
                                                            'material_preparation_id'   => $material_preparation->id,
                                                            'item_id'                   => $material_preparation->item_id,
                                                            'item_code'                 => $item_code_book,
                                                            'type_po'                   => 2,
                                                            'qty_movement'              => $__supplied,
                                                            'date_movement'             => $confirm_date,
                                                            'is_active'                 => false,
                                                            'user_id'                   => $user,
                                                            'created_at'                => $confirm_date,
                                                            'updated_at'                => $confirm_date,
                                                        ]);

                                                        DetailMaterialPreparation::FirstOrCreate([
                                                            'material_preparation_id'   => $material_preparation->id,
                                                            'material_arrival_id'       => $material_arrival_id,
                                                            'user_id'                   => $user
                                                        ]);
                                                        
                                                        //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
            
                                                        $qty_supply         -= $_supplied;
                                                        $qty_required       -= $_supplied;
                                                        $qty_outstanding    -= $_supplied;
                                                        $qty_allocated      += $_supplied;

                                                        $old                = $available_qty;
                                                        $new                = $available_qty - $_supplied;

                                                        if($new <= 0) $new  = '0';
                                                        else $new           = $new;
            
                                                        HistoryStock::approved($material_stock_id
                                                        ,$new
                                                        ,$old
                                                        ,$__supplied
                                                        ,$new_po_buyer
                                                        ,$style
                                                        ,$article_no
                                                        ,$lc_date
                                                        ,$allocation_source
                                                        ,$username
                                                        ,$user
                                                        ,$type_stock_erp_code
                                                        ,$type_stock
                                                        ,$is_integrate
                                                        ,true);
                                                        
                                                        $available_qty  -= $_supplied;
                                                        $reserved_qty   += $_supplied;
            
                                                        $is_bom_exists++;
                                                    }
                                                }
                                            }
                                        }
                                        
                                        //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                                        if($qty_outstanding<=0)
                                        {
                                            $auto_allocation->is_upload_manual                  = true;
                                            $auto_allocation->is_already_generate_form_booking  = true;
                                            $auto_allocation->generate_form_booking             = carbon::now();
                                        }
            
                                        $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                                        $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                                        $auto_allocation->save();
                                    }
                                }
                                
                            }

                            if($is_bom_exists > 0)
                            {
                                if($available_qty <=0 )
                                {
                                    $material_stock->is_allocated   = true;
                                    $material_stock->is_active      = false;
                                }

                                $material_stock->available_qty      = sprintf('%0.8f',$available_qty);
                                $material_stock->reserved_qty       = sprintf('%0.8f',$reserved_qty);
                                
                            }

                            $material_stock->last_status            = null;
                            $material_stock->save();
                        }
                    }

                    
                    
                    
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

        

    static function updateTotalAllocationSOTF()
    {
        $data = DB::select(db::raw("SELECT * FROM get_total_auto_allocation();")); // lc datenya jangan lupa di ganti ya
        foreach ($data as $key => $value) 
        {
            $id                             = $value->id;
            $material_stock                 = MaterialStock::find($id);
            $reserved_qty                   = sprintf('%0.8f',$value->total_allocation);

            $material_stock->reserved_qty   = $reserved_qty;
            $material_stock->save();
        }
    }

    static function insertAllocationFromMovementAccessories($stock_accessories,$_warehouse_id)
    {
        //return view('errors.503');

        $allocation_stock                       = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','FREE STOCK');
        })
        ->where('rack','ALLOCATION STOCK')
        ->first();

        $confirm_date   = Carbon::now();
        $get_stocks     = MaterialStock::whereIn('id',$stock_accessories)
        ->where('is_allocated',false)
        ->where('is_closing_balance',false)
        ->whereNotNull('approval_date')
        ->orderby('available_qty','asc')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($get_stocks as $key => $value) 
            {
                $material_stock_id  = $value->id;
                $material_stock     = MaterialStock::where([
                    ['id',$material_stock_id],
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                ])
                ->whereNull('last_status')
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->first();

                $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                ->where('material_stock_id',$material_stock_id)
                ->exists();

                if(!$has_outstanding_approval_exists)
                {
                    if($material_stock)
                    {
                        $material_stock->last_status = 'prepared';
                        $material_stock->save();
    
                        $document_no                = strtoupper($material_stock->document_no);
                        $item_code                  = strtoupper($material_stock->item_code);
                        $po_buyer_source            = $material_stock->po_buyer;
                        $type_stock_erp_code        = $material_stock->type_stock_erp_code;
                        $type_stock                 = $material_stock->type_stock;
                        $po_detail_id               = $material_stock->po_detail_id;
                        $uom_source                 = ($material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);
    
                        $c_bpartner_id              = $material_stock->c_bpartner_id;
                        $supplier_name              = strtoupper($material_stock->supplier_name);
                        $_supplier                  = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                        $supplier_code              = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                        $warehouse_id               = $value->warehouse_id;
                        $reserved_qty               = sprintf('%0.8f',$material_stock->reserved_qty);
                        $available_qty              = sprintf('%0.8f',$material_stock->available_qty);
                        $is_bom_exists              = 0;
                        $c_order_id                 = $material_stock->c_order_id;
                        $item_id                    = $material_stock->item_id;
                        $is_material_others         = $material_stock->is_material_others;
                        
                        if($is_material_others)
                        {
                            if($c_order_id == 'FREE STOCK')
                            {
                                $is_need_inserted_to_locator_free_stock_erp = true;
                                $is_need_inserted_to_locator_inventory_erp  = true;
                            }
                            else
                            {
                                $is_need_inserted_to_locator_free_stock_erp = false;
                                $is_need_inserted_to_locator_inventory_erp  = true;
                            }
                        }else
                        {
                            $is_need_inserted_to_locator_free_stock_erp = false;
                            $is_need_inserted_to_locator_inventory_erp  = false;
                        }
    
                        $material_arrival           = MaterialArrival::where([
                            ['po_detail_id',$po_detail_id],
                            ['warehouse_id',$warehouse_id],
                        ])
                        ->whereNull('material_subcont_id')
                        ->first();
                        
                        if (strpos($document_no, "POAR") !== false) $is_poar = true;
                        else $is_poar = false;
    
                        $prepared_status            = ($material_arrival ? $material_arrival->prepared_status : null);
                        $material_arrival_id        = ($material_arrival ? $material_arrival->id : null);
                        $is_moq                     = ($material_arrival ? $material_arrival->is_moq : null);
                        $po_buyer                   = ($material_arrival ? $material_arrival->po_buyer : null);
                        $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : '-');
    
                        $system                     = User::where([
                            ['name','system'],
                            ['warehouse',$warehouse_id]
                        ])
                        ->first();
    
                        $conversion = UomConversion::where([
                            ['item_id',$item_id],
                            ['uom_from',$uom_source]
                        ])
                        ->first();
                    
                        if($conversion)
                        {
                            $multiplyrate   = $conversion->multiplyrate;
                            $dividerate     = $conversion->dividerate;
                        }else
                        {
                            $dividerate     = 1;
                            $multiplyrate   = 1;
                        }
                        
                        $auto_allocations = AutoAllocation::where([
                            ['c_order_id',$c_order_id],
                            ['item_id_source',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['qty_outstanding','>',0],
                            ['status_po_buyer','active'],
                            ['is_additional',false],
                            ['is_fabric',false],
                            ['is_allocation_purchase',false],
                        ]);
    
                        //if($type_stock_erp_code) $auto_allocations = $auto_allocations->where('type_stock_erp_code',$type_stock_erp_code);
                        
                        $auto_allocations = $auto_allocations->whereNull('deleted_at')
                        ->orderby('promise_date','asc')
                        ->orderby('qty_allocation','asc')
                        ->get();
                    
                        if($available_qty > 0)
                        {
                            //echo $available_qty.'<br/>';
                            foreach ($auto_allocations as $key_2 => $auto_allocation)
                            {
                                $auto_allocation_id = $auto_allocation->id;
                                $c_order_id         = $auto_allocation->c_order_id;
                                $item_id_book       = $auto_allocation->item_id_book;
                                $item_id_source     = $auto_allocation->item_id_source;
                                $new_po_buyer       = $auto_allocation->po_buyer;
                                $old_po_buyer       = $auto_allocation->old_po_buyer;
                                $_item_code_book    = $auto_allocation->item_code;
                                $pos                = strpos($_item_code_book, '|');
    
                                if ($pos === false)
                                {
                                    $item_code_book = $_item_code_book;
                                    $_article_no    = null;
                                }else 
                                {
                                    $split          = explode('|',$_item_code_book);
                                    $item_code_book = $split[0];
                                    $_article_no    = $split[1];
                                }
                                
    
                                if($auto_allocation->is_upload_manual)
                                {
                                    $allocation_source  = 'ALLOCATION MANUAL';
                                    $user               = $auto_allocation->user_id;
                                    $username           = strtoupper($auto_allocation->user->name);
                                    $is_integrate       = false;
                                }else
                                {
                                    $allocation_source  = ($auto_allocation->is_allocation_purchase) ? 'ALLOCATION PURCHASE' : 'ALLOCATION ERP';
                                    $user               = $system->id;
                                    $username           = strtoupper($auto_allocation->user->name);
                                    $is_integrate       = true;
                                }
    
                                $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                                $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                                
                                $_po_buyer              = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                                $material_requirements  = MaterialRequirement::where([
                                    ['po_buyer',$new_po_buyer],
                                    [db::raw('upper(item_code)'),$item_code_book],
                                ]);
    
                                if($_article_no) 
                                    $material_requirements = $material_requirements->where('article_no',$_article_no);
    
                                $material_requirements     = $material_requirements->orderby('article_no','asc')
                                ->orderby('style','asc')
                                ->get();
    
                                $count_mr = count($material_requirements);
                                if($count_mr > 0)
                                {
                                    if($qty_outstanding > 0)
                                    {
                                        if ($available_qty/$qty_outstanding >= 1) $_supply = $qty_outstanding;
                                        else $_supply = $available_qty;
                                        
                                        $qty_supply = $_supply;
                                        
                                        foreach ($material_requirements as $key => $material_requirement) 
                                        {
                                            $lc_date        = $material_requirement->lc_date;
                                            $item_desc      = strtoupper($material_requirement->item_desc);
                                            $uom            = $material_requirement->uom;
                                            $category       = $material_requirement->category;
                                            $style          = $material_requirement->style;
                                            $job_order      = $material_requirement->job_order;
                                            $_style         = explode('::',$job_order)[0];
                                            $article_no     = $material_requirement->article_no;
                                            $qty_required   = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocatedAllocation($new_po_buyer,$item_code,$style,$article_no));
                                    
                                            //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                            //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                
                                            //echo $qty_supply.' '.$qty_required.'<br/>';
                                            //echo $po_buyer.' '.$item_code.' '.$is_exists.' '.$qty_supply.' '.$qty_required.'</br>';
                                            
                                            if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                                            else $__supplied = sprintf('%0.8f',$qty_supply);
            
                                            //echo $qty_supply.' '.$qty_required.'<br/>';
                                            if($__supplied > 0 && $qty_supply > 0)
                                            {
                                                if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                                else $_supplied = $qty_supply;
            
                                                $__supplied = sprintf('%0.8f',$_supplied);
                                            
            
                                                $is_exists = MaterialPreparation::where([
                                                    ['material_stock_id',$material_stock_id],
                                                    ['auto_allocation_id',$auto_allocation_id],
                                                    ['c_order_id',$c_order_id],
                                                    ['c_bpartner_id',$c_bpartner_id],
                                                    ['po_buyer',$new_po_buyer],
                                                    ['item_id',$item_id_book],
                                                    ['uom_conversion',$uom],
                                                    ['warehouse',$warehouse_id],
                                                    ['style',$style],
                                                    ['article_no',$article_no],
                                                    ['qty_conversion',$__supplied],
                                                    ['is_backlog',false],
                                                ]);
    
                                                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
                                                $is_exists = $is_exists->exists();
                                                
                                                if(!$is_exists && $_supplied > 0)
                                                {
                                                    $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);
                                                    $so_id                  = PoBuyer::where('po_buyer', $po_buyer)->first();
                                                    $material_preparation = MaterialPreparation::FirstOrCreate([
                                                        'material_stock_id'                                     => $material_stock_id,
                                                        'auto_allocation_id'                                    => $auto_allocation_id,
                                                        'po_detail_id'                                          => $po_detail_id,
                                                        'barcode'                                               => 'BELUM DI PRINT',
                                                        'referral_code'                                         => 'BELUM DI PRINT',
                                                        'sequence'                                              => 0,
                                                        'item_id'                                               => $item_id_book,
                                                        'c_order_id'                                            => $c_order_id,
                                                        'c_bpartner_id'                                         => $c_bpartner_id,
                                                        'supplier_name'                                         => $supplier_name,
                                                        'document_no'                                           => $document_no,
                                                        'po_buyer'                                              => $new_po_buyer,
                                                        'uom_conversion'                                        => $uom,
                                                        'qty_conversion'                                        => $__supplied,
                                                        'qty_reconversion'                                      => $qty_reconversion,
                                                        'job_order'                                             => $job_order,
                                                        'style'                                                 => $style,
                                                        '_style'                                                => $_style,
                                                        'article_no'                                            => $article_no,
                                                        'warehouse'                                             => $warehouse_id,
                                                        'item_code'                                             => $item_code_book,
                                                        'item_desc'                                             => $item_desc,
                                                        'category'                                              => $category,
                                                        'is_backlog'                                            => false,
                                                        'total_carton'                                          => 1,
                                                        'type_po'                                               => 2,
                                                        'user_id'                                               => auth::user()->id,
                                                        'last_status_movement'                                  => 'allocation',
                                                        'is_need_to_be_switch'                                  => false,
                                                        'last_locator_id'                                       => $allocation_stock->id,
                                                        'last_movement_date'                                    => $confirm_date,
                                                        'created_at'                                            => $confirm_date,
                                                        'updated_at'                                            => $confirm_date,
                                                        'is_need_inserted_to_locator_free_stock_erp'            => $is_need_inserted_to_locator_free_stock_erp,
                                                        'is_need_inserted_to_locator_inventory_erp'             => $is_need_inserted_to_locator_inventory_erp,
                                                        'first_print_date'                                      => null,
                                                        'deleted_at'                                            => null,
                                                        'last_user_movement_id'                                 => auth::user()->id,
                                                        'is_stock_already_created'                              => false,
                                                        'so_id'                                                 => $so_id->so_id
                                                    ]);
                                                    
                                                    $allocation_source = $allocation_source.', UNTUK PO BUYER '.$material_preparation->po_buyer.', ARTICLE '.$material_preparation->article_no.', STYLE '.$material_preparation->style.', DAN QTY '.$material_preparation->qty_conversion;

                                                    //echo $material_preparation->po_buyer.'<br/>';
                                                    $is_material_movement_exists     = MaterialMovement::where([
                                                        ['from_location',$allocation_stock->id],
                                                        ['to_destination',$allocation_stock->id],
                                                        ['from_locator_erp_id',$allocation_stock->area->erp_id],
                                                        ['to_locator_erp_id',$allocation_stock->area->erp_id],
                                                        ['po_buyer',$material_preparation->po_buyer],
                                                        ['no_packing_list',$no_packing_list],
                                                        ['no_invoice',$no_invoice],
                                                        ['is_integrate',false],
                                                        ['is_active',true],
                                                        ['status','allocation'],
                                                    ])
                                                    ->first();

                                                    if(!$is_material_movement_exists)
                                                    {
                                                        $material_movement = MaterialMovement::FirstOrCreate([
                                                            'from_location'         => $allocation_stock->id,
                                                            'to_destination'        => $allocation_stock->id,
                                                            'from_locator_erp_id'   => $allocation_stock->area->erp_id,
                                                            'to_locator_erp_id'     => $allocation_stock->area->erp_id,
                                                            'is_integrate'          => false,
                                                            'is_active'             => true,
                                                            'po_buyer'              => $material_preparation->po_buyer,
                                                            'created_at'            => $material_preparation->last_movement_date,
                                                            'updated_at'            => $material_preparation->last_movement_date,
                                                            'status'                => 'allocation'
                                                        ]);
                                                        $material_movement_id = $material_movement->id;
                                                    }else
                                                    {
                                                        $is_material_movement_exists->updated_at = $material_preparation->last_movement_date;
                                                        $is_material_movement_exists->save();

                                                        $material_movement_id = $is_material_movement_exists->id;
                                                    }
                                                   
                        
                                                    $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                    ->where([
                                                        'material_movement_id'      => $material_movement_id,
                                                        'material_preparation_id'   => $material_preparation->id,
                                                        'item_id'                   => $material_preparation->item_id,
                                                        'warehouse_id'              => $material_preparation->warehouse,
                                                        'type_po'                   => 2,
                                                        'date_movement'             => $material_preparation->last_movement_date,
                                                        'is_integrate'              => false,
                                                        'is_active'                 => true,
                                                        'qty_movement'              => $material_preparation->qty_conversion,
                                                    ])
                                                    ->exists();
                                    
                                                    if(!$is_line_exists)
                                                    {
                                                        $new_material_movement_line = MaterialMovementLine::firstOrCreate([
                                                            'material_movement_id'          => $material_movement_id,
                                                            'material_preparation_id'       => $material_preparation->id,
                                                            'item_id'                       => $material_preparation->item_id,
                                                            'item_id_source'                => $material_preparation->item_code,
                                                            'c_order_id'                    => $material_preparation->c_order_id,
                                                            'c_orderline_id'                => $c_orderline_id,
                                                            'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                            'supplier_name'                 => $material_preparation->supplier_name,
                                                            'document_no'                   => $material_preparation->document_no,
                                                            'item_code'                     => $material_preparation->item_code,
                                                            'nomor_roll'                    => '-',
                                                            'type_po'                       => 2,
                                                            'uom_movement'                  => $material_preparation->uom_conversion,
                                                            'qty_movement'                  => $material_preparation->qty_conversion,
                                                            'warehouse_id'                  => $material_preparation->warehouse,
                                                            'date_movement'                 => $material_preparation->last_movement_date,
                                                            'created_at'                    => $material_preparation->last_movement_date,
                                                            'updated_at'                    => $material_preparation->last_movement_date,
                                                            'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                                            'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                            'is_active'                     => true,
                                                            'is_integrate'                  => false,
                                                            'user_id'                       => $system->id
                                                        ]);
                                    
                                                        $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                        $material_preparation->save();
                                                    }
    
                                                    DetailMaterialPreparation::FirstOrCreate([
                                                        'material_preparation_id'   => $material_preparation->id,
                                                        'material_arrival_id'       => $material_arrival_id,
                                                        'user_id'                   => $system->id
                                                    ]);
            
                                                    //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
            
                                                    $qty_supply             -= $_supplied;
                                                    $qty_required           -= $_supplied;
                                                    $qty_outstanding        -= $_supplied;
                                                    $qty_allocated          += $_supplied;
    
                                                    $old = $available_qty;
                                                    $new = $available_qty - $_supplied;
                                                    
                                                    if($new <= 0) $new = '0';
                                                    else $new = $new;
            
                                                    HistoryStock::approved($material_stock_id
                                                    ,$new
                                                    ,$old
                                                    ,$__supplied
                                                    ,$new_po_buyer
                                                    ,$style
                                                    ,$article_no
                                                    ,$lc_date
                                                    ,$allocation_source
                                                    ,$username
                                                    ,$user
                                                    ,$type_stock_erp_code
                                                    ,$type_stock
                                                    ,$is_integrate
                                                    ,true);
                                                    
                                                    $available_qty -= $_supplied;
                                                    $reserved_qty += $_supplied;
                                                    
                                                    $is_bom_exists++;
                                                }
                                            }
                                        }
                                    }
                                    
                                    //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                                    if($qty_outstanding<=0)
                                    {
                                        $auto_allocation->is_upload_manual                  = true;
                                        $auto_allocation->is_already_generate_form_booking  = true;
                                        $auto_allocation->generate_form_booking             = carbon::now();
                                    }
            
                                    $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                                    $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                                    $auto_allocation->save();
                                }
                            }
                            
                        }
    
                        if($is_bom_exists > 0)
                        {
                            if($available_qty <=0 ){
                                $material_stock->is_allocated   = true;
                                $material_stock->is_active      = false;
                            }
    
                            $material_stock->available_qty      = sprintf('%0.8f',$available_qty);
                            $material_stock->reserved_qty       = sprintf('%0.8f',$reserved_qty);
                        
                        }
    
                        $material_stock->last_status            = null;
                        $material_stock->save();
                    }
                }
                
               
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertAllocationFabricItems()
    {
        //return view('errors.503');
        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['status_po_buyer','active'],
            ['qty_allocated', 0],
            //['is_additional',true],
        ])
        ->whereNull('deleted_at')
        ->orderBy('is_ordering_for_japan_china', 'desc')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();

        try 
        {
            DB::beginTransaction();

            $buyers         = array();
            $movement_date  = carbon::now()->todatetimestring();

            foreach ($auto_allocations as $key_2 => $auto_allocation)
            {
                $is_additional          = $auto_allocation->is_additional;
                $auto_allocation_id     = $auto_allocation->id;
                $c_order_id             = $auto_allocation->c_order_id;
                $user_id                = $auto_allocation->user_id;
                $document_no            = $auto_allocation->document_no;
                $c_bpartner_id          = $auto_allocation->c_bpartner_id;
                $new_po_buyer           = $auto_allocation->po_buyer;
                $old_po_buyer           = $auto_allocation->old_po_buyer;
                $item_id_book           = $auto_allocation->item_id_book;
                $item_id_source         = $auto_allocation->item_id_source;
                $item_code              = $auto_allocation->item_code;
                $item_code_source       = $auto_allocation->item_code_source;
                $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $warehouse_id           = $auto_allocation->warehouse_id;
                $supplier               = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
                
                if($auto_allocation->is_upload_manual)
                {
                    $allocation_source                                              = 'ALLOCATION MANUAL';
                    Temporary::create([
                        'barcode'       => $auto_allocation->id,
                        'status'        => 'insert_stock_fabric',
                        'user_id'       => $system->id,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);
                }else
                {
                    if($auto_allocation->is_allocation_purchase) $allocation_source = 'ALLOCATION PURCHASE';
                    else $allocation_source                                         = 'AUTO ALLOCATION';
                }

                $_po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->whereNull('cancel_date')->first();

                if($_po_buyer)
                {
                    $material_requirements = MaterialRequirement::where([
                        ['po_buyer',$new_po_buyer],
                        ['item_id',$item_id_book],
                    ])
                    ->orderby('article_no','asc')
                    ->get();
        
                    if($qty_outstanding > 0.0000)
                    {
                        $count_mr = count($material_requirements);
                        //AllocationItem::where('auto_allocation_id',$auto_allocation_id)->delete();
                        //dd($material_requirements);
                        if($count_mr > 0)
                        {
                            foreach ($material_requirements as $key => $material_requirement) 
                            {
                                $lc_date        = $material_requirement->lc_date;
                                $item_desc      = strtoupper($material_requirement->item_desc);
                                $uom            = $material_requirement->uom;
                                $category       = $material_requirement->category;
                                $style          = $material_requirement->style;
                                $job_order      = $material_requirement->job_order;
                                $_style         = explode('::',$job_order)[0];
                                $article_no     = $material_requirement->article_no;
                                
                                //$month_lc       = $material_requirement->lc_date->format('m');
                                if($is_additional)
                                {
                                    if(($key+1) != $count_mr) $qty_required = sprintf('%0.8f',1);
                                    else $qty_required = sprintf('%0.8f',$qty_outstanding);
                                }else
                                {
                                    $qty_bom             = $material_requirement->qty_required;
                                    $qty_allocation_item = $material_requirement->qtyAllocatedFabric($new_po_buyer,$item_code,$style,$article_no);
                                    // if($qty_bom - $qty_allocation + $qty_allocation_item > 0)
                                    // {
                                    //     $qty_required    = $qty_allocation - $qty_allocation_item;
                                    // }
                                    // else
                                    // {
                                        $qty_required    = $qty_bom - $qty_allocation_item;
                                    //}
                                }
                                
                                $__supplied = sprintf('%0.8f',$qty_required);
    
                                $_warehouse_erp_id = $material_requirement->warehouse_id;
    
                                if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                                else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                                else $warehouse_production_id = $warehouse_id;
                                
                                if($qty_required > 0.0000 && $qty_outstanding > 0.0000)
                                {
                                    if ($qty_outstanding/$__supplied >= 1) $qty_required = $__supplied;
                                    else $qty_required = $qty_outstanding;
    
                                    $__supply = sprintf('%0.8f',$qty_required);
                                
                                    if($is_additional)
                                    {
                                        $is_exists = false;
                                    }else
                                    {
                                        $is_exists = AllocationItem::where([
                                            ['auto_allocation_id',$auto_allocation_id],
                                            ['item_id_book',$item_id_book],
                                            ['item_id_source',$item_id_source],
                                            ['style',$style],
                                            ['po_buyer',$new_po_buyer],
                                            //['qty_booking',$__supply],
                                            ['is_additional',false],
                                            ['is_from_allocation_fabric',true],
                                            ['warehouse_inventory',$warehouse_id] // warehouse inventory
                                        ])
                                        ->where(function($query){
                                            $query->where('confirm_by_warehouse','approved')
                                            ->OrWhereNull('confirm_by_warehouse');
                                        })
                                        ->whereNull('deleted_at')
                                        ->exists();
                                    }
                                    
                                    if(!$is_exists && $qty_required > 0)
                                    {
                                        //jika jepang cina dimaksimalkan 2 lot
                                        if($auto_allocation->is_ordering_for_japan_china == 1)
                                        {
                                            //ambil lot dengan qty paling gede
                                            $max_qty_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                            ->whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['qty_available', '>', 0],
                                            ])
                                            ->groupBy('actual_lot')
                                            ->orderBy('total_qty', 'desc')
                                            ->first();
    
                                            $lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->actual_lot : null;
                                            $qty_lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->total_qty : 0;
    
                                            if($lot_max != null)
                                            {
    
                                                //cari lot sebelumnya 
                                                $before_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                                ->whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                    ['actual_lot', '<', $lot_max],
                                                ])
                                                ->groupBy('actual_lot')
                                                ->orderBy('total_qty', 'desc')
                                                ->first();
    
                                                $lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->actual_lot : null;
                                                $qty_lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->total_qty : 0;
    
                                                //cari lot setelahnya
                                                $after_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                                ->whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                    ['actual_lot', '>', $lot_max],
                                                ])
                                                ->groupBy('actual_lot')
                                                ->orderBy('total_qty', 'desc')
                                                ->first();
    
                                                $lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->actual_lot : null;
                                                $qty_lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->total_qty : 0;
    
                                                //list urutan lotnya 
                                                $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                                ->whereNotNull('actual_lot')
                                                ->where([
                                                    ['c_order_id',$c_order_id],
                                                    ['item_id',$item_id_source],
                                                    ['warehouse_id',$warehouse_id],
                                                ])
                                                ->orderBy(db::raw(" actual_lot ='".$lot_max."'"), 'desc')
                                                ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                    actual_lot ='".$lot_before."'
                                                    else
                                                    actual_lot ='".$lot_after."'
                                                    end"), 'desc')
                                                ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                    actual_lot >'".$lot_before."'
                                                    else
                                                    actual_lot >'".$lot_after."'
                                                    end"), 'desc')
                                                ->get();    
                                            }
                                            else
                                            {
                                                $material_stock_per_lots = array();
                                            }
    
                                        }
                                        else
                                        {
                                            $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['qty_available', '>', 0],
                                            ])
                                            ->orderBy('actual_lot', 'asc')
                                            ->orderBy('qty_available', 'desc')
                                            ->get();
    
                                        }
    
                                        //$count_lot = 0;
                                        if(count($material_stock_per_lots) > 0)
                                        {
                                            //dd($material_stock_per_lots->pluck('id', 'qty_available'));
                                            //$ini_array = array();
                                                foreach ($material_stock_per_lots as $key_3 => $material_stock_per_lot) 
                                                {
                                                    if($qty_required > 0.0000)
                                                    {
                                                        $material_stock_per_lot_id = $material_stock_per_lot->id;
                                                        $qty_available_lot         = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                                                        $qty_reserved_lot          = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);
    
                                                        //cek apakah stock per lot cukup atau tidak 
                                                        if($qty_available_lot >= $qty_required )
                                                        {
                                                            // $ini_array [] = [
                                                            //     'qty_available_lot'         => $qty_available_lot,
                                                            //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            //     'qty_required'              => $qty_required,
                                                            //     'style'                     => $style
                                                            // ];
                                                            $qty_booking = $qty_required;
                                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                                'auto_allocation_id'        => $auto_allocation_id,
                                                                'lc_date'                   => $lc_date,
                                                                'c_bpartner_id'             => $c_bpartner_id,
                                                                'c_order_id'                => $c_order_id,
                                                                'supplier_code'             => $supplier_code,
                                                                'supplier_name'             => $supplier_name,
                                                                'document_no'               => $document_no,
                                                                'item_code'                 => $item_code,
                                                                'item_id_book'              => $item_id_book,
                                                                'item_code_source'          => $item_code_source,
                                                                'item_id_source'            => $item_id_source,
                                                                'item_desc'                 => $item_desc,
                                                                'category'                  => $category,
                                                                'uom'                       => $uom,
                                                                'job'                       => $job_order,
                                                                '_style'                    => $_style,
                                                                'style'                     => $style,
                                                                'article_no'                => $article_no,
                                                                'po_buyer'                  => $new_po_buyer,
                                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                                'warehouse'                 => $warehouse_production_id,
                                                                'warehouse_inventory'       => $warehouse_id,
                                                                'is_need_to_handover'       => false,
                                                                'qty_booking'               => $qty_booking,
                                                                'is_from_allocation_fabric' => true,
                                                                'is_additional'             => $is_additional,
                                                                'user_id'                   => $user_id,
                                                                'confirm_by_warehouse'      => 'approved',
                                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                                'confirm_date'              => Carbon::now(),
                                                                'remark'                    => $allocation_source,
                                                                'confirm_user_id'           => $system->id,
                                                                'deleted_at'                => null,
                                                                'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            ]);
    
                                                            //update available stock per lot
                                                            $qty_available_lot                     -= $qty_booking;
                                                            $qty_reserved_lot                      += $qty_booking;
                                                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                            $material_stock_per_lot->save();
                                                            //$__supply                               = $qty_booking;
                                                        }
                                                        else
                                                        {
                                                            //jika kurang dari qty booking pakai available yang tersedia
                                                            $qty_booking = $qty_available_lot;
                                                            // $ini_array [] = [
                                                            //     'qty_available_lot'         => $qty_available_lot,
                                                            //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            //     'qty_required'              => $qty_required,
                                                            //     'style'                     => $style
                                                            // ];
    
                                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                                'auto_allocation_id'        => $auto_allocation_id,
                                                                'lc_date'                   => $lc_date,
                                                                'c_bpartner_id'             => $c_bpartner_id,
                                                                'c_order_id'                => $c_order_id,
                                                                'supplier_code'             => $supplier_code,
                                                                'supplier_name'             => $supplier_name,
                                                                'document_no'               => $document_no,
                                                                'item_code'                 => $item_code,
                                                                'item_id_book'              => $item_id_book,
                                                                'item_code_source'          => $item_code_source,
                                                                'item_id_source'            => $item_id_source,
                                                                'item_desc'                 => $item_desc,
                                                                'category'                  => $category,
                                                                'uom'                       => $uom,
                                                                'job'                       => $job_order,
                                                                '_style'                    => $_style,
                                                                'style'                     => $style,
                                                                'article_no'                => $article_no,
                                                                'po_buyer'                  => $new_po_buyer,
                                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                                'warehouse'                 => $warehouse_production_id,
                                                                'warehouse_inventory'       => $warehouse_id,
                                                                'is_need_to_handover'       => false,
                                                                'qty_booking'               => $qty_booking,
                                                                'is_from_allocation_fabric' => true,
                                                                'is_additional'             => $is_additional,
                                                                'user_id'                   => $user_id,
                                                                'confirm_by_warehouse'      => 'approved',
                                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                                'confirm_date'              => Carbon::now(),
                                                                'remark'                    => $allocation_source,
                                                                'confirm_user_id'           => $system->id,
                                                                'deleted_at'                => null,
                                                                'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                            ]);
    
                                                            //update available stock per lot
                                                            $qty_available_lot                     -= $qty_booking;
                                                            $qty_reserved_lot                      += $qty_booking;
                                                            $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                            $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                            $material_stock_per_lot->save();
                                                            //$__supply                               = $qty_booking;
                                                        }
    
                                                        //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                                        //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                                                        $qty_required       -= $qty_booking;
                                                        $qty_outstanding    -= $qty_booking;
                                                        $qty_allocated      += $qty_booking;
                                                        //$count_lot          += 1;
                                                        $buyers []           = $new_po_buyer;
                                                    }
                                                    else
                                                    {
                                                        break;
                                                    }
                                                }
                                                //dd($ini_array);
    
                                        }
                                        
                                        if($qty_required > 0.0000)
                                        {
                                            $__supply = $qty_required;
    
                                            $allocation_item = AllocationItem::FirstOrCreate([
                                                'auto_allocation_id'        => $auto_allocation_id,
                                                'lc_date'                   => $lc_date,
                                                'c_bpartner_id'             => $c_bpartner_id,
                                                'c_order_id'                => $c_order_id,
                                                'supplier_code'             => $supplier_code,
                                                'supplier_name'             => $supplier_name,
                                                'document_no'               => $document_no,
                                                'item_code'                 => $item_code,
                                                'item_id_book'              => $item_id_book,
                                                'item_code_source'          => $item_code_source,
                                                'item_id_source'            => $item_id_source,
                                                'item_desc'                 => $item_desc,
                                                'category'                  => $category,
                                                'uom'                       => $uom,
                                                'job'                       => $job_order,
                                                '_style'                    => $_style,
                                                'style'                     => $style,
                                                'article_no'                => $article_no,
                                                'po_buyer'                  => $new_po_buyer,
                                                'old_po_buyer_reroute'      => $old_po_buyer,
                                                'warehouse'                 => $warehouse_production_id,
                                                'warehouse_inventory'       => $warehouse_id,
                                                'is_need_to_handover'       => false,
                                                'qty_booking'               => $__supply,
                                                'is_from_allocation_fabric' => true,
                                                'is_additional'             => $is_additional,
                                                'user_id'                   => $user_id,
                                                'confirm_by_warehouse'      => 'approved',
                                                'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                'confirm_date'              => Carbon::now(),
                                                'remark'                    => $allocation_source,
                                                'confirm_user_id'           => $system->id,
                                                'deleted_at'                => null
                                        ]);
    
                                        $qty_required       -= $__supply;
                                        $qty_outstanding    -= $__supply;
                                        $qty_allocated      += $__supply;
                                        $buyers []           = $new_po_buyer;
    
                                        }
                                    }

                                    if($is_additional == true)
                                    {
                                        //tambahkan additional ke preparation 
                                        // $is_material_preparation_exists = MaterialPreparationFabric::where([
                                        //     ['c_order_id',$c_order_id],
                                        //     ['article_no',$article_no],
                                        //     ['warehouse_id',$warehouse_id],
                                        //     ['item_id_book',$item_id_book],
                                        //     ['item_id_source',$item_id_source],
                                        //     ['planning_date',$movement_date->format('Y-m-d')],
                                        //     ['is_piping',false],
                                        //     ['_style',$style],
                                        //     ['is_additional', false],
                                        //     ['total_reserved_qty', $qty_allocation]
                                        // ])
                                        // ->first();
                                
                                        // if(!$is_material_preparation_exists)
                                        // {
                                            $check_material_preparation_fab = db::table('material_preparation_fabrics')
                                            ->where('auto_allocation_id', $auto_allocation_id)
                                            ->where('document_no',$document_no)
                                            ->where('item_code', $item_code )
                                            ->where('po_buyer', $new_po_buyer)
                                            ->exists();

                                            if($check_material_preparation_fab == false){

                                            

                                            $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                                                'c_bpartner_id'         => $c_bpartner_id,
                                                'supplier_name'         => $supplier_name,
                                                'document_no'           => $document_no,
                                                'po_buyer'              => $new_po_buyer,    
                                                'item_code'             => $item_code,
                                                'item_code_source'      => $item_code_source,
                                                'c_order_id'            => $c_order_id,
                                                'item_id_book'          => $item_id_book,
                                                'item_id_source'        => $item_id_source,
                                                'uom'                   => 'YDS',
                                                'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                                                'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                                                'planning_date'         => Carbon::parse($movement_date)->format('Y-m-d'),
                                                // 'planning_date'         => $movement_date->format('Y-m-d'),
                                                'is_piping'             => false,
                                                'article_no'            => $article_no,
                                                '_style'                => $style,
                                                'is_from_additional'    => true,
                                                'is_balance_marker'     => false,
                                                'preparation_date'      => null,
                                                'preparation_by'        => null,
                                                'warehouse_id'          => $warehouse_id,
                                                'user_id'               => $user_id,
                                                'created_at'            => $movement_date,
                                                'updated_at'            => $movement_date,
                                                'auto_allocation_id'    => $auto_allocation_id,
                                            ]);
                                        // }
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                    
                    if(floatVal($qty_outstanding)<= 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = true;
                        $auto_allocation->generate_form_booking             = carbon::now();
                        $auto_allocation->qty_outstanding                   = 0;
                    }else
                    {
                        $auto_allocation->qty_outstanding                   = sprintf('%0.8f',$qty_outstanding);
                    
                    }
        
                    $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                    $auto_allocation->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        PlanningFabric::getAllocationFabricPerBuyer($buyers);
        
        
    }

    static function scheduleInsertAllocationFabric()
    {
        //return view('errors.503');
        $auto_allocations = AutoAllocation::where([
            ['is_fabric',true],
            ['status_po_buyer','active']
        ])
        ->whereIn('id',function($query)
        {
            $query->select('barcode')
            ->from('temporaries')
            ->where('status','update_auto_allocation_fabric')
            ->groupby('barcode');
        })
        ->whereNull('locked_date')
        ->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();
        
        $buyers         = array();
        $movement_date  = carbon::now()->todatetimestring();

        foreach ($auto_allocations as $key_2 => $auto_allocation) 
        {
            $auto_allocation->locked_date   = $movement_date;
            $auto_allocation->save();

            try 
            {
                DB::beginTransaction();
                
                if($auto_allocation->is_upload_manual)
                {
                    $allocation_source = 'ALLOCATION MANUAL';
                    Temporary::create([
                        'barcode'       => $auto_allocation->id,
                        'status'        => 'insert_stock_fabric',
                        'user_id'       => $auto_allocation->user_id,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);
                }else
                {
                    if($auto_allocation->is_allocation_purchase) $allocation_source = 'ALLOCATION PURCHASE';
                    else $allocation_source = 'AUTO ALLOCATION';
                }

                $user_id                = $auto_allocation->user_id;
                $auto_allocation_id     = $auto_allocation->id;
                $c_order_id             = $auto_allocation->c_order_id;
                $document_no            = $auto_allocation->document_no;
                $c_bpartner_id          = $auto_allocation->c_bpartner_id;
                $new_po_buyer           = $auto_allocation->po_buyer;
                $old_po_buyer           = $auto_allocation->old_po_buyer;
                $item_code              = $auto_allocation->item_code;
                $item_id_book           = $auto_allocation->item_id_book;
                $item_id_source         = $auto_allocation->item_id_source;
                $item_code_source       = $auto_allocation->item_code_source;
                $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $qty_outstanding        = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                $warehouse_id           = $auto_allocation->warehouse_id;
                $remark_update          = $auto_allocation->remark_update;
                $is_additional          = $auto_allocation->is_additional;
                $supplier               = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
                
                $_po_buyer = PoBuyer::where('po_buyer',$new_po_buyer)->first();

                $material_requirements = MaterialRequirement::where([
                    ['po_buyer',$new_po_buyer],
                    [db::raw('upper(item_id)'),$item_id_book],
                ])
                ->get();

                // $total_need = MaterialRequirement::where([
                //     ['po_buyer',$new_po_buyer],
                //     [db::raw('upper(item_id)'),$item_id_book],
                // ])
                // ->sum('qty_required');

                

                //echo $qty_outstanding.'<br/>';
                if($qty_outstanding > 0.0000)
                {
                    $count_mr = count($material_requirements);
                    //AllocationItem::where('auto_allocation_id',$auto_allocation_id)->delete();
                    //dd($material_requirements);
                    if($count_mr > 0)
                    {
                        foreach ($material_requirements as $key => $material_requirement) 
                        {
                            $lc_date        = $material_requirement->lc_date;
                            $item_desc      = strtoupper($material_requirement->item_desc);
                            $uom            = $material_requirement->uom;
                            $category       = $material_requirement->category;
                            $style          = $material_requirement->style;
                            $job_order      = $material_requirement->job_order;
                            $_style         = explode('::',$job_order)[0];
                            $article_no     = $material_requirement->article_no;
                            
                            //$month_lc       = $material_requirement->lc_date->format('m');
                            if($is_additional)
                            {
                                if(($key+1) != $count_mr) $qty_required = sprintf('%0.8f',1);
                                else $qty_required = sprintf('%0.8f',$qty_outstanding);
                            }else
                            {
                                $qty_bom             = $material_requirement->qty_required;
                                $qty_allocation_item = $material_requirement->qtyAllocatedFabric($new_po_buyer,$item_code,$style,$article_no);
                                // if($qty_bom - $qty_allocation + $qty_allocation_item > 0)
                                // {
                                //     $qty_required    = $qty_allocation - $qty_allocation_item;
                                // }
                                // else
                                // {
                                    $qty_required    = $qty_bom - $qty_allocation_item;
                                //}
                            }
                            
                            $__supplied = sprintf('%0.8f',$qty_required);

                            $_warehouse_erp_id = $material_requirement->warehouse_id;

                            if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                            else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                            else $warehouse_production_id = $warehouse_id;
                            
                            if($qty_required > 0.0000 && $qty_outstanding > 0.0000)
                            {
                                if ($qty_outstanding/$__supplied >= 1) $qty_required = $__supplied;
                                else $qty_required = $qty_outstanding;

                                $__supply = sprintf('%0.8f',$qty_required);
                            
                                if($is_additional)
                                {
                                    $is_exists = false;
                                }else
                                {
                                    $is_exists = AllocationItem::where([
                                        ['auto_allocation_id',$auto_allocation_id],
                                        ['item_id_book',$item_id_book],
                                        ['item_id_source',$item_id_source],
                                        ['style',$style],
                                        ['po_buyer',$new_po_buyer],
                                        //['qty_booking',$__supply],
                                        ['is_additional',false],
                                        ['is_from_allocation_fabric',true],
                                        ['warehouse_inventory',$warehouse_id] // warehouse inventory
                                    ])
                                    ->where(function($query){
                                        $query->where('confirm_by_warehouse','approved')
                                        ->OrWhereNull('confirm_by_warehouse');
                                    })
                                    ->whereNull('deleted_at')
                                    ->exists();
                                }
                                
                                if(!$is_exists && $qty_required > 0)
                                {
                                    //jika jepang cina dimaksimalkan 2 lot
                                    if($auto_allocation->is_ordering_for_japan_china == 1)
                                    {
                                        //ambil lot dengan qty paling gede
                                        $max_qty_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                        ->whereNull('deleted_at')
                                        ->whereNotNull('actual_lot')
                                        ->where([
                                            ['c_order_id',$c_order_id],
                                            ['item_id',$item_id_source],
                                            ['warehouse_id',$warehouse_id],
                                            ['qty_available', '>', 0],
                                        ])
                                        ->groupBy('actual_lot')
                                        ->orderBy('total_qty', 'desc')
                                        ->first();

                                        $lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->actual_lot : null;
                                        $qty_lot_max = ($max_qty_stock_per_Lot)? $max_qty_stock_per_Lot->total_qty : 0;

                                        if($lot_max != null)
                                        {

                                            //cari lot sebelumnya 
                                            $before_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                            ->whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['actual_lot', '<', $lot_max],
                                            ])
                                            ->groupBy('actual_lot')
                                            ->orderBy('total_qty', 'desc')
                                            ->first();

                                            $lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->actual_lot : null;
                                            $qty_lot_before = ($before_max_stock_per_Lot) ? $before_max_stock_per_Lot->total_qty : 0;

                                            //cari lot setelahnya
                                            $after_max_stock_per_Lot = MaterialStockPerLot::select('actual_lot', db::raw("sum(qty_available) as total_qty"))
                                            ->whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                                ['actual_lot', '>', $lot_max],
                                            ])
                                            ->groupBy('actual_lot')
                                            ->orderBy('total_qty', 'desc')
                                            ->first();

                                            $lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->actual_lot : null;
                                            $qty_lot_after = ($after_max_stock_per_Lot) ? $after_max_stock_per_Lot->total_qty : 0;

                                            //list urutan lotnya 
                                            $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                            ->whereNotNull('actual_lot')
                                            ->where([
                                                ['c_order_id',$c_order_id],
                                                ['item_id',$item_id_source],
                                                ['warehouse_id',$warehouse_id],
                                            ])
                                            ->orderBy(db::raw(" actual_lot ='".$lot_max."'"), 'desc')
                                            ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                actual_lot ='".$lot_before."'
                                                else
                                                actual_lot ='".$lot_after."'
                                                end"), 'desc')
                                            ->orderBy(db::raw("case when '".$qty_lot_before."' < '".$qty_lot_after."'  then
                                                actual_lot >'".$lot_before."'
                                                else
                                                actual_lot >'".$lot_after."'
                                                end"), 'desc')
                                            ->get();    
                                        }
                                        else
                                        {
                                            $material_stock_per_lots = array();
                                        }

                                    }
                                    else
                                    {
                                        $material_stock_per_lots = MaterialStockPerLot::whereNull('deleted_at')
                                        ->whereNotNull('actual_lot')
                                        ->where([
                                            ['c_order_id',$c_order_id],
                                            ['item_id',$item_id_source],
                                            ['warehouse_id',$warehouse_id],
                                            ['qty_available', '>', 0],
                                        ])
                                        ->orderBy('actual_lot', 'asc')
                                        ->orderBy('qty_available', 'desc')
                                        ->get();

                                    }

                                    //$count_lot = 0;
                                    if(count($material_stock_per_lots) > 0)
                                    {
                                        //dd($material_stock_per_lots->pluck('id', 'qty_available'));
                                        //$ini_array = array();
                                            foreach ($material_stock_per_lots as $key_3 => $material_stock_per_lot) 
                                            {
                                                if($qty_required > 0.0000)
                                                {
                                                    $material_stock_per_lot_id = $material_stock_per_lot->id;
                                                    $qty_available_lot         = sprintf('%0.8f',$material_stock_per_lot->qty_available);
                                                    $qty_reserved_lot          = sprintf('%0.8f',$material_stock_per_lot->qty_reserved);

                                                    //cek apakah stock per lot cukup atau tidak 
                                                    if($qty_available_lot >= $qty_required )
                                                    {
                                                        // $ini_array [] = [
                                                        //     'qty_available_lot'         => $qty_available_lot,
                                                        //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                        //     'qty_required'              => $qty_required,
                                                        //     'style'                     => $style
                                                        // ];
                                                        $qty_booking = $qty_required;
                                                        $allocation_item = AllocationItem::FirstOrCreate([
                                                            'auto_allocation_id'        => $auto_allocation_id,
                                                            'lc_date'                   => $lc_date,
                                                            'c_bpartner_id'             => $c_bpartner_id,
                                                            'c_order_id'                => $c_order_id,
                                                            'supplier_code'             => $supplier_code,
                                                            'supplier_name'             => $supplier_name,
                                                            'document_no'               => $document_no,
                                                            'item_code'                 => $item_code,
                                                            'item_id_book'              => $item_id_book,
                                                            'item_code_source'          => $item_code_source,
                                                            'item_id_source'            => $item_id_source,
                                                            'item_desc'                 => $item_desc,
                                                            'category'                  => $category,
                                                            'uom'                       => $uom,
                                                            'job'                       => $job_order,
                                                            '_style'                    => $_style,
                                                            'style'                     => $style,
                                                            'article_no'                => $article_no,
                                                            'po_buyer'                  => $new_po_buyer,
                                                            'old_po_buyer_reroute'      => $old_po_buyer,
                                                            'warehouse'                 => $warehouse_production_id,
                                                            'warehouse_inventory'       => $warehouse_id,
                                                            'is_need_to_handover'       => false,
                                                            'qty_booking'               => $qty_booking,
                                                            'is_from_allocation_fabric' => true,
                                                            'is_additional'             => $is_additional,
                                                            'user_id'                   => $user_id,
                                                            'confirm_by_warehouse'      => 'approved',
                                                            'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                            'confirm_date'              => Carbon::now(),
                                                            'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                                            'confirm_user_id'           => $system->id,
                                                            'deleted_at'                => null,
                                                            'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                        ]);

                                                        //update available stock per lot
                                                        $qty_available_lot                     -= $qty_booking;
                                                        $qty_reserved_lot                      += $qty_booking;
                                                        $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                        $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                        $material_stock_per_lot->save();
                                                        //$__supply                               = $qty_booking;
                                                    }
                                                    else
                                                    {
                                                        //jika kurang dari qty booking pakai available yang tersedia
                                                        $qty_booking = $qty_available_lot;
                                                        // $ini_array [] = [
                                                        //     'qty_available_lot'         => $qty_available_lot,
                                                        //     'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                        //     'qty_required'              => $qty_required,
                                                        //     'style'                     => $style
                                                        // ];

                                                        $allocation_item = AllocationItem::FirstOrCreate([
                                                            'auto_allocation_id'        => $auto_allocation_id,
                                                            'lc_date'                   => $lc_date,
                                                            'c_bpartner_id'             => $c_bpartner_id,
                                                            'c_order_id'                => $c_order_id,
                                                            'supplier_code'             => $supplier_code,
                                                            'supplier_name'             => $supplier_name,
                                                            'document_no'               => $document_no,
                                                            'item_code'                 => $item_code,
                                                            'item_id_book'              => $item_id_book,
                                                            'item_code_source'          => $item_code_source,
                                                            'item_id_source'            => $item_id_source,
                                                            'item_desc'                 => $item_desc,
                                                            'category'                  => $category,
                                                            'uom'                       => $uom,
                                                            'job'                       => $job_order,
                                                            '_style'                    => $_style,
                                                            'style'                     => $style,
                                                            'article_no'                => $article_no,
                                                            'po_buyer'                  => $new_po_buyer,
                                                            'old_po_buyer_reroute'      => $old_po_buyer,
                                                            'warehouse'                 => $warehouse_production_id,
                                                            'warehouse_inventory'       => $warehouse_id,
                                                            'is_need_to_handover'       => false,
                                                            'qty_booking'               => $qty_booking,
                                                            'is_from_allocation_fabric' => true,
                                                            'is_additional'             => $is_additional,
                                                            'user_id'                   => $user_id,
                                                            'confirm_by_warehouse'      => 'approved',
                                                            'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                                            'confirm_date'              => Carbon::now(),
                                                            'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                                            'confirm_user_id'           => $system->id,
                                                            'deleted_at'                => null,
                                                            'material_stock_per_lot_id' => $material_stock_per_lot_id,
                                                        ]);

                                                        //update available stock per lot
                                                        $qty_available_lot                     -= $qty_booking;
                                                        $qty_reserved_lot                      += $qty_booking;
                                                        $material_stock_per_lot->qty_available  = $qty_available_lot;
                                                        $material_stock_per_lot->qty_reserved   = $qty_reserved_lot;
                                                        $material_stock_per_lot->save();
                                                        //$__supply                               = $qty_booking;
                                                    }

                                                    //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                                                    //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                                                    $qty_required       -= $qty_booking;
                                                    $qty_outstanding    -= $qty_booking;
                                                    $qty_allocated      += $qty_booking;
                                                    //$count_lot          += 1;
                                                    $buyers []           = $new_po_buyer;
                                                }
                                                else
                                                {
                                                    break;
                                                }
                                            }
                                            //dd($ini_array);

                                    }
                                    
                                    if($qty_required > 0.0000)
                                    {
                                        $__supply = $qty_required;

                                        $allocation_item = AllocationItem::FirstOrCreate([
                                            'auto_allocation_id'        => $auto_allocation_id,
                                            'lc_date'                   => $lc_date,
                                            'c_bpartner_id'             => $c_bpartner_id,
                                            'c_order_id'                => $c_order_id,
                                            'supplier_code'             => $supplier_code,
                                            'supplier_name'             => $supplier_name,
                                            'document_no'               => $document_no,
                                            'item_code'                 => $item_code,
                                            'item_id_book'              => $item_id_book,
                                            'item_code_source'          => $item_code_source,
                                            'item_id_source'            => $item_id_source,
                                            'item_desc'                 => $item_desc,
                                            'category'                  => $category,
                                            'uom'                       => $uom,
                                            'job'                       => $job_order,
                                            '_style'                    => $_style,
                                            'style'                     => $style,
                                            'article_no'                => $article_no,
                                            'po_buyer'                  => $new_po_buyer,
                                            'old_po_buyer_reroute'      => $old_po_buyer,
                                            'warehouse'                 => $warehouse_production_id,
                                            'warehouse_inventory'       => $warehouse_id,
                                            'is_need_to_handover'       => false,
                                            'qty_booking'               => $__supply,
                                            'is_from_allocation_fabric' => true,
                                            'is_additional'             => $is_additional,
                                            'user_id'                   => $user_id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'type_stock_buyer'          => ($_po_buyer)? $_po_buyer->type_stock : null,
                                            'confirm_date'              => Carbon::now(),
                                            'remark'                    => $allocation_source.($remark_update ? ', '.$remark_update :null),
                                            'confirm_user_id'           => $system->id,
                                            'deleted_at'                => null
                                    ]);

                                    $qty_required       -= $__supply;
                                    $qty_outstanding    -= $__supply;
                                    $qty_allocated      += $__supply;
                                    $buyers []           = $new_po_buyer;

                                    }
                                }
                                
                                if($is_additional == true)
                                    {
                                        //tambahkan additional ke preparation 
                                        // $is_material_preparation_exists = MaterialPreparationFabric::where([
                                        //     ['c_order_id',$c_order_id],
                                        //     ['article_no',$article_no],
                                        //     ['warehouse_id',$warehouse_id],
                                        //     ['item_id_book',$item_id_book],
                                        //     ['item_id_source',$item_id_source],
                                        //     ['planning_date',Carbon::now()->format('Y-m-d')],
                                        //     ['is_piping',false],
                                        //     ['_style',$style],
                                        //     ['is_from_additional', false],
                                        //     ['total_reserved_qty', $qty_allocation]
                                        // ])
                                        // ->first();
                                
                                        // if(!$is_material_preparation_exists)
                                        // {
                                            $check_material_preparation_fab = db::table('material_preparation_fabrics')
                                            ->where('auto_allocation_id', $auto_allocation_id)
                                            ->where('document_no',$document_no)
                                            ->where('item_code', $item_code )
                                            ->where('po_buyer', $new_po_buyer)
                                            ->exists();

                                             //dd($check_material_preparation_fab);

                                            if($check_material_preparation_fab == false){

                                                $material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                                                    'c_bpartner_id'         => $c_bpartner_id,
                                                    'supplier_name'         => $supplier_name,
                                                    'document_no'           => $document_no,
                                                    'po_buyer'              => $new_po_buyer,    
                                                    'item_code'             => $item_code,
                                                    'item_code_source'      => $item_code_source,
                                                    'c_order_id'            => $c_order_id,
                                                    'item_id_book'          => $item_id_book,
                                                    'item_id_source'        => $item_id_source,
                                                    'uom'                   => 'YDS',
                                                    'total_reserved_qty'    => sprintf('%0.8f',$qty_allocation),
                                                    'total_qty_outstanding' => sprintf('%0.8f',$qty_allocation),
                                                    'planning_date'         => Carbon::now()->format('Y-m-d'),
                                                    'is_piping'             => false,
                                                    'article_no'            => $article_no,
                                                    '_style'                => $style,
                                                    'is_from_additional'    => true,
                                                    'is_balance_marker'     => false,
                                                    'preparation_date'      => null,
                                                    'preparation_by'        => null,
                                                    'warehouse_id'          => $warehouse_id,
                                                    'user_id'               => '91',
                                                    'created_at'            => $movement_date,
                                                    'updated_at'            => $movement_date,
                                                    'auto_allocation_id'    => $auto_allocation_id,
                                                ]);
                                            }


                                        // }
                                    }
                            }
                        }
                    }
                    
                }
                
                //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
                
                if(floatVal($qty_outstanding)<= 0)
                {
                    $auto_allocation->is_already_generate_form_booking  = true;
                    $auto_allocation->generate_form_booking             = carbon::now();
                    $auto_allocation->qty_outstanding                   = 0;
                }else
                {
                    $auto_allocation->qty_outstanding                   = sprintf('%0.8f',$qty_outstanding);
                
                }

                $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                $auto_allocation->locked_date                           = null;
                $auto_allocation->save();

                Temporary::where([
                    ['barcode',$auto_allocation_id],
                    ['status','update_auto_allocation_fabric'],
                ])
                ->delete();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
        }

        PlanningFabric::getAllocationFabricPerBuyer($buyers);
    }

    static function cancelPoBuyer($cancel_reason,$po_buyer)
    {
        $upload_date                = carbon::now();
        $auto_allocations           = AutoAllocation::where([
            ['po_buyer',$po_buyer],
            ['status_po_buyer','active'],
        ])
        ->whereNull('deleted_at')
        ->get();

        foreach ($auto_allocations as $key => $auto_allocation)
        {
            try
            {
                db::beginTransaction();
                $auto_allocation_id  = $auto_allocation->id;
                $document_no         = strtoupper($auto_allocation->document_no);
                $c_order_id          = $auto_allocation->c_order_id;
                $c_bpartner_id       = $auto_allocation->c_bpartner_id;
                $warehouse_id        = $auto_allocation->warehouse_id;
                $type_stock_erp_code = $auto_allocation->type_stock_erp_code;
                $type_stock          = $auto_allocation->type_stock;
                $uom                 = $auto_allocation->uom;
                $lc_date             = $auto_allocation->lc_date;
                $is_fabric           = $auto_allocation->is_fabric;
                $item_code_source    = $auto_allocation->item_code_source;
                $item_code_book      = $auto_allocation->item_code_book;
                $category            = $auto_allocation->category;
                $item_desc           = $auto_allocation->item_desc;
                $po_buyer            = $auto_allocation->po_buyer;
                $old_qty_allocation  = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $qty_outstanding     = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $qty_allocated       = sprintf('%0.8f',$auto_allocation->qty_allocated);
                $old_qty_outstanding = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                $item_id_source      = $auto_allocation->item_id_source;
                $item_id_book        = $auto_allocation->item_id_book;
                $supplier_name       = $auto_allocation->supplier_name;
                
                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
                
                if($is_fabric)
                {
                    $allocation_items = AllocationItem::where('auto_allocation_id', $auto_allocation_id)->get();
                    foreach ($allocation_items as $key => $allocation_item) 
                    {
                        // belum tau mau balikin stock nya gimana
                        $allocation_item->delete();
                    }

                    $auto_allocation->update_user_id                    = (isset(auth::user()->id) ? auth::user()->id : $system->id);
                    $auto_allocation->qty_outstanding                   = $auto_allocation->qty_allocation;
                    $auto_allocation->qty_allocated                     = 0;
                    $auto_allocation->is_already_generate_form_booking  = false;
                    $auto_allocation->generate_form_booking             = null;
                    $auto_allocation->status_po_buyer                   = 'cancel';
                    $auto_allocation->cancel_date                       = $upload_date;
                    $auto_allocation->save();

                    HistoryAutoAllocations::Create([
                        'auto_allocation_id'        => $auto_allocation->id,
                        'note'                      => 'PO ini cancel pada tanggal '.$upload_date.' , dengan alasan '.$cancel_reason,
                        'is_integrate'              => true,
                        'user_id'                   => $system->id
                    ]);
                }else
                {
                    if(!$auto_allocation->is_allocation_purchase)
                    {
                        $auto_allocation->update_user_id                = $system->id;
                        $auto_allocation->deleted_at                    = $upload_date;
                        $auto_allocation->status_po_buyer               = 'cancel';
                        $auto_allocation->cancel_date                   = $upload_date;
                        
                        if($qty_allocated > 0.0000)
                        {
                            $auto_allocation->qty_allocation                    = $qty_allocated;
                            $auto_allocation->qty_outstanding                   = 0;
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = $upload_date;
                            $auto_allocation->save();
                        }

                        HistoryAutoAllocations::Create([
                            'auto_allocation_id'        => $auto_allocation->id,
                            'note'                      => 'PO ini cancel pada tanggal '.carbon::now().' , dengan alasan '.$cancel_reason,
                            'is_integrate'              => true,
                            'user_id'                   => $system->id
                        ]);
                        
                        $auto_allocation->save();
                        
                    }else
                    {
                        if($qty_outstanding > 0)
                        {
                            if(($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP') && $c_bpartner_id == 'FREE STOCK')
                            {
                                $c_bpartner_id                  = 'FREE STOCK';
                                $supplier_code                  = 'FREE STOCK';
                
                                $mapping_stock_id               = null;
                                $type_stock_erp_code            = '2';
                                $type_stock                     = 'REGULER';
                                
                            }else
                            {
                                
                                $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                               
                                if($_supplier) $supplier_code   = $_supplier->supplier_code;
                                else $supplier_code             = null;
                
                                $get_4_digit_from_beginning     = substr($document_no,0, 4);
                                $_mapping_stock_4_digt          = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                                
                                if($_mapping_stock_4_digt)
                                {
                                    $mapping_stock_id           = $_mapping_stock_4_digt->id;
                                    $type_stock_erp_code        = $_mapping_stock_4_digt->type_stock_erp_code;
                                    $type_stock                 = $_mapping_stock_4_digt->type_stock;

                                }else
                                {
                                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                                    $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                   
                                    if($_mapping_stock_5_digt)
                                    {
                                        $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                                    }else
                                    {
                                        $_po_buyer              = PoBuyer::where('po_buyer',$po_buyer)->first();
                                        $mapping_stock_id       = null;
                                        $type_stock_erp_code    = ($_po_buyer)? $_po_buyer->type_stock_erp_code : NULL;
                                        $type_stock             = ($_po_buyer)? $_po_buyer->type_stock : null;
                                    }
                                }
                            }

                            $check_material_stock = MaterialStock::where([
                                ['c_order_id',$c_order_id],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['item_code',$item_code_source],
                                ['warehouse_id',$warehouse_id],
                                ['is_stock_on_the_fly',true],
                                ['is_running_stock',false],
                                ['stock',$qty_outstanding],
                                ['type_stock_erp_code',$type_stock_erp_code],
                            ]);

                            if($po_buyer) $check_material_stock = $check_material_stock->where('po_buyer',$po_buyer);
                            $check_material_stock = $check_material_stock->exists();

                            if(!$check_material_stock)
                            {
                                $material_stock = MaterialStock::FirstOrCreate([
                                    'type_stock_erp_code'   => $type_stock_erp_code,
                                    'type_stock'            => $type_stock,
                                    'supplier_code'         => $supplier_code,
                                    'supplier_name'         => $supplier_name,
                                    'document_no'           => $document_no,
                                    'c_bpartner_id'         => $c_bpartner_id,
                                    'c_order_id'            => $c_order_id,
                                    'item_id'               => $item_id_source,
                                    'item_code'             => $item_code_source,
                                    'po_buyer'              => ($po_buyer ? $po_buyer : null),
                                    'item_desc'             => $item_desc,
                                    'category'              => $category,
                                    'type_po'               => 2,
                                    'warehouse_id'          => $warehouse_id,
                                    'uom'                   => $uom,
                                    'qty_carton'            => 1,
                                    'stock'                 => $qty_outstanding,
                                    'remark'                => 'PO CANCEL '.$cancel_reason,
                                    'reserved_qty'          => 0,
                                    'created_at'            => $upload_date,
                                    'updated_at'            => $upload_date,
                                    'is_active'             => true,
                                    'is_stock_on_the_fly'   => true,
                                    'source'                => 'STOCK ON THE FLY DUE CANCEL FROM '.$po_buyer,
                                    'user_id'               => $system->id
                                ]);

                                HistoryStock::approved($material_stock->id
                                ,$material_stock->stock
                                ,'0'
                                ,null
                                ,null
                                ,null
                                ,null
                                ,null
                                ,'PO CANCEL '.$material_stock->po_buyer.' '.$cancel_reason
                                ,$material_stock->user->name
                                ,$material_stock->user_id
                                ,$material_stock->type_stock_erp_code
                                ,$material_stock->type_stock
                                ,false
                                ,false);
                            }
                        }

                        HistoryAutoAllocations::Create([
                            'auto_allocation_id'        => $auto_allocation->id,
                            'note'                      => 'PO ini cancel pada tanggal '.$upload_date.' , dengan alasan '.$cancel_reason,
                            'is_integrate'              => true,
                            'user_id'                   => $system->id
                        ]);

                        $auto_allocation->status_po_buyer   = 'cancel';
                        $auto_allocation->cancel_date       = $upload_date;
                        $auto_allocation->save();
                    }
                }
                
                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    static function exportUpdateFileAccExcel()
    {
        $location = Config::get('storage.auto_allocation');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);
        
        $auto_allocations = AutoAllocation::where([
            ['qty_allocated','0'],
            ['status_po_buyer','active'],
        ])
        ->whereNull('deleted_at')
        ->whereIn('warehouse_id',['1000013','1000002'])
        ->orderby('lc_date','desc')
        ->orderby('warehouse_name','asc')
        ->get();

        $file = Excel::create('form_auto_allocation_acc',function($excel) use($auto_allocations){
            $excel->sheet('active', function($sheet){
                //$sheet->setCellValue('A1','ID');
                $sheet->setCellValue('A1','BOOKING NUMBER');
                //$sheet->setCellValue('C1','SUPPLIER_CODE_OLD');
                $sheet->setCellValue('B1','SUPPLIER_CODE_NEW');
                //$sheet->setCellValue('E1','NO_PO_SUPPLIER_OLD');
                $sheet->setCellValue('C1','NO_PO_SUPPLIER_NEW');
                //$sheet->setCellValue('G1','ITEM_CODE_SOURCE_OLD');
                $sheet->setCellValue('D1','ITEM_CODE_SOURCE_NEW');
                //$sheet->setCellValue('I1','ITEM_CODE_BOOK_OLD');
                $sheet->setCellValue('E1','ITEM_CODE_BOOK_NEW');
                $sheet->setCellValue('F1','UOM');
                //$sheet->setCellValue('L1','PO_BUYER_OLD');
                $sheet->setCellValue('G1','PO_BUYER_NEW');
                //$sheet->setCellValue('N1','QTY_INPUT_OLD');
                $sheet->setCellValue('H1','QTY_INPUT_NEW');
                //$sheet->setCellValue('P1','WAREHOUSE_INVENTORY_OLD');
                $sheet->setCellValue('I1','WAREHOUSE_INVENTORY_NEW');
                //$sheet->setCellValue('R1','IS_ADDITIONAL_OLD');
                $sheet->setCellValue('J1','IS_ADDITIONAL_NEW');
                //$sheet->setCellValue('T1','REMARK_ADDITIONAL_OLD');
                $sheet->setCellValue('K1','REMARK_ADDITIONAL_NEW');
                $sheet->setCellValue('L1','REASON_UPDATE');

                $sheet->setAutoSize(true);
                /*$sheet->cell('A1', function($cell) {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });*/
                $sheet->cell('B1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('E1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('F1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('G1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('H1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('I1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('J1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('K1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('L1', function($cell) {
                    $cell->setBackground('#DC143C');
                    $cell->setFontColor('#ffffff');
                });

                $sheet->setColumnFormat(array(
                    'G' => '@',
                ));
            });

            $excel->sheet('data_allocation',function($sheet) use($auto_allocations){
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','BOOKING NUMBER');
                $sheet->setCellValue('C1','SUPPLIER_CODE_OLD');
                $sheet->setCellValue('D1','SUPPLIER_CODE_NEW');
                $sheet->setCellValue('E1','NO_PO_SUPPLIER_OLD');
                $sheet->setCellValue('F1','NO_PO_SUPPLIER_NEW');
                $sheet->setCellValue('G1','ITEM_CODE_SOURCE_OLD');
                $sheet->setCellValue('H1','ITEM_CODE_SOURCE_NEW');
                $sheet->setCellValue('I1','ITEM_CODE_BOOK_OLD');
                $sheet->setCellValue('J1','ITEM_CODE_BOOK_NEW');
                $sheet->setCellValue('K1','UOM');
                $sheet->setCellValue('L1','PO_BUYER_OLD');
                $sheet->setCellValue('M1','PO_BUYER_NEW');
                $sheet->setCellValue('N1','QTY_INPUT_OLD');
                $sheet->setCellValue('O1','QTY_INPUT_NEW');
                $sheet->setCellValue('P1','WAREHOUSE_INVENTORY_OLD');
                $sheet->setCellValue('Q1','WAREHOUSE_INVENTORY_NEW');
                $sheet->setCellValue('R1','IS_ADDITIONAL_OLD');
                $sheet->setCellValue('S1','IS_ADDITIONAL_NEW');
                $sheet->setCellValue('T1','REMARK_ADDITIONAL_OLD');
                $sheet->setCellValue('U1','REMARK_ADDITIONAL_NEW');
                $sheet->setCellValue('V1','REASON_UPDATE');

                $sheet->setAutoSize(true);

                $row=2;
                foreach ($auto_allocations as $auto_allocation) 
                {  
                    if($auto_allocation->c_bpartner_id == 'FREE STOCK') $supplier_code = 'FREE STOCK';
                    else{
                        $_supplier = Supplier::where('c_bpartner_id',$auto_allocation->c_bpartner_id)->first();
                        $supplier_code = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                    }
                   

                    $sheet->setCellValue('A'.$row,$auto_allocation->id);
                    $sheet->setCellValue('B'.$row,$auto_allocation->document_allocation_number);
                    $sheet->setCellValue('C'.$row,$supplier_code);
                    $sheet->setCellValue('D'.$row,null);
                    $sheet->setCellValue('E'.$row,$auto_allocation->document_no);
                    $sheet->setCellValue('F'.$row,null);
                    $sheet->setCellValue('G'.$row,$auto_allocation->item_code_source);
                    $sheet->setCellValue('H'.$row,null);
                    $sheet->setCellValue('I'.$row,$auto_allocation->item_code);
                    $sheet->setCellValue('J'.$row,null);
                    $sheet->setCellValue('K'.$row,$auto_allocation->uom);
                    $sheet->setCellValue('L'.$row,$auto_allocation->po_buyer);
                    $sheet->setCellValue('M'.$row,null);
                    $sheet->setCellValue('N'.$row,$auto_allocation->qty_allocation);
                    $sheet->setCellValue('O'.$row,null);
                    $sheet->setCellValue('P'.$row,$auto_allocation->warehouse_name);
                    $sheet->setCellValue('Q'.$row,null);
                    $sheet->setCellValue('R'.$row,$auto_allocation->is_additional);
                    $sheet->setCellValue('S'.$row,null);
                    $sheet->setCellValue('T'.$row,$auto_allocation->remark_additional);
                    $sheet->setCellValue('U'.$row,null);
                    $sheet->setCellValue('V'.$row,null);

                    $sheet->cell('A'.$row, function($cell) {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->cell('D'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('F'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('H'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('J'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('K'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('M'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('O'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    
                    $sheet->cell('Q'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('S'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });

                    $sheet->cell('U'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });

                    $sheet->cell('V'.$row, function($cell) {
                        $cell->setBackground('#DC143C');
                        $cell->setFontColor('#ffffff');
                    });
                  $row++;
                }


                $sheet->cell('A1', function($cell) {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('F1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('H1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('J1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('K1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('M1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('O1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('Q1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('S1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('U1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('V1', function($cell) {
                    $cell->setBackground('#DC143C');
                    $cell->setFontColor('#ffffff');
                });

                

                $sheet->setColumnFormat(array(
                    'B' => '@',
                    'M' => '@',
                ));
            });
        })
        ->save('xlsx', $location);
    }

    static function exportUpdateFileFabExcel()
    {
        $location = Config::get('storage.auto_allocation');
        if (!File::exists($location)) File::makeDirectory($location, 0777, true);
        
        $now = Carbon::today()->format('Y-m-d');
       
        $auto_allocations = AutoAllocation::where([
            ['status_po_buyer','active'],
            ['promise_date','>',$now],
            //['po_buyer','0122595845']
        ])
        ->whereNull('deleted_at')
        ->whereIn('warehouse_id',['1000011','1000001'])
        ->orderby('lc_date','desc')
        ->orderby('warehouse_name','asc')
        ->get();

        $file = Excel::create('form_auto_allocation_fab',function($excel) use($auto_allocations){
            $excel->sheet('active', function($sheet){
                //$sheet->setCellValue('A1','ID');
                $sheet->setCellValue('A1','BOOKING NUMBER');
                //$sheet->setCellValue('C1','SUPPLIER_CODE_OLD');
                $sheet->setCellValue('B1','SUPPLIER_CODE_NEW');
                //$sheet->setCellValue('E1','NO_PO_SUPPLIER_OLD');
                $sheet->setCellValue('C1','NO_PO_SUPPLIER_NEW');
                //$sheet->setCellValue('G1','ITEM_CODE_SOURCE_OLD');
                $sheet->setCellValue('D1','ITEM_CODE_SOURCE_NEW');
                //$sheet->setCellValue('I1','ITEM_CODE_BOOK_OLD');
                $sheet->setCellValue('E1','ITEM_CODE_BOOK_NEW');
                $sheet->setCellValue('F1','UOM');
                //$sheet->setCellValue('L1','PO_BUYER_OLD');
                $sheet->setCellValue('G1','PO_BUYER_NEW');
                //$sheet->setCellValue('N1','QTY_INPUT_OLD');
                $sheet->setCellValue('H1','QTY_INPUT_NEW');
                //$sheet->setCellValue('P1','WAREHOUSE_INVENTORY_OLD');
                $sheet->setCellValue('I1','WAREHOUSE_INVENTORY_NEW');
                //$sheet->setCellValue('R1','IS_ADDITIONAL_OLD');
                $sheet->setCellValue('J1','IS_ADDITIONAL_NEW');
                //$sheet->setCellValue('T1','REMARK_ADDITIONAL_OLD');
                $sheet->setCellValue('K1','REMARK_ADDITIONAL_NEW');
                $sheet->setCellValue('L1','REASON_UPDATE');

                $sheet->setAutoSize(true);
                /*$sheet->cell('A1', function($cell) {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });*/
                $sheet->cell('B1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('C1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('E1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('F1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('G1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('H1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('I1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('J1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('K1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('L1', function($cell) {
                    $cell->setBackground('#DC143C');
                    $cell->setFontColor('#ffffff');
                });

                $sheet->setColumnFormat(array(
                    'G' => '@',
                ));
            });

            $excel->sheet('data_allocation',function($sheet) use($auto_allocations){
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','BOOKING NUMBER');
                $sheet->setCellValue('C1','SUPPLIER_CODE_OLD');
                $sheet->setCellValue('D1','SUPPLIER_CODE_NEW');
                $sheet->setCellValue('E1','NO_PO_SUPPLIER_OLD');
                $sheet->setCellValue('F1','NO_PO_SUPPLIER_NEW');
                $sheet->setCellValue('G1','ITEM_CODE_SOURCE_OLD');
                $sheet->setCellValue('H1','ITEM_CODE_SOURCE_NEW');
                $sheet->setCellValue('I1','ITEM_CODE_BOOK_OLD');
                $sheet->setCellValue('J1','ITEM_CODE_BOOK_NEW');
                $sheet->setCellValue('K1','UOM');
                $sheet->setCellValue('L1','PO_BUYER_OLD');
                $sheet->setCellValue('M1','PO_BUYER_NEW');
                $sheet->setCellValue('N1','QTY_INPUT_OLD');
                $sheet->setCellValue('O1','QTY_INPUT_NEW');
                $sheet->setCellValue('P1','WAREHOUSE_INVENTORY_OLD');
                $sheet->setCellValue('Q1','WAREHOUSE_INVENTORY_NEW');
                $sheet->setCellValue('R1','IS_ADDITIONAL_OLD');
                $sheet->setCellValue('S1','IS_ADDITIONAL_NEW');
                $sheet->setCellValue('T1','REMARK_ADDITIONAL_OLD');
                $sheet->setCellValue('U1','REMARK_ADDITIONAL_NEW');
                $sheet->setCellValue('V1','REASON_UPDATE');

                $sheet->setAutoSize(true);

                $row=2;
                foreach ($auto_allocations as $auto_allocation) 
                {  
                    if($auto_allocation->c_bpartner_id == 'FREE STOCK') $supplier_code = 'FREE STOCK';
                    else{
                        $_supplier = Supplier::where('c_bpartner_id',$auto_allocation->c_bpartner_id)->first();
                        $supplier_code = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                    }
                   

                    $sheet->setCellValue('A'.$row,$auto_allocation->id);
                    $sheet->setCellValue('B'.$row,$auto_allocation->document_allocation_number);
                    $sheet->setCellValue('C'.$row,$supplier_code);
                    $sheet->setCellValue('D'.$row,null);
                    $sheet->setCellValue('E'.$row,$auto_allocation->document_no);
                    $sheet->setCellValue('F'.$row,null);
                    $sheet->setCellValue('G'.$row,$auto_allocation->item_code_source);
                    $sheet->setCellValue('H'.$row,null);
                    $sheet->setCellValue('I'.$row,$auto_allocation->item_code);
                    $sheet->setCellValue('J'.$row,null);
                    $sheet->setCellValue('K'.$row,$auto_allocation->uom);
                    $sheet->setCellValue('L'.$row,$auto_allocation->po_buyer);
                    $sheet->setCellValue('M'.$row,null);
                    $sheet->setCellValue('N'.$row,$auto_allocation->qty_allocation);
                    $sheet->setCellValue('O'.$row,null);
                    $sheet->setCellValue('P'.$row,$auto_allocation->warehouse_name);
                    $sheet->setCellValue('Q'.$row,null);
                    $sheet->setCellValue('R'.$row,$auto_allocation->is_additional);
                    $sheet->setCellValue('S'.$row,null);
                    $sheet->setCellValue('T'.$row,$auto_allocation->remark_additional);
                    $sheet->setCellValue('U'.$row,null);
                    $sheet->setCellValue('V'.$row,null);

                    $sheet->cell('A'.$row, function($cell) {
                        $cell->setBackground('#000000');
                        $cell->setFontColor('#ffffff');
                    });
                    $sheet->cell('D'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('F'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('H'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('J'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('K'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('M'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('O'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    
                    $sheet->cell('Q'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });
                    $sheet->cell('S'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });

                    $sheet->cell('U'.$row, function($cell) {
                        $cell->setBackground('#FFFF00');
                    });

                    $sheet->cell('V'.$row, function($cell) {
                        $cell->setBackground('#DC143C');
                        $cell->setFontColor('#ffffff');
                    });
                  $row++;
                }


                $sheet->cell('A1', function($cell) {
                    $cell->setBackground('#000000');
                    $cell->setFontColor('#ffffff');
                });
                $sheet->cell('D1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('F1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('H1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('J1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('K1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('M1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('O1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('Q1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                $sheet->cell('S1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });
                
                $sheet->cell('U1', function($cell) {
                    $cell->setBackground('#FFFF00');
                });

                $sheet->cell('V1', function($cell) {
                    $cell->setBackground('#DC143C');
                    $cell->setFontColor('#ffffff');
                });

                

                $sheet->setColumnFormat(array(
                    'B' => '@',
                    'M' => '@',
                ));
            });
        })
        ->save('xlsx', $location);
    }

    public function etaDelay()
    {
        return view('master_data_auto_allocation.eta_delay');
    }

    public function downloadEtaDelay()
    {
        return Excel::create('upload_eta_delay',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('B1','ETA_DELAY');
                $sheet->setCellValue('C1','REMARK_DELAY');
                $sheet->setCellValue('D1','REMARK_UPDATE');
                $sheet->setCellValue('E1','IS_CLOSED');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'C' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadEtaDelay(Request $request)
    {

        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    $auto_allocation_id = $value->auto_allocation_id;
                    $eta_delay          = $value->eta_delay;
                    $remark_update      = $value->remark_update;
                    $remark_delay       = $value->remark_delay;
                    $eta_delay          = ($eta_delay ? Carbon::parse($eta_delay)->format('Y-m-d') : null);
                    $is_closed          = ($value->is_closed ? $value->is_closed : false);

                    $auto_allocation = AutoAllocation::find($auto_allocation_id);
                    if($auto_allocation) 
                    {
                        $auto_allocation->eta_delay = $eta_delay;
                        $auto_allocation->is_closed = $is_closed;
                        $auto_allocation->remark_delay = $remark_delay;
                        $auto_allocation->remark_update = $remark_update;
                        $auto_allocation->save();

                        $obj                 = new stdClass();
                        $obj->eta_delay      = $eta_delay;
                        $obj->document_no    = $auto_allocation->document_no;
                        $obj->item_code      = $auto_allocation->item_code_book;
                        $obj->po_buyer       = $auto_allocation->po_buyer;
                        $obj->uom            = $auto_allocation->uom;
                        $obj->qty_allocation = $auto_allocation->qty_allocation;
                        $obj->is_closed      = $auto_allocation->is_closed;
                        $obj->warehouse_name = $auto_allocation->warehouse_name;
                        $obj->remark_delay   = $auto_allocation->remark_delay;
                        $obj->remark_update   = $auto_allocation->remark_update;
                        $obj->error_upload   = false;
                        $obj->remark         = 'SUCCESS, UPDATE BERHASIL';
                        $array []            = $obj;
                    } 
                    else
                    {
                        $obj                 = new stdClass();
                        $obj->eta_delay      = '-';
                        $obj->document_no    = '-';
                        $obj->item_code      = '-';
                        $obj->po_buyer       = '-';
                        $obj->uom            = '-';
                        $obj->qty_allocation = '-';
                        $obj->is_closed      = '-';
                        $obj->warehouse_name = '-';
                        $obj->remark_delay = '-';
                        $obj->remark_update   = '-';
                        $obj->error_upload   = true;
                        $obj->remark         = 'ERROR, DATA '.$auto_allocation_id.' TIDAK DITEMUKAN' ;
                        $array []                   = $obj;

                    }                  
                }
            }
        }

        return response()->json($array,'200');

    }

    public function recalculateDashboard()
    {
        return view('master_data_auto_allocation.recalculate_dashboard');
    }

    public function downloadRecalculateDashboard()
    {
        return Excel::create('upload_recalculate_dashboard',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','DOCUMENT_NO');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'C' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadRecalculateDashboard(Request $request)
    {

        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                // $refresh = DB::select(db::raw("
                // refresh materialized view monitoring_allocations_mv;"
                // ));
                foreach ($data as $key => $value) 
                {
                    $document_no = strtoupper($value->document_no);
                    $item_code          = $value->item_code;

                    $auto_allocations = AutoAllocation::select('c_order_id', 'item_id_source', 'document_no', 'item_code', 'warehouse_id')
                                      ->where('document_no', $document_no)
                                      ->where('item_code', $item_code)
                                      ->whereNull('deleted_at')
                                      ->groupBy('c_order_id', 'item_id_source', 'document_no', 'item_code', 'warehouse_id')
                                      ->get();

                    if(count($auto_allocations) > 0) 
                    {
                        foreach($auto_allocations as $key => $auto_allocation)
                        {
                            $c_order_id   = $auto_allocation->c_order_id;
                            $item_id      = $auto_allocation->item_id_source;
                            $warehouse_id = $auto_allocation->warehouse_id;
                            Dashboard::dailyUpdateMonitoringMaterial($c_order_id, $item_id, $warehouse_id);

                            $obj                 = new stdClass();
                            $obj->document_no    = $document_no;
                            $obj->item_code      = $item_code;
                            $obj->error_upload   = false;
                            $obj->remark         = 'SUCCESS, UPDATE BERHASIL';
                            $array []            = $obj;
                        }
                    } 
                    else
                    {
                        $obj                 = new stdClass();
                        $obj->document_no    = '-';
                        $obj->item_code      = '-';
                        $obj->error_upload   = true;
                        $obj->remark         = 'ERROR, DATA PO SUPPLIER'.$document_no.' ITEM CODE'. $item_code. 'TIDAK DITEMUKAN' ;
                        $array []            = $obj;

                    }                  
                }
            }
        }

        return response()->json($array,'200');

    }

    public function removeFromDashboard()
    {
        return view('master_data_auto_allocation.remove_from_dashboard');
    }

    public function downloadRemoveFromDashboard()
    {
        return Excel::create('upload_remove_from_dashboard',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('B1','IS_REMOVE');
                $sheet->setCellValue('C1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function uploadRemoveFromDashboard(Request $request)
    {

        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    //dd($value);
                    $auto_allocation_id       = $value->auto_allocation_id;
                    $remark_remove            = $value->remark;
                    $is_remove_from_dashboard = ($value->is_remove ? $value->is_remove : false);

                    $auto_allocation = AutoAllocation::find($auto_allocation_id);
                    if($auto_allocation) 
                    {

                        $auto_allocation->is_remove_from_dashboard = $is_remove_from_dashboard;
                        $auto_allocation->remark_remove            = $remark_remove;
                        $auto_allocation->user_id_remove           = Auth::user()->id;
                        $auto_allocation->save();

                        $obj                           = new stdClass();
                        $obj->document_no              = $auto_allocation->document_no;
                        $obj->item_code                = $auto_allocation->item_code_book;
                        $obj->po_buyer                 = $auto_allocation->po_buyer;
                        $obj->uom                      = $auto_allocation->uom;
                        $obj->qty_allocation           = $auto_allocation->qty_allocation;
                        $obj->is_remove_from_dashboard = $auto_allocation->is_remove_from_dashboard;
                        $obj->is_closed                = $auto_allocation->is_closed;
                        $obj->warehouse_name           = $auto_allocation->warehouse_name;
                        $obj->remark_remove            = $auto_allocation->remark_remove;
                        $obj->error_upload             = false;
                        $obj->remark                   = 'SUCCESS, UPDATE BERHASIL';
                        $array []                      = $obj;
                    } 
                    else
                    {
                        $obj                 = new stdClass();
                        $obj->eta_delay      = '-';
                        $obj->document_no    = '-';
                        $obj->item_code      = '-';
                        $obj->po_buyer       = '-';
                        $obj->uom            = '-';
                        $obj->qty_allocation = '-';
                        $obj->is_closed      = '-';
                        $obj->warehouse_name = '-';
                        $obj->remark_delay = '-';
                        $obj->remark_update   = '-';
                        $obj->error_upload   = true;
                        $obj->remark         = 'ERROR, DATA '.$auto_allocation_id.' TIDAK DITEMUKAN' ;
                        $array []                   = $obj;

                    }                  
                }
            }
        }

        return response()->json($array,'200');

    }

}
