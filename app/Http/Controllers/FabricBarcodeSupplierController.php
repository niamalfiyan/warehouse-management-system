<?php namespace App\Http\Controllers;

use DB;
use View;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Item;
use App\Models\Barcode;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialFebric;
use App\Models\ItemPackingList;
use App\Models\MaterialArrival;
use App\Models\MaterialFebricDev;
use App\Models\SummaryStockFabric;
use App\Models\PurchaseOrderDetail;
use App\Models\DetailMaterialStock;
use App\Models\MovementStockHistory;
use App\Models\DetailMaterialArrival;
use App\Models\PurchaseOrderDetailDev;
use App\Models\MaterialReadyPreparation;
use App\Models\MonitoringReceivingFabric;

class FabricBarcodeSupplierController extends Controller
{
    public function index(request $request)
    {
        // return view('errors.migration');
        //dd($request->ajax());
        if ($request->ajax()) 
        {
            $warehouse_id       = $request->warehouse_id;
            $no_surat_jalan     = strtoupper(trim($request->no_surat_jalan));
            $document_no        = strtoupper(trim($request->document_no));
            $item_code          = strtoupper(trim($request->item_code));
            $no_invoice         = strtoupper(trim($request->no_invoice));

            $app_env = Config::get('app.env');
            if($app_env == 'live')
            {
                $po_details = PurchaseOrderDetail::select(db::raw('max(po_detail_id) as po_detail_id'),'c_order_id','c_orderline_id','c_bpartner_id','no_packinglist','supplier','documentno','item','kst_invoicevendor','flag_wms','kst_season','qty_upload','uomsymbol')
                ->where([
                    ['type_po',1],
                    ['isactive',true],
                    ['flag_erp',true],
                    ['is_locked',true],
                    ['m_warehouse_id',$warehouse_id]
                ]);
            }else
            {
                $po_details = PurchaseOrderDetailDev::select(db::raw('max(po_detail_id) as po_detail_id'),'c_order_id','c_orderline_id','c_bpartner_id','no_packinglist','supplier','documentno','item','kst_invoicevendor','flag_wms','kst_season','qty_upload','uomsymbol')
                ->where([
                    ['type_po',1],
                    ['isactive',true],
                    ['flag_erp',true],
                    ['is_locked',true],
                    ['m_warehouse_id',$warehouse_id]
                ]);
            }
            

            if($document_no != null || $document_no !='') $po_details = $po_details->where(db::raw('upper(trim(documentno))'),'LIKE',"%$document_no%");
            if($no_surat_jalan != null || $no_surat_jalan !='') $po_details = $po_details->where(db::raw('upper(trim(no_packinglist))'),'LIKE',"%$no_surat_jalan%");
            if($item_code != null || $item_code !='') $po_details = $po_details->where(db::raw('upper(trim(item))'),'LIKE',"%$item_code%");
            if($no_invoice != null || $no_invoice !='') $po_details = $po_details->where(db::raw('upper(trim(kst_invoicevendor))'),'LIKE',"%$no_invoice%");

            $po_details = $po_details->groupby('no_packinglist','c_order_id','c_orderline_id','c_bpartner_id','supplier','documentno','item','kst_invoicevendor','flag_wms','kst_season','qty_upload','uomsymbol')
            ->orderby('no_packinglist','asc')
            ->orderby('kst_invoicevendor','asc')
            ->orderby('documentno','asc')
            ->orderby('item','asc')
            ->paginate(20);

            $data = [
                'view' => View::make('fabric_barcode_supplier.data',compact('po_details'))->render(),
                'data' => $po_details
            ];

            return response()->json($data);
        }

        return view('fabric_barcode_supplier.index');

    }

    public function store(request $request)
    {
        $validator =  Validator::make($request->all(), [
            'data_print' => 'required|not_in:[]'
        ]);

        if ($validator->passes())
        {
            $app_env                = Config::get('app.env');
            $is_ready_prepared      = array();
            $packing_list           = array();
            $document_no_array      = array();
            $_warehouse_id          = $request->warehouse_id;
            $data_prints            = json_decode($request->data_print);
            $return_print           = '';
            $concatenate            = '';
            $concatenate_detail     = '';
            $material_arrival_ids   = array();
            
            try 
            {
                DB::beginTransaction();

                //dd($data_prints);
                foreach ($data_prints as $key => $data_print)
                {
                    $no_packing_list    = trim($data_print->no_packing_list);
                    $no_invoice         = trim($data_print->no_invoice);
                    $document_no        = trim($data_print->document_no);
                    $item_code          = trim($data_print->item_code);

                    if($app_env == 'live')
                    {
                        $web_po = PurchaseOrderDetail::where([
                            ['po_detail_id',$data_print->po_detail_id],
                            ['type_po',1],
                            ['flag_wms',false],
                        ])
                        ->first();
                    }else
                    {
                        $web_po = PurchaseOrderDetailDev::where([
                            ['po_detail_id',$data_print->po_detail_id],
                            ['type_po',1],
                            ['flag_wms',false],
                        ])
                        ->first();
                    }

                    //return response()->json($web_po);
                    $concatenate .= "'".$data_print->po_detail_id."',";
                    if($web_po)
                    {
                        $packing_list[]         =  $web_po->no_packinglist;
                        $document_no_array []   = trim(strtoupper($web_po->documentno));

                        if(trim($web_po->pobuyer) != '' || trim($web_po->pobuyer) != null) $is_ready_prepared [] = $web_po->po_detail_id;
                        
                        $total_roll         = null;
                        $is_arrival_exists  = MaterialArrival::where([
                            ['po_detail_id',$web_po->po_detail_id],
                            ['warehouse_id',$web_po->m_warehouse_id]
                        ])
                        ->exists();

                        if(!$is_arrival_exists)
                        {
                            if($app_env == 'live')
                            {
                                PurchaseOrderDetail::where([
                                    ['po_detail_id',$web_po->po_detail_id]
                                ])
                                ->update(['flag_wms' => true]);
                            }else
                            {
                                PurchaseOrderDetailDev::where([
                                    ['po_detail_id',$web_po->po_detail_id]
                                ])
                                ->update(['flag_wms' => true]);
                            }

                            $material_arrival = MaterialArrival::firstorcreate([
                                'po_detail_id'              => $web_po->po_detail_id,
                                'item_id'                   => $web_po->m_product_id,
                                'item_code'                 => trim(strtoupper($web_po->item)),
                                'item_desc'                 => trim(strtoupper($web_po->desc_product)),
                                'supplier_name'             => trim(strtoupper($web_po->supplier)),
                                'document_no'               => trim(strtoupper($web_po->documentno)),
                                'type_po'                   => $web_po->type_po,
                                'c_order_id'                => $web_po->c_order_id,
                                'c_orderline_id'            => $web_po->c_orderline_id,
                                'c_bpartner_id'             => $web_po->c_bpartner_id,
                                'warehouse_id'              => $web_po->m_warehouse_id,
                                'po_buyer'                  => trim($web_po->pobuyer),
                                'category'                  => $web_po->category,
                                'uom'                       => trim($web_po->uomsymbol),
                                'no_packing_list'           => $web_po->no_packinglist,
                                'no_resi'                   => $web_po->kst_resi,
                                'no_surat_jalan'            => $web_po->kst_suratjalanvendor,
                                'no_invoice'                => $web_po->kst_invoicevendor,
                                'etd_date'                  => $web_po->kst_etddate,
                                'eta_date'                  => $web_po->kst_etadate,
                                'qty_entered'               => sprintf('%0.8f', $web_po->qtyentered),
                                'qty_ordered'               => sprintf('%0.8f', $web_po->qtyordered),
                                'qty_carton'                => sprintf('%0.8f', $web_po->qty_carton),
                                'qty_delivered'             => sprintf('%0.8f', $web_po->qtydelivered),
                                'qty_upload'                => sprintf('%0.8f', $web_po->qty_upload),
                                'qty_available_rma'         => sprintf('%0.8f', $web_po->qty_upload),
                                'qty_reserved_rma'          => 0,
                                'total_roll'                => $total_roll,
                                'foc'                       => sprintf('%0.8f', $web_po->foc),
                                'is_subcont'                => false,
                                'prepared_status'           => 'NEED PREPARED',
                                'remark'                    => 'NEW',
                                'is_active'                 => true,
                                'jenis_po'                  => strtoupper($web_po->c_bp_group_id),
                                'user_id'                   => Auth::user()->id
                            ]);

                            if($app_env == 'live')
                            {
                                $supplier_material_fabrics = MaterialFebric::where([
                                    ['po_detail_id',$web_po->po_detail_id],
                                    ['flag_wms',false],
                                ])
                                ->orderby('nomor_roll','asc')
                                ->get();
                            }else
                            {
                                $supplier_material_fabrics = MaterialFebricDev::where([
                                    ['po_detail_id',$web_po->po_detail_id],
                                    ['flag_wms',false],
                                ])
                                ->orderby('nomor_roll','asc')
                                ->get();
                            }
                            
                            foreach ($supplier_material_fabrics as $key_material => $supplier_material_fabric) 
                            {
                                $conversion = UomConversion::where([
                                    ['item_code',$web_po->item],
                                    ['uom_from',trim(strtoupper($supplier_material_fabric->uomsymbol))]
                                ])
                                ->first();

                                if($conversion)
                                {
                                    $dividerate     = $conversion->dividerate;
                                    $uom_conversion = $conversion->uom_to;
                                }else
                                {
                                    $dividerate     = 1;
                                    $uom_conversion = $web_po->uomsymbol;
                                }

                                $is_detail_exists = DetailMaterialArrival::where([
                                    ['barcode_supplier',$supplier_material_fabric->barcode_id],
                                    ['batch_number',$supplier_material_fabric->batch_number],
                                    ['nomor_roll',$supplier_material_fabric->nomor_roll],
                                ])
                                ->exists();

                                if(!$is_detail_exists)
                                {
                                    if($app_env == 'live')
                                    {
                                        MaterialFebric::where([
                                            ['barcode_id',$supplier_material_fabric->barcode_id],
                                            ['po_detail_id',$web_po->po_detail_id]
                                        ])
                                        ->update(['flag_wms' => true]);
                                    }else
                                    {
                                        MaterialFebricDev::where([
                                            ['barcode_id',$supplier_material_fabric->barcode_id],
                                            ['po_detail_id',$web_po->po_detail_id]
                                        ])
                                        ->update(['flag_wms' => true]);
                                    }

                                    DetailMaterialArrival::FirstOrCreate([
                                        'material_arrival_id'       => $material_arrival->id,
                                        'po_buyer'                  => trim($web_po->pobuyer),
                                        'barcode_supplier'          => $supplier_material_fabric->barcode_id,
                                        'qty_delivered'             => $supplier_material_fabric->qty,
                                        'qty_upload'                => $supplier_material_fabric->qty * $dividerate,
                                        'uom'                       => $supplier_material_fabric->uomsymbol,
                                        'uom_conversion'            => $uom_conversion,
                                        'batch_number'              => $supplier_material_fabric->batch_number,
                                        'nomor_roll'                => $supplier_material_fabric->nomor_roll,
                                        'user_id'                   =>  Auth::user()->id,
                                        'jenis_po'                  => strtoupper($web_po->c_bp_group_id),
                                        'is_active'                 =>  true
                                    ]);   
                                    
                                    $concatenate_detail .= "'".$supplier_material_fabric->barcode_id."',";
                                }
                            }

                            $material_arrival_ids [] = $material_arrival->id;
                        }
                    }else
                    {
                        if($app_env == 'live')
                        {
                            $web_po = PurchaseOrderDetail::where([
                                ['po_detail_id',$data_print->po_detail_id],
                                ['type_po',1],
                                ['flag_wms',true],
                            ])
                            ->first();

                            $supplier_material_fabrics = MaterialFebric::where([
                                ['po_detail_id',$web_po->po_detail_id],
                                ['flag_wms',false],
                            ])
                            ->orderby('nomor_roll','asc')
                            ->get();
                        }else
                        {
                            $web_po = PurchaseOrderDetailDev::where([
                                ['po_detail_id',$data_print->po_detail_id],
                                ['type_po',1],
                                ['flag_wms',true],
                            ])
                            ->first();

                            $supplier_material_fabrics = MaterialFebricDev::where([
                                ['po_detail_id',$web_po->po_detail_id],
                                ['flag_wms',false],
                            ])
                            ->orderby('nomor_roll','asc')
                            ->get();
                        }
                        
                        if(count($supplier_material_fabrics) > 0)
                        {
                            $is_arrival_exists = MaterialArrival::where([
                                ['po_detail_id',$web_po->po_detail_id],
                                ['warehouse_id',$web_po->m_warehouse_id]
                            ])
                            ->first();

                            foreach ($supplier_material_fabrics as $key_material => $supplier_material_fabric) 
                            {
                                $conversion = UomConversion::where([
                                    ['item_code',$web_po->item],
                                    ['uom_from',trim(strtoupper($supplier_material_fabric->uomsymbol))]
                                ])
                                ->first();

                                if($conversion)
                                {
                                    $dividerate     = $conversion->dividerate;
                                    $uom_conversion = $conversion->uom_to;
                                }else
                                {
                                    $dividerate     = 1;
                                    $uom_conversion = $web_po->uomsymbol;
                                }

                                $is_detail_exists = DetailMaterialArrival::where([
                                    ['barcode_supplier',$supplier_material_fabric->barcode_id],
                                    ['batch_number',$supplier_material_fabric->batch_number],
                                    ['nomor_roll',$supplier_material_fabric->nomor_roll],
                                ])
                                ->exists();

                                if(!$is_detail_exists)
                                {
                                    if($app_env == 'live')
                                    {
                                        MaterialFebric::where([
                                            ['barcode_id',$supplier_material_fabric->barcode_id],
                                            ['po_detail_id',$web_po->po_detail_id]
                                        ])
                                        ->update(['flag_wms' => true]);
                                    }else
                                    {
                                        MaterialFebricDev::where([
                                            ['barcode_id',$supplier_material_fabric->barcode_id],
                                            ['po_detail_id',$web_po->po_detail_id]
                                        ])
                                        ->update(['flag_wms' => true]);
                                    }

                                    DetailMaterialArrival::FirstOrCreate([
                                        'material_arrival_id'   => $is_arrival_exists->id,
                                        'po_buyer'              => trim($web_po->pobuyer),
                                        'barcode_supplier'      => $supplier_material_fabric->barcode_id,
                                        'qty_delivered'         => $supplier_material_fabric->qty,
                                        'qty_upload'            => $supplier_material_fabric->qty * $dividerate,
                                        'uom'                   => $supplier_material_fabric->uomsymbol,
                                        'uom_conversion'        => $uom_conversion,
                                        'batch_number'          => $supplier_material_fabric->batch_number,
                                        'nomor_roll'            => $supplier_material_fabric->nomor_roll,
                                        'user_id'               => Auth::user()->id,
                                        'jenis_po'              => strtoupper($web_po->c_bp_group_id),
                                        'is_active'             => true
                                    ]); 
                                    $concatenate_detail .= "'".$supplier_material_fabric->barcode_id."',";   
                                }
                            }

                            $material_arrival_ids [] = $is_arrival_exists->id;
                        }else
                        {

                            $material_arrivals = MaterialArrival::where([
                                [DB::raw('upper(document_no)'),strtoupper(strtoupper($document_no))],
                                [DB::raw('upper(item_code)'),strtoupper(($item_code))],
                                ['warehouse_id',$_warehouse_id],
                                [DB::raw('upper(no_packing_list)'),strtoupper($no_packing_list)],
                                [DB::raw('upper(no_invoice)'),strtoupper($no_invoice)],
                            ])
                            ->whereNull('material_roll_handover_fabric_id')
                            ->whereNull('summary_handover_material_id')
                            ->get();

                            foreach($material_arrivals as $i)
                            {
                                $material_arrival_ids [] = $i->id;
                            }
                        }
                    }
                }

                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_arrival(array[".$concatenate ."]);" ));
                }

                if($concatenate_detail != '')
                {
                    $concatenate_detail = substr_replace($concatenate_detail, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_detail_arrival(array[".$concatenate_detail ."]);" ));
                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            $this->storeItemPackingList($packing_list);
            //$this->storeReadyPrepare($is_ready_prepared);
            $return_print = $this->storeMaterialStock($material_arrival_ids,$_warehouse_id);
            
            return response()->json($return_print,200);
        }

    }

    public function barcode(request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $barcodes       = json_decode($request->list_barcodes);
       
        $material_stocks  = MaterialStock::wherein('id',$barcodes)
        ->where([
            ['warehouse_id',$warehouse_id],
            ['type_po',1],
        ])
        ->orderby('document_no','asc')
        ->orderby('item_code','asc')
        ->orderby('nomor_roll','asc')
        ->get();
       
        foreach ($material_stocks as $key => $material_stock) 
        {
            $get_barcode        = $this->randomCode();
            $barcode            = $get_barcode->barcode;
            $referral_code      = $get_barcode->referral_code;
            $sequence           = $get_barcode->sequence;
            $_barcode           = $material_stock->barcode_supplier;

            if($_barcode == 'BELUM DI PRINT')
            {
                $material_stock->barcode_supplier   = $barcode;
                $material_stock->referral_code      = $referral_code;
                $material_stock->sequence           = $sequence;
                $material_stock->save();
            }
        }
        
        return view('fabric_barcode_supplier.barcode',compact('material_stocks'));
    }

    static function storeItemPackingList($packing_lists)
    {
        $data = DB::connection('web_po') //web_po //dev_web_po
        ->table('adt_item_pl')
        ->select('no_packinglist as no_packing_list',
                'kst_invoicevendor as no_invoice',
                'kst_resi as no_resi',
                'c_bpartner_id',
                'ttl_item as total_item',
                'type_po'
                )
        ->whereIn('no_packinglist',$packing_lists)
        ->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($data as $key => $value) 
            {
                $no_packing_list    = $value->no_packing_list;
                $no_resi            = $value->no_resi;
                $no_invoice         = $value->no_invoice;
                $c_bpartner_id      = $value->c_bpartner_id;
                
                $is_exists = ItemPackingList::where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',1]
                ])
                ->first();
                
                $count_packinglist = MaterialArrival::whereNotNull('no_packing_list')
                ->whereNotNull('no_resi')
                ->whereNotNull('no_invoice')
                ->whereNotNull('c_bpartner_id')
                ->whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where([
                    ['no_packing_list',$no_packing_list],
                    ['no_resi',$no_resi],
                    ['no_invoice',$no_invoice],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['type_po',1]
                ])
                ->count();

                if($is_exists)
                {
                    $is_exists->total_item          = $value->total_item;
                    $is_exists->total_item_receive  = $count_packinglist;
                    $is_exists->save();
                }else
                {
                    ItemPackingList::FirstOrCreate([
                        'no_packing_list'       => $value->no_packing_list,
                        'no_resi'               => $value->no_resi,
                        'no_invoice'            => $value->no_invoice,
                        'c_bpartner_id'         => $value->c_bpartner_id,
                        'total_item'            => $value->total_item,
                        'total_item_receive'    => $count_packinglist,
                        'type_po'               => 1
                    ]);
                }
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    static function storeReadyPrepare($barcode)
    {
        $_po_buyer          = array(); 
        $material_arrival   = MaterialArrival::whereIn('po_detail_id',$barcode)
        ->where('warehouse_id',auth::user()->warehouse)->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($material_arrival as $key => $value)
            {
                $details        = $value->detailattr($value->id);
                $po_buyer       = $value->po_buyer;
                //$qty_reject = $value->reject($value->po_detail_id);

                if($po_buyer != '' || $po_buyer != null)
                {
                    $has_many_po_buyer = strpos($po_buyer, ",");
                    if($has_many_po_buyer == false)
                    {
                        // hanya satu po buyer
                        $is_exists = MaterialReadyPreparation::where([
                            ['material_arrival_id',$value->id],
                            ['item_code',$value->item_code],
                            ['category',$value->category],
                            ['document_no',$value->document_no],
                            ['type_po',$value->type_po],
                            ['c_order_id',$value->c_order_id],
                            ['c_orderline_id',$value->c_orderline_id],
                            ['c_bpartner_id',$value->c_bpartner_id],
                            ['po_buyer',trim($po_buyer)]
                        ])
                        ->exists();
                        
                        if(!$is_exists)
                        {
                            MaterialReadyPreparation::firstOrCreate([
                                'material_arrival_id'   => $value->id,
                                'item_id'               => $value->item_id,
                                'item_code'             => $value->item_code,
                                'item_desc'             => $value->item_desc,
                                'category'              => $value->category,
                                'po_buyer'              => trim($po_buyer),
                                'document_no'           => $value->document_no,
                                'c_order_id'            => $value->c_order_id,
                                'c_orderline_id'        => $value->c_orderline_id,
                                'c_bpartner_id'         => $value->c_bpartner_id,
                                'supplier_name'         => $value->supplier_name,
                                'type_po'               => $value->type_po,
                                'user_id'               => Auth::user()->id
                            ]);
                            $_po_buyer[] = trim($po_buyer);
                        }
                        
                    }else
                    {
                        // banyak po buyer
                        $split = explode(',',$po_buyer);
                        foreach ($split as $key_po_buyer => $split_po_buyer) 
                        {
                            $is_exists = MaterialReadyPreparation::where([
                                ['material_arrival_id',$value->id],
                                ['c_order_id',$value->c_order_id],
                                ['c_orderline_id',$value->c_orderline_id],
                                ['c_bpartner_id',$value->c_bpartner_id],
                                ['item_code',$value->item_code],
                                ['category',$value->category],
                                ['document_no',$value->document_no],
                                ['type_po',$value->type_po],
                                ['po_buyer',trim($split_po_buyer)]
                            ])
                            ->exists();
                            
                            if(!$is_exists)
                            {
                                MaterialReadyPreparation::firstOrCreate([
                                    'material_arrival_id'       => $value->id,
                                    'c_order_id'                => $value->c_order_id,
                                    'c_orderline_id'            => $value->c_orderline_id,
                                    'c_bpartner_id'             => $value->c_bpartner_id,
                                    'supplier_name'             => $value->supplier_name,
                                    'item_id'                   => $value->item_id,
                                    'item_code'                 => $value->item_code,
                                    'item_desc'                 => $value->item_desc,
                                    'category'                  => $value->category,
                                    'po_buyer'                  => trim($split_po_buyer),
                                    'document_no'               => $value->document_no,
                                    'type_po'                   => $value->type_po,
                                    'user_id'                   => Auth::user()->id
                                ]);
                                
                                $_po_buyer[] = trim($split_po_buyer);
                            }
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
        

        return $_po_buyer;
    }

    private function storeMaterialStock($material_arrival_ids,$_warehouse_id)
    {
        $concatenate            = '';
        $area_receive_fabric    = Locator::whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where('name','RECEIVING')
            ->where('warehouse',$_warehouse_id);
        })
        ->first();

        $supplier_location      = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','SUPPLIER');
        })
        ->first();

        $approval_date          = carbon::now()->toDateTimeString();
        $summary_stock_fabrics  = array();
        $return_barcode         = array();

        $material_stocks = DetailMaterialArrival::select(
            'detail_material_arrivals.barcode_supplier',
            'material_arrivals.po_detail_id',
            'material_arrivals.item_id',
            'material_arrivals.item_code',
            'material_arrivals.item_desc',
            'material_arrivals.c_bpartner_id',
            'material_arrivals.supplier_name',
            'material_arrivals.category',
            'material_arrivals.warehouse_id',
            'material_arrivals.no_packing_list',
            'material_arrivals.no_invoice',
            'detail_material_arrivals.qty_upload as qty_ordered', //QTY ORIGIN DARI KEDATANGAN PER TANGGAL 11.28
            'material_arrivals.c_order_id',
            'material_arrivals.document_no',
            'detail_material_arrivals.nomor_roll',
            'detail_material_arrivals.batch_number',
            'detail_material_arrivals.uom_conversion as uom',
            'detail_material_arrivals.jenis_po',
            'detail_material_arrivals.qty_upload as total_qty_upload'
       )
        ->join('material_arrivals','material_arrivals.id','detail_material_arrivals.material_arrival_id')
        ->whereNull('detail_material_arrivals.preparation_date')
        ->where('material_arrivals.remark','NEW')
        ->whereIn('material_arrival_id',$material_arrival_ids)
        ->get();

        if(count($material_stocks) > 0)
        {
            try 
            {
                DB::beginTransaction();

                foreach ($material_stocks as $key => $value) 
                {
                
                    $item_id                = $value->item_id;
                    $item_code              = $value->item_code;
                    $_item                  = Item::where('item_id',$item_id)->first();

                    $barcode_supplier       = $value->barcode_supplier;
                    $item_desc              = ($_item ? $_item->item_desc : $value->item_desc);
                    $color                  = ($_item ? $_item->color : null);
                    $upc                    = ($_item ? $_item->upc : null);
                    $is_require_lot         = ($_item ? $_item->required_lot : false);
                    $category               = $value->category;
                    $no_packing_list        = $value->no_packing_list;
                    $po_detail_id           = $value->po_detail_id;
                    $no_invoice             = $value->no_invoice;
                    $c_order_id             = $value->c_order_id;
                   
                    $stock                  = sprintf('%0.8f',$value->total_qty_upload);
                    $qty_order              = sprintf('%0.8f',$value->qty_ordered);
                    $available_qty          = sprintf('%0.8f',$value->total_qty_upload);
                    $batch_number           = $value->batch_number;
                    $nomor_roll             = $value->nomor_roll;
                    $uom                    = $value->uom;
                    $c_bpartner_id          = $value->c_bpartner_id;
                    $supplier               = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                    $supplier_name          = ($supplier ? $supplier->supplier_name : $value->supplier_name);
                    $document_no            = $value->document_no;
                    $warehouse_id           = $value->warehouse_id;
                    $jenis_po               = strtoupper($value->jenis_po);

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    if($upc == '60001344' || $upc == '62712089' || $upc == '70000121')
                    {
                        if($warehouse_id == '1000011')
                        {
                            $inspect_lot_result = 'RELEASE';
                            $load_actual        = 'POCKET';
                            $user_lab_id        = $system->id;
                            $inspect_lab_remark = '60001344,62712089,70000121 SECARA SISTEM AKAN LGSG DI ISIKAN LOTNYA POCKET  ( SESUAI KESEPAKATAN TANGGAL 24/DES/2019 DI SKYPE AOI 2 LGSG RELEASE, AOI 1 PERLU KONFIRMASI)';
                            $inspect_lab_date   = $approval_date;
                        }else
                        {
                            $inspect_lot_result = 'HOLD';
                            $load_actual        = 'POCKET';
                            $user_lab_id        = $system->id;
                            $inspect_lab_remark = '60001344,62712089,70000121 SECARA SISTEM AKAN LGSG DI ISIKAN LOTNYA POCKET  ( SESUAI KESEPAKATAN TANGGAL 24/DES/2019 DI SKYPE AOI 2 LGSG RELEASE, AOI 1 PERLU KONFIRMASI)';
                            $inspect_lab_date   = $approval_date;
                        }
                        
                        
                    }else
                    {
                        if(!$is_require_lot)
                        {
                            $inspect_lot_result = 'RELEASE';
                            $load_actual        = 'POCKET';
                            $user_lab_id        = $system->id;
                            $inspect_lab_remark = 'ITEM '.$upc.' TIDAK MEMERLUKAN LOT, MAKA LGSG DI ANGGAP POCKET ( SESUAI KESEPAKATAN TANGGAL 24/DES/2019 DI SKYPE AOI 2 LGSG RELEASE, AOI 1 SUDAH JUGA KONFIRMASI TANGGAL 08/APRIL/2020 DI SKYPE)';
                            $inspect_lab_date   = $approval_date;
                        }else
                        {
                            $inspect_lot_result  = null;
                            $load_actual        = null;
                            $user_lab_id        = null;
                            $inspect_lab_remark = null;
                            $inspect_lab_date   = null;
                        }
                        
                    }

                    $is_stock_exists = MaterialStock::where([
                        ['locator_id',$area_receive_fabric->id],
                        ['po_detail_id',$po_detail_id],
                        ['item_id',$item_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['c_order_id',$c_order_id],
                        ['batch_number',$batch_number],
                        ['nomor_roll',$nomor_roll],
                        ['document_no',$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['stock',$stock],
                        ['type_po',1],
                        ['source','DI DAPATKAN DARI BARCODE '.$barcode_supplier]
                        
                    ])
                    ->exists();

                    if(!$is_stock_exists)
                    {

                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }else
                        {
                            $get_4_digit_from_beginning = substr($document_no,0, 4);
                            $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                            
                            if($_mapping_stock_4_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_4_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_4_digt->type_stock;
                            }else
                            {
                                $get_5_digit_from_beginning = substr($document_no,0, 5);
                                $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                
                                if($_mapping_stock_5_digt)
                                {
                                    $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                    $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                    $type_stock             = $_mapping_stock_5_digt->type_stock;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                                
                            }
                        }
                        
                        $barcode            = 'BELUM DI PRINT';
                        $referral_code      = 'BELUM DI PRINT';
                        $sequence           = '-1';
                        $supplier           = Supplier::select('supplier_code')
                        ->where('c_bpartner_id',$c_bpartner_id)
                        ->groupby('supplier_code')
                        ->first();

                        $supplier_code      = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';

                        $material_stock     = MaterialStock::FirstOrCreate([
                            'locator_id'            => $area_receive_fabric->id,
                            'po_detail_id'          => $po_detail_id,
                            'barcode_supplier'      => $barcode,
                            'referral_code'         => $referral_code,
                            'sequence'              => $sequence,
                            'item_code'             => $item_code,
                            'item_desc'             => $item_desc,
                            'color'                 => $color,
                            'upc_item'              => $upc,
                            'category'              => $category,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'type_po'               => 1,
                            'c_order_id'            => $c_order_id,
                            'item_id'               => $item_id,
                            'qty_order'             => $qty_order,
                            'qty_arrival'           => $qty_order,
                            'stock'                 => $stock,
                            'available_qty'         => $available_qty,
                            'batch_number'          => $batch_number,
                            'nomor_roll'            => $nomor_roll,
                            'uom'                   => $uom,
                            'source'                => 'DI DAPATKAN DARI BARCODE '.$barcode_supplier,
                            'is_master_roll'        => true,
                            'is_active'             => true,
                            'inspect_lot_result'    => $inspect_lot_result,
                            'load_actual'           => $load_actual,
                            'user_lab_id'           => $user_lab_id,
                            'inspect_lab_remark'    => $inspect_lab_remark,
                            'inspect_lab_date'      => $inspect_lab_date,
                            'supplier_name'         => $supplier_name,
                            'document_no'           => $document_no,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'supplier_code'         => $supplier_code,
                            'warehouse_id'          => $warehouse_id,
                            'type_stock'            => $type_stock,
                            'type_stock_erp_code'   => $type_stock_erp_code,
                            'jenis_po'              => $jenis_po,
                            'user_id'               => auth::user()->id,
                            'approval_user_id'      => auth::user()->id,
                            'approval_date'         => $approval_date,
                            'created_at'            => $approval_date,
                            'updated_at'            => $approval_date,
                        ]);

                        $detail_material_arrivals = DetailMaterialArrival::whereHas('materialArrival',function($query) use($po_detail_id,$c_order_id,$item_id,$document_no,$c_bpartner_id,$warehouse_id,$no_invoice)
                        {
                            $query->where([
                                ['po_detail_id',$po_detail_id],
                                ['c_order_id',$c_order_id],
                                ['item_id',$item_id],
                                ['document_no',$document_no],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['warehouse_id',$warehouse_id],
                                ['no_invoice',$no_invoice],
                            ]);
                        })
                        ->where([
                            ['nomor_roll',$nomor_roll],
                            ['batch_number',$batch_number],
                            ['barcode_supplier',$barcode_supplier]
                        ])
                        ->wherein('material_arrival_id',$material_arrival_ids)
                        ->get();
    
                        foreach ($detail_material_arrivals as $key_1 => $detail_material_arrival)
                        {
                            $detail_material_arrival_id         = $detail_material_arrival->id;
                            $material_arrival_id                = $detail_material_arrival->material_arrival_id;
                            $uom                                = $detail_material_arrival->uom;
                            $qty_upload                         = $detail_material_arrival->qty_upload;
                            $barcode_supplier                   = $detail_material_arrival->barcode_supplier;
    
                            DetailMaterialStock::FirstOrCreate([
                                'material_stock_id'         => $material_stock->id,
                                'material_arrival_id'       => $material_arrival_id,
                                'remark'                    => $barcode_supplier,
                                'uom'                       => $uom,
                                'qty'                       => $qty_upload,
                                'user_id'                   => Auth::user()->id,
                                'created_at'                => $approval_date,
                                'updated_at'                => $approval_date,
                            ]);
    
                            $update_detail_arrival                      = DetailMaterialArrival::find($detail_material_arrival_id);
                            $update_detail_arrival->preparation_date    = carbon::now();
                            $update_detail_arrival->save();
                        }
                        
    
                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id'         => $material_stock->id,
                            'from_location'             => $supplier_location->id,
                            'to_destination'            => $area_receive_fabric->id,
                            'uom'                       => $uom,
                            'qty'                       => $stock,
                            'movement_date'             => $approval_date,
                            'status'                    => 'receiving',
                            'user_id'                   => auth::user()->id
                        ]);
    
                        $summary_stock_fabrics[]        = $material_stock->id;
                        $return_barcode[] = $material_stock->id;
                    }
                    

                    $concatenate    .= "'".$po_detail_id."',";
                }

                DB::commit();

                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_stock_fabric(array[".$concatenate ."]);" ));
                }

                $total_item_on_locator              = MaterialStock::where('locator_id',$area_receive_fabric->id)
                ->whereNull('deleted_at')
                ->count();
                
                $area_receive_fabric->counter_in    = $total_item_on_locator;
                $area_receive_fabric->save();
                
                MaterialArrival::whereIn('id',$material_arrival_ids)->update(['remark' => null]);
                $summary_stock = $this->storeSummaryStockFabric($summary_stock_fabrics,$_warehouse_id);
                $this->storeMonitoringReceiving($material_arrival_ids);
            
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
        
        }else
        {
            $material_stocks = MaterialStock::whereIn('id',function($query) use($material_arrival_ids)
            {
                $query->select('material_stock_id')
                ->from('detail_material_stocks')
                ->whereIn('material_arrival_id',$material_arrival_ids)
                ->groupby('material_stock_id');
            })
            ->get();



            foreach ($material_stocks as $key => $material_stock) 
            {
                $return_barcode[] = $material_stock->id;
            }
        }

        return $return_barcode;
       
    }

    static function randomCode()
    {
        
        $referral_code  = '1S'.Carbon::now()->format('u');
        $sequence       = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null)
            $sequence = 1;
        else
            $sequence += 1;

        Barcode::FirstOrCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);
        
        $obj                = new stdClass();
        $obj->barcode       = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
        return $obj;
        
    }

    static function storeSummaryStockFabric($material_arrival_ids,$_warehouse_id)
    {
        $material_stocks = MaterialStock::select('document_no'
        ,'c_order_id'
        ,'c_bpartner_id'
        ,'item_code'
        ,'item_id'
        ,'uom'
        ,db::raw('sum(stock) as stock')
        ,db::raw('sum(qty_order) as qty_order'))
        ->whereIn('id',$material_arrival_ids)
        ->groupby('document_no','c_bpartner_id','c_order_id','item_code','item_id','uom')
        ->get();

        $summary_stocks = array();
        try 
        {
            DB::beginTransaction();

            foreach ($material_stocks as $key => $material_stock) 
            {
                $item                   = Item::where('item_code',$material_stock->item_code)->first();
                $new_c_bpartner_id      = $material_stock->c_bpartner_id;
                $new_document_no        = $material_stock->document_no;
                $new_item_id            = $material_stock->item_id;
                $new_item_code          = $material_stock->item_code;
                $new_c_order_id         = $material_stock->c_order_id;

                $supplier               = Supplier::select('supplier_code','supplier_name')->where('c_bpartner_id',$new_c_bpartner_id)->groupby('supplier_code','supplier_name')->first();
                $supplier_code          = ($supplier)? $supplier->supplier_code : null;
                $supplier_name          = ($supplier)? $supplier->supplier_name : null;
                $new_color              = ($item)? $item->color : 'NOT FOUND';
                $new_uom                = $material_stock->uom;
                $new_stock              = sprintf('%0.8f',$material_stock->stock);
                $qty_order              = sprintf('%0.8f',$material_stock->qty_order);
            
                if($new_document_no == 'FREE STOCK' || $new_document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($new_document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($new_document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $is_exists = SummaryStockFabric::where([
                    ['c_order_id',$new_c_order_id],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$_warehouse_id]
                ])
                ->first();

                if($is_exists)
                {
                    $_summary_stock_id = $is_exists->id;
                }else
                {
                    if($new_stock >= $qty_order) $_qty_order = $new_stock;
                    else $_qty_order = $qty_order;

                    $summary_stock_fabric = SummaryStockFabric::FirstOrCreate([
                        'c_bpartner_id'         => $new_c_bpartner_id,
                        'document_no'           => $new_document_no,
                        'c_order_id'            => $new_c_order_id,
                        'supplier_code'         => $supplier_code,
                        'supplier_name'         => $supplier_name,
                        'item_code'             => $new_item_code,
                        'item_id'               => $new_item_id,
                        'color'                 => $new_color,
                        'category_stock'        => $type_stock,
                        'uom'                   => $new_uom,
                        'stock'                 => $new_stock,
                        'reserved_qty'          => 0,
                        'available_qty'         => $new_stock,
                        'qty_order'             => sprintf('%0.8f',$_qty_order),
                        'warehouse_id'          => $_warehouse_id,
                    ]);
                    
                    $_summary_stock_id = $summary_stock_fabric->id;
                }

                MaterialStock::where([
                    ['c_order_id',$new_c_order_id],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$_warehouse_id],
                ])
                ->whereNull('summary_stock_fabric_id')
                ->wherein('id',$material_arrival_ids)
                ->update(['summary_stock_fabric_id' => $_summary_stock_id]);

                $summary_stocks [] =  $_summary_stock_id;
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();
            $sum_summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$summary_stocks)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($sum_summary_stocks as $key => $sum_summary_stock) 
            {
                $summary_stock_fabric   = SummaryStockFabric::find($sum_summary_stock->summary_stock_fabric_id);
                $qty_order              = $summary_stock_fabric->qty_order;
                $qty_order_reserved     = $summary_stock_fabric->qty_order_reserved;

                $new_stock              = $sum_summary_stock->total_stock;
                $total_reserved         = $sum_summary_stock->total_reserved;
                $total_available        = $sum_summary_stock->total_available;
            

                if($new_stock >= $qty_order) $_qty_order = $new_stock;
                else $_qty_order = $qty_order;

                $new_available_qty_order = $_qty_order - $qty_order_reserved;

                if($new_available_qty_order > 0)
                    $summary_stock_fabric->is_allocated = false;
                
                $summary_stock_fabric->stock                = sprintf('%0.8f',$new_stock);
                $summary_stock_fabric->reserved_qty         = sprintf('%0.8f',$total_reserved);
                $summary_stock_fabric->available_qty        = sprintf('%0.8f',$total_available);
                $summary_stock_fabric->qty_order            = sprintf('%0.8f',$_qty_order);
                $summary_stock_fabric->save();
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return $summary_stocks;

    }

    static function storeMonitoringReceiving($material_arrival_ids)
    {
        $arrivals = MaterialArrival::select('no_packing_list','no_invoice','c_order_id','document_no','item_id','c_bpartner_id','warehouse_id')
        ->whereIn('id',$material_arrival_ids)
        ->groupby('no_packing_list','no_invoice','c_order_id','document_no','item_id','c_bpartner_id','warehouse_id')
        ->get();

        try 
        {
            DB::beginTransaction();
            
            $array = array();
            foreach ($arrivals as $key => $arrival) 
            {
                $_no_packing_list       = ($arrival->no_packing_list ? $arrival->no_packing_list : '1');
                $_no_invoice            = ($arrival->no_invoice ?  $arrival->no_invoice : '1');
                $_document_no           = $arrival->document_no;
                $_c_order_id            = $arrival->c_order_id;
                $_item_id               = $arrival->item_id;
                $_c_bpartner_id         = $arrival->c_bpartner_id;
                $_warehouse_id          = $arrival->warehouse_id;


                $monitoring          = DB::select(db::raw("SELECT * FROM get_monitoring_receiving_fabric_new(
                    '".$_no_packing_list."',
                    '".$_no_invoice."',
                    '".$_document_no."',
                    '".$_c_bpartner_id."',
                    '".$_item_id."',
                    '".$_warehouse_id."'
                );"));
                

                foreach ($monitoring as $key_2 => $value)
                {
                    $c_bpartner_id                  = $value->c_bpartner_id;
                    $arrival_date                   = $value->arrival_date;
                    $po_detail_id                   = $value->po_detail_id;
                    $document_no                    = $value->document_no;
                    $c_order_id                     = $value->c_order_id;
                    $no_invoice                     = $value->no_invoice;
                    $item_id                        = $value->item_id;
                    $item_code                      = $value->item_code;
                    $no_packing_list                = $value->no_packing_list;
                    $supplier_name                  = $value->supplier_name;
                    $color                          = $value->color;
                    $warehouse_id                   = $value->warehouse_id;
                    $total_batch                    = $value->total_batch;
                    $total_roll                     = $value->total_roll;
                    $total_yard                     = $value->total_yard;
                    $user_receive                   = $value->user_receive;
                    $type_supplier                  = $value->type_supplier;

                    $is_exists = MonitoringReceivingFabric::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        [db::raw('upper(item_id)'),$item_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['is_handover',false],
                    ])
                    ->first();

                    if($is_exists)
                    {
                        $is_exists->po_detail_id    = $po_detail_id;
                        $is_exists->arrival_date    = $arrival_date;
                        $is_exists->total_batch     = $total_batch;
                        $is_exists->total_roll      = $total_roll;
                        $is_exists->c_order_id      = $c_order_id;
                        $is_exists->item_id         = $item_id;
                        $is_exists->total_yard      = $total_yard;
                        $is_exists->user_receive    = $user_receive;
                        $is_exists->jenis_po        = $type_supplier;
                        $is_exists->save();

                        $monitoring_receiving_fabric_id = $is_exists->id;

                    }else
                    {
                        
                        $monitoring_receiving_fabric = MonitoringReceivingFabric::FirstOrCreate([
                            'po_detail_id'          => $po_detail_id,
                            'arrival_date'          => $arrival_date,
                            'c_order_id'            => $c_order_id,
                            'document_no'           => $document_no,
                            'no_packing_list'       => $no_packing_list,
                            'no_invoice'            => $no_invoice,
                            'supplier_name'         => $supplier_name,
                            'item_code'             => $item_code,
                            'item_id'               => $item_id,
                            'c_bpartner_id'         => $c_bpartner_id,
                            'color'                 => $color,
                            'warehouse_id'          => $warehouse_id,
                            'total_batch'           => $total_batch,
                            'total_roll'            => $total_roll,
                            'total_yard'            => $total_yard,
                            'user_receive'          => $user_receive,
                            'jenis_po'              => $type_supplier
                        ]);

                        $monitoring_receiving_fabric_id = $monitoring_receiving_fabric->id;
                    }

                    MaterialStock::where([
                        ['document_no',$document_no],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['item_id',$item_id],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->whereNull('material_roll_handover_fabric_id')
                    ->whereNull('monitoring_receiving_fabric_id')
                    ->update([
                        'monitoring_receiving_fabric_id' => $monitoring_receiving_fabric_id
                    ]);

                    Temporary::FirstOrCreate([
                        'barcode'       => $monitoring_receiving_fabric_id,
                        'status'        => 'insert_stock_per_lot',
                        'user_id'       => Auth::user()->id,
                        'created_at'    => $arrival_date,
                        'updated_at'    => $arrival_date,
                    ]);

                    $array [] = $monitoring_receiving_fabric_id;

                    
                }
            }


            $data = MonitoringReceivingFabric::whereIn('id',$array)->get();

            foreach ($data as $key => $value) 
            {
                $total_roll_inspected           = MaterialStock::where([
                    ['is_master_roll',true],
                    ['monitoring_receiving_fabric_id',$value->id],
                ])
                ->whereNotNull('load_actual')
                ->count();

                $value->total_roll_inspected    = $total_roll_inspected;
                $value->save();
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }  
}
