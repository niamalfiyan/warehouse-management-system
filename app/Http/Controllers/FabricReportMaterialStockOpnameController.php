<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricReportMaterialStockOpnameController extends Controller
{
    public function index()
    {
        return view('fabric_report_material_stock_opname.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $user_id            = $request->user_id ;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_stock_opname = DB::table('material_stock_opname_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderby('created_at','desc');

            return DataTables::of($material_stock_opname)
           
            ->editColumn('created_at',function ($material_stock_opname)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_stock_opname->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('available_stock_old',function ($material_stock_opname)
            {
                return number_format($material_stock_opname->available_stock_old, 4, '.', ',');
            })
            ->editColumn('available_stock_new',function ($material_stock_opname)
            {
                return number_format($material_stock_opname->available_stock_new, 4, '.', ',');
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $warehouse_name     = ($warehouse_id == '1000001' ?'warehouse_fabric_aoi_1':'warehouse_fabric_aoi_2' );
        
        $material_bapb      = DB::table('material_stock_opname_v')
        ->where('warehouse_id','LIKE',"%$warehouse_id%")
        ->orderby('created_at','desc')
        ->get();

        $file_name = 'material_stock_opname_v'.$warehouse_name;
        return Excel::create($file_name,function($excel) use ($material_bapb){
            $excel->sheet('active',function($sheet)use($material_bapb)
            {
                $sheet->setCellValue('A1','STO DATE');
                $sheet->setCellValue('B1','STO BY');
                $sheet->setCellValue('C1','PO SUPPLIER');
                $sheet->setCellValue('D1','KODE SUPPLIER');
                $sheet->setCellValue('E1','NAMA SUPPLIER');
                $sheet->setCellValue('F1','NO INVOICE');
                $sheet->setCellValue('G1','NO ROLL');
                $sheet->setCellValue('H1','ITEM CODE');
                $sheet->setCellValue('I1','COLOR');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','QTY BEFORE');
                $sheet->setCellValue('L1','QTY AFTER');
                $sheet->setCellValue('M1','LOCATOR BEFORE');
                $sheet->setCellValue('N1','LOCATOR AFTER');
                

            
            $row=2;
            foreach ($material_bapb as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->created_at);
                $sheet->setCellValue('B'.$row,$i->name);
                $sheet->setCellValue('C'.$row,$i->document_no);
                $sheet->setCellValue('D'.$row,$i->supplier_code);
                $sheet->setCellValue('E'.$row,$i->supplier_name);
                $sheet->setCellValue('F'.$row,$i->no_invoice);
                $sheet->setCellValue('G'.$row,$i->nomor_roll);
                $sheet->setCellValue('H'.$row,$i->item_code);
                $sheet->setCellValue('I'.$row,$i->color);
                $sheet->setCellValue('J'.$row,$i->uom);
                $sheet->setCellValue('K'.$row,$i->available_stock_old);
                $sheet->setCellValue('L'.$row,$i->available_stock_new);
                $sheet->setCellValue('M'.$row,$i->old_locator);
                $sheet->setCellValue('N'.$row,$i->new_locator);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}

