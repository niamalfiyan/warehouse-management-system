<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Locator;
use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Models\MaterialSubcont;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialMovementPerSize;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesMaterialReverseOutController extends Controller
{
    public function index(Request $request)
    {
        return view('accessories_material_reverse_out.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $barcode            = trim($request->barcode);
        $has_dash           = strpos($barcode, "-");

        if($has_dash == false)
        {
            $_barcode   = strtoupper($barcode);
            $flag       = 1;
        }else
        {
            $split      = explode('-',$barcode);
            $_barcode   = strtoupper($split[0]);
            $flag       = 0;
        }

        $material_preparation = MaterialPreparation::where([
            ['barcode',$_barcode],
            ['warehouse',$_warehouse_id],
            ['qty_conversion','>', 0],
        ])
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found',422);
        if(!$material_preparation->deleted_at) return response()->json('Material is still active',422);
        
        $material_preparation_id        = $material_preparation->id;
        $c_order_id                     = $material_preparation->c_order_id;
        $po_detail_id                   = $material_preparation->po_detail_id;
        $c_bpartner_id                  = $material_preparation->c_bpartner_id;
        $document_no                    = $material_preparation->document_no;
        $warehouse                      = $material_preparation->warehouse;
        $po_buyer                       = $material_preparation->po_buyer;
        $item_code                      = $material_preparation->item_code;
        $item_id                        = $material_preparation->item_id;
        $last_status_movement           = $material_preparation->last_status_movement;
        $last_locator_id                = $material_preparation->last_locator_id;
        $last_locator_code              = $material_preparation->lastLocator->code;
        $last_locator_erp_id            = $material_preparation->lastLocator->area->erp_id;
        $total_carton                   = $material_preparation->total_carton;
        $style                          = $material_preparation->style;
        $qty_conversion                 = sprintf('%0.8f',$material_preparation->qty_conversion);
        $item_desc                      = $material_preparation->item_desc;
        $type_po                        = $material_preparation->type_po;
        $uom_conversion                 = $material_preparation->uom_conversion;
        $is_allocation                  = $material_preparation->is_allocation;
        $job_order                      = $material_preparation->job_order;
        $category                       = $material_preparation->category;
        $article_no                     = $material_preparation->article_no;
        $ict_log                        = $material_preparation->ict_log;
        $last_material_movement_line_id = $material_preparation->last_material_movement_line_id;
        $material_requirement           = $material_preparation->dataRequirement($po_buyer,$item_code,$style,$article_no);
        
        if($last_status_movement == 'in' || $last_status_movement == 'change') return response()->json('Material still active in '.$last_locator_code,422);
        if($total_carton > 1)
        {
            if($flag == 1) return response()->json('Wrong barcode.',422);
        }

        $qty_required       = ($material_requirement) ? $material_requirement->qty_required : 0;
        $statistical_date   = ($material_requirement) ? ($material_requirement->statistical_date)? $material_requirement->statistical_date->format('d-M-Y'):null:null;;

        $obj                                = new StdClass();
        $obj->last_material_movement_line_id= $last_material_movement_line_id;
        $obj->material_preparation_id       = $material_preparation_id;
        $obj->item_id                       = $item_id;
        $obj->item_code                     = $item_code;
        $obj->item_desc                     = $item_desc;
        $obj->c_order_id                    = $c_order_id;
        $obj->po_detail_id                  = $po_detail_id;
        $obj->type_po                       = $type_po;
        $obj->uom_conversion                = $uom_conversion;
        $obj->qty_required                  = $qty_required;
        $obj->c_bpartner_id                 = $c_bpartner_id;
        $obj->document_no                   = $document_no;
        $obj->warehouse                     = $warehouse;
        $obj->po_buyer                      = $po_buyer;
        $obj->qty_input                     = $qty_conversion;
        $obj->is_allocation                 = $is_allocation;
        $obj->job                           = $job_order;
        $obj->category                      = $category;
        $obj->style                         = $style;
        $obj->article_no                    = $article_no;
        $obj->barcode                       = $barcode;
        $obj->last_locator_id               = $last_locator_id;
        $obj->last_locator_code             = $last_locator_code;
        $obj->last_locator_erp_id           = $last_locator_erp_id;
        $obj->movement_date                 = Carbon::now()->toDateTimeString();
        $obj->statistical_date              = $statistical_date;
        $obj->ict_log                       = $ict_log;
        $obj->last_status_movement          = $last_status_movement;
        
        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if($validator->passes()){
           
            

            $_warehouse_id  = $request->warehouse_id;
            $items          = json_decode($request->barcode_products);

            $missing = Locator::with('area')
            ->whereHas('area',function ($query) use ($_warehouse_id)
            {
                $query->where([
                    ['warehouse',$_warehouse_id],
                    ['name','MISSING ITEM'],
                    ['is_destination',false]
                ]);

            })
            ->first();

            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();


            //dd($items);
            try 
            {
                DB::beginTransaction();

                foreach ($items as $key => $item) 
                {
                    $last_material_movement_line_id = $item->last_material_movement_line_id;
                    $material_preparation_id        = $item->material_preparation_id;
                    $last_status_movement           = strtoupper($item->last_status_movement);
                    $movement_date                  = $item->movement_date;
                    $po_detail_id                   = $item->po_detail_id;
                    $from_locator_id                = $item->last_locator_id;
                    $from_locator_erp_id            = $item->last_locator_erp_id;
                    $po_buyer                       = $item->po_buyer;
                    $c_bpartner_id                  = $item->c_bpartner_id;
                    $warehouse_id                   = $item->warehouse;
                    $document_no                    = strtoupper($item->document_no);
                    $item_code                      = strtoupper($item->item_code);
                    $ict_note                       = strtoupper($item->ict_log);
                    $qty_input                      = sprintf('%0.8f',$item->qty_input);
                    $barcode                        = $item->barcode;
                    $material_preparation           = MaterialPreparation::find($material_preparation_id);
                    $item_id                        = $material_preparation->item_id;
                    $c_order_id                     = $material_preparation->c_order_id;
                    
                    if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                    {
                        $c_order_id             = 'FREE STOCK';
                        $no_packing_list        = '-';
                        $no_invoice             = '-';
                        $c_orderline_id         = '-';
                    }else
                    {
                        $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                        ->whereNull('material_roll_handover_fabric_id')
                        ->where('po_detail_id',$po_detail_id)
                        ->first();

                        $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                        $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                        $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    }


                    if($last_status_movement == 'OUT-SUBCONT' || $last_status_movement == 'OUT-HANDOVER')
                    {
                        MaterialSubcont::where('barcode',$barcode)->delete();
                        $status = 'reverse-handover';
                    }else if($last_status_movement == 'CANCEL-ORDER' 
                    || $last_status_movement == 'CANCEL ORDER' 
                    || $last_status_movement == 'CANCEL ITEM' 
                    || $last_status_movement == 'REROUTE')
                    {
                        $document_no = $document_no;
                        $material_stock = MaterialStock::where([
                            ['po_buyer',$po_buyer],
                            ['c_order_id',$c_order_id],
                            ['item_id',$item_id],
                            ['source',$last_status_movement],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['warehouse_id',$warehouse_id],
                        ])
                        ->first();

                        if($material_stock)
                        {
                            $id                 = $material_stock->id;
                            $reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                            $curr_available_qty = sprintf('%0.8f',$material_stock->available_qty);
                            if($reserved_qty == 0)
                            {
                                $material_stock->reserved_qty       = sprintf('%0.8f',$curr_available_qty + $reserved_qty);
                                $material_stock->available_qty      = 0;
                                $material_stock->is_allocated       = true;
                                $material_stock->is_closing_balance = true;
                                $material_stock->is_active          = false;
                                HistoryStock::approved($id,'0',$curr_available_qty);
                                $material_stock->save();
                            }
                        }

                        $status = 'reverse-stock';
                    }else $status = 'reverse';

                    if ($item->is_unintegrated == true) 
                    {
                        $is_integrate       = true; 
                        $is_complete_erp    = true; 
                        $is_active          = false;
                    }
                    else
                    {
                        $is_complete_erp    = false; 
                        $is_active          = true;
                        $is_integrate       = false; 
                    } 
                    
                    $is_movement_reverse_exists = MaterialMovement::where([
                        ['from_location',$from_locator_id],
                        ['to_destination',$missing->id],
                        ['from_locator_erp_id',$inventory_erp->area->erp_id],
                        ['to_locator_erp_id',$inventory_erp->area->erp_id],
                        ['po_buyer',$po_buyer],
                        ['is_integrate',$is_integrate],
                        ['is_complete_erp',$is_complete_erp],
                        ['is_integrate',false],
                        ['is_active',true],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['status',$status],
                    ])
                    ->first();
                    
                    if(!$is_movement_reverse_exists)
                    {
                        
                        $movement_reverse = MaterialMovement::firstOrCreate([
                            'from_location'             => $from_locator_id,
                            'from_locator_erp_id'       => $inventory_erp->area->erp_id,
                            'to_destination'            => $missing->id,
                            'to_locator_erp_id'         => $inventory_erp->area->erp_id,
                            'po_buyer'                  => $po_buyer,
                            'is_complete_erp'           => $is_complete_erp,
                            'is_integrate'              => $is_integrate ,
                            'is_active'                 => $is_active,
                            'status'                    => $status,
                            'no_packing_list'           => $no_packing_list,
                            'no_invoice'                => $no_invoice,
                            'created_at'                => $movement_date,
                            'updated_at'                => $movement_date,
                        ]);

                        $movement_reverse_id = $movement_reverse->id;
                    }else
                    {
                        $is_movement_reverse_exists->updated_at = $movement_date;
                        $is_movement_reverse_exists->save();

                        $movement_reverse_id = $is_movement_reverse_exists->id;
                    }

                    $is_line_reverse_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                    ->where([
                        ['material_movement_id',$movement_reverse_id],
                        ['material_preparation_id',$material_preparation_id],
                        ['item_id',$material_preparation->item_id],
                        ['warehouse_id',$material_preparation->warehouse],
                        ['qty_movement',$qty_input],
                        ['date_movement',$movement_date],
                        ['is_integrate',false],
                        ['is_active',true],
                    ]) 
                    ->exists();

                    if(!$is_line_reverse_exists)
                    {
                        $new_material_movement_line_reverse = MaterialMovementLine::Create([
                            'material_movement_id'                      => $movement_reverse_id,
                            'material_preparation_id'                   => $material_preparation_id,
                            'item_code'                                 => $material_preparation->item_code,
                            'item_id'                                   => $material_preparation->item_id,
                            'c_order_id'                                => $material_preparation->c_order_id,
                            'c_orderline_id'                            => $c_orderline_id,
                            'c_bpartner_id'                             => $material_preparation->c_bpartner_id,
                            'supplier_name'                             => $material_preparation->supplier_name,
                            'type_po'                                   => 2,
                            'is_integrate'                              => false,
                            'is_inserted_to_material_movement_per_size' => false,
                            'uom_movement'                              => $material_preparation->uom_conversion,
                            'qty_movement'                              => $qty_input,
                            'date_movement'                             => $movement_date,
                            'created_at'                                => $movement_date,
                            'updated_at'                                => $movement_date,
                            'date_receive_on_destination'               => $movement_date,
                            'nomor_roll'                                => '-',
                            'warehouse_id'                              => $material_preparation->warehouse,
                            'document_no'                               => $material_preparation->document_no,
                            'note'                                      => 'MOVEMENT DI KEMBALIKAN KE INVENTORY',
                            'is_active'                                 => true,
                            'user_id'                                   => auth::user()->id,
                        ]);

                        $material_preparation->last_material_movement_line_id = $new_material_movement_line_reverse->id;
                        
                    }

                    
                    
                    $obj_line                               = new stdClass();
                    $obj_line->material_movement_line_id    = $last_material_movement_line_id;
                    $obj_line->movement_date                = $movement_date;
                    $delete_material_movement_lines []      = $obj_line;
                    
                    $material_preparation->last_user_movement_id    = auth::user()->id;
                    $material_preparation->last_movement_date       = $movement_date;
                    $material_preparation->updated_at               = $movement_date;
                    $material_preparation->last_status_movement     = 'in';
                    $material_preparation->last_locator_id          = $missing->id;
                    $material_preparation->deleted_at               = null;
                    $material_preparation->save();

                    Temporary::Create([
                        'barcode'       => $po_buyer,
                        'status'        => 'mrp',
                        'user_id'       => Auth::user()->id,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);
                    
                }

                $this->deleteMaterialMovementLines($delete_material_movement_lines);
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json(200);
        }else{
            return response()->json('Barcode not found.',422);
        }
    }

    public function import()
    {
        return view('accessories_material_reverse_out.import');
    }


    static function deleteMaterialMovementLines($delete_material_movement_lines)
    {
        try 
        {
            DB::beginTransaction();

            foreach ($delete_material_movement_lines as $key => $delete_material_movement_line) 
            {
                $material_movement_line_id  = $delete_material_movement_line->material_movement_line_id;
                $movement_date              = $delete_material_movement_line->movement_date;

                $material_movement_line = MaterialMovementLine::find($material_movement_line_id);
                if($material_movement_line)
                {
                    $material_preparation_id            = $material_movement_line->material_preparation_id;
                    $material_preparation               = MaterialPreparation::find($material_preparation_id);
                    $last_material_movement_line_id     = $material_preparation->last_material_movement_line_id;
                    $material_movement_line->deleted_at = $movement_date;
                    $material_movement_line->note       = 'MOVEMENT INI DIBATALKAN';

                    $data = MaterialMovementPerSize::where('material_movement_line_id',$material_movement_line_id)->get();
                    foreach ($data as $key => $datum) 
                    {
                        MaterialMovementPerSize::FirstOrCreate([
                            'material_movement_line_id' => $last_material_movement_line_id,
                            'size'                      => $datum->size,
                            'po_buyer'                  => $datum->po_buyer,
                            'item_id'                   => $datum->item_id,
                            'qty_per_size'              => sprintf('%0.8f',$datum->qty_per_size),
                            'reverse_date'              => $movement_date,
                            'created_at'                => $movement_date,
                            'updated_at'                => $movement_date,
                        ]);

                       $datum->reverse_date = $movement_date;
                       $datum->save();
                    }

                    $material_movement_line->save();
                }
            
            }
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function downloadFormUpload()
    {
        return Excel::create('upload_reverse',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_PREPARATION_ID');
                $sheet->setCellValue('B1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function upload(Request $request)
    {

        // dirubah 
        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);
  
            $path   = $request->file('upload_file')->getRealPath();
            $data   = Excel::selectSheets('active')->load($path,function($render){})->get();
            $result = array();
            
            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $movement_date = carbon::now()->todatetimestring();

                    foreach($data as $key => $value)
                    {
                        $material_preparation_id    = $value->material_preparation_id;
                        $destination_id             = $value->destination_id;
                        $ict_note                   = strtoupper(trim($value->remark));
                        
                        $material_preparation       = MaterialPreparation::find($material_preparation_id);
                        $from_locator_id            = $material_preparation->last_locator_id;
                        $warehouse_id               = $material_preparation->warehouse;
                        $item_id                    = $material_preparation->item_id;
                        $item_code                  = $material_preparation->item_code;
                        $qty_input                  = $material_preparation->qty_conversion;
                        $po_buyer                   = $material_preparation->po_buyer;
                        $last_status_movement       = $material_preparation->last_status_movement;

                        $missing = Locator::with('area')
                        ->whereHas('area',function ($query) use ($warehouse_id)
                        {
                            $query->where([
                                ['warehouse',$warehouse_id],
                                ['name','MISSING ITEM'],
                                ['is_destination',false]
                            ]);

                        })
                        ->first();

                        if($last_status_movement == 'OUT-SUBCONT' || $last_status_movement == 'OUT-HANDOVER')
                        {
                            MaterialSubcont::where('barcode',$barcode)->delete();
                        }else if($last_status_movement == 'CANCEL-ORDER' 
                        || $last_status_movement == 'CANCEL ORDER' 
                        || $last_status_movement == 'CANCEL ITEM' 
                        || $last_status_movement == 'REROUTE')
                        {
                            $document_no = $document_no;
                            $material_stock = MaterialStock::where([
                                ['po_buyer',$po_buyer],
                                [db::raw('upper(item_code)'),$item_code],
                                [db::raw('upper(document_no)'),$document_no],
                                ['source',$last_status_movement],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['warehouse_id',$warehouse_id],
                            ])
                            ->first();
    
                            if($material_stock)
                            {
                                $id                 = $material_stock->id;
                                $reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                                $curr_available_qty = sprintf('%0.8f',$material_stock->available_qty);
                                if($reserved_qty == 0)
                                {
                                    $material_stock->reserved_qty       = sprintf('%0.8f',$curr_available_qty + $reserved_qty);
                                    $material_stock->available_qty      = 0;
                                    $material_stock->is_allocated       = true;
                                    $material_stock->is_closing_balance = true;
                                    $material_stock->is_active          = false;
                                    HistoryStock::approved($id,'0',$curr_available_qty);
                                    $material_stock->save();
                                }
                            }
                        }

                        $movement_reverse = MaterialMovement::firstOrCreate([
                            'from_location'             => $from_locator_id,
                            'to_destination'            => $missing->id,
                            'po_buyer'                  => $po_buyer,
                            'is_complete_erp'           => false,
                            'is_integrate'              => false ,
                            'is_active'                 => true,
                            'status'                    => 'reverse',
                            'created_at'                => $movement_date,
                            'updated_at'                => $movement_date,
                        ]);
    
                        MaterialMovementLine::FirstOrCreate([
                            'material_movement_id'        => $movement_reverse->id,
                            'material_preparation_id'     => $material_preparation_id,
                            'item_id'                     => $item_id,
                            'item_code'                   => $item_code,
                            'type_po'                     => 2,
                            'qty_movement'                => $qty_input,
                            'date_movement'               => $movement_date,
                            'created_at'                  => $movement_date,
                            'updated_at'                  => $movement_date,
                            'date_receive_on_destination' => $movement_date,
                            'is_active'                   => true,
                            'parent_id'                   => null,
                            'note'                        => 'REVERSE, '.$ict_note,
                            'user_id'                     => Auth::user()->id
                        ]);
                        
                        $material_preparation->last_user_movement_id    = auth::user()->id;
                        $material_preparation->last_movement_date       = $movement_date;
                        $material_preparation->updated_at               = $movement_date;
                        $material_preparation->last_status_movement     = 'in';
                        $material_preparation->last_locator_id          = $missing->id;
                        $material_preparation->deleted_at               = null;
                        $material_preparation->save();
    
                        $obj = new stdClass();
                        $obj->barcode       = $material_preparation->barcode;
                        $obj->document_no   = $material_preparation->document_no;
                        $obj->po_buyer      = $material_preparation->po_buyer;
                        $obj->item_code     = $material_preparation->item_code;
                        $obj->uom           = $material_preparation->uom_conversion;
                        $obj->qty           = $material_preparation->qty_conversion;
                        $obj->is_error      = false;
                        $obj->status        = 'success';
                        $result []          = $obj;
                        
                        Temporary::Create([
                            'barcode'       => $po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => Auth::user()->id,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                return response()->json($result,200);
            }
        }
        return response()->json('Import failed, please check again',422);
    }
}
