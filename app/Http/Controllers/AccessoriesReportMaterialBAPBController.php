<?php namespace App\Http\Controllers;

use Auth;
use DB;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialBAPBController extends Controller
{
    public function index()
    {
        return view('accessories_report_material_bapb.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $material_bapb = db::table('allocation_bapb')
            ->where('warehouse','LIKE',"%$warehouse_id%")
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderby('created_at','desc');


            return DataTables::of($material_bapb)
            ->editColumn('qty_booking',function ($material_bapb)
            {
                return number_format($material_bapb->qty_booking, 4, '.', ',');
            })
            ->editColumn('created_at',function ($material_bapb)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_bapb->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse',function ($material_bapb)
            {
                if($material_bapb->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($material_bapb->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }
}
