<?php namespace App\Http\Controllers;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Models\Temporary;

class TemporaryController extends Controller
{

    public function isUsedByOther (Request $request){
        $is_exists = Temporary::where([
            ['barcode',$request->barcode],
            ['status',$request->status],
        ])
        ->exists();

        if($is_exists)
            return response()->json('exists',200);
        
        if(!$is_exists)
            return response()->json('not exists',200);
    }

    public function post(Request $request){
        $temporary = Temporary::where('user_id',Auth::user()->id)->delete();

        try{
            db::beginTransaction();
            $temporary = Temporary::firstOrCreate([
                'barcode' => $request->barcode,
                'warehouse' => auth::user()->warehouse,
                'status' => $request->status,
                'user_id' => Auth::user()->id
             ]);
            db::commit();
        }catch (Exception $ex){
            db::rollback();
            $message = $ex->getMessage();
            ErrorHandler::db($message);
        }

    }
    
    public function destroy(Request $request){
        $temporary = Temporary::where([
            ['barcode',$request->id],
            ['status',$request->status],
            ['warehouse',auth::user()->warehouse]
        ])
        ->delete();

        return response()->json(200);
    }

    public function resetAll(Request $request){
        $temporary = Temporary::where('user_id',Auth::user()->id)
        ->where('status',$request->status)
        ->delete();
        
        return response()->json(200);
    }
}
