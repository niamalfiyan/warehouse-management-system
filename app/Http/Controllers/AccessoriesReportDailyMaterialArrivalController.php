<?php namespace App\Http\Controllers;

use DB;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportDailyMaterialArrivalController extends Controller
{
    public function index()
    {
        //return view('errors.503');
        return view('accessories_report_daily_material_arrival.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            //dd($warehouse_id, $start_date, $end_date);
            $daily_arrivals     = DB::table('material_receivement_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->whereBetween('created_at',[$start_date,$end_date])
            ->orderby('created_at','desc');
            
            return DataTables::of($daily_arrivals)
            ->editColumn('created_at',function ($daily_arrivals)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_arrivals->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('qty_ordered',function ($daily_arrivals)
            {
                return number_format($daily_arrivals->qty_ordered, 4, '.', ',');
            })
            ->editColumn('qty_upload',function ($daily_arrivals)
            {
                return number_format($daily_arrivals->qty_upload, 4, '.', ',');
            })
            ->editColumn('warehouse_id',function ($daily_arrivals)
            {
                if($daily_arrivals->warehouse_id == '1000002') return 'Warehouse accessories AOI 1';
                elseif($daily_arrivals->warehouse_id == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $filename   = 'report_material_receivement';
        $file       = Config::get('storage.report') . '/' . e($filename).'.csv';
        
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
