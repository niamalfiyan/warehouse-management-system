<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Mail;
use Excel;
use File;
use View;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\CuttingInstruction;
use App\Models\MaterialStockPerLot;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialPlanningFabric;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\HistoryMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\DetailMaterialPlanningFabricPerPart;

use App\Http\Controllers\MasterDataAutoAllocationController as AllocationFabric;

class FabricMaterialPlanningController extends Controller
{
    public function index()
    {
        //return view('errors.503');
        return view('fabric_material_planning.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null);
            $end_date           = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : null);

            $material_plannings = MaterialPlanningFabric::whereBetween('planning_date',[$start_date,$end_date])
            ->where('warehouse_id',$warehouse_id);

            return DataTables::of($material_plannings)
            ->editColumn('qty_consumtion',function ($material_plannings)
            {
                return number_format($material_plannings->qty_consumtion, 4, '.', ',');
            })
            ->editColumn('planning_date',function ($material_plannings)
            {
                return  $material_plannings->planning_date->format('d/M/Y'); 
                //else return '-';
            })
            ->editColumn('is_piping',function($material_plannings)
            {
            	if ($material_plannings->is_piping) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->rawColumns(['is_piping'])
            ->make(true);
        }
    }

    public function downloadSPK(Request $request)
    {
        if($request->_start_date=='' || $request->_end_date=='')
        {
            return redirect()->back()
            ->withErrors([
                'start_date' => 'Please select planning cutting date start first',
                'end_date' => 'Please select planning cutting date to first',
            ]);
        }else
        {
            $start_date = ( $request->_start_date ? Carbon::createFromFormat('d/m/Y', $request->_start_date)->format('Y-m-d') : null);
            $end_date   = ( $request->_end_date ? Carbon::createFromFormat('d/m/Y', $request->_end_date)->format('Y-m-d') : null);
            $warehouse  = $request->warehouse_id;

            $data       = db::table('surat_perintah_kerja_v')
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->where('warehouse_id','LIKE',"%$warehouse%")
            ->get();

            return Excel::create('surat perintah kerja',function($excel) use($data){
            $excel->sheet('active',function($sheet) use ($data){
                $sheet->setWidth(array(
                'A'=>5,
                'B'=>20,
                'B'=>23,
                'C'=>27,
                'D'=>18,
                'E'=>30,
                'F'=>21,
                'G'=>20,
                ));
                $sheet->setHeight(array(
                '1'=>20,
                ));
                $sheet->setCellValue('A1','No.');
                $sheet->setCellValue('B1','DATE START SCHEDULE');
                $sheet->setCellValue('C1','STYLE');
                $sheet->setCellValue('D1','PO BUYER');
                $sheet->setCellValue('E1','DESTINATION');
                $sheet->setCellValue('F1','ARTICLE NO');
                $sheet->setCellValue('G1','MATERIAL');
                $sheet->setCellValue('H1','COLOR NAME');
                $sheet->setCellValue('I1','IS PIPING');
                $sheet->setCellValue('J1','SUM OF FBC');
                $sheet->setCellValue('K1','PO SUPPLIER');
                $sheet->setCellValue('L1','LOCATOR');
        
                $row=2;
                foreach ($data as $key => $i)
                {
                    $sheet->setHeight(array(
                        $row    =>  20,
                    ));
                    
                    $sheet->setCellValue('A'.$row,$key+1);
                    $sheet->setCellValue('B'.$row,Carbon::parse($i->planning_date)->format('d/M/Y'));
                    $sheet->setCellValue('C'.$row,$i->_style);
                    $sheet->setCellValue('D'.$row,$i->po_buyer);
                    $sheet->setCellValue('E'.$row,$i->destination);
                    $sheet->setCellValue('F'.$row,$i->article_no);
                    $sheet->setCellValue('G'.$row,$i->item_code);
                    $sheet->setCellValue('H'.$row,$i->color);
                    $sheet->setCellValue('I'.$row,$i->is_piping);
                    $sheet->setCellValue('J'.$row,$i->qty_booking);
                    $sheet->setCellValue('K'.$row,$i->document_no);
                    $sheet->setCellValue('L'.$row,$i->locator);
                    $row++;
                }
            });
            })
            ->export('xlsx');
        }
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'start_date'    => 'required',
            'end_date'      => 'required',
        ]);

        if ($validator->passes()) 
        {
            $app_env            = Config::get('app.env');
            $start_date         = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null);
            $end_date           = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : null);
            $warehouse_user     = $request->warehouse;
            $movement_date      = carbon::now()->toDateTimeString();

            if($warehouse_user == '1000001') $_warehouse = '1000005';
            else if($warehouse_user == '1000011') $_warehouse = '1000012';

           
            $cutting_instructions =  DB::connection('erp')
            ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_date(
                '".$start_date."',
                '".$end_date."',
                '".$_warehouse."'
                )
                order by datestartschedule asc,po_buyer asc,item_code asc;"
            )); 
            

            $cutting_instruction_per_parts =  DB::connection('erp')
            ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_per_part(
                '".$start_date."',
                '".$end_date."',
                '".$_warehouse."'
                )
                order by datestartschedule asc,po_buyer asc,item_code asc;"
            )); 
            

            $curr_planning_po_buyers        = array();
            $check_planning_on_po_buyers    = array();
            $planning_cancel_wms            = array();
            $_concate2                      = '';
            $concate                        = '';
            
            foreach ($cutting_instructions as $key => $cutting_instruction) 
            {
                $po_buyer           = $cutting_instruction->po_buyer;
                $item_code          = strtoupper($cutting_instruction->item_code);
                $planning_date      = $cutting_instruction->datestartschedule;
                $warehouse_id       = $warehouse_user;
                $item_id            = $cutting_instruction->item_id;
                $style              = $cutting_instruction->style;
                $article_no         = $cutting_instruction->article_no;
                $job_order          = $cutting_instruction->job_order;
                $color              = $cutting_instruction->color;
                $uom                = $cutting_instruction->uom;
                $is_piping          = $cutting_instruction->is_piping;
                $qty_consumtion     = sprintf('%0.8f',$cutting_instruction->qty_consumtion);
                $is_po_buyer_cancel = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();
                
                
                if(!$is_po_buyer_cancel)
                {
                    $obj                                = new stdclass();
                    $obj->planning_date                 = $planning_date;
                    $obj->po_buyer                      = $po_buyer;
                    $obj->item_id                       = $item_id;
                    $obj->item_code                     = $item_code;
                    $obj->color                         = $color;
                    $obj->style                         = $style;
                    $obj->article_no                    = $article_no;
                    $obj->job_order                     = $job_order;
                    $obj->uom                           = $uom;
                    $obj->is_piping                     = $is_piping;
                    $obj->qty_consumtion                = $qty_consumtion;
                    $obj->warehouse_id                  = $warehouse_id;
                    $curr_planning_po_buyers []         = $obj;
                    $_concate2                          .= "'".$po_buyer."',";
                }
            }

            //check planning on wms
            $planning_on_wms = MaterialPlanningFabric::select('po_buyer')
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->where('warehouse_id',$warehouse_user)
            ->groupby('po_buyer')
            ->get();

            foreach ($planning_on_wms as $key => $planning_on_wms) $_concate2   .= "'".$planning_on_wms->po_buyer."',";

           
            if($_concate2 != '')
            {
                $_concate2 = substr_replace($_concate2, '', -1);
                $planning_po_buyers = db::select(db::raw("
                    select * from material_planning_fabrics
                    where (po_buyer,item_id,style,article_no,is_piping,planning_date,warehouse_id) not in (
                            select po_buyer,item_id,style,article_no,is_piping,planning_date,warehouse_id
                            From erp_fabric_planning_per_buyer_v
                            where  po_buyer::varchar = any(array[".$_concate2 ."])
                    )
                    and po_buyer::varchar = any(array[".$_concate2 ."])
                "));

                foreach ($planning_po_buyers as $key_2 => $planning_po_buyer) 
                {
                    $material_planning_fabric_id    = $planning_po_buyer->id;
                    $planning_cancel_wms []         = $material_planning_fabric_id;
                }
            }

            //delete planning cancel
            if(count($planning_cancel_wms) > 0)
            {
                try 
                {
                    DB::beginTransaction();

                    $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                        ,'c_order_id'
                        ,'_style'
                        ,'warehouse_id'
                        ,'article_no'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_source'
                        ,'item_id_book'
                        ,'uom'
                        ,'planning_date'
                        ,'is_piping'
                        ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                        ,db::raw("sum(qty_booking) as total_booking"))
                    ->whereIn('material_planning_fabric_id',array_unique($planning_cancel_wms))
                    ->groupby('document_no'
                        ,'warehouse_id'
                        ,'uom'
                        ,'item_code'
                        ,'item_code_source'
                        ,'item_id_source'
                        ,'item_id_book'
                        ,'c_order_id'
                        ,'planning_date'
                        ,'_style'
                        ,'article_no'
                        ,'is_piping')
                    ->get();
                    
                    foreach ($detail_material_plannings as $key => $detail_material_planning) 
                    {
                        
                        $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                        $_document_no_preparation       = $detail_material_planning->document_no;
                        $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                        $_item_id_book_preparation      = $detail_material_planning->item_id_book;
                        $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                        $_item_code_preparation         = $detail_material_planning->item_code;
                        $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                        $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                        $_is_piping_preparation         = $detail_material_planning->is_piping;
                        $_article_no_preparation        = $detail_material_planning->article_no;
                        $_style_preparation             = $detail_material_planning->_style;
                        $_uom                           = $detail_material_planning->uom;
                        $_po_buyer                      = $detail_material_planning->po_buyer;
                        $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                        $is_material_preparation_exists = MaterialPreparationFabric::where([
                            ['article_no',$_article_no_preparation],
                            ['c_order_id',$_c_order_id_preparation],
                            ['warehouse_id',$_warehouse_id_preparation],
                            ['item_id_source',$_item_id_source_preparation],
                            ['item_id_book',$_item_id_book_preparation],
                            ['planning_date',$_planning_date_preparation],
                            ['_style',$_style_preparation],
                            ['is_piping',$_is_piping_preparation],
                        ])
                        ->get();

                        if(count($is_material_preparation_exists) > 0)
                        {
                            foreach ($is_material_preparation_exists as $key1 => $is_material_preparation_exist) 
                            {
                                $total_roll = $is_material_preparation_exist->detailMaterialPreparationFabric()
                                ->where('is_closing',false)
                                ->count();
                                if($is_material_preparation_exist->is_from_additional == false){

                                    if($total_roll == 0)
                                    {
                                        $is_material_preparation_exist->delete();
                                    }else
                                    {
                                        $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exist->total_reserved_qty);
                                        $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exist->total_qty_rilex);
                
                                        if($_total_booking_preparation != $total_reserved_qty)
                                        {
                                            $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                                            
                                            if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                                            else $remark_planning = 'pengurangan qty alokasi';
                
                                            $remark_planning .= '. Perubahan total qty prepare karena plan yang di cancel . Sebelumnya digunakan untuk po buyer '.$_po_buyer;
                
                                            HistoryPreparationFabric::create([
                                                'material_preparation_fabric_id'    => $is_material_preparation_exist->id,
                                                'remark'                            => $remark_planning,
                                                'qty_before'                        => $total_reserved_qty,
                                                'qty_after'                         => $_total_booking_preparation,
                                                'created_at'                        => $movement_date,
                                                'updated_at'                        => $movement_date,
                                            ]);
                
                                            $is_material_preparation_exist->updated_at             = $movement_date;
                                            $is_material_preparation_exist->total_reserved_qty     = $_total_booking_preparation;
                                            $is_material_preparation_exist->total_qty_outstanding  = $new_qty_oustanding;
                                        }
                                        
                                        $curr_remark_planning = $is_material_preparation_exist->remark_planning;
                                        $is_material_preparation_exist->remark_planning = ( $curr_remark_planning ? $curr_remark_planning .', Cancel Planning Total cancel qty for plan on '.$_planning_date_preparation.' is '.$_total_booking_preparation.'('.$_uom.'). Please cancel this preparation and do preparation again for new plan.' : 'Cancel Planning Total cancel qty for plan on '.$_planning_date_preparation.' is '.$_total_booking_preparation.'('.$_uom.'). Please cancel this preparation and do preparation again for new plan.' );
                                        $is_material_preparation_exist->save();
                                    }
                                }
                            }
                        }
                    }

                    $delete_material_plannings = MaterialPlanningFabric::whereIn('id',array_unique($planning_cancel_wms))->get();
            
                    foreach ($delete_material_plannings as $key => $delete_material_planning) 
                    {
                        HistoryMaterialPlanningFabric::FirstOrCreate([
                            'po_buyer'          => $delete_material_planning->po_buyer,
                            'item_id'           => $delete_material_planning->item_id,
                            'item_code'         => $delete_material_planning->item_code,
                            'color'             => $delete_material_planning->color,
                            'planning_date'     => $delete_material_planning->planning_date,
                            'style'             => $delete_material_planning->style,
                            'article_no'        => $delete_material_planning->article_no,
                            'job_order'         => $delete_material_planning->job_order,
                            'uom'               => $delete_material_planning->uom,
                            'is_piping'         => $delete_material_planning->is_piping,
                            'qty_consumtion'    => $delete_material_planning->qty_consumtion,
                            'warehouse_id'      => $delete_material_planning->warehouse_id,
                            'created_at'        => $movement_date,
                            'updated_at'        => $movement_date,
                            'note'              => 'PLANNING DI HAPUS'
                        ]);

                        DetailPoBuyerPerRoll::where('material_planning_fabric_id',$delete_material_planning->id)->delete();

                        $delete_material_planning->delete();
                    }

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }

            $material_planning_fabric_ids   = array();
            $concatenate                    = ''; 
            
            try 
            {
                DB::beginTransaction();

                foreach ($curr_planning_po_buyers as $key_1 => $curr_planning_po_buyer) 
                {
                    $planning_date                  = $curr_planning_po_buyer->planning_date;
                    $po_buyer                       = $curr_planning_po_buyer->po_buyer;
                    $item_id                        = $curr_planning_po_buyer->item_id;
                    $item_code                      = $curr_planning_po_buyer->item_code;
                    $style                          = $curr_planning_po_buyer->style;
                    $color                          = $curr_planning_po_buyer->color;
                    $is_piping                      = $curr_planning_po_buyer->is_piping;
                    $article_no                     = $curr_planning_po_buyer->article_no;
                    $job_order                      = $curr_planning_po_buyer->job_order;
                    $uom                            = $curr_planning_po_buyer->uom;
                    $qty_consumtion                 = sprintf("%.8f",$curr_planning_po_buyer->qty_consumtion);
                    $warehouse_id                   = $curr_planning_po_buyer->warehouse_id;
                    
                    $is_planning_exists = MaterialPlanningFabric::where([
                        ['planning_date',$planning_date],
                        ['item_id',$item_id],
                        ['po_buyer',$po_buyer],
                        ['style',$style],
                        ['article_no',$article_no],
                        ['is_piping',$is_piping],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->first();

                    if(!$is_planning_exists)
                    {
                        $new_material_planning_fabric = MaterialPlanningFabric::FirstOrCreate([
                            'po_buyer'          => $po_buyer,
                            'item_id'           => $item_id,
                            'item_code'         => $item_code,
                            'color'             => $color,
                            'planning_date'     => $planning_date,
                            'style'             => $style,
                            'article_no'        => $article_no,
                            'job_order'         => $job_order,
                            'uom'               => $uom,
                            'is_piping'         => $is_piping,
                            'qty_consumtion'    => $qty_consumtion,
                            'warehouse_id'      => $warehouse_id,
                            'user_id'           => auth::user()->id,
                            'created_at'        => $movement_date,
                            'updated_at'        => $movement_date
                        ]);
                        
                        $concatenate                    .= "'".$po_buyer."',";
                        $material_planning_fabric_ids [] = $new_material_planning_fabric->id;

                        DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$new_material_planning_fabric->id)->delete();
                    }else
                    {
                        
                        $concatenate                       .= "'".$is_planning_exists->po_buyer."',";
                        $old_qty_consumtion                 = $is_planning_exists->qty_consumtion;

                        if($old_qty_consumtion != $qty_consumtion)
                        {
                            HistoryMaterialPlanningFabric::FirstOrCreate([
                                'po_buyer'          => $is_planning_exists->po_buyer,
                                'item_id'           => $is_planning_exists->item_id,
                                'item_code'         => $is_planning_exists->item_code,
                                'color'             => $is_planning_exists->color,
                                'planning_date'     => $is_planning_exists->planning_date,
                                'style'             => $is_planning_exists->style,
                                'article_no'        => $is_planning_exists->article_no,
                                'job_order'         => $is_planning_exists->job_order,
                                'uom'               => $is_planning_exists->uom,
                                'is_piping'         => $is_planning_exists->is_piping,
                                'qty_consumtion'    => $is_planning_exists->qty_consumtion,
                                'warehouse_id'      => $is_planning_exists->warehouse_id,
                                'created_at'        => $movement_date,
                                'updated_at'        => $movement_date,
                                'note'              => 'QTY CONSUMTION BERUBAH MENJADI '.$qty_consumtion
                            ]);
                            
                            $is_planning_exists->updated_at     = $movement_date;
                            $is_planning_exists->qty_consumtion = $qty_consumtion;
                            $is_planning_exists->save();
                            DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$is_planning_exists->id)->delete();
                        }

                        $material_planning_fabric_ids [] = $is_planning_exists->id;
                    }
                }

                foreach ($cutting_instruction_per_parts as $key => $cutting_instruction_per_part) 
                {
                    $po_buyer           = $cutting_instruction_per_part->po_buyer;
                    $planning_date      = $cutting_instruction_per_part->datestartschedule;
                    $warehouse_id       = $warehouse_user;
                    $item_id            = $cutting_instruction_per_part->item_id;
                    $style              = $cutting_instruction_per_part->style;
                    $article_no         = $cutting_instruction_per_part->article_no;
                    $uom                = $cutting_instruction_per_part->uom;
                    $is_piping          = $cutting_instruction_per_part->is_piping;
                    $part_no            = $cutting_instruction_per_part->part_no;
                    $qty_consumtion     = sprintf('%0.8f',$cutting_instruction_per_part->qty_consumtion);

                    $is_planning_exists = MaterialPlanningFabric::where([
                        ['planning_date',$planning_date],
                        ['item_id',$item_id],
                        ['po_buyer',$po_buyer],
                        ['style',$style],
                        ['article_no',$article_no],
                        ['is_piping',$is_piping],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->whereNull('deleted_at')
                    ->first();

                    if($is_planning_exists)
                    {
                        $is_planning_per_pert_exists = DetailMaterialPlanningFabricPerPart::where([
                            ['material_planning_fabric_id',$is_planning_exists->id],
                            ['part_no',$part_no],
                            ['qty_per_part',$qty_consumtion],
                        ])
                        ->exists();

                        if(!$is_planning_per_pert_exists)
                        {
                            DetailMaterialPlanningFabricPerPart::FirstOrCreate([
                                'material_planning_fabric_id'   => $is_planning_exists->id,
                                'part_no'                       => $part_no,
                                'qty_per_part'                  => $qty_consumtion,
                                'created_at'                    => $movement_date,
                                'updated_at'                    => $movement_date
                            ]);
                        }
                    }
                
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            
            try 
            {
                DB::beginTransaction();

                if($concatenate != '')
                {
                    $concatenate = substr_replace($concatenate, '', -1);
                    DB::select(db::raw("SELECT *  FROM delete_duplicate_planning_fabric(array[".$concatenate ."]);" ));
                }


                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $this->getAllocationFabric($material_planning_fabric_ids);
            $this->storeCuttingInstructionHeaderReport($start_date,$end_date,$warehouse_user);

            if($app_env == 'dev')
            {
                $this->insertMaterialPreparationFabric($material_planning_fabric_ids);
                $this->checkMaterialPreparationFabric($start_date,$end_date,$warehouse_user);
            }else
            {
                //if($start_date >= '2019-09-25')
                //{
                    $this->insertMaterialPreparationFabric($material_planning_fabric_ids);
                    $this->checkMaterialPreparationFabric($start_date,$end_date,$warehouse_user);
                //} 
            }
            
            
            //$this->insertMaterialPreparationFabric($material_planning_fabric_ids);
            //$this->checkMaterialPreparationFabric($start_date,$end_date,$warehouse_user);
            

            return response()->json(['start_date' => $start_date, 'end_date' => $end_date,'total' => count($material_planning_fabric_ids)],200);

            
        }else
        {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400); 
        }
  
    }

    static function getAllocationFabric($material_planning_fabric_ids)
    {
        $material_planning_fabrics  = MaterialPlanningFabric::whereIn('id',$material_planning_fabric_ids)
        ->orderby('is_piping','desc')
        ->get();
        
        $concatenate                = '';
        $movement_date              = carbon::now()->toDateTimeString();
        $document_no_arrays         = array();
        $item_code_source_arrays    = array();
        $item_code_arrays           = array();
        $check_details              = array();

        try 
        {
            DB::beginTransaction();

            foreach ($material_planning_fabrics as $key => $material_planning_fabric)  $check_details [] = $material_planning_fabric->id;
            
            $cheks = db::table('planning_vs_detail_planning_fabric_v')
            ->whereIn('id',$check_details)
            ->whereRaw("total_booking::numeric != qty_consumtion::numeric")
            ->get();

            foreach ($cheks as $key => $value) 
            {
                if($value->total_booking > $value->qty_consumtion)
                {
                    DetailMaterialPlanningFabric::where('material_planning_fabric_id',$value->id)->delete();
                } 
            }

            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_allocated                  = sprintf("%.8f",$material_planning_fabric->getTotalAllocation($material_planning_fabric->id));
                $qty_consumtion                 = sprintf("%.8f",$material_planning_fabric->qty_consumtion);
                $qty_outstanding                = sprintf("%.8f",$qty_consumtion - $qty_allocated);
                $po_buyer                       = $material_planning_fabric->po_buyer;
                $item_code                      = $material_planning_fabric->item_code;
                $item_id                        = $material_planning_fabric->item_id;
                $style                          = $material_planning_fabric->style;
                $article_no                     = $material_planning_fabric->article_no;
                $warehouse_id                   = $material_planning_fabric->warehouse_id;
                $planning_date                  = $material_planning_fabric->planning_date;
                $is_piping                      = $material_planning_fabric->is_piping;
                $material_planning_fabric_id    = $material_planning_fabric->id;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $allocation_items = AllocationItem::where([
                    ['po_buyer',$po_buyer],
                    ['item_id_book',$item_id],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_additional',false],
                ])
                ->orderby('c_order_id','asc')
                ->orderby('qty_booking','asc')
                ->get();

                foreach ($allocation_items as $key_2 => $allocation_item)
                {
                    $allocation_item_id         = $allocation_item->id;
                    $document_no                = $allocation_item->document_no;
                    $c_bpartner_id              = $allocation_item->c_bpartner_id;
                    $supplier_name              = $allocation_item->supplier_name;
                    $uom                        = $allocation_item->uom;
                    $item_id_source             = $allocation_item->item_id_source;
                    $item_code_source           = $allocation_item->item_code_source;
                    $item_id_book               = $allocation_item->item_id_book;
                    $item_code                  = $allocation_item->item_code;
                    $c_order_id                 = $allocation_item->c_order_id;
                    $_style                     = $allocation_item->_style;
                    $material_stock_per_lot     = MaterialStockPerLot::find($allocation_item->material_stock_per_lot_id);
                    $actual_lot                 = ($material_stock_per_lot ? $material_stock_per_lot->actual_lot : null);

                    if($c_order_id == null || $c_order_id == '')
                    {
                        $po_supplier = PoSupplier::where('document_no', $document_no)->first();
                        $c_order_id  = ($po_supplier ? $po_supplier->c_order_id : null );
                    }
                    
                    $document_no_arrays []      = $document_no;
                    $item_code_source_arrays [] = $item_code_source;
                    $item_code_arrays []        = $item_code;

                    if($qty_outstanding > 0)
                    {
                        $qty_allocation_allocated       = sprintf("%.8f",$allocation_item->getTotalAllocation($allocation_item->id));
                        $qty_booking                    = sprintf("%.8f",$allocation_item->qty_booking);
                        $qty_allocation_outstanding     = sprintf("%.8f",$qty_booking - $qty_allocation_allocated);

                        if($qty_allocation_outstanding >0)
                        {
                            if ($qty_outstanding/$qty_allocation_outstanding >= 1) $supplied = $qty_allocation_outstanding;
                            else $supplied = $qty_outstanding;

                            if($supplied >0)
                            {
                                DetailMaterialPlanningFabric::firstOrCreate([
                                    'allocation_item_id'            => $allocation_item_id,
                                    'material_planning_fabric_id'   => $material_planning_fabric_id,
                                    'planning_date'                 => $planning_date,
                                    'planning_date'                 => $planning_date,
                                    'po_buyer'                      => $po_buyer,
                                    'c_order_id'                    => $c_order_id,
                                    'item_code'                     => $item_code,
                                    'item_id_book'                  => $item_id_book,
                                    'item_code_source'              => $item_code_source,
                                    'item_id_source'                => $item_id_source,
                                    'qty_booking'                   => sprintf("%.8f",$supplied),
                                    'actual_lot'                    => $actual_lot,
                                    'style'                         => $style,
                                    '_style'                        => $_style,
                                    'article_no'                    => $article_no,
                                    'is_from_additional'            => false,
                                    'is_piping'                     => $is_piping,
                                    'warehouse_id'                  => $warehouse_id,
                                    'document_no'                   => $document_no,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'uom'                           => $uom,
                                    'user_id'                       => $system->id,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                ]);
                                $concatenate .= "'".$po_buyer."',";
                                $qty_outstanding -= $supplied;
                            } 
                        }
                        
                    }
                }

            }
        
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();

            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_detail_planning_fabric(array[".$concatenate ."]);" ));
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //check if prepare already exsits
        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                ,'c_order_id'
                ,'warehouse_id'
                ,'_style'
                ,'article_no'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_source'
                ,'item_id_book'
                ,'uom'
                ,'planning_date'
                ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                ,'is_piping',db::raw("sum(qty_booking) as total_booking"))
            ->whereIn('document_no',array_unique($document_no_arrays))
            ->whereIn('item_code_source',array_unique($item_code_source_arrays))
            ->whereIn('item_code',array_unique($item_code_arrays))
            ->groupby('document_no'
                ,'warehouse_id'
                ,'c_order_id'
                ,'article_no'
                ,'_style'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_source'
                ,'item_id_book'
                ,'uom'
                ,'planning_date'
                ,'is_piping')
            ->get();

            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $_item_code_preparation             = $detail_material_planning->item_code;
                $_c_order_id_preparation            = $detail_material_planning->c_order_id;
                $_document_no_preparation           = $detail_material_planning->document_no;
                $_article_no_preparation            = $detail_material_planning->article_no;
                $_warehouse_id_preparation          = $detail_material_planning->warehouse_id;
                $_item_code_source_preparation      = $detail_material_planning->item_code_source;
                $_item_id_source_preparation        = $detail_material_planning->item_id_source;
                $_item_id_book_preparation          = $detail_material_planning->item_id_book;
                $_planning_date_preparation         = $detail_material_planning->planning_date->format('d/M/y');
                $_is_piping_preparation             = $detail_material_planning->is_piping;
                $_uom                               = $detail_material_planning->uom;
                $_po_buyer                          = $detail_material_planning->po_buyer;
                $_style_preparation                 = $detail_material_planning->_style;
                $_total_booking_preparation         = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['article_no',$_article_no_preparation],
                    ['c_order_id',$_c_order_id_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_id_book',$_item_id_book_preparation],
                    ['item_id_source',$_item_id_source_preparation],
                    ['_style',$_style_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                        
                        if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                        else $remark_planning = 'pengurangan qty alokasi';

                        $remark_planning .= '. Digunakan untuk po buyer '.$_po_buyer;
                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning,
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->updated_at            = $movement_date;
                        $is_material_preparation_exists->total_reserved_qty    = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding = $new_qty_oustanding;
                    }
                    
                    $is_material_preparation_exists->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function storeCuttingInstructionHeaderReport($start_date,$end_date,$warehouse_id)
    {

        $material_planning_fabrics = MaterialPlanningFabric::select(db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'uom'
            ,'warehouse_id'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->where('warehouse_id',$warehouse_id)
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            CuttingInstruction::where('warehouse_id',$warehouse_id)
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->delete();

            $movement_date = carbon::now()->toDateTimeString();
            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.8f",$material_planning_fabric->qty_need);
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $uom            = $material_planning_fabric->uom;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertMaterialPreparationFabric($material_planning_fabric_ids)
    {
        $movement_date = carbon::now();
        try 
        {
            DB::beginTransaction();

            $allocations = DetailMaterialPlanningFabric::select('document_no'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'c_order_id'
                ,'_style'
                ,'article_no'
                ,'warehouse_id'
                ,'item_code_source'
                ,'item_id_source'
                ,'planning_date'
                ,'c_bpartner_id'
                ,'is_piping'
                ,'uom'
                ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                ,db::raw("sum(qty_booking) as qty_prepare")
            )
            ->whereIn('material_planning_fabric_id',$material_planning_fabric_ids)
            ->groupby('document_no'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'c_order_id'
                ,'article_no'
                ,'_style'
                ,'warehouse_id'
                ,'item_code_source'
                ,'item_id_source'
                ,'planning_date'
                ,'c_bpartner_id'
                ,'is_piping'
                ,'uom')
            ->get();

            foreach ($allocations as $key => $allocation) 
            {
                
                $_document_no_preparation       = $allocation->document_no;
                $_article_no_preparation        = $allocation->article_no;
                $_warehouse_id_preparation      = $allocation->warehouse_id;
                $_c_order_id_preparation        = $allocation->c_order_id;
                $_item_code_preparation         = $allocation->item_code;
                $_item_code_source_preparation  = $allocation->item_code_source;
                $_item_id_book_preparation      = $allocation->item_id_book;
                $_item_id_source_preparation    = $allocation->item_id_source;
                $_c_bpartner_id_preparation     = $allocation->c_bpartner_id;
                $_planning_date_preparation     = $allocation->planning_date;
                $_is_piping_preparation         = $allocation->is_piping;
                $_style_preparation             = $allocation->_style;
                $_uom                           = $allocation->uom;
                $_po_buyer                      = $allocation->po_buyer;
                $_total_booking_preparation     = sprintf('%0.8f',$allocation->qty_prepare);

                if($_c_order_id_preparation == null || $_c_order_id_preparation == '')
                    {
                        $po_supplier = PoSupplier::where('document_no', $_document_no_preparation)->first();
                        $_c_order_id_preparation  = ($po_supplier ? $po_supplier->c_order_id : null );
                    }

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$_c_order_id_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_id_book',$_item_id_book_preparation],
                    ['item_id_source',$_item_id_source_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                    ['_style',$_style_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                        
                        if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                        else $remark_planning = 'pengurangan qty alokasi';

                        $remark_planning .= '. Digunakan untuk po buyer '.$_po_buyer;
                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning,
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->updated_at            = $movement_date;
                        $is_material_preparation_exists->total_reserved_qty    = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding = $new_qty_oustanding;
                        
                    }else
                    {
                        if($_total_booking_preparation == $total_reserved_qty)
                        {
                            $is_material_preparation_exists->remark_planning = null;
                        }
                    }
                    
                    $is_material_preparation_exists->created_at                 = $_planning_date_preparation;
                    $is_material_preparation_exists->updated_at                 = $_planning_date_preparation;
                    $is_material_preparation_exists->save();

                    $material_preparation_fabric_id = $is_material_preparation_exists->id;
                    DB::select(db::raw("SELECT * FROM delete_duplicate_summary_preparation_fabric('".$material_preparation_fabric_id."');"));
                }else
                {
                    $supplier = Supplier::where('c_bpartner_id',$_c_bpartner_id_preparation)->first();

                    $_material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                        'c_bpartner_id'         => $_c_bpartner_id_preparation,
                        'supplier_name'         => ($supplier ? $supplier->supplier_name : null ),
                        'document_no'           => $_document_no_preparation,
                        'item_code'             => $_item_code_preparation,
                        'item_code_source'      => $_item_code_source_preparation,
                        'c_order_id'            => $_c_order_id_preparation,
                        'item_id_book'          => $_item_id_book_preparation,
                        'item_id_source'        => $_item_id_source_preparation,
                        'uom'                   => $_uom,
                        'total_reserved_qty'    => sprintf('%0.8f',$_total_booking_preparation),
                        'total_qty_outstanding' => sprintf('%0.8f',$_total_booking_preparation),
                        'planning_date'         => $_planning_date_preparation,
                        'is_piping'             => $_is_piping_preparation,
                        'article_no'            => $_article_no_preparation,
                        '_style'                => $_style_preparation,
                        'is_from_additional'    => false,
                        'preparation_date'      => null,
                        'preparation_by'        => null,
                        'warehouse_id'          => $_warehouse_id_preparation,
                        'user_id'               => auth::user()->id,
                        'created_at'            => $movement_date,
                        'updated_at'            => $movement_date
                    ]);

                    HistoryPreparationFabric::create([
                        'material_preparation_fabric_id'    => $_material_preparation_fabric->id,
                        'remark'                            => 'Digunakan untuk po buyer '.$_po_buyer,
                        'qty_before'                        => 0,
                        'qty_after'                         => $_material_preparation_fabric->total_reserved_qty,
                        'created_at'                        => $_material_preparation_fabric->created_at,
                        'updated_at'                        => $_material_preparation_fabric->created_at,
                    ]);

                    $material_preparation_fabric_id = $_material_preparation_fabric->id;
                    DB::select(db::raw("SELECT * FROM delete_duplicate_summary_preparation_fabric('".$material_preparation_fabric_id."');"));
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function checkMaterialPreparationFabric($start_date,$end_date,$warehouse_id)
    {
        $material_preparation_fabrics = MaterialPreparationFabric::wherebetween('planning_date',[$start_date,$end_date])
        ->where('warehouse_id',$warehouse_id)
        ->whereNotNull('planning_date')
        ->get();

        try 
        {
            DB::beginTransaction();

            $updated_at = carbon::now();
            foreach ($material_preparation_fabrics as $key => $material_preparation_fabric) 
            {
                $material_preparation_fabric_id = $material_preparation_fabric->id;
                $is_piping                      = $material_preparation_fabric->is_piping;
                $c_order_id                     = $material_preparation_fabric->c_order_id;
                $item_id_book                   = $material_preparation_fabric->item_id_book;
                $item_id_source                 = $material_preparation_fabric->item_id_source;
                $_style                         = $material_preparation_fabric->_style;
                $warehouse_id                   = $material_preparation_fabric->warehouse_id;
                $article_no                     = $material_preparation_fabric->article_no;
                $planning_date                  = $material_preparation_fabric->planning_date;
                $total_reserved_qty             = sprintf("%.8f",$material_preparation_fabric->total_reserved_qty);

                $is_allocation_exists           = DetailMaterialPlanningFabric::where([
                    ['warehouse_id',$warehouse_id],
                    ['is_piping',$is_piping],
                    ['article_no',$article_no],
                    ['_style',$_style],
                    ['planning_date',$planning_date],
                    ['c_order_id',$c_order_id],
                    ['item_id_book',$item_id_book],
                    ['item_id_source',$item_id_source],
                ])
                ->exists();

                if(!$is_allocation_exists)
                {
                    $is_detail_exists = DetailMaterialPreparationFabric::where([
                        ['material_preparation_fabric_id',$material_preparation_fabric_id],
                        ['is_closing',false],
                    ])
                    ->exists();

                    $total_roll = $material_preparation_fabric->detailMaterialPreparationFabric()
                    ->where('is_closing',false)
                    ->count();

                    if($material_preparation_fabric->is_from_additional == false){
                        

                        if(!$is_detail_exists && $total_roll == 0)
                        {
                            $material_preparation_fabric->delete();
                        }else
                        {
                            HistoryPreparationFabric::create([
                                'material_preparation_fabric_id'    => $material_preparation_fabric->id,
                                'remark'                            => 'Alokasi untuk plan ini dihapus',
                                'qty_before'                        => $material_preparation_fabric->total_reserved_qty,
                                'qty_after'                         => 0,
                                'created_at'                        => $updated_at,
                                'updated_at'                        => $updated_at,
                            ]);

                            $material_preparation_fabric->updated_at            = $updated_at;
                            $material_preparation_fabric->total_reserved_qty    = 0;
                            $material_preparation_fabric->total_qty_outstanding = 0;
                            $material_preparation_fabric->remark_planning       = 'please cancel this data due allocation is not longer exists';
                            $material_preparation_fabric->save();
                        }                  
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function getAllocationFabricPerBuyer($plannings)
    {
        $material_planning_fabrics  = MaterialPlanningFabric::whereIn('po_buyer',$plannings)
        ->orderby('is_piping','desc')
        ->orderby('po_buyer','asc')
        ->get();

        $concatenate                = '';
        $movement_date              = Carbon::now()->toDateTimeString();
        $document_no_arrays         = array();
        $item_code_source_arrays    = array();
        $item_code_arrays           = array();
        $check_details              = array();

        try 
        {
            DB::beginTransaction();

            foreach ($material_planning_fabrics as $key => $material_planning_fabric)  $check_details [] = $material_planning_fabric->id;
            
            $cheks = db::table('planning_vs_detail_planning_fabric_v')
            ->whereIn('id',$check_details)
            //->where(db::raw("total_booking::numeric != qty_consumtion::numeric"))
            ->whereRaw("total_booking::numeric != qty_consumtion::numeric")
            ->get();

            foreach ($cheks as $key => $value) 
            {
                if($value->total_booking > $value->qty_consumtion)  DetailMaterialPlanningFabric::where('material_planning_fabric_id',$value->id)->delete();
            }

            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_allocated               = sprintf("%.8f",$material_planning_fabric->getTotalAllocation($material_planning_fabric->id));
                $qty_consumtion              = sprintf("%.8f",$material_planning_fabric->qty_consumtion);
                $qty_outstanding             = sprintf("%.8f",$qty_consumtion - $qty_allocated);
                $po_buyer                    = $material_planning_fabric->po_buyer;
                $item_id                     = $material_planning_fabric->item_id;
                $item_code                   = $material_planning_fabric->item_code;
                $style                       = $material_planning_fabric->style;
                $article_no                  = $material_planning_fabric->article_no;
                $warehouse_id                = $material_planning_fabric->warehouse_id;
                $planning_date               = $material_planning_fabric->planning_date;
                $is_piping                   = $material_planning_fabric->is_piping;
                $material_planning_fabric_id = $material_planning_fabric->id;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $allocation_items = AllocationItem::where([
                    ['po_buyer',$po_buyer],
                    ['item_id_book',$item_id],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_additional',false],
                ])
                ->orderby('c_order_id','asc')
                ->orderby('qty_booking','asc')
                ->get();

                foreach ($allocation_items as $key_2 => $allocation_item)
                {
                    $allocation_item_id     = $allocation_item->id;
                    $document_no            = $allocation_item->document_no;
                    $item_code              = $allocation_item->item_code;
                    $item_id_book           = $allocation_item->item_id_book;
                    $item_code_source       = $allocation_item->item_code_source;
                    $item_id_source         = $allocation_item->item_id_source;
                    $c_bpartner_id          = $allocation_item->c_bpartner_id;
                    $supplier_name          = $allocation_item->supplier_name;
                    $uom                    = $allocation_item->uom;
                    $c_order_id             = $allocation_item->c_order_id;
                    $_style                 = $allocation_item->_style;
                    $material_stock_per_lot = MaterialStockPerLot::find($allocation_item->material_stock_per_lot_id);
                    $actual_lot             = ($material_stock_per_lot ? $material_stock_per_lot->actual_lot : null);
                   
                    $document_no_arrays []      = $document_no;
                    $item_code_source_arrays [] = $item_code_source;
                    $item_code_arrays []        = $item_code;

                    if($qty_outstanding > 0)
                    {
                        $qty_allocation_allocated   = sprintf("%.8f",$allocation_item->getTotalAllocation($allocation_item->id));
                        $qty_booking                = sprintf("%.8f",$allocation_item->qty_booking);
                        $qty_allocation_outstanding = sprintf("%.8f",$qty_booking - $qty_allocation_allocated);

                        if($qty_allocation_outstanding >0)
                        {
                            if ($qty_outstanding/$qty_allocation_outstanding >= 1) $supplied = $qty_allocation_outstanding;
                            else $supplied = $qty_outstanding;

                            //echo $supplied.'<br/>';
                            if($supplied >0)
                            {
                                DetailMaterialPlanningFabric::FirstOrCreate([
                                    'allocation_item_id'            => $allocation_item_id,
                                    'material_planning_fabric_id'   => $material_planning_fabric_id,
                                    'planning_date'                 => $planning_date,
                                    'c_order_id'                    => $c_order_id,
                                    'po_buyer'                      => $po_buyer,
                                    'item_code'                     => $item_code,
                                    'item_id_book'                  => $item_id_book,
                                    'item_code_source'              => $item_code_source,
                                    'item_id_source'                => $item_id_source,
                                    'qty_booking'                   => sprintf("%.8f",$supplied),
                                    'style'                         => $style,
                                    '_style'                        => $_style,
                                    'article_no'                    => $article_no,
                                    'is_from_additional'            => false,
                                    'is_piping'                     => $is_piping,
                                    'warehouse_id'                  => $warehouse_id,
                                    'document_no'                   => $document_no,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'uom'                           => $uom,
                                    'user_id'                       => $system->id,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'actual_lot'                    => $actual_lot,
                                ]);

                                $concatenate .= "'".$po_buyer."',";
                                $qty_outstanding -= $supplied;
                            } 
                        }
                        
                    }
                }

            }

            DB::commit();
        }catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();

            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_detail_planning_fabric(array[".$concatenate ."]);" ));
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //check if prepare already exsits
        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                ,'c_order_id'
                ,'_style'
                ,'article_no'
                ,'warehouse_id'
                ,'item_id_book'
                ,'item_id_source'
                ,'item_code'
                ,'item_code_source'
                ,'uom'
                ,'planning_date'
                ,'is_piping'
                ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                ,db::raw("sum(qty_booking) as total_booking")
            )
            ->whereIn('document_no',array_unique($document_no_arrays))
            ->whereIn('item_code_source',array_unique($item_code_source_arrays))
            ->whereIn('item_code',array_unique($item_code_arrays))
            ->groupby('document_no'
                ,'c_order_id'
                ,'_style'
                ,'article_no'
                ,'warehouse_id'
                ,'item_id_book'
                ,'item_id_source'
                ,'item_code'
                ,'item_code_source'
                ,'uom'
                ,'planning_date'
                ,'is_piping')
            ->get();

            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                $_article_no_preparation        = $detail_material_planning->article_no;
                $_document_no_preparation       = $detail_material_planning->document_no;
                $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                $_item_code_preparation         = $detail_material_planning->item_code;
                $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                $_item_id_book_preparation      = $detail_material_planning->item_id_book;
                $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                $_planning_date_preparation     = $detail_material_planning->planning_date;
                $_style_preparation             = $detail_material_planning->_style;
                $_is_piping_preparation         = $detail_material_planning->is_piping;
                $_uom                           = $detail_material_planning->uom;
                $_po_buyer                      = $detail_material_planning->po_buyer;
                $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$_c_order_id_preparation],
                    ['document_no',$_document_no_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_code',$_item_code_preparation],
                    ['item_id_book',$_item_id_book_preparation],
                    ['item_id_source',$_item_id_source_preparation],
                    ['_style',$_style_preparation],
                    ['item_code_source',$_item_code_source_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                        
                        if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                        else $remark_planning = 'pengurangan qty alokasi';

                        $remark_planning .= '. digunakan untuk po buyer '.$_po_buyer;
                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning,
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->total_reserved_qty    = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding = $new_qty_oustanding;
                    }
                    
                    $is_material_preparation_exists->save();
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    static function dailyInsertPlanning($start_date,$end_date,$movement_date)
    {
        $cutting_instructions =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_date_schedule(
            '".$start_date."',
            '".$end_date."'
            )
            order by datestartschedule asc,po_buyer asc,item_code asc;"
        )); 

        $cutting_instruction_per_parts =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_per_part_schedule(
            '".$start_date."',
            '".$end_date."'
            )
            order by datestartschedule asc,po_buyer asc,item_code asc;"
        )); 


        $planning_cancel_wms            = array();
        $curr_planning_po_buyers        = array();
        $check_planning_on_po_buyers    = array();
        $_concate2                      = '';
        $concate                        = '';

        foreach ($cutting_instructions as $key => $cutting_instruction) 
        {
            $po_buyer               = $cutting_instruction->po_buyer;
            $item_id                = $cutting_instruction->item_id;
            $item_code              = $cutting_instruction->item_code;
            $planning_date          = $cutting_instruction->datestartschedule;
            $_warehouse_id          = $cutting_instruction->m_warehouse_id;
            $style                  = $cutting_instruction->style;
            $article_no             = $cutting_instruction->article_no;
            $job_order              = $cutting_instruction->job_order;
            $color                  = $cutting_instruction->color;
            $uom                    = $cutting_instruction->uom;
            $is_piping              = $cutting_instruction->is_piping;
            $qty_consumtion         = sprintf('%0.8f',$cutting_instruction->qty_consumtion);
            $is_po_buyer_cancel     = PoBuyer::where('po_buyer',$po_buyer)->whereNotNull('cancel_date')->exists();
            
            if($_warehouse_id == '1000005') $warehouse_id = '1000001';
            else if($_warehouse_id == '1000012') $warehouse_id = '1000011';

            if(!$is_po_buyer_cancel)
            {
                $obj                                = new stdclass();
                $obj->planning_date                 = $planning_date;
                $obj->po_buyer                      = $po_buyer;
                $obj->item_id                       = $item_id;
                $obj->item_code                     = $item_code;
                $obj->color                         = $color;
                $obj->style                         = $style;
                $obj->article_no                    = $article_no;
                $obj->job_order                     = $job_order;
                $obj->uom                           = $uom;
                $obj->is_piping                     = $is_piping;
                $obj->qty_consumtion                = $qty_consumtion;
                $obj->warehouse_id                  = $warehouse_id;
                $curr_planning_po_buyers []         = $obj;
                $_concate2                          .= "'".$po_buyer."',";
            }
        }

        //check planning on wms
        $planning_on_wms = MaterialPlanningFabric::select('po_buyer')
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->groupby('po_buyer')
        ->get();

        foreach ($planning_on_wms as $key => $planning_on_wms) $_concate2 .= "'".$planning_on_wms->po_buyer."',"; 
        

        if($_concate2 != '')
        {
            $_concate2 = substr_replace($_concate2, '', -1);
            $planning_po_buyers = db::select(db::raw("
                select * from material_planning_fabrics
                where (po_buyer,item_id,style,article_no,is_piping,planning_date,warehouse_id) not in (
                        select po_buyer,item_id,style,article_no,is_piping,planning_date,warehouse_id
                        From erp_fabric_planning_per_buyer_v
                        where  po_buyer::varchar = any(array[".$_concate2 ."])
                )
                and po_buyer::varchar = any(array[".$_concate2 ."])
            "));

            foreach ($planning_po_buyers as $key_2 => $planning_po_buyer) 
            {
                $material_planning_fabric_id    = $planning_po_buyer->id;
                $planning_cancel_wms []         = $material_planning_fabric_id;
            }
        }

        //delete planning cancel
        if(count($planning_cancel_wms) > 0)
        {
            try 
            {
                DB::beginTransaction();

                $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                    ,'c_order_id'
                    ,'warehouse_id'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_source'
                    ,'item_id_book'
                    ,'_style'
                    ,'uom'
                    ,'planning_date'
                    ,'is_piping'
                    ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                    ,db::raw("sum(qty_booking) as total_booking"))
                ->whereIn('material_planning_fabric_id',array_unique($planning_cancel_wms))
                ->groupby('document_no'
                    ,'c_order_id'
                    ,'warehouse_id'
                    ,'article_no'
                    ,'item_code'
                    ,'item_code_source'
                    ,'item_id_source'
                    ,'item_id_book'
                    ,'_style'
                    ,'uom'
                    ,'planning_date'
                    ,'is_piping')
                ->get();
                
                foreach ($detail_material_plannings as $key => $detail_material_planning) 
                {
                    
                    $_article_no_preparation        = $detail_material_planning->article_no;
                    $_document_no_preparation       = $detail_material_planning->document_no;
                    $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                    $_item_code_preparation         = $detail_material_planning->item_code;
                    $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                    $_item_id_book_preparation      = $detail_material_planning->item_id_book;
                    $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                    $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                    $_planning_date_preparation     = $detail_material_planning->planning_date;
                    $_style_preparation             = $detail_material_planning->_style;
                    $_is_piping_preparation         = $detail_material_planning->is_piping;
                    $_uom                           = $detail_material_planning->uom;
                    $_po_buyer                      = $detail_material_planning->po_buyer;
                    $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                    $is_material_preparation_exists = MaterialPreparationFabric::where([
                        ['article_no',$_article_no_preparation],
                        ['document_no',$_document_no_preparation],
                        ['warehouse_id',$_warehouse_id_preparation],
                        ['c_order_id',$_c_order_id_preparation],
                        ['item_code',$_item_code_preparation],
                        ['item_code_source',$_item_code_source_preparation],
                        ['planning_date',$_planning_date_preparation],
                        ['item_id_book',$_item_id_book_preparation],
                        ['_style',$_style_preparation],
                        ['item_id_source',$_item_id_source_preparation],
                        ['is_piping',$_is_piping_preparation],
                    ])
                    ->get();
                        if(count($is_material_preparation_exists) > 0)
                        {
                            foreach ($is_material_preparation_exists as $key1 => $is_material_preparation_exist) 
                            {
                                $total_roll = $is_material_preparation_exist->detailMaterialPreparationFabric()
                                ->where('is_closing',false)
                                ->count();
                                if($is_material_preparation_exist->is_from_additional == false){

                                    if($total_roll == 0)
                                    {
                                        $is_material_preparation_exist->delete();
                                    }else
                                    {
                                        $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exist->total_reserved_qty);
                                        $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exist->total_qty_rilex);
                
                                        if($_total_booking_preparation != $total_reserved_qty)
                                        {
                                            $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                                            
                                            if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                                            else $remark_planning = 'pengurangan qty alokasi';
                
                                            $remark_planning .= '. Perubahan total qty prepare karena plan yang di cancel . Sebelumnya digunakan untuk po buyer '.$_po_buyer;
                
                                            HistoryPreparationFabric::create([
                                                'material_preparation_fabric_id'    => $is_material_preparation_exist->id,
                                                'remark'                            => $remark_planning,
                                                'qty_before'                        => $total_reserved_qty,
                                                'qty_after'                         => $_total_booking_preparation,
                                                'created_at'                        => $movement_date,
                                                'updated_at'                        => $movement_date,
                                            ]);
                
                                            $is_material_preparation_exist->updated_at             = $movement_date;
                                            $is_material_preparation_exist->total_reserved_qty     = $_total_booking_preparation;
                                            $is_material_preparation_exist->total_qty_outstanding  = $new_qty_oustanding;
                                        }
                                        
                                        $curr_remark_planning = $is_material_preparation_exist->remark_planning;
                                        $is_material_preparation_exist->remark_planning = ( $curr_remark_planning ? $curr_remark_planning .', Cancel Planning Total cancel qty for plan on '.$_planning_date_preparation.' is '.$_total_booking_preparation.'('.$_uom.'). Please cancel this preparation and do preparation again for new plan.' : 'Cancel Planning Total cancel qty for plan on '.$_planning_date_preparation.' is '.$_total_booking_preparation.'('.$_uom.'). Please cancel this preparation and do preparation again for new plan.' );
                                        $is_material_preparation_exist->save();
                                    }

                                }
                            }
                        }

                }

                $delete_material_plannings = MaterialPlanningFabric::whereIn('id',array_unique($planning_cancel_wms))->get();
                foreach ($delete_material_plannings as $key => $delete_material_planning) 
                {
                    HistoryMaterialPlanningFabric::FirstOrCreate([
                        'po_buyer'          => $delete_material_planning->po_buyer,
                        'item_id'           => $delete_material_planning->item_id,
                        'item_code'         => $delete_material_planning->item_code,
                        'color'             => $delete_material_planning->color,
                        'planning_date'     => $delete_material_planning->planning_date,
                        'style'             => $delete_material_planning->style,
                        'article_no'        => $delete_material_planning->article_no,
                        'job_order'         => $delete_material_planning->job_order,
                        'uom'               => $delete_material_planning->uom,
                        'is_piping'         => $delete_material_planning->is_piping,
                        'qty_consumtion'    => $delete_material_planning->qty_consumtion,
                        'warehouse_id'      => $delete_material_planning->warehouse_id,
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'note'              => 'PLANNING DI HAPUS'
                    ]);

                    DetailPoBuyerPerRoll::where('material_planning_fabric_id',$delete_material_planning->id)->delete();
                    
                    $delete_material_planning->delete();
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
        
        $material_planning_fabric_ids   = array();
        $concatenate                    = ''; 

        try 
        {
            DB::beginTransaction();
            
            foreach ($curr_planning_po_buyers as $key_1 => $curr_planning_po_buyer) 
            {
                $planning_date                  = $curr_planning_po_buyer->planning_date;
                $po_buyer                       = $curr_planning_po_buyer->po_buyer;
                $item_id                        = $curr_planning_po_buyer->item_id;
                $item_code                      = $curr_planning_po_buyer->item_code;
                $style                          = $curr_planning_po_buyer->style;
                $color                          = $curr_planning_po_buyer->color;
                $is_piping                      = $curr_planning_po_buyer->is_piping;
                $article_no                     = $curr_planning_po_buyer->article_no;
                $job_order                      = $curr_planning_po_buyer->job_order;
                $uom                            = $curr_planning_po_buyer->uom;
                $qty_consumtion                 = sprintf("%.8f",$curr_planning_po_buyer->qty_consumtion);
                $warehouse_id                   = $curr_planning_po_buyer->warehouse_id;

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $is_planning_exists = MaterialPlanningFabric::where([
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['item_id',$item_id],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_piping',$is_piping],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('deleted_at')
                ->first();

                if(!$is_planning_exists)
                {
                    $new_material_planning_fabric = MaterialPlanningFabric::FirstOrCreate([
                        'po_buyer'          => $po_buyer,
                        'item_id'           => $item_id,
                        'item_code'         => $item_code,
                        'color'             => $color,
                        'planning_date'     => $planning_date,
                        'style'             => $style,
                        'article_no'        => $article_no,
                        'job_order'         => $job_order,
                        'uom'               => $uom,
                        'is_piping'         => $is_piping,
                        'qty_consumtion'    => $qty_consumtion,
                        'warehouse_id'      => $warehouse_id,
                        'user_id'           => $system->id,
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date
                    ]);
                    
                    $concatenate                    .= "'".$po_buyer."',";
                    $material_planning_fabric_ids [] = $new_material_planning_fabric->id;
                    DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$new_material_planning_fabric->id)->delete();
                }else
                {
                    $concatenate                        .= "'".$is_planning_exists->po_buyer."',";
                    $old_qty_consumtion                 = $is_planning_exists->qty_consumtion;

                    if($old_qty_consumtion != $qty_consumtion)
                    {
                        HistoryMaterialPlanningFabric::FirstOrCreate([
                            'po_buyer'          => $is_planning_exists->po_buyer,
                            'item_id'           => $is_planning_exists->item_id,
                            'item_code'         => $is_planning_exists->item_code,
                            'color'             => $is_planning_exists->color,
                            'planning_date'     => $is_planning_exists->planning_date,
                            'style'             => $is_planning_exists->style,
                            'article_no'        => $is_planning_exists->article_no,
                            'job_order'         => $is_planning_exists->job_order,
                            'uom'               => $is_planning_exists->uom,
                            'is_piping'         => $is_planning_exists->is_piping,
                            'qty_consumtion'    => $is_planning_exists->qty_consumtion,
                            'warehouse_id'      => $is_planning_exists->warehouse_id,
                            'created_at'        => $movement_date,
                            'updated_at'        => $movement_date,
                            'note'              => 'QTY CONSUMTION BERUBAH MENJADI '.$qty_consumtion
                        ]);

                        $is_planning_exists->updated_at     = $movement_date;
                        $is_planning_exists->qty_consumtion = $qty_consumtion;
                        $is_planning_exists->save();
                        
                        DetailMaterialPlanningFabricPerPart::where('material_planning_fabric_id',$is_planning_exists->id)->delete();
                    }

                    $material_planning_fabric_ids [] = $is_planning_exists->id;
                }
            }

            foreach ($cutting_instruction_per_parts as $key => $cutting_instruction_per_part) 
            {
                $po_buyer           = $cutting_instruction_per_part->po_buyer;
                $planning_date      = $cutting_instruction_per_part->datestartschedule;
                $_warehouse_id      = $cutting_instruction_per_part->m_warehouse_id;
                $item_id            = $cutting_instruction_per_part->item_id;
                $style              = $cutting_instruction_per_part->style;
                $article_no         = $cutting_instruction_per_part->article_no;
                $uom                = $cutting_instruction_per_part->uom;
                $is_piping          = $cutting_instruction_per_part->is_piping;
                $part_no            = $cutting_instruction_per_part->part_no;
                $qty_consumtion     = sprintf('%0.8f',$cutting_instruction_per_part->qty_consumtion);

                if($_warehouse_id == '1000005') $warehouse_id = '1000001';
                else if($_warehouse_id == '1000012') $warehouse_id = '1000011';

                $is_planning_exists = MaterialPlanningFabric::where([
                    ['planning_date',$planning_date],
                    ['item_id',$item_id],
                    ['po_buyer',$po_buyer],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_piping',$is_piping],
                    ['warehouse_id',$warehouse_id],
                ])
                ->whereNull('deleted_at')
                ->first();

                if($is_planning_exists)
                {
                    $is_planning_per_pert_exists = DetailMaterialPlanningFabricPerPart::where([
                        ['material_planning_fabric_id',$is_planning_exists->id],
                        ['part_no',$part_no],
                        ['qty_per_part',$qty_consumtion],
                    ])
                    ->exists();

                    if(!$is_planning_per_pert_exists)
                    {
                        DetailMaterialPlanningFabricPerPart::FirstOrCreate([
                            'material_planning_fabric_id'   => $is_planning_exists->id,
                            'part_no'                       => $part_no,
                            'qty_per_part'                  => $qty_consumtion,
                            'created_at'                    => $movement_date,
                            'updated_at'                    => $movement_date
                        ]);
                    }
                }
            
            }
            
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        try 
        {
            DB::beginTransaction();

            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_planning_fabric(array[".$concatenate ."]);" ));
            }
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
    }

    static function getCuttingInstruction($start_date,$end_date,$movement_date)
    {
        $material_planning_fabrics = MaterialPlanningFabric::select(db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'warehouse_id'
            ,'uom'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            CuttingInstruction::whereBetween('planning_date',[$start_date,$end_date])->delete();

            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.8f",$material_planning_fabric->qty_need);
                $uom            = $material_planning_fabric->uom;
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function getAllocationFabricSchedule($start_date,$end_date,$movement_date)
    {
        $material_planning_fabrics = MaterialPlanningFabric::whereBetween('planning_date',[$start_date,$end_date])
        ->whereNull('deleted_at')
        ->orderby('is_piping','desc')
        ->orderby('po_buyer','asc')
        ->get();
        
        $concatenate    = '';
        $check_details   = array();
        try 
        {
            DB::beginTransaction();

            foreach ($material_planning_fabrics as $key => $material_planning_fabric)  $check_details [] = $material_planning_fabric->id;
            
            $cheks = db::table('planning_vs_detail_planning_fabric_v')
            ->whereIn('id',$check_details)
            //->where(db::raw("total_booking::numeric != qty_consumtion::numeric"))
            ->whereRaw("total_booking::numeric != qty_consumtion::numeric")
            ->get();

            foreach ($cheks as $key => $value) 
            {
                if($value->total_booking > $value->qty_consumtion)  DetailMaterialPlanningFabric::where('material_planning_fabric_id',$value->id)->delete();
            }

            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_allocated                  = sprintf("%.8f",$material_planning_fabric->getTotalAllocation($material_planning_fabric->id));
                $qty_consumtion                 = sprintf("%.8f",$material_planning_fabric->qty_consumtion);
                $qty_outstanding                = sprintf("%.8f",$qty_consumtion - $qty_allocated);
                $po_buyer                       = $material_planning_fabric->po_buyer;
                $item_id                        = $material_planning_fabric->item_id;
                $item_code                      = $material_planning_fabric->item_code;
                $style                          = $material_planning_fabric->style;
                $article_no                     = $material_planning_fabric->article_no;
                $warehouse_id                   = $material_planning_fabric->warehouse_id;
                $planning_date                  = $material_planning_fabric->planning_date;
                $is_piping                      = $material_planning_fabric->is_piping;
                $material_planning_fabric_id    = $material_planning_fabric->id;
                

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $allocation_items = AllocationItem::whereNull('deleted_at')
                ->where([
                    ['po_buyer',$po_buyer],
                    ['item_id_book',$item_id],
                    ['style',$style],
                    ['article_no',$article_no],
                    ['is_additional',false],
                ])
                ->orderby('c_order_id','asc')
                ->orderby('qty_booking','asc')
                ->get();

                foreach ($allocation_items as $key_2 => $allocation_item)
                {
                    $allocation_item_id     = $allocation_item->id;
                    $document_no            = $allocation_item->document_no;
                    $c_bpartner_id          = $allocation_item->c_bpartner_id;
                    $supplier_name          = $allocation_item->supplier_name;
                    $uom                    = $allocation_item->uom;
                    $item_code_source       = $allocation_item->item_code_source;
                    $item_id_source         = $allocation_item->item_id_source;
                    $item_id_book           = $allocation_item->item_id_book;
                    $c_order_id             = $allocation_item->c_order_id;
                    $_style                 = $allocation_item->_style;
                    $material_stock_per_lot = MaterialStockPerLot::find($allocation_item->material_stock_per_lot_id);
                    $actual_lot             = ($material_stock_per_lot ? $material_stock_per_lot->actual_lot : null);

                    if($qty_outstanding > 0.0000)
                    {
                        $qty_allocation_allocated   = sprintf("%.8f",$allocation_item->getTotalAllocation($allocation_item->id));
                        $qty_booking                = sprintf("%.8f",$allocation_item->qty_booking);
                        $qty_allocation_outstanding = sprintf("%.8f",$qty_booking - $qty_allocation_allocated);

                        if($qty_allocation_outstanding >0.0000)
                        {
                            if ($qty_outstanding/$qty_allocation_outstanding >= 1) $supplied = $qty_allocation_outstanding;
                            else $supplied = $qty_outstanding;

                            if($supplied >0.0000)
                            {
                                DetailMaterialPlanningFabric::FirstOrCreate([
                                    'allocation_item_id'            => $allocation_item_id,
                                    'material_planning_fabric_id'   => $material_planning_fabric_id,
                                    'planning_date'                 => $planning_date,
                                    'planning_date'                 => $planning_date,
                                    'po_buyer'                      => $po_buyer,
                                    'item_code'                     => $item_code,
                                    'item_id_source'                => $item_id_source,
                                    'item_id_book'                  => $item_id_book,
                                    'item_code_source'              => $item_code_source,
                                    'qty_booking'                   => sprintf("%.8f",$supplied),
                                    '_style'                        => $_style,
                                    'style'                         => $style,
                                    'article_no'                    => $article_no,
                                    'is_from_additional'            => false,
                                    'is_piping'                     => $is_piping,
                                    'warehouse_id'                  => $warehouse_id,
                                    'document_no'                   => $document_no,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'c_order_id'                    => $c_order_id,
                                    'supplier_name'                 => $supplier_name,
                                    'uom'                           => $uom,
                                    'user_id'                       => $system->id,
                                    'created_at'                    => $movement_date,
                                    'updated_at'                    => $movement_date,
                                    'actual_lot'                    => $actual_lot,
                                ]);
                                $concatenate .= "'".$po_buyer."',";
                                $qty_outstanding -= $supplied;
                            } 
                        }
                        
                    }
                }

            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();

            if($concatenate != '')
            {
                $concatenate = substr_replace($concatenate, '', -1);
                DB::select(db::raw("SELECT *  FROM delete_duplicate_detail_planning_fabric(array[".$concatenate ."]);" ));
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function recalculateAutoAllocationFabric($start_date,$end_date,$movement_date)
    {
        $auto_allocations   = AutoAllocation::whereIn('po_buyer',function($query) use($start_date,$end_date)
        {
            $query->select('po_buyer')
            ->from('material_planning_fabrics')
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->groupby('po_buyer');
        })
        ->whereNull('deleted_at')
        ->where([
            ['status_po_buyer','active'],
            ['category','FB'],
            ['is_fabric',true],
        ])
        ->get();


        foreach ($auto_allocations as $key => $auto_allocation) 
        {
            $auto_allocation_id = $auto_allocation->id;
            $warehouse_id       = $auto_allocation->warehouse_id;

            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            AllocationFabric::doRecalculate($auto_allocation_id);
            $data = AutoAllocation::find($auto_allocation_id);;

            if($data->qty_allocation != $data->qty_allocated && $data->qty_allocated != 0)
            {
                AllocationFabric::doCancellation($auto_allocation_id,'CANCEL KARNA QTY ALLOCATED TIDAK SAMA DENGAN QTY ALLOCATION, MENGGUNAKAN SCHEDULER',$system->id);
                AllocationFabric::updateAllocationFabricItems([$auto_allocation_id],$auto_allocation_id);

            }
        }
    }

    static function insertMaterialPreparationFabricSchedule($start_date,$end_date,$movement_date)
    {
        try 
        {
            DB::beginTransaction();

            $allocations = DetailMaterialPlanningFabric::select('document_no'
                ,'c_order_id'    
                ,'item_code'
                ,'item_id_book'
                ,'item_id_source'
                ,'item_code_source'
                ,'_style'
                ,'article_no'
                ,'warehouse_id'
                ,'planning_date'
                ,'c_bpartner_id'
                ,'is_piping'
                ,db::raw("string_agg((po_buyer||' ('||qty_booking ||')') ,', ') as po_buyer")
                ,db::raw("sum(qty_booking) as qty_prepare"))
            ->whereIn('material_planning_fabric_id',function($query) use($start_date,$end_date) 
            {
                $query->select('id')
                ->from('material_planning_fabrics')
                ->whereBetween('planning_date',[$start_date,$end_date])
                ->whereNull('deleted_at');
            })
            ->groupby('document_no'
                ,'item_code'
                ,'item_id_book'
                ,'c_order_id'
                ,'_style'
                ,'article_no'
                ,'warehouse_id'
                ,'item_code_source'
                ,'item_id_source'
                ,'planning_date'
                ,'c_bpartner_id'
                ,'is_piping')
            ->get();

            foreach ($allocations as $key => $allocation) 
            {
                
                $_c_order_id_preparation        = $allocation->c_order_id;
                $_document_no_preparation       = $allocation->document_no;
                $_article_no_preparation        = $allocation->article_no;
                $_warehouse_id_preparation      = $allocation->warehouse_id;
                $_item_code_preparation         = $allocation->item_code;
                $_item_id_book_preparation      = $allocation->item_id_book;
                $_item_code_source_preparation  = $allocation->item_code_source;
                $_item_id_source_preparation    = $allocation->item_id_source;
                $_c_bpartner_id_preparation     = $allocation->c_bpartner_id;
                $_planning_date_preparation     = $allocation->planning_date;
                $_style_preparation             = $allocation->_style;
                $_is_piping_preparation         = $allocation->is_piping;
                $_uom                           = $allocation->uom;
                $_po_buyer                      = $allocation->po_buyer;
                $_total_booking_preparation     = sprintf('%0.8f',$allocation->qty_prepare);

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$_warehouse_id_preparation]
                ])
                ->first();

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$_c_order_id_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_id_book',$_item_id_book_preparation],
                    ['item_id_source',$_item_id_source_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                    ['_style',$_style_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax    = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                        
                        if($_total_booking_preparation > $total_reserved_qty) $remark_planning = 'penambahan qty alokasi';
                        else $remark_planning = 'pengurangan qty alokasi';

                        $remark_planning .= '. Digunakan untuk po buyer '.$_po_buyer;
                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning,
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->updated_at             = $movement_date;
                        $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                    }else
                    {
                        if($_total_booking_preparation == $total_reserved_qty)
                        {
                            $is_material_preparation_exists->remark_planning = null;
                        }
                    }
                    
                    $is_material_preparation_exists->created_at = $_planning_date_preparation;
                    $is_material_preparation_exists->updated_at = $_planning_date_preparation;
                    $is_material_preparation_exists->save();

                    $material_preparation_fabric_id = $is_material_preparation_exists->id;
                    DB::select(db::raw("SELECT * FROM delete_duplicate_summary_preparation_fabric('".$material_preparation_fabric_id."');"));
                }else
                {
                    $supplier       = Supplier::where('c_bpartner_id',$_c_bpartner_id_preparation)->first();
                    if($_document_no_preparation == 'FREE STOCK' || $_document_no_preparation == 'FREE STOCK-CELUP' )
                    {
                        $c_order_id = 'FREE STOCK';
                    }else
                    {
                        $po_supplier    = DB::connection('erp')
                        ->table('wms_po_supplier')
                        ->where(db::raw('upper(documentno)'),$_document_no_preparation)
                        ->first();
                        
                        $c_order_id     = ($po_supplier ? $po_supplier->c_order_id : 'MASTER TIDAK DITEMUKAN');
                    }
                    

                    $_material_preparation_fabric = MaterialPreparationFabric::firstorcreate([
                        'c_bpartner_id'         => $_c_bpartner_id_preparation,
                        'supplier_name'         => ($supplier ? $supplier->supplier_name : null ),
                        'document_no'           => $_document_no_preparation,
                        'c_order_id'            => $c_order_id,
                        'item_code'             => $_item_code_preparation,
                        'item_code_source'      => $_item_code_source_preparation,
                        'item_id_book'          => $_item_id_book_preparation,
                        'item_id_source'        => $_item_id_source_preparation,
                        'uom'                   => $_uom,
                        'total_reserved_qty'    => sprintf('%0.8f',$_total_booking_preparation),
                        'total_qty_outstanding' => sprintf('%0.8f',$_total_booking_preparation),
                        'planning_date'         => $_planning_date_preparation,
                        'is_piping'             => $_is_piping_preparation,
                        'article_no'            => $_article_no_preparation,
                        '_style'                => $_style_preparation,
                        'is_from_additional'    => false,
                        'preparation_date'      => null,
                        'preparation_by'        => null,
                        'warehouse_id'          => $_warehouse_id_preparation,
                        'user_id'               => $system->id,
                        'created_at'            => $movement_date,
                        'updated_at'            => $movement_date,
                    ]);

                    HistoryPreparationFabric::create([
                        'material_preparation_fabric_id'    => $_material_preparation_fabric->id,
                        'remark'                            => 'Digunakan untuk po buyer '.$_po_buyer,
                        'qty_before'                        => 0,
                        'qty_after'                         => $_material_preparation_fabric->total_reserved_qty,
                        'created_at'                        => $_material_preparation_fabric->created_at,
                        'updated_at'                        => $_material_preparation_fabric->created_at,
                    ]);

                    $material_preparation_fabric_id = $_material_preparation_fabric->id;
                    DB::select(db::raw("SELECT * FROM delete_duplicate_summary_preparation_fabric('".$material_preparation_fabric_id."');"));
                }
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertCheckMaterialPreparationFabricSchedule($start_date,$end_date,$movement_date)
    {
        $material_preparation_fabrics = MaterialPreparationFabric::wherebetween('planning_date',[$start_date,$end_date])
        ->whereNotNull('planning_date')
        ->get();

        try 
        {
            DB::beginTransaction();

            foreach ($material_preparation_fabrics as $key => $material_preparation_fabric) 
            {
                $material_preparation_fabric_id = $material_preparation_fabric->id;
                $is_piping                      = $material_preparation_fabric->is_piping;
                $c_order_id                     = $material_preparation_fabric->c_order_id;
                $item_id_book                   = $material_preparation_fabric->item_id_book;
                $item_id_source                 = $material_preparation_fabric->item_id_source;
                $_style                         = $material_preparation_fabric->_style;
                $warehouse_id                   = $material_preparation_fabric->warehouse_id;
                $article_no                     = $material_preparation_fabric->article_no;
                $planning_date                  = $material_preparation_fabric->planning_date;
                $total_reserved_qty             = sprintf("%.8f",$material_preparation_fabric->total_reserved_qty);

                $is_allocation_exists           = DetailMaterialPlanningFabric::where([
                    ['warehouse_id',$warehouse_id],
                    ['is_piping',$is_piping],
                    ['article_no',$article_no],
                    ['_style',$_style],
                    ['planning_date',$planning_date],
                    ['c_order_id',$c_order_id],
                    ['item_id_book',$item_id_book],
                    ['item_id_source',$item_id_source],
                ])
                ->exists();

                if(!$is_allocation_exists)
                {
                    $is_detail_exists = DetailMaterialPreparationFabric::where([
                        ['material_preparation_fabric_id',$material_preparation_fabric_id],
                        ['is_closing',false],
                    ])
                    ->exists();

                    $total_roll = $material_preparation_fabric->detailMaterialPreparationFabric()
                    ->where('is_closing',false)
                    ->count();

                    if($material_preparation_fabric->is_from_additional == false){
                        if(!$is_detail_exists && $total_roll == 0)
                        {
                            $material_preparation_fabric->delete();
                        }else
                        {
                            HistoryPreparationFabric::create([
                                'material_preparation_fabric_id'    => $material_preparation_fabric->id,
                                'remark'                            => 'Alokasi untuk plan ini dihapus',
                                'qty_before'                        => $material_preparation_fabric->total_reserved_qty,
                                'qty_after'                         => 0,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                            ]);

                            $material_preparation_fabric->total_reserved_qty    = 0;
                            $material_preparation_fabric->total_qty_outstanding = 0;
                            $material_preparation_fabric->remark_planning       = 'please cancel this data due allocation is not longer exists';
                            $material_preparation_fabric->save();
                        }
                             
                    }
                   
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function sendEmailOutstandingAllocation($start_date,$end_date,$movement_date)
    {
        try
        {
            $cc         = ['aditya.eka@aoi.co.id','charis.azwar@aoi.co.id','indra.setyawan@aoi.co.id','sentot@aoi.co.id','silvester.deprianto@aoi.co.id','fran@triputra-group.com','nicky.puspitasari@aoi.co.id','erika.sunarto@aoi.co.id','septo.prayefta@aoi.co.id','siti.kholirofiah@aoi.co.id','myrta.puspita@aoi.co.id','kinanti.fatmawati@aoi.co.id', 'niam.alfiyan@aoi.co.id', 'zaltika.bellaifa@aoi.co.id'];
            $emails     = ['agung.nugroho@aoi.co.id','intan.martiana@aoi.co.id'];
            
            $data           = DB::select(db::raw("SELECT *  FROM get_outstanding_allocation_per_planning('".$start_date."','".$end_date."') where total_allocation = '0' order by planning_date asc,po_buyer asc,item_code asc;"));
            $data_limit_100 = DB::select(db::raw("SELECT *  FROM get_outstanding_allocation_per_planning('".$start_date."','".$end_date."') where total_allocation = '0' order by planning_date asc,po_buyer asc,item_code asc limit 100;"));
           

            $location = Config::get('storage.auto_allocation');
            if (!File::exists($location)) File::makeDirectory($location, 0777, true);
            
            $send_date = carbon::now()->format('Ymd');
            $file_name = 'outstanding_allocation_fab_'.$send_date;

            $file = Excel::create($file_name,function($excel) use($data)
            {
                $excel->sheet('data_allocation',function($sheet) use($data)
                {
                    $sheet->setCellValue('A1','#');
                    $sheet->setCellValue('B1','PLANNING_CUTTING_DATE');
                    $sheet->setCellValue('C1','PIPING');
                    $sheet->setCellValue('D1','PO_BUYER');
                    $sheet->setCellValue('E1','ITEM_CODE');
                    $sheet->setCellValue('F1','COLOR');
                    $sheet->setCellValue('G1','STYLE');
                    $sheet->setCellValue('H1','ARTICLE_NO');
                    $sheet->setCellValue('I1','WAREHOUSE');
                    $sheet->setCellValue('J1','UOM');
                    $sheet->setCellValue('K1','QTY_CONSUMTION');
                    $sheet->setCellValue('L1','TOTAL_ALOKASI');

                    $sheet->setAutoSize(true);

                   
                    $row=2;
                    foreach ($data as $key => $datum) 
                    {  
                        if($datum->is_piping ) $_piping = 'PIPING';
                        else $_piping = 'BUKAN PIPING';
                        
                        $sheet->setCellValue('A'.$row,($key+1));
                        $sheet->setCellValue('B'.$row,$datum->planning_date);
                        $sheet->setCellValue('C'.$row,$_piping);
                        $sheet->setCellValue('D'.$row,$datum->po_buyer);
                        $sheet->setCellValue('E'.$row,$datum->item_code);
                        $sheet->setCellValue('F'.$row,$datum->color);
                        $sheet->setCellValue('G'.$row,$datum->style);
                        $sheet->setCellValue('H'.$row,$datum->article_no);
                        $sheet->setCellValue('I'.$row,$datum->warehouse);
                        $sheet->setCellValue('J'.$row,$datum->uom);
                        $sheet->setCellValue('K'.$row,$datum->qty_consumtion);
                        $sheet->setCellValue('L'.$row,$datum->total_allocation);
                        $row++;
                    }
                    
                    $sheet->setColumnFormat(array(
                        'C' => '@'
                    ));
                });
            })
            ->save('xlsx', $location);
            
            $file = $location.'/'.$file_name.'.xlsx';
            Mail::send('fabric_material_planning.email', ['total_outstanding' => count($data),'data' => $data_limit_100,'start_date' => $start_date, 'end_date' => $end_date], function ($message) use ($emails,$cc,$file,$file_name)
            {
                $message->subject('OUTSTANDING ALLOCATION PER PLANNING');
                $message->from('no-reply@wms.aoi.co.id', 'WMS');
                $message->cc($cc);
                $message->to($emails);
                $message->attach($file, array('as' => $file_name.'.xlsx', 'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'));
            });
            
        }catch (Exception $e)
        {
            return response (['status' => false,'errors' => $e->getMessage()]);
        }
    }
}
