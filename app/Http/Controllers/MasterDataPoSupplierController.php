<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use stdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Scheduler;
use App\Models\PoSupplier;
use App\Models\ItemPurchase;
use App\Models\UomConversion;
use App\Models\MappingStocks;
use App\Models\PurchaseItem;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\ReroutePoBuyer;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;
use App\Models\SummaryStockFabric;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\HistoryMaterialStocks;
use App\Models\HistoryAutoAllocations;
use App\Models\SummaryHandoverMaterial;
use App\Models\MaterialReadyPreparation;
use App\Models\MonitoringReceivingFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPlanningFabric;


use App\Http\Controllers\ERPController;

class MasterDataPoSupplierController extends Controller
{
    public function index()
    {
       return view('master_data_po_supplier.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $master_suppliers     = DB::table('master_po_supplier_v')
            ->orderby('periode_po','desc');
            
            return DataTables::of($master_suppliers)
            ->addColumn('action',function($master_suppliers){
                return view('master_data_po_supplier._action', [
                    'model'     => $master_suppliers,
                    'item'      => route('masterDataPoSupplier.dataItem',$master_suppliers->c_order_id)
                ]);
            })
            ->rawColumns(['is_selected','action'])
            ->make(true);
        }
    }

    public function show(Request $request)
    {
        $q = strtoupper(trim($request->q));
        $po_suppliers =  PoSupplier::select('document_no')
        ->where([
            ['supplier_id',$request->id],
            ['document_no','LIKE',"%$q%"],
        ])
        ->whereIn('document_no',function($query){
            $query->select('document_no')
            ->from('item_purchases')
            ->whereIn('item_code',function($query2){
                
                $warehouse = auth::user()->warehouse;
                if($warehouse == '1000011' || $warehouse == '1000001'){
                    $query2->select('item_code')
                    ->from('items')
                    ->where('category','FB')
                    ->groupby('item_code');
                }else{
                    $query2->select('item_code')
                    ->from('items')
                    ->where('category','!=','FB')
                    ->groupby('item_code');
                }
            })
            ->groupby('document_no');
        })
        ->orderBy('integration_date','desc')
        ->paginate(25);
       
        return view('supplier._detail_supplier',compact('po_suppliers'));
    }

    public function showItem(Request $request)
    {
        $items = ItemPurchase::select('item_purchases.item_code','items.item_desc','items.category','items.uom')
        ->where('document_no',$request->document_no)
        ->leftJoin('items','item_purchases.item_code','items.item_code')
        ->paginate(25);

        return view('supplier._item',compact('items'));
    }

    public function erpPoSupplierPickList(Request $request)
    {
        $q      = strtoupper(trim($request->q));
        $lists  = DB::connection('erp')
        ->table('wms_po_supplier')
        ->where(db::raw('upper(documentno)'),'like',"%$q%")
        ->paginate(10);
        
        return view('master_data_po_supplier._erp_po_supplier_list',compact('lists'));
    }

    public function store(Request $request)
    {  
       $c_order_id      = $request->c_order_id;
       $c_bpartner_id   = $request->c_bpartner_id;
       $sync_date       = carbon::now();

       try 
       {
            DB::beginTransaction();

            $supplier       = $this->storeSupplier($c_bpartner_id);
            $po_supplier    = $this->storePoSupplier($c_order_id,$c_bpartner_id,$supplier,$sync_date);
            
            $this->storeItemPurhase($c_order_id,$po_supplier);
            //$this->storeAllocationItemPurhase($c_order_id,$c_bpartner_id); // jangan di uncomment

            DB::commit();

        
       } catch (Exception $e) 
       {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
       }

       return response()->json(200);
    }

    static function storeSupplier($c_bpartner_id)
    {
        $supplier_erp    = DB::connection('erp')
        ->table('c_bpartner')
        ->where('c_bpartner_id',$c_bpartner_id)
        ->first();

        $_type      = $supplier_erp->c_bp_group_id;
        if($_type == '1000003') $supplier_type = 'LOCAL';
        else $supplier_type = 'IMPORT';

        $is_supplier_exits = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
        
        if($is_supplier_exits)
        {
            $is_supplier_exits->supplier_code = trim(strtoupper($supplier_erp->value));
            $is_supplier_exits->supplier_name = trim(strtoupper($supplier_erp->name));
            $is_supplier_exits->type_supplier = $supplier_type;
            $is_supplier_exits->save();

            $supplier_id = $is_supplier_exits->id;
        }else
        {
            $supplier = Supplier::FirstOrCreate([
                'supplier_code' => trim(strtoupper($supplier_erp->value)),
                'supplier_name' => trim(strtoupper($supplier_erp->name)),
                'c_bpartner_id' => trim(strtoupper($supplier_erp->c_bpartner_id)),
                'type_supplier' => $supplier_type
            ]);

            $supplier_id = $supplier->id;
        }

        return $supplier_id;
    }

    static function storePoSupplier($c_order_id,$c_bpartner_id,$supplier,$sync_date)
    {
        $po_supplier_erp = DB::connection('erp')
        ->table('wms_po_supplier')
        ->where('c_order_id',$c_order_id)
        ->first();

        $document_no = trim(strtoupper($po_supplier_erp->documentno));
        if($document_no == 'FREE STOCK')
        {
                $c_bpartner_id          = 'FREE STOCK';
                $supplier_code          = 'FREE STOCK';
                $_mapping_stock         = MappingStocks::where('document_number',$document_no)->first();
                $type_stock_erp_code    = '2';
                $type_stock             = 'REGULER';
        }else
        {
                $get_4_digit_from_beginning = substr($document_no,0, 4);
                $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                if($_mapping_stock_4_digt)
                {
                    $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                    $type_stock             = $_mapping_stock_4_digt->type_stock;
                }else
                {
                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                    $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                    if($_mapping_stock_5_digt)
                    {
                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                    }else
                    {
                        $type_stock_erp_code    = null;
                        $type_stock             = null;
                    }
                    
                }
        } 

        $is_po_supplier_exists  = PoSupplier::where('c_order_id',$c_order_id)->first();

        if($is_po_supplier_exists)
        {
            $is_po_supplier_exists->supplier_id         = $supplier;
            $is_po_supplier_exists->c_order_id          = $c_order_id;
            $is_po_supplier_exists->c_bpartner_id       = $c_bpartner_id;
            $is_po_supplier_exists->type_stock_erp_code = $type_stock_erp_code;
            $is_po_supplier_exists->type_stock          = $type_stock;
            $is_po_supplier_exists->document_no         = $document_no;
            $is_po_supplier_exists->periode_po          = $po_supplier_erp->period_po;
            $is_po_supplier_exists->save();

            $po_supplier_id = $is_po_supplier_exists->id;
        }else
        {
            $po_supplier = PoSupplier::create([
                'c_order_id'            => $c_order_id,
                'c_bpartner_id'         => $c_bpartner_id,
                'supplier_id'           => $supplier,
                'integration_date'      => $sync_date,
                'type_stock_erp_code'   => $type_stock_erp_code,
                'type_stock'            => $type_stock,
                'document_no'           => $document_no,
                'periode_po'            => $po_supplier_erp->period_po
            ]);

            $po_supplier_id = $po_supplier->id;
        }

        MaterialArrival::where('c_order_id',$c_order_id)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MaterialStock::where('c_order_id',$c_order_id)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        SummaryStockFabric::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MonitoringReceivingFabric::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MaterialReadyPreparation::where('c_order_id',$c_order_id)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MaterialPreparation::where('c_order_id',$c_order_id)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MaterialPreparationFabric::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        MaterialCheck::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        AllocationItem::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'c_order_id'    => $po_supplier_erp->c_order_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);
        
        DetailMaterialPlanningFabric::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'c_order_id'    => $po_supplier_erp->c_order_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        SummaryHandoverMaterial::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        AutoAllocation::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        HistoryMaterialStocks::where('document_no',$document_no)
        ->update([
            'c_bpartner_id' => $po_supplier_erp->c_bpartner_id,
            'supplier_name' => $po_supplier_erp->supplier_name,
        ]);

        

        return $po_supplier_id;
    }

    static function storeItemPurhase($c_order_id,$po_supplier)
    {
        $item_purchase_erp = DB::connection('erp')
        ->table('wms_po_per_item')
        ->where('c_order_id',$c_order_id)
        ->get();

        foreach ($item_purchase_erp as $key => $value) 
        {
            $document_no_purchase   = trim(strtoupper($value->documentno));
            $item_code_purchase     = trim(strtoupper($value->item));
            $category_purchase      = trim(strtoupper($value->category));

            $is_purchase_item_exists = ItemPurchase::where([
                ['document_no',$document_no_purchase],
                ['item_code',$item_code_purchase],
            ])
            ->first();

            if($is_purchase_item_exists)
            {
                $is_purchase_item_exists->po_supplier_id    = $po_supplier;
                $is_purchase_item_exists->document_no       = $document_no_purchase;
                $is_purchase_item_exists->item_code         = $item_code_purchase;
                $is_purchase_item_exists->category          = $category_purchase;
                $is_purchase_item_exists->save();
            }else
            {
                ItemPurchase::FirstorCreate([
                    'po_supplier_id'    => $po_supplier,
                    'document_no'       => $document_no_purchase,
                    'item_code'         => $item_code_purchase,
                    'category'          => $category_purchase,
                ]);
            }
        }
    }

    static function storeAllocationItemPurhase($c_order_id,$c_bpartner_id)
    {
        $purchase_fabrics       = [];
        $movement_date          = carbon::now()->todatetimestring();
        $allocation_purchases   = DB::connection('erp') //dev_erp //erp
        ->table('rma_wms_planning_v2')
        ->select('document_no',
            'item_id',
            'item_desc',
            'c_order_id',
            'c_bpartner_id',
            'supplier_name',
            'po_buyer',
            'm_warehouse_id as warehouse',
            'item_code',
            'uom_pr as uom',
            'uom_po as uom_po',
            'category',
            db::raw('sum(qty_pr) as qty'),
            db::raw('sum(qtyordered) as qty_order'))
        ->where([
            ['c_order_id',$c_order_id],
            ['c_bpartner_id',$c_bpartner_id],
            ['po_buyer','!=',''],
        ])
        ->where('document_no','NOT LIKE',"%^%")
        ->whereNotNull('po_buyer')
        ->orderBy('po_buyer','desc')
        ->groupby('document_no',
            'item_id',
            'item_desc',
            'c_order_id',
            'c_bpartner_id',
            'supplier_name',
            'po_buyer',
            'm_warehouse_id',
            'item_code',
            'uom_pr',
            'uom_po',
            'category')
        ->get();

        foreach ($allocation_purchases as $key => $allocation_purchase) 
        {
            $document_no    = strtoupper($allocation_purchase->document_no);
            $supplier_name  = strtoupper($allocation_purchase->supplier_name);
            $po_buyer       = $allocation_purchase->po_buyer;
            $item_code      = strtoupper($allocation_purchase->item_code);
            $c_order_id     = $allocation_purchase->c_order_id;
            $item_id        = $allocation_purchase->item_id;
            $item_desc      = $allocation_purchase->item_desc;
            $warehouse      = $allocation_purchase->warehouse;
            $qty_allocation = sprintf('%0.8f',$allocation_purchase->qty);
            $qty_order      = sprintf('%0.8f',$allocation_purchase->qty_order);
            $uom            = $allocation_purchase->uom;
            $uom_po         = $allocation_purchase->uom_po;
            $category       = $allocation_purchase->category;
            $so_id          = PoBuyer::where('po_buyer', $po_buyer)->first();
           
            $system                         = User::where([
                ['name','system'],
                ['warehouse',$warehouse]
            ])
            ->first();

            $reroute            = ReroutePoBuyer::where('old_po_buyer',$po_buyer)->first();
            if($reroute)
            {
                $new_po_buyer = $reroute->new_po_buyer;
                $old_po_buyer = $reroute->old_po_buyer;
            }else
            {
                $new_po_buyer = $po_buyer;
                $old_po_buyer = $po_buyer;
            }

            $__po_buyer             = PoBuyer::where(db::raw('po_buyer'),$new_po_buyer)->first();
            $lc_date                = ($__po_buyer)? $__po_buyer->lc_date : null;
            $season                 = ($__po_buyer)? $__po_buyer->season : null;
            $type_stock_erp_code    = ($__po_buyer)? $__po_buyer->type_stock_erp_code : null;
            $cancel_date            = ($__po_buyer)? $__po_buyer->cancel_date : null;

            if($__po_buyer)
            {
                if($__po_buyer->statistical_date) $promise_date = ($__po_buyer)? $__po_buyer->statistical_date : null;
                else $promise_date = ($__po_buyer)? $__po_buyer->promise_date : null;
            }else
            {
                $promise_date = null;
            }

            if($cancel_date != null) $status_po_buyer = 'cancel';
            else $status_po_buyer = 'active';

            if($type_stock_erp_code == 1) $type_stock = 'SLT';
            else if($type_stock_erp_code == 2) $type_stock = 'REGULER';
            else if($type_stock_erp_code == 3) $type_stock = 'PR/SR';
            else if($type_stock_erp_code == 4) $type_stock = 'MTFC';
            else if($type_stock_erp_code == 5) $type_stock = 'NB';
            else $type_stock = null;

            if($category == 'FB' && $lc_date >= '2019-08-15')// cut off mulai lc tanggal ini, user baru setuju mau maintain
            {   
                if($warehouse == '1000011') $warehouse_name = 'FABRIC AOI 2';
                else if($warehouse == '1000001') $warehouse_name = 'FABRIC AOI 1';

                $is_exists = AutoAllocation::where([
                    [db::raw('upper(document_no)'),$document_no],
                    ['c_bpartner_id',$c_bpartner_id],
                    ['po_buyer',$new_po_buyer],
                    [db::raw('upper(item_code)'),$item_code],
                    ['warehouse_id',$warehouse],
                    ['qty_allocation',$qty_allocation],
                    ['is_fabric',true],
                ])
                ->exists();

                if(!$is_exists)
                {
                    AutoAllocation::firstorcreate([
                        'type_stock_erp_code'               => $type_stock_erp_code,
                        'type_stock'                        => $type_stock,
                        'lc_date'                           => $lc_date,
                        'promise_date'                      => $promise_date,
                        'season'                            => $season,
                        'document_no'                       => $document_no,
                        'c_bpartner_id'                     => $c_bpartner_id,
                        'supplier_name'                     => $supplier_name,
                        'po_buyer'                          => $new_po_buyer,
                        'old_po_buyer'                      => $old_po_buyer,
                        'c_order_id'                        => $c_order_id,
                        'item_id_source'                    => $item_id,
                        'item_id_book'                      => $item_id,
                        'item_code_source'                  => $item_code,
                        'item_code_book'                    => $item_code,
                        'item_code'                         => $item_code,
                        'item_desc'                         => $item_desc,
                        'category'                          => $category,
                        'uom'                               => $uom,
                        'warehouse_name'                    => $warehouse_name,
                        'warehouse_id'                      => $warehouse,
                        'qty_allocation'                    => $qty_allocation,
                        'qty_outstanding'                   => $qty_allocation,
                        'qty_allocated'                     => 0,
                        'status_po_buyer'                   => $status_po_buyer,
                        'is_fabric'                         => true,
                        'is_already_generate_form_booking'  => false,
                        'is_upload_manual'                  => false,
                        'is_allocation_purchase'            => true,
                        'created_at'                        => $movement_date,
                        'updated_at'                        => $movement_date,
                        'user_id'                           => $system->id,
                        'so_id'                             => $so_id->so_id
                    ]);

                    $purchase_fabrics [] = $document_no;
                }
            }else
            {
                if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != ''))
                {
                    if($warehouse == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                    else if($warehouse == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

                    $is_exists = AutoAllocation::where([
                        [db::raw('upper(document_no)'),$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['po_buyer',$new_po_buyer],
                        [db::raw('upper(item_code)'),$item_code],
                        ['warehouse_id',$warehouse],
                        ['qty_allocation',$qty_allocation],
                        ['is_fabric',false],
                    ])
                    ->exists();

                    if(!$is_exists)
                    {
                        $tgl_date                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($__po_buyer->lc_date ? $__po_buyer->lc_date->format('my')  : 'LC NOT FOUND');
    
                        if($warehouse == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse == '1000013') $warehouse_sequnce = '002';
    
                        $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;

                        AutoAllocation::firstorcreate([
                            'document_allocation_number'        => $document_allocation_number,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'type_stock'                        => $type_stock,
                            'lc_date'                           => $lc_date,
                            'promise_date'                      => $promise_date,
                            'season'                            => $season,
                            'c_order_id'                        => $c_order_id,
                            'document_no'                       => $document_no,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $new_po_buyer,
                            'old_po_buyer'                      => $old_po_buyer,
                            'item_code_source'                  => $item_code,
                            'item_code_book'                    => $item_code,
                            'item_id_book'                      => $item_id,
                            'item_id_source'                    => $item_id,
                            'item_code'                         => $item_code,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse,
                            'qty_allocation'                    => $qty_allocation,
                            'qty_outstanding'                   => $qty_allocation,
                            'qty_allocated'                     => 0,
                            'status_po_buyer'                   => $status_po_buyer,
                            'is_fabric'                         => false,
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => false,
                            'is_allocation_purchase'            => true,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                            'user_id'                           => $system->id,
                            'so_id'                             => $so_id->so_id
                        ]);
                    }
                }
            }
        }

        $auto_allocations = AutoAllocation::where([
            ['is_already_generate_form_booking',false],
            ['is_fabric',true],
            ['is_allocation_purchase',true],
        ])
        ->whereIn('document_no',$purchase_fabrics)
        ->whereNull('deleted_at')
        ->orderby('promise_date','asc')
        ->orderby('qty_outstanding','asc')
        ->get();

        foreach ($auto_allocations as $key_2 => $auto_allocation) 
        {
            $auto_allocation_id             = $auto_allocation->id;
            $document_no                    = $auto_allocation->document_no;
            $c_bpartner_id                  = $auto_allocation->c_bpartner_id;
            $po_buyer                       = $auto_allocation->po_buyer;
            $item_code                      = $auto_allocation->item_code;
            $qty_outstanding                = sprintf('%0.8f',$auto_allocation->qty_outstanding);
            $qty_allocated                  = sprintf('%0.8f',$auto_allocation->qty_allocated);
            $warehouse_id                   = $auto_allocation->warehouse_id;
            $supplier                       = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
            $supplier_name                  = ($supplier)? $supplier->supplier_name : null;
            $supplier_code                  = ($supplier)? $supplier->supplier_code : null;
            
            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();
            
            $material_requirements = MaterialRequirement::where([
                ['po_buyer',$po_buyer],
                [db::raw('upper(item_code)'),$item_code],
            ])
            ->orderby('article_no','asc')
            ->orderby('style','asc')
            ->get();

            if($qty_outstanding > 0)
            {
                $count_mr = count($material_requirements);
                foreach ($material_requirements as $key => $material_requirement) 
                {
                    $lc_date        = $material_requirement->lc_date;
                    $item_desc      = strtoupper($material_requirement->item_desc);
                    $uom            = $material_requirement->uom;
                    $category       = $material_requirement->category;
                    $style          = $material_requirement->style;
                    $job_order      = $material_requirement->job_order;
                    $article_no     = $material_requirement->article_no;
                    
                    /*$month_lc = $material_requirement->lc_date->format('m');
                    if($month_lc <= '02') $qty_required = sprintf("%0.2f",$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                    else $qty_required = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));*/
                    $qty_required = sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code,$style,$article_no));
                    
                    if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                    else $__supplied = sprintf('%0.8f',$qty_outstanding);

                    $_warehouse_erp_id = $material_requirement->warehouse_id;

                    if($_warehouse_erp_id == '1000007' && $warehouse_id == '1000001') $warehouse_production_id = '1000001';
                    else if($_warehouse_erp_id == '1000010' && $warehouse_id == '1000011') $warehouse_production_id = '1000011';
                    else $warehouse_production_id = $warehouse_id;
                    
                    if($qty_required > 0 && $qty_outstanding > 0)
                    {
                        if ($qty_outstanding/$__supplied >= 1) $_supply = $__supplied;
                        else $_supply = $qty_outstanding;

                        /*if($month_lc <= '02') $__supply = sprintf("%0.2f",$_supply);
                        else $__supply = sprintf('%0.8f',$_supply);*/

                        $__supply = sprintf('%0.8f',$_supply);
                    
                        $is_exists = AllocationItem::where([
                            ['auto_allocation_id',$auto_allocation_id],
                            ['item_code',$item_code],
                            ['style',$style],
                            ['po_buyer',$po_buyer],
                            ['qty_booking',$__supply],
                            ['is_additional',false],
                            ['is_from_allocation_fabric',true],
                            ['warehouse_inventory',$warehouse_id] // warehouse inventory
                        ])
                        ->where(function($query){
                            $query->where('confirm_by_warehouse','approved')
                            ->OrWhereNull('confirm_by_warehouse');
                        })
                        ->whereNull('deleted_at')
                        ->exists();
                        
                        if(!$is_exists && $_supply > 0)
                        {
                            $allocation_item = AllocationItem::FirstOrCreate([
                                'auto_allocation_id'        => $auto_allocation_id,
                                'lc_date'                   => $lc_date,
                                'c_bpartner_id'             => $c_bpartner_id,
                                'supplier_code'             => $supplier_code,
                                'supplier_name'             => $supplier_name,
                                'document_no'               => $document_no,
                                'item_code'                 => $item_code,
                                'item_desc'                 => $item_desc,
                                'category'                  => $category,
                                'uom'                       => $uom,
                                'job'                       => $job_order,
                                'style'                     => $style,
                                'article_no'                => $article_no,
                                'po_buyer'                  => $po_buyer,
                                'warehouse'                 => $warehouse_production_id,
                                'warehouse_inventory'       => $warehouse_id,
                                'is_need_to_handover'       => false,
                                'qty_booking'               => $__supply,
                                'is_from_allocation_fabric' => true,
                                'user_id'                   => $system->id,
                                'confirm_by_warehouse'      => 'approved',
                                'confirm_date'              => Carbon::now(),
                                'remark'                    => 'ALLOCATION PURCHASE',
                                'confirm_user_id'           => $system->id,
                                'deleted_at'                => null
                            ]);

                            //echo $po_buyer.' '.$_supplied.' '.$material_stock_id.'<br/>';
                            //if($is_need_handover == true) $insert_summary_handover [] = $allocation_item->id;
                            $qty_required       -= $_supply;
                            $qty_outstanding    -= $_supply;
                            $qty_allocated      += $_supply;
                        }
                    }
                }
            }
            
            //echo $po_buyer.' '.$qty_outstanding.' '.$qty_allocated.'<br/>';
            
            if(intVal($qty_outstanding)<= 0)
            {
                $auto_allocation->is_already_generate_form_booking  = true;
                $auto_allocation->generate_form_booking             = carbon::now();
                $auto_allocation->qty_outstanding                   = 0;
            }else
            {
                $auto_allocation->qty_outstanding                   = sprintf('%0.8f',$qty_outstanding);
            
            }

            $auto_allocation->qty_allocated = sprintf('%0.8f',$qty_allocated);
            $auto_allocation->save();
            
        }
    }
}
