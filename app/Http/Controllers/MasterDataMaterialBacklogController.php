<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Validator;
use stdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\MaterialBacklog;

class MaterialBacklogController extends Controller
{
    public function index(Request $request,Builder $htmlBuilder){
      if ($request->ajax()){
          $material_backlog = MaterialBacklog::orderBy('created_at','asc');

          return DataTables::of($material_backlog)
          ->addColumn('action', function($material_backlog) {
              return view('_action', [
                  'model' => $material_backlog,
                  'delete' => route('materialbacklog.delete',$material_backlog->id),
              ]);
          })
          ->make(true);
      }

      $html = $htmlBuilder
      ->addColumn(['data'=>'po_buyer', 'name'=>'po_buyer', 'title'=>'PO BUYER'])
      ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'ITEM CODE'])
      ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center', 'style'=>'width: 30px;']);

      return view('material_backlog.index')
      ->with(compact('html'));
    }

    public function store(Request $request){
      if(MaterialBacklog::where([
        'item_code'=>strtoupper($request->item_code),
        'po_buyer'=>strtoupper($request->po_buyer)
      ])
      ->exists())
          return response()->json(['message' => 'Data already exists.'
          ], 422);

      $material_backlog = MaterialBacklog::create([
          'item_code'=> strtoupper($request->item_code),
          'po_buyer' => strtoupper($request->po_buyer),
      ]);
      $material_backlog->save();
      return response()->json('success', 200);
    }

    public function destroy($id){
      MaterialBacklog::where('id',$id)->delete();
    }

    public function import(){
      return view('material_backlog.import');
    }

    public function importFromExcel(Request $request){
      $array = array();
      if($request->hasFile('upload_file')){
          $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
          ]);

          $path = $request->file('upload_file')->getRealPath();
          $data = Excel::selectSheets('active')->load($path,function($render){})->get();

          if(!empty($data) && $data->count()){
              $array_material_backlog = array();
              foreach ($data as $key => $value){
                $obj=new stdClass();
                $obj->po_buyer=$value->po_buyer;
                $obj->item_code=$value->item_code;
                if($obj->po_buyer!=null||$obj->item_code!=null){
                    if(MaterialBacklog::where([
                      'po_buyer'=>strtoupper($obj->po_buyer),
                      'item_code'=>strtoupper($obj->item_code),
                    ])->exists()
                    ){
                      $obj->status='Error !,Duplikasi Data !';
                    }else{
                      MaterialBacklog::create([
                        'po_buyer' => strtoupper($value->po_buyer),
                        'item_code' => strtoupper($value->item_code),
                      ]);
                      $obj->status='Berhasil';
                    }
                }else{
                  $obj->status='Data PO BUYER / ITEM CODE Tidak Boleh Kosong !';
                }
                $array_material_backlog[]=$obj;
              }
              return response()->json($array_material_backlog,200);
          }
      }
      return response()->json('import gagal, silahkan cek file anda',422);
    }

    public function exportToExcel(Request $request){
      return Excel::create('upload_material_backlog',function ($excel){
         $excel->sheet('active', function($sheet){
             $sheet->setCellValue('A1','PO_BUYER');
             $sheet->setCellValue('B1','ITEM_CODE');
             $sheet->setWidth('A', 15);
             $sheet->setWidth('B', 15);
             $sheet->setColumnFormat(array(
                 'A' => '@',
                 'B' => '@'
             ));

         });
         $excel->setActiveSheetIndex(0);
     })->export('xlsx');
    }
}
