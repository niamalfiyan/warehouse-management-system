<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;


use App\Models\Locator;
use App\Models\Temporary;
use App\Models\MaterialCheck;
use App\Models\UomConversion;
use App\Models\AllocationItem;
use App\Models\AutoAllocation;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;

class AccessoriesMaterialReverseQualityControlController extends Controller
{
    public function index()
    {
        return view('accessories_material_reverse_quality_control.index');
    }

    public function create(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $barcode            = trim($request->barcode);

        $has_dash   = strpos($barcode, "-");
        if($has_dash == false)
        {
            $_barcode   = strtoupper($barcode);
            $flag       = 1;
        }else
        {
            $split      = explode('-',$barcode);
            $_barcode   = strtoupper($split[0]);
            $flag       = 0;
        }

        $material_preparation = MaterialPreparation::where([
            ['barcode',$barcode],
            ['warehouse',$_warehouse_id]
        ])
        ->first();

        if(!$material_preparation) return response()->json('Barcode not found',422);


        $material_check = MaterialCheck::where('material_preparation_id',$material_preparation->id)
        ->whereNull('deleted_at')
        ->first();

        if(!$material_check) return response()->json('Barcode not find in qc',422);
        if($material_check->status != 'REJECT') return response()->json('This material is not reject',422);

        $material_preparation_id        = $material_preparation->id;
        $barcode                        = $material_preparation->barcode;
        $item_code                      = $material_preparation->item_code;
        $po_buyer                       = $material_preparation->po_buyer;
        $style                          = $material_preparation->style;
        $article_no                     = $material_preparation->article_no;
        $uom                            = $material_preparation->uom_conversion;
        $qty_reject                     = sprintf('%0.8f',$material_check->qty_reject);
        $obj                            = new StdClass();
        $obj->material_preparation_id   = $material_preparation_id;
        $obj->item_code                 = $item_code;
        $obj->po_buyer                  = $po_buyer;
        $obj->style                     = $style;
        $obj->article_no                = $article_no;
        $obj->uom                       = $uom;
        $obj->qty_reject                = $qty_reject;
        $obj->barcode                   = $barcode;
        $obj->movement_date             = Carbon::now()->toDateTimeString();
        $obj->ict_log                   = $material_preparation->ict_log;
       
        return response()->json($obj,200);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $__warehouse_id = $request->warehouse_id;
            $items          = json_decode($request->barcode_products);

            $locator_reject_qc = Locator::whereHas('area',function($query){
                $query->where('name','QC')
                ->where('warehouse',auth::user()->warehouse);
            })
            ->where('rack','REJECT')
            ->first();

            $checking_qc    = Locator::whereHas('area',function($query){
                $query->where('name','QC')
                ->where('warehouse',auth::user()->warehouse);
            })
            ->where('rack','CHECKING')
            ->first();

            try 
            {
                DB::beginTransaction();

                foreach ($items as $key => $item) 
                {
                    $material_preparation_id    = $item->material_preparation_id;
                    $movement_date              = $item->movement_date;
                    $ict_note                   = strtoupper($item->ict_log);
                    
                    $material_preparation       = MaterialPreparation::find($material_preparation_id);
                    $po_buyer                   = $material_preparation->po_buyer;
                    $item_id                    = $material_preparation->item_id;
                    $item_code                  = $material_preparation->item_code;
                    $last_locator_id            = $material_preparation->last_locator_id;
                    $uom_conversion             = $material_preparation->uom_conversion;
                    $material_check             = MaterialCheck::where('material_preparation_id',$material_preparation_id)
                    ->whereNull('deleted_at')
                    ->first();

                    $qty_reject         = sprintf('%0.8f',$material_check->qty_reject);
                    $qty_preparation    = sprintf('%0.8f',$material_check->uom_po);

                    $conversion  = UomConversion::where([
                        ['item_code',$item_code],
                        ['uom_to',$uom_conversion],
                    ])
                    ->first();

                    if($conversion) $multiplyrate = $conversion->multiplyrate;
                    else $multiplyrate = 1;

                    $qty_reconversion = sprintf('%0.8f',$multiplyrate * $qty_preparation);

                    $movement_reverse = MaterialMovement::FirstOrCreate([
                        'from_location'     =>  $locator_reject_qc->id,
                        'to_destination'    => $checking_qc->id,
                        'po_buyer'          => $po_buyer,
                        'is_complete_erp'   => true,
                        'is_active'         => true,
                        'status'            => 'reverse-qc',
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                    ]);

                    MaterialMovementLine::FirstOrCreate([
                        'material_movement_id'      => $movement_reverse->id,
                        'material_preparation_id'   => $material_preparation_id,
                        'item_id'                   => $item_id,
                        'item_code'                 => $item_code,
                        'type_po'                   => 2,
                        'qty_movement'              => $qty_preparation,
                        'date_movement'             => $movement_date,
                        'created_at'                  => $movement_date,
                        'updated_at'                  => $movement_date,
                        'is_active'                 => true,
                        'parent_id'                 => null,
                        'note'                      => 'REVERSE QC ( QTY REJECT SEBELUMNYA SEBESAR '.$qty_reject.'), '.$ict_note,
                        'user_id'                   => Auth::user()->id
                    ]);
                    
                    $material_preparation->qc_status            = 'HOLD';
                    $material_preparation->qty_reconversion     = $qty_reconversion;
                    $material_preparation->qty_conversion       = $qty_preparation;
                    $material_preparation->last_movement_date   = $movement_date;
                    $material_preparation->qty_reject           = '0';
                    $material_preparation->updated_at           = $movement_date;
                    $material_preparation->deleted_at           = null;
                    
                    $material_check->deleted_at                 = $movement_date;

                    $auto_allocation                = AutoAllocation::find($material_preparation->auto_allocation_id);
                    if($auto_allocation)
                    {
                        $qty_reject_auto_allocation     = $auto_allocation->qty_reject;
                        $new_qty_reject_auto_allocation = sprintf('%0.8f',$qty_reject_auto_allocation - $qty_reject);
                        
                        $cur_note                       = 'QTY SEBESAR '.$qty_reject.' TIDAK JADI DI REJECT QC';
                        $qc_note_auto_allocation        = $auto_allocation->qc_note;
                        $new_note                       = $qc_note_auto_allocation.', '.$cur_note;

                        $auto_allocation->qty_reject    = $new_qty_reject_auto_allocation;
                        $auto_allocation->qc_note       = $new_note;
                        $auto_allocation->save();
                    }


                    $material_check->save();
                    $material_preparation->save();

                    Temporary::Create([
                        'barcode'       => $po_buyer,
                        'status'        => 'mrp',
                        'user_id'       => Auth::user()->id,
                        'created_at'    => $movement_date,
                        'updated_at'    => $movement_date,
                    ]);
                    
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
            
            return response()->json(200);
        }else{
            return response()->json('Barcode not found.',422);
        }
    }
}
