<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Yajra\Datatables\Datatables;
use Excel;

class AccessoriesReportMaterialAdjustmentController extends Controller
{
    public function index()
    {
       
        return view('accessories_report_material_adjustment.index');
    }
    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();
            
            $material_adj = db::table('report_material_adjustment')
            ->where([
                ['warehouse',$warehouse_id]
            ])
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderBy('created_at','desc');
            
            
            return DataTables::of($material_adj)
           

            ->editColumn('operator',function ($material_adj)
            {
                return number_format($material_adj->operator, 4, '.', ',');
            })
            ->editColumn('warehouse',function ($material_adj)
            {
                if($material_adj->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($material_adj->warehouse == '1000013') return 'Warehouse accessories AOI 2';
                // else if(/$material_out->warehouse == '1000013') return 'Warehouse ACC AOI 2';
            })
          
            // ->rawColumns(['is_piping','action','style','status'])
            ->make(true);
        }
    }

    public function exportAll(Request $request)
    {
        $warehouse_id       = ($request->warehouse ? $request->warehouse : auth::user()->warehouse);
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        
        $_start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d') : Carbon::today()->subDays(30)->format('Y-m-d');
        $_end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d') : Carbon::now()->format('Y-m-d');
    
        
        $warehouse_name     = ($warehouse_id == '1000002' ?'AOI-1':'AOI-2' );
        
            $material_adj = db::table('report_material_adjustment')
            ->where([
                ['warehouse',$warehouse_id]
            ])
            ->whereBetween('created_at', [$start_date, $end_date])
            ->orderBy('created_at','desc')
            ->get();

            $file_name = 'REPORT_FAB_MATERIAL_ADJUSTMENT'.$warehouse_name.'_FROM_'.$_start_date.'_TO_'.$_end_date;
    
      

        return Excel::create($file_name,function($excel) use ($material_adj)
        {
            $excel->sheet('ACTIVE',function($sheet)use($material_adj)
            {
                $sheet->setCellValue('A1','CREATE_DATE');
                $sheet->setCellValue('B1','WAREHOUSE');
                $sheet->setCellValue('C1','PO_SUPPLIER');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','UOM');
                $sheet->setCellValue('F1','QTY');
                $sheet->setCellValue('G1','NOTE');

            
            $row=2;

           
            foreach ($material_adj as $i) 
            {  


                if($i->warehouse == '1000002') $warehouse_name =  'Warehouse accessories AOI 1';
                elseif($i->warehouse == '1000013') $warehouse_name =  'Warehouse accessories AOI 2';

                $sheet->setCellValue('A'.$row,$i->created_at);
                $sheet->setCellValue('B'.$row,$warehouse_name);
                $sheet->setCellValue('C'.$row,$i->document_no);
                $sheet->setCellValue('D'.$row,$i->item_code);
                $sheet->setCellValue('E'.$row,$i->uom);
                $sheet->setCellValue('F'.$row,$i->operator);
                $sheet->setCellValue('G'.$row,$i->note);
                $row++;
            }
            });

        })
        ->export('xlsx');
    }
}
