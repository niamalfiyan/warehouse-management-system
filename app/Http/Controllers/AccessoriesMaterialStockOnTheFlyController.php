<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use File;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Item;
use App\Models\Supplier;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesMaterialStockOnTheFlyController extends Controller
{
    public function index()
    {
        $flag = Session::get('flag');
        return view('accessories_material_stock_on_the_fly.index',compact('flag','html'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $stofs = db::table('material_stocks_on_the_fly_v')->orderby('created_at','desc');

            return DataTables::of($stofs)
            ->editColumn('created_at',function ($stofs)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $stofs->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse_id',function ($stofs)
            {
                if($stofs->warehouse_id == '1000002') return 'Warehouse accessories AOI 1';
                elseif($stofs->warehouse_id == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->editColumn('stock',function ($stofs)
            {
                return number_format($stofs->stock, 4, '.', ',');
            })
            ->editColumn('stock_arrival',function ($stofs)
            {
                return number_format($stofs->stock_arrival, 4, '.', ',');
            })
            ->editColumn('stock_reserved',function ($stofs)
            {
                return number_format($stofs->stock_reserved, 4, '.', ',');
            })
            ->editColumn('stock_available',function ($stofs)
            {
                return number_format($stofs->stock_available, 4, '.', ',');
            })
            ->addColumn('status_arrival',function($stofs)
            {
                if($stofs->outstanding <= 0) return '<span class="label label-success">closed</span>';
                else return '<span class="label label-default">open</span>';
            })
            ->addColumn('action', function($material_stocks) {
                return view('accessories_material_stock_on_the_fly._action',[
                    'model'                 => $material_stocks,
                    'delete'                => route('accessoriesMaterialStockOnTheFly.delete',$material_stocks->material_stock_id),
                ]);
            })
            ->setRowAttr([
                'style' => function($stofs)
                {
                    if($stofs->outstanding <= 0) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['status_arrival','style','action']) 
            ->make(true);
        }
    }

    public function import()
    {
        return view('accessories_material_stock_on_the_fly.import'); 
    }

    public function downloadFormImport()
    {
        return Excel::create('upload_stock_on_the_fly',function ($excel)
        {
            $excel->sheet('active', function($sheet)
            {
                $sheet->setCellValue('A1','TYPE_STOCK');
                $sheet->setCellValue('B1','PO_SUPPLIER');
                $sheet->setCellValue('C1','PO_BUYER');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','UOM');
                $sheet->setCellValue('F1','STOCK BEGINING');
                $sheet->setCellValue('G1','REMARK');

                for ($i=2; $i <200 ; $i++) {

                    //TYPE STOCK
                    $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                    $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                    $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                    $objValidation->setAllowBlank(false);
                    $objValidation->setShowInputMessage(true);
                    $objValidation->setShowErrorMessage(true);
                    $objValidation->setShowDropDown(true);
                    $objValidation->setErrorTitle('Input error');
                    $objValidation->setError('Value tidak ada dalam list.');
                    $objValidation->setPromptTitle('Pilih Type Stock');
                    $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                    $objValidation->setFormula1('type_stock');
                }
            });

            $excel->sheet('list',function($sheet)
            {
                $sheet->SetCellValue("A1", "TYPE STOCK");
                $sheet->SetCellValue("A2", "SLT");
                $sheet->SetCellValue("A3", "REGULER");
                $sheet->SetCellValue("A4", "PR/SR");
                $sheet->SetCellValue("A5", "MTFC");
                $sheet->SetCellValue("A6", "NB");
                $sheet->SetCellValue("A7", "ALLOWANCE");

                $variantsSheet = $sheet->_parent->getSheet(1); // Variants Sheet , "0" is the Index of Variants Sheet
  
                $sheet->_parent->addNamedRange(
                  new \PHPExcel_NamedRange(
                     'type_stock', $variantsSheet, 'A2:A7' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                  )
               );
              });
        })
        ->export('xlsx');
    }

    public function uploadFormImport(Request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path           = $request->file('upload_file')->getRealPath();
            $data           = Excel::selectSheets('active')->load($path,function($render){})->get();
            $upload_date    = Carbon::now()->toDateTimeString();
            
            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {
                    $type_stock     = strtoupper(trim($value->type_stock));
                    $document_no    = strtoupper(trim($value->po_supplier));
                    $item_code      = strtoupper(trim($value->item_code));
                    $po_buyer       = strtoupper(trim($value->po_buyer));
                    $uom            = strtoupper(trim($value->uom));
                    $remark         = strtoupper(trim($value->remark));
                    $stock          = sprintf('%0.8f',$value->stock_begining);
                    $item           = Item::where('item_code',$item_code)->first();
                    $item_id        = ($item)? $item->item_id : -1;
                    $item_desc      = ($item)? $item->item_desc : null;
                    $category       = ($item)? $item->category : null;
                    $uom_item       = ($item)? strtoupper(trim($item->uom)) : null;
                   
                    if($type_stock == 'SLT') $type_stock_erp_code = 1;
                    elseif($type_stock == 'REGULER') $type_stock_erp_code = 2;
                    elseif($type_stock == 'PR/SR') $type_stock_erp_code = 3;
                    elseif($type_stock == 'MTFC') $type_stock_erp_code = 4;
                    elseif($type_stock == 'NB') $type_stock_erp_code = 5;
                    elseif($type_stock == 'ALLOWANCE') $type_stock_erp_code = 6;
                    else $type_stock_erp_code = -1;

                    $check_item_on_supplier = DB::connection('erp')
                    ->table('wms_po_per_item')
                    ->where([
                        [db::raw('upper(documentno)'),$document_no],
                        [db::raw('upper(item)'),$item_code]
                    ])
                    ->exists();

                    if($type_stock_erp_code != -1 )
                    {
                        if($check_item_on_supplier)
                        {
                            $po_supplier_erp = DB::connection('erp')
                            ->table('wms_po_supplier')
                            ->where('documentno',$document_no)
                            ->first();

                            if($po_supplier_erp)
                            {
                                if($item_id != -1)
                                {
                                    $c_order_id     = $po_supplier_erp->c_order_id;
                                    $warehouse_id   = $po_supplier_erp->m_warehouse_id;
                                    $c_bpartner_id  = $po_supplier_erp->c_bpartner_id;
                                    $supplier       = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                                    $supplier_code  = ($supplier)? $supplier->supplier_code : null;
                                    $supplier_name  = ($supplier)? $supplier->supplier_name : null;
                                    
                                    $check_material_stock = MaterialStock::where([
                                        ['c_order_id',$c_order_id],
                                        ['c_bpartner_id',$c_bpartner_id],
                                        ['document_no',$document_no],
                                        ['item_code',$item_code],
                                        ['warehouse_id',$warehouse_id],
                                        ['is_stock_on_the_fly',true],
                                        ['is_running_stock',false],
                                        ['is_closing_balance',false],
                                        ['stock',$stock],
                                        ['type_stock_erp_code',$type_stock_erp_code],
                                    ]);

                                    if($po_buyer) $check_material_stock = $check_material_stock->where('po_buyer',$po_buyer);
                                    $check_material_stock = $check_material_stock->exists();

                                    if(!$check_material_stock)
                                    {
                                        if($uom == $uom_item)
                                        {
                                            try 
                                            {
                                                DB::beginTransaction();
                                                $material_stock = MaterialStock::FirstOrCreate([
                                                    'type_stock_erp_code'   => $type_stock_erp_code,
                                                    'type_stock'            => $type_stock,
                                                    'supplier_code'         => $supplier_code,
                                                    'supplier_name'         => $supplier_name,
                                                    'document_no'           => $document_no,
                                                    'c_bpartner_id'         => $c_bpartner_id,
                                                    'c_order_id'            => $c_order_id,
                                                    'item_id'               => $item_id,
                                                    'item_code'             => $item_code,
                                                    'po_buyer'              => ($po_buyer ? $po_buyer : null),
                                                    'item_desc'             => $item_desc,
                                                    'category'              => $category,
                                                    'type_po'               => 2,
                                                    'warehouse_id'          => $warehouse_id,
                                                    'uom'                   => $uom,
                                                    'qty_carton'            => 1,
                                                    'stock'                 => $stock,
                                                    'remark'                => ($remark ? $remark : null),
                                                    'reserved_qty'          => 0,
                                                    'created_at'            => $upload_date,
                                                    'updated_at'            => $upload_date,
                                                    'is_active'             => true,
                                                    'is_stock_on_the_fly'   => true,
                                                    'is_closing_balance'    => false,
                                                    'source'                => 'STOCK ON THE FLY',
                                                    'user_id'               => Auth::user()->id
                                                ]);
            
                                                HistoryStock::approved($material_stock->id
                                                ,$stock
                                                ,'0'
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,'SOTF'
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$type_stock_erp_code
                                                ,$type_stock
                                                ,false
                                                ,false);

                                                DB::commit();
                                            } catch (Exception $e) 
                                            {
                                                DB::rollBack();
                                                $message = $e->getMessage();
                                                ErrorHandler::db($message);
                                            }

                                            $obj                = new stdClass;
                                            $obj->type_stock    = $type_stock;
                                            $obj->supplier_code = $supplier_code;
                                            $obj->supplier_name = $supplier_name;
                                            $obj->document_no   = $document_no;
                                            $obj->po_buyer      = $po_buyer;
                                            $obj->item_code     = $item_code;
                                            $obj->warehouse_id  = $warehouse_id;
                                            $obj->warehouse_name= ($warehouse_id == '1000013'? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1');
                                            $obj->uom           = $uom;
                                            $obj->stock         = $stock;
                                            $obj->result        = 'SUCCESS';
                                            $obj->is_error      = false;
                                            $array[]            = $obj;
                                        }else
                                        {
                                            $obj                = new stdClass;
                                            $obj->type_stock    = $type_stock;
                                            $obj->supplier_code = $supplier_code;
                                            $obj->supplier_name = $supplier_name;
                                            $obj->document_no   = $document_no;
                                            $obj->po_buyer      = $po_buyer;
                                            $obj->item_code     = $item_code;
                                            $obj->warehouse_id  = $warehouse_id;
                                            $obj->warehouse_name= ($warehouse_id == '1000013'? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1');
                                            $obj->uom           = $uom;
                                            $obj->stock         = $stock;
                                            $obj->result        = 'FAILED, SILAHKAN CEK KEMBALI UOM ANDA';
                                            $obj->is_error      = true;
                                            $array[]            = $obj;
                                        }
                                    }else
                                    {
                                        $obj                = new stdClass;
                                        $obj->type_stock    = $type_stock;
                                        $obj->supplier_code = $supplier_code;
                                        $obj->supplier_name = $supplier_name;
                                        $obj->document_no   = $document_no;
                                        $obj->po_buyer      = $po_buyer;
                                        $obj->item_code     = $item_code;
                                        $obj->warehouse_id  = $warehouse_id;
                                        $obj->warehouse_name= ($warehouse_id == '1000013'? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1');
                                        $obj->uom           = $uom;
                                        $obj->stock         = $stock;
                                        $obj->result        = 'FAILED, DATA SUDAH ADA';
                                        $obj->is_error      = true;
                                        $array[]            = $obj;
                                    }
                                }else
                                {
                                    $obj                = new stdClass;
                                    $obj->type_stock    = null;
                                    $obj->supplier_code = null;
                                    $obj->supplier_name = null;
                                    $obj->document_no   = $document_no;
                                    $obj->po_buyer      = $po_buyer;
                                    $obj->item_code     = $item_code;
                                    $obj->warehouse_id  = null;
                                    $obj->warehouse_name= null;
                                    $obj->uom           = $uom;
                                    $obj->stock         = $stock;
                                    $obj->result        = 'FAILED, MASTER DATA ITEM TIDAK DITEMUKAN, SILAHKAN INFO ICT UNTUK PENARIKAN MASTER ITEM';
                                    $obj->is_error      = true;
                                    $array[]            = $obj;
                                }
                            }else
                            {
                                $obj                = new stdClass;
                                $obj->type_stock    = null;
                                $obj->supplier_code = null;
                                $obj->supplier_name = null;
                                $obj->document_no   = $document_no;
                                $obj->po_buyer      = $po_buyer;
                                $obj->item_code     = $item_code;
                                $obj->warehouse_id  = null;
                                $obj->warehouse_name= null;
                                $obj->uom           = $uom;
                                $obj->stock         = $stock;
                                $obj->result        = 'FAILED, DATA PO SUPPLIER TIDAK DITEMUKAN DI MASTER ERP';
                                $obj->is_error      = true;
                                $array[]            = $obj;
                            }
                        }else
                        {
                            $obj                = new stdClass;
                            $obj->type_stock    = null;
                            $obj->document_no   = $document_no;
                            $obj->po_buyer      = $po_buyer;
                            $obj->item_code     = $item_code;
                            $obj->warehouse_id  = null;
                            $obj->warehouse_name= null;
                            $obj->uom           = $uom;
                            $obj->stock         = $stock;
                            $obj->result        = 'FAILED, DATA TIDAK DITEMUKAN DI MASTER ERP';
                            $obj->is_error      = true;
                            $array[]            =$obj;
                        }
                    }else
                    {
                        $obj                = new stdClass;
                        $obj->type_stock    = null;
                        $obj->document_no   = $document_no;
                        $obj->po_buyer      = $po_buyer;
                        $obj->item_code     = $item_code;
                        $obj->warehouse_id  = null;
                        $obj->warehouse_name= null;
                        $obj->uom           = $uom;
                        $obj->stock         = $stock;
                        $obj->result        = 'FAILED, SILAHKAN MASUKAN TIPE STOCK';
                        $obj->is_error      = true;
                        $array[]            =$obj;
                    }
                    
                }
                return response()->json($array,200);
            }else{
                return response()->json('Import failed, please check your file',422);
            }


        }

    }

    static function updateStockArrival($stock_accessories)
    {
        $material_stocks     = MaterialStock::whereIn('id',$stock_accessories)->get();

        foreach ($material_stocks as $key => $material_stock) 
        {
            $document_no    = $material_stock->document_no;
            $c_order_id     = $material_stock->c_order_id;
            $item_id        = $material_stock->item_id;
            $item_code      = $material_stock->item_code;
            $warehouse_id   = $material_stock->warehouse_id;
            $c_bpartner_id  = $material_stock->c_bpartner_id;
            $po_buyer       = $material_stock->po_buyer;
            $qty_arrival    = sprintf('%0.8f',$material_stock->stock);

            $sotfs           = MaterialStock::where([
                ['c_order_id',$c_order_id],
                ['item_id',$item_id],
                ['warehouse_id',$warehouse_id],
                ['c_bpartner_id',$c_bpartner_id],
                ['is_stock_on_the_fly',true],
                ['is_running_stock',false],
            ]);

            if($po_buyer) $sotfs = $sotfs->where('po_buyer',$po_buyer);
            $sotfs = $sotfs->orderby('stock','asc')
            ->get();

            $total_line = count($sotfs);
            foreach ($sotfs as $key_line => $sotf) 
            {
                $_begining_stock    = sprintf('%0.8f',$sotf->stock);
                $available_qty      = ( $sotf->available_qty ? sprintf('%0.8f',$sotf->available_qty) : '0');
                
                if($total_line != ($key_line+1)) $begining_stock     = sprintf('%0.8f',$_begining_stock - $available_qty);
                else $begining_stock     = sprintf('%0.8f',$qty_arrival);
                
                
                if($begining_stock > 0)
                {
                    if ($qty_arrival/$begining_stock >= 1) $supplied = $begining_stock;
                    else  $supplied = $qty_arrival;

                    if($supplied > 0)
                    {
                        $sotf->available_qty = sprintf('%0.8f',$supplied);
                        $sotf->save();

                        $begining_stock -= $supplied;
                        $qty_arrival    -= $supplied;
                    }
                }
                
            }
            
        }
    }

    public function export()
    {
        $file = Config::get('storage.report') . '/' . e('report_sotf.csv');

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function delete(Request $request, $id)
    {
        $material_stock = MaterialStock::find($id);

        HistoryStock::approved($id
        ,'0'
        ,$material_stock->stock
        ,null
        ,null
        ,null
        ,null
        ,null
        ,'DELETE STOCK ON THE FLY'
        ,auth::user()->name
        ,auth::user()->id
        ,$material_stock->type_stock_erp_code
        ,$material_stock->type_stock
        ,false
        ,false);

        $material_stock->deleted_at         = carbon::now();
        $material_stock->is_closing_balance = true;
        $material_stock->save();

    }

    public function deleteFromBulk(Request $request)
    {
        return view('accessories_material_stock_on_the_fly.import_delete');
    }

    public function importDelete(Request $request)
    {
        return Excel::create('delete_stock_on_the_fly',function ($excel)
        {
            $excel->sheet('active', function($sheet)
            {
                $sheet->setCellValue('A1','ID');
                $sheet->setCellValue('B1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function exportDelete(Request $request)
    {
        if($request->hasFile('upload_delete_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);

            $path                           = $request->file('upload_delete_file')->getRealPath();
            $data                           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();

                    foreach($data as $key => $value)
                    {
                        $material_stock = MaterialStock::where('id', $value->id)
                                          ->whereNull('deleted_at')
                                          ->first();

                        if($material_stock)
                        {

                            HistoryStock::approved($value->id
                            ,'0'
                            ,$material_stock->stock
                            ,null
                            ,null
                            ,null
                            ,null
                            ,null
                            ,'DELETE STOCK ON THE FLY '.$value->remark
                            ,auth::user()->name
                            ,auth::user()->id
                            ,$material_stock->type_stock_erp_code
                            ,$material_stock->type_stock
                            ,false
                            ,false);

                            $material_stock->deleted_at         = carbon::now();
                            $material_stock->is_closing_balance = true;
                            $material_stock->save();

                            $obj                = new stdClass;
                            $obj->type_stock    = $material_stock->type_stock;
                            $obj->supplier_code = $material_stock->supplier_code;
                            $obj->supplier_name = $material_stock->supplier_name;
                            $obj->document_no   = $material_stock->document_no;
                            $obj->po_buyer      = $material_stock->po_buyer;
                            $obj->item_code     = $material_stock->item_code;
                            $obj->warehouse_id  = $material_stock->warehouse_id;
                            $obj->warehouse_name= ($material_stock->warehouse_id == '1000013'? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1');
                            $obj->uom           = $material_stock->uom;
                            $obj->stock         = $material_stock->stock;
                            $obj->result        = 'SUCCESS';
                            $obj->is_error      = false;
                            $array[]            = $obj;
                        }
                        else
                        {
                            $obj                = new stdClass;
                            $obj->type_stock    = null;
                            $obj->document_no   = null;
                            $obj->po_buyer      = null;
                            $obj->item_code     = null;
                            $obj->warehouse_id  = null;
                            $obj->warehouse_name= null;
                            $obj->uom           = null;
                            $obj->stock         = null;
                            $obj->result        = 'FAILED, ID '.$value->id. 'TIDAK DITEMUKAN';
                            $obj->is_error      = true;
                            $array[]            =$obj;
                        }


                    }
               
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }
            return response()->json($array,200);
        }
        return response()->json('Import failed, please check your file',422);
    }


}
