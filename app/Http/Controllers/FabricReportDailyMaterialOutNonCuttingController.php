<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Locator;
use App\Models\Barcode;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;
use App\Models\SummaryHandoverMaterial;
use App\Models\MaterialRollHandoverFabric;


class FabricReportDailyMaterialOutNonCuttingController extends Controller
{
    public function index()
    {
        $active_tab = 'handover';
        return view('fabric_report_daily_material_out_non_cutting.index',compact('active_tab'));
    }

    public function dataHandover(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $daily_handovers     = DB::table('daily_material_handover_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->where('is_move_order', false)
            ->whereBetween('created_at',[$start_date,$end_date])
            ->orderby('created_at','desc')
            ->take(50);
            
            return DataTables::of($daily_handovers)
            ->editColumn('created_at',function ($daily_handovers)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_handovers->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('complete_date',function ($daily_handovers)
            {
                if($daily_handovers->complete_date) return Carbon::createFromFormat('Y-m-d H:i:s', $daily_handovers->complete_date)->format('d/M/Y H:i:s');
                else null;
            })
            ->editColumn('warehouse_id',function ($daily_handovers)
            {
                if($daily_handovers->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($daily_handovers->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('total_qty_handover',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_handover, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('total_qty_supply',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_supply, 4, '.', ',');
            })
            ->addColumn('action',function($daily_handovers){
                return view('fabric_report_daily_material_out_non_cutting._action',[
                  'model'   => $daily_handovers,
                  'detail'  => route('fabricReportDailyMaterialOutNonCutting.detail',$daily_handovers->id),
                  'edit'    => route('fabricReportDailyMaterialOutNonCutting.edit',$daily_handovers->id),
                  'destroy' => route('fabricReportDailyMaterialOutNonCutting.destroy',$daily_handovers->id)
                ]);
              })
            ->setRowAttr([
                'style' => function($daily_handovers)
                {
                    if($daily_handovers->total_qty_outstanding != 0) return  'background-color: #fab1b1;';
                },
            ])
            ->rawColumns(['action','style']) 
            ->make(true);
        }
    }

    public function dataMoveOrder(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $daily_handovers     = DB::table('daily_material_handover_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->where('is_move_order', true)
            ->whereBetween('created_at',[$start_date,$end_date])
            ->orderby('created_at','desc');
            
            return DataTables::of($daily_handovers)
            ->editColumn('created_at',function ($daily_handovers)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_handovers->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('complete_date',function ($daily_handovers)
            {
                if($daily_handovers->complete_date) return Carbon::createFromFormat('Y-m-d H:i:s', $daily_handovers->complete_date)->format('d/M/Y H:i:s');
                else null;
            })
            ->editColumn('warehouse_id',function ($daily_handovers)
            {
                if($daily_handovers->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($daily_handovers->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('total_qty_handover',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_handover, 4, '.', ',');
            })
            ->editColumn('total_qty_outstanding',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_outstanding, 4, '.', ',');
            })
            ->editColumn('total_qty_supply',function ($daily_handovers)
            {
                return number_format($daily_handovers->total_qty_supply, 4, '.', ',');
            })
            ->addColumn('action',function($daily_handovers){
                return view('fabric_report_daily_material_out_non_cutting._action',[
                  'model'   => $daily_handovers,
                  'detail'  => route('fabricReportDailyMaterialOutNonCutting.detail',$daily_handovers->id),
                  'edit'    => route('fabricReportDailyMaterialOutNonCutting.edit',$daily_handovers->id),
                  'destroy' => route('fabricReportDailyMaterialOutNonCutting.destroy',$daily_handovers->id)
                ]);
              })
            ->setRowAttr([
                'style' => function($daily_handovers)
                {
                    if($daily_handovers->total_qty_outstanding != 0) return  'background-color: #fab1b1;';
                },
            ])
            ->rawColumns(['action','style']) 
            ->make(true);
        }
    }


    public function detail($id)
    {
        $daily_handover     = DB::table('daily_material_handover_v')->where('id',$id)->first();
        return view('fabric_report_daily_material_out_non_cutting.detail',compact('daily_handover'));
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $detail_materials     = DB::table('material_out_non_cutting_v')
            ->where('summary_handover_material_id',$id)
            ->orderby('created_at','desc');
            
            return DataTables::of($detail_materials)
            ->editColumn('created_at',function ($detail_materials)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $detail_materials->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('receive_date',function ($detail_materials)
            {
                if($detail_materials->receive_date)return  Carbon::createFromFormat('Y-m-d H:i:s', $detail_materials->receive_date)->format('d/M/Y H:i:s');
                else null;
            })
            ->editColumn('qty_handover',function ($detail_materials)
            {
                return number_format($detail_materials->qty_handover, 4, '.', ',');
            })
            ->editColumn('warehouse_to_id',function ($detail_materials)
            {
                if($detail_materials->warehouse_to_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($detail_materials->warehouse_to_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->addColumn('action',function($detail_materials) use($id){
                return view('fabric_report_daily_material_out_non_cutting._action',[
                  'model' => $detail_materials,
                  'print' => route('fabricReportDailyMaterialOutNonCutting.printBarcode',[$id,$detail_materials->id])
                ]);
              })
            ->make(true);
        }
    }

    public function edit(Request $request,$id)
    {
        $summary_material_handover  = SummaryHandoverMaterial::find($id);
        return response()->json($summary_material_handover,200);
    }

    public function update(Request $request)
    {
        $id                 = $request->id;
        $no_pt              = strtoupper(trim($request->no_pt));
        $no_bc              = strtoupper(trim($request->no_bc));
        $total_qty_handover = sprintf('%0.8f',$request->total_qty_handover);

        $summary_material_handover  = SummaryHandoverMaterial::find($id);
        $total_qty_supply           = $summary_material_handover->total_qty_supply;
        $new_total_qty_outstanding  = sprintf('%0.8f',$total_qty_handover - $total_qty_supply);

        try 
        {
            DB::beginTransaction();
              
            $summary_material_handover->handover_document_no    = $no_pt;
            $summary_material_handover->no_bc                   = $no_bc;
            $summary_material_handover->total_qty_handover      = $total_qty_handover;
            $summary_material_handover->total_qty_outstanding   = $new_total_qty_outstanding;

            if($new_total_qty_outstanding != 0) $summary_material_handover->complete_date = null;
            else if($new_total_qty_outstanding < 0) $summary_material_handover->complete_date = carbon::now();
            
            $summary_material_handover->save();

                
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json($summary_material_handover,200);
    }

    //belum ada fungsi balikin stock erp
    public function delete(Request $request,$id)
    {
        $total_material_receive     = MaterialRollHandoverFabric::where('summary_handover_material_id',$id)
        ->whereNotNull('receive_date')
        ->count();

        if($total_material_receive > 0)
        {
            return response()->json('error',422); 
        }else
        {
            $area_receive_fabric        = Locator::whereHas('area',function ($query){
                $query->where('name','RECEIVING')
                ->where('warehouse',auth::user()->warehouse);
            })
            ->first();

            $summary_material_handover  = SummaryHandoverMaterial::find($id);
            $detail_material_handovers  = MaterialRollHandoverFabric::where('summary_handover_material_id',$id)->get();
            $movement_date              = carbon::now()->toDateTimeString();
            $update_summary_stocks      = array();
            $concatenate                = '';
            
            try 
            {
                DB::beginTransaction();
                
                foreach ($detail_material_handovers as $key => $detail_material_handover) 
                {
                    $material_stock_id                      = $detail_material_handover->material_stock_id;
                    $barcode                                = $detail_material_handover->barcode;
                    $referral_code                          = $detail_material_handover->referral_code;
                    $sequence                               = $detail_material_handover->sequence;
                    $qty_handover                           = sprintf('%0.8f',$detail_material_handover->qty_handover);

                    if($summary_material_handover->is_move_order)
                    {
                        $status_movement    = 'cancel-move-order';
                        $source             = 'CANCEL MOVE ORDER';
                        $note_movement      = 'NOMOR SERI BARCODE '.$barcode.' TIDAK JADI DI MOVE ORDER, QTY '.$qty_handover.' KEMBALI KE STOCK';
                    }else
                    {
                        $status_movement    = 'cancel-handover';
                        $source             = 'CANCEL HANDOVER';
                        $note_movement      = 'NOMOR SERI BARCODE '.$barcode.' TIDAK JADI DI PINDAH TANGAN, QTY '.$qty_handover.' KEMBALI KE STOCK';
                    }
                    
                    $material_stock                         = MaterialStock::find($material_stock_id);
                    $reserved_qty                           = sprintf('%0.8f',$material_stock->reserved_qty);
                    $qty_order                              = sprintf('%0.8f',$material_stock->qty_order);
                    $stock                                  = sprintf('%0.8f',$material_stock->stock);
                    $barcode_parent                         = $material_stock->barcode_supplier;
                    $item_id                                = $material_stock->item_id;
                    $po_detail_id                           = $material_stock->po_detail_id;
                    $no_packing_list                        = $material_stock->no_packing_list;
                    $no_invoice                             = $material_stock->no_invoice;
                    $c_order_id                             = $material_stock->c_order_id;
                    $batch_number                           = $material_stock->batch_number;
                    $nomor_roll                             = $material_stock->nomor_roll;
                    $uom                                    = $material_stock->uom;
                    $document_no                            = $material_stock->document_no;
                    $c_bpartner_id                          = $material_stock->c_bpartner_id;
                    $warehouse_id                           = $material_stock->warehouse_id;
                    $summary_stock_fabric_id                = $material_stock->summary_stock_fabric_id;
                    $item_code                              = $material_stock->item_code;
                    $item_desc                              = $material_stock->item_desc;
                    $color                                  = $material_stock->color;
                    $upc                                    = $material_stock->upc;
                    $category                               = $material_stock->category;
                    $supplier_name                          = $material_stock->supplier_name;
                    $supplier_code                          = $material_stock->supplier_code;
                    $load_actual                            = $material_stock->load_actual;
                    $_material_roll_handover_fabric_id      = $material_stock->material_roll_handover_fabric_id;
                    $summary_handover_material_id           = $material_stock->summary_handover_material_id;
                    $jenis_po                               = $material_stock->jenis_po;
                    $qc_result                              = $material_stock->qc_result;
                    $begin_width                            = $material_stock->begin_width;
                    $middle_width                           = $material_stock->middle_width;
                    $end_width                              = $material_stock->end_width;
                    $actual_width                           = $material_stock->actual_width;
                    $actual_length                          = $material_stock->actual_length;
                    $different_yard                         = $material_stock->different_yard;
                    $inspect_lab_date                       = $material_stock->inspect_lab_date;
                    $user_lab_id                            = $material_stock->user_lab_id;
                    $inspect_lab_remark                     = $material_stock->inspect_lab_remark;
                    $is_from_handover                       = $material_stock->is_from_handover;
                    $monitoring_receiving_fabric_id         = $material_stock->monitoring_receiving_fabric_id;
                    $upc_item                               = $material_stock->upc_item;
                    $type_stock_erp_code                    = $material_stock->type_stock_erp_code;
                    $type_stock                             = $material_stock->type_stock;
                    $new_qty_order                          = sprintf('%0.8f',$qty_order - $qty_handover);

                    $material_stock->qty_order              = $new_qty_order;

                    //insert new roll
                    $is_stock_exists = MaterialStock::where([
                        ['summary_stock_fabric_id',$summary_stock_fabric_id],
                        ['barcode_supplier',$barcode],
                        ['item_id',$item_id],
                        ['no_packing_list',$no_packing_list],
                        ['no_invoice',$no_invoice],
                        ['c_order_id',$c_order_id],
                        ['batch_number',$batch_number],
                        ['nomor_roll',$nomor_roll],
                        ['document_no',$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['stock',$qty_handover],
                        ['type_po',1],
                        ['source',$source.', INDUK '.$barcode_parent],
                    ])
                    ->exists();

                    //dd($is_stock_exists);
                    if(!$is_stock_exists)
                    {

                        $new_material_stock = MaterialStock::FirstOrCreate([
                            'locator_id'                            => $area_receive_fabric->id,
                            'barcode_supplier'                      => $barcode,
                            'referral_code'                         => $referral_code,
                            'sequence'                              => $sequence,
                            'monitoring_receiving_fabric_id'        => $monitoring_receiving_fabric_id,
                            'po_detail_id'                          => $po_detail_id,
                            'item_code'                             => $item_code,
                            'item_desc'                             => $item_desc,
                            'user_lab_id'                           => $user_lab_id,
                            'color'                                 => $color,
                            'category'                              => $category,
                            'no_packing_list'                       => $no_packing_list,
                            'inspect_lab_remark'                    => $inspect_lab_remark,
                            'no_invoice'                            => $no_invoice,
                            'load_actual'                           => $load_actual,
                            'qc_result'                             => $qc_result,
                            'is_from_handover'                      => $is_from_handover,
                            'upc_item'                              => $upc_item,
                            'actual_length'                         => $actual_length,
                            'type_po'                               => 1,
                            'c_order_id'                            => $c_order_id,
                            'item_id'                               => $item_id,
                            'begin_width'                           => $begin_width,
                            'middle_width'                          => $middle_width,
                            'end_width'                             => $end_width,
                            'actual_width'                          => $actual_width,
                            'inspect_lab_date'                      => $inspect_lab_date,
                            'qty_order'                             => $qty_handover,
                            'qty_arrival'                           => $qty_handover,
                            'type_stock_erp_code'                   => $type_stock_erp_code,
                            'type_stock'                            => $type_stock,
                            'summary_stock_fabric_id'               => $summary_stock_fabric_id,
                            'stock'                                 => $qty_handover,
                            'available_qty'                         => $qty_handover,
                            'batch_number'                          => $batch_number,
                            'is_material_others'                    => true,
                            'nomor_roll'                            => $nomor_roll,
                            'uom'                                   => $uom,
                            'different_yard'                        => $different_yard,
                            'source'                                => $source.', INDUK '.$barcode_parent,
                            'is_active'                             => true,
                            'supplier_name'                         => $supplier_name,
                            'document_no'                           => $document_no,
                            'c_bpartner_id'                         => $c_bpartner_id,
                            'supplier_code'                         => $supplier_code,
                            'warehouse_id'                          => $warehouse_id,
                            'material_roll_handover_fabric_id'      => $_material_roll_handover_fabric_id,
                            'summary_handover_material_id'          => $summary_handover_material_id,
                            'jenis_po'                              => $jenis_po,
                            'user_id'                               => auth::user()->id,
                            'approval_user_id'                      => auth::user()->id,
                            'approval_date'                         => $movement_date,
                            'created_at'                            => $movement_date,
                            'updated_at'                            => $movement_date,
                        ]);

                        MovementStockHistory::FirstOrCreate([
                            'material_stock_id'     => $new_material_stock->id,
                            'from_location'         => null,
                            'to_destination'        => $area_receive_fabric->id,
                            'status'                => $status_movement,
                            'uom'                   => $uom,
                            'qty'                   => $qty_handover,
                            'movement_date'         => $movement_date,
                            'note'                  => $source.', BARCODE INDUK '.$barcode_parent.$note_movement,
                            'user_id'               => auth::user()->id
                        ]);

                        if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$po_detail_id)
                            ->first();
                            
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : '=');
                        }

                        $inventory_erp = Locator::with('area')
                        ->where([
                            ['is_active',true],
                            ['rack','ERP INVENTORY'],
                        ])
                        ->whereHas('area',function ($query) use($warehouse_id)
                        {
                            $query->where('is_active',true);
                            $query->where('warehouse',$warehouse_id);
                            $query->where('name','ERP INVENTORY');
                        })
                        ->first();

                        if(!$summary_material_handover->is_move_order)
                        {

                        $is_movement_exists = MaterialMovement::where([
                            ['from_location',$inventory_erp->id],
                            ['to_destination',$inventory_erp->id],
                            ['from_locator_erp_id',$inventory_erp->area->erp_id],
                            ['to_locator_erp_id',$inventory_erp->area->erp_id],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','integration-to-inventory-erp'],
                        ])
                        ->first();
                        
                        if(!$is_movement_exists)
                        {
                            $material_movement = MaterialMovement::firstOrCreate([
                                'from_location'         => $inventory_erp->id,
                                'to_destination'        => $inventory_erp->id,
                                'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'status'                => 'integration-to-inventory-erp',
                                'created_at'            => $movement_date,
                                'updated_at'            => $movement_date,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_movement_exists->updated_at = $movement_date;
                            $is_movement_exists->save();

                            $material_movement_id = $is_movement_exists->id;
                        }
                        
                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_stock_id',$material_stock_id],
                            ['qty_movement',$qty_handover],
                            ['item_id',$item_id],
                            ['warehouse_id',$warehouse_id],
                            ['date_movement',$movement_date],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_exists)
                        {
                            MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_id,
                                'material_stock_id'             => $material_stock_id,
                                'item_code'                     => $item_code,
                                'item_id'                       => $item_id,
                                'c_order_id'                    => $c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $c_bpartner_id,
                                'supplier_name'                 => $supplier_name,
                                'type_po'                       => 1,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $uom,
                                'qty_movement'                  => $qty_handover,
                                'date_movement'                 => $movement_date,
                                'created_at'                    => $movement_date,
                                'updated_at'                    => $movement_date,
                                'date_receive_on_destination'   => $movement_date,
                                'nomor_roll'                    => $nomor_roll,
                                'warehouse_id'                  => $warehouse_id,
                                'document_no'                   => $document_no,
                                'note'                          => 'PROSES INTEGRASI UNTUK CANCEL HANDOVER FABRIC DILAKUKAN OLEH '.auth::user()->name,
                                'is_active'                     => true,
                                'user_id'                       => auth::user()->id,
                            ]);
                        }

                    }


                        $concatenate                            .= "'".$new_material_stock->id."',";
                    }

                    $update_summary_stocks [] = $summary_stock_fabric_id;
                    $material_stock->save();
                    $detail_material_handover->delete();

                }

                $summary_material_handover->total_qty_outstanding   = '0';
                $summary_material_handover->total_qty_supply        = '0';
                $summary_material_handover->deleted_user_id         = auth::user()->id;
                $summary_material_handover->deleted_at              = $movement_date;
                $summary_material_handover->save();

                
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            $this->updateSummaryStockFabric($update_summary_stocks);

            return response()->json('success',200); 
        }

    }
    
    public function export(Request $request)
    {
        $warehouse = $request->warehouse;
        if(auth::user()->hasRole(['admin-ict-fabric','mm-staff']))
        {
            $filename = 'report_monitoring_receiving_fabric.csv';
        }else
        {
            if($warehouse == '1000011') $filename = 'report_monitoring_receiving_fabric_aoi2.csv';
            else if($warehouse == '1000001') $filename = 'report_monitoring_receiving_fabric_aoi1.csv';
        }
      
          $file = Config::get('storage.report') . '/' . $filename;
      
          if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
          
          $resp = response()->download($file);
          $resp->headers->set('Pragma', 'no-cache');
          $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
          $resp->headers->set('X-Content-Type-Options', 'nosniff');
          return $resp;
    }

    public function printBarcode($summary_id,$id)
    {
        $material_stocks  = MaterialRollHandoverFabric::where('id',$id)->get();
        foreach ($material_stocks as $key => $value) 
        {
            
            if($value->barcode == 'BELUM DI PRINT')
            {

                $_warehouse_id          = $value->summaryHandoverMaterial->warehouse_id;
                $handover_document_no   = $value->summaryHandoverMaterial->handover_document_no;

                $handover       = Locator::with('area')
                ->whereHas('area',function ($query) use ($_warehouse_id)
                {
                    $query->where([
                        ['warehouse',$_warehouse_id],
                        ['name','HANDOVER'],
                        ['is_destination',true]
                    ]);

                })
                ->first();

                $inventory  = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($_warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$_warehouse_id);
                })
                ->first();
                
                if($value->summaryHandoverMaterial->is_move_order)
                {
                    if($value->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($value->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$value->summaryHandoverMaterial->no_kk;
                        if($value->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$value->summaryHandoverMaterial->no_bc;
                    }
                    $note           = 'ROLL DIKELUARKAN KE '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper($value->user->name);
                    
                    $status         = 'move-order';
                    $destination    = null;
                }else
                {
                    if($value->summary_handover_material_id)
                    {
                        $no_kk = null;
                        $no_bc = null;
                        if($value->summaryHandoverMaterial->no_kk) $no_kk = ' ,DENGAN NOMOR KK '.$value->summaryHandoverMaterial->no_kk;
                        if($value->summaryHandoverMaterial->no_bc) $no_bc = ' ,DENGAN NOMOR BC '.$value->summaryHandoverMaterial->no_bc;
                    }
                    $note           = 'PINDAH TANGAN NOMOR PT '.$handover_document_no.$no_kk.$no_bc.' DILAKUKAN OLEH '.strtoupper(auth::user()->name);
                   
                    $status         = 'out-handover';
                    $destination    = $handover->id;
                }

                $get_barcode        = $this->randomCode($value->materialStock->barcode_supplier);
                $new_barcode        = $get_barcode->barcode;
                $new_referral_code  = $get_barcode->referral_code;
                $new_sequence       = $get_barcode->sequence;
                
                $value->barcode         = $new_barcode;
                $value->referral_code   = $new_referral_code;
                $value->sequence        = $new_sequence;

                MovementStockHistory::FirstOrCreate([
                    'material_stock_id'     => $value->material_stock_id,
                    'from_location'         => $inventory->id,
                    'to_destination'        => $destination,
                    'status'                => $status,
                    'uom'                   => $value->uom,
                    'qty'                   => $value->qty_handover,
                    'movement_date'         => $value->created_at,
                    'note'                  => $note.' (NOMOR SERI BARCODE '.$new_barcode.')',
                    'user_id'               => $value->user_id
                ]);

                $value->save();
            }
        }
        
        return view('fabric_report_daily_material_out_non_cutting.barcode',compact('material_stocks'));
    }

    static function randomCode($barcode)
    {
        $sequence = Barcode::where('referral_code',$barcode)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::FirstOrCreate([
            'barcode'       => $barcode.'-'.$sequence,
            'referral_code' => $barcode,
            'sequence'      => $sequence,
        ]);

        $obj                = new stdClass();
        $obj->barcode       = $barcode.'-'.$sequence;
        $obj->referral_code = $barcode;
        $obj->sequence      = $sequence;

        return $obj;
    }

    static function updateSummaryStockFabric($summary_stock_fabric_ids)
    {
        $summary_stock_fabrics = DB::table('summary_vs_stock_v')
        ->whereIn('id',$summary_stock_fabric_ids)
        ->get();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($summary_stock_fabrics as $key => $value) 
            {
                $summary_stock_fabric_id  = $value->id;
                $total_stock                          = sprintf('%0.8f',$value->detail_total_stock);
                $total_reserved_qty                   = sprintf('%0.8f',$value->detail_total_reserved_qty);
                $total_available_qty                  = sprintf('%0.8f',$value->detail_total_available_qty);
                
                $summary_stock_fabric                 = SummaryStockFabric::find($summary_stock_fabric_id);
                $summary_stock_fabric->stock          = $total_stock; 
                $summary_stock_fabric->reserved_qty   = $total_reserved_qty; 
                $summary_stock_fabric->available_qty  = $total_available_qty; 
                $summary_stock_fabric->save();
            }
        
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
