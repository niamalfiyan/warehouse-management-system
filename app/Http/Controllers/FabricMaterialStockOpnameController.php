<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;
use App\Models\HistoryMaterialStockOpname;

use App\Http\Controllers\FabricMaterialStockApprovalController as HistoryStock;

class FabricMaterialStockOpnameController extends Controller
{
    public function index()
    {
        return view('fabric_material_stock_opname.index');
        
    }

    public function create(Request $request)
    {
        $warehouse_id   =  $request->warehouse_id;
        $barcode        = trim(strtoupper($request->barcode));
        $type   =  $request->type;

        $is_barcode_locator = substr($barcode,0, 1);
        if($is_barcode_locator != 'R')
        {
            $array = $this->getDataBarcode($barcode,$warehouse_id,$type);

            if(isset($array->original)) return response()->json($array->original,422);

            
            return response()->json(['data' => $array],200);
        }else
        {
            $locator = Locator::where('barcode',$barcode)->first();
            if(!$locator) return response()->json('Locator not found ditemukan',422);
 
            return response()->json(['locator' => $locator],200);
        }
    }

    public function locatorPicklist(Request $request)
    {
        $q          = strtoupper($request->q);
        $warehouse  = $request->warehouse_id;
        $lists = Locator::whereHas('area',function($query) use($warehouse){
            $query->where([
                ['warehouse',$warehouse],
                ['is_active',true],
                ['is_destination',false],
                ['name','INVENTORY'],
            ]);
        })
        ->where('is_active',true)
        ->where(function($query) use($q)
        {
            $query->where('barcode','like',"%$q%")
            ->orWhere('code','like',"%$q%");
        })
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->paginate('10');
        
        return view('fabric_material_stock_opname._rack_fabric_list',compact('lists'));
    }

    static function getDataBarcode($barcode,$warehouse_id,$type)
    {
        //get data free stock
        if($type == 'free stock')
        {
            $material_stock = MaterialStock::where([
                ['barcode_supplier',$barcode],
                ['warehouse_id',$warehouse_id],
            ])
            ->orderby('created_at','desc')
            ->first();

            if(!$material_stock) return response()->json('Barcode not found',422);
        
            if($material_stock->is_short_roll)
            {
                if(!$material_stock->movement_to_locator_reject_date_from_short_roll) return response()->json('Actual yard is not same with barcode, please move to locator reject first',422);
            } 

            if($material_stock->is_reject_by_lot)
            {
                if(!$material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Roll cannot be used due of lot is not match,please move to locator reject first',422);
            } 

            if($material_stock->is_reject_by_rma)
            {
                if(!$material_stock->movement_to_locator_reject_date_from_rma) return response()->json('cannot be used due of material is wrong,please move to locator reject first',422);
            } 

            if($material_stock->is_reject)
            {
                if(!$material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Roll cannot be used due of reject result by qc,please move to locator reject first',422);
            } 

            if($material_stock->movement_to_locator_reject_date_from_lot) return response()->json('Barcode cannot used due reject by lot',422);
            if($material_stock->movement_to_locator_reject_date_from_rma) return response()->json('Barcode cannot used due reject by return material',422);
            if($material_stock->movement_to_locator_reject_date_from_inspect) return response()->json('Barcode cannot used due reject by qc',422);
            
            $is_outstanding_approval_exists = HistoryMaterialStockOpname::where('material_stock_id',$material_stock->id)
            ->where(function($query){
                $query->whereNull('approval_date')
                ->OrwhereNull('accounting_approval_date');
            })
            ->exists();

            if($is_outstanding_approval_exists) return response()->json('Roll cannot be STO due outstanding approval STO',422);

            $obj                        = new StdClass();
            $obj->id                    = $material_stock->id;
            $obj->source_rak_id         = ($material_stock->locator_id) ? $material_stock->locator_id : null;
            $obj->source_rak_code       = ($material_stock->locator_id) ? $material_stock->locator->code : null;
            $obj->warehouse_id          = $material_stock->warehouse_id;
            $obj->item_code             = $material_stock->item_code;
            $obj->color                 = $material_stock->color;
            $obj->category              = $material_stock->category;
            $obj->barcode_supplier      = $material_stock->barcode_supplier;
            $obj->uom                   = $material_stock->uom;
            $obj->stock                 = $material_stock->stock;
            $obj->available_qty_on_db   = $material_stock->available_qty;
            $obj->available_qty         = $material_stock->available_qty;
            $obj->adjustment_stock      = null;
            $obj->batch_number          = $material_stock->batch_number;
            $obj->nomor_roll            = $material_stock->nomor_roll;
            $obj->document_no           = $material_stock->document_no;
            $obj->_is_sto               = $material_stock->is_sto;
            $obj->is_sto                = $material_stock->is_sto;
            $obj->is_error              = false;
            $obj->remark                = null;
            $obj->sto_note              = null;
            $obj->check_all             = false;
            $obj->is_exclude            = false;
            $obj->last_sto_date         = ($material_stock->sto_date ? $material_stock->sto_date->format('d/M/y H:i:s') : null) ;
            $obj->last_sto_name         = ($material_stock->sto_user_id ? $material_stock->userSto->name : null);

            $material_stock->last_date_used     = carbon::now();
            $material_stock->last_status        = 'sto';
            $material_stock->last_user_used_id  = auth::user()->id;
            $material_stock->save();

        }
        else
        {
            $detail_material_preparation_fabric = DetailMaterialPreparationFabric::where([
                ['barcode',$barcode],
                ['warehouse_id',$warehouse_id],
            ])
            ->first();

            if($detail_material_preparation_fabric)
            {
                $material_stock = MaterialStock::where([
                    ['id',$detail_material_preparation_fabric->material_stock_id],
                ])
                ->orderby('created_at','desc')
                ->first();

               
            }

            if(!$material_stock) return response()->json('Barcode not found',422);


            
            $obj                        = new StdClass();
            $obj->id                    = $detail_material_preparation_fabric->id;
            $obj->source_rak_id         = ($detail_material_preparation_fabric->last_locator_id) ? $detail_material_preparation_fabric->last_locator_id : null;
            $obj->source_rak_code       = ($detail_material_preparation_fabric->last_locator_id) ? $detail_material_preparation_fabric->locator->code : null;
            $obj->warehouse_id          = $material_stock->warehouse_id;
            $obj->item_code             = $material_stock->item_code;
            $obj->color                 = $material_stock->color;
            $obj->category              = $material_stock->category;
            $obj->barcode_supplier      = $detail_material_preparation_fabric->barcode;
            $obj->uom                   = $material_stock->uom;
            $obj->stock                 = $material_stock->stock;
            $obj->available_qty_on_db   = $detail_material_preparation_fabric->reserved_qty;
            $obj->available_qty         = $detail_material_preparation_fabric->reserved_qty;
            $obj->adjustment_stock      = null;
            $obj->batch_number          = $material_stock->batch_number;
            $obj->nomor_roll            = $material_stock->nomor_roll;
            $obj->document_no           = $material_stock->document_no;
            $obj->_is_sto               = $material_stock->is_sto;
            $obj->is_sto                = $material_stock->is_sto;
            $obj->is_error              = false;
            $obj->remark                = null;
            $obj->sto_note              = null;
            $obj->check_all             = false;
            $obj->is_exclude            = false;
            $obj->last_sto_date         = ($material_stock->sto_date ? $material_stock->sto_date->format('d/M/y H:i:s') : null) ;
            $obj->last_sto_name         = ($material_stock->sto_user_id ? $material_stock->userSto->name : null);
            
        }
        return $obj;
    }

    static function removeLastUser(Request $request)
    {
        $material_stock                     = MaterialStock::find($request->id);
        $material_stock->last_user_used_id  = null;
        $material_stock->last_status        = null;
        $material_stock->last_date_used     = null;
        $material_stock->save();
        
        return response()->json(200);
    }

    public function store(Request $request)
    {
        $validator =  Validator::make($request->all(), [
            'barcode_products' => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $warehouse_id           = $request->warehouse_id;
            $type           = $request->type;
            $data                   = json_decode($request->barcode_products);
            $area_receive_fabric    = Locator::whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('name','RECEIVING')
                ->where('warehouse',$warehouse_id);
            })
            ->first();

            $sto_area = Locator::whereHas('area',function ($query) use ($warehouse_id)
            {
                $query->where('name','STO')
                ->where('warehouse',$warehouse_id);
            })
            ->first();
            
            $array          = array();
            $list_approval_ids       = array();
            $sto_date       = carbon::now();
            $new_locator_id = $request->locator_in_fabric_id;
            
            try 
            {
                DB::beginTransaction();

                foreach ($data as $key => $value) 
                {
                    if($type == 'free stock')
                    {
                        $id                 = $value->id;
                        $material_stock     = MaterialStock::find($id);
                        $locator_id         = $material_stock->locator_id;
                        $available_qty_old  = $material_stock->available_qty;
                        $available_qty_new  = sprintf('%0.8f',$value->adjustment_stock);
                        $remark             = strtoupper(trim($value->remark));
                        $sto_note           = strtoupper(trim($value->sto_note));
                        $operator           = sprintf('%0.8f',$available_qty_old - $available_qty_new);
                    
                        
                        if($operator < 0)
                        {
                            $_operator = (-1*$operator);
                        }else
                        {
                            if($operator == 0)
                            {
                                $_operator = $operator;
                            }else
                            {
                                $_operator = (-1*$operator);
                            }
                        }
                        
                        if($_operator == 0)
                        {
                            $material_stock->is_sto      = true;
                            $material_stock->sto_remark  = $remark;
                            $material_stock->sto_date    = $sto_date;
                            $material_stock->sto_user_id = auth::user()->id;
                        }
                        else
                        {
                            $history_material_stock_opname = HistoryMaterialStockOpname::create([
                                'material_stock_id'     => $id,
                                'locator_old_id'        => $locator_id,
                                'locator_new_id'        => ($new_locator_id ? $new_locator_id : ($locator_id ? $locator_id : $area_receive_fabric->id)),
                                'available_stock_old'   => $available_qty_old,
                                'available_stock_new'   => $available_qty_new,
                                'operator'              => $_operator,
                                'source'                => 'mutasi',
                                'note'                  => $remark,
                                'sto_note'              => $remark.'. [ '.$sto_note.']',
                                'sto_date'              => $sto_date,
                                'user_id'               => auth::user()->id,
                            ]);

                            $list_approval_ids [] = $history_material_stock_opname->id;
                        }

                        $material_stock->last_date_used     = null;
                        $material_stock->last_status        = null;
                        $material_stock->last_user_used_id  = null;
                        $material_stock->save();
                    }
                    else
                    {
                        $id                                                  = $value->id;
                        $remark                                              = strtoupper(trim($value->remark));
                        //dd($id, $remark, $new_locator_id);
                        $detail_material_preparation_fabric                  = DetailMaterialPreparationFabric::find($id);
                        $detail_material_preparation_fabric->last_locator_id = $new_locator_id;
                        $detail_material_preparation_fabric->remark_ict      = $remark;
                        $detail_material_preparation_fabric->save();
                    }

                }

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            foreach($list_approval_ids as $key => $list_approval_id)
            {
                HistoryStock::itemApprove($list_approval_id);
            }

            return response()->json(200);
        }else{
            return response()->json('Please scan barcode first.',422);
        }
            
    }

    public function importRemoveStock()
    {
        return view('fabric_material_stock_opname.import_remove_stock');
    }

    public function removeStock(Request $request)
    {
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
            ]);

            $path                           = $request->file('upload_file')->getRealPath();
            $data                           = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                    try 
                {
                    DB::beginTransaction();
                    $sto_date       = carbon::now();
                    foreach ($data as $key => $value) 
                    {
                        $id                 = $value->material_stock_id;
                        $material_stock     = MaterialStock::find($id);

                        if($material_stock)
                        {
                            $locator_id         = $material_stock->locator_id;
                            $available_qty_old  = $material_stock->available_qty;
                            $available_qty_new  = 0;
                            $remark             = strtoupper(trim($value->remark));
                            $sto_note           = 'STOCK BERKURANG MENJADI 0';
                            $operator           = sprintf('%0.5f',$available_qty_old - $available_qty_new);
                        
                            
                            if($operator < 0)
                            {
                                $_operator = (-1*$operator);
                            }else
                            {
                                if($operator == 0)
                                {
                                    $_operator = $operator;
                                }else
                                {
                                    $_operator = (-1*$operator);
                                }
                            }
                            
                            HistoryMaterialStockOpname::create([
                                'material_stock_id'     => $id,
                                'locator_old_id'        => $locator_id,
                                'locator_new_id'        => $locator_id,
                                'available_stock_old'   => $available_qty_old,
                                'available_stock_new'   => $available_qty_new,
                                'operator'              => $_operator,
                                'source'                => 'mutasi',
                                'note'                  => $remark,
                                'sto_note'              => $remark.'. [ '.$sto_note.']',
                                'sto_date'              => $sto_date,
                                'user_id'               => auth::user()->id,
                            ]);

                            $material_stock->last_date_used     = null;
                            $material_stock->last_status        = null;
                            $material_stock->last_user_used_id  = null;
                            $material_stock->save();

                            $obj = new stdClass();
                            $obj->barcode       = $material_stock->barcode_supplier;
                            $obj->document_no   = $material_stock->document_no;
                            $obj->nomor_roll    = $material_stock->nomor_roll;
                            $obj->item_code     = $material_stock->item_code;
                            $obj->uom           = $material_stock->uom;
                            $obj->qty           = $material_stock->available_qty;
                            $obj->is_error      = false;
                            $obj->status        = 'success';
                            $result []          = $obj;

                        }
                        else
                        {
                            $obj = new stdClass();
                            $obj->barcode       = null;
                            $obj->document_no   = null;
                            $obj->nomor_roll    = null;
                            $obj->item_code     = null;
                            $obj->uom           = null;
                            $obj->qty           = null;
                            $obj->is_error      = true;
                            $obj->status        = $id.' Data Tidak Ditemukan';
                            $result []          = $obj;
                        }

                    }

                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                return response()->json($result,200);
            }
        }
        return response()->json('Import failed, please check again',422);           
    }

    public function downloadFormUpload()
    {
        return Excel::create('upload_remove_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','REMARK');
                $sheet->setColumnFormat(array(
                    'A' => '@'
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

}
