<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\SummaryStockFabric;
use App\Models\MovementStockHistory;
use App\Models\MaterialMovementLine;
use App\Models\HistoryMaterialStockOpname;

class FabricMaterialStockApprovalController extends Controller
{
    public function index()
    {
        return view('fabric_material_stock_approval.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        { 
            $warehouse_id       = $request->warehouse;
            $user_department    = auth::user()->department;
            $data = DB::table('fabric_material_stock_approval_v');
           

            if($user_department == 'finance & accounting' || $user_department == 'ict') $data = $data->whereNull('accounting_approval_date');
            else if($user_department == 'material management') $data = $data->whereNull('approval_date');
           
            $data = $data->where('warehouse_id','LIKE',"%$warehouse_id%");
            
            return DataTables::of($data)
            ->editColumn('checkbox',function($data){
                return "<input type='checkbox' name='is_checked[]' id='".$data->history_material_stock_opname_id."' data-id='".$data->history_material_stock_opname_id."' class='mycheckbox' data-status='0'>";
           })
            ->editColumn('operator',function ($data)
            {
                if ($data->operator > 0)
                {
                    return '+'.number_format($data->operator, 4, '.', ',').'('.trim($data->uom).')';
                }else
                {
                    return number_format($data->operator, 4, '.', ',').'('.trim($data->uom).')';
                }
                
            })
            ->editColumn('source',function ($data)
            {
                
                if($data->source == 'new'){
                    return 'Stock baru, sumbernya dari '.$data->note;
                }else
                {
                    return 'Hasil opname gudang, alasan di rubah karena '.$data->note;
                }
            })
            ->editColumn('created_at',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
            })
            ->addColumn('action', function($data)  use($user_department)
            {
                if($user_department == 'finance & accounting' || $user_department == 'material management')
                {
                    return view('fabric_material_stock_approval._action', [
                        'model'         => $data,
                        'reject'        => route('fabricMaterialStockApproval.reject',$data->history_material_stock_opname_id),
                        'item_approve'  => route('fabricMaterialStockApproval.itemApprove',$data->history_material_stock_opname_id),
                    ]);
                }
                
            })
            ->setRowAttr([
                'style' => function($data)
                {
                    if($data->approval_date) return  'background-color: #fffed9;';
                    else if($data->accounting_approval_date) return  'background-color: #d9ffde;';
                },
            ])
            ->rawColumns(['action','style','checkbox']) 
            ->make(true);
        }
    }

    public function reject($id)
    {
        $history_material_stock_opname = HistoryMaterialStockOpname::find($id);
        $material_stock_id  = $history_material_stock_opname->material_stock_id;
       
        if($history_material_stock_opname->source =='new')
        {
            $material_stock     = MaterialStock::find($material_stock_id)->delete();;
        }else
        {
            $history_material_stock_opname->delete();
        }
        
        return response()->json(200);
    }

    static function itemApprove($id)
    {
        $history_material_stock_opname = HistoryMaterialStockOpname::find($id);
        $user_department               = auth::user()->department;

        try 
        {
            DB::beginTransaction();

            // if($user_department == 'finance & accounting' || $user_department == 'ict')
            // {
                //if(!$history_material_stock_opname->approval_date) return response()->json('MM must approve first then you can approve it.',422); 
                
                if(!$history_material_stock_opname->accounting_approval_date)
                {
                    $history_material_stock_opname->accounting_approval_date    = carbon::now();
                    $history_material_stock_opname->accounting_user_id          = auth::user()->id;
                    $history_material_stock_opname->save();
                    FabricMaterialStockApprovalController::checkApprovalStatus($id);
                }
                
            // }else if($user_department == 'material management')
            // {
            //     if(!$history_material_stock_opname->approval_date)
            //     {
            //         $history_material_stock_opname->approval_date       = carbon::now();
            //         $history_material_stock_opname->user_approval_id    = auth::user()->id;
            //         $history_material_stock_opname->save();
            //         FabricMaterialStockApprovalController::checkApprovalStatus($id);
            //     }
                
            // }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json(200);
    }

    static function checkApprovalStatus($id)
    {
        try 
        {
            DB::beginTransaction();

            $summary_stock_fabrics          = array();
            $update_counter_old_locators    = array();
            $update_counter_new_locators    = array();
            $update_summary_stock           = array();
            $history_material_stock_opname  = HistoryMaterialStockOpname::find($id);

            if($history_material_stock_opname->accounting_approval_date)
            {
                if(!$history_material_stock_opname->approval_date)
                {
                    $history_material_stock_opname->approval_date       = carbon::now();
                    $history_material_stock_opname->user_approval_id    = auth::user()->id;
                    $history_material_stock_opname->save();
                }

                $material_stock_id  = $history_material_stock_opname->material_stock_id;
                $user_id            = $history_material_stock_opname->user_id;
                $created_at         = $history_material_stock_opname->created_at;
                $note               = $history_material_stock_opname->sto_note;
                $operator           = $history_material_stock_opname->operator;
                
                $material_stock     = MaterialStock::find($material_stock_id);
                $warehouse_id       = $material_stock->warehouse_id;
                $po_detail_id       = $material_stock->po_detail_id;
                $item_code          = $material_stock->item_code;
                $item_id            = $material_stock->item_id;
                $c_bpartner_id      = $material_stock->c_bpartner_id;
                $supplier_name      = $material_stock->supplier_name;
                $document_no        = $material_stock->document_no;
                $nomor_roll         = $material_stock->nomor_roll;
                $uom                = $material_stock->uom;

                $inventory_erp      = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $sto_area  = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','STO'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','STO');
                })
                ->first();

                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                {
                    $c_order_id             = 'FREE STOCK';
                    $no_packing_list        = '-';
                    $no_invoice             = '-';
                    $c_orderline_id         = '-';
                }else
                {
                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                    ->whereNull('material_roll_handover_fabric_id')
                    ->where('po_detail_id',$po_detail_id)
                    ->first();

                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                    $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : '-');
                }

                if($history_material_stock_opname->source =='new')
                {
                    $material_stock->approval_date      = $history_material_stock_opname->accounting_approval_date;
                    $material_stock->approval_user_id   = $history_material_stock_opname->accounting_user_id;
                    $material_stock->save();

                    $summary_stock_fabrics []        = $material_stock_id;

                    FabricMaterialStockApprovalController::insertSummaryStockFabric($summary_stock_fabrics);
                }else
                {
                    if($history_material_stock_opname->operator > 0)
                    {
                        $new_stock                  = ($material_stock->stock + $history_material_stock_opname->operator);
                        $material_stock->stock      = $new_stock;
                    }else
                    {
                        $new_reserved_qty               = sprintf('%0.8f',$material_stock->reserved_qty + (-1*$history_material_stock_opname->operator));
                        $material_stock->reserved_qty   = $new_reserved_qty;
                    }
                    
                    if($history_material_stock_opname->available_stock_new <= 0)
                    {
                        if($material_stock->locator_id) $update_counter_old_locators [] = $material_stock->locator_id;
                    
                        $material_stock->locator_id     = null;
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                        $material_stock->deleted_at     = carbon::now();
                        $to_destination                 = $sto_area->id;
                    }else
                    {
                        $new_locator_id                 = ($history_material_stock_opname->locator_new_id ? $history_material_stock_opname->locator_new_id : $material_stock->locator_id);
                        $material_stock->locator_id     = $new_locator_id;
                        $material_stock->is_allocated   = false;
                        $material_stock->is_active      = true;
                        $material_stock->deleted_at     = null;
                        $to_destination                 = $new_locator_id;
                    }

                    MovementStockHistory::create([
                        'material_stock_id'     => $material_stock_id,
                        'from_location'         => $material_stock->locator_id,
                        'to_destination'        => $to_destination,
                        'status'                => 'sto',
                        'uom'                   => $material_stock->uom,
                        'qty'                   => sprintf('%0.8f',$history_material_stock_opname->available_stock_new),
                        'movement_date'         => $history_material_stock_opname->created_at,
                        'note'                  => $history_material_stock_opname->note.'. [ '.$history_material_stock_opname->sto_note.' (QTY AWAL '.$history_material_stock_opname->available_stock_old.')], di approve MM pada tanggal '.$history_material_stock_opname->approval_date->format('d/m/Y H:i:s').' oleh '.$history_material_stock_opname->mMApproval->name.', dan di approve oleh accounting pada tanggal '.$history_material_stock_opname->accounting_approval_date->format('d/m/Y H:i:s').' oleh '.$history_material_stock_opname->accountingApproval->name,
                        'user_id'               => $history_material_stock_opname->user_id
                    ]);

                    
                    $material_stock->available_qty  = $history_material_stock_opname->available_stock_new;
                    $material_stock->save();


                    $update_summary_stock []        = $material_stock->summary_stock_fabric_id;
                    $update_counter_new_locators [] = $to_destination;

                    FabricMaterialStockApprovalController::updateSummaryStockFabric(array_unique($update_summary_stock));
                }


                $is_movement_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();


                if(!$is_movement_exists)
                {
                    $material_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $created_at,
                        'updated_at'            => $created_at,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_movement_exists->updated_at = $created_at;
                    $is_movement_exists->save();
                    
                    $material_movement_id = $is_movement_exists->id;
                }
                
                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['material_stock_id',$material_stock_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['qty_movement',$operator],
                    ['date_movement',$created_at],
                ])
                ->exists();

                if(!$is_material_movement_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_stock_id'             => $material_stock_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => 1,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $operator,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => $nomor_roll,
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => 'PROSES INTEGRASI UNTUK PENYESUAIAN QTY DIKARNAKAN, '.$note,
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }
            }

            if(count($update_counter_old_locators))
            {
                $concatenate = '';
                foreach (array_unique($update_counter_old_locators) as $key => $update_counter_old_locator) 
                {
                    $concatenate    .= "'".$update_counter_old_locator."',";
                }

            }

            if(count($update_counter_new_locators))
            {
                $concatenate = '';
                foreach (array_unique($update_counter_new_locators) as $key => $update_counter_new_locator) 
                {
                    $concatenate    .= "'".$update_counter_new_locator."',";
                }
                //DB::select(db::raw("SELECT *  FROM update_conter_locator_fab_per_id(array[".$concatenate ."]);" ));
            }

            DB::commit();

            //$concatenate = substr_replace($concatenate, '', -1);
            //DB::select(db::raw("SELECT *  FROM update_conter_locator_fab_per_id(array[".$concatenate ."]);" ));
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function insertSummaryStockFabric($summary_stock_fabrics)
    {
        $material_stocks = MaterialStock::select('document_no'
            ,'c_bpartner_id'
            ,'item_code'
            ,'item_id'
            ,'uom'
            ,'warehouse_id'
        ,db::raw('sum(stock) as stock')
        ,db::raw('sum(qty_order) as qty_order'))
        ->wherein('id',$summary_stock_fabrics)
        ->groupby('document_no','c_bpartner_id','item_code','item_id','uom','warehouse_id')
        ->get();

        $summary_stocks = array();
        
        try 
        {
            DB::beginTransaction();
            
            foreach ($material_stocks as $key => $material_stock) 
            {
                $new_c_bpartner_id  = $material_stock->c_bpartner_id;
                $new_document_no    = $material_stock->document_no;
                $new_item_id        = $material_stock->item_id;
                $new_item_code      = $material_stock->item_code;
                $new_color          = $material_stock->color;
                $new_uom            = $material_stock->uom;
                $new_stock          = $material_stock->stock;
                $qty_order          = $material_stock->qty_order;
                $supplier_code      = $material_stock->supplier_code;
                $new_supplier_name  = $material_stock->supplier_name;
                $new_warehouse_id   = $material_stock->warehouse_id;
                

                if($new_document_no == 'FREE STOCK' || $new_document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($new_document_no,0, 4);
                    $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($new_document_no,0, 5);
                        $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = null;
                            $type_stock             = null;
                        }
                        
                    }
                }

                $is_exists = SummaryStockFabric::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$new_warehouse_id],
                ])
                ->first();

                if($is_exists)
                {
                    $__summary_stock_fabric_id  = $is_exists->id;
                }else
                {
                    if($new_stock >= $qty_order) $_qty_order = $new_stock;
                    else $_qty_order = $qty_order;

                    $summary_stock_fabric = SummaryStockFabric::FirstOrCreate([
                        'c_bpartner_id'     => $new_c_bpartner_id,
                        'document_no'       => $new_document_no,
                        'supplier_code'     => $supplier_code,
                        'supplier_name'     => $new_supplier_name,
                        'item_code'         => $new_item_code,
                        'item_id'           => $new_item_id,
                        'color'             => $new_color,
                        'category_stock'    => $type_stock,
                        'uom'               => $new_uom,
                        'stock'             => $new_stock,
                        'reserved_qty'      => 0,
                        'available_qty'     => $new_stock,
                        'qty_order'         => sprintf('%0.8f',$_qty_order),
                        'warehouse_id'      => $new_warehouse_id,
                    ]);

                    $__summary_stock_fabric_id = $summary_stock_fabric->id;
                }

                MaterialStock::where([
                    ['document_no',$new_document_no],
                    ['c_bpartner_id',$new_c_bpartner_id],
                    ['item_id',$new_item_id],
                    ['warehouse_id',$new_warehouse_id],
                    ['is_material_others',true],
                ])
                ->wherenull('summary_stock_fabric_id')
                ->update(['summary_stock_fabric_id' => $__summary_stock_fabric_id]);

                $summary_stocks [] =  $__summary_stock_fabric_id;
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        try 
        {
            DB::beginTransaction();
            $sum_summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$summary_stocks)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($sum_summary_stocks as $key => $sum_summary_stock) 
            {
                $summary_stock_fabric                       = SummaryStockFabric::find($sum_summary_stock->summary_stock_fabric_id);
                $qty_order                                  = $summary_stock_fabric->qty_order;
                $qty_order_reserved                         = $summary_stock_fabric->qty_order_reserved;
                $new_stock                                  = $sum_summary_stock->total_stock;
                $total_reserved                             = $sum_summary_stock->total_reserved;
                $total_available                            = $sum_summary_stock->total_available;
            
                $summary_stock_fabric->stock                = sprintf('%0.8f',$new_stock);
                $summary_stock_fabric->reserved_qty         = sprintf('%0.8f',$total_reserved);
                $summary_stock_fabric->available_qty        = sprintf('%0.8f',$total_available);
                $summary_stock_fabric->qty_order            = sprintf('%0.8f',$qty_order);
                $summary_stock_fabric->save();
            }

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
    
    static function updateSummaryStockFabric($update_summary_stock_fabrics)
    {
        try 
        {
            DB::beginTransaction();
            
            $summary_stocks = MaterialStock::select(
                'summary_stock_fabric_id',
                db::raw('sum(stock) as total_stock'),
                db::raw('sum(reserved_qty) as total_reserved'),
                db::raw('sum(available_qty) as total_available')
            )
            ->whereIn('summary_stock_fabric_id',$update_summary_stock_fabrics)
            ->groupby('summary_stock_fabric_id')
            ->get();

            foreach ($summary_stocks as $key => $summary_stock) 
            {
                $id                     = $summary_stock->summary_stock_fabric_id;
                $total_stock            = sprintf('%0.8f',$summary_stock->total_stock);
                $total_reserved_qty     = sprintf('%0.8f',$summary_stock->total_reserved);
                $total_available_qty    = sprintf('%0.8f',$summary_stock->total_available);
                

                $summary_stock_fabric_data = SummaryStockFabric::find($id);
                if($summary_stock_fabric_data)
                {
                    $summary_stock_fabric_data->stock           = $total_stock; 
                    $summary_stock_fabric_data->reserved_qty    = $total_reserved_qty; 
                    $summary_stock_fabric_data->available_qty   = $total_available_qty; 
                    $summary_stock_fabric_data->save();
                }
            
            }

            DB::commit();
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function approveSelected(Request $request)
    {
        $list_history_material_stock_opnames = array();
        $list_history_material_stock_opnames = explode(',', $request->list_history_material_stock_opname);

        foreach($list_history_material_stock_opnames as $key => $list_history_material_stock_opname)
        {
            try 
            {
                DB::beginTransaction();

                $this->itemApprove($list_history_material_stock_opname);  

                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }
           

        return response()->json(200);

    }

    public function approveAll(Request $request)
    {
        $warehouse_id                        = $request->warehouse_id;
        $user_department                     = auth::user()->department;
        $list_history_material_stock_opnames = DB::table('fabric_material_stock_approval_v')
        ->where('warehouse_id','LIKE',"%$warehouse_id%")
        ->orderby('operator','desc')
        ->get();

        
            
            foreach($list_history_material_stock_opnames as $key => $list_history_material_stock_opname)
            {    
                try 
                {
                    DB::beginTransaction();
                    
                    $this->itemApprove($list_history_material_stock_opname->history_material_stock_opname_id);
                    
                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }

            

        return response()->json(200);
    }

}
