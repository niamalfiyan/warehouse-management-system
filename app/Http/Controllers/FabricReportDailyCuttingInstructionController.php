<?php namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\CuttingInstruction;
use App\Models\DetailMaterialPreparationFabric;

class FabricReportDailyCuttingInstructionController extends Controller
{
    public function index(Request $request)
    {
        return view('fabric_report_cutting_instruction.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = ($request->warehouse? $request->warehouse : auth::user()->warehouse);
            $start_date             = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date               = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $cutting_instructions   =  DB::table('cutting_instruction_report_v')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->whereBetween('planning_date',[$start_date,$end_date])
            ->orderby('planning_date','desc');
            
            return DataTables::of($cutting_instructions)
            ->editColumn('warehouse_id',function ($cutting_instructions)
            {
                if($cutting_instructions->warehouse_id == '1000001') return 'Warehouse fabric AOI 1';
                elseif($cutting_instructions->warehouse_id == '1000011') return 'Warehouse fabric AOI 2';
            })
            ->editColumn('planning_date',function ($cutting_instructions)
            {
                return Carbon::createFromFormat('Y-m-d', $cutting_instructions->planning_date)->format('d/M/Y');
            })
            ->editColumn('qty_need',function ($cutting_instructions)
            {
                return number_format($cutting_instructions->qty_need, 4, '.', ',');
            })
            ->editColumn('total_out',function ($cutting_instructions)
            {
                return number_format($cutting_instructions->total_out, 4, '.', ',');
            })
            ->editColumn('total_relax',function ($cutting_instructions)
            {
                return number_format($cutting_instructions->total_relax, 4, '.', ',');
            })
            ->addColumn('status',function($cutting_instructions)
            {
                if($cutting_instructions->total_prepared != 0 && round($cutting_instructions->total_prepared, 4) - (round($cutting_instructions->total_rejected, 4) + round($cutting_instructions->total_approved, 4)) > 0.001)
                {
                    return 'PROGRESS';
                }
                elseif($cutting_instructions->total_prepared != 0 && $cutting_instructions->total_prepared == $cutting_instructions->total_approved) 
                {
                    return 'APPROVED';
                }
                elseif($cutting_instructions->total_prepared != 0 && round($cutting_instructions->total_prepared, 4) - (round($cutting_instructions->total_approved,4) + round($cutting_instructions->total_rejected, 4)) < 0.001 && $cutting_instructions->total_rejected > 0)
                {
                    return 'REJECT';
                }
            })
            ->addColumn('action',function($cutting_instructions){
                return view('fabric_report_cutting_instruction._action',[
                    'model'             => $cutting_instructions,
                    'detail'            => route('fabricReportDailyCuttingInstruction.detail',$cutting_instructions->id),
                    'export'            => route('fabricReportDailyCuttingInstruction.exportDetail',$cutting_instructions->id),
                ]);
            })
            ->rawColumns(['status','action'])
            ->make(true);
        }
    }

    public function detail($id)
    {
        $cutting_instruction = DB::table('cutting_instruction_report_v')->where('id',$id)->first();
        return view('fabric_report_cutting_instruction.detail',compact('cutting_instruction'));
    }

    public function dataDetail(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $article_no     = $request->article_no;
            $planning_date  = $request->planning_date;
            $style          = $request->style;
            $warehouse_id   = $request->warehouse_id;
            
            $detail_cutting_instructions = DB::select(db::raw("SELECT * FROM get_detail_ci(
                '".$article_no."',
                '".$planning_date."',
                '".$style."',
                '".$warehouse_id."'
                )"
            ));
            
            return DataTables::of($detail_cutting_instructions)
            ->editColumn('checkbox',function($data){
                return "<input type='checkbox' name='is_checked[]' id='".$data->id."' data-id='".$data->id."' class='mycheckbox' data-status='0'>";
           })
            ->editColumn('is_selected',function($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->last_status_movement == 'out' && $detail_cutting_instructions->user_receive_cutting_id == null) return "<input type='checkbox' onclick='checkbox(&apos;".$detail_cutting_instructions->id."&apos;)' name='is_checked' id='is_checked_".$detail_cutting_instructions->id."' data-id='".$detail_cutting_instructions->id."' data-status='0'>";
            })
            ->editColumn('last_movement_date',function ($detail_cutting_instructions)
            {
                return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('TANGGAL_PREPARE',function ($detail_cutting_instructions)
            {
                return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->TANGGAL_PREPARE)->format('d/M/Y H:i:s');
            })
            ->editColumn('date_receive_cutting',function ($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->date_receive_cutting) return Carbon::createFromFormat('Y-m-d H:i:s', $detail_cutting_instructions->date_receive_cutting)->format('d/M/Y H:i:s');
                else return null;
            })
            ->editColumn('piping',function($detail_cutting_instructions)
            {
                if($detail_cutting_instructions->piping) return '<span class="label label-success">Piping</span>';
                else return '<span class="label label-info">Not Piping</span>';
            })
            ->setRowAttr([
            'style' => function($detail_cutting_instructions) 
                {
                    if($detail_cutting_instructions->last_status_movement != 'out') return  'background-color: #e8e8e8;';
                    else if($detail_cutting_instructions->user_receive_cutting_id != null) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['is_selected','action','piping', 'checkbox'])
            ->make(true);
        }
    } 
    
    public function export(Request $request)
    {
        if(auth::user()->hasRole(['admin-ict-fabric','mm-staff']))
        {
            $filename = 'report_stock_fabric.csv';
        }else
        {
            $warehouse = ($request->_warehouse_id ? $request->_warehouse_id : auth::user()->warehouse);
            if($warehouse == '1000011') $filename = 'report_stock_fabric_aoi2.csv';
            else if($warehouse == '1000001') $filename = 'report_stock_fabric_aoi1.csv';
        }
    
        $file = Config::get('storage.report') . '/' . $filename;
        if(!file_exists($file)) return 'File not found';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function exportDetail(Request $request,$id)
    {
        $cutting_instruction    = DB::table('cutting_instruction_report_v')->where('id',$id)->first();
        $article_no             = $cutting_instruction->article_no;
        $planning_date          = $cutting_instruction->planning_date;
        $style                  = $cutting_instruction->style;
        $warehouse_id           = $cutting_instruction->warehouse_id;
        
        $detail_cutting_instructions = DB::select(db::raw("SELECT * FROM get_detail_ci(
            '".$article_no."',
            '".$planning_date."',
            '".$style."',
            '".$warehouse_id."'
            )"
        ));
        
        $file_name = 'cutting_instructions_'.$planning_date;
        return Excel::create($file_name,function($excel) use ($cutting_instruction,$detail_cutting_instructions)
        {
            $excel->sheet('active',function($sheet) use ($cutting_instruction,$detail_cutting_instructions)
            {
                //Header
                //JUDUL
                $sheet->mergeCells('B1:P2');
                $sheet->setCellValue('B1','CUTTING INSTRUCTION');

                //PIPING 
                $sheet->setCellValue('A7','PIPING');

                //TANGGAL PLANNING
                $sheet->mergeCells('B3:C3');
                $sheet->mergeCells('E3:F3');
                $sheet->setCellValue('B3','TANGGAL PLANNING');
                $sheet->setCellValue('D3',':');
                $sheet->setCellValue('E3',$cutting_instruction->planning_date);

                //ARTICLE
                $sheet->mergeCells('B4:C4');
                $sheet->mergeCells('E4:F4');
                $sheet->setCellValue('B4','ARTICLE');
                $sheet->setCellValue('D4',':');
                $sheet->setCellValue('E4',$cutting_instruction->article_no);

                //STYLE
                $sheet->mergeCells('B5:C5');
                $sheet->mergeCells('E5:F5');
                $sheet->setCellValue('B5','STYLE');
                $sheet->setCellValue('D5',':');
                $sheet->setCellValue('E5',$cutting_instruction->style);

                //PO BUYER
                $sheet->mergeCells('I3:J3');
                $sheet->mergeCells('L3:M3');
                $sheet->setCellValue('I3','PO BUYER');
                $sheet->setCellValue('K3',':');
                $sheet->setCellValue('L3',$cutting_instruction->po_buyer);

                //TANGGAL PREPARE
                $sheet->mergeCells('B7:C7');
                $sheet->setCellValue('B7','TANGGAL PREPARE');

                //PIC
                $sheet->mergeCells('D7:E7');
                $sheet->setCellValue('D7','PIC');

                //NO. PO SUPPLIER
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('F7','NO. PO SUPPLIER');

                //KODE ITEM
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('G7','KODE ITEM');

                //WARNA
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('H7','WARNA');

                //NOMOR ROLL
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('I7','NOMOR ROLL');

                //AKTUAL LOT
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('J7','AKTUAL LOT');

                //TOP WIDTH
                $sheet->mergeCells('K7:L7');
                $sheet->setCellValue('K7','TOP WIDTH');

                //MIDDLE WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('M7','MIDDLE WIDTH');

                //END WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('N7','END WIDTH');

                //ACTUAL WIDTH
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('O7','ACTUAL WIDTH');

                //RESERVED QTY (IN YDS)
                //$sheet->mergeCells('F7:G7');
                $sheet->setCellValue('P7','RESERVED QTY (IN YDS)');
                
                //Detail Data
                $row    = 8 ;
                $total  = 0;
                foreach($detail_cutting_instructions as $key => $i)
                {
                    //piping
                    $status ="";
                    if($i->piping) $piping='PIPING';
                    else  $piping='BUKAN PIPING';
                    
                    $sheet->setCellValue('A'.$row, $piping);
                    
                    $sheet->mergeCells('B'.$row.':C'.$row);
                    $sheet->mergeCells('K'.$row.':L'.$row);
                    $sheet->mergeCells('D'.$row.':E'.$row);
                
                    $sheet->setCellValue('B'.$row,$i->TANGGAL_PREPARE);
                    $sheet->setCellValue('D'.$row,$i->PIC);
                    $sheet->setCellValue('F'.$row,$i->NO_PO_SUPPLIER);
                    $sheet->setCellValue('G'.$row,$i->KODE_ITEM);
                    $sheet->setCellValue('H'.$row,$i->WARNA);
                    $sheet->setCellValue('I'.$row,$i->NOMOR_ROLL);
                    $sheet->setCellValue('J'.$row,$i->AKTUAL_LOT);
                    $sheet->setCellValue('K'.$row,$i->TOP_WIDTH);
                    $sheet->setCellValue('M'.$row,$i->MIDDLE_WIDTH);
                    $sheet->setCellValue('N'.$row,$i->BOTTOM_WIDTH);
                    $sheet->setCellValue('O'.$row,$i->AKTUAL_WIDTH);
                    $sheet->setCellValue('P'.$row,$i->QTY_RESERVED);

                    if ($total == 0) $total = $i->QTY_RESERVED;
                    else $total = $total + $i->QTY_RESERVED;
                
                    $row++;
                    $sheet->setCellValue('O'.($row), 'Total : ');
                    $sheet->setCellValue('P'.($row), $total);
                    $sheet->setCellValue('O'.($row+1), 'Jumlah Roll: ');
                    $sheet->setCellValue('P'.($row+1), $key+1);
                }
            });
        })->export('xlsx');
    }

    public function approve(Request $request)
    {
        $list_detail_material_preparation_fabric_ids  = explode(',',$request->list_id_approve_qc);
    
        $update = DetailMaterialPreparationFabric::whereIn('id', $list_detail_material_preparation_fabric_ids)
                                                 ->whereNull('ci_approval_status')
                                                 ->update(
                                                     ['ci_approval_status' => 'approved',
                                                     'ci_approval_date' => carbon::now(),
                                                     'ci_approval_user_id' => Auth::user()->id
                                                     ]
                                                 );

    }

    public function reject(Request $request)
    {
        //dd($request->list_id_reject_qc);
        $list_detail_material_preparation_fabric_ids  = explode(',',$request->list_id_reject_qc);
        
        //dd($list_detail_material_preparation_fabric_ids);
        $update = DetailMaterialPreparationFabric::whereIn('id', $list_detail_material_preparation_fabric_ids)
                                                 ->whereNull('ci_approval_status')
                                                 ->update(
                                                     ['ci_approval_status' => 'rejected',
                                                     'ci_approval_date' => carbon::now(),
                                                     'ci_approval_user_id' => Auth::user()->id
                                                     ]
                                                 );
                                                 
        //dd($update);

    }

}
