<?php namespace App\Http\Controllers;

use DB;
use Excel;
use Auth;
use stdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

use App\Models\Item;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\MaterialSaving;

class MasterDataMaterialSavingController extends Controller
{
    public function index()
    {
        return view('master_data_material_saving.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        { 
            
            $warehouse_id       = ($request->warehouse)?$request->warehouse : Auth::user()->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_saving    = MaterialSaving::where('warehouse_id',$warehouse_id)
            ->whereBetween('planning_date', [$start_date, $end_date])
            ->orderby('planning_date','desc');

            return DataTables::of($material_saving)
             ->editColumn('planning_date',function($material_saving)
             {
                if($material_saving->planning_date) return Carbon::parse($material_saving->planning_date)->format('d/M/y');
                else null;
             })
             ->editColumn('created_at',function($material_saving)
             {
                if($material_saving->created_at) return Carbon::parse($material_saving->planning_cutting)->format('d/M/y H:i:s');
                else null;
             })
             ->editColumn('warehouse_id',function($material_saving)
             {
                if($material_saving->warehouse_id=='1000011') return 'Warehouse fabric aoi 2';
                else if($material_saving->warehouse_id=='1000001') return 'Warehouse fabric aoi 1';
             })
             ->editColumn('upload_user_id',function($material_saving)
             {
                return ($material_saving->user)?$material_saving->user->name:'';
             })
             ->editColumn('qty_saving',function ($material_saving)
             {
                 return number_format($material_saving->qty_saving, 4, '.', ',');
             })
             ->addColumn('action', function($material_saving)
             {
                return view('_action', [
                    'edit'      => route('masterDataMaterialSaving.update', $material_saving->id),
                    'print'      => route('masterDataMaterialSaving.print', $material_saving->id),
                    'delete'    => route('masterDataMaterialSaving.delete', $material_saving->id)
                ]);
            })    
            ->make(true);
        }
    }

    public function create()
    {
        return view('master_data_material_saving.create');
    }

    public function exportFileUpload()
    {
        return Excel::create('upload_material_saving',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PLANNING_DATE');
                $sheet->setCellValue('B1','STYLE');
                $sheet->setCellValue('C1','ARTICLE');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','PO_SUPPLIER');
                $sheet->setCellValue('F1','UOM');
                $sheet->setCellValue('G1','QTY_SAVING');
                $sheet->setCellValue('H1','WAREHOUSE_PREPARATION');
                
                
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);
                $sheet->setWidth('E', 15);
                $sheet->setWidth('F', 15);
                $sheet->setWidth('G', 15);
                $sheet->setWidth('H', 15);
            });

            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function export(Request $request)
    {

        return 'fungsi belum ada';
        $warehouse_id = ($request->warehouse_id)?$request->warehouse_id:Auth::user()->warehouse;
        return Excel::create('material_saving_'.Carbon::now(),function ($excel) use ($warehouse_id){
            $excel->sheet('ACTIVE', function($sheet) use ($warehouse_id){
                $sheet->setCellValue('A1','PCD');
                $sheet->setCellValue('B1','STYLE');
                $sheet->setCellValue('C1','ARTICLE');
                $sheet->setCellValue('D1','ITEM_CODE');
                $sheet->setCellValue('E1','PO SUPPLIER');
                $sheet->setCellValue('F1','SUPPLIER_CODE');
                $sheet->setCellValue('G1','QTY_SAVING');
                $sheet->setCellValue('H1','WAREHOUSE');
                $sheet->setCellValue('I1','USER');
                
                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);
                $sheet->setWidth('E', 15);
                $sheet->setWidth('F', 15);
                $sheet->setWidth('G', 15);
                $sheet->setWidth('H', 15);
                $sheet->setWidth('I', 15);
                
                
                $material_saving = MaterialSaving::where('warehouse_id',$warehouse_id)->get();
                $row=2;
                $warehouse='';
                foreach($material_saving as $i){
                    if($i->warehouse_id=='1000011'){
                        $warehouse='AOI-2';
                    }else if($i->warehouse_id=='1000001'){
                        $warehouse='AOI-1';
                    }
                    $sheet->setCellValue('A'.$row,Carbon::parse($i->planning_date)->toDateString());
                    $sheet->setCellValue('B'.$row,$i->style);
                    $sheet->setCellValue('C'.$row,$i->article_no);
                    $sheet->setCellValue('D'.$row,$i->item_code);
                    $sheet->setCellValue('E'.$row,$i->supplier_name);
                    $sheet->setCellValue('F'.$row,$i->po_supplier);
                    $sheet->setCellValue('G'.$row,$i->qty_saving);
                    $sheet->setCellValue('H'.$row,$warehouse);
                    $sheet->setCellValue('I'.$row,($i->upload_user_id)?$i->user->name:null);
                    $row++;
                }
            });
        })->export('xlsx');

    }

    public function importFileExcel(Request $request){
        if($request->hasFile('upload_file'))
        {
            $path   = $request->file('upload_file')->getRealPath();
            $data   = Excel::selectSheets('active')->load($path,function($render){})->get();
            $result = array();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();

                    foreach ($data as $key => $value)
                    {
                        $warehouse        = (isset($value->warehouse_preparation))? $value->warehouse_preparation:null;
                        $pcd              = (isset($value->planning_date))? Carbon::parse($value->planning_date)->toDateString():null;
                        $style            = (isset($value->style))? $value->style:null;
                        $article          = (isset($value->article))? $value->article:null;
                        $uom              = (isset($value->uom))? $value->uom:null;
                        $item_code        = (isset($value->item_code))? strtoupper(trim($value->item_code)) :null;
                        $po_supplier      = (isset($value->po_supplier))? strtoupper(trim($value->po_supplier)) :null;
                        $qty_saving       = (isset($value->qty_saving))? sprintf('%0.8f', $value->qty_saving):'0';
                    
                        if($warehouse 
                        && $pcd 
                        && $style 
                        && $article 
                        && $item_code 
                        && $po_supplier 
                        && $qty_saving)
                        {
                            if($po_supplier == 'FREE STOCK')
                            {
                                $c_bpartner_id = 'FREE STOCK';
                                $supplier_name = 'FREE STOCK';
                                $c_order_id    = 'FREE STOCK';
                            }else
                            {
                                $_po_supplier = PoSupplier::where('document_no',$po_supplier)->first(); 
                                if($_po_supplier)
                                {
                                    $supplier       = Supplier::where('c_bpartner_id',$_po_supplier->c_bpartner_id)->first();
                                    $c_bpartner_id  = $_po_supplier->c_bpartner_id;
                                    $supplier_name  = ($supplier ? $supplier->supplier_name : 'SUPPLIER NAME NOT FOUND');
                                    $c_order_id     = $_po_supplier->c_order_id;
                                }else
                                {
                                    $c_bpartner_id = -1;
                                    $supplier_name = -1;
                                    $c_order_id    = -1;
                                } 
                            }
                            

                            $item       = Item::where('item_code',$item_code)->first();
                            $item_id    = ($item ? $item->item_id : -1);
                            $color      = ($item ? $item->color : -1);
                            $uom        = ($item ? $item->uom : -1);

                            if($warehouse=='AOI-1' || $warehouse=='FABRIC AOI-1' || $warehouse=='WAREHOUSE FABRIC AOI-1') $warehouse_id = '1000001';
                            else if($warehouse=='AOI-2' || $warehouse=='FABRIC AOI-2' || $warehouse=='WAREHOUSE FABRIC AOI-2')$warehouse_id = '1000011';
                            else $warehouse_id = -1;


                            if($warehouse_id != -1)
                            {

                                if($c_order_id != -1)
                                {
                                    if($item_id != -1)
                                    {
                                        $is_exists = MaterialSaving::where([
                                            'planning_date'     => $pcd,
                                            'style'             => $style,
                                            'article_no'        => $article,
                                            'item_id'           => $item_id,
                                            'c_order_id'        => $c_order_id,
                                            'c_bpartner_id'     => $c_bpartner_id,
                                            'warehouse_id'      => $warehouse_id,
                                            'qty_saving'        => $qty_saving
                                        ])
                                        ->exists();

                                        if(!$is_exists)
                                        {
                                            MaterialSaving::FirstOrCreate([
                                                'planning_date'     => $pcd,
                                                'style'             => $style,
                                                'article_no'        => $article,
                                                'item_id'           => $item_id,
                                                'uom'               => $uom,
                                                'color'             => $color,
                                                'item_code'         => $item_code,
                                                'po_supplier'       => $po_supplier,
                                                'supplier_name'     => $supplier_name,
                                                'c_bpartner_id'     => $c_bpartner_id,
                                                'c_order_id'        => $c_order_id,
                                                'warehouse_id'      => $warehouse_id,
                                                'qty_saving'        => $qty_saving,
                                                'upload_user_id'    => Auth::user()->id,
                                            ]);
                
                                            $obj                = new stdClass;
                                            $obj->pcd           = $pcd;
                                            $obj->style         = $style;
                                            $obj->uom           = $uom;
                                            $obj->article       = $article;
                                            $obj->item_code     = $item_code;
                                            $obj->po_supplier   = $po_supplier;
                                            $obj->qty_saving    = $qty_saving;
                                            $obj->warehouse     = $warehouse;
                                            $obj->is_error      = false;
                                            $obj->status        = 'SUCCESS';
                                            $result []          = $obj;
                                        }else
                                        {
                                            $obj = new stdClass;
                                            $obj->pcd           = $pcd;
                                            $obj->style         = $style;
                                            $obj->uom           = $uom;
                                            $obj->article       = $article;
                                            $obj->item_code     = $item_code;
                                            $obj->po_supplier   = $po_supplier;
                                            $obj->qty_saving    = $qty_saving;
                                            $obj->warehouse     = $warehouse;
                                            $obj->is_error      = true;
                                            $obj->status        = 'FAILED,DATA ALREADY EXISTS';
                                            $result[]           = $obj;
                                        }
                                    }else
                                    {
                                        $obj = new stdClass;
                                        $obj->pcd           = $pcd;
                                        $obj->style         = $style;
                                        $obj->article       = $article;
                                        $obj->uom           = $uom;
                                        $obj->item_code     = $item_code;
                                        $obj->po_supplier   = $po_supplier;
                                        $obj->qty_saving    = $qty_saving;
                                        $obj->warehouse     = $warehouse;
                                        $obj->is_error      = true;
                                        $obj->status        = 'FAILED,ITEM TIDAK DITEMUKAN';
                                        $result[]           = $obj;
                                    }
                                    
                                }else
                                {
                                    $obj = new stdClass;
                                    $obj->pcd           = $pcd;
                                    $obj->style         = $style;
                                    $obj->uom           = $uom;
                                    $obj->article       = $article;
                                    $obj->item_code     = $item_code;
                                    $obj->po_supplier   = $po_supplier;
                                    $obj->qty_saving    = $qty_saving;
                                    $obj->warehouse     = $warehouse;
                                    $obj->is_error      = true;
                                    $obj->status        = 'FAILED,PO SUPPLIER TIDAK DITEMUKAN';
                                    $result[]           = $obj;
                                }
                            }else
                            {
                                $obj = new stdClass;
                                $obj->pcd           = $pcd;
                                $obj->style         = $style;
                                $obj->uom           = $uom;
                                $obj->article       = $article;
                                $obj->item_code     = $item_code;
                                $obj->po_supplier   = $po_supplier;
                                $obj->qty_saving    = $qty_saving;
                                $obj->warehouse     = $warehouse;
                                $obj->is_error      = true;
                                $obj->status        = 'FAILED,WAREHOUSE TIDAK DITEMUKAN';
                                $result[]           = $obj;
                            }
                        }else
                        {
                            $obj = new stdClass;
                            $obj->pcd           = $pcd;
                            $obj->style         = $style;
                            $obj->uom           = $uom;
                            $obj->article       = $article;
                            $obj->item_code     = $item_code;
                            $obj->po_supplier   = $po_supplier;
                            $obj->qty_saving    = $qty_saving;
                            $obj->warehouse     = $warehouse;
                            $obj->is_error      = true;
                            $obj->status        = 'FAILED,SEMUA FIELD WAJIB DI ISI';
                            $result[]           = $obj;
                        }
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                return response()->json($result,200);
            }
        }
        return response()->json('import gagal, silahkan cek file anda',422);
    }

}
