<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\ItemPurchase;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialStock;
use App\Models\HistoryMaterialStocks;
use App\Models\PurchaseOrderDetail;

use App\Http\Controllers\MasterDataAutoAllocationController as AutoAllocation;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;

class AccessoriesMaterialStockController extends Controller
{
    public function index()
    {
        //$flag = Session::get('flag');
        //return view('accessories_material_stock.index',compact('html','flag'));

        $mapping_stocks = MappingStocks::select('type_stock_erp_code','type_stock','document_number')->get();
        $suppliers      = Supplier::select('supplier_code','c_bpartner_id')->get();
        return view('accessories_material_stock.create',compact('mapping_stocks','suppliers'));
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {
            $material_stocks = db::table('material_stocks_v')->where([
                ['warehouse_id',auth::user()->warehouse],
            ]);

            return DataTables::of($material_stocks)
                ->editColumn('created_at',function ($material_stocks)
                {
                    if($material_stocks->created_at)
                    {  
                        return  Carbon::createFromFormat('Y-m-d H:i:s', $material_stocks->created_at)->format('d/M/Y H:i:s');
                    }else return null;
                })
                ->editColumn('qty',function ($material_stocks)
                {
                    return number_format($material_stocks->qty, 4, '.', ',');
                })
                ->addColumn('approval_status',function ($material_stocks){
                    if($material_stocks->approval_date)
                    {
                        return '<span class="label label-success">Disetujui</span>';
                    }

                    if(!$material_stocks->approval_date)
                    {
                        return '<span class="label label-info">Menunggu di setujui</span>';
                    }

                    if($material_stocks->reject_date)
                    {
                        return '<span class="label label-danger">Ditolak</span>';
                    }
                })
                ->rawColumns(['approval_status'])
                ->make(true);
        }
    }

    public function create()
    {
        //return view('errors.503');
        $mapping_stocks = MappingStocks::select('type_stock_erp_code','type_stock','document_number')->get();
        $suppliers      = Supplier::select('supplier_code','c_bpartner_id')->get();
        return view('accessories_material_stock.create',compact('mapping_stocks','suppliers'));
    }

    public function store(Request $request)
    {
        //return view('errors.503');
        $validator =  Validator::make($request->all(), [
            'materials' => 'required|not_in:[]'
        ]);

        if($validator->passes()){
            $warehouse_id   = $request->warehouse_id;
            $items          = json_decode($request->materials);

            if(Session::has('flag'))
                Session::forget('flag');

            //return response()->json($items);
            $stock_accessories = array();
            $detail_material_stock_ids = array();
            $upload_date       = Carbon::now()->toDateTimeString();
            
            try 
            {
                DB::beginTransaction();

                foreach ($items as $key => $item) 
                {
                    $document_no            = trim(strtoupper($item->document_no));
                    $supplier_name          = trim(strtoupper($item->supplier_name));
                    $item_desc              = trim(strtoupper($item->item_desc));
                    $category               = trim(strtoupper($item->category));
                    $mapping_stock_id       = $item->mapping_stock_id;
                    $type_stock             = trim($item->type_stock);
                    $_type_stock_erp_code   = trim($item->type_stock_erp_code);
                    $c_bpartner_id          = $item->c_bpartner_id;
                    $item_id                = $item->item_id;
                    $supplier_code          = $item->supplier_code;
                    $is_error               = $item->is_error;

                    if($_type_stock_erp_code == 'SLT') $type_stock_erp_code = '1';
                    else if($_type_stock_erp_code == 'REGULER') $type_stock_erp_code = '2';
                    else if ($_type_stock_erp_code == 'PR/SR') $type_stock_erp_code = '3';
                    else if ($_type_stock_erp_code == 'MTFC') $type_stock_erp_code = '4';
                    else if ($_type_stock_erp_code == 'NB') $type_stock_erp_code = '5';
                    else if ($_type_stock_erp_code == 'ALLOWANCE') $type_stock_erp_code = '6';
                    else $type_stock_erp_code = $_type_stock_erp_code;

                    if(!$is_error)
                    {
                        $c_order_id         = ($item->c_order_id != '')? $item->c_order_id:null;
                        $po_buyer           = ($item->po_buyer !='')? $item->po_buyer: null;
                        $source             = ($item->source !='')? $item->source : null;
                        $locator_id         = $item->locator_id;
                        $item_code          = $item->item_code;
                        $uom                = $item->uom;
                        $_stock             = sprintf('%0.8f',$item->qty);

                        $_item              = Item::where(db::raw('upper(item_code)'),strtoupper($item->item_code))->first();
                        $upc_item           = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                        
                        if($item_id && $c_bpartner_id != -1)
                        {
                            $is_exists = MaterialStock::whereNull('deleted_at')
                            ->where([
                                [db::raw("upper(document_no)"),$document_no],
                                ['item_id',$item_id],
                                ['type_po', 2],
                                ['uom',$uom],
                                ['locator_id',$locator_id],
                                ['warehouse_id',$warehouse_id],
                                ['is_material_others',true],
                                ['is_closing_balance',false],
                                ['is_running_stock',false],
                            ]);

                            if($type_stock_erp_code || $type_stock_erp_code != '') $is_exists = $is_exists->where('type_stock_erp_code',$type_stock_erp_code);
                            else $is_exists = $is_exists->whereNull('type_stock_erp_code');

                            if($c_bpartner_id || $c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$c_bpartner_id);
                            else $is_exists = $is_exists->whereNull('c_bpartner_id');

                            if($c_order_id || $c_order_id != '') $is_exists = $is_exists->where('c_order_id',$c_order_id);
                            else $is_exists = $is_exists->whereNull('c_order_id');

                            if($po_buyer || $po_buyer != '') $is_exists = $is_exists->where('po_buyer',$po_buyer);
                            else $is_exists = $is_exists->whereNull('po_buyer');

                            if($source || $source != '') $is_exists = $is_exists->where('source',$source);
                            else $is_exists = $is_exists->whereNull('source');

                            $is_exists = $is_exists->first();

                            //dd($is_exists);
                            if(!$is_exists)
                            {
                                $supplier = Supplier::select('supplier_code')
                                ->where('c_bpartner_id',$c_bpartner_id)
                                ->groupby('supplier_code')
                                ->first();

                                $supplier_code = ($supplier)? trim(strtoupper($supplier->supplier_code)) : 'FREE STOCK';
                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',auth::user()->warehouse]
                                ])
                                ->first();
                                
                                if($document_no != 'FREE STOCK' && $document_no != 'FREE STOCK-CELUP')
                                {
                                    $app_env = Config::get('app.env');
                                    if($app_env == 'live')
                                    {
                                        $po_detail = PurchaseOrderDetail::where([
                                            ['c_order_id', $c_order_id],
                                            ['m_product_id', $item_id]
                                        ])->first();
                                        
                                        if($po_detail)
                                        {
                                            $po_detail_id = $po_detail->po_detail_id;
                                        }
                                        else
                                        {
                                            $po_detail_id = 'FREE STOCK';
                                        }

                                    }
                                }
                                else
                                {
                                    $po_detail_id = 'FREE STOCK';
                                    $c_order_id   = 'FREE STOCK';
                                }

                                $material_stock = MaterialStock::FirstOrCreate([
                                    'mapping_stock_id'          => $mapping_stock_id,
                                    'type_stock_erp_code'       => $type_stock_erp_code,
                                    'type_stock'                => $type_stock,
                                    'document_no'               => $document_no,
                                    'po_detail_id'              => $po_detail_id,
                                    'supplier_code'             => $supplier_code,
                                    'supplier_name'             => $supplier_name,
                                    'c_bpartner_id'             => $c_bpartner_id,
                                    'c_order_id'                => $c_order_id,
                                    'locator_id'                => $locator_id,
                                    'item_id'                   => $item_id,
                                    'item_code'                 => $item_code,
                                    'item_desc'                 => $item_desc,
                                    'category'                  => $category,
                                    'type_po'                   => 2,
                                    'warehouse_id'              => $warehouse_id,
                                    'uom'                       => $uom,
                                    'po_buyer'                  => $po_buyer,
                                    'qty_carton'                => 1,
                                    'stock'                     => $_stock,
                                    'reserved_qty'              => 0,
                                    'available_qty'             => $_stock,
                                    'is_active'                 => true,
                                    'is_material_others'        => true,
                                    'source'                    => $source,
                                    'upc_item'                  => $upc_item,
                                    'created_at'                => $upload_date,
                                    'updated_at'                => $upload_date,
                                    'user_id'                   => Auth::user()->id
                                ]);

                                $material_stock_id          = $material_stock->id;
                                $old_available              = '0';
                                $new_available              = $_stock;
                                $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
                                $curr_type_stock            = $material_stock->type_stock;
                                
                                if($material_stock->po_buyer) $note_po_buyer = ', ASAL STOCK DARI PO BUYER '.$material_stock->po_buyer;
                                else $note_po_buyer = null;

                                $detail_material_stock = DetailMaterialStock::Create([
                                                            'material_stock_id'     => $material_stock_id,
                                                            'remark'                => trim(strtoupper($item->remark)).$note_po_buyer,
                                                            'uom'                   => $uom,
                                                            'qty'                   => $_stock,
                                                            'locator_id'            => $locator_id,
                                                            'user_id'               => Auth::user()->id,
                                                            'created_at'            => $upload_date,
                                                            'updated_at'            => $upload_date,
                                                            'source'                => 'NEW'
                                                        ]);

                                $detail_material_stock_ids [] = $detail_material_stock->id;
                                
                                
                            }else
                            {
                                $material_stock_id          = $is_exists->id;
                                
                                if($is_exists->po_buyer) $note_po_buyer = ', ASAL STOCK DARI PO BUYER '.$is_exists->po_buyer;
                                else $note_po_buyer = null;

                                $detail_material_stock = DetailMaterialStock::Create([
                                                            'material_stock_id'     => $material_stock_id,
                                                            'remark'                => trim(strtoupper($item->remark)).$note_po_buyer,
                                                            'uom'                   => $uom,
                                                            'qty'                   => $_stock,
                                                            'locator_id'            => $locator_id,
                                                            'user_id'               => Auth::user()->id,
                                                            'created_at'            => $upload_date,
                                                            'updated_at'            => $upload_date,
                                                            'source'                => 'MUTASI'
                                                        ]);
                                //list id untuk approval                        
                                $detail_material_stock_ids [] = $detail_material_stock->id;
                            }
                        }
                    }
                    
                }

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            foreach($detail_material_stock_ids as $key => $detail_material_stock_id)
            {
                HistoryStock::itemApprove($detail_material_stock_id, 'detail_material_stocks');
            }

            Session::flash('flag', 'success');
            return response()->json(200);
        }else
        {
            return response()->json('tidak ada data yang diinput.',422);
        }
    }

    public function locatorPicklist(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;
        $q              = strtoupper($request->q);

        $lists = Locator::whereHas('area',function($query) use ($warehouse_id)
        {
            $query->where('name','FREE STOCK')
            ->where('warehouse',$warehouse_id);
        })
        ->where([
            ['is_active',true],
            ['rack','<>','PRINT BARCODE']
        ])
        ->where(function($query) use ($q)
        {
            $query->where('code','LIKE',"%$q%")
            ->orWhere('rack','LIKE',"%$q%");
        })
        ->orderby('rack','asc')
        ->orderby('y_row','asc')
        ->orderby('z_column','asc')
       ->paginate('5');

        return view('accessories_material_stock._locator_list',compact('lists'));
    }

    public function itemPicklist(request $request)
    {
        $q              = $request->q;
        $document_no    = $request->document_no;

        if($document_no=='null' || $document_no =='FREE STOCK' || $document_no =='FREE STOCK-CELUP')
        {
            $lists = Item::where('category','!=','FB')
            ->where(function($query) use($q){
                $query->where('item_code','like',"%$q%")
                ->orWhere('item_desc','like',"%$q%");
            })
            ->paginate(10);
        }else
        {
            $lists =  DB::table('purchase_item_accessories_v')
            ->where('document_no',$document_no)
            ->where(function($query) use($q)
            {
                $query->where('item_code','like',"%$q%")
                ->orWhere('item_desc','like',"%$q%");
            })
            ->paginate(10);
        }

        return view('accessories_material_stock._item_list',compact('lists'));
    }

    public function poSupplierPicklist(request $request)
    {
        $q      = trim(strtoupper($request->q));
       
        $lists  = db::table('po_suppliers_v')
        ->where('document_no','like',"%$q%")
        ->paginate(10);


        return view('accessories_material_stock._document_no_list',compact('lists'));
    }

    public function supplierNamePickList(request $request)
    {
        $q      = trim(strtoupper($request->q));

        $lists  = Supplier::select('supplier_name','supplier_code','c_bpartner_id')
        ->where('supplier_name','like',"%$q%")
        ->paginate(10);

        return view('accessories_material_stock._supplier_name_list',compact('lists'));
    }

    public function exportSupplier(Request $request)
    {
        $filename   = 'report_master_supplier';
        $file       = Config::get('storage.report') . '/' . e($filename).'.csv';
        
        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
        
        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }

    public function export()
    {
        return Excel::create('upload_stock',function ($excel)
        {
            $excel->sheet('active', function($sheet)
            {
                $sheet->setCellValue('A1','TYPE STOCK');
                $sheet->setCellValue('B1','SUPPLIER_CODE');
                $sheet->setCellValue('C1','PO_SUPPLIER');
                $sheet->setCellValue('D1','PO_BUYER');
                $sheet->setCellValue('E1','LOCATOR');
                $sheet->setCellValue('F1','ITEM_CODE');
                $sheet->setCellValue('G1','UOM');
                $sheet->setCellValue('H1','QTY_INPUT');
                $sheet->setCellValue('I1','SOURCE');
                $sheet->setCellValue('J1','REMARK');

                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 40);
                $sheet->setWidth('H', 15);
                $sheet->setColumnFormat(array(
                    'B' => '@'
                ));


                for ($i=2; $i <100 ; $i++) 
                {                
                  //TYPE STOCK
                  $objValidation = $sheet->getCell('A'.$i)->getDataValidation();
                  $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                  $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                  $objValidation->setAllowBlank(false);
                  $objValidation->setShowInputMessage(true);
                  $objValidation->setShowErrorMessage(true);
                  $objValidation->setShowDropDown(true);
                  $objValidation->setErrorTitle('Input error');
                  $objValidation->setError('Value tidak ada dalam list.');
                  $objValidation->setPromptTitle('Pilih Type Stock');
                  $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                  $objValidation->setFormula1('type_stock');

                  //SOURCE
                  $objValidation = $sheet->getCell('I'.$i)->getDataValidation();
                  $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                  $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
                  $objValidation->setAllowBlank(false);
                  $objValidation->setShowInputMessage(true);
                  $objValidation->setShowErrorMessage(true);
                  $objValidation->setShowDropDown(true);
                  $objValidation->setErrorTitle('Input error');
                  $objValidation->setError('Value tidak ada dalam list.');
                  $objValidation->setPromptTitle('Pilih Source');
                  $objValidation->setPrompt('Silahkan input sesuai dengan list.');
                  $objValidation->setFormula1('source');
                }

            });

            $excel->sheet('conversion', function($sheet){
                $conversions = UomConversion::get();

                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setCellValue('C1','UOM_FROM');
                $sheet->setCellValue('D1','UOM_TO');
                $sheet->setCellValue('E1','MULTIPLICATION');

                $index = 0;
                foreach ($conversions as $key => $conversion) {
                    $row = $index + 2;
                    $sheet->setCellValue('A'.$row, ($index+1));
                    $sheet->setCellValue('B'.$row, $conversion->item_code);
                    $sheet->setCellValue('C'.$row, $conversion->uom_from);
                    $sheet->setCellValue('D'.$row, $conversion->uom_to);
                    $sheet->setCellValue('E'.$row, $conversion->dividerate);
                    $index++;
                }
            });

            $excel->sheet('locator', function($sheet)
            {
                $locators = Locator::whereHas('area',function($query){
                    $query->where('name','FREE STOCK')
                    ->where('warehouse',auth::user()->warehouse);
                })
                ->where([
                    ['is_active',true],
                    ['rack','<>','PRINT BARCODE']
                ])
                ->orderby('rack','asc')
                ->orderby('y_row','asc')
                ->orderby('z_column','asc')
                ->get();

                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','CODE');

                $index = 0;
                foreach ($locators as $key => $locator) {
                    $row = $index + 2;
                    $sheet->setCellValue('A'.$row, ($index+1));
                    $sheet->setCellValue('B'.$row, $locator->code);
                    $index++;
                }
            });

            $excel->sheet('list',function($sheet)
            {
              $sheet->SetCellValue("A1", "SOURCE");
              $sheet->SetCellValue("A2", "");
              $sheet->SetCellValue("A3", "CANCEL ORDER");
              $sheet->SetCellValue("A4", "CANCEL ITEM");
              $sheet->SetCellValue("A5", "DOUBLE PR");

              $sheet->SetCellValue("B1", "TYPE STOCK");
              $sheet->SetCellValue("B2", "");
              $sheet->SetCellValue("B3", "SLT");
              $sheet->SetCellValue("B4", "REGULER");
              $sheet->SetCellValue("B5", "PR/SR");
              $sheet->SetCellValue("B6", "MTFC");
              $sheet->SetCellValue("B7", "NB");
              $sheet->SetCellValue("B8", "ALLOWANCE");
              $variantsSheet = $sheet->_parent->getSheet(3); // Variants Sheet , "0" is the Index of Variants Sheet

              $sheet->_parent->addNamedRange(
                 new \PHPExcel_NamedRange(
                    'source', $variantsSheet, 'A2:A5' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                 )
              );
              $sheet->_parent->addNamedRange(
                new \PHPExcel_NamedRange(
                   'type_stock', $variantsSheet, 'B2:B6' // You can also replace 'A2:A7' with $variantsSheet ->calculateWorksheetDimension()
                )
             );
            });

            $excel->setActiveSheetIndex(0);



        })
        ->export('xlsx');
    }

    public function import(Request $request)
    {
        //return view('errors.503');
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                foreach ($data as $key => $value) 
                {

                    $_supplier_code = trim(strtoupper($value->supplier_code));
                    if($_supplier_code == 'FREE STOCK' || $_supplier_code == null || $_supplier_code == '' || $_supplier_code == 'FREE STOCK-CELUP')
                    {
                        $c_bpartner_id = 'FREE STOCK';
                        $supplier_code = 'FREE STOCK';
                        $supplier_name = 'FREE STOCK';
                    }else
                    {
                        $supplier_code      = $_supplier_code;
                        $master_supplier    =  Supplier::where(db::raw('upper(supplier_code)'),$supplier_code)->first();

                        if($master_supplier)
                        {
                            $_c_bpartner_id = $master_supplier->c_bpartner_id;
                            $_supplier_name = $master_supplier->supplier_name;
                        }else
                        {
                            $_c_bpartner_id = null;
                            $_supplier_name = null;
                        }

                        $c_bpartner_id = $_c_bpartner_id;
                        $supplier_name = $_supplier_name;
                    }

                    $_po_supplier = trim(strtoupper($value->po_supplier));
                    if($_po_supplier == 'FREE STOCK' || $_po_supplier == 'FREE STOCK-CELUP' ||$_po_supplier == null || $_po_supplier == '')
                    {
                        if($_po_supplier == 'FREE STOCK') $document_no = 'FREE STOCK';
                        else if($_po_supplier == 'FREE STOCK-CELUP') $document_no = 'FREE STOCK-CELUP';

                        $c_order_id  = 'FREE STOCK';
                    }else
                    {
                        $po_supplier =  db::table('po_suppliers_v')
                        ->where(db::raw('upper(document_no)'),$_po_supplier)
                        ->first();

                        if($po_supplier)
                        {
                            $_document_no = $po_supplier->document_no;
                            $_c_order_id = $po_supplier->c_order_id;
                        }else
                        {
                            $_document_no = 'FREE STOCK';
                            $_c_order_id = 'FREE STOCK';
                        }

                        $c_order_id = $_c_order_id;
                        $document_no = $_document_no;
                    }

                    $type_stocks = trim($value->type_stock);
                    if($type_stocks==null || $type_stocks=='')
                    {
                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                        {
                            $mapping_stocks             = null;
                        }else
                        {
                            $get_4_digit_from_beginning = substr($document_no,0, 4);
                            $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();

                            if($mapping_stocks)
                            {
                                $get_5_digit_from_beginning = substr($document_no,0, 5);
                                $mapping_stocks             = MappingStocks::select('type_stock','type_stock_erp_code')->where('document_number',$get_4_digit_from_beginning)->first();
                            }
                        }

                        if($mapping_stocks != null)
                        {
                            $type_stock          = $mapping_stocks->type_stock;
                            $type_stock_erp_code = $mapping_stocks->type_stock_erp_code;
                        }else
                        {
                            $type_stock          = 'REGULER';
                            $type_stock_erp_code = '2';
                        }
                    }else
                    {
                        $type_stock          = $type_stocks;
                        $type_stock_erp_code = $this->getTypeStockErpCode($type_stock);
                    }

                    $locator = Locator::where('code',$value->locator)
                    ->whereHas('area',function($query){
                        $query->where('warehouse',auth::user()->warehouse);
                    })
                    ->first();

                    if($locator)
                    {
                        $locator_id     = $locator->id;
                        $locator_name   = $locator->code;
                    }else
                    {
                        $locator_id     = null;
                        $locator_name   = null;
                    }

                    $item = Item::where(db::raw('upper(item_code)'),trim(strtoupper($value->item_code)))->first();

                    $obj = new stdClass();
                    $obj->supplier_code       = $supplier_code;
                    $obj->c_bpartner_id       = $c_bpartner_id;
                    $obj->c_order_id          = $c_order_id;
                    $obj->document_no         = $document_no;
                    $obj->po_buyer            = $value->po_buyer;
                    $obj->supplier_name       = $supplier_name;
                    $obj->locator_id          = $locator_id;
                    $obj->locator_name        = $locator_name;
                    $obj->source              = $value->source;
                    $obj->qty                 = $value->qty_input;
                    $obj->item_id             = ($item ?$item->item_id:null);
                    $obj->item_code           = ($item ?$item->item_code:null);
                    $obj->item_desc           = ($item ?$item->item_desc:null);
                    $obj->category            = ($item ?$item->category:null);
                    $obj->uom                 = ($item ?$item->uom:null);
                    $obj->remark              = $value->remark;
                    $obj->type_stock          = $type_stock;
                    $obj->type_stock_erp_code = $type_stock_erp_code;
                    $obj->mapping_stock_id    = null;
                    $obj->is_error            = false;
                    $obj->is_warning          = false;

                    if(!is_numeric($obj->qty))
                    {
                        $obj->is_error  = true;
                        $obj->remark    = 'QTY BUKAN ANGKA !,CEK KEMBALI QTY YANG DIMASUKAN';
                    }
                    //cek apakah locator ditemukan atau tidak
                    if($obj->locator_id == null)
                    {
                        $obj->is_error  = true;
                        $obj->remark    = 'LOKASI RAK '.$value->locator.' TIDAK DITEMUKAN !';
                    }

                    // if($obj->document_no != 'FREE STOCK' && $obj->document_no != 'FREE STOCK-CELUP')
                    // {
                    //     $obj->is_error  = true;
                    //     $obj->remark    = 'PO SUPPLIER YANG DAPAT DITERIMA HANYALAH FREE STOCK !';
                    // }

                    if($obj->locator_id == null)
                    {
                        $obj->is_error  = true;
                        $obj->remark    = 'LOKASI RAK '.$value->locator.' TIDAK DITEMUKAN !';
                    }

                    //cek apakah po buyer ada
                    
                    if($item)
                    {
                        if($c_bpartner_id)
                        {
                            if($obj->document_no != 'FREE STOCK' && $obj->document_no != 'FREE STOCK-CELUP')
                            {
                                $is_item_purchase = ItemPurchase::where([
                                    ['document_no',$obj->document_no],
                                    ['item_code',$obj->item_code],
                                ])
                                ->exists();

                                if(!$is_item_purchase)
                                {
                                    $obj->is_error              = true;
                                    $obj->remark                = 'TIDAK ADA PEMBELIAN ITEM INI DARI PO SUPPLIER YANG DIPILIH. SILAHKAN HUBUNGI ICT';
                                }
                            }
                        }else
                        {
                            $obj->is_error              = true;
                            $obj->remark                = 'KODE SUPPLIER TIDAK DI TEMUKAN DI DATABASE. SILAHKAN HUBUNGI ICT';
                        }
                    }else
                    {
                        $obj->item_id               = null;
                        $obj->item_code             = $value->item_code;
                        $obj->item_desc             = null;
                        $obj->category              = null;
                        $obj->uom                   = $value->uom;
                        $obj->qty                   = $value->qty_input;
                        $obj->is_error              = true;
                        $obj->remark                = 'KODE ITEM TIDAK DI TEMUKAN DI DATABASE. SILAHKAN HUBUNGI ICT';
                    }
                    
                    $array [] = $obj;
                }
                return response()->json($array,200);
            }else{
                return response()->json('import gagal, silahkan cek file anda',422);
            }


        }

    }

    static function updateStock()
    {
        $upload_date = carbon::now()->todatetimestring();
        //return view('errors.503');
        $material_arrivals = MaterialArrival::select('document_no','c_bpartner_id','item_code','c_order_id','warehouse_id','uom',db::raw('sum(qty_upload) as qty_upload'))
        ->where([
            ['is_subcont',false],
            ['po_buyer',''],
        ])
        ->groupby('document_no','c_bpartner_id','item_code','c_order_id','warehouse_id','uom')
        ->get();

        foreach ($material_arrivals as $key => $material_arrival) 
        {
            $conversion = UomConversion::where([
                ['item_code',$material_arrival->item_code],
                ['uom_from',trim($material_arrival->uom)]
            ])
            ->first();

            if($conversion)
            {
                $dividerate     = $conversion->dividerate;
                $uom_conversion = $conversion->uom_to;
            }else
            {
                $dividerate     = 1;
                $uom_conversion = $material_arrival->uom;
            }

            $material_stock = MaterialStock::where([
                ['c_order_id',$material_arrival->c_order_id],
                ['c_bpartner_id',$material_arrival->c_bpartner_id],
                ['document_no',$material_arrival->document_no],
                ['item_code',$material_arrival->item_code],
                ['warehouse_id',$material_arrival->warehouse_id],
                ['is_closing_balance',false],
                ['is_stock_on_the_fly',false],
                ['is_running_stock',false],
            ])
            ->first();

            if($material_stock)
            {
                $qty_conversion     = sprintf('%0.8f',$material_arrival->qty_upload * $dividerate);
                $reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                $new_available_qty  = sprintf('%0.8f',$qty_conversion - $reserved_qty);

                $material_stock->stock          = $qty_conversion;
                $material_stock->available_qty  = $new_available_qty;

                if($new_available_qty > 0)
                {
                    $material_stock->is_allocated = false;
                    $material_stock->is_active = true;
                }

                $material_stock->save();

                $detail_material_arrivals = MaterialArrival::select('id','uom','qty_upload')
                ->where([
                    ['c_order_id',$material_arrival->c_order_id],
                    ['c_bpartner_id',$material_arrival->c_bpartner_id],
                    ['item_code',$material_arrival->item_code],
                    ['warehouse_id',$material_arrival->warehouse_id],
                ])
                ->groupby('id','uom','qty_upload')
                ->get();

                foreach ($detail_material_arrivals as $key => $detail_material_arrival) 
                {
                    $is_exists = DetailMaterialStock::where('material_arrival_id',$detail_material_arrival->id)->exists();

                    if(!$is_exists)
                    {
                        $qty_conversion_2 = sprintf('%0.8f',$detail_material_arrival->qty_upload * $dividerate);
                        DetailMaterialStock::firstorcreate([
                            'material_arrival_id'   => $detail_material_arrival->id,
                            'material_stock_id'     => $material_stock->id,
                            'uom'                   => $uom_conversion,
                            'qty'                   => sprintf('%0.8f',$qty_conversion_2),
                            'remark'                => 'RECEIVING FROM PF',
                            'user_id'               => 1,
                            'created_at'            => $upload_date,
                            'updated_at'            => $upload_date
                        ]);
                    }
                }
            }
        }
    }

    static function updateTypeStock()
    {
        //return view('errors.503');
        $material_stocks = MaterialStock::get();

        foreach ($material_stocks as $key => $material_stock)
        {
            $document_no = $material_stock->document_no;
            if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
            {
                $mapping_stock_id       = null;
                $type_stock_erp_code    = '2';
                $type_stock             = 'REGULER';
            }else
            {
                $get_4_digit_from_beginning = substr($document_no,0, 4);
                $_mapping_stock_4_digt      = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();

                if($_mapping_stock_4_digt)
                {
                    $mapping_stock_id       = $_mapping_stock_4_digt->id;
                    $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                    $type_stock             = $_mapping_stock_4_digt->type_stock;
                }else
                {
                    $get_5_digit_from_beginning = substr($document_no,0, 5);
                    $_mapping_stock_5_digt      = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();

                    if($_mapping_stock_5_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_5_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_5_digt->type_stock;
                    }else
                    {
                        $mapping_stock_id = null;
                        $type_stock_erp_code = null;
                        $type_stock = null;
                    }
                    
                }
            }

            MaterialStock::where('material_stock_id',$material_stock->id)
            ->update([
                'mapping_stock_id' =>  $mapping_stock_id,
                'type_stock_erp_code' =>  $type_stock_erp_code,
                'type_stock' =>  $type_stock
             ]);

            HistoryMaterialStocks::where('material_stock_id',$material_stock->id)
            ->update([
               'mapping_stock_id' =>  $mapping_stock_id,
               'type_stock_erp_code' =>  $type_stock_erp_code,
               'type_stock' =>  $type_stock
            ]);
        }
    }

    private function getTypeStockErpCode($type_stock)
    {
        $result = null;
        
        if($type_stock == 'SLT') $result = 1;
        else if($type_stock == 'REGULER') $result = 2;
        else if($type_stock == 'PR/SR') $result = 3;
        else if($type_stock == 'MTFC') $result = 4;
        else if($type_stock == 'NB') $result = 5;
        else if($type_stock == 'ALLOWANCE') $result = 6;
        
        return $result;
    }
}
