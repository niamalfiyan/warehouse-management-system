<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Validator;
use stdClass;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\User;
use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Temporary;
use App\Models\ReroutePoBuyer;
use App\Models\AutoAllocation;
use App\Models\AllocationItem;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\CuttingInstruction;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\MaterialMovementLine;
use App\Models\HistoryReroutePoBuyer;
use App\Models\MaterialPlanningFabric;
use App\Models\HistoryPreparationFabric;
use App\Models\MaterialPreparationFabric;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;


class MasterDataReroutePoBuyerController extends Controller
{
    public function index()
    {
        return view('master_data_reroute_buyer.index');
    }

    public function data(Request $request)
    {
        if ($request->ajax()) 
        {
            $reroutes = ReroutePoBuyer::orderBy('created_at','desc');

            return DataTables::of($reroutes)
             ->EditColumn('is_prefix',function ($reroutes){
                if($reroutes->is_prefix ) return 'GANTI PREFIX';
                else return 'REROUTE';
            })
            ->editColumn('is_prefix',function ($reroutes){
                if ($reroutes->is_prefix) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
            	else return null;
            })
            ->addColumn('action', function($reroutes) 
            {
                return view('master_data_reroute_buyer._action',[
                    'model' => $reroutes,
                    'refresh' => route('masterDataReroute.refresh',$reroutes->id),
                ]);
            })
            ->rawColumns(['action','is_prefix'])
            ->make(true);
        }

    }

    public function BuyerPickList(Request $request)
    {
        $q = $request->q;
        $lists = PoBuyer::select('lc_date','season','po_buyer','type_stock','brand')
        ->where('po_buyer','like',"%$q%")
        ->groupby('lc_date','season','po_buyer','type_stock','brand')
        ->paginate('10');

        return view('master_data_reroute_buyer._buyer_list',compact('lists'));
    }

    public function store(Request $request)
    {
        //return response()->json($request->all());
        $old_buyer  = $request->old_buyer;
        $new_buyer  = $request->new_buyer;
        $note       = trim(strtoupper($request->note));
        $is_prefix  = ($request->exists('is_prefix')) ? true:false;

        $is_exists = ReroutePoBuyer::where([
            ['old_po_buyer',$old_buyer],
            ['new_po_buyer',$new_buyer],
        ])
        ->exists();

        if($is_exists) return response()->json('Data already exists',422); 
        
        try 
        {
            DB::beginTransaction();
            ReroutePoBuyer::FirstOrCreate([
                'old_po_buyer'  => $old_buyer,
                'new_po_buyer'  => $new_buyer,
                'user_id'       => auth::user()->id,
                'note'          => $note,
                'is_prefix'     => $is_prefix,
            ]);

            PoBuyer::where('po_buyer',$old_buyer)
            ->update([
                'is_reroute' => true
            ]);
            DB::commit();

        
       } catch (Exception $e) 
       {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
       }

        if($is_prefix) $note = 'UBAH PREFIX DARI '.$old_buyer.' KE '.$new_buyer;
        else $note = 'REROUTE DARI '.$old_buyer.' KE '.$new_buyer;

        $movement_date = carbon::now()->toDateTimeString();
        $this->copyFromOldToNew($old_buyer,$new_buyer,null,$movement_date,$note);
        $this->removeOldPoBuyer($old_buyer,$new_buyer,null,$note);
        $this->deletePlanningFabrics($old_buyer);
        return response()->json(200);
    }

    public function refresh($id)
    {
        $reroute = ReroutePoBuyer::find($id);
        $old_buyer  = $reroute->old_po_buyer;
        $new_buyer  = $reroute->new_po_buyer;
        $is_prefix  = $reroute->is_prefix;
        $start_date = MaterialPlanningFabric::where('po_buyer',$old_buyer)->min('planning_date');
        $end_date   = MaterialPlanningFabric::where('po_buyer',$old_buyer)->max('planning_date');

        if($is_prefix) $note = 'UBAH PREFIX DARI '.$old_buyer.' KE '.$new_buyer;
        else $note = 'REROUTE DARI '.$old_buyer.' KE '.$new_buyer;

        $movement_date = carbon::now()->toDateTimeString();
        $this->copyFromOldToNew($old_buyer,$new_buyer,null,$movement_date,$note);
        $this->removeOldPoBuyer($old_buyer,$new_buyer,null,$note);
        $this->deletePlanningFabrics($old_buyer);
        $this->storeCuttingInstructionHeaderReport($start_date,$end_date);
        return response()->json(200);
    }

    static function copyFromOldToNew($old_buyer,$new_buyer,$item_code,$movement_date,$note)
    {
        $user = User::where('nik','9901')->first();

        $old_preparations = MaterialPreparation::where([
            ['po_buyer',$old_buyer],
            ['is_reroute',false],
            
        ]);
        
        if($item_code) $old_preparations = $old_preparations->where('item_code',$item_code);

        $old_preparations   = $old_preparations->get();
        $old_array          = [];
        $new_array          = [];
        $movement_date      = carbon::now()->toDateTimeString();

        MaterialRequirement::where([
            ['po_buyer',$old_buyer],
            ['is_upload_manual', true],
        ])
        ->when($item_code,function($q) use ($item_code)
        {
            $q->where('item_code',$item_code);
        })
        ->update([
            'po_buyer' => $new_buyer
        ]);

        
        try 
        {
            DB::beginTransaction();

            $concatenate = '';

            foreach ($old_preparations as $key => $old_preparation) 
            {
                $old_material_preparation_id    = $old_preparation->id;
                $new_po_buyer                   = $new_buyer;
                $new_item_code                  = $old_preparation->item_code;
                $new_warehouse                  = $old_preparation->warehouse;

                $new_article_no                     = $old_preparation->article_no;
                $new_style                          = $old_preparation->style;
                $new_job_order                      = $old_preparation->job_order;
                $new_uom_conversion                 = $old_preparation->uom_conversion;
                $new_material_stock_id              = $old_preparation->material_stock_id;
                $new_auto_allocation_id             = $old_preparation->auto_allocation_id;
                $new_qty_conversion                 = $old_preparation->qty_conversion;
                $new_qty_reconversion               = $old_preparation->qty_reconversion;
                $new_qc_status                      = $old_preparation->qc_status;
                $new_total_carton                   = $old_preparation->total_carton;
                $new_type_po                        = $old_preparation->type_po;
                $new_is_allocation                  = $old_preparation->is_allocation;
                $new_warehouse                      = $old_preparation->warehouse;
                $new_item_desc                      = $old_preparation->item_desc;
                $new_category                       = $old_preparation->category;
                $new_po_detail_id                   = $old_preparation->po_detail_id;
                $new_first_print_date               = $old_preparation->first_print_date;
                $new__style                         = $old_preparation->_style;
                $new_first_inserted_to_locator_date = $old_preparation->first_inserted_to_locator_date;
                $new_reprint_date                   = $old_preparation->reprint_date;
                $new_reprint_user_id                = $old_preparation->reprint_user_id;
                $new_is_additional                  = $old_preparation->is_additional;
                $new_is_moq                         = $old_preparation->is_moq;
                $new_last_status_movement           = $old_preparation->last_status_movement;
                $new_last_movement_date             = $old_preparation->last_movement_date;
                $new_last_locator_id                = $old_preparation->last_locator_id;
                $new_last_user_movement_id          = $old_preparation->last_user_movement_id;
                $new_remark                         = $old_preparation->remark;
                $new_c_order_id                     = $old_preparation->c_order_id;
                $new_c_bpartner_id                  = $old_preparation->c_bpartner_id;
                $new_document_no                    = $old_preparation->document_no;
                $new_supplier_name                  = $old_preparation->supplier_name;
                $new_is_movement_adjustment         = $old_preparation->is_movement_adjustment;
                $new_user_id                        = $old_preparation->user_id;
                $new_item_id                        = $old_preparation->item_id;
                $new_is_reduce                      = $old_preparation->is_reduce;
                $new_ict_log                        = $note;
                $new_is_backlog                     = $old_preparation->is_backlog;
                $new_barcode                        = $old_preparation->barcode;
                $new_referral_code                  = $old_preparation->referral_code;
                $new_sequence                       = $old_preparation->sequence;
                $new_deleted_at                     = $old_preparation->deleted_at;
                $old_array []                       = $old_material_preparation_id;

                $_new_qty_conversion        = sprintf('%0.8f',$new_qty_conversion);
                $_new_qty_reconversion      = sprintf('%0.8f',$new_qty_reconversion);

                $is_exists = MaterialPreparation::where([
                    ['material_stock_id',$new_material_stock_id],
                    ['auto_allocation_id',$new_auto_allocation_id],
                    ['c_order_id',$new_c_order_id],
                    ['po_buyer',$new_po_buyer],
                    ['item_id',$new_item_id],
                    ['uom_conversion',$new_uom_conversion],
                    ['warehouse',$new_warehouse],
                    ['article_no',$new_article_no],
                    ['style',$new_style],
                    ['qty_conversion',$_new_qty_conversion],
                    ['is_backlog',$new_is_backlog],
                ])
                ->first();

                if(!$is_exists)
                {
                    $detail_material_preparations = DetailMaterialPreparation::where('material_preparation_id',$old_material_preparation_id)->get();
                    $new_material_preparation = MaterialPreparation::Create([
                        'barcode'                        => $new_barcode,
                        'auto_allocation_id'             => $new_auto_allocation_id,
                        'material_stock_id'              => $new_material_stock_id,
                        'referral_code'                  => $new_referral_code,
                        'sequence'                       => $new_sequence,
                        'item_id'                        => $new_item_id,
                        'c_order_id'                     => $new_c_order_id,
                        'c_bpartner_id'                  => $new_c_bpartner_id,
                        'supplier_name'                  => $new_supplier_name,
                        'document_no'                    => $new_document_no,
                        'po_buyer'                       => $new_po_buyer,
                        'uom_conversion'                 => $new_uom_conversion,
                        'qty_conversion'                 => $_new_qty_conversion,
                        'qty_reconversion'               => $_new_qty_reconversion,
                        'job_order'                      => $new_job_order,
                        'style'                          => $new_style,
                        'article_no'                     => $new_article_no,
                        'warehouse'                      => $new_warehouse,
                        'item_code'                      => $new_item_code,
                        'item_desc'                      => $new_item_desc,
                        'qc_status'                      => $new_qc_status,
                        'category'                       => $new_category,
                        'po_detail_id'                   => $new_po_detail_id,
                        'is_backlog'                     => $new_is_backlog,
                        'total_carton'                   => $new_total_carton,
                        'type_po'                        => $new_type_po,
                        'user_id'                        => $new_user_id,
                        'last_status_movement'           => $new_last_status_movement,
                        'last_locator_id'                => $new_last_locator_id,
                        'last_movement_date'             => $new_last_movement_date,
                        'created_at'                     => $new_last_movement_date,
                        'updated_at'                     => $new_last_movement_date,
                        'first_print_date'               => $new_first_print_date,
                        'first_inserted_to_locator_date' => $new_first_inserted_to_locator_date,
                        '_style'                         => $new__style,
                        'last_user_movement_id'          => $new_last_user_movement_id,
                        'ict_log'                        => $new_ict_log,
                        'is_reroute'                     => false,
                        'is_reduce'                      => $new_is_reduce,
                        'deleted_at'                     => $new_deleted_at,
                    ]);

                    $new_material_preparation_id    = $new_material_preparation->id;
                    $new_barcode                    = $new_material_preparation->barcode;
                    $new_array []                   = $new_material_preparation_id;

                    foreach ($detail_material_preparations as $key_2 => $detail_material_preparation) 
                    {
                        $new_material_arrival_id    = $detail_material_preparation->material_arrival_id;
                        $new_allocation_item_id     = $detail_material_preparation->allocation_item_id;

                        DetailMaterialPreparation::FirstOrCreate([
                            'material_preparation_id'   => $new_material_preparation_id,
                            'material_arrival_id'       => $new_material_arrival_id,
                            'allocation_item_id'        => $new_allocation_item_id,
                            'user_id'                   => $user->id
                        ]);


                    }

                    Temporary::Create([
                        'barcode'       => $new_po_buyer,
                        'status'        => 'mrp',
                        'user_id'       => $user->id,
                        'created_at'    => $new_last_movement_date,
                        'updated_at'    => $new_last_movement_date,
                    ]);

                    $concatenate .= "'" .$new_barcode."',";

                }else $new_material_preparation = $is_exists;

                if($new_material_preparation->save())
                {
                    //copy from old movement
                    $old_material_movements = MaterialMovement::where('status','!=','reroute')
                    ->wherein('id',function($query) use($old_material_preparation_id) {
                        $query->select('material_movement_id')
                        ->from('material_movement_lines')
                        ->where('material_preparation_id',$old_material_preparation_id)
                        ->groupby('material_movement_id');
                    })
                    ->get();

                    foreach ($old_material_movements as $key => $old_material_movement) 
                    {
                        $old_movement_id            = $old_material_movement->id;
                        $new_from_location          = $old_material_movement->from_location;
                        $new_from_location_erp_id   = $old_material_movement->fromLocator->area->erp_id;
                        $new_to_destination         = $old_material_movement->to_destination;
                        $new_to_destination_erp_id  = $old_material_movement->destinationLocator->area->erp_id;
                        $new_no_packing_list        = $old_material_movement->no_packing_list;
                        $new_no_invoice             = $old_material_movement->no_invoice;
                        $new_po_buyer               = $new_buyer;
                        $new_is_active              = $old_material_movement->is_active;
                        $new_status                 = $old_material_movement->status;
                        $new_is_additional          = $old_material_movement->is_additional;
                        $new_is_integrate           = $old_material_movement->is_integrate;
                        $new_created_at             = $old_material_movement->created_at;
                        $new_updated_at             = $old_material_movement->updated_at;

                        $is_new_materal_movement_exists = MaterialMovement::where([
                            ['from_location',$new_from_location],
                            ['to_destination',$new_to_destination],
                            ['from_locator_erp_id',$new_from_location_erp_id],
                            ['to_locator_erp_id',$new_to_destination_erp_id],
                            ['no_packing_list',$new_no_packing_list],
                            ['no_invoice',$new_no_invoice],
                            ['po_buyer',$new_po_buyer],
                            ['status',$new_status],
                            ['is_integrate',$new_is_integrate],
                            ['is_active',$new_is_active],
                        ])
                        ->first();

                        if(!$is_new_materal_movement_exists)
                        {
                            $new_material_movement = MaterialMovement::Create([
                                'from_location'         => $new_from_location,
                                'to_destination'        => $new_to_destination,
                                'from_locator_erp_id'   => $new_from_location_erp_id,
                                'to_locator_erp_id'     => $new_to_destination_erp_id,
                                'po_buyer'              => $new_po_buyer,
                                'no_packing_list'       => $new_no_packing_list,
                                'no_invoice'            => $new_no_invoice,
                                'is_integrate'          => $new_is_integrate,
                                'is_active'             => $new_is_active,
                                'is_additional'         => $new_is_additional,
                                'status'                => $new_status,
                                'created_at'            => $new_created_at,
                                'updated_at'            => $new_updated_at,
                            ]);

                            $new_material_movement_id = $new_material_movement->id;
                        }else $new_material_movement_id = $is_new_materal_movement_exists->id;
                        

                        $old_material_movement_lines = MaterialMovementLine::where([
                            ['material_movement_id',$old_movement_id],
                            ['material_preparation_id',$old_material_preparation_id],
                        ])
                        ->get();

                        foreach ($old_material_movement_lines as $key_2 => $old_material_movement_line) 
                        {
                            $new_c_order_id                                 = $old_material_movement_line->c_order_id;
                            $new_c_orderline_id                             = $old_material_movement_line->c_orderline_id;
                            $new_c_bpartner_id                              = $old_material_movement_line->c_bpartner_id;
                            $new_supplier_name                              = $old_material_movement_line->supplier_name;
                            $new_item_id_source                             = $old_material_movement_line->item_id_source;
                            $new_item_id                                    = $old_material_movement_line->item_id;
                            $new_item_code                                  = $old_material_movement_line->item_code;
                            $new_is_inserted_to_material_movement_per_size  = $old_material_movement_line->is_inserted_to_material_movement_per_size;
                            $new_type_po                                    = $old_material_movement_line->type_po;
                            $new_date_receive_on_destination                = $old_material_movement_line->date_receive_on_destination;
                            $new_date_movement                              = $old_material_movement_line->date_movement;
                            $new_is_active_line                             = $old_material_movement_line->is_active;
                            $new_is_inserted_to_subcont                     = $old_material_movement_line->is_inserted_to_subcont;
                            $new_is_exclude                                 = $old_material_movement_line->is_exclude;
                            $new_user_id                                    = $old_material_movement_line->user_id;
                            $new_parent_id                                  = $old_material_movement_line->parent_id;
                            $new_qty_movement                               = $old_material_movement_line->qty_movement;
                            $new_warehouse_id                               = $old_material_movement_line->warehouse_id;
                            $new_note                                       = $note;
                            $new_uom_conversion                             = $old_material_movement_line->uom_movement;
                            
                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                'material_movement_id'              => $new_material_movement_id,
                                'material_preparation_id'           => $new_material_preparation->id,
                                'item_id'                           => $new_item_id,
                                'item_code'                         => $new_item_code,
                                'type_po'                           => $new_type_po,
                                'qty_movement'                      => $new_qty_movement,
                                'date_movement'                     => $new_date_movement,
                                'is_active'                         => $new_is_active_line,
                                'is_integrate'                      => false,
                                'parent_id'                         => $new_parent_id,
                                'note'                              => $new_note,
                                'user_id'                           => $new_user_id
                            ])
                            ->exists();

                            if(!$is_line_exists)
                            {
                                $new_material_movement_line = MaterialMovementLine::Create([
                                    'material_movement_id'                      => $new_material_movement_id,
                                    'material_preparation_id'                   => $new_material_preparation->id,
                                    'c_order_id'                                => $new_c_order_id,
                                    'c_orderline_id'                            => $new_c_orderline_id,
                                    'c_bpartner_id'                             => $new_c_bpartner_id,
                                    'supplier_name'                             => $new_supplier_name,
                                    'item_id'                                   => $new_item_id,
                                    'item_id_source'                            => $new_item_id_source,
                                    'is_integrate'                              => false,
                                    'is_exclude'                                => $new_is_exclude,
                                    'item_code'                                 => $new_item_code,
                                    'type_po'                                   => $new_type_po,
                                    'uom_movement'                              => $new_uom_conversion,
                                    'qty_movement'                              => $new_qty_movement,
                                    'date_movement'                             => $new_date_movement,
                                    'created_at'                                => $new_date_movement,
                                    'updated_at'                                => $new_date_movement,
                                    'warehouse_id'                              => $new_warehouse_id,
                                    'nomor_roll'                                => '-',
                                    'date_receive_on_destination'               => $new_date_receive_on_destination,
                                    'is_inserted_to_material_movement_per_size' => $new_is_inserted_to_material_movement_per_size,
                                    'is_active'                                 => $new_is_active_line,
                                    'parent_id'                                 => $new_parent_id,
                                    'note'                                      => $new_note,
                                    'user_id'                                   => $new_user_id
                                ]);

                                $new_material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                $new_material_preparation->save();
                            }
                            
                        }

                        if($old_material_movement->is_integrate == false && ($old_material_movement->status == 'out' || $old_material_movement->status == 'out-handover' || $old_material_movement->status == 'reverse' || $old_material_movement->status == 'out-subcont'))
                        {
                            $old_material_movement->is_integrate = true;
                            $old_material_movement->save();
                        }
                    }
                    
                }
                
            
            }

            

            
            
            DB::commit();
        
       } catch (Exception $e) 
       {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
       }

        $concatenate = substr_replace($concatenate, '', -1);
        if($concatenate !='')
        {
            DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
            DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
        }

        

        //update last locator 
        Locator::where('last_po_buyer',$old_buyer)->update([
            'last_po_buyer' => $new_buyer
        ]);

        //update auto allocation
        AutoAllocation::where('po_buyer',$old_buyer)->update([
            'po_buyer'      => $new_buyer,
            'old_po_buyer'  => $old_buyer,
            'is_reroute'    => true,
        ]);

        //update material ready preparation
        MaterialReadyPreparation::whereIn('material_arrival_id',function($query) use($old_buyer)
        {
            $query->select('id')
            ->from('material_arrivals')
            ->where('po_buyer',$old_buyer);
        })
         ->update([
            'po_buyer'      => $new_buyer
        ]);

        //update material arrival
        MaterialArrival::where('po_buyer',$old_buyer)->update([
            'po_buyer'      => $new_buyer
        ]);

        //update allocation
        $allocation_items = AllocationItem::whereNotNull('po_buyer')
        ->where('po_buyer',$old_buyer);
        
        if($item_code) $allocation_items = $allocation_items->where('item_code',$item_code);
        $allocation_items = $allocation_items->get();

        try 
        {
            DB::beginTransaction();
            
            foreach ($allocation_items as $key => $allocation_item) 
            {
                $item_code      = $allocation_item->item_code;
                $style          = $allocation_item->style;
                $article_no     = $allocation_item->article_no;
                
                $material_requirement = MaterialRequirement::where([
                    ['po_buyer',$new_buyer],
                    ['item_code',$item_code],
                    ['style',$style],
                    ['article_no',$article_no],
                ])
                ->first();
                
                $allocation_item->po_buyer              = $new_buyer;
                $allocation_item->style                 = ($material_requirement)? $material_requirement->style : null;
                $allocation_item->job                   = ($material_requirement)? $material_requirement->job_order : null;
                $allocation_item->article_no            = ($material_requirement)? $material_requirement->article_no : null;
                $allocation_item->is_reroute            = true;
                $allocation_item->old_po_buyer_reroute  = $old_buyer;
                $allocation_item->save();
            
            }

            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

    }

    static function removeOldPoBuyer($old_buyer,$new_buyer,$item_code,$note)
    {
        $user           = User::where('nik','9901')->first();
        $movement_date  = carbon::now()->toDateTimeString();
        $material_preparations = MaterialPreparation::where([
            ['po_buyer',$old_buyer],
            ['is_reroute',false],
        ]);
        
        if($item_code) $material_preparations = $material_preparations->where('item_code',$item_code);
        
        $material_preparations = $material_preparations->get();

        try 
        {
            DB::beginTransaction();
            foreach ($material_preparations as $key => $material_preparation) 
            {
                $barcode                        = $material_preparation->barcode;
                $old_material_preparation_id    = $material_preparation->id;
                $old_c_order_id                 = $material_preparation->c_order_id;
                $old_c_bpartner_id              = $material_preparation->c_bpartner_id;
                $old_supplier_name              = $material_preparation->supplier_name;
                $old_document_no                = $material_preparation->document_no;
                $old_po_detail_id               = $material_preparation->po_detail_id;
                $old_last_locator_id            = $material_preparation->last_locator_id;
                $old_last_locator_erp_id        = $material_preparation->lastLocator->area->erp_id;
                $old_item_id                    = $material_preparation->item_id;
                $old_item_id_source             = $material_preparation->item_id_source;
                $old_warehouse_id               = $material_preparation->warehouse;
                $old_item_code                  = $material_preparation->item_code;
                $old_style                      = $material_preparation->style;
                $old_article_no                 = $material_preparation->article_no;
                $old_type_po                    = $material_preparation->type_po;
                $old_uom_conversion             = $material_preparation->uom_conversion;
                $old_qty_conversion             = $material_preparation->qty_conversion;
                $old_warehouse                  = $material_preparation->warehouse;

                $reroute_destination = Locator::with('area')
                ->whereHas('area',function ($query) use ($old_warehouse_id)
                {
                    $query->where([
                        ['warehouse',$old_warehouse_id],
                        ['name','REROUTE'],
                        ['is_destination',true]
                    ]);

                })
                ->first();

                if(strpos($material_preparation->barcode, 'REROUTE_') !== false)
                {
                    $material_preparation->barcode              = $barcode;
                }
                else
                {
                    $material_preparation->barcode              = 'REROUTE_'.$barcode;
                }

                $material_preparation->last_status_movement     = 'reroute';
                $material_preparation->last_locator_id          = $reroute_destination->id;
                $material_preparation->deleted_at               = carbon::now();
                $material_preparation->last_movement_date       = $movement_date;
                $material_preparation->last_user_movement_id    = $user->id;
                $material_preparation->ict_log                  = $note;
                $material_preparation->is_reroute               = true;

                   
    
                $reroute_movement_is_exists     = MaterialMovement::where([
                    ['from_location',$old_last_locator_id],
                    ['to_destination',$reroute_destination->id],
                    ['from_locator_erp_id',$old_last_locator_erp_id],
                    ['to_locator_erp_id',$reroute_destination->area->erp_id],
                    ['po_buyer',$old_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['status','reroute'],
                ])
                ->first();

                if(!$reroute_movement_is_exists)
                {
                    $movement = MaterialMovement::firstOrCreate([
                        'from_location'             => $old_last_locator_id,
                        'to_destination'            => $reroute_destination->id,
                        'from_locator_erp_id'       => $old_last_locator_erp_id,
                        'to_locator_erp_id'         => $reroute_destination->area->erp_id,
                        'po_buyer'                  => $old_buyer,
                        'is_integrate'              => false,
                        'is_active'                 => true,
                        'status'                    => 'reroute',
                        'created_at'                => $movement_date,
                        'updated_at'                => $movement_date,
    
                    ]);

                    $movement_id = $movement->id;
                }else
                {
                    $reroute_movement_is_exists->updated_at = $movement_date;
                    $reroute_movement_is_exists->save();

                    $movement_id = $reroute_movement_is_exists->id;
                }
                
                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    'material_movement_id'      => $movement_id,
                    'material_preparation_id'   => $old_material_preparation_id,
                    'item_id'                   => $old_item_id,
                    'warehouse_id'              => $old_warehouse_id,
                    'type_po'                   => $old_type_po,
                    'date_movement'             => $movement_date,
                    'is_integrate'              => false,
                    'is_active'                 => true,
                    'qty_movement'              => $old_qty_conversion,
                ])
                ->exists();

                if(!$is_line_exists)
                {
                    $new_material_movement_line = MaterialMovementLine::Create([
                        'material_movement_id'          => $movement_id,
                        'material_preparation_id'       => $old_material_preparation_id,
                        'item_id'                       => $old_item_id,
                        'item_id_source'                => $old_item_id_source,
                        'c_order_id'                    => $old_c_order_id,
                        'c_orderline_id'                => null,
                        'c_bpartner_id'                 => $old_c_bpartner_id,
                        'supplier_name'                 => $old_supplier_name,
                        'document_no'                   => $old_document_no,
                        'item_code'                     => $old_item_code,
                        'nomor_roll'                    => '-',
                        'type_po'                       => $old_type_po,
                        'uom_movement'                  => $old_uom_conversion,
                        'qty_movement'                  => $old_qty_conversion,
                        'warehouse_id'                  => $old_warehouse,
                        'date_movement'                 => $movement_date,
                        'created_at'                    => $movement_date,
                        'updated_at'                    => $movement_date,
                        'date_receive_on_destination'   => $movement_date,
                        'is_active'                     => true,
                        'is_integrate'                  => false,
                        'note'                          => $note,
                        'user_id'                       => $user->id
                    ]);

                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                    $material_preparation->save();
                }

                //update deleted at material movement line
                MaterialMovementLine::where('material_preparation_id',$old_material_preparation_id)
                ->update([ 
                    'deleted_at' => carbon::now(),
                    'is_active' => false,
                ]);

                $material_preparation->save();
            }

            DB::commit();
            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function deletePlanningFabrics($_po_buyer)
    {
        try 
        {
            DB::beginTransaction();

            $detail_material_plannings = DetailMaterialPlanningFabric::select('document_no'
                ,'c_order_id'
                ,'article_no'
                ,'warehouse_id'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'_style'
                ,'uom'
                ,'planning_date'
                ,'is_piping'
                ,db::raw("sum(qty_booking) as total_booking")
            )
            ->where('po_buyer','!=',$_po_buyer)
            ->groupby('document_no'
                ,'c_order_id'
                ,'warehouse_id'
                ,'uom'
                ,'_style'
                ,'item_code'
                ,'item_code_source'
                ,'item_id_book'
                ,'item_id_source'
                ,'article_no'
                ,'planning_date'
                ,'is_piping')
            ->get();

            $movement_date  = carbon::now()->toDateTimeString();
            
            foreach ($detail_material_plannings as $key => $detail_material_planning) 
            {
                
                $_c_order_id_preparation        = $detail_material_planning->c_order_id;
                $_style_preparation             = $detail_material_planning->_style;
                $_article_no_preparation        = $detail_material_planning->article_no;
                $_document_no_preparation       = $detail_material_planning->document_no;
                $_warehouse_id_preparation      = $detail_material_planning->warehouse_id;
                $_item_code                     = $detail_material_planning->item_code;
                $_item_code_source_preparation  = $detail_material_planning->item_code_source;
                $_item_id_book                  = $detail_material_planning->item_id_book;
                $_item_id_source_preparation    = $detail_material_planning->item_id_source;
                $_planning_date_preparation     = $detail_material_planning->planning_date->format('d/M/y');
                $_is_piping_preparation         = $detail_material_planning->is_piping;
                $_uom                           = $detail_material_planning->uom;
                $_total_booking_preparation     = sprintf('%0.8f',$detail_material_planning->total_booking);

                $is_material_preparation_exists = MaterialPreparationFabric::where([
                    ['c_order_id',$_c_order_id_preparation],
                    ['_style',$_style_preparation],
                    ['article_no',$_article_no_preparation],
                    ['warehouse_id',$_warehouse_id_preparation],
                    ['item_id_book',$_item_id_book],
                    ['item_id_source',$_item_id_source_preparation],
                    ['planning_date',$_planning_date_preparation],
                    ['is_piping',$_is_piping_preparation],
                ])
                ->first();

                if($is_material_preparation_exists)
                {
                    $total_reserved_qty     = sprintf('%0.8f',$is_material_preparation_exists->total_reserved_qty);
                    $total_qty_relax        = sprintf('%0.8f',$is_material_preparation_exists->total_qty_rilex);
                    $curr_remark_planning   = $is_material_preparation_exists->remark_planning;

                    if($_total_booking_preparation != $total_reserved_qty)
                    {
                        $new_qty_oustanding = sprintf('%0.8f',$_total_booking_preparation - $total_qty_relax);
                       
                        if($new_qty_oustanding < 0) $remark_planning = 'pengurangan qty alokasi';
                        else $remark_planning = 'penambahan qty alokasi';

                        HistoryPreparationFabric::create([
                            'material_preparation_fabric_id'    => $is_material_preparation_exists->id,
                            'remark'                            => $remark_planning.'. Po buyer '.$_po_buyer.' Cancel',
                            'qty_before'                        => $total_reserved_qty,
                            'qty_after'                         => $_total_booking_preparation,
                            'created_at'                        => $movement_date,
                            'updated_at'                        => $movement_date,
                        ]);

                        $is_material_preparation_exists->total_reserved_qty     = $_total_booking_preparation;
                        $is_material_preparation_exists->total_qty_outstanding  = $new_qty_oustanding;
                        $is_material_preparation_exists->remark_planning        = ( $curr_remark_planning ? $curr_remark_planning .', Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' : 'Po Buyer '.$_po_buyer.' cancel please reduce qty '.(-1*$new_qty_oustanding).'('.$_uom.') from preparation and do re-preparation roll to buyer' );
                        $is_material_preparation_exists->save();
                    }
                }
            }
            

            MaterialPlanningFabric::where('po_buyer',$_po_buyer)->delete();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function storeCuttingInstructionHeaderReport($start_date,$end_date)
    {

        $material_planning_fabrics = MaterialPlanningFabric::select(db::raw("substr(job_order,0,POSITION ( ':' IN job_order )) AS style")
            ,'article_no'
            ,'planning_date'
            ,'uom'
            ,'warehouse_id'
            ,db::raw("string_agg ( distinct item_code, ',' ) AS item_code")
            ,db::raw("string_agg ( distinct po_buyer, ',' ) AS po_buyer")
            ,db::raw("sum(qty_consumtion) as qty_need"))
        ->whereBetween('planning_date',[$start_date,$end_date])
        ->groupby(db::raw("substr(job_order,0,POSITION ( ':' IN job_order ))")
            ,'uom'
            ,'article_no'
            ,'warehouse_id'
            ,'planning_date')
        ->get();
        
        try 
        {
            DB::beginTransaction();

            CuttingInstruction::whereBetween('planning_date',[$start_date,$end_date])->delete();

            $movement_date = carbon::now()->toDateTimeString();
            foreach ($material_planning_fabrics as $key => $material_planning_fabric) 
            {
                $qty_need       = sprintf("%.8f",$material_planning_fabric->qty_need);
                $style          = $material_planning_fabric->style;
                $article_no     = $material_planning_fabric->article_no;
                $uom            = $material_planning_fabric->uom;
                $po_buyer       = $material_planning_fabric->po_buyer;
                $item_code      = $material_planning_fabric->item_code;
                $warehouse_id   = $material_planning_fabric->warehouse_id;
                $planning_date  = $material_planning_fabric->planning_date;
                
                $is_exists = CuttingInstruction::where([
                    ['article_no',$article_no],
                    ['style',$style],
                    ['uom',$uom],
                    ['item_code',$item_code],
                    ['planning_date',$planning_date],
                    ['po_buyer',$po_buyer],
                    ['qty_need',$qty_need],
                    ['warehouse_id',$warehouse_id],
                ])
                ->exists();

                if(!$is_exists)
                {
                    CuttingInstruction::FirstOrCreate([
                        'created_at'        => $movement_date,
                        'updated_at'        => $movement_date,
                        'article_no'        => $article_no,
                        'uom'               => $uom,
                        'style'             => $style,
                        'item_code'         => $item_code,
                        'planning_date'     => $planning_date,
                        'po_buyer'          => $po_buyer,
                        'qty_need'          => $qty_need,
                        'warehouse_id'      => $warehouse_id,
                    ]);
                }
                
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    public function import()
    {
      return view('master_data_reroute_buyer.import');
    }

    public function downloadFromImport()
    {
      return Excel::create('upload_reroute_po_buyer',function ($excel){
         $excel->sheet('active', function($sheet){
             $sheet->setCellValue('A1','OLD_PO_BUYER');
             $sheet->setCellValue('B1','NEW_PO_BUYER');
             $sheet->setCellValue('C1','PREFIX');
             $sheet->setCellValue('D1','NOTE');
             $sheet->setWidth('A', 15);
             $sheet->setWidth('B', 15);
             $sheet->setWidth('C', 10);
             $sheet->setWidth('D', 40);
             $sheet->setColumnFormat(array(
                 'A' => '@',
                 'B' => '@',
                 'C' => '@',
                 'D' => '@',
             ));

         });
         $excel->setActiveSheetIndex(0);
     })->export('xlsx');
    }

    public function uploadFromImport(Request $request)
    {
      if($request->hasFile('upload_file'))
      {
          $validator = Validator::make($request->all(), [
              'upload_file' => 'required|mimes:xls'
          ]);

          $path = $request->file('upload_file')->getRealPath();
          $data = Excel::selectSheets('active')->load($path,function($render){})->get();

          if(!empty($data) && $data->count())
          {
              $array            = array();
              $movement_date    = carbon::now()->toDateTimeString();

              foreach ($data as $key => $value)
              {
                try 
                {
  
                DB::beginTransaction();
                    $old_po_buyer   = $value->old_po_buyer;
                    $new_po_buyer   = $value->new_po_buyer;
                    $note           = trim(strtoupper($value->note));
                    $start_date     = MaterialPlanningFabric::where('po_buyer',$old_po_buyer)->min('planning_date');
                    $end_date       = MaterialPlanningFabric::where('po_buyer',$old_po_buyer)->max('planning_date');

                    if($request->is_prefix!=null && ($request->is_prefix == 'TRUE'||$request->is_prefix == 'true')) $is_prefix = true;
                    else $is_prefix = false;

                    if($old_po_buyer != null && $new_po_buyer != null)
                    {
                        $is_exists = ReroutePoBuyer::where([
                            ['old_po_buyer',$old_po_buyer],
                            ['new_po_buyer',$new_po_buyer],
                        ])
                        ->exists();

                        if(!$is_exists)
                        {
                            if($is_prefix) $note .= '(UBAH PREFIX DARI '.$old_po_buyer.' KE '.$new_po_buyer.')';
                            else $note .= '(REROUTE DARI '.$old_po_buyer.' KE '.$new_po_buyer.')';

                            $newData = ReroutePoBuyer::firstorcreate([
                                'old_po_buyer' => $old_po_buyer,
                                'new_po_buyer' => $new_po_buyer,
                                'user_id' => auth::user()->id,
                                'note' => $note,
                                'created_at' => $movement_date
                            ]);
                            
        
                            Temporary::firstorcreate([
                                'string_1' => $newData->id,
                                'string_2' => 'reroute po buyer'
                            ]);

                            /*$this->copyFromOldToNew($old_po_buyer,$new_po_buyer,null,$movement_date,$note);
                            $this->removeOldPoBuyer($old_po_buyer,$new_po_buyer,null,$note);
                            $this->deletePlanningFabrics($old_po_buyer);
                            $this->storeCuttingInstructionHeaderReport($start_date,$end_date);*/

                            $obj = new stdClass();
                            $obj->old_po_buyer  = $old_po_buyer;
                            $obj->new_po_buyer  = $new_po_buyer;
                            $obj->is_prefix     = $is_prefix;
                            $obj->note          = $note;
                            $obj->is_error      = false;
                            $obj->status        = 'Success';
                            $array[] = $obj;
                        }else
                        {
                            $obj = new stdClass();
                            $obj->old_po_buyer  = $old_po_buyer;
                            $obj->new_po_buyer  = $new_po_buyer;
                            $obj->is_prefix     = $is_prefix;
                            $obj->note          = $note;
                            $obj->is_error      = true;
                            $obj->status        = 'Failed, data sudah ada!';
                            $array[] = $obj;
                        }
                    }else
                    {
                        $obj = new stdClass();
                        $obj->old_po_buyer  = $old_po_buyer;
                        $obj->new_po_buyer  = $new_po_buyer;
                        $obj->is_prefix     = $is_prefix;
                        $obj->note          = $note;
                        $obj->is_error      = true;
                        $obj->status        = 'Data Old / New PO Buyer Tidak Boleh Kosong !';
                        $array[] = $obj;
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                     DB::rollBack();
                     $message = $e->getMessage();
                     ErrorHandler::db($message);
                }

              }
              return response()->json($array,200);
          }
      }
      return response()->json('import gagal, silahkan cek file anda',422);
    }

    // public function uploadFromImport(Request $request)
    // {
    //   if($request->hasFile('upload_file'))
    //   {
    //       $validator = Validator::make($request->all(), [
    //           'upload_file' => 'required|mimes:xls'
    //       ]);

    //       $path = $request->file('upload_file')->getRealPath();
    //       $data = Excel::selectSheets('active')->load($path,function($render){})->get();

    //       if(!empty($data) && $data->count())
    //       {
    //           $array            = array();
    //           $movement_date    = carbon::now()->toDateTimeString();

    //           foreach ($data as $key => $value)
    //           {
    //             try 
    //             {
  
    //             DB::beginTransaction();
    //                 $old_po_buyer   = $value->old_po_buyer;
    //                 $new_po_buyer   = $value->new_po_buyer;
    //                 $note           = trim(strtoupper($value->note));
    //                 $start_date     = MaterialPlanningFabric::where('po_buyer',$old_po_buyer)->min('planning_date');
    //                 $end_date       = MaterialPlanningFabric::where('po_buyer',$old_po_buyer)->max('planning_date');

    //                 if($request->is_prefix!=null && ($request->is_prefix == 'TRUE'||$request->is_prefix == 'true')) $is_prefix = true;
    //                 else $is_prefix = false;

    //                 if($old_po_buyer != null && $new_po_buyer != null)
    //                 {
    //                     $is_exists = ReroutePoBuyer::where([
    //                         ['old_po_buyer',$old_po_buyer],
    //                         ['new_po_buyer',$new_po_buyer],
    //                     ])
    //                     ->exists();

    //                     if(!$is_exists)
    //                     {
    //                         ReroutePoBuyer::firstorcreate([
    //                             'old_po_buyer' => $old_po_buyer,
    //                             'new_po_buyer' => $new_po_buyer,
    //                             'user_id' => auth::user()->id,
    //                             'note' => $note
    //                         ]);
    //                         if($is_prefix) $note .= '(UBAH PREFIX DARI '.$old_po_buyer.' KE '.$new_po_buyer.')';
    //                         else $note .= '(REROUTE DARI '.$old_po_buyer.' KE '.$new_po_buyer.')';
        
    //                         $this->copyFromOldToNew($old_po_buyer,$new_po_buyer,null,$movement_date,$note);
    //                         $this->removeOldPoBuyer($old_po_buyer,$new_po_buyer,null,$note);
    //                         $this->deletePlanningFabrics($old_po_buyer);
    //                         $this->storeCuttingInstructionHeaderReport($start_date,$end_date);

    //                         $obj = new stdClass();
    //                         $obj->old_po_buyer  = $old_po_buyer;
    //                         $obj->new_po_buyer  = $new_po_buyer;
    //                         $obj->is_prefix     = $is_prefix;
    //                         $obj->note          = $note;
    //                         $obj->is_error      = false;
    //                         $obj->status        = 'Success';
    //                         $array[] = $obj;
    //                     }else
    //                     {
    //                         $obj = new stdClass();
    //                         $obj->old_po_buyer  = $old_po_buyer;
    //                         $obj->new_po_buyer  = $new_po_buyer;
    //                         $obj->is_prefix     = $is_prefix;
    //                         $obj->note          = $note;
    //                         $obj->is_error      = true;
    //                         $obj->status        = 'Failed, data sudah ada!';
    //                         $array[] = $obj;
    //                     }
    //                 }else
    //                 {
    //                     $obj = new stdClass();
    //                     $obj->old_po_buyer  = $old_po_buyer;
    //                     $obj->new_po_buyer  = $new_po_buyer;
    //                     $obj->is_prefix     = $is_prefix;
    //                     $obj->note          = $note;
    //                     $obj->is_error      = true;
    //                     $obj->status        = 'Data Old / New PO Buyer Tidak Boleh Kosong !';
    //                     $array[] = $obj;
    //                 }

    //                 DB::commit();
    //             } catch (Exception $e) 
    //             {
    //                  DB::rollBack();
    //                  $message = $e->getMessage();
    //                  ErrorHandler::db($message);
    //             }

    //           }
    //           return response()->json($array,200);
    //       }
    //   }
    //   return response()->json('import gagal, silahkan cek file anda',422);
    // }

}
