<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class FabricReportMaterialReceivePipingController extends Controller
{
    public function index()
    {
        return view('fabric_report_material_receive_piping.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id               = $request->warehouse;
            $user_id                    = $request->user_id ;
            $last_status_movement       = $request->status;
            $start_date                 = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date                   = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
            
            $material_receive_piping      = DB::table('report_material_receive_piping_v')
            ->where('warehouse_id', $warehouse_id)
            ->where('last_status_movement', 'like', '%' .$last_status_movement. '%')
            ->whereBetween('received_piping_date',[$start_date,$end_date])
            ->orderby('received_piping_date','desc');

            return DataTables::of($material_receive_piping)
           
            ->editColumn('received_piping_date',function ($material_receive_piping)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_receive_piping->received_piping_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('qty',function ($material_receive_piping)
            {
                return number_format($material_receive_piping->qty, 4, '.', ',');
            })
            ->make(true);
        }
    }
}
