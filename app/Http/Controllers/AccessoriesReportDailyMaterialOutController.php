<?php namespace App\Http\Controllers;

use DB;
use Excel;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportDailyMaterialOutController extends Controller
{
    public function index()
    {
        return view('accessories_report_daily_material_out.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            
            $daily_out          = DB::table('material_out_v')
            ->where('warehouse','LIKE',"%$warehouse_id%")
            ->whereBetween('last_movement_date',[$start_date,$end_date])
            ->orderby('last_movement_date','desc')
            ->take(100);
            
            return DataTables::of($daily_out)
            ->editColumn('last_movement_date',function ($daily_out)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_out->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('qty_conversion',function ($daily_out)
            {
                return number_format($daily_out->qty_conversion, 4, '.', ',');
            })
            ->editColumn('warehouse',function ($daily_out)
            {
                if($daily_out->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($daily_out->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $warehouse_id       = $request->warehouse;
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

        
        $daily_out          = DB::table('material_out_v')
                            ->where('warehouse','LIKE',"%$warehouse_id%")
                            ->whereBetween('last_movement_date',[$start_date,$end_date])
                            ->orderby('last_movement_date','desc')
                            ->get();


        $file_name = 'accessories_report_material_out date'.$start_date.'-'.$end_date;
                        
        return Excel::create($file_name,function($excel) use ($daily_out)
        {
            $excel->sheet('active',function($sheet)use($daily_out)
            {
                $sheet->setCellValue('A1','SUPPLIED_DATE');
                $sheet->setCellValue('B1','WAREHOUSE_SUPPLIED');
                $sheet->setCellValue('C1','PO_SUPPLIER');
                $sheet->setCellValue('D1','SUPPLIER_NAME');
                $sheet->setCellValue('E1','PO_BUYER');
                $sheet->setCellValue('F1','ITEM_CODE');
                $sheet->setCellValue('G1','CATEGORY');
                $sheet->setCellValue('H1','STYLE');
                $sheet->setCellValue('I1','ARTICLE');
                $sheet->setCellValue('J1','UOM');
                $sheet->setCellValue('K1','QTY_SUPPLIED');
                $sheet->setCellValue('L1','DESTINATION');
                $sheet->setCellValue('M1','PIC OUT');

            $row=2;
            foreach ($daily_out as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->created_at);
                if($i->warehouse == '1000002') $warehouse = 'Warehouse accessories AOI 1';
                elseif($i->warehouse == '1000013') $warehouse = 'Warehouse accessories AOI 2';
                $sheet->setCellValue('B'.$row,$warehouse);
                $sheet->setCellValue('C'.$row,$i->document_no);
                $sheet->setCellValue('D'.$row,$i->supplier_name);
                $sheet->setCellValue('E'.$row,$i->po_buyer);
                $sheet->setCellValue('F'.$row,$i->item_code);
                $sheet->setCellValue('G'.$row,$i->category);
                $sheet->setCellValue('H'.$row,$i->style);
                $sheet->setCellValue('I'.$row,$i->article_no);
                $sheet->setCellValue('J'.$row,$i->uom_conversion);
                $sheet->setCellValue('K'.$row,$i->qty_conversion);
                $sheet->setCellValue('L'.$row,$i->destination);
                $sheet->setCellValue('M'.$row,$i->pic_out);
                $row++;
            }
            });

        })
        ->export('xlsx');

    }
}
