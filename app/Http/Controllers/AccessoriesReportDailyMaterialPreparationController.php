<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportDailyMaterialPreparationController extends Controller
{
    public function index()
    {
        return view('accessories_report_daily_material_preparation.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            
            $daily_preprations     = DB::table('monitoring_receiving')
            ->where('warehouse','LIKE',"%$warehouse_id%")
            ->whereBetween('created_at',[$start_date,$end_date])
            ->orderby('created_at','desc')
            ->take(100);
            
            return DataTables::of($daily_preprations)
            ->editColumn('created_at',function ($daily_preprations)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_preprations->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('last_movement_date',function ($daily_preprations)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $daily_preprations->last_movement_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('qty_conversion',function ($daily_preprations)
            {
                return number_format($daily_preprations->qty_conversion, 4, '.', ',');
            })
            ->editColumn('warehouse',function ($daily_preprations)
            {
                if($daily_preprations->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($daily_preprations->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
        $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();
        $warehouse_id       = ($request->warehouse) ? $request->warehouse : auth::user()->warehouse;

        $monitor_receivements = db::table('monitoring_receiving')
        ->where('warehouse','LIKE',"%$warehouse_id%")
        ->whereBetween('created_at', [$start_date, $end_date])
        ->get();

        //return response()->json($monitor_receivements);
        return Excel::create('LAPORAN_DAILY_PREPARATION',function ($excel) use($monitor_receivements)
        {
            $excel->setCreator(auth::user()->name);
            $excel->sheet('main', function($sheet) use($monitor_receivements) {
                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','CREATED AT');
                $sheet->setCellValue('C1','NO PO SUPPLIER');
                $sheet->setCellValue('D1','NAMA SUPPLIER');
                $sheet->setCellValue('E1','PO BUYER');
                $sheet->setCellValue('F1','KODE ITEM');
                $sheet->setCellValue('G1','STYLE');
                $sheet->setCellValue('H1','KATEGORI ITEM');
                $sheet->setCellValue('I1','UOM CONVERSION');
                $sheet->setCellValue('J1','QTY CONVERSION');
                $sheet->setCellValue('K1','QC STATUS');
                $sheet->setCellValue('L1','QC CHECKER');
                $sheet->setCellValue('M1','QC DATE');
                $sheet->setCellValue('N1','STATUS TERAKHIR');
                $sheet->setCellValue('O1','LOKASI TERAKHIR');
                $sheet->setCellValue('P1','TANGGAL PINDAH TERKAHIR');
                $sheet->setCellValue('Q1','PIC RECEIVE');
                $sheet->setCellValue('R1','PODD');
                $index = 0;
                foreach ($monitor_receivements as $key => $value) {
                    $row = $index + 2;
                    $index++;

                    if($value->last_status_movement == 'receiving')
                        $_last_status  ='MATERIAL DITERIMA DIGUDANG';
                    elseif($value->last_status_movement == 'not receive')
                        $_last_status  ='MATERIAL BELUM DITERIMA DIGUDANG';
                    elseif($value->last_status_movement == 'in')
                        $_last_status  ='MATERIAL SIAP SUPLAI';
                    elseif($value->last_status_movement == 'expanse')
                        $_last_status  ='EXPENSE';
                    elseif($value->last_status_movement == 'out')
                        $_last_status  ='MATERIAL SUDAH DISUPLAI';	
                    elseif($value->last_status_movement == 'out-handover')
                        $_last_status  ='MATERIAL PINDAH TANGAN';
                     elseif($value->last_status_movement == 'out-subcont')
                        $_last_status  ='MATERIAL SUDAH DISUPLAI KE SUBCONT';
                    elseif($value->last_status_movement == 'reject')
                        $_last_status  ='REJECT';	
                    elseif($value->last_status_movement == 'change')
                        $_last_status  ='MATERIAL PINDAH LOKASI RAK';
                    elseif($value->last_status_movement == 'hold')
                        $_last_status  ='MATERIAL DITAHAN QC UNTUK PENGECEKAN';
                    elseif($value->last_status_movement == 'adjustment' || $value->last_status_movement == 'adjusment')
                        $_last_status  ='PENYESUAIAN QTY';
                    elseif($value->last_status_movement == 'print')
                        $_last_status  ='CETAK BARCODE';
                    elseif($value->last_status_movement == 'reroute')
                        $_last_status  ='REROUTE';
                    elseif($value->last_status_movement == 'cancel item')
                        $_last_status  ='CANCEL ITEM';
                    elseif($value->last_status_movement == 'cancel order')
                        $_last_status  ='CANCEL ORDER';
                    elseif($value->last_status_movement == 'check')
                        $_last_status  ='MATERIAL DALAM PENGECEKAN QC';
                    else if($value->last_status_movement == 'out-cutting')
                        $_last_status = 'MATERIAL SUDAH DISUPLAI KE CUTTING';    
                    else if($value->last_status_movement == 'relax')
                        $_last_status = 'RELAX CUTTING';   
                    else
                        $_last_status  = $value->last_status_movement;
                        

                    $sheet->setCellValue('A'.$row, $index);
                    $sheet->setCellValue('B'.$row, $value->created_at);
                    $sheet->setCellValue('C'.$row, $value->document_no);
                    $sheet->setCellValue('D'.$row, $value->supplier_name);
                    $sheet->setCellValue('E'.$row, $value->po_buyer);
                    $sheet->setCellValue('F'.$row, $value->item_code);
                    $sheet->setCellValue('G'.$row, $value->style);
                    $sheet->setCellValue('H'.$row, $value->category);
                    $sheet->setCellValue('I'.$row, $value->uom_conversion);
                    $sheet->setCellValue('J'.$row, $value->qty_conversion);
                    $sheet->setCellValue('K'.$row, $value->qc_status);
                    $sheet->setCellValue('L'.$row, $value->qc_checker);
                    $sheet->setCellValue('M'.$row, $value->qc_date);
                    $sheet->setCellValue('N'.$row, $_last_status);
                    $sheet->setCellValue('O'.$row, $value->code);
                    $sheet->setCellValue('P'.$row, $value->last_movement_date);
                    $sheet->setCellValue('Q'.$row, $value->pic_receive);
                    $sheet->setCellValue('R'.$row, $value->statistical_date);

                }
            });

        })
        ->export('csv');
    }

    public function printPreview(Request $request)
    {
        $start_date             = ($request->_start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->_start_date." 00:00:00") : Carbon::today();
        $end_date               = ($request->_end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->_end_date." 23:59:59") : Carbon::now();
        $monitor_receivements   = db::table('material_receivement_v')
        ->where('warehouse_id','LIKE',"%$warehouse_id%")
        ->whereBetween('created_at',[$start_date,$end_date])
        ->orderby('created_at','desc')
        ->get();

        return view('accessories_report_material_preparation.print_preview', compact('monitor_receivements'));
    }
}
