<?php namespace App\Http\Controllers;

use DB;
use Auth;
use File;
use Excel;
use Config;
use stdClass;
use Validator;
use Carbon\Carbon;
use IntlDateFormatter;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\PurchaseItem;
use App\Models\MaterialStock;
use App\Models\AllocationItem;
use App\Models\MaterialCheck;
use App\Models\MaterialArrival;

use App\Models\MovementStockHistory;
use App\Models\DetailMaterialArrival;
use App\Models\MaterialPlanningFabric;
use App\Models\SummaryHandoverMaterial;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialRequirement;
use App\Models\BarcodePreparationFabric;
use App\Models\MaterialRequirementPerPart;
use App\Models\MonitoringReceivingFabric;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\DetailMaterialCheck;
use App\Models\MaterialOpnameFabricSummary;
use App\Models\MaterialRollHandoverFabric;
use App\Models\Locator;


class ReportFabricController extends Controller
{
  public function receivement(Request $request, Builder $htmlBuilder)
  {
      if ($request->ajax())
      {
          $receivements = db::table('material_receive_fabric_v')->where('warehouse_id',auth::user()->warehouse);
          return DataTables::of($receivements)
          ->make(true);
      }

      $html = $htmlBuilder
      ->addColumn(['data'=>'no_invoice', 'name'=>'no_invoice', 'title'=>'NO INVOICE'])
      ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
      ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
      ->addColumn(['data'=>'item_code', 'name'=>'item_desc', 'title'=>'KODE ITEM'])
      ->addColumn(['data'=>'item_desc', 'name'=>'item_desc', 'title'=>'DEKSRIPSI ITEM'])
      ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
      ->addColumn(['data'=>'total_carton', 'name'=>'total_carton', 'title'=>'TOTAL ROLL'])
      ->addColumn(['data'=>'total_qty_roll', 'name'=>'total_qty_roll', 'title'=>'TOTAL QTY ROLL'])
      ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
      ->addColumn(['data'=>'eta_date', 'name'=>'eta_date', 'title'=>'ETA DATE'])
      ->addColumn(['data'=>'date_receive', 'name'=>'date_receive', 'title'=>'DATE RECEIVE'])
      ->addColumn(['data'=>'pic_receive', 'name'=>'pic_receive', 'title'=>'PIC RECEIVE']);

      return view('report_fabric.receivement',compact('html'));
  }

  

  private function getDetailMaterialArrivals($header)
  {
    $total_qty_roll = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(stock) as stock'))
    ->where([
        ['document_no',$header->document_no],
        ['c_bpartner_id',$header->c_bpartner_id],
        ['item_code',$header->item_code],
        ['no_invoice',$header->no_invoice],
        ['warehouse_id',$header->warehouse_id],
        ['warehouse_id',$header->warehouse_id],
        ['monitoring_receiving_fabric_id',$header->monitoring_receiving_fabric_id],
    ])
    ->groupby('uom')
    ->first();

    //dd($total_qty_roll);
    $total_qty_inspect_roll = MaterialStock::select('uom',db::raw('count(0) as total_roll'),db::raw('sum(stock) as stock'))
    ->where([
        ['document_no',$header->document_no],
        ['c_bpartner_id',$header->c_bpartner_id],
        ['item_code',$header->item_code],
        ['no_invoice',$header->no_invoice],
        ['warehouse_id',$header->warehouse_id],
        ['monitoring_receiving_fabric_id',$header->monitoring_receiving_fabric_id],
    ])
    ->whereNotNull('inspect_lab_date')
    ->groupby('uom')
    ->first();

    $header->total_roll_all=($total_qty_roll)? $total_qty_roll->total_roll:0;
    $header->total_qty_roll_all=($total_qty_roll) ? $total_qty_roll->stock:0;
    $header->uom_roll_all=($total_qty_roll) ? $total_qty_roll->uom:0;
    $header->total_roll_inspect= ($total_qty_inspect_roll) ?$total_qty_inspect_roll->total_roll:0;
    $header->total_qty_roll_inspect= ($total_qty_inspect_roll) ? $total_qty_inspect_roll->stock : 0;
    $header->uom_roll_inspect= ($total_qty_inspect_roll) ? $total_qty_inspect_roll->uom : null;
    return $header;
  }

  

 

  

 

  public function actualWidth()
  {
    return view('report_fabric.actual_width');
  }

  public function actualWidthExcel(Request $request)
  {
      $validator =  Validator::make($request->all(), [
          'start_date' => 'required',
          'end_date' => 'required',
      ]);

      if ($validator->passes()){
          $start_pcd = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d');
          $_start_pcd = Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Ymd');
          $end_pcd = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d');
          $_end_pcd = Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Ymd');
          $pcd = new stdClass();
          $pcd->start_pcd = $start_pcd;
          $pcd->end_pcd = $end_pcd;

          $material_planning_fabrics = MaterialPlanningFabric::select('id','planning_date','po_buyer','item_code','color','job_order','article_no','style')
          ->where('warehouse_id',auth::user()->warehouse)
          ->whereBetween('planning_date',[$pcd->start_pcd,$pcd->end_pcd])
          ->orderby('po_buyer','asc')
          ->orderby('item_code','asc')
          ->groupby('id','planning_date','po_buyer','item_code','color','job_order','article_no','style')
          ->orderby('planning_date','asc')
          ->get();

          $file_name = 'INFORMASI_LEBAR_AKTUAL_'.$_start_pcd.'_'.$_end_pcd;
          return Excel::create($file_name,function($excel) use($pcd){
            $excel->setCreator('ICT')
            ->setCompany('AOI');

            $material_planning_fabrics = MaterialPlanningFabric::select('id','planning_date','po_buyer','item_code','color','job_order','article_no','style')
            ->where('warehouse_id',auth::user()->warehouse)
            ->whereBetween('planning_date',[$pcd->start_pcd,$pcd->end_pcd])
            ->orderby('po_buyer','asc')
            ->orderby('item_code','asc')
            ->groupby('id','planning_date','po_buyer','item_code','color','job_order','article_no','style')
            ->get();

            $excel->sheet('active',function($sheet) use($material_planning_fabrics){
              $sheet->setCellValue('A1','PLANNING DATE');
              $sheet->setCellValue('B1','FINISH PREPARE FABRIC');
              $sheet->setCellValue('C1','STYLE');
              $sheet->setCellValue('D1','JOB ORDER');
              $sheet->setCellValue('E1','ARTICLE_NO');
              $sheet->setCellValue('F1','PO_BUYER');
              $sheet->setCellValue('G1','LC_DATE');
              $sheet->setCellValue('H1','SHIPMENT_DATE');
              $sheet->setCellValue('I1','ITEM_CODE');
              $sheet->setCellValue('J1','COLOR');
              $sheet->setCellValue('K1','IS_PIPING');
              $sheet->setCellValue('L1','PART_NO');
              $sheet->setCellValue('M1','UOM');
              $sheet->setCellValue('N1','QTY_NEED');
              $sheet->setCellValue('O1','QTY_OUTSTANDING');
              //$sheet->setCellValue('O1','ROLL_NUMBER');

              $sheet->cell('A1:AZ1',function($cell){
                $cell->setFont(array(
                  'bold'     => true
                ));
              });

              $array_sheet = [
                0 => 'P,Q',
                1 => 'R,S',
                2 => 'T,U',
                3 => 'V,W',
                4 => 'X,Y',
                5 => 'Z,AA',
                6 => 'AB,AC',
                7 => 'AD,AE',
                8 => 'AF,AG',

              ];

              $index_1 = 0;
              foreach ($material_planning_fabrics as $key_0 => $material_planning_fabric) {
                $planning_date = $material_planning_fabric->planning_date;
                $po_buyer = $material_planning_fabric->po_buyer;
                $item_code = $material_planning_fabric->item_code;
                $color = $material_planning_fabric->color;
                $job_order = $material_planning_fabric->job_order;
                $style = $material_planning_fabric->style;
                $article_no = $material_planning_fabric->article_no;
                $actual_planning = $material_planning_fabric->getActualPlanning($material_planning_fabric->id);

                $master_lc = PoBuyer::select('lc_date','statistical_date')
                ->where('po_buyer',$po_buyer)
                ->groupby('lc_date','statistical_date')
                ->first();

                $lc_date = ($master_lc)? $master_lc->lc_date : null;
                $statistical_date = ($master_lc)? $master_lc->statistical_date : null;

                /*$detail_requirements = DetailMaterialRequirement::select('part_no','uom','is_piping',db::raw('sum(qty_required) as qty_required'))
                ->where([
                  ['po_buyer',$po_buyer],
                  ['item_code',$item_code],
                  ['style',$style],
                  ['article_no',$article_no],
                  ['is_piping',false],
                  ['category','FB'],
                ])
                ->whereNotNull('part_no')
                ->orderby('part_no','asc')
                ->groupby('part_no','uom','is_piping')
                ->get();*/

                $detail_requirements = MaterialRequirementPerPart::select('part_no','uom','is_piping','qty_required')
                ->where([
                  ['po_buyer',$po_buyer],
                  ['item_code',$item_code],
                  ['style',$style],
                  ['article_no',$article_no],
                  ['is_piping',false],
                  ['category','FB'],
                ])
                ->whereNotNull('part_no')
                ->orderby('part_no','asc')
                ->get();

                foreach ($detail_requirements as $key_1 => $detail_requirement) {
                  $row = $index_1 + 2;

                  $uom_need = $detail_requirement->uom;
                  $is_piping = $detail_requirement->is_piping;
                  $part_no = $detail_requirement->part_no;
                  $qty_need = sprintf('%0.8f',$detail_requirement->qty_required);

                  $detail_rolls = $material_planning_fabric->getDetailRoll($material_planning_fabric->id,$part_no);
                  $index_1++;

                  $sheet->setCellValue('A'.$row, $planning_date);
                  $sheet->setCellValue('B'.$row, $actual_planning);
                  $sheet->setCellValue('C'.$row, $style);
                  $sheet->setCellValue('D'.$row, $job_order);
                  $sheet->setCellValue('E'.$row, $article_no);
                  $sheet->setCellValue('F'.$row, $po_buyer);
                  $sheet->setCellValue('G'.$row, $lc_date);
                  $sheet->setCellValue('H'.$row, $statistical_date);
                  $sheet->setCellValue('I'.$row, $item_code);
                  $sheet->setCellValue('J'.$row, $color);
                  $sheet->setCellValue('K'.$row, $is_piping);
                  $sheet->setCellValue('L'.$row, $part_no);
                  $sheet->setCellValue('M'.$row, $uom_need);
                  $sheet->setCellValue('N'.$row, $qty_need);

                  $total_reserved_qty = 0;
                  foreach ($detail_rolls as $key_2 => $detail_roll) {
                    //$nomor_roll = $detail_roll->nomor_roll;
                    $actual_width = $detail_roll->actual_width;
                    $uom = $detail_roll->uom;
                    $reserved_qty = $detail_roll->reserved_qty;
                    $split = explode(',',$array_sheet[$key_2]);
                    $sheet->setCellValue($split[0].'1', 'ACTUAL_WIDTH_'.($key_2+1));
                    $sheet->setCellValue($split[1].'1', 'RESERVED_QTY_'.($key_2+1));
                    $sheet->setCellValue($split[0].$row, $actual_width);
                    $sheet->setCellValue($split[1].$row, $reserved_qty);
                    $total_reserved_qty += $reserved_qty;
                  }
                  $qty_outstanding = $qty_need - $total_reserved_qty;
                  $sheet->setCellValue('O'.$row, sprintf('%0.8f',$qty_outstanding));


                }
              }
            });
          })
          ->export('xlsx');



      }else{
        return redirect()->action('ReportFabricController@actualWidth')
        ->withErrors($validator);
      }

  }

  /******************************** REPORT CI **************************/

  /*********************************OUT ******************************/
  public function outFabric(Request $request, Builder $htmlBuilder)
  {
    $start_date = ($request->has('start_date')) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.' 00:00:00') : Carbon::today()->subDays(15);
    $end_date = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.' 23:59:59') : Carbon::now();
    $daily_date = ($request->has('daily_date')) ? Carbon::createFromFormat('d/m/Y', $request->daily_date) : Carbon::now()->format('d/m/Y');
    
    if(auth::user()->hasRole(['ppc']))$warehouse_id = '1000011';
    else $warehouse_id = auth::user()->warehouse;

    $warehouse = ($request->has('warehouse'))?$request->warehouse:$warehouse_id;

    if($request->ajax()){
      $material_out = DB::table('out_fabric_v')
      ->where('warehouse_id',$warehouse)
      ->whereBetween('actual_planning_warehouse_date', [$start_date, $end_date])
      ->orderBy('actual_planning_warehouse_date','desc');
      return DataTables::of($material_out)
      ->make(true);
    }

    $html = $htmlBuilder
    ->addColumn(['data'=>'last_movement_date', 'name'=>'last_movement_date', 'title'=>'TANGGAL KELUAR'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'PO SUPPLIER'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'ITEM CODE'])
    ->addColumn(['data'=>'nomor_roll', 'name'=>'document_no', 'title'=>'NOMOR ROLL'])
    ->addColumn(['data'=>'total_qty_preparation', 'name'=>'total_qty_preparation', 'title'=>'TOTAL QTY PREPARATION'])
    ->addColumn(['data'=>'actual_planning_warehouse_date', 'name'=>'actual_planning_warehouse_date', 'title'=>'PLANNING DATE'])
    ->addColumn(['data'=>'name', 'name'=>'name', 'title'=>'USER OUT'])
    ->addColumn(['data'=>'begin_width', 'name'=>'begin_width', 'title'=>'BEGIN WIDTH'])
    ->addColumn(['data'=>'middle_width', 'name'=>'middle_width', 'title'=>'MIDDLE WIDTH'])
    ->addColumn(['data'=>'end_width', 'name'=>'end_width', 'title'=>'END WIDTH'])
    ->addColumn(['data'=>'actual_width', 'name'=>'actual_width', 'title'=>'ACTUAL WIDTH'])
    ->addColumn(['data'=>'actual_length', 'name'=>'actual_length', 'title'=>'ACTUAL LENGTH'])
    ->addColumn(['data'=>'actual_lot', 'name'=>'actual_lot', 'title'=>'ACTUAL LOT'])
    ->addColumn(['data'=>'jenis_po', 'name'=>'actual_lot', 'title'=>'JENIS PO'])
    ->addColumn(['data'=>'barcode', 'name'=>'barcode', 'title'=>'BARCODE']);
    $start = $start_date->format('d/m/Y');
    $end = $end_date->format('d/m/Y');
    //$daily_date = $daily_date->format('d/m/Y');

    $active_tab = ($request->has('active_tab_daily'))?'daily':'data';
    return view('report_fabric.material_out',compact('start','end','daily_date','html','active_tab','warehouse'));
  }

  public function outFabricExcel(Request $request)
  { 
    $validator =  Validator::make($request->all(), [
        '__start_date' => 'required',
        '__end_date' => 'required',
        '__warehouse'=>'required',
    ]);

    if($validator->passes()){
        $start_date = ($request->__start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->__start_date.' 00:00:00') : Carbon::now()->addDays(3);
        $end_date = ($request->__end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->__end_date.' 23:59:59') : Carbon::now()->addDays(3);
        $warehouse = $request->__warehouse;
        $warehouse_name = ($warehouse=='1000001')?'AOI-1':'AOI-2';
        $material_preparation = DB::table('out_fabric_v')
        ->whereBetween('actual_planning_warehouse_date',[$start_date,$end_date])
        ->where('warehouse_id',$warehouse)
        ->orderBy('actual_planning_warehouse_date','ASC')->get();

        $file_name = 'MATERIAL_OUT_FABRIC_'.$warehouse_name.'_FROM_'.$start_date->todatestring().'_TO_'.$end_date->todatestring();
        return Excel::create($file_name,function($excel) use ($material_preparation){
          $excel->setCreator('ICT')
          ->setCompany('AOI');

          
          $excel->sheet('ACTIVE',function($sheet)use($material_preparation){
            $sheet->setCellValue('A1','TANGGAL KELUAR');
            $sheet->setCellValue('B1','PLANNING DATE');
            $sheet->setCellValue('C1','PO SUPPLIER');
            $sheet->setCellValue('D1','NAMA SUPPLIER');
            $sheet->setCellValue('E1','ITEM CODE');
            $sheet->setCellValue('F1','NOMOR ROLL');
            $sheet->setCellValue('G1','TOTAL QTY PREPARATION');
            $sheet->setCellValue('H1','BEGIN WIDTH');
            $sheet->setCellValue('I1','MIDDLE WIDTH');
            $sheet->setCellValue('J1','END WIDTH');
            $sheet->setCellValue('K1','ACTUAL WIDTH');
            $sheet->setCellValue('L1','ACTUAL LENGTH');
            $sheet->setCellValue('M1','ACTUAL LOT');
            $sheet->setCellValue('N1','USER OUT');

            
            $row=2;
            foreach ($material_preparation as $i) {  
              $sheet->setCellValue('A'.$row,$i->last_movement_date);
              $sheet->setCellValue('B'.$row,$i->actual_planning_warehouse_date);
              $sheet->setCellValue('C'.$row,$i->document_no);
              $sheet->setCellValue('D'.$row,$i->supplier_name);
              $sheet->setCellValue('E'.$row,$i->item_code);
              $sheet->setCellValue('F'.$row,$i->nomor_roll);
              $sheet->setCellValue('G'.$row,$i->total_qty_preparation);
              $sheet->setCellValue('H'.$row,$i->begin_width);
              $sheet->setCellValue('I'.$row,$i->middle_width);
              $sheet->setCellValue('J'.$row,$i->end_width);
              $sheet->setCellValue('K'.$row,$i->actual_width);
              $sheet->setCellValue('L'.$row,$i->actual_length);
              $sheet->setCellValue('M'.$row,$i->actual_lot);
              $sheet->setCellValue('N'.$row,$i->name);
              $row++;
            }
          });

        })->export('xlsx');
    }else{
      return redirect()->action('ReportFabricController@preparationFabric')
      ->withErrors($validator);
    }
  }

  public function dailyOutFabric(Request $request)
  {
    $move_date = ($request->daily_date) ? Carbon::createFromFormat('d/m/Y', $request->daily_date) : Carbon::today();
    $move_date = $move_date->format('Y-m-d');
    
    if(auth::user()->hasRole(['ppc']))$warehouse_id = '1000011';
    else $warehouse_id = auth::user()->warehouse;

    $material_out = DB::table('summary_user_out_v')
    ->where('warehouse_id',$warehouse_id)
    ->where('out_date', $move_date);

    return DataTables::of($material_out)
    ->make(true);
  }

  /*********************************END OUT */
  public function planningFabric()
  {
    $start_date = Carbon::now()->addDays(3)->format('d/m/Y');
    $end_date = $start_date;

    return view('report_fabric.planning_fabric',compact('start_date','end_date'));
  }

  public function planningFabricExport(Request $request)
  {
    $validator =  Validator::make($request->all(), [
        'start_date' => 'required',
        'end_date' => 'required',
    ]);

    if($validator->passes()){
        $start_date = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date) : Carbon::now()->addDays(3);
        $end_date = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date) : Carbon::now()->addDays(3);
        $cutting_instructions =  DB::connection('erp')
        ->select(db::raw("SELECT * FROM adempiere.wms_get_csi_date_mm(
            '".$start_date."',
            '".$end_date."'
            )
            order by warehouse asc,po_buyer asc;"
        )); 
        
        $file_name = 'MATERIAL_PLANNING_FABRIC_FROM_'.$start_date->todatestring().'_TO_'.$end_date->todatestring();
        return Excel::create($file_name,function($excel) use ($cutting_instructions){
          $excel->sheet('active',function($sheet)use($cutting_instructions){
            $sheet->setCellValue('A1','PLANNING DATE');
            $sheet->setCellValue('B1','WAREHOUSE PRODUCTION');
            $sheet->setCellValue('C1','PO BUYER');
            $sheet->setCellValue('D1','KODE ITEM');
            $sheet->setCellValue('E1','COLOR');
            $sheet->setCellValue('F1','STYLE');
            $sheet->setCellValue('G1','ARTICLE NO');
            $sheet->setCellValue('H1','JOB ORDER');
            
            $row=2;
            foreach ($cutting_instructions as $i) {  
              $po_buyer = $i->po_buyer;
              $item_code = $i->item_code;
              $planning_date = $i->datestartschedule;
              $warehouse = $i->warehouse;
              $style = $i->style;
              $article_no = $i->article_no;
              $job_order = $i->job_order;
              $item_code = $i->item_code;
              $color = $i->color;
              $uom = $i->uom;

              $sheet->setCellValue('A'.$row,$planning_date);
              $sheet->setCellValue('B'.$row,$warehouse);
              $sheet->setCellValue('C'.$row,$po_buyer);
              $sheet->setCellValue('D'.$row,$item_code);
              $sheet->setCellValue('E'.$row,$color);
              $sheet->setCellValue('F'.$row,$style);
              $sheet->setCellValue('G'.$row,$article_no);
              $sheet->setCellValue('H'.$row,$job_order);
              $row++;
            }
          });

        })
        ->export('xlsx');

    }else{
      return redirect()->action('ReportFabricController@preparationFabric')
      ->withErrors($validator);
    }
  }

  public function summaryStock(Request $request, Builder $htmlBuilder)
  {
    if(auth::user()->hasRole('admin-ict-fabric')){
      $_warehouse = auth::user()->warehouse;
      $flag = 1;
    }
    else if(auth::user()->hasRole('mm-staff')){
      $_warehouse = null;
      $flag = 2;
    }else{
      $_warehouse = auth::user()->warehouse;
      $flag = 3;
    } 

    $warehouse = ($request->warehouse)?$request->warehouse: $_warehouse;

    if($request->ajax()){
        $summary_stock = SummaryStockFabric::orderby('created_at');
        if($warehouse)
          $warehouse = $summary_stock->where('warehouse_id',$warehouse);

        return DataTables::of($summary_stock)
        ->editColumn('warehouse_id',function ($summary_stock){
            if($summary_stock->warehouse_id == '1000001') return 'Fabric AOI 1';
            elseif($summary_stock->warehouse_id == '1000011') return 'Fabric AOI 2';
        })
        ->addColumn('status_stock_warehouse',function($summary_stock){
            if($summary_stock->available_qty > 0) return 'STOK MASIH ADA';
            else return 'STOK SUDAH HABIS';
        })
        ->addColumn('status_stock_mm',function($summary_stock){
            if($summary_stock->qty_order_available > 0) return 'STOK MASIH ADA';
            else return 'STOK SUDAH HABIS';
        })
        ->addColumn('status_material',function($summary_stock){
            if($summary_stock->stock >=  $summary_stock->qty_order) return 'STOCK SUDAH DI TERIMA SEMUA';
            else return 'STOK DITERIMA SEBAGIAN';
        })
        ->addColumn('tipe',function($summary_stock){
          if($summary_stock->is_from_receiving == true ) return 'KEDATANGAN DARI SUPPLIER';
          else if($summary_stock->is_from_input_stock == true ) return 'INPUT STOK';
          else if($summary_stock->is_from_handover == true ) return 'PINDAH TANGAN';
          else return '';
        })
        ->addColumn('action',function($summary_stock){
            return view('_action',[
                'model' => $summary_stock,
                'detail' => route('report.materialStockFabric',['id'=>$summary_stock->id]),
                'recalculate' => route('report.summaryStockRecalculate',['id'=>$summary_stock->id]),
                'unlocked' => route('report.unlockSummaryMaterialStockFabric', ['id'=>$summary_stock->id]),
            ]);
        })
        ->make(true);
    }

    /*if($flag == 1){
      $html = $htmlBuilder
      ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
      ->addColumn(['data'=>'supplier_code', 'name'=>'supplier_code', 'title'=>'SUPPLIER CODE'])
      ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
      ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
      ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
      ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
      //->addColumn(['data'=>'qty_order', 'name'=>'qty_order', 'title'=>'QTY ORDER'])
      //->addColumn(['data'=>'qty_order_reserved', 'name'=>'qty_order_reserved', 'title'=>'RESERVED QTY ORDER'])
      //->addColumn(['data'=>'qty_order_available', 'name'=>'qty_order_available', 'title'=>'AVAILABLE QTY ORDER'])
      ->addColumn(['data'=>'stock', 'name'=>'stock', 'title'=>'STOCK ON WHS'])
      ->addColumn(['data'=>'reserved_qty', 'name'=>'reserved_qty', 'title'=>'RESERVED QTY STOCK'])
      ->addColumn(['data'=>'available_qty', 'name'=>'available_qty', 'title'=>'AVAILABLE QTY STOCK'])
      ->addColumn(['data'=>'status_stock_warehouse', 'name'=>'status_stock_warehouse', 'title'=>'STATUS STOCK WAREHOUSE', 'orderable' => false, 'searchable' => false])
      //->addColumn(['data'=>'status_stock_mm', 'name'=>'status_stock_mm', 'title'=>'STATUS STOCK MM', 'orderable' => false, 'searchable' => false])
      //->addColumn(['data'=>'status_material', 'name'=>'status_material', 'title'=>'STATUS MATERIAL', 'orderable' => false, 'searchable' => false])
      ->addColumn(['data'=>'warehouse_id', 'name'=>'warehouse_id', 'title'=>'LOKASI WAREHOUSE'])
      //->addColumn(['data'=>'tipe', 'name'=>'tipe', 'title'=>'ASAL STOK', 'orderable' => false, 'searchable' => false])
      ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    }
    else if($flag == 2){
      $html = $htmlBuilder
      ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
      ->addColumn(['data'=>'supplier_code', 'name'=>'supplier_code', 'title'=>'SUPPLIER CODE'])
      ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
      ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
      ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
      ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
      ->addColumn(['data'=>'qty_order', 'name'=>'qty_order', 'title'=>'QTY ORDER'])
      ->addColumn(['data'=>'stock', 'name'=>'stock', 'title'=>'STOCK ON WHS'])
      ->addColumn(['data'=>'qty_order_reserved', 'name'=>'qty_order_reserved', 'title'=>'RESERVED QTY ORDER'])
      ->addColumn(['data'=>'qty_order_available', 'name'=>'qty_order_available', 'title'=>'AVAILABLE QTY ORDER'])
      ->addColumn(['data'=>'status_stock_mm', 'name'=>'status_stock_mm', 'title'=>'STATUS STOCK'])
      ->addColumn(['data'=>'status_material', 'name'=>'status_material', 'title'=>'STATUS MATERIAL', 'orderable' => false, 'searchable' => false])
      ->addColumn(['data'=>'warehouse_id', 'name'=>'warehouse_id', 'title'=>'LOKASI WAREHOUSE'])
      //->addColumn(['data'=>'tipe', 'name'=>'tipe', 'title'=>'TIPE'])
      ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    }else if($flag == 3){
      $html = $htmlBuilder
      ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
      ->addColumn(['data'=>'supplier_code', 'name'=>'supplier_code', 'title'=>'SUPPLIER CODE'])
      ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
      ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
      ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
      ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
      ->addColumn(['data'=>'qty_order', 'name'=>'qty_order', 'title'=>'QTY ORDER'])
      ->addColumn(['data'=>'stock', 'name'=>'stock', 'title'=>'STOCK ON WHS'])
      ->addColumn(['data'=>'reserved_qty', 'name'=>'reserved_qty', 'title'=>'RESERVED QTY'])
      ->addColumn(['data'=>'available_qty', 'name'=>'available_qty', 'title'=>'AVAILABLE QTY'])
      ->addColumn(['data'=>'status_stock_warehouse', 'name'=>'status_stock_warehouse', 'title'=>'STATUS STOCK'])
      ->addColumn(['data'=>'status_material', 'name'=>'status_material', 'title'=>'STATUS MATERIAL', 'orderable' => false, 'searchable' => false])
      ->addColumn(['data'=>'warehouse_id', 'name'=>'warehouse_id', 'title'=>'LOKASI WAREHOUSE'])
      //->addColumn(['data'=>'tipe', 'name'=>'tipe', 'title'=>'TIPE'])
      ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    }*/
    
    $html = $htmlBuilder
    ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
    ->addColumn(['data'=>'supplier_code', 'name'=>'supplier_code', 'title'=>'SUPPLIER CODE'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
    ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
    ->addColumn(['data'=>'uom', 'name'=>'uom', 'title'=>'UOM'])
    ->addColumn(['data'=>'qty_order', 'name'=>'qty_order', 'title'=>'STOCK KEDATANGAN'])
    ->addColumn(['data'=>'available_qty', 'name'=>'available_qty', 'title'=>'STOCK TERSEDIA'])
    ->addColumn(['data'=>'status_stock_warehouse', 'name'=>'status_stock_warehouse', 'title'=>'STATUS STOCK WAREHOUSE', 'orderable' => false, 'searchable' => false])
    ->addColumn(['data'=>'warehouse_id', 'name'=>'warehouse_id', 'title'=>'LOKASI WAREHOUSE'])
    ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    
    return view('report_fabric_stock.index',compact('html','warehouse'));
  }

  public function summaryStockRecalculate($id,Request $request)
  {
    if(auth::user()->hasRole(['admin-ict-fabric','mm-staff','supervisor-fabric'])){
      $summary_stock_fabric = SummaryStockFabric::find($id);
      
      $is_sto_exists = MaterialOpnameFabricSummary::where('summary_stock_fabric_id',$summary_stock_fabric->id)->exists();
      if($is_sto_exists) return response()->json('stok ini pernah dilakukan sto, tidak dapat di ubah.',422);

      
    
      return response()->json(200);
    }else{
      return response()->json('FUNGSI BELUM ADA.',422);
    } 
  }

  public function materialStock(Request $request, Builder $htmlBuilder)
  {
    $summary_stock_fabric = SummaryStockFabric::find($request->id);

    if($request->ajax())
    {
      $material_stock = MaterialStock::where([
        ['summary_stock_fabric_id',$request->id],
        ['is_closing_balance',false]
      ]);

        return DataTables::of($material_stock)
        ->addColumn('status',function($material_stock){
          if($material_stock->available_qty>0){
            if($material_stock->is_used_to_relax==true || $material_stock->reserved_qty>0){
              return 'TERPAKAI SEBAGIAN';
            }
            return 'BELUM TERPAKAI';
          }else{
            return 'TERPAKAI SEMUA';
          }
        })
        
        ->addColumn('last_locator',function($material_stock){
          if($material_stock->locator_id !=null) return $material_stock->locator->code;
          else return '';
        })
        ->addColumn('action',function($material_stock){
          if(auth::user()->hasRole(['admin-ict-fabric'])){
            return view('_action',[
              'model' => $material_stock,
              'delete' => route('report.materialStockFabric.delete',['id' =>$material_stock->id]),
              'historyModal' => route('report.movementStockHistory',['id'=>$material_stock->id,'summary_stock_fabric_id'=>$material_stock->summary_stock_fabric_id]),
              'print' => route('report.materialStockFabric.printBarcode',['id'=>$material_stock->id]),
              'unlocked' => route('report.unlockMaterialStockFabric',['id'=>$material_stock->id]),
            ]);
          }else{
            return view('_action',[
              'model' => $material_stock,
              'historyModal' => route('report.movementStockHistory',['id'=>$material_stock->id,'summary_stock_fabric_id'=>$material_stock->summary_stock_fabric_id]),
              'print' => route('report.materialStockFabric.printBarcode',['id'=>$material_stock->id]),
              'unlocked' => route('report.unlockMaterialStockFabric',['id'=>$material_stock->id]),
            ]);
          }
        
        })
        ->setRowAttr([
          'style' => function($material_stock) {
              if($material_stock->available_qty > 0){
                  return  'background-color: green;color:white';
              }
          },
      ])
      ->rawColumns(['status','action'])
        ->make(true);
    }

    $html = $htmlBuilder
    ->addColumn(['data'=>'barcode_supplier', 'name'=>'barcode_supplier', 'title'=>'BARCODE'])
    ->addColumn(['data'=>'nomor_roll', 'name'=>'nomor_roll', 'title'=>'NO ROLL'])
    ->addColumn(['data'=>'batch_number', 'name'=>'batch_number', 'title'=>'BATCH NUMBER'])
    ->addColumn(['data'=>'load_actual', 'name'=>'load_actual', 'title'=>'LOT ACTUAL'])
    ->addColumn(['data'=>'begin_width', 'name'=>'begin_width', 'title'=>'BEGIN WIDTH'])
    ->addColumn(['data'=>'middle_width', 'name'=>'middle_width', 'title'=>'MIDDLE WIDTH'])
    ->addColumn(['data'=>'end_width', 'name'=>'end_width', 'title'=>'END WIDTH'])
    ->addColumn(['data'=>'actual_width', 'name'=>'actual_width', 'title'=>'ACTUAL WIDTH'])
    ->addColumn(['data'=>'qty_order', 'name'=>'qty_order', 'title'=>'QTY RECEIVING'])
    ->addColumn(['data'=>'available_qty', 'name'=>'available_qty', 'title'=>'AVAILABLE QTY'])
    ->addColumn(['data'=>'status', 'name'=>'status', 'title'=>'STATUS'])
    ->addColumn(['data'=>'tipe', 'name'=>'tipe', 'title'=>'ASAL STOK'])
    ->addColumn(['data'=>'last_locator', 'name'=>'last_locator', 'title'=>'LAST LOCATOR'])
    ->addColumn(['data'=>'source', 'name'=>'source', 'title'=>'SOURCE'])
    ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    return view('report_fabric_stock.detail',compact('html','summary_stock_fabric'));
  }

  public function materialStockDelete(Request $request)
  {
    try 
    {
        DB::beginTransaction();
        
        $material_stock = MaterialStock::find($request->id);
        $material_stock->update([
            'reserved_qty'=> $material_stock->stock,
            'is_closing_balance'=> true,
            'is_allocated'=> true,
            'is_active'=> false,
            'available_qty'=> 0,
            'deleted_at'=> carbon::now(),
        ]);
        $summary_stock_id = $material_stock->summary_stock_fabric_id;
        
        $sum_summary_stock = MaterialStock::select(
            db::raw('sum(stock) as total_stock'),
            db::raw('sum(reserved_qty) as total_reserved'),
            db::raw('sum(available_qty) as total_available')
        )
        ->where([
            ['summary_stock_fabric_id',$summary_stock_id],
            ['is_closing_balance',false],
        ])
        ->first();
    
        SummaryStockFabric::where('id',$summary_stock_id)->update([
            'stock' => $sum_summary_stock->total_stock,
            'reserved_qty' => $sum_summary_stock->total_reserved,
            'available_qty' => $sum_summary_stock->total_available,
        ]);
        DB::commit();

        return response()->json(200);
    } catch (Exception $e) {
        DB::rollBack();
        $message = $e->getMessage();
        ErrorHandler::db($message);
    }
  }

  public function materialStockPrintBarcode(Request $request)
  {
    $material_stocks  = MaterialStock::where('id',$request->id)->get();
    return view('report_fabric_stock.reprint_barcode_supplier',compact('material_stocks'));
  }

  public function stockExcel(Request $request)
  {
    
    
    
  }

  public function movementStockHistory(Request $request)
  {
    
  }

 

  public function handoverMaterial(Request $request, Builder $htmlBuilder)
  {
    
    if(auth::user()->hasRole(['admin-ict-fabric','mm-staff'])) $_warehouse = auth::user()->warehouse;
    else $_warehouse = auth::user()->warehouse;
    
    $warehouse=($request->warehouse)?$request->warehouse: $_warehouse;
    
    if($request->ajax()){
      $allocation_items = $this->getDataAllocation($warehouse);
      return DataTables::of($allocation_items)
      ->editColumn('warehouse_id',function ($allocation_items){
          if($allocation_items->warehouse_id == '1000001') return 'Fabric AOI 1';
          elseif($allocation_items->warehouse_id == '1000011')  return 'Fabric AOI 2';
      })
      ->addColumn('action',function($allocation_items){
        return view('_action',[
          'model' => $allocation_items,
          'detail' => route('report.handoverMaterialDetail',
          [
            'id'=>$allocation_items->id
          ])
        ]);
      })
      ->make(true);
    }
    
    $html = $htmlBuilder
    ->addColumn(['data'=>'created_at', 'name'=>'created_at', 'title'=>'TANGGAL DI BUAT'])
    ->addColumn(['data'=>'complete_date', 'name'=>'complete_date', 'title'=>'TANGGAL SELESAI'])
    ->addColumn(['data'=>'handover_document_no', 'name'=>'handover_document_no', 'title'=>'NO PT'])
    ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
    ->addColumn(['data'=>'item_desc', 'name'=>'item_desc', 'title'=>'DEKSRIPSI ITEM'])
    ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
    ->addColumn(['data'=>'total_qty_handover', 'name'=>'total_qty_handover', 'title'=>'TOTAL QTY HANDOVER'])
    ->addColumn(['data'=>'total_qty_outstanding', 'name'=>'total_qty_outstanding', 'title'=>'TOTAL QTY OUTSTANDING'])
    ->addColumn(['data'=>'total_qty_supply', 'name'=>'total_qty_supply', 'title'=>'TOTAL QTY SUPPLAI'])
    ->addColumn(['data'=>'warehouse_id', 'name'=>'warehouse_id', 'title'=>'LOKASI WAREHOUSE'])
    ->addColumn(['data'=>'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
    
    return view('report_fabric.handover_material',compact('html','warehouse'));
  }

  public function handoverMaterialDetail(Request $request,Builder $htmlBuilder)
  {

    if($request->ajax()){
      $material_handover = MaterialRollHandoverFabric::where('summary_handover_material_id',$request->id);
      return DataTables::of($material_handover)
      ->editColumn('warehouse_from_id',function ($material_handover){
          if($material_handover->warehouse_from_id == '1000001')
              return 'Fabric AOI 1';
          elseif($material_handover->warehouse_from_id == '1000011')
              return 'Fabric AOI 2';
      })
      ->editColumn('warehouse_to_id',function ($material_handover){
        if($material_handover->warehouse_to_id == '1000001')
            return 'Fabric AOI 1';
        elseif($material_handover->warehouse_to_id == '1000011')
            return 'Fabric AOI 2';
      })
      ->editColumn('user_receive_id',function ($material_handover){
        if($material_handover->user_receive_id){
          return $material_handover->userReceive->name;
        }
        
      })
      ->addColumn('action',function($material_handover){
        return view('_action',[
          'model' => $material_handover,
          'print' => route('report.handoverMaterialDetail.printBarcode',
          [
            'id'=>$material_handover->id
          ])
        ]);
      })
      ->make(true);
    }

    $html = $htmlBuilder
    ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
    ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'WARNA'])
    ->addColumn(['data'=>'nomor_roll', 'name'=>'nomor_roll', 'title'=>'NOMOR ROLL'])
    ->addColumn(['data'=>'batch_number', 'name'=>'batch_number', 'title'=>'BATCH NUMBER'])
    ->addColumn(['data'=>'actual_lot', 'name'=>'actual_lot', 'title'=>'ACTUAL LOT'])
    ->addColumn(['data'=>'actual_width', 'name'=>'actual_width', 'title'=>'ACTUAL WIDTH'])
    ->addColumn(['data'=>'qty_handover', 'name'=>'qty_handover', 'title'=>'QTY HANDOVER'])
    ->addColumn(['data'=>'warehouse_from_id', 'name'=>'warehouse_from_id', 'title'=>'WAREHOUSE SUMBER'])
    ->addColumn(['data'=>'warehouse_to_id', 'name'=>'warehouse_to_id', 'title'=>'WAREHOUSE TUJUAN'])
    ->addColumn(['data'=>'user_receive_id', 'name'=>'user_receive_id', 'title'=>'USER RECEIVE'])
    ->addColumn(['data'=>'receive_date', 'name'=>'receive_date', 'title'=>'RECEIVE DATE'])
    ->addColumn(['data'=>'barcode', 'name'=>'barcode', 'title'=>'BARCODE'])
    ->addColumn(['data'=>'action', 'name'=>'action', 'title'=>'ACTION']);
    
    return view('report_fabric.detail_handover_material',compact('html'));
  }

  public function handoverMaterialPrintBarcode(Request $request){
    $material_stocks  = MaterialRollHandoverFabric::where('id',$request->id)->get();
    return view('report_fabric.reprint_barcode_handover',compact('material_stocks'));
  }

  public function handoverMaterialExcel(Request $request){
    if(auth::user()->hasRole(['admin-ict-fabric','mm-staff'])) $_warehouse = null;
    else $_warehouse = auth::user()->warehouse;
    
    $warehouse = ($request->warehouse)?$request->warehouse: $_warehouse;

    $allocation_items = $this->getDataAllocation($warehouse);
    //mengatur nama file
    $file_name = 'MATERIAL_HANDOVER_'.Carbon::now()->format('yyyymmdd');
    return Excel::create($file_name,function($excel) use($allocation_items){
      $excel->setCreator('ICT')
      ->setCompany('AOI');
      $excel->sheet('active',function($sheet) use($allocation_items){
        $this->handoverMaterialExcelHeader($sheet);
        $row=2;
        $this->handoverMaterialExcelValue($sheet,$allocation_items,$row);
      });
    })
    ->export('xlsx');
  }

  private function getDataAllocation($warehouse){ 
    $summary_handover_materials = SummaryHandoverMaterial::select('summary_handover_materials.created_at','summary_handover_materials.complete_date','summary_handover_materials.handover_document_no','summary_handover_materials.id','document_no','supplier_name','c_bpartner_id','summary_handover_materials.item_code','items.item_desc','items.color','total_qty_handover','total_qty_outstanding','total_qty_supply','warehouse_id')
    ->leftJoin('items',function($join){
      $join->select('color','item_desc');
      $join->on(db::raw('upper(items.item_code)'),db::raw('upper(summary_handover_materials.item_code)'));
    });

    if($warehouse)
      $summary_handover_materials = $summary_handover_materials->where('warehouse_id',$warehouse);
    
    $summary_handover_materials = $summary_handover_materials->whereNotNull('handover_document_no')->get();
    return $summary_handover_materials;
  }

  //fungsi untuk set header pada sheet summary di export stock
  private function handoverMaterialExcelHeader($sheet){
      $sheet->setCellValue('A1','NAMA SUPPLIER');
      $sheet->setCellValue('B1','NO. PO SUPPLIER');
      $sheet->setCellValue('D1','KODE ITEM');
      $sheet->setCellValue('D1','DEKSRIPSI ITEM');
      $sheet->setCellValue('E1','WARNA');
      $sheet->setCellValue('F1','TOTAL QTY HANDOVER');
      $sheet->setCellValue('G1','TOTAL QTY OUTSTANDING');
      $sheet->setCellValue('H1','TOTAL QTY SUPPLAI');
      $sheet->setCellValue('I1','LOKASI WAREHOUSE');
  }

  private function handoverMaterialExcelValue($sheet,$data,$row){
    foreach ($data as $i){
      if($i->warehouse_id == '1000001') $warehouse = 'Fabric AOI 1';
      else if($i->warehouse_id == '1000011') $warehouse = 'Fabric AOI 2';

      $sheet->setCellValue('A'.$row,$i->supplier_name);
      $sheet->setCellValue('B'.$row,$i->document_no);
      $sheet->setCellValue('C'.$row,$i->item_code);
      $sheet->setCellValue('D'.$row,$i->item_desc);
      $sheet->setCellValue('E'.$row,$i->color);
      $sheet->setCellValue('F'.$row,$i->total_qty_handover);
      $sheet->setCellValue('G'.$row,$i->total_qty_outstanding);
      $sheet->setCellValue('H'.$row,$i->total_qty_supply);
      $sheet->setCellValue('I'.$row,$warehouse);
      $row++;
    }
  }

  public function inspectQc(Request $request, Builder $htmlBuilder){
    $start_date = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date." 00:00:00") : Carbon::now()->subDays(30);
    $end_date = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date." 23:59:59") : Carbon::now();
    if($request->ajax()){

      $inspect_lab_qc = db::table('inspect_lab_qc_report_v')
      ->where('warehouse_id',auth::user()->warehouse)
      ->whereBetween('receiving_date',[$start_date,$end_date])
      ->orderBy('inspection_date','asc');

      return DataTables::of($inspect_lab_qc)
      ->addColumn('action',function($inspect_lab_qc){
      return view('_action',[
          'model' => $inspect_lab_qc,
          'detail' => route('report.fabric.detailInspectQc',
          [
            'id'=>$inspect_lab_qc->id,
            'percentage'=>$inspect_lab_qc->percentage,
            'point_1'=>$inspect_lab_qc->point_1,
            'point_2'=>$inspect_lab_qc->point_2,
            'point_3'=>$inspect_lab_qc->point_3,
            'point_4'=>$inspect_lab_qc->point_4,
            'total_point'=>$inspect_lab_qc->total_point
          ])
        ]);
      })
      ->make(true);
    }

    $html = $htmlBuilder
    ->addColumn(['data'=>'inspection_date', 'name'=>'inspection_date', 'title'=>'INSPECT DATE'])
    ->addColumn(['data'=>'user_name', 'name'=>'user_name', 'title'=>'INSPECT USER'])
    ->addColumn(['data'=>'receiving_date', 'name'=>'receiving_date', 'title'=>'RECEIVE DATE'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'DOCUMENT NO'])
    ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'SUPPLIER NAME'])
    ->addColumn(['data'=>'no_invoice', 'name'=>'no_invoice', 'title'=>'INVOICE NO'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'ITEM CODE'])
    ->addColumn(['data'=>'color', 'name'=>'color', 'title'=>'COLOR'])
    ->addColumn(['data'=>'total_point', 'name'=>'total_point', 'title'=>'TOTAL POINT'])
    ->addColumn(['data'=>'percentage', 'name'=>'percentage', 'title'=>'PERCENTAGE'])
    ->addColumn(['data'=>'remark', 'name'=>'remark', 'title'=>'RESULT'])
    ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);
    
    $start = $start_date->format('d/m/Y');
    $end = $end_date->format('d/m/Y');
    $daily_date = Carbon::now()->format('d/m/Y');
    $active_tab = ($request->has('active_tab_daily'))?'summary':'detail';
    return view('report_fabric.inspect_qc',compact('html','start','end','daily_date','active_tab'));
  }

  

 

  public function inspectQcDetail(Request $request){
    if($request->has('id')){
      $data =DetailMaterialCheck::where('material_check_id',$request->id)
      ->orderBy('start_point_check','asc')
      ->get();
      if($data!=null){
        $header=$request;
        return view('report_fabric.detail_inspect_qc',compact('data','header'));
      }else{
        return response()->json('Tidak ada Data Untuk Material Check ID '.$request->id,422);
      }

    }else{
      if(!$request->has('id')){
        return response()->json('ID Material Check Kosong !,silahkan coba refresh',422);
      }
    }
  }

  public function inspectQcExportToExcel(Request $request){
    $start_date = ($request->__start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->__start_date." 00:00:00") : Carbon::now()->subDays(30);
    $end_date = ($request->__end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->__end_date." 23:59:59") : Carbon::now();
    $data = db::table('inspect_lab_qc_report_v')
    ->whereBetween('receiving_date',[$start_date,$end_date])
    ->where('warehouse_id',auth::user()->warehouse)
    ->orderBy('inspection_date','asc')
    ->get();

    $file_name = 'LIST REPORT INSPECT LAB QC '.$start_date->format('d/m/Y').' TO '.$end_date->format('d/m/Y');
    return Excel::create($file_name,function($excel) use ($data){
      $excel->setCreator('ICT')
      ->setCompany('AOI');
      $excel->sheet('ACTIVE',function($sheet) use ($data){
        $this->listHeaderDataInspectQcExportToExcel($sheet);
        $row=2;
        $this->listBodyDataInspectQcExportToExcel($sheet,$data,$row);
      });
    })->export('xlsx');
  }

  private function listHeaderDataInspectQcExportToExcel($sheet){
    //INSPECTION DATE
    $sheet->setCellValue('A1','INSPECTION DATE');
    //RECEIVING DATE
    $sheet->setCellValue('B1','RECEIVING DATE');
    //DOCUMENT NO
    $sheet->setCellValue('C1','DOCUMENT NO');
    //SUPPLIER NAME
    $sheet->setCellValue('D1','SUPPLIER NAME');
    //INVOICE NO
    $sheet->setCellValue('E1','INVOICE NO');
    //ITEM CODE
    $sheet->setCellValue('F1','ITEM CODE');
    //COLOR
    $sheet->setCellValue('G1','COLOR');
    //TOTAL BATCH NUMBER
    $sheet->setCellValue('H1','TOTAL POINT');
    //TOTAL INSPECTION DATE
    $sheet->setCellValue('I1','PERCENTAGE');
    //STATUS
    $sheet->setCellValue('J1','RESULT');
  }

  private function listBodyDataInspectQcExportToExcel($sheet,$data,$row){
    foreach ($data as $i){
      //INSPECTION DATE
      $sheet->setCellValue('A'.$row,$i->inspection_date);
      //RECEIVING DATE
      $sheet->setCellValue('B'.$row,$i->receiving_date);
      //DOCUMENT NO
      $sheet->setCellValue('C'.$row,$i->document_no);
      //SUPPLIER NAME
      $sheet->setCellValue('D'.$row,$i->supplier_name);
      //INVOICE NO
      $sheet->setCellValue('E'.$row,$i->no_invoice);
      //ITEM CODE
      $sheet->setCellValue('F'.$row,$i->item_code);
      //COLOR
      $sheet->setCellValue('G'.$row,$i->color);
      //TOTAL BATCH NUMBER
      $sheet->setCellValue('H'.$row,$i->total_point);
      //TOTAL INSPECTION BATCH
      $sheet->setCellValue('I'.$row,$i->percentage);
      //STATUS
      $sheet->setCellValue('J'.$row,$i->remark);

      $row++;
    }
    return $row;
  }

  public function unlockSummaryStockFabric(Request $request)
  {
    

  }

  public function unlockMaterialStockFabric($id)
  {
    
    

  }

  public function qtyRejectFIR(Request $request, Builder $htmlBuilder)
  {
    if($request->ajax()){
    $data = DB::select(db::raw("SELECT * FROM qty_reject_confirm where status_warehouse='f'"));

    return DataTables::of($data)
      ->addColumn('action',function($data){
        return view('_action',[
          'model' => $data,
          'confirm' => route('report.qtyRejectFIRFabricConfirm', $data->id)
        ]);
      })
      ->make(true);
    }
    
    $html = $htmlBuilder
    ->addColumn(['data'=>'created_at', 'name'=>'created_at', 'title'=>'DATE INSPECT'])
    ->addColumn(['data'=>'document_no', 'name'=>'document_no', 'title'=>'NO. PO SUPPLIER'])
    ->addColumn(['data'=>'supplier_name', 'name'=>'supplier_name', 'title'=>'NAMA SUPPLIER'])
    ->addColumn(['data'=>'item_code', 'name'=>'item_code', 'title'=>'KODE ITEM'])
    ->addColumn(['data'=>'no_invoice', 'name'=>'no_invoice', 'title'=>'NO. INVOICE'])    
    ->addColumn(['data'=>'batch_number', 'name'=>'batch_number', 'title'=>'NO. BATCH'])
    ->addColumn(['data'=>'nomor_roll', 'name'=>'nomor_roll', 'title'=>'NO. ROLL'])
    ->addColumn(['data'=>'qty_reject', 'name'=>'qty_reject', 'title'=>'QTY REJECT'])
    ->addColumn(['data'=>'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center']);

    return view('report_fabric.qty_reject_fir',compact('html'));

    
  }

  public function qtyRejectFIRConfirm($id)
  {
    $material_check = MaterialCheck::find($id);
    $material_check->status_warehouse ='true';
    $material_check->save();
    //
    if($material_check->save())
    {
      $material_stock_id = $material_check->material_stock_id;
      $qty_reject = $material_check->qty_reject;
      $material_stock = MaterialStock::find($material_stock_id);
      $available_qty = $material_stock->available_qty;
      $new_qty = $available_qty - $qty_reject;
      $material_stock->available_qty = $new_qty;
      $material_stock->save();

      return response()->json('success',200);
    }
    else
    {
      return response()->json('Hubungi ICT',422);
    }

  }
  public function preparationFabricRemark(Request $request){
      $id = $request->id;
      $remark = $request->remark;
      $material_preparation_fabric = MaterialPreparationFabric::find($id);
      $material_preparation_fabric->remark = $remark;
      $material_preparation_fabric->save();
      return response()->json('success',200);
  } 
    
}