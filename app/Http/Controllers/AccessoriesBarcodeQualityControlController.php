<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Models\MaterialPreparation;

class AccessoriesBarcodeQualityControlController extends Controller
{
    public function index(Request $request)
    {
        $warehouse_id   = $request->warehouse_id;

        if($request->barcode) $barcode = trim($request->barcode);
        else $barcode = trim($request->po_buyer_id);

        if($barcode==null) return view('accessories_barcode_material_quality_control.index');
        
        $items = MaterialPreparation::where([
            ['barcode',$barcode],
            ['warehouse',$warehouse_id],
        ])
        ->get();

        
        if($items->count() > 0) return view('accessories_barcode_material_quality_control.barcode', compact('items'));
        else return view('accessories_barcode_material_quality_control.index');
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer       = $request->po_buyer;
        $item_code      = $request->item_code;
        $warehouse_id   = $request->warehouse_id;

        $lists = MaterialPreparation::select('barcode','document_no','po_buyer','item_code','style','article_no')
        ->where(function($query) use ($po_buyer,$item_code,$warehouse_id)
        { 
            $query->where([
                ['po_buyer','like',"%$po_buyer%"],
                ['item_code','like',"%$item_code%"],
                ['warehouse',$warehouse_id],
            ]);
        })
        ->whereIn('id',function($query)
        {
            $query->select('material_preparation_id')
            ->from('material_checks')
            ->where('status','REJECT')
            ->groupby('material_preparation_id');
        })
        ->paginate(10);
        
        return view('accessories_barcode_material_quality_control._po_buyer_picklist',compact('lists'));
    }
}
