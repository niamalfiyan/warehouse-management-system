<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use DB;
use Auth;
use Mail;
use Config;
use Validator;
use Carbon\Carbon;

use App\Models\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function sendResetLinkEmail(Request $request){
        $validator =  Validator::make($request->all(), [ 
            'email' => 'required|email'
        ]); 
        
        if ($validator->passes()) { 
            $email = $request->email;
        
            if ($email)
                $user = User::where('email', '=', $email)
                    ->first();
            
            if (!$user) 
                return response()->json('1',422);

            $datetime = Carbon::now()->format('YmdHi');
            $token = base64_encode(hash_hmac('sha256', $datetime|$email, Config::get('app.reminder_key'), true));
            $token = strtr($token, ['/' => '_', '+' => '-', '=' => '~']);

            Mail::send('email.forget_password', ['user' => $user, 'datetime' => $datetime, 'token' => $token], function($message) use ($user)
            {
                $message->from('no-reply@aoi.co.id','WMS System');
                $message->to($user->email);
                $message->subject("Forget Password");
            });
            return response()->json('success',200);
        }else{
            return response()->json([ 
                'errors' => $validator->getMessageBag()->toArray() 
            ],400);  
        }
       

        /*return redirect()->action('Admin\Auth\LoginController@showLoginForm')
            ->with('_SEND_FORGET_PASS.OK', true);*/
    }
}
