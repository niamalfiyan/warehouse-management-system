<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\Locator;
use App\Models\Barcode;
use App\Models\Temporary;
use App\Models\UomConversion;
use App\Models\MaterialArrival;
use App\Models\AutoAllocation;
use App\Models\MaterialExclude;
use App\Models\MaterialMovement;
use App\Models\MaterialBacklog;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\DetailMaterialArrival;
use App\Models\MaterialMovementLine;
use App\Models\MaterialReadyPreparation;
use App\Models\DetailMaterialPreparation;



use App\Http\Controllers\MaterialArrivalController;

class AccessoriesBarcodeBillOfMaterialController extends Controller
{
    public function index()
    {
       return view('accessories_barcode_bill_of_material.index');
    }

    public function upload()
    {
        return view('accessories_barcode_bill_of_material.upload');
    }

    public function exportFileUpload()
    {
         return Excel::create('upload_bom',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','PO_BUYER');
                $sheet->setCellValue('B1','ITEM_CODE');
                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@'
                ));
            });

             $excel->sheet('list_item', function($sheet){
                $material_ilas = MaterialExclude::select('item_code')
                ->where(function($query){
                    $query->where('is_ila',true)
                    ->orWhere('is_from_buyer',true);
                })
                ->groupby('item_code')
                ->get();

                $sheet->setCellValue('A1','#');
                $sheet->setCellValue('B1','ITEM CODE');

                $index = 0;
                foreach ($material_ilas as $key => $material_ila) {
                    $row = $index + 2;
                    $sheet->setCellValue('A'.$row, ($index+1));
                    $sheet->setCellValue('B'.$row, $material_ila->item_code);
                    $index++;
                }
            });

            $excel->setActiveSheetIndex(0);

        })->export('xlsx');
    }

    public function import(Request $request)
    {
        $array = array();
        $user = auth::user();

        // set_time_limit(300);
        
        if( auth::user()->nik == '170700223' || auth::user()->nik == '170500005' || auth::user()->nik == '160309938' || auth::user()->nik == '160209757' || auth::user()->nik == '160309916' || $user->hasRole(['admin-ict-acc','supervisor'])){
            $material_ilas = MaterialExclude::select('item_code')
            ->where(function($query) use ($user){
                $query->Where('is_paxar',true);

                if(auth::user()->nik == '160209757' || auth::user()->nik == '170700223'|| auth::user()->nik == '160309916' || auth::user()->nik == '170500005' || auth::user()->nik == '160309938' || $user->hasRole(['admin-ict-acc','supervisor']))
                    $query->orwhere('is_ila',true)
                    ->orWhere('is_from_buyer',true);
            })
            ->groupby('item_code')
            ->get()
            ->toArray();
        }else
        {
            $material_ilas = MaterialExclude::select('item_code')
            ->where(function($query){
                $query->where('is_ila',true)
                ->orWhere('is_from_buyer',true);
            })
            ->groupby('item_code')
            ->get()
            ->toArray();
        }


        if($request->hasFile('upload_file')){
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count()){
                //return response()->json($data);
               
                foreach ($data as $key => $value) 
                {
                    $po_buyer   = trim(strtoupper($value->po_buyer));
                    $item_code  = trim(strtoupper($value->item_code));

                    $material_requirements = MaterialRequirement::whereIn('item_code',$material_ilas)
                    ->where([
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(trim(item_code))'),$item_code],
                    ])
                    ->get();

                    // if($material_requirements->count() < 1) {
                    //     ErpMaterialRequirementController::getPobuyer($po_buyer, '');
                    //     ErpMaterialRequirementController::getSummaryBuyerPerPart($po_buyer, '');
                    //     ErpMaterialRequirementController::getSummaryBuyer($po_buyer, '');
                    // }

                    // foreach ($material_requirements as $key_1 => $material_requirement) 
                    //     {
                    //         $material_preparation = MaterialPreparation::
                    //         where([
                    //             ['po_buyer',$po_buyer],
                    //             [db::raw('upper(trim(item_code))'),$item_code],
                    //             ['style',$material_requirement->style],
                    //             ['article_no',$material_requirement->article_no],
                    //             ['barcode', 'like', '%I%'],
                    //         ])
                    //         ->orderBy('last_movement_date', 'desc')
                    //         ->first();

                    //         $qty_free_stock = MaterialPreparation::where([
                    //             ['po_buyer',$po_buyer],
                    //             [db::raw('upper(trim(item_code))'),$item_code],
                    //             ['style',$material_requirement->style],
                    //             ['article_no',$material_requirement->article_no],
                    //             ['last_status_movement', '!=', 'out-handover']
                    //         ])
                    //         ->sum('qty_conversion');
                        
                    //         if($qty_free_stock > 0) {
                    //             if($material_requirement->qty_required - $qty_free_stock > 0)
                    //             {
                    //                 $qty_conversion = $material_requirement->qty_required - $qty_free_stock;
                    //             }
                    //             else
                    //             {
                    //                 $qty_conversion = 0;
                    //             }
                    //         }
                    //         else {
                    //             $qty_conversion = $material_requirement->qty_required;
                    //         }

                    //         if(!$material_preparation) {
                    //             $obj                            = new stdClass();
                    //             $obj->material_preparation_id   = -1;
                    //             $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                    //             $obj->item_code                 = trim(strtoupper($value->item_code));
                    //             $obj->style                     = $material_requirement->style;
                    //             $obj->category                  = $material_requirement->category;
                    //             $obj->job_order                 = $material_requirement->job_order;
                    //             $obj->article_no                = $material_requirement->article_no;
                    //             $obj->uom                       = $material_requirement->uom;
                    //             $obj->qty                       = $qty_conversion;
                    //             $obj->print_date                = carbon::now()->toDateTimeString();
                    //             $obj->result                    = 'READY TO PRINT';
                    //         } else {

                    //             if(($qty_conversion - $material_preparation->qty_conversion) > 0) {
                    //                 $qty_conversion  = $qty_conversion - $material_preparation->qty_conversion;
                    //                 $obj                            = new stdClass();
                    //                 $obj->material_preparation_id   = -1;
                    //                 $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                    //                 $obj->item_code                 = trim(strtoupper($value->item_code));
                    //                 $obj->style                     = $material_requirement->style;
                    //                 $obj->category                  = $material_requirement->category;
                    //                 $obj->job_order                 = $material_requirement->job_order;
                    //                 $obj->article_no                = $material_requirement->article_no;
                    //                 $obj->uom                       = $material_requirement->uom;
                    //                 $obj->qty                       = $qty_conversion;
                    //                 $obj->print_date                = carbon::now()->toDateTimeString();
                    //                 $obj->result                    = 'READY TO PRINT';

                    //             } else {
                    //                 if($material_preparation->last_status_movement == 'reject') {
                    //                     $obj                            = new stdClass();
                    //                     $obj->material_preparation_id   = -1;
                    //                     $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                    //                     $obj->item_code                 = trim(strtoupper($value->item_code));
                    //                     $obj->style                     = $material_requirement->style;
                    //                     $obj->category                  = $material_requirement->category;
                    //                     $obj->job_order                 = $material_requirement->job_order;
                    //                     $obj->article_no                = $material_requirement->article_no;
                    //                     $obj->uom                       = $material_requirement->uom;
                    //                     $obj->qty                       = $qty_conversion;
                    //                     $obj->print_date                = carbon::now()->toDateTimeString();
                    //                     $obj->result                    = 'READY TO PRINT';
                    //                 } else {
                    //                     $_status = '';

                    //                     $additional  = $material_preparation->is_additional;
                    //                     if($additional)
                    //                     {
                    //                         $is_addtional = 'DENGAN ADDITIONAL';
                    //                     }
                    //                     else
                    //                     {
                    //                         $is_addtional = '';
                    //                     }



                    //                     if($material_preparation->warehouse == '1000002') $warehouse = 'ACC AOI 1';
                    //                     if($material_preparation->warehouse == '1000013') $warehouse = 'ACC AOI 2';

                    //                     if($material_preparation->last_status_movement != 'in' || $material_preparation->last_status_movement != 'out' || $material_preparation->last_status_movement != 'receiving')
                    //                     {
                    //                         $_material_preparation_id = $material_preparation->id;
                    //                         $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$warehouse;
                    //                     }


                    //                     if($material_preparation->last_status_movement == 'in' || $material_preparation->last_status_movement == 'out')
                    //                     {
                    //                         $destination = Locator::find($material_preparation->last_locator_id);
                    //                         $_material_preparation_id = 0;
                                
                    //                         if($material_preparation->last_status_movement == 'receiving') $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse;
                    //                         if($material_preparation->last_status_movement == 'in') $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse.' dan sudah masuk kedalam rak '.$destination->code;
                    //                         if($material_preparation->last_status_movement == 'out')  $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse.' dan sudah dikeluarkan ke '.$destination->code;
                    //                     }

                    //                     $qty_required   = $qty_conversion;
                    //                     $qty_conversion = $material_preparation->qty_conversion;
                                        
                    //                     if($qty_required != $qty_conversion) $_qty_conversion = $qty_required;
                    //                     else $_qty_conversion = $qty_conversion;

                    //                     $obj                            = new stdClass();
                    //                     $obj->material_preparation_id   = $_material_preparation_id;
                    //                     $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                    //                     $obj->item_code                 = trim(strtoupper($value->item_code));
                    //                     $obj->job_order                 = $material_preparation->job_order;
                    //                     $obj->article_no                = $material_preparation->article_no;
                    //                     $obj->style                     = $material_preparation->style;
                    //                     $obj->uom                       = $material_preparation->uom_conversion;
                    //                     $obj->qty                       = $_qty_conversion;
                    //                     $obj->result                    = $_status;
                    //                 }
                    //         }
                    //         }

                    //     }


                    if($material_requirements->count() > 0)
                    {
                        foreach ($material_requirements as $key_1 => $material_requirement) 
                        {
                            $material_preparation = MaterialPreparation::
                            where([
                                ['po_buyer',$po_buyer],
                                [db::raw('upper(trim(item_code))'),$item_code],
                                ['style',$material_requirement->style],
                                ['article_no',$material_requirement->article_no],
                                ['barcode', 'like', '%I%'],
                            ])
                            ->orderBy('last_movement_date', 'desc')
                            ->first();

                            $qty_free_stock = MaterialPreparation::where([
                                ['po_buyer',$po_buyer],
                                [db::raw('upper(trim(item_code))'),$item_code],
                                ['style',$material_requirement->style],
                                ['article_no',$material_requirement->article_no],
                                ['last_status_movement', '!=', 'out-handover']
                            ])
                            ->sum('qty_conversion');
                        
                            if($qty_free_stock > 0) {
                                if($material_requirement->qty_required - $qty_free_stock > 0)
                                {
                                    $qty_conversion = $material_requirement->qty_required - $qty_free_stock;
                                }
                                else
                                {
                                    $qty_conversion = 0;
                                }
                            }
                            else {
                                $qty_conversion = $material_requirement->qty_required;
                            }

                            if(!$material_preparation) {
                                $obj                            = new stdClass();
                                $obj->material_preparation_id   = -1;
                                $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                                $obj->item_code                 = trim(strtoupper($value->item_code));
                                $obj->style                     = $material_requirement->style;
                                $obj->category                  = $material_requirement->category;
                                $obj->job_order                 = $material_requirement->job_order;
                                $obj->article_no                = $material_requirement->article_no;
                                $obj->uom                       = $material_requirement->uom;
                                $obj->qty                       = $qty_conversion;
                                $obj->print_date                = carbon::now()->toDateTimeString();
                                $obj->result                    = 'READY TO PRINT';
                            } else {

                                if(($qty_conversion - $material_preparation->qty_conversion) > 0) {
                                    $qty_conversion  = $qty_conversion - $material_preparation->qty_conversion;
                                    $obj                            = new stdClass();
                                    $obj->material_preparation_id   = -1;
                                    $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                                    $obj->item_code                 = trim(strtoupper($value->item_code));
                                    $obj->style                     = $material_requirement->style;
                                    $obj->category                  = $material_requirement->category;
                                    $obj->job_order                 = $material_requirement->job_order;
                                    $obj->article_no                = $material_requirement->article_no;
                                    $obj->uom                       = $material_requirement->uom;
                                    $obj->qty                       = $qty_conversion;
                                    $obj->print_date                = carbon::now()->toDateTimeString();
                                    $obj->result                    = 'READY TO PRINT';

                                } else {
                                    if($material_preparation->last_status_movement == 'reject') {
                                        $obj                            = new stdClass();
                                        $obj->material_preparation_id   = -1;
                                        $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                                        $obj->item_code                 = trim(strtoupper($value->item_code));
                                        $obj->style                     = $material_requirement->style;
                                        $obj->category                  = $material_requirement->category;
                                        $obj->job_order                 = $material_requirement->job_order;
                                        $obj->article_no                = $material_requirement->article_no;
                                        $obj->uom                       = $material_requirement->uom;
                                        $obj->qty                       = $qty_conversion;
                                        $obj->print_date                = carbon::now()->toDateTimeString();
                                        $obj->result                    = 'READY TO PRINT';
                                    } else {
                                        $_status = '';

                                        $additional  = $material_preparation->is_additional;
                                        if($additional)
                                        {
                                            $is_addtional = 'DENGAN ADDITIONAL';
                                        }
                                        else
                                        {
                                            $is_addtional = '';
                                        }



                                        if($material_preparation->warehouse == '1000002') $warehouse = 'ACC AOI 1';
                                        if($material_preparation->warehouse == '1000013') $warehouse = 'ACC AOI 2';

                                        if($material_preparation->last_status_movement != 'in' || $material_preparation->last_status_movement != 'out' || $material_preparation->last_status_movement != 'receiving')
                                        {
                                            $_material_preparation_id = $material_preparation->id;
                                            $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$warehouse;
                                        }


                                        if($material_preparation->last_status_movement == 'in' || $material_preparation->last_status_movement == 'out')
                                        {
                                            $destination = Locator::find($material_preparation->last_locator_id);
                                            $_material_preparation_id = 0;
                                
                                            if($material_preparation->last_status_movement == 'receiving') $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse;
                                            if($material_preparation->last_status_movement == 'in') $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse.' dan sudah masuk kedalam rak '.$destination->code;
                                            if($material_preparation->last_status_movement == 'out')  $_status = 'SUDAH DICETAK, '.$material_preparation->created_at.' oleh '.$material_preparation->user->name.' '.$is_addtional.' '.$warehouse.' dan sudah dikeluarkan ke '.$destination->code;
                                        }

                                        $qty_required   = $qty_conversion;
                                        $qty_conversion = $material_preparation->qty_conversion;
                                        
                                        if($qty_required != $qty_conversion) $_qty_conversion = $qty_required;
                                        else $_qty_conversion = $qty_conversion;

                                        $obj                            = new stdClass();
                                        $obj->material_preparation_id   = $_material_preparation_id;
                                        $obj->po_buyer                  = trim(strtoupper($value->po_buyer));
                                        $obj->item_code                 = trim(strtoupper($value->item_code));
                                        $obj->job_order                 = $material_preparation->job_order;
                                        $obj->article_no                = $material_preparation->article_no;
                                        $obj->style                     = $material_preparation->style;
                                        $obj->uom                       = $material_preparation->uom_conversion;
                                        $obj->qty                       = $_qty_conversion;
                                        $obj->result                    = $_status;
                                    }
                            }
                            }

                        }
                    }else
                    {
                        $obj                                    = new stdClass();
                        $obj->material_preparation_id           = 0;
                        $obj->po_buyer                          = trim(strtoupper($value->po_buyer));
                        $obj->item_code                         = trim(strtoupper($value->item_code));
                        $obj->style                             = null;
                        $obj->uom                               = null;
                        $obj->qty                               = null;
                        $obj->result                            = 'ITEM UNTUK PO BUYER INI TIDAK DITEMUKAN,SILAHKAN HUBUNGI ICT';
                    }

                    $array [] = $obj;
                }
                
                return response()->json($array,200);
            }else{
                return response()->json('import gagal, silahkan cek file anda',422);
            }


        }

    }

    public function create(Request $request)
    {
        $po_buyer       = $request->po_buyer;
        $item_code      = $request->item_code;
        $user           = auth::user();
        
        if(auth::user()->nik == '170500108' || auth::user()->nik == '170500005' || auth::user()->nik == '160309938' || auth::user()->nik == '170700223' || auth::user()->nik == '160209757' || auth::user()->nik == '160309916' || $user->hasRole(['admin-ict-acc','supervisor'])){
            $data = MaterialRequirement::whereIn('item_code',function($query) use ($user){
                $query->select('item_code')
                ->from('material_excludes')
                ->where(function($query) use ($user){
                    $query->Where('is_paxar',true);
                    
                    if(auth::user()->nik == '160209757' || auth::user()->nik == '170700223'|| auth::user()->nik == '170500005' || auth::user()->nik == '160309938' || auth::user()->nik == '160309916' || $user->hasRole(['admin-ict-acc','supervisor'])) $query->orwhere('is_ila',true)->orWhere('is_from_buyer',true);
                })
                ->groupby('item_code');
            });
        }else{
            $data = MaterialRequirement::whereIn('item_code',function($query){
                $query->select('item_code')
                ->from('material_excludes')
                ->where(function($query){
                    $query->where('is_ila',true)
                    ->orWhere('is_from_buyer',true);
                })
                ->groupby('item_code');
            });
        }

       

        if($po_buyer)  $data = $data->where('po_buyer','like',"%$po_buyer%");
        if($item_code) $data = $data->where('item_code','like',"%$item_code%");
        $data = $data->paginate(10);

        if($request->additional==true) return view('accessories_barcode_bill_of_material._po_buyer_additional_list',compact('data'));
        return view('accessories_barcode_bill_of_material._po_buyer_list',compact('data'));
    }

    public function store(Request $request)
    {
        $movement_date      = carbon::now()->toDateTimeString();
        $_warehouse_id      = $request->warehouse;
        $insert_movements   = array();
        $concatenate        = '';
        $return_print       = array();

        if($request->has('is_additional') && $request->is_additional==true)
        {
          $is_additional    = true;
          $item_code        = trim(strtoupper($request->additional_item_code));
          $item_desc        = trim(strtoupper($request->additional_item_desc));
          $po_buyer         = trim(strtoupper($request->additional_po_buyer));
          $article          = $request->additional_article;
          $job_order        = $request->additional_job_order;
          $category         = $request->additional_category;
          $style            = $request->additional_style;
          $uom              = $request->additional_uom;
          $qty_need         = $request->additional_qty_required;
        }else{
          $item_code        = trim(strtoupper($request->__item_code));
          $item_desc        = trim(strtoupper($request->__item_desc));
          $po_buyer         = trim(strtoupper($request->__po_buyer));
          $article          = $request->___article;
          $job_order        = $request->___job_order;
          $category         = $request->__category;
          $style            = $request->__style;
          $uom              = $request->__uom;
          $qty_need         = $request->__qty_required;
          $is_additional    = false;
        }

        $bom_location   = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','BOM');
        })
        ->first();

        $material_exlcude   = MaterialExclude::where('item_code',$item_code)->first();
        $_document_no       = 'FREE STOCK';
        $_supplier_name     = 'FREE STOCK';
        $item               = Item::where('item_code',$item_code)->first();

        $so_id              = PoBuyer::where('po_buyer', $po_buyer)->first();


        $conversion = UomConversion::where([
            ['item_code',$item->item_code],
            ['category',$item->category],
            ['uom_to',$uom]
        ])
        ->first();

        if($conversion)
        {
            $dividerate     = $conversion->dividerate;
            $multiplyrate   = $conversion->multiplyrate;
        }else
        {
            $dividerate     = 1;
            $multiplyrate   = 1;
        }

        $qty_conversion = $qty_need * $dividerate;

        if($is_additional == false)
        {
            $material_preparation = MaterialPreparation::where('item_code',$item_code)
            ->where([
                ['po_buyer',$po_buyer],
                ['article_no',$article],
                ['style',$style],
                ['is_from_barcode_bom',true],
            ])
            ->first();
        }else
        {
            $material_preparation = false;
        }
       
        try 
        {
            DB::beginTransaction();

            if(!$material_preparation)
            {
                /*$is_backlog = MaterialBacklog::where([
                    ['po_buyer',$po_buyer],
                    ['item_code',$item_code],
                ])
                ->exists();*/
                
                $_style = explode('::',$job_order)[0];
            
                $material_preparation = MaterialPreparation::FirstOrCreate([
                    'barcode'                                       => 'BELUM DI PRINT',
                    'referral_code'                                 => '-1',
                    'sequence'                                      => '-1',
                    'item_id'                                       => $item->item_id,
                    'c_order_id'                                    => 'FREE STOCK',
                    'c_bpartner_id'                                 => 'FREE STOCK',
                    'supplier_name'                                 => $_supplier_name,
                    'document_no'                                   => $_document_no,
                    'po_buyer'                                      => $po_buyer,
                    'is_moq'                                        => false,
                    'is_additional'                                 => $is_additional,
                    'uom_conversion'                                => $uom,
                    'qty_conversion'                                => $qty_need,
                    'qty_reconversion'                              => $qty_need * $multiplyrate,
                    'job_order'                                     => $job_order,
                    'style'                                         => $style,
                    '_style'                                        => $_style,
                    'article_no'                                    => $article,
                    'warehouse'                                     => $_warehouse_id,
                    'item_code'                                     => $item_code,
                    'item_desc'                                     => $item_desc,
                    'category'                                      => $category,
                    'total_carton'                                  => 1,
                    'type_po'                                       => 2,
                    'is_from_barcode_bom'                           => true,
                    'is_backlog'                                    => false,
                    'user_id'                                       => auth::user()->id,
                    'last_status_movement'                          => 'print',
                    'last_locator_id'                               => $bom_location->id,
                    'first_print_date'                              => null,
                    'last_movement_date'                            => $movement_date,
                    'created_at'                                    => $movement_date,
                    'updated_at'                                    => $movement_date,
                    'last_user_movement_id'                         => auth::user()->id,
                    'so_id'                                         => $so_id->so_id
                ]);
                //close alokasi ila untuk keperluan dashboard
                $auto_allocation_ila = AutoAllocation::where('po_buyer', $po_buyer)
                                   ->where('item_code', $item_code)
                                   ->where('document_no', 'like', '%S-%')
                                   ->whereNull('deleted_at')
                                   ->first();

                if($auto_allocation_ila)
                {
                    $auto_allocation_ila->qty_in_house = $auto_allocation_ila->qty_allocation;
                    $auto_allocation_ila->save();

                     //alokasi yang ori ilangin 
                $auto_allocation_ori = AutoAllocation::where('po_buyer', $po_buyer)
                ->where('item_code', $item_code)
                ->where('document_no', 'not like', '%S-%')
                ->whereNull('deleted_at')
                ->first();
                
                if($auto_allocation_ori)
                {
                $auto_allocation_ori->deleted_at = $movement_date;
                $auto_allocation_ori->save();
                }
                }
               
                //insert detail
                DetailMaterialPreparation::firstOrCreate([
                    'material_preparation_id'   => $material_preparation->id,
                    'material_arrival_id'       => null,
                    'user_id'                   => auth::user()->id
                ]);

                $is_exists = MaterialMovement::where([
                    ['from_location',$bom_location->id],
                    ['to_destination',$bom_location->id],
                    ['from_locator_erp_id',$bom_location->area->erp_id],
                    ['to_locator_erp_id',$bom_location->area->erp_id],
                    ['po_buyer',$material_preparation->po_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list','-'],
                    ['no_invoice','-'],
                    ['status','print'],
                ]);
                
                if($is_exists)
                {
                    $material_movement = MaterialMovement::firstorcreate([
                        'from_location'         => $bom_location->id,
                        'to_destination'        => $bom_location->id,
                        'from_locator_erp_id'   => $bom_location->area->erp_id,
                        'to_locator_erp_id'     => $bom_location->area->erp_id,
                        'po_buyer'              => $material_preparation->po_buyer,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'status'                => 'print',
                        'no_packing_list'       => '-',
                        'no_invoice'            => '-',
                        'created_at'            => $material_preparation->created_at,
                        'updated_at'            => $material_preparation->updated_at,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_exists->updated_at = $material_preparation->updated_at;
                    $is_exists->save();

                    $material_movement_id = $is_exists->id;
                }

                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['material_preparation_id',$material_preparation->id],
                    ['item_id',$item->item_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['date_movement',$material_preparation->created_at],
                    ['item_id',$material_preparation->item_id],
                    ['qty_movement',$material_preparation->qty_conversion],
                    ['warehouse_id',$material_preparation->warehouse],
                ])
                ->exists();

                if(!$is_material_movement_line_exists)
                {
                    $material_movement_line = MaterialMovementLine::firstorcreate([
                        'material_movement_id'      => $material_movement_id,
                        'material_preparation_id'   => $material_preparation->id,
                        'item_id'                   => $material_preparation->item_id,
                        'item_code'                 => $material_preparation->item_code,
                        'type_po'                   => $material_preparation->type_po,
                        'c_order_id'                => $material_preparation->c_order_id,
                        'c_orderline_id'            => '-',
                        'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                        'supplier_name'             => $material_preparation->supplier_name,
                        'uom_movement'              => $material_preparation->uom_conversion,
                        'qty_movement'              => $material_preparation->qty_conversion,
                        'warehouse_id'              => $material_preparation->warehouse,
                        'document_no'               => $material_preparation->document_no,
                        'date_movement'             => $material_preparation->created_at,
                        'created_at'                => $material_preparation->created_at,
                        'updated_at'                => $material_preparation->created_at,
                        'nomor_roll'                => '-',
                        'is_active'                 => true,
                        'is_integrate'              => false,
                        'user_id'                   => $material_preparation->user_id
                    ]);
                }

                $material_preparation->last_material_movement_line_id = $material_movement_line->id;
                $material_preparation->save();

                $temporary = Temporary::Create([
                    'barcode'       => $po_buyer,
                    'status'        => 'mrp',
                    'user_id'       => Auth::user()->id,
                    'created_at'    => $movement_date,
                    'updated_at'    => $movement_date,
                ]);

                $return_print []    = $material_preparation->id;
                $concatenate        .= '\''.$material_preparation->id.'\',';
                $insert_movements [] = $material_preparation->id;

            }else
            {
                if($is_additional == false && $qty_conversion != $material_preparation->qty_conversion)
                {
                    $material_preparation->qty_reconversion = $qty_conversion;
                    $material_preparation->qty_conversion = $qty_conversion;
                }

                if($material_preparation->last_status_movement == 'print')
                {
                    $return_print [] = $material_preparation->id;
                }else
                {
                    if($material_movement_line)
                    {
                        return response()->json('item ini sudah ada di lokasi '.$material_preparation->locator_id,422);
                    }
                }
            }

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            }
            
            $this->insertToMovementInventory($insert_movements);
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        
        return response()->json($return_print,200);
    }

    public function printout(Request $request)
    {
       // $get_barcode = $this->randomCode();
        $list_barcodes       = json_decode($request->list_barcodes);
        $movement_date      = carbon::now();
       
        foreach ($list_barcodes as $key => $list_barcode) 
        {
            $material_preparation   = MaterialPreparation::find($list_barcode);
            if($material_preparation->first_print_date)
            {
                $reprint_counter                        = $material_preparation->reprint_counter;
                $material_preparation->reprint_user_id  = auth::user()->id;
                $material_preparation->reprint_date     = $movement_date;
                $material_preparation->reprint_counter  = $reprint_counter+1;
            }else
            {
                if($material_preparation->barcode == 'BELUM DI PRINT')
                {
                    $get_barcode                                = $this->randomCode();
                    $barcode                                    = $get_barcode->barcode;
                    $referral_code                              = $get_barcode->referral_code;
                    $sequence                                   = $get_barcode->sequence;
                    $material_preparation->barcode              = $barcode;
                    $material_preparation->referral_code        = $referral_code;
                    $material_preparation->sequence             = $sequence;
                    $material_preparation->first_print_date     = $movement_date;
                }
            }
            
            $material_preparation->save();

        }

        $items = MaterialPreparation::whereIn('material_preparations.id',$list_barcodes)->get();
        return view('accessories_barcode_bill_of_material.barcode', compact('items'));
    }

    public function printoutAll(Request $request)
    {
        $_warehouse_id      = $request->warehouse_id;
        $bom                = json_decode($request->bom);
        $insert_movements   = array();

        $receiving_location = Locator::with('area')
        ->whereHas('area',function ($query) use($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','RECEIVING');
        })
        ->first();

        $bom_location = Locator::with('area')
        ->whereHas('area',function ($query) use ($_warehouse_id)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$_warehouse_id);
            $query->where('name','BOM');
        })
        ->first();
        $print = array();
        
        try 
        {
            DB::beginTransaction();

            $concatenate    = '';
            $movement_date  = carbon::now()->toDateTimeString();
            
            foreach ($bom as $key => $value) 
            {
                if($value->material_preparation_id == -1)
                {

                    $material_exlcude   = MaterialExclude::where('item_code',$value->item_code)->first();
                    $item               = Item::where('item_code',$value->item_code)->first();
                    $_document_no       = 'FREE STOCK';
                    $_supplier_name     = 'FREE STOCK';

                    $is_exists = MaterialPreparation::where([
                        ['item_code',$value->item_code],
                        ['po_buyer',$value->po_buyer],
                        ['style',$value->style],
                        ['document_no',$_document_no],
                        ['warehouse',$_warehouse_id],
                        ['barcode', 'like', 'I%'],
                    ])
                    ->first();

                    if(!$is_exists)
                    {
                        $conversion = UomConversion::where([
                            ['item_code',$item->item_code],
                            ['category',$item->category],
                            ['uom_to',$value->uom]
                        ])
                        ->first();

                        if($conversion){
                            $dividerate = $conversion->dividerate;
                            $multiplyrate = $conversion->multiplyrate;
                        }else{
                            $dividerate = 1;
                            $multiplyrate = 1;
                        }

                        //close alokasi ila untuk keperluan dashboard
                        $auto_allocation_ila = AutoAllocation::where('po_buyer', $value->po_buyer)
                        ->where('item_code', $item->item_code)
                        ->where('document_no', 'like', '%S-%')
                        ->whereNull('deleted_at')
                        ->first();

                        if($auto_allocation_ila)
                        {
                            $auto_allocation_ila->qty_in_house = $auto_allocation_ila->qty_allocation;
                            $auto_allocation_ila->save();

                            //alokasi yang ori ilangin 
                            $auto_allocation_ori = AutoAllocation::where('po_buyer', $value->po_buyer)
                            ->where('item_code', $item->item_code)
                            ->where('document_no', 'not like', '%S-%')
                            ->whereNull('deleted_at')
                            ->first();
                            
                            if($auto_allocation_ori)
                            {
                                $auto_allocation_ori->deleted_at = $movement_date;
                                $auto_allocation_ori->save();
                            }
                        }

                        $so_id                = PoBuyer::select('so_id')->where('po_buyer', $value->po_buyer)->first();

                        $material_preparation = MaterialPreparation::FirstOrCreate([
                            'barcode'                                       => 'BELUM DI PRINT',
                            'referral_code'                                 => '-1',
                            'sequence'                                      => '-1',
                            'po_detail_id'                                  => 'FREE STOCK',
                            'item_id'                                       => $item->item_id,
                            'c_order_id'                                    => 'FREE STOCK',
                            'c_bpartner_id'                                 => 'FREE STOCK',
                            'supplier_name'                                 => $_supplier_name,
                            'document_no'                                   => $_document_no,
                            'po_buyer'                                      => $value->po_buyer,
                            'is_moq'                                        => false,
                            'uom_conversion'                                => $value->uom,
                            'qty_conversion'                                => $value->qty,
                            'qty_reconversion'                              =>  $value->qty * $multiplyrate,
                            'job_order'                                     => $value->job_order,
                            'style'                                         => $value->style,
                            'article_no'                                    => $value->article_no,
                            'warehouse'                                     => $_warehouse_id,
                            'item_code'                                     => $item->item_code,
                            'item_desc'                                     => $item->item_desc,
                            'category'                                      => $item->category,
                            'total_carton'                                  => 1,
                            'type_po'                                       => 2,
                            'user_id'                                       => auth::user()->id,
                            'last_status_movement'                          => 'receiving',
                            'is_from_barcode_bom'                           => true,
                            'is_need_inserted_to_locator_free_stock_erp'    => true,
                            'is_need_inserted_to_locator_inventory_erp'     => true,
                            'last_locator_id'                               => $bom_location->id,
                            'last_movement_date'                            => $value->print_date,
                            'created_at'                                    => $value->print_date,
                            'updated_at'                                    => $value->print_date,
                            'last_user_movement_id'                         => auth::user()->id,
                            'so_id'                                         => $so_id->so_id
                        ]);

                        DetailMaterialPreparation::FirstOrCreate([
                            'material_preparation_id'   => $material_preparation->id,
                            'material_arrival_id'       => null,
                            'user_id'                   => auth::user()->id
                        ]);

                        $is_exists = MaterialMovement::where([
                            ['from_location',$bom_location->id],
                            ['to_destination',$bom_location->id],
                            ['from_locator_erp_id',$bom_location->area->erp_id],
                            ['to_locator_erp_id',$bom_location->area->erp_id],
                            ['po_buyer',$material_preparation->po_buyer],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list','-'],
                            ['no_invoice','-'],
                            ['status','print'],
                        ]);
                        
                        if($is_exists)
                        {
                            $material_movement = MaterialMovement::firstorcreate([
                                'from_location'         => $bom_location->id,
                                'to_destination'        => $bom_location->id,
                                'from_locator_erp_id'   => $bom_location->area->erp_id,
                                'to_locator_erp_id'     => $bom_location->area->erp_id,
                                'po_buyer'              => $material_preparation->po_buyer,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'status'                => 'print',
                                'no_packing_list'       => '-',
                                'no_invoice'            => '-',
                                'created_at'            => $material_preparation->created_at,
                                'updated_at'            => $material_preparation->updated_at,
                            ]);
        
                            $material_movement_id = $material_movement->id;
                        }else
                        {
                            $is_exists->updated_at = $material_preparation->created_at;
                            $is_exists->save();
        
                            $material_movement_id = $is_exists->id;
                        }
        
                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['material_preparation_id',$material_preparation->id],
                            ['item_id',$material_preparation->item_id],
                            ['warehouse_id',$material_preparation->warehouse,],
                            ['date_movement',$material_preparation->created_at],
                            ['qty_movement',$material_preparation->qty_conversion],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_exists)
                        {
                            $material_movement_line = MaterialMovementLine::firstorcreate([
                                'material_movement_id'      => $material_movement_id,
                                'material_preparation_id'   => $material_preparation->id,
                                'item_id'                   => $material_preparation->item_id,
                                'item_code'                 => $material_preparation->item_code,
                                'type_po'                   => $material_preparation->type_po,
                                'c_order_id'                => $material_preparation->c_order_id,
                                'c_orderline_id'            => '-',
                                'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                'supplier_name'             => $material_preparation->supplier_name,
                                'uom_movement'              => $material_preparation->uom_conversion,
                                'qty_movement'              => $material_preparation->qty_conversion,
                                'warehouse_id'              => $material_preparation->warehouse,
                                'document_no'               => $material_preparation->document_no,
                                'nomor_roll'                => '-',
                                'date_movement'             => $material_preparation->created_at,
                                'created_at'                => $material_preparation->created_at,
                                'updated_at'                => $material_preparation->created_at,
                                'is_integrate'              => false,
                                'is_active'                 => true,
                                'user_id'                   => $material_preparation->user_id
                            ]);
    
                            $material_preparation->last_material_movement_line_id = $material_movement_line->id;
                            $material_preparation->save();
                        }
                       

                        $insert_movements []    = $material_preparation->id;
                        $print []               = $material_preparation->id;
                        $concatenate            .= '\''.$material_preparation->id.'\',';
                    }else
                    {
                        $print [] = $is_exists->id;
                    }


                }else
                {
                    if($value->material_preparation_id != '0'){
                        $material_preparation = MaterialPreparation::find($value->material_preparation_id);
                    
                        $material_preparation->qty_reconversion = $value->qty;
                        $material_preparation->qty_conversion = $value->qty;
                        $material_preparation->reprint_user_id = auth::user()->id;
                        $material_preparation->reprint_date = carbon::now();
                        $material_preparation->save();
                        $print [] = $material_preparation->id;
                    }
                }
            }

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !=''){
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            }
    
    
            $items = MaterialPreparation::whereIn('material_preparations.id',$print)->get();
          
            foreach ($items as $key => $item) 
            {
                if($item->first_print_date)
                {
                    $reprint_counter            = $item->reprint_counter;
                    $item->reprint_user_id      = auth::user()->id;
                    $item->reprint_date         = carbon::now();
                    $item->reprint_counter      = $reprint_counter+1;
                }else
                {
                    if($item->barcode == 'BELUM DI PRINT')
                    {
                        $get_barcode               = $this->randomCode();
                        $barcode                   = $get_barcode->barcode;
                        $referral_code             = $get_barcode->referral_code;
                        $sequence                  = $get_barcode->sequence;
    
                        $item->barcode              = $barcode;
                        $item->referral_code        = $referral_code;
                        $item->sequence             = $sequence;
                    }
    
                    $item->first_print_date = $movement_date;
                }
               
                $item->save();
            }
    
            $this->insertToMovementInventory($insert_movements);
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        //dd($print);
       
        return view('accessories_barcode_bill_of_material.barcode', compact('items'));
    }

    static function randomCode()
    {
        $referral_code = '2I'.Carbon::now()->format('u');
        $sequence = Barcode::where('referral_code',$referral_code)->max('sequence');

        if ($sequence == null) $sequence = 1;
        else $sequence += 1;
        
        Barcode::firstorCreate([
            'barcode'       => $referral_code.''.$sequence,
            'referral_code' => $referral_code,
            'sequence'      => $sequence,
        ]);

        $obj                = new stdClass();
        $obj->barcode       = $referral_code.''.$sequence;
        $obj->referral_code = $referral_code;
        $obj->sequence      = $sequence;
        return $obj;
        
    }

    static function insertToMovementInventory($insert_movements)
    {
        try
        {
            $data = MaterialPreparation::whereIn('id',$insert_movements)->get();
            
            foreach ($data as $key => $datum) 
            {
                $warehouse_id                   = $datum->warehouse;
                $material_preparation_id        = $datum->id;
                $created_at                     = $datum->created_at;
                $qty_conversion                 = sprintf('%0.8f',$datum->qty_conversion);
                $user_id                        = $datum->user_id;
                $po_buyer                       = $datum->po_buyer;
                $po_detail_id                   = $datum->po_detail_id;
                $item_code                      = $datum->item_code;
                $item_id                        = $datum->item_id;
                $c_bpartner_id                  = $datum->c_bpartner_id;
                $supplier_name                  = $datum->supplier_name;
                $document_no                    = $datum->document_no;
                $uom                            = $datum->uom_conversion;
                $c_order_id                     = $datum->c_order_id;
                $no_packing_list                = '-';
                $no_invoice                     = '-';
                $c_orderline_id                 = '-';
                $note                           = 'PROSES INTEGRASI UNTUK MATERIAL YG BARCODENYA DI CETAK MELALUI BARCODE BOM';
              
                $inventory_erp = Locator::with('area')
                ->where([
                    ['is_active',true],
                    ['rack','ERP INVENTORY'],
                ])
                ->whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $is_movement_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['po_buyer',$po_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();

                if(!$is_movement_exists)
                {
                    $material_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'po_buyer'              => $po_buyer,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $created_at,
                        'updated_at'            => $created_at,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_movement_exists->updated_at = $created_at;
                    $is_movement_exists->save();

                    $material_movement_id = $is_movement_exists->id;
                }

                $is_material_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['material_preparation_id',$material_preparation_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['item_id',$item_id],
                    ['warehouse_id',$warehouse_id],
                    ['date_movement',$created_at],
                    ['qty_movement',$qty_conversion],
                ])
                ->exists();

                if(!$is_material_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => 1,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => $qty_conversion,
                        'date_movement'                 => $created_at,
                        'created_at'                    => $created_at,
                        'updated_at'                    => $created_at,
                        'date_receive_on_destination'   => $created_at,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => $note,
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }
                
            }

        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }
}
