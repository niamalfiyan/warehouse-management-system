<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use Validator;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\Role;

class MasterDataRoleController extends Controller
{
    public function index(Request $request, Builder $htmlBuilder){
        if ($request->ajax()){ 
            $user_application = auth::user()->application;
            $roles = Role::where('application',$user_application)
            ->orderBy('created_at','asc');

            return DataTables::of($roles)
             ->addColumn('action', function($roles){ return view('_action', [
                'edit' => route('masterDataRole.edit', $roles->id),
                'delete' => route('masterDataRole.delete', $roles->id)]);})    
            ->make(true);
        }

         $html = $htmlBuilder
        ->addColumn(['data'=>'display_name', 'name'=>'display_name', 'title'=>'NAME'])
        ->addColumn(['data' => 'action','name' => 'action', 'title' => 'ACTION', 'orderable' => false, 'searchable' => false, 'class' => 'text-center', 'style'=>'width: 30px;']);

        return view('master_data_role.index')
                ->with('flag',Session::get('flag'))
                ->with(compact('html'));
    }

    public function create(){
        return view('master_data_role.create');
    }

    public function store(Request $request){
        if(Session::has('flag'))
            Session::forget('flag');

        $this->validate($request, [
            'name' => 'required|min:3'
        ]);
        
        if(Role::where('name',str_slug($request->name))->exists())
            return response()->json(['message' => 'Role Name already exists.'
                ], 422);
        
        $permissions =  json_decode($request->permissions);
        $permission_array = array();

        foreach ($permissions as $key => $value) {
            $permission_array [] = [
                'id' => $value->id
            ];
        }

        $role = new Role();
        $role->name = str_slug($request->name);
        $role->display_name = $request->name;
        $role->description  = $request->description;
        $role->application  = auth::user()->application;

        
        if($role->save()){
            $role->attachPermissions($permission_array);
        }

        Session::flash('flag', 'success');
        return response()->json(200);
    }

    public function edit($id){
        $role = Role::find($id);
        return view('master_data_role.edit')
            ->with('role',$role);
    }

    public function update(Request $request, $id){
        if(Session::has('flag'))
            Session::forget('flag');

        $this->validate($request, [
            'name' => 'required|min:3'
        ]);
        
        if(Role::where('name',str_slug($request->name))->where('id','!=',$id)->exists())
            return response()->json(['message' => 'Role Name already exists.'
                ], 422);
        
        $permissions =  json_decode($request->permissions);
        $permission_array = array();
        
        foreach ($permissions as $key => $value) {
            $permission_array [] = [
                'id' => $value->id
            ];
        }
      
        $role = Role::find($id);
        $role->name = str_slug($request->name);
        $role->display_name = $request->name;
        $role->description  =  $request->description;
        $role->application  =  auth::user()->application;

        if($role->update()){
            $role->perms()->sync([]);
            $role->attachPermissions($permission_array);
        }

        Session::flash('flag', 'success_2');
        return response()->json(200);
    }

    public function destroy($id){
        $role = Role::findorFail($id)
                ->delete();
        return response()->json(200);
    }

    public function picklist(Request $request){
		$query = $request->input('q');
		$state = $request->state;
		$items = Role::orderBy('name')
        ->Where('name','like',"%$query%");
           
		
		return view('modal_lov._role_list')
			->with('items', $items->paginate(5));
	}
}
