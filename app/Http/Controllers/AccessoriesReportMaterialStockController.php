<?php namespace App\Http\Controllers;

use DB;
use Auth;
use View;
use Excel;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;

use App\Models\Temporary;
use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Locator;
use App\Models\Supplier;
use App\Models\DetailMaterialRequirement;
use App\Models\MaterialStock;
use App\Models\MappingStocks;
use App\Models\AllocationItem;
use App\Models\UomConversion;
use App\Models\AutoAllocation;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\DetailMaterialStock;
use App\Models\MaterialRequirement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialPreparation;

use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\ErpMaterialRequirementController as EMRC;


class AccessoriesReportMaterialStockController extends Controller
{
    public function index(Request $request)
    {
        $warehouse_id                   = ($request->has('warehouse_id'))?$request->warehouse_id:auth::user()->warehouse;
        $_mapping_stock                 = ($request->has('mapping_stock_id'))?$request->mapping_stock_id:'ALL';
        $active_tab                     = ($request->has('active_tab'))?'inactive':'active';
        
        return view('accessories_report_material_stock.index',compact('warehouse_id','array_mapping_stocks','_mapping_stock','active_tab'));
    }

    public function dataActive(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $type_stock_erp_code    = $request->type_stock_erp_code;
            $material_stocks        = db::table('report_material_stocks_acc_v')->where('warehouse_id','LIKE',"%$warehouse_id%");
            if($type_stock_erp_code) $material_stocks = $material_stocks->where('type_stock_erp_code',$type_stock_erp_code);
            
            $material_stocks  = $material_stocks->orderby('available_qty','desc')->take(50);
            
            return DataTables::of($material_stocks)
            ->addColumn('source',function ($material_stocks)
            {
                return '<b>Po Buyer Origin</b><br/> '.$material_stocks->po_buyer.
                '<br><b>Origin Stock</b><br/> '.$material_stocks->source;

            })
            ->addColumn('supplier',function ($material_stocks)
            {
                return'<br><b>Supplier Code</b><br/> '.$material_stocks->supplier_code.
                '<br><b>Supplier Name</b><br/> '.$material_stocks->supplier_name;
            })
            ->addColumn('item',function ($material_stocks){
                return '<b>Item Code</b><br/> '.$material_stocks->item_code.
                '<br><b>Item Desc</b><br/> '.$material_stocks->item_desc.
                '<br><b>Category</b><br/> '.$material_stocks->category;
            })
            ->editColumn('stock',function ($material_stocks){
                return number_format($material_stocks->stock, 4, '.', ',');
            })
            ->editColumn('reserved_qty',function ($material_stocks){
                return number_format($material_stocks->reserved_qty, 4, '.', ',');
            })
            ->addColumn('_available_qty',function ($material_stocks){
                return number_format($material_stocks->available_qty, 4, '.', ',');
            })
            ->addColumn('action', function($material_stocks) {
                if(auth::user()->hasRole('admin-ict-acc'))
                {
                    return view('accessories_report_material_stock._action',[
                        'model'                 => $material_stocks,
                        'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                        'edit_modal'            => route('accessoriesReportMaterialStock.editStock',$material_stocks->material_stock_id),
                        'delete'                => route('accessoriesReportMaterialStock.deleteStock',$material_stocks->material_stock_id),
                        'recalculate'           => route('accessoriesReportMaterialStock.recalculate',$material_stocks->material_stock_id),
                        'unlock'                => route('accessoriesReportMaterialStock.unlock',$material_stocks->material_stock_id),
                        'allocatingStock'       => route('accessoriesReportMaterialStock.allocatingStock',$material_stocks->material_stock_id),
                        'cancelAutoAllocation'  => route('accessoriesReportMaterialStock.cancelAllocation',$material_stocks->material_stock_id),
                        'move'                  => route('accessoriesReportMaterialStock.moveLocator',$material_stocks->material_stock_id),
                        'transfer'              => route('accessoriesReportMaterialStock.transferStock',$material_stocks->material_stock_id),
                        'change_type'           => route('accessoriesReportMaterialStock.changeTypeStock',$material_stocks->material_stock_id),
                        'change_source'         => route('accessoriesReportMaterialStock.changeSourceStock',$material_stocks->material_stock_id),
                    ]);
                }else{
                    if(auth::user()->hasRole(['mm-staff-acc']))
                    {
                        return view('accessories_report_material_stock._action',[
                            'model'                 => $material_stocks,
                            'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                            'edit_modal'            => route('accessoriesReportMaterialStock.editStock',$material_stocks->material_stock_id),
                            'delete'                => route('accessoriesReportMaterialStock.deleteStock',$material_stocks->material_stock_id),
                            'recalculate'           => route('accessoriesReportMaterialStock.recalculate',$material_stocks->material_stock_id),
                            'unlock'                => route('accessoriesReportMaterialStock.unlock',$material_stocks->material_stock_id),
                            'allocatingStock'       => route('accessoriesReportMaterialStock.allocatingStock',$material_stocks->material_stock_id),
                            'move'                  => route('accessoriesReportMaterialStock.moveLocator',$material_stocks->material_stock_id),
                            'transfer'              => route('accessoriesReportMaterialStock.transferStock',$material_stocks->material_stock_id),
                            'change_type'           => route('accessoriesReportMaterialStock.changeTypeStock',$material_stocks->material_stock_id),
                        ]);
                    }
                    elseif(auth::user()->hasRole(['free-stock']))
                    {
                        return view('accessories_report_material_stock._action',[
                            'model'                 => $material_stocks,
                            'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                            'edit_modal'            => route('accessoriesReportMaterialStock.editStock',$material_stocks->material_stock_id),
                            'delete'                => route('accessoriesReportMaterialStock.deleteStock',$material_stocks->material_stock_id),
                            'recalculate'           => route('accessoriesReportMaterialStock.recalculate',$material_stocks->material_stock_id),
                            'unlock'                => route('accessoriesReportMaterialStock.unlock',$material_stocks->material_stock_id),
                            'allocatingStock'       => route('accessoriesReportMaterialStock.allocatingStock',$material_stocks->material_stock_id),
                            'move'                  => route('accessoriesReportMaterialStock.moveLocator',$material_stocks->material_stock_id),
                            'transfer'              => route('accessoriesReportMaterialStock.transferStock',$material_stocks->material_stock_id),
                            'change_type'           => route('accessoriesReportMaterialStock.changeTypeStock',$material_stocks->material_stock_id),
                            'change_source'         => route('accessoriesReportMaterialStock.changeSourceStock',$material_stocks->material_stock_id),
                        ]);
                    }
                    elseif(auth::user()->hasRole(['finance-account-acc']))
                    {
                        return view('accessories_report_material_stock._action',[
                            'model'                 => $material_stocks,
                            'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                        ]);
                    }
                    else{
                        return view('accessories_report_material_stock._action',[
                            'model'                 => $material_stocks,
                            'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                            'recalculate'           => route('accessoriesReportMaterialStock.recalculate',$material_stocks->material_stock_id),
                            'allocatingStock'       => route('accessoriesReportMaterialStock.allocatingStock',$material_stocks->material_stock_id),
                        ]);
                    }
                    
                }
            })
            ->setRowAttr([
                'style' => function($material_stocks) 
                {
                    if($material_stocks->is_running_stock && $material_stocks->available_qty > 0) return  'background-color: #ffffa3';
                    else if(round($material_stocks->available_qty, 4) > 0) return  'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['source','quantity','item','action','style','supplier']) 
            ->make(true);
        }
    }

    public function dataInactive(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id           = $request->warehouse;
            $type_stock_erp_code    = $request->type_stock_erp_code;
            $material_stocks = db::table('report_material_stocks_inactive_acc_v')->where('warehouse_id','LIKE',"%$warehouse_id%");
            if($type_stock_erp_code) $material_stocks = $material_stocks->where('type_stock_erp_code',$type_stock_erp_code);
            
            return DataTables::of($material_stocks)
            ->addColumn('source',function ($material_stocks){
                return '<b>Po Buyer Origin</b><br/> '.$material_stocks->po_buyer.
                '<br><b>Origin Stock</b><br/> '.$material_stocks->source;

            })
            ->addColumn('supplier',function ($material_stocks){
                return'<br><b>Supplier Code</b><br/> '.$material_stocks->supplier_code.
                '<br><b>Supplier Name</b><br/> '.$material_stocks->supplier_name;
            })
            ->addColumn('item',function ($material_stocks){
                return '<b>Item Code</b><br/> '.$material_stocks->item_code.
                '<br><b>Item Desc</b><br/> '.$material_stocks->item_desc.
                '<br><b>Category</b><br/> '.$material_stocks->category;
            })
            ->editColumn('stock',function ($material_stocks){
                return number_format($material_stocks->stock, 4, '.', ',');
            })
            ->editColumn('reserved_qty',function ($material_stocks){
                return number_format($material_stocks->reserved_qty, 4, '.', ',');
            })
            ->addColumn('_available_qty',function ($material_stocks){
                return number_format($material_stocks->available_qty, 4, '.', ',');
            })
            ->addColumn('action', function($material_stocks) 
            {
                if(auth::user()->hasRole('admin-ict-acc'))
                {
                    return view('accessories_report_material_stock._action',[
                        'model'                 => $material_stocks,
                        'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                        'restore'               => route('accessoriesReportMaterialStock.restoreStock',$material_stocks->material_stock_id),
                    ]);
                }else
                {
                    return view('accessories_report_material_stock._action',[
                        'model'                 => $material_stocks,
                        'stockMutation'         => route('accessoriesReportMaterialStock.stockMutation',$material_stocks->material_stock_id),
                    ]);
                }
            })
            ->setRowAttr([
                'style' => function($material_stocks) 
                {
                    if($material_stocks->available_qty > 0) return 'background-color: #d9ffde';
                },
            ])
            ->rawColumns(['source','quantity','item','action','style','supplier']) 
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        if(auth::user()->hasRole(['admin-ict-acc','mm-staff-acc']))
        {
            $filename = 'report_stock_acc.csv';
        }else
        {
            $warehouse_id = ($request->warehouse_id ? $request->warehouse_id : auth::user()->warehouse);
            if($warehouse_id == '1000013') $filename = 'report_stock_acc_aoi2.csv';
            else if($warehouse_id == '1000002') $filename = 'report_stock_acc_aoi1.csv';
        }
        
        $file = Config::get('storage.report') . '/' . $filename;

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;

    }

    public function exportAllocation(Request $request)
    {
        $warehouse_id = ($request->warehouse_id ? $request->warehouse_id : auth::user()->warehouse);
        // if(auth::user()->hasRole(['admin-ict-acc','mm-staff-acc']))
        // {
        //     $filename1 = 'report_allocation_aoi1';
        //     $filename2 = 'report_allocation_aoi2';   

        //     $file1 = Config::get('storage.report') . '/' . e($filename1).'.csv';
        //     $file2 = Config::get('storage.report') . '/' . e($filename2).'.csv';

        //     if(!file_exists($file1)) return 'file yang anda cari tidak ditemukan';
        //     if(!file_exists($file2)) return 'file yang anda cari tidak ditemukan';

        //     $resp1 = response()->download($file1);
        //     $resp1->headers->set('Pragma', 'no-cache');
        //     $resp1->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        //     $resp1->headers->set('X-Content-Type-Options', 'nosniff');

        //     $resp2 = response()->download($file2);
        //     $resp2->headers->set('Pragma', 'no-cache');
        //     $resp2->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        //     $resp2->headers->set('X-Content-Type-Options', 'nosniff');
        //     return array($resp2, $resp1);
        // }
        // else
        // {
            if($warehouse_id == '1000013') $filename = 'report_allocation_aoi2';
            else if($warehouse_id == '1000002') $filename = 'report_allocation_aoi1';
            
            $file = Config::get('storage.report') . '/' . e($filename).'.csv';

            if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';
            
            $resp = response()->download($file);
            $resp->headers->set('Pragma', 'no-cache');
            $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
            $resp->headers->set('X-Content-Type-Options', 'nosniff');
            return $resp;
        // }
    }

    public function stockMutation(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data  = DB::select(db::raw("SELECT * FROM get_history_stock('".$id."')")); 

            return DataTables::of($data)
            ->addColumn('qty_db',function ($data){
                if($data->status =='DB') return number_format($data->qty, 4, '.', ',');
                else return null;
            })
            ->addColumn('qty_cr',function ($data){
                if($data->status =='CR') return number_format($data->qty, 4, '.', ',');
                else return null;
            })
            ->editColumn('created_at',function ($data){
                return ( $data->created_at ? Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/m/Y H:i:s') : null );
            })
            ->make(true);
        }
    }

    /*public function dataAllocationBuyer(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data  = MaterialPreparation::where([
                ['material_stock_id',$id],
                ['last_status_movement','!=','reroute'],
            ])
            
            ->orderby('created_at','asc');
            return DataTables::of($data)
            ->editColumn('qty_conversion',function ($data)
            {
                return number_format($data->qty_conversion, 4, '.', ',');
            })
            ->editColumn('created_at',function ($data){
                return ( $data->created_at ? $data->created_at->format('d/M/Y H:i:s') : null );
            })
            ->editColumn('last_movement_date',function ($data)
            {
                return ( $data->last_movement_date ? $data->last_movement_date->format('d/M/Y H:i:s') : null );
            })
            ->editColumn('user_id',function ($data){
                return ( $data->user ? $data->user->name : null );
            })
            ->editColumn('last_user_movement_id',function ($data){
                return ( $data->last_user_movement_id ? $data->lastUserMovement->name : null );
            })
            ->editColumn('last_locator_id',function ($data){
                return ( $data->last_locator_id ? $data->lastLocator->code : null );
            })
            ->make(true);
        }
    }

    public function dataAllocationNonBuyer(Request $request,$id)
    {
        if(request()->ajax()) 
        {
            $data  =  $allocation_non_per_pobuyer = AllocationItem::where([
                ['material_stock_id',$id]
            ])
            ->where(function($query){
                $query->where('confirm_by_warehouse','approved')
                ->orWhereNull('confirm_by_warehouse');
            })
            ->orderBy('created_at','asc');

            return DataTables::of($data)
            ->editColumn('qty_booking',function ($data){
                return number_format($data->qty_booking, 4, '.', ',');
            })
            ->editColumn('created_at',function ($data){
                return ( $data->created_at ? $data->created_at->format('d/M/Y H:i:s') : null );
            })
            ->editColumn('user_id',function ($data){
                return ( $data->user ? $data->user->name : null );
            })
            ->make(true);
        }
    }*/

    public function detailAllocation(Request $request,$id)
    {
        //return view('errors.503');
        $po_buyer           = $request->q;
        $material_stock     = MaterialStock::find($id);
        $obj                = new stdClass();
        $obj->supplier_name = $material_stock->supplier_name;
        $obj->document_no   = $material_stock->document_no;
        $obj->item_code     = $material_stock->item_code;
        $obj->po_buyer      = ($material_stock->po_buyer) ? $material_stock->po_buyer : '';
        $obj->locator       = ($material_stock->locator_id) ? $material_stock->locator->code : '';

        $detail_stock = DetailMaterialStock::where('material_stock_id',$id)->orderBy('created_at','desc')->get();

        $allocation_per_pobuyer = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_from_allocation_buyer',true],
            ['is_not_allocation',false],
            ['po_buyer','LIKE',"%$po_buyer%"]
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $allocation_non_per_pobuyer = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_from_allocation_buyer',false],
            ['is_not_allocation',false]
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $non_allocation = AllocationItem::where([
            ['material_stock_id',$id],
            ['is_not_allocation',true],
        ])
        ->whereNull('deleted_at')
        ->where(function($query){
            $query->where('confirm_by_warehouse','approved')
            ->orWhereNull('confirm_by_warehouse');
        })
        ->orderBy('created_at','desc')
        ->get();

        $data = [
            'view_detail_stock'                 => View::make('accessories_report_material_stock._detail_material_stock',compact('detail_stock'))->render(),
            'view_allocation_per_pobuyer'       => View::make('accessories_report_material_stock._booking_material_stock',compact('allocation_per_pobuyer'))->render(),
            'view_allocation_non_per_pobuyer'   => View::make('accessories_report_material_stock._booking_non_buyer_material_stock',compact('allocation_non_per_pobuyer'))->render(),
            'view_non_allocation'               => View::make('accessories_report_material_stock._booking_non_allocation',compact('non_allocation'))->render(),
            'material_stock'                    => $obj
        ];

        return $data;
    }

    public function recalculate($id)
    {
        $material_stock         = MaterialStock::find($id);
        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        $this->doRecalculate($id);

        return response()->json('success',200);
    }

    public function unlock($id)
    {
        MaterialStock::where('id',$id)
        ->update([
            'last_status'       => Null,
            'ip_address'        => Null,
            'last_date_used'    => Null,
            'last_user_used_id' => Null
        ]);
        return response()->json('success',200);
    }

    public function changeTypeStock($id)
    {
        $material_stock             = MaterialStock::find($id);
        $obj                        = new stdClass();
        $obj->id                    = $id;
        $obj->supplier_name         = $material_stock->supplier_name;
        $obj->document_no           = $material_stock->document_no;
        $obj->item_code             = $material_stock->item_code;
        $obj->po_buyer              = $material_stock->po_buyer;
        $obj->available_qty         = $material_stock->available_qty;
        $obj->type_stock_erp_code   = $material_stock->type_stock_erp_code;
        $obj->type_stock            = $material_stock->type_stock;
        $obj->uom                   = $material_stock->uom;
        $obj->locator_name          = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        
        if(!auth::user()->hasRole(['admin-ict-acc','mm-staff-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }
        
        return response()->json($obj,200);
    }

    public function storeChangeTypeStock(Request $request)
    {
        $id                         = $request->id;
        $move_type                  = $request->change_stock_type_option;
        $new_type_stock             = $request->mapping_type_stock_option;
        $qty_change                 = $request->qty_change_stock;

        $material_stock             = MaterialStock::find($id);
        $old_type_stock             = $material_stock->type_stock;
        $old_type_stock_erp_code    = $material_stock->type_stock_erp_code;
        $locator_id                 = $material_stock->locator_id;
        $upload_date                = Carbon::now()->toDateTimeString();
        // dd($new_type_stock);
        $system = User::where([
            ['name','system'],
            ['warehouse',$material_stock->warehouse_id]
        ])
        ->first();

        try 
        {
            DB::beginTransaction();
            
            if($old_type_stock != $new_type_stock)
            {
                $old_available          = sprintf('%0.8f',$material_stock->available_qty);
                $curr_available_qty     = sprintf('%0.8f',$material_stock->available_qty);
                $cur_reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);

                if ($qty_change/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                else $_supplied = $qty_change;

                $mapping_stock              = MappingStocks::where('type_stock',$new_type_stock)->first();
                $new_type_stock_erp_code    = ($mapping_stock)? $mapping_stock->type_stock_erp_code : null;

                if($_supplied > 0 && $curr_available_qty > 0)
                {
                    $date_now = carbon::now();
                    //insert new allocataion
                    AllocationItem::FirstOrCreate([
                        'material_stock_id'         => $material_stock->id,
                        'c_order_id'                => $material_stock->c_order_id,
                        'document_no'               => $material_stock->document_no,
                        'c_bpartner_id'             => $material_stock->c_bpartner_id,
                        'supplier_code'             => $material_stock->supplier_code,
                        'supplier_name'             => $material_stock->supplier_name,
                        'item_id_source'            => $material_stock->item_id_source,
                        'item_id_book'              => $material_stock->item_id,
                        'item_code'                 => $material_stock->item_code,
                        'item_code_source'          => $material_stock->item_code,
                        'item_desc'                 => $material_stock->item_desc,
                        'category'                  => $material_stock->category,
                        'uom'                       => $material_stock->uom,
                        'type_stock_material'       => $material_stock->type_stock,
                        'warehouse'                 => $material_stock->warehouse_id,
                        'qty_booking'               => sprintf('%0.8f',$_supplied),
                        'is_not_allocation'         => true,
                        'is_from_allocation_buyer'  => false,
                        'user_id'                   => auth::user()->id,
                        'confirm_user_id'           => auth::user()->id,
                        'confirm_by_warehouse'      => 'approved',
                        'confirm_date'              => carbon::now(),
                        'created_at'                => $date_now,
                        'remark'                    => 'DIPINDAHKAN KE TIPE STOCK '.$new_type_stock,
                        'mm_approval_date'          => carbon::now(),
                        'accounting_approval_date'  => carbon::now(),
                        'accounting_user_id'        => $system->id,
                        'mm_user_id'                => $system->id,
                    ]);

                
                    //insert new stock
                    $is_exists = MaterialStock::whereNull('deleted_at')
                    ->where([
                        [db::raw("upper(document_no)"),$material_stock->document_no],
                        [db::raw('upper(item_code)'),strtoupper($material_stock->item_code)],
                        ['type_po', 2],
                        ['uom',$material_stock->uom],
                        ['locator_id',$locator_id],
                        ['warehouse_id',$material_stock->warehouse_id],
                        ['is_material_others',$material_stock->is_material_others],
                        ['is_closing_balance',false],
                        ['is_running_stock',false],
                        ['type_stock',$new_type_stock],
                    ])
                    ->whereNotNull('approval_date');

                    if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                    else $is_exists = $is_exists->whereNull('po_detail_id');

                    if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                    else $is_exists = $is_exists->whereNull('c_bpartner_id');

                    if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                    else $is_exists = $is_exists->whereNull('c_order_id');

                    if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                    else $is_exists = $is_exists->whereNull('po_buyer');

                    if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                    else $is_exists = $is_exists->whereNull('source');

                    $is_exists = $is_exists->first();
                    
                    if(!$is_exists)
                    {
                        $new_material_stock = MaterialStock::FirstOrCreate([
                            'uom_source'            => $material_stock->uom_source,
                            'po_detail_id'          => $material_stock->po_detail_id,
                            'document_no'           => $material_stock->document_no,
                            'supplier_code'         => $material_stock->supplier_code,
                            'supplier_name'         => $material_stock->supplier_name,
                            'c_bpartner_id'         => $material_stock->c_bpartner_id,
                            'c_order_id'            => $material_stock->c_order_id,
                            'locator_id'            => $locator_id,
                            'item_id'               => $material_stock->item_id,
                            'item_code'             => $material_stock->item_code,
                            'item_desc'             => $material_stock->item_desc,
                            'category'              => $material_stock->category,
                            'type_po'               => $material_stock->type_po,
                            'warehouse_id'          => $material_stock->warehouse_id,
                            'uom'                   => $material_stock->uom,
                            'po_buyer'              => $material_stock->po_buyer,
                            'qty_carton'            => $material_stock->qty_carton,
                            'stock'                 => sprintf('%0.8f',$_supplied),
                            'reserved_qty'          => 0,
                            'available_qty'         => sprintf('%0.8f',$_supplied),
                            'is_active'             =>  $material_stock->is_active,
                            'is_material_others'    => $material_stock->is_material_others,
                            'upc_item'              => $material_stock->upc_item,
                            'source'                => $material_stock->source,
                            'type_stock_erp_code'   => $new_type_stock_erp_code,
                            'type_stock'            => $new_type_stock,
                            'source'                => $material_stock->source,
                            'user_id'               => $material_stock->user_id,
                            'created_at'            => $upload_date,
                            'updated_at'            => $upload_date,
                        ]);

                        $new_material_stock_id      = $new_material_stock->id;
                        $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                        $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                        $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                       
                        HistoryStock::approved($new_material_stock_id
                        ,$_supplied
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,$remark_mutasi
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$new_type_stock_erp_code
                        ,$new_type_stock
                        ,false
                        ,true);
                        
                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'         => $new_material_stock->id,
                            'remark'                    => $remark_mutasi,
                            'uom'                       => $new_material_stock->uom,
                            'qty'                       => $_supplied,
                            'locator_id'                => $material_stock->locator_id,
                            'user_id'                   => Auth::user()->id,
                            'created_at'                => $upload_date,
                            'updated_at'                => $upload_date,
                            'approve_date_mm'           => $upload_date,
                            'approve_date_accounting'   => $upload_date,
                            'mm_user_id'                => $system->id,
                            'accounting_user_id'        => $system->id,
                        ]);

                    }else
                    {
                        $new_material_stock_id      = $is_exists->id;
                        $new_stock                  = sprintf('%0.8f',$is_exists->stock);
                        $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                        $new_reserved_qty           = sprintf('%0.8f',$is_exists->reserved_qty);
                        $_new_stock                 = sprintf('%0.8f',$new_stock + $_supplied);
                        $new_availabilty_qty        = sprintf('%0.8f',$_new_stock - $new_reserved_qty);

                        $is_exists->stock           = sprintf('%0.8f',$_new_stock);
                        $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                        
                    
                        if($new_availabilty_qty > 0)
                        {
                            $is_exists->is_allocated    = false;
                            $is_exists->is_active       = true;
                        }

                        $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                        $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                        $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                        
                        HistoryStock::approved($new_material_stock_id
                        ,$new_availabilty_qty
                        ,$old_available_qty_curr
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,$remark_mutasi
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$new_type_stock_erp_code
                        ,$new_type_stock
                        ,false
                        ,true);
                        
                        $is_exists->save();

                        DetailMaterialStock::FirstOrCreate([
                            'material_stock_id'         => $is_exists->id,
                            'remark'                    => $remark_mutasi,
                            'uom'                       => $is_exists->uom,
                            'qty'                       => $_supplied,
                            'locator_id'                => $is_exists->locator_id,
                            'user_id'                   => Auth::user()->id,
                            'created_at'                => $upload_date,
                            'updated_at'                => $upload_date,
                            'approve_date_mm'           => $upload_date,
                            'approve_date_accounting'   => $upload_date,
                            'mm_user_id'                => $system->id,
                            'accounting_user_id'        => $system->id,
                        ]);
                    }

                    $curr_available_qty -= $_supplied;
                    
                    $new = $old_available - $_supplied;
                    if($new <= 0) $new = '0';
                    else $new = $new;

                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                    $remark_mutasi              = 'DI PINDAHKAN KE TIPE '. $_new_type_stock;
                    
                    HistoryStock::approved($id
                    ,$new
                    ,$old_available
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,$remark_mutasi
                    ,auth::user()->name
                    ,auth::user()->id
                    ,$old_type_stock_erp_code
                    ,$old_type_stock
                    ,false
                    ,true);
                    
                    $material_stock->reserved_qty   = sprintf('%0.8f',$cur_reserved_qty + $_supplied);
                    $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                    if($curr_available_qty <= 0)
                    {
                        $material_stock->is_allocated   = true;
                        $material_stock->is_active      = false;
                    }

                    $material_stock->save();
                    
                }

                $locator = Locator::find($locator_id);
                if($locator)
                {
                    $counter_locator = MaterialStock::where([
                        ['locator_id',$locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();

                    $locator->counter_in = $counter_locator ;
                    $locator->save();
                }
                

            }

            DB::commit();
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function downloadChangeTypeStock()
    {
        return Excel::create('upload_change_type',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','material_stock_id');
                $sheet->setCellValue('B1','type_stock');
                $sheet->setCellValue('C1','uom');
                $sheet->setCellValue('D1','qty_change');

                $sheet->setWidth('A', 15);
                $sheet->setWidth('B', 15);
                $sheet->setWidth('C', 15);
                $sheet->setWidth('D', 15);

                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@'
                ));

               

            });

         
           
            $excel->setActiveSheetIndex(0);
          
            
            
        })->export('xlsx');
    }

    public function uploadChangeTypeStock(Request $request)
    {

        // dd($request->hasFile('upload_manual_file'));
        $array = array();
        if($request->hasFile('upload_manual_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_manual_file' => 'required|not_in:[]'
            ]);
          
            $path = $request->file('upload_manual_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

        //    dd($data);
        if(!empty($data) && $data->count())
         {
            try 
            {
                DB::beginTransaction();

                foreach ($data as $key => $item) 
                {
                    $id                         = $item->material_stock_id;
                    $new_type_stock             = $item->type_stock;
                    $qty_change                 = $item->qty_change;
                    $uom                        = $item->uom;
                    $material_stock             = MaterialStock::find($id);
                    $old_type_stock             = $material_stock->type_stock;
                    $old_type_stock_erp_code    = $material_stock->type_stock_erp_code;
                    $locator_id                 = $material_stock->locator_id;
                    $upload_date                = Carbon::now()->toDateTimeString();
                            
                   
                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$material_stock->warehouse_id]
                    ])
                    ->first();
    
                       
                        if($old_type_stock != $new_type_stock)
                        {
                            $old_available          = sprintf('%0.8f',$material_stock->available_qty);
                            $curr_available_qty     = sprintf('%0.8f',$material_stock->available_qty);
                            $cur_reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                            // dd($qty_change/$curr_available_qty);
                            if ($qty_change/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                            else $_supplied = $qty_change;
            
                            $mapping_stock              = MappingStocks::where('type_stock',$new_type_stock)->first();
                            $new_type_stock_erp_code    = ($mapping_stock)? $mapping_stock->type_stock_erp_code : null;
            
                            if($_supplied > 0 && $curr_available_qty > 0)
                            {
                                $date_now = carbon::now();
                                //insert new allocataion
                                AllocationItem::FirstOrCreate([
                                    'material_stock_id'         => $material_stock->id,
                                    'c_order_id'                => $material_stock->c_order_id,
                                    'document_no'               => $material_stock->document_no,
                                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                                    'supplier_code'             => $material_stock->supplier_code,
                                    'supplier_name'             => $material_stock->supplier_name,
                                    'item_id_source'            => $material_stock->item_id_source,
                                    'item_id_book'              => $material_stock->item_id,
                                    'item_code'                 => $material_stock->item_code,
                                    'item_code_source'          => $material_stock->item_code,
                                    'item_desc'                 => $material_stock->item_desc,
                                    'category'                  => $material_stock->category,
                                    'uom'                       => $material_stock->uom,
                                    'type_stock_material'       => $material_stock->type_stock,
                                    'warehouse'                 => $material_stock->warehouse_id,
                                    'qty_booking'               => sprintf('%0.8f',$_supplied),
                                    'is_not_allocation'         => true,
                                    'is_from_allocation_buyer'  => false,
                                    'user_id'                   => auth::user()->id,
                                    'confirm_user_id'           => auth::user()->id,
                                    'confirm_by_warehouse'      => 'approved',
                                    'confirm_date'              => carbon::now(),
                                    'created_at'                => $date_now,
                                    'remark'                    => 'DIPINDAHKAN KE TIPE STOCK '.$new_type_stock,
                                    'mm_approval_date'          => carbon::now(),
                                    'accounting_approval_date'  => carbon::now(),
                                    'accounting_user_id'        => $system->id,
                                    'mm_user_id'                => $system->id,
                                ]);
            
                            
                                //insert new stock
                                $is_exists = MaterialStock::whereNull('deleted_at')
                                ->where([
                                    [db::raw("upper(document_no)"),$material_stock->document_no],
                                    [db::raw('upper(item_code)'),strtoupper($material_stock->item_code)],
                                    ['type_po', 2],
                                    ['uom',$material_stock->uom],
                                    ['locator_id',$locator_id],
                                    ['warehouse_id',$material_stock->warehouse_id],
                                    ['is_material_others',$material_stock->is_material_others],
                                    ['is_closing_balance',false],
                                    ['is_running_stock',false],
                                    ['type_stock',$new_type_stock],
                                ])
                                ->whereNotNull('approval_date');
            
                                if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                                else $is_exists = $is_exists->whereNull('po_detail_id');
            
                                if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                                else $is_exists = $is_exists->whereNull('c_bpartner_id');
            
                                if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                                else $is_exists = $is_exists->whereNull('c_order_id');
            
                                if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                                else $is_exists = $is_exists->whereNull('po_buyer');
            
                                if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                                else $is_exists = $is_exists->whereNull('source');
            
                                $is_exists = $is_exists->first();
                                
                                if(!$is_exists)
                                {
                                    $new_material_stock = MaterialStock::FirstOrCreate([
                                        'uom_source'            => $material_stock->uom_source,
                                        'po_detail_id'          => $material_stock->po_detail_id,
                                        'document_no'           => $material_stock->document_no,
                                        'supplier_code'         => $material_stock->supplier_code,
                                        'supplier_name'         => $material_stock->supplier_name,
                                        'c_bpartner_id'         => $material_stock->c_bpartner_id,
                                        'c_order_id'            => $material_stock->c_order_id,
                                        'locator_id'            => $locator_id,
                                        'item_id'               => $material_stock->item_id,
                                        'item_code'             => $material_stock->item_code,
                                        'item_desc'             => $material_stock->item_desc,
                                        'category'              => $material_stock->category,
                                        'type_po'               => $material_stock->type_po,
                                        'warehouse_id'          => $material_stock->warehouse_id,
                                        'uom'                   => $material_stock->uom,
                                        'po_buyer'              => $material_stock->po_buyer,
                                        'qty_carton'            => $material_stock->qty_carton,
                                        'stock'                 => sprintf('%0.8f',$_supplied),
                                        'reserved_qty'          => 0,
                                        'available_qty'         => sprintf('%0.8f',$_supplied),
                                        'is_active'             =>  $material_stock->is_active,
                                        'is_material_others'    => $material_stock->is_material_others,
                                        'upc_item'              => $material_stock->upc_item,
                                        'source'                => $material_stock->source,
                                        'type_stock_erp_code'   => $new_type_stock_erp_code,
                                        'type_stock'            => $new_type_stock,
                                        'source'                => $material_stock->source,
                                        'user_id'               => $material_stock->user_id,
                                        'created_at'            => $upload_date,
                                        'updated_at'            => $upload_date,
                                    ]);
            
                                    $new_material_stock_id      = $new_material_stock->id;
                                    $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                    $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                                   
                                    HistoryStock::approved($new_material_stock_id
                                    ,$_supplied
                                    ,'0'
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,$remark_mutasi
                                    ,auth::user()->name
                                    ,auth::user()->id
                                    ,$new_type_stock_erp_code
                                    ,$new_type_stock
                                    ,false
                                    ,true);
                                    
                                    DetailMaterialStock::FirstOrCreate([
                                        'material_stock_id'         => $new_material_stock->id,
                                        'remark'                    => $remark_mutasi,
                                        'uom'                       => $new_material_stock->uom,
                                        'qty'                       => $_supplied,
                                        'locator_id'                => $material_stock->locator_id,
                                        'user_id'                   => Auth::user()->id,
                                        'created_at'                => $upload_date,
                                        'updated_at'                => $upload_date,
                                        'approve_date_mm'           => $upload_date,
                                        'approve_date_accounting'   => $upload_date,
                                        'mm_user_id'                => $system->id,
                                        'accounting_user_id'        => $system->id,
                                    ]);
            
                                }else
                                {
                                    $new_material_stock_id      = $is_exists->id;
                                    $new_stock                  = sprintf('%0.8f',$is_exists->stock);
                                    $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                                    $new_reserved_qty           = sprintf('%0.8f',$is_exists->reserved_qty);
                                    $_new_stock                 = sprintf('%0.8f',$new_stock + $_supplied);
                                    $new_availabilty_qty        = sprintf('%0.8f',$_new_stock - $new_reserved_qty);
            
                                    $is_exists->stock           = sprintf('%0.8f',$_new_stock);
                                    $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                                    
                                
                                    if($new_availabilty_qty > 0)
                                    {
                                        $is_exists->is_allocated    = false;
                                        $is_exists->is_active       = true;
                                    }
            
                                    $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                    $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock;
                                    
                                    HistoryStock::approved($new_material_stock_id
                                    ,$new_availabilty_qty
                                    ,$old_available_qty_curr
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,$remark_mutasi
                                    ,auth::user()->name
                                    ,auth::user()->id
                                    ,$new_type_stock_erp_code
                                    ,$new_type_stock
                                    ,false
                                    ,true);
                                    
                                    $is_exists->save();
            
                                    DetailMaterialStock::FirstOrCreate([
                                        'material_stock_id'         => $is_exists->id,
                                        'remark'                    => $remark_mutasi,
                                        'uom'                       => $is_exists->uom,
                                        'qty'                       => $_supplied,
                                        'locator_id'                => $is_exists->locator_id,
                                        'user_id'                   => Auth::user()->id,
                                        'created_at'                => $upload_date,
                                        'updated_at'                => $upload_date,
                                        'approve_date_mm'           => $upload_date,
                                        'approve_date_accounting'   => $upload_date,
                                        'mm_user_id'                => $system->id,
                                        'accounting_user_id'        => $system->id,
                                    ]);
                                }
            
                                $curr_available_qty -= $_supplied;
                                
                                $new = $old_available - $_supplied;
                                if($new <= 0) $new = '0';
                                else $new = $new;
            
                                $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                $remark_mutasi              = 'DI PINDAHKAN KE TIPE '. $_new_type_stock;
                                
                                HistoryStock::approved($id
                                ,$new
                                ,$old_available
                                ,null
                                ,null
                                ,null
                                ,null
                                ,null
                                ,$remark_mutasi
                                ,auth::user()->name
                                ,auth::user()->id
                                ,$old_type_stock_erp_code
                                ,$old_type_stock
                                ,false
                                ,true);
                                
                                $material_stock->reserved_qty   = sprintf('%0.8f',$cur_reserved_qty + $_supplied);
                                $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                                if($curr_available_qty <= 0)
                                {
                                    $material_stock->is_allocated   = true;
                                    $material_stock->is_active      = false;
                                }
            
                                $material_stock->save();
                                
                            }
            
                            $locator = Locator::find($locator_id);
                            if($locator)
                            {
                                $counter_locator = MaterialStock::where([
                                    ['locator_id',$locator_id],
                                    ['is_allocated',false],
                                    ['is_closing_balance',false],
                                ])
                                ->whereNull('deleted_at')
                                ->count();
            
                                $locator->counter_in = $counter_locator ;
                                $locator->save();
                            }
                            
            
                        }
            
                        DB::commit();
                        return response()->json(200);
                   
    
    
                     
                }
    
                
             
    
                DB::commit();
                return response()->json(200);
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
           
            return response()->json('Upload done',200);
        }else{
            return response()->json('Data not found.',422);
        }
            
            

            
        }
    }

    public function transferStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }

        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    }

    public function storeTransferStock(Request $request)
    {
        $free_stock_destination = Locator::with('area')
        ->whereHas('area',function ($query){
            $query->where([
                ['warehouse',auth::user()->warehouse],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();

        try 
        {
            DB::beginTransaction();

            $id                 = $request->id;
            $qty_transfer       = sprintf('%0.8f',$request->qty_transfer);
            $upload_date        = Carbon::now()->toDateTimeString();
            $material_stock     = MaterialStock::find($id);
            $available_qty      = sprintf('%0.8f',$material_stock->available_qty);
            $curr_available_qty = sprintf('%0.8f',$material_stock->available_qty);
            $curr_reserved_qty  = sprintf('%0.8f',$material_stock->reserved_qty);
            $warehouse_id       = $material_stock->warehouse_id;
            
            $system = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            
            if($warehouse_id == '1000013')
            {
                $to_warehouse           = '1000002';
                $to_warehouse_name      = 'AOI-1';
                $from_warehouse_name    = 'AOI-2';
            } 
            else if($warehouse_id == '1000002')
            {
                $to_warehouse           = '1000013';
                $to_warehouse_name      = 'AOI-2';
                $from_warehouse_name    = 'AOI-1';
            } 

            if($curr_available_qty > 0 && $qty_transfer > 0)
            {
                if ($qty_transfer/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                else $_supplied = $qty_transfer;

                AllocationItem::FirstOrCreate([
                    'material_stock_id'         => $material_stock->id,
                    'c_order_id'                => $material_stock->c_order_id,
                    'document_no'               => $material_stock->document_no,
                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                    'supplier_code'             => $material_stock->supplier_code,
                    'supplier_name'             => $material_stock->supplier_name,
                    'item_id_source'            => $material_stock->item_id_source,
                    'item_id_book'              => $material_stock->item_id,
                    'item_code'                 => $material_stock->item_code,
                    'item_code_source'          => $material_stock->item_code,
                    'item_desc'                 => $material_stock->item_desc,
                    'category'                  => $material_stock->category,
                    'type_stock_material'       => $material_stock->type_stock,
                    'uom'                       => $material_stock->uom,
                    'warehouse'                 => auth::user()->warehouse,
                    'qty_booking'               => sprintf('%0.8f',$_supplied),
                    'is_not_allocation'         => true,
                    'is_from_allocation_buyer'  => false,
                    'user_id'                   => auth::user()->id,
                    'confirm_user_id'           => auth::user()->id,
                    'confirm_by_warehouse'      => 'approved',
                    'confirm_date'              => carbon::now(),
                    'mm_approval_date'          => carbon::now(),
                    'accounting_approval_date'  => carbon::now(),
                    'accounting_user_id'        => $system->id,
                    'mm_user_id'                => $system->id,
                    'remark'                    => 'STOCK DI TRANSFER KE '.$to_warehouse_name,
                ]);

                $document_no    = $material_stock->document_no;
                $po_buyer       = $material_stock->po_buyer;

                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                {
                    $mapping_stock_id       = null;
                    $type_stock_erp_code    = '2';
                    $type_stock             = 'REGULER';
                }else
                {
                    $get_4_digit_from_beginning = substr($document_no,0, 4);
                    $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                    if($_mapping_stock_4_digt)
                    {
                        $mapping_stock_id       = $_mapping_stock_4_digt->id;
                        $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                        $type_stock             = $_mapping_stock_4_digt->type_stock;
                    }else
                    {
                        $get_5_digit_from_beginning = substr($document_no,0, 5);
                        $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                        if($_mapping_stock_5_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_5_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_5_digt->type_stock;
                        }else
                        {
                            if($po_buyer)
                                {
                                    $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                                    $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                                }else
                                {
                                    $mapping_stock_id       = null;
                                    $type_stock_erp_code    = null;
                                    $type_stock             = null;
                                }
                        }
                        
                    }
                }

                $new_material_stock = MaterialStock::FirstOrCreate([
                    'po_detail_id'        => $material_stock->po_detail_id,
                    'uom_source'          => $material_stock->uom_source,
                    'document_no'         => $material_stock->document_no,
                    'supplier_code'       => $material_stock->supplier_code,
                    'supplier_name'       => $material_stock->supplier_name,
                    'c_bpartner_id'       => $material_stock->c_bpartner_id,
                    'c_order_id'          => $material_stock->c_order_id,
                    'locator_id'          => $free_stock_destination->id,
                    'item_id'             => $material_stock->item_id,
                    'item_code'           => $material_stock->item_code,
                    'item_desc'           => $material_stock->item_desc,
                    'category'            => $material_stock->category,
                    'type_po'             => $material_stock->type_po,
                    'warehouse_id'        => $to_warehouse,
                    'uom'                 => $material_stock->uom,
                    'po_buyer'            => $material_stock->po_buyer,
                    'qty_carton'          => $material_stock->qty_carton,
                    'stock'               => sprintf('%0.8f',$_supplied),
                    'reserved_qty'        => 0,
                    'available_qty'       => sprintf('%0.8f',$_supplied),
                    'is_active'           => $material_stock->is_active,
                    'is_material_others'  => $material_stock->is_material_others,
                    'reject_date'         => null,
                    'approval_date'       => null,
                    'approval_user_id'    => null,
                    'source'              => $material_stock->source,
                    'user_id'             => $material_stock->user_id,
                    'mapping_stock_id'    => $mapping_stock_id,
                    'type_stock_erp_code' => $type_stock_erp_code,
                    'type_stock'          => $type_stock,
                    'created_at'          => $upload_date,
                    'updated_at'          => $upload_date,
                    'approval_user_id'    => $material_stock->user_id,
                    'approval_date'       => $upload_date,

                ]);

                $new_material_stock_id = $new_material_stock->id;

                DetailMaterialStock::FirstOrCreate([
                    'material_stock_id'         => $new_material_stock_id,
                    'remark'                    => 'DITRANSFER DARI '.$from_warehouse_name,
                    'uom'                       => $material_stock->uom,
                    'qty'                       => $_supplied,
                    'user_id'                   => Auth::user()->id,
                    'created_at'                => $upload_date,
                    'updated_at'                => $upload_date,
                    'approve_date_mm'           => $upload_date,
                    'approve_date_accounting'   => $upload_date,
                    'mm_user_id'                => $system->id,
                    'accounting_user_id'        => $system->id,
                ]);
                
                $curr_available_qty -= $_supplied;

                $material_stock->reserved_qty   = sprintf('%0.8f',$_supplied + $curr_reserved_qty);
                $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                
                if($curr_available_qty <=0)
                {
                    $material_stock->is_allocated       = true;
                    $material_stock->is_closing_balance = true;
                    $material_stock->is_active          = false;
                }
                
                $new = $available_qty - $_supplied;
                if($new <= 0) $new = '0';
                else $new = $new;

                HistoryStock::approved($id
                ,$new
                ,$available_qty
                ,null
                ,null
                ,null
                ,null
                ,null
                ,'TRANSFER STOCK KE '.$to_warehouse_name 
                ,auth::user()->name
                ,auth::user()->id
                ,$material_stock->type_stock_erp_code
                ,$material_stock->type_stock
                ,false
                ,true);

                $material_stock->save();
                
            }
       
        
            DB::commit();
            return response()->json($new_material_stock_id,200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }

        
    }

    public function transferStockBarcode(Request $request)
    {
        $material_stock_id  = trim($request->id,'"');
        $material_stock     = MaterialStock::where('id',$material_stock_id)->get();
        return view('report_material_stock_accessories.reprint_stock_transfer_barcode', compact('material_stock'));
    }

    public function moveLocator(Request $request,$id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->locator_id        = $material_stock->locator_id;
        $obj->locator_name      = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }

        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    } 

    public function locatorPicklist(Request $request)
    {
        $q = strtoupper($request->q);
        $lists = Locator::whereHas('area',function($query){
            $query->where('name','FREE STOCK')
            ->where('warehouse',auth::user()->warehouse);
        })
        ->where([
            ['is_active',true],
            ['rack','<>','PRINT BARCODE']
        ])
        ->where(function($query) use ($q){
            $query->where('code','LIKE',"%$q%")
            ->orWhere('rack','LIKE',"%$q%");
        })
        ->orderby('rack','asc')
        ->orderby('y_row','asc')
        ->orderby('z_column','asc')
       ->paginate('10');

        return view('accessories_report_material_stock._locator_list',compact('lists'));
    }

    public function storeMoveLocator(Request $request)
    {
        $id                             = $request->id;
        $move_type                      = $request->move_type;
        $new_locator_id                 = $request->locator_id;
        $new_locator_name               = $request->locator_name;
        $uom                            = $request->uom;
        $qty_available                  = sprintf('%0.8f',$request->qty_available);
        $qty_move                       = sprintf('%0.8f',$request->qty_move);
        $upload_date                    = Carbon::now()->toDateTimeString();

        $material_stock                 = MaterialStock::find($id);
        $curr_type_stock_erp_code       = $material_stock->type_stock_erp_code;
        $curr_type_stock                = $material_stock->type_stock;
        $old_locator_id                 = $material_stock->locator_id;
        $po_buyer                       = $material_stock->po_buyer;
        $old_available                  = sprintf('%0.8f',$material_stock->available_qty);
        $available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
        $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
        
        $locator_new                    = Locator::find($new_locator_id);
        $locator_old                    = Locator::find($old_locator_id);

        $system = User::where([
            ['name','system'],
            ['warehouse',$material_stock->warehouse_id]
        ])
        ->first();

        if($material_stock)
        {
            if($material_stock->item_id) $item_id = $material_stock->item_id;
        }else
        {
            $item       = Item::where('item_id',$material_stock->item_id)->first();
            $item_id    = ($item ? $item->item_id : null);
        }
       
        if($material_stock->last_status != 'prepared')
        {
            $material_stock->last_status    = 'prepared';
            $material_stock->save();

            if ($qty_move/$available_qty >= 1) $_supplied = sprintf('%0.8f',$available_qty);
            else $_supplied = sprintf('%0.8f',$qty_move);

            //return response()->json($_supplied);
            try 
            {
                DB::beginTransaction();
                if($old_locator_id != $new_locator_id)
                {
                    
                    if($_supplied > 0 && $available_qty > 0)
                    {
                        //insert new allocation for old locator
                        AllocationItem::Create([
                            'material_stock_id'         => $material_stock->id,
                            'c_order_id'                => $material_stock->c_order_id,
                            'document_no'               => $material_stock->document_no,
                            'item_id_source'            => $material_stock->item_id_source,
                            'item_id_book'              => $material_stock->item_id,
                            'item_code'                 => $material_stock->item_code,
                            'item_code_source'          => $material_stock->item_code,
                            'item_desc'                 => $material_stock->item_desc,
                            'category'                  => $material_stock->category,
                            'uom'                       => $material_stock->uom,
                            'type_stock_material'       => $material_stock->type_stock,
                            'warehouse'                 => $material_stock->warehouse_id,
                            'qty_booking'               => sprintf('%0.8f',$_supplied),
                            'is_not_allocation'         => true,
                            'is_from_allocation_buyer'  => false,
                            'user_id'                   => auth::user()->id,
                            'confirm_user_id'           => auth::user()->id,
                            'confirm_by_warehouse'      => 'approved',
                            'confirm_date'              => carbon::now(),
                            'mm_approval_date'          => carbon::now(),
                            'accounting_approval_date'  => carbon::now(),
                            'accounting_user_id'        => $system->id,
                            'mm_user_id'                => $system->id,
                            'remark'                    => 'PINDAH KE LOCATOR '.$locator_new->code,
                        ]);
        
                        $document_no = $material_stock->document_no;

                        if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                        {
                            $mapping_stock_id       = null;
                            $type_stock_erp_code    = '2';
                            $type_stock             = 'REGULER';
                        }else
                        {
                            $get_4_digit_from_beginning = substr($document_no,0, 4);
                            $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                            if($_mapping_stock_4_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_4_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_4_digt->type_stock;
                            }else
                            {
                                $get_5_digit_from_beginning = substr($document_no,0, 5);
                                $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                                if($_mapping_stock_5_digt)
                                {
                                    $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                    $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                    $type_stock             = $_mapping_stock_5_digt->type_stock;
                                }else
                                {
                                    if($po_buyer)
                                    {
                                        $master_po_buyer        = PoBuyer::where('po_buyer',$material_stock->po_buyer)->first();
                                        $mapping_stock_id       = null;
                                        $type_stock_erp_code    = ($master_po_buyer) ? $master_po_buyer->type_stock_erp_code : null;
                                        $type_stock             = ($master_po_buyer) ? $master_po_buyer->type_stock : null;
                                    }else
                                    {
                                        $mapping_stock_id       = null;
                                        $type_stock_erp_code    = null;
                                        $type_stock             = null;
                                    }
                                }
                                
                            }
                        }
                        
                        //check new stock is already exists
                        $is_exists = MaterialStock::whereNull('deleted_at')
                        ->where([
                            [db::raw("upper(document_no)"),$material_stock->document_no],
                            ['type_po', 2],
                            ['uom',$material_stock->uom],
                            ['locator_id',$new_locator_id],
                            ['warehouse_id',$material_stock->warehouse_id],
                            ['is_material_others',$material_stock->is_material_others],
                            ['type_stock',$material_stock->type_stock],
                            ['is_closing_balance',false],
                            ['is_stock_on_the_fly',false],
                            ['is_running_stock',false],
                        ])
                        ->whereNotNull('approval_date');

                        if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                        else $is_exists = $is_exists->whereNull('po_detail_id');
        
                        if($material_stock->item_id) $is_exists = $is_exists->where('item_id',$material_stock->item_id);
                        else $is_exists = $is_exists->where('item_code',$material_stock->item_code);

                        if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                        else $is_exists = $is_exists->whereNull('c_bpartner_id');
        
                        if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                        else $is_exists = $is_exists->whereNull('c_order_id');
        
                        if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                        else $is_exists = $is_exists->whereNull('po_buyer');
        
                        if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                        else $is_exists = $is_exists->whereNull('source');
        
                        $is_exists = $is_exists->first();
                        
                        if(!$is_exists)
                        {
                            $new_material_stock = MaterialStock::FirstOrCreate([
                                'po_detail_id'          => $material_stock->po_detail_id,
                                'document_no'           => $material_stock->document_no,
                                'supplier_code'         => $material_stock->supplier_code,
                                'supplier_name'         => $material_stock->supplier_name,
                                'c_bpartner_id'         => $material_stock->c_bpartner_id,
                                'c_order_id'            => $material_stock->c_order_id,
                                'locator_id'            => $new_locator_id,
                                'item_id'               => $material_stock->item_id,
                                'item_code'             => $material_stock->item_code,
                                'uom_source'            => $material_stock->uom_source,
                                'item_desc'             => $material_stock->item_desc,
                                'category'              => $material_stock->category,
                                'type_po'               => $material_stock->type_po,
                                'warehouse_id'          => $material_stock->warehouse_id,
                                'uom'                   => $material_stock->uom,
                                'po_buyer'              => $material_stock->po_buyer,
                                'qty_carton'            => $material_stock->qty_carton,
                                'stock'                 => sprintf('%0.8f',$_supplied),
                                'reserved_qty'          => 0,
                                'available_qty'         => sprintf('%0.8f',$_supplied),
                                'is_active'             => $material_stock->is_active,
                                'is_material_others'    => $material_stock->is_material_others,
                                'approval_date'         => carbon::now(),
                                'approval_user_id'      => $material_stock->approval_user_id,
                                'source'                => $material_stock->source,
                                'user_id'               => $material_stock->user_id,
                                'mapping_stock_id'      => $material_stock->mapping_stock_id,
                                'type_stock_erp_code'   => $material_stock->type_stock_erp_code,
                                'type_stock'            => $material_stock->type_stock,
                                'created_at'            => $upload_date,
                                'updated_at'            => $upload_date,
                            ]);

                            $new_material_stock_id      = $new_material_stock->id;
                            HistoryStock::approved($new_material_stock_id
                            ,$_supplied
                            ,'0'
                            ,null
                            ,null
                            ,null
                            ,null
                            ,null
                            ,'MOVE LOCATOR FROM '.($locator_old ? $locator_old->code : 'FREE-STOCK FREE STOCK')
                            ,auth::user()->name
                            ,auth::user()->id
                            ,$new_material_stock->type_stock_erp_code
                            ,$new_material_stock->type_stock
                            ,false
                            ,true
                            );

                            DetailMaterialStock::Create([
                                'material_stock_id'         => $new_material_stock->id,
                                'remark'                    => 'STOCK BERASAL DARI PERPINDAHAN RAK '.($locator_old ? $locator_old->code : 'FREE-STOCK FREE STOCK'),
                                'uom'                       => $new_material_stock->uom,
                                'qty'                       => $_supplied,
                                'locator_id'                => $new_material_stock->locator_id,
                                'user_id'                   => Auth::user()->id,
                                'created_at'                => $upload_date,
                                'updated_at'                => $upload_date,
                                'approve_date_mm'           => $upload_date,
                                'approve_date_accounting'   => $upload_date,
                                'mm_user_id'                => $system->id,
                                'accounting_user_id'        => $system->id,
                            ]);

                        }else
                        {
                            $new_material_stock_id      = $is_exists->id;
                            $is_exist_stock             = sprintf('%0.8f',$is_exists->stock);
                            $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                            $is_exist_reserved_qty      = sprintf('%0.8f',$is_exists->reserved_qty);
                            $new_stock                  = sprintf('%0.8f',$is_exist_stock + $_supplied);
                            $new_availabilty_qty        = sprintf('%0.8f',$new_stock - $is_exist_reserved_qty);
        
                            $is_exists->stock           = sprintf('%0.8f',$new_stock);
                            $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                            
                        
                            if($new_availabilty_qty > 0)
                            {
                                $is_exists->is_allocated    = false;
                                $is_exists->is_active       = true;
                            }
        
                            HistoryStock::approved($new_material_stock_id
                            ,$new_availabilty_qty
                            ,$old_available_qty_curr
                            ,null
                            ,null
                            ,null
                            ,null
                            ,null
                            ,'MOVE LOCATOR FROM '.($locator_old ? $locator_old->code : 'FREE-STOCK FREE STOCK')
                            ,auth::user()->name
                            ,auth::user()->id
                            ,$is_exists->type_stock_erp_code
                            ,$is_exists->type_stock
                            ,false
                            ,false);

                            DetailMaterialStock::Create([
                                'material_stock_id'     => $is_exists->id,
                                'remark'                => 'STOCK BERASAL DARI PERPINDAHAN RAK '.($locator_old ? $locator_old->code : 'FREE-STOCK FREE STOCK'),
                                'uom'                   => $is_exists->uom,
                                'qty'                   => $_supplied,
                                'locator_id'            => $is_exists->locator_id,
                                'user_id'               => Auth::user()->id,
                                'created_at'            => $upload_date,
                                'updated_at'            => $upload_date,
                                'approve_date_mm'           => $upload_date,
                                'approve_date_accounting'   => $upload_date,
                                'mm_user_id'                => $system->id,
                                'accounting_user_id'        => $system->id,
                            ]);

                            $is_exists->save();
                        }
        
                        $available_qty -= $_supplied;
                        //update old stock
                        HistoryStock::approved($id
                        ,$available_qty
                        ,$old_available
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,'MOVE LOCATOR TO '.($locator_new ? $locator_new->code : 'FREE-STOCK FREE STOCK')
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$curr_type_stock_erp_code
                        ,$curr_type_stock
                        ,false
                        ,false);

                        $material_stock->reserved_qty   = sprintf('%0.8f',$reserved_qty + $_supplied);
                        $material_stock->available_qty  = sprintf('%0.8f',$available_qty);
                        
                        if($available_qty <= 0)
                        {
                            $material_stock->is_allocated           = true;
                            $material_stock->is_active              = false;
                            $material_stock->is_closing_balance     = true;
                            $locator_old                            = Locator::find($old_locator_id);
                            
                            if($locator_old)
                            {
                                $count_item_on_locator_old = MaterialStock::where([
                                    ['locator_id',$locator_old],
                                    ['is_allocated',false],
                                    ['is_closing_balance',false],
                                ])
                                ->whereNull('deleted_at')
                                ->count();

                                $counter                    = $locator_old->counter_in;
                                $locator_old->counter_in    = $count_item_on_locator_old ;
                                $locator_old->save();
                            }
                            
                        }
                        
                        $material_stock->last_status    = null;
                        $material_stock->save();
                        
                    }
                    
                    $count_item_on_locator_new = MaterialStock::where([
                        ['locator_id',$new_locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();

                    if($locator_new)
                    {
                        $locator_new->counter_in = $count_item_on_locator_new;
                        $locator_new->save();
                    }

                    $count_item_on_locator_old = MaterialStock::where([
                        ['locator_id',$old_locator_id],
                        ['is_allocated',false],
                        ['is_closing_balance',false],
                    ])
                    ->whereNull('deleted_at')
                    ->count();

                    if($locator_old)
                    {
                        $locator_old->counter_in = $count_item_on_locator_old;
                        $locator_old->save();
                    }
                }
            
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }else
        {
            return response()->json('stock in used',422);
        }
        
        
        return response()->json(200);
        
    }

    public function editStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }

        return response()->json($obj,200);
    }

    public function storeEditStock(Request $request)
    {
        $new_available_qty_stock    = sprintf('%0.8f',$request->new_available_qty_stock);

        $material_stock             = MaterialStock::find($request->id);
        $curr_type_stock            = $material_stock->type_stock;
        $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
        $curr_available_qty         = sprintf('%0.8f',$material_stock->available_qty);
        $locator_id                 = $material_stock->locator_id;
        $operator                   = $new_available_qty_stock - $curr_available_qty;
        $remark                     = trim(strtoupper($request->update_reason));
        $upload_date                = Carbon::now()->toDateTimeString();

        try 
        {
            DB::beginTransaction();
            
            if($operator > 0)
            {
                $detail_material_stock = DetailMaterialStock::FirstOrCreate([
                    'material_stock_id'     => $material_stock->id,
                    'remark'                => 'ada tambahan stock dari warehouse di dapatkan dari '.$remark,
                    'uom'                   => $material_stock->uom,
                    'qty'                   => $operator,
                    'locator_id'            => $material_stock->locator_id,
                    'user_id'               => Auth::user()->id,
                    'created_at'            => $upload_date,
                    'updated_at'            => $upload_date,
                    'source'                => 'mutasi',
                ]);

                HistoryStock::itemApprove($detail_material_stock->id, 'detail_material_stocks');

            }else
            {
                //lock adjustment minus(-)
                if(auth::user()->hasRole(['admin-ict-acc']))
                {
                    $new_reserved_qty = $material_stock->reserved_qty + (-1*$operator);
                    $detail_material_stock = AllocationItem::Create([
                        'material_stock_id'         => $material_stock->id,
                        'c_order_id'                => $material_stock->c_order_id,
                        'document_no'               => $material_stock->document_no,
                        'c_bpartner_id'             => $material_stock->c_bpartner_id,
                        'supplier_code'             => $material_stock->supplier_code,
                        'supplier_name'             => $material_stock->supplier_name,
                        'item_id_source'            => $material_stock->item_id_source,
                        'item_id_book'              => $material_stock->item_id,
                        'item_code'                 => $material_stock->item_code,
                        'item_code_source'          => $material_stock->item_code,
                        'item_desc'                 => $material_stock->item_desc,
                        'category'                  => $material_stock->category,
                        'uom'                       => $material_stock->uom,
                        'warehouse'                 => $material_stock->warehouse_id,
                        'type_stock_material'       => $material_stock->type_stock,
                        'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                        'is_not_allocation'         => true,
                        'is_from_allocation_buyer'  => false,
                        'user_id'                   => auth::user()->id,
                        'confirm_user_id'           => auth::user()->id,
                        'confirm_by_warehouse'      => 'approved',
                        'confirm_date'              => carbon::now(),
                        'remark'                    => $remark,
                    ]);

                    HistoryStock::itemApprove($detail_material_stock->id, 'allocation_items');

                }
                else
                {
                    return response()->json('Adjustment minus tidak diijinkan, silahkan buat BA ',422); 
                }
                
            }
            DB::commit();
            
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }

    public function restoreStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    }

    public function storeRestoreStock(Request $request)
    {
        $restore_available_qty_stock    = sprintf('%0.8f',$request->restore_available_qty_stock);
        $remark                         = trim(strtoupper($request->restore_reason));
        $upload_date                    = Carbon::now()->toDateTimeString();
       
        $material_stock             = MaterialStock::find($request->id);
        $curr_c_bpartner_id         = $material_stock->c_bpartner_id;
        $curr_c_order_id            = $material_stock->c_order_id;
        $curr_po_detail_id          = $material_stock->po_detail_id;
        $curr_supplier_name         = $material_stock->supplier_name;
        $curr_supplier_code         = $material_stock->supplier_code;
        $curr_item_id               = $material_stock->item_id;
        $curr_item_code             = $material_stock->item_code;
        $curr_item_desc             = $material_stock->item_desc;
        $curr_type_stock            = $material_stock->type_stock;
        $curr_category              = $material_stock->category;
        $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
        $curr_uom                   = $material_stock->uom;
        $curr_po_detail_id          = $material_stock->po_detail_id;
        $curr_document_no           = $material_stock->document_no;
        $curr_type_po               = $material_stock->type_po;
        $curr_warehouse_id          = $material_stock->warehouse_id;
        $curr_po_buyer              = $material_stock->po_buyer;
        $curr_qty_carton            = $material_stock->qty_carton;
        $curr_is_material_others    = $material_stock->is_material_others;
        $curr_mapping_stock_id      = $material_stock->mapping_stock_id;
        $curr_uom_source            = $material_stock->uom_source;
        $curr_uom_nomor_roll        = '-';

        $free_stock_destination     = Locator::with('area')
        ->whereHas('area',function ($query) use ($curr_warehouse_id)
        {
            $query->where([
                ['warehouse',$curr_warehouse_id],
                ['name','FREE STOCK'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $system = User::where([
            ['name','system'],
            ['warehouse',$curr_warehouse_id]
        ])
        ->first();
        

        try 
        {
            DB::beginTransaction();
            
            $new_material_stock = MaterialStock::FirstOrCreate([
                'po_detail_id'                  => $curr_po_detail_id,
                'document_no'                   => $curr_document_no,
                'supplier_code'                 => $curr_supplier_code,
                'supplier_name'                 => $curr_supplier_name,
                'c_bpartner_id'                 => $curr_c_order_id,
                'c_order_id'                    => $curr_c_order_id,
                'locator_id'                    => $free_stock_destination->id,
                'item_id'                       => $curr_item_id,
                'item_code'                     => $curr_item_code,
                'item_desc'                     => $curr_item_desc,
                'category'                      => $curr_category,
                'type_po'                       => $curr_type_po,
                'warehouse_id'                  => $curr_warehouse_id,
                'uom'                           => $curr_uom,
                'po_buyer'                      => $curr_po_buyer,
                'qty_carton'                    => $curr_qty_carton,
                'stock'                         => $restore_available_qty_stock,
                'reserved_qty'                  => 0,
                'available_qty'                 => $restore_available_qty_stock,
                'is_active'                     => true,
                'is_material_others'            => $curr_is_material_others,
                'reject_date'                   => null,
                'approval_date'                 => $upload_date,
                'approval_user_id'              => auth::user()->id,
                'source'                        => 'RESTORE STOCK',
                'user_id'                       => auth::user()->id,
                'mapping_stock_id'              => $curr_mapping_stock_id,
                'type_stock_erp_code'           => $curr_type_stock_erp_code,
                'type_stock'                    => $curr_type_stock,
                'uom_source'                    => $curr_uom_source,
                'created_at'                    => $upload_date,
                'updated_at'                    => $upload_date,
            ]);

            $new_material_stock_id = $new_material_stock->id;

            DetailMaterialStock::Create([
                'material_stock_id'         => $new_material_stock_id,
                'remark'                    => 'RESTORE STOCK. [ '.$remark.']',
                'uom'                       => $curr_uom,
                'qty'                       => $restore_available_qty_stock,
                'user_id'                   => Auth::user()->id,
                'created_at'                => $upload_date,
                'updated_at'                => $upload_date,
                'approve_date_mm'           => $upload_date,
                'approve_date_accounting'   => $upload_date,
                'mm_user_id'                => $system->id,
                'accounting_user_id'        => $system->id,
            ]);
            

            HistoryStock::approved($request->id
                ,$restore_available_qty_stock
                ,'0'
                ,null
                ,null
                ,null
                ,null
                ,null
                ,null
                ,auth::user()->name
                ,auth::user()->id
                ,$curr_type_stock_erp_code
                ,$curr_type_stock
                ,false
                ,false
            );

            if($curr_po_detail_id == 'FREE STOCK' || $curr_po_detail_id == 'FREE STOCK-CELUP')
            {
                $no_packing_list        = '-';
                $no_invoice             = '-';
                $c_orderline_id         = '-';
            }else
            {
                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$curr_po_detail_id)
                ->first();

                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
            }

            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($curr_warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$curr_warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();


            $is_movement_exists = MaterialMovement::where([
                ['from_location',$inventory_erp->id],
                ['to_destination',$inventory_erp->id],
                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                ['is_integrate',false],
                ['is_active',true],
                ['no_packing_list',$no_packing_list],
                ['no_invoice',$no_invoice],
                ['status','integration-to-inventory-erp'],
            ])
            ->first();
            
            if(!$is_movement_exists)
            {
                $material_movement = MaterialMovement::firstOrCreate([
                    'from_location'         => $inventory_erp->id,
                    'to_destination'        => $inventory_erp->id,
                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                    'is_integrate'          => false,
                    'is_active'             => true,
                    'no_packing_list'       => $no_packing_list,
                    'no_invoice'            => $no_invoice,
                    'status'                => 'integration-to-inventory-erp',
                    'created_at'            => $new_material_stock->created_at,
                    'updated_at'            => $new_material_stock->created_at,
                ]);

                $material_movement_id           = $material_movement->id;
                $material_movement_created_at   = $material_movement->created_at;
            }else
            {
                $is_movement_exists->updated_at = $new_material_stock->created_at;
                $is_movement_exists->save();

                $material_movement_id           = $is_movement_exists->id;
                $material_movement_created_at   = $new_material_stock->created_at;
            }

            $is_line_exists = MaterialMovementLine::whereNotNull('material_stock_id')
            ->where([
                ['material_movement_id',$material_movement_id],
                ['material_stock_id',$new_material_stock_id],
                ['qty_movement',$restore_available_qty_stock],
                ['is_integrate',false],
                ['is_active',true],
                ['date_movement',$material_movement_created_at],
            ])
            ->exists();

            if(!$is_line_exists)
            {
                MaterialMovementLine::Create([
                    'material_movement_id'          => $material_movement_id,
                    'material_stock_id'             => $new_material_stock_id,
                    'item_code'                     => $curr_item_code,
                    'item_id'                       => $curr_item_id,
                    'c_order_id'                    => $curr_c_order_id,
                    'c_orderline_id'                => $c_orderline_id,
                    'c_bpartner_id'                 => $curr_c_bpartner_id,
                    'supplier_name'                 => $curr_supplier_name,
                    'type_po'                       => 1,
                    'is_integrate'                  => false,
                    'uom_movement'                  => $curr_uom,
                    'qty_movement'                  => $restore_available_qty_stock,
                    'date_movement'                 => $material_movement_created_at,
                    'created_at'                    => $material_movement_created_at,
                    'updated_at'                    => $material_movement_created_at,
                    'date_receive_on_destination'   => $material_movement_created_at,
                    'nomor_roll'                    => $curr_uom_nomor_roll,
                    'warehouse_id'                  => $curr_warehouse_id,
                    'document_no'                   => $curr_document_no,
                    'note'                          => 'RESTORE STOCK. [ '.$remark.']',
                    'is_active'                     => true,
                    'user_id'                       => auth::user()->id,
                ]);
            }
            
                        
            $count_item_on_locator = MaterialStock::where([
                ['locator_id',$free_stock_destination->id],
                ['is_allocated',false],
                ['is_closing_balance',false],
            ])
            ->whereNull('deleted_at')
            ->count();

            if($free_stock_destination)
            {
                $free_stock_destination->counter_in = $count_item_on_locator;
                $free_stock_destination->save();
            }

            DB::commit();
            
            return response()->json(200);
        } catch (Exception $e) 
        {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
    	}
    }

    public function deleteStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->locator_id        = $material_stock->locator_id;
        $obj->locator_name      = ($material_stock->locator_id) ? $material_stock->locator->code : null;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }

        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        return response()->json($obj,200);
    }

    public function storeDeleteStock(Request $request)
    {
        $id                             = $request->id;
        $delete_type                    = $request->delete_type;
        $uom                            = $request->delete_uom;
        $delete_qty                     = $request->delete_qty;
        $delete_reason                  = strtoupper($request->delete_reason);

        $material_stock                 = MaterialStock::find($id);
        $locator_id                     = $material_stock->locator_id;
        $locator                        = Locator::find($locator_id);
        $old_available_qty              = $material_stock->available_qty;
        $curr_available_qty             = $material_stock->available_qty;
        $curr_reserved_qty              = $material_stock->reserved_qty;
        $curr_type_stock                = $material_stock->type_stock;
        $curr_type_stock_erp_code       = $material_stock->type_stock_erp_code;

        try 
        {
    		DB::beginTransaction();
            
            if($material_stock)
            {
                if($material_stock->last_status != 'prepared')
                {
                    $material_stock->last_status = 'prepared';
                    $material_stock->save();

                    if ($delete_qty/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                    else $_supplied = $delete_qty;

                    //echo $_supplied.' '.$curr_available_qty;
                    if($_supplied > 0 && $curr_available_qty > 0)
                    {
                        AllocationItem::Create([
                            'material_stock_id'             => $material_stock->id,
                            'c_order_id'                    => $material_stock->c_order_id,
                            'document_no'                   => $material_stock->document_no,
                            'item_id_source'                => $material_stock->item_id_source,
                            'item_id_book'                  => $material_stock->item_id,
                            'item_code'                     => $material_stock->item_code,
                            'item_code_source'              => $material_stock->item_code,
                            'item_desc'                     => $material_stock->item_desc,
                            'category'                      => $material_stock->category,
                            'uom'                           => $material_stock->uom,
                            'type_stock_material'           => $material_stock->type_stock,
                            'warehouse'                     => $material_stock->warehouse_id,
                            'qty_booking'                   => sprintf('%0.8f',$_supplied),
                            'is_not_allocation'             => true,
                            'is_from_allocation_buyer'      => false,
                            'user_id'                       => auth::user()->id,
                            'confirm_user_id'               => auth::user()->id,
                            'confirm_by_warehouse'          => 'approved',
                            'confirm_date'                  => carbon::now(),
                            'remark'                        => 'STOCK DI HAPUS, '.$delete_reason,
                        ]);

                        $material_stock->last_status = null;
                        $material_stock->save();
                    }
                } 
            }

            if($locator)
            {
                $count_item_on_locator = MaterialStock::where([
                    ['locator_id',$locator->id],
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                ])
                ->whereNull('deleted_at')
                ->count();

                $locator->counter_in = $count_item_on_locator;
                $locator->save();
            }

            DB::commit();
            return response()->json(200);
    	} catch (Exception $e) {
    		DB::rollBack();
    		$message = $e->getMessage();
    		ErrorHandler::db($message);
        }
    }

    public function deleteBulk()
    {
        return view('accessories_report_material_stock.delete_bulk');
    }

    public function exportFormImportDeleteBulk()
    {
        return Excel::create('delete_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeDeleteBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();

                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id  = $value->material_stock_id;
                        $reason             = strtoupper($value->reason);
                        
                        if($material_stock_id)
                        {
                            
                            if($reason)
                            {
                                try
                                {
                                    db::beginTransaction();
                                    $material_stock     = MaterialStock::find($material_stock_id);
                                    $old_available_qty  = $material_stock->available_qty;

                                    $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                                    ->where('material_stock_id',$material_stock_id)
                                    ->exists();
                            
                                    if(!$has_outstanding_approval_exists)
                                    {
                                        if($material_stock->is_closing_balance == false)
                                        {
                                            $system = User::where([
                                                ['name','system'],
                                                ['warehouse',$material_stock->warehouse_id]
                                            ])
                                            ->first();

                                            AllocationItem::FirstOrCreate([
                                                'material_stock_id'         => $material_stock->id,
                                                'c_order_id'                => $material_stock->c_order_id,
                                                'document_no'               => $material_stock->document_no,
                                                'c_bpartner_id'             => $material_stock->c_bpartner_id,
                                                'supplier_code'             => $material_stock->supplier_code,
                                                'supplier_name'             => $material_stock->supplier_name,
                                                'item_id_source'            => $material_stock->item_id_source,
                                                'item_id_book'              => $material_stock->item_id,
                                                'item_code'                 => $material_stock->item_code,
                                                'item_code_source'          => $material_stock->item_code,
                                                'item_desc'                 => $material_stock->item_desc,
                                                'category'                  => $material_stock->category,
                                                'type_stock_material'       => $material_stock->type_stock,
                                                'uom'                       => $material_stock->uom,
                                                'warehouse'                 => auth::user()->warehouse,
                                                'qty_booking'               => sprintf('%0.8f',$material_stock->available_qty),
                                                'is_not_allocation'         => true,
                                                'user_id'                   => auth::user()->id,
                                                'confirm_user_id'           => auth::user()->id,
                                                'mm_approval_date'          => carbon::now(),
                                                'accounting_approval_date'  => carbon::now(),
                                                'accounting_user_id'        => $system->id,
                                                'mm_user_id'                => $system->id,
                                                'confirm_by_warehouse'      => 'approved',
                                                'confirm_date'              => carbon::now(),
                                                'remark'                    => 'STOCK DI HAPUS, '.$reason,
                                            ]);
                            
                                            $material_stock->reserved_qty       = sprintf('%0.8f',$material_stock->available_qty + $material_stock->reserved_qty);
                                            $material_stock->available_qty      = 0;
                                            $material_stock->is_allocated       = true;
                                            $material_stock->is_closing_balance = true;
                                            $material_stock->is_active          = false;
                            
                                            HistoryStock::approved($material_stock->id
                                            ,'0'
                                            ,$old_available_qty
                                            ,null
                                            ,null
                                            ,null
                                            ,null
                                            ,null
                                            ,'DELETE STOCK'
                                            ,auth::user()->name
                                            ,auth::user()->id
                                            ,$material_stock->type_stock_erp_code
                                            ,$material_stock->type_stock
                                            ,false
                                            ,true);
                            
                                            $material_stock->save();

                                            $obj                    = new stdClass();
                                            $obj->supplier_name     = $material_stock->supplier_name;
                                            $obj->document_no       = $material_stock->document_no;
                                            $obj->item_code         = $material_stock->item_code;
                                            $obj->uom               = $material_stock->uom;
                                            $obj->qty_available     = $old_available_qty;
                                            $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                            $obj->reason            = $reason;
                                            $obj->error_upload      = false;
                                            $obj->remark            = 'DATA BERHASIL DI HAPUS';
                                            $array [] = $obj;
                                        }else
                                        {
                                            $obj                    = new stdClass();
                                            $obj->supplier_name     = $material_stock->supplier_name;
                                            $obj->document_no       = $material_stock->document_no;
                                            $obj->item_code         = $material_stock->item_code;
                                            $obj->uom               = $material_stock->uom;
                                            $obj->qty_available     = $material_stock->qty_available;
                                            $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                            $obj->reason            = $reason;
                                            $obj->error_upload      = true;
                                            $obj->remark            = 'ERROR, ID SUDAH TIDAK AKTIF';
                                            $array [] = $obj;
                                        }
                                    }else
                                    {
                                            $obj                    = new stdClass();
                                            $obj->supplier_name     = $material_stock->supplier_name;
                                            $obj->document_no       = $material_stock->document_no;
                                            $obj->item_code         = $material_stock->item_code;
                                            $obj->uom               = $material_stock->uom;
                                            $obj->qty_available     = $material_stock->qty_available;
                                            $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                            $obj->reason            = $reason;
                                            $obj->error_upload      = true;
                                            $obj->remark            = 'ERROR, ID ADA OUTSTANDING APPROVAL STOCK, SILAHKAN APPROVE TERLEBIH DAHULU';
                                            $array [] = $obj;
                                    }

                                    
                                    db::commit();
                                }catch (Exception $ex){
                                    db::rollback();
                                    $message = $ex->getMessage();
                                    ErrorHandler::db($message);
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, SILAHKAN ISI ALASANNYA TERLEBIH DAHULU';
                                $array [] = $obj;
                            }
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }
                
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                    
            }
        }

        return response()->json($array,'200');
    }

    public function changeTypeBulk()
    {
        return view('accessories_report_material_stock.change_type_bulk');
    }

    public function exportFormImportChangeTypeBulk()
    {
        return Excel::create('change_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','TYPE_STOCK_NEW');
                $sheet->setCellValue('C1','QTY_CHANGE');
                $sheet->setCellValue('D1','REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function changeSourceBulk()
    {
        return view('accessories_report_material_stock.change_source_bulk');
    }

    public function exportFormImportChangeSourceBulk()
    {
        return Excel::create('change_source_material',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','ACTUAL_STOCK');
                $sheet->setCellValue('C1','REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeChangeSourceBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                //dd($data);
                foreach ($data as $key => $value) 
                {
                    $id                               = $value->material_stock_id;
                    $reason                           = strtoupper(trim($value->reason));
                    $actual_stock                     = sprintf('%0.8f',$value->actual_stock);

                    if($actual_stock)
                    {
                        if($reason)
                        {
                            $material_stock                   = MaterialStock::find($id);
                            
                            if($material_stock)
                            {
                                if($material_stock->is_closing_balance == false)
                                {
                                    $available_qty                    = sprintf('%0.8f',$material_stock->available_qty);
                                    try 
                                    {    
                                        DB::beginTransaction();

                                        $material_stock->source           = 'NON RESERVED STOCK';
                                        $material_stock->is_running_stock = false;
                                        $material_stock->save();
                                        
                                        $system                     = User::where([
                                            ['name','system'],
                                            ['warehouse',$material_stock->warehouse_id]
                                        ])
                                        ->first();

                                        if($available_qty > $actual_stock)
                                        {
                                            $operator                         = sprintf('%0.8f',$actual_stock - $available_qty);
                                        
                                            HistoryStock::approved($material_stock->id
                                                ,$actual_stock
                                                ,'0'
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,'PEMINDAHAN DARI RESERVED STOCK KE NON RESERVED STOCK'
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$material_stock->type_stock_erp_code
                                                ,$material_stock->type_stock
                                                ,false
                                                ,true
                                            );

                                            AllocationItem::Create([
                                                'material_stock_id'         => $material_stock->id,
                                                'c_order_id'                => $material_stock->c_order_id,
                                                'document_no'               => $material_stock->document_no,
                                                'c_bpartner_id'             => $material_stock->c_bpartner_id,
                                                'supplier_code'             => $material_stock->supplier_code,
                                                'supplier_name'             => $material_stock->supplier_name,
                                                'item_id_source'            => $material_stock->item_id,
                                                'item_id_book'              => $material_stock->item_id,
                                                'item_code'                 => $material_stock->item_code,
                                                'item_code_source'          => $material_stock->item_code,
                                                'item_desc'                 => $material_stock->item_desc,
                                                'category'                  => $material_stock->category,
                                                'uom'                       => $material_stock->uom,
                                                'warehouse'                 => $material_stock->warehouse_id,
                                                'type_stock_material'       => $material_stock->type_stock,
                                                'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                                                'is_not_allocation'         => true,
                                                'is_from_allocation_buyer'  => false,
                                                'user_id'                   => auth::user()->id,
                                                'confirm_user_id'           => auth::user()->id,
                                                'confirm_by_warehouse'      => 'approved',
                                                'confirm_date'              => carbon::now(),
                                                'mm_approval_date'          => carbon::now(),
                                                'accounting_approval_date'  => carbon::now(),
                                                'mm_user_id'                => $system->id,
                                                'accounting_user_id'        => $system->id,
                                                'remark'                    => $reason,
                                            ]);

                                            $new_reserved_qty               = $material_stock->reserved_qty + (-1*$operator);
                                            $material_stock->reserved_qty   = $new_reserved_qty;
                                            $material_stock->available_qty  = $actual_stock;
                                        }else
                                        {
                                            HistoryStock::approved($material_stock->id
                                                ,$material_stock->available_qty
                                                ,'0'
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,'PEMINDAHAN DARI RESERVED STOCK KE NON RESERVED STOCK'
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$material_stock->type_stock_erp_code
                                                ,$material_stock->type_stock
                                                ,false
                                                ,true)
                                            ;
                                        }
                                        
                                        $material_stock->save();

                                        DB::commit();

                                        $obj                 = new stdClass();
                                        $obj->supplier_name  = $material_stock->supplier_name;
                                        $obj->document_no    = $material_stock->document_no;
                                        $obj->item_code      = $material_stock->item_code;
                                        $obj->uom            = $material_stock->uom;
                                        $obj->qty_available  = $actual_stock;
                                        $obj->warehouse_name = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                        $obj->reason         = $reason;
                                        $obj->error_upload   = false;
                                        $obj->remark         = 'DATA BERHASIL DI UBAH KE NON RESERVED';
                                        $array []            = $obj;

                                        } catch (Exception $e) 
                                        {
                                            DB::rollBack();
                                            $message = $e->getMessage();
                                            ErrorHandler::db($message);
                                        }

                                }
                                else
                                {
                                    $obj                 = new stdClass();
                                    $obj->supplier_name  = $material_stock->supplier_name;
                                    $obj->document_no    = $material_stock->document_no;
                                    $obj->item_code      = $material_stock->item_code;
                                    $obj->uom            = $material_stock->uom;
                                    $obj->qty_available  = $material_stock->qty_available;
                                    $obj->warehouse_name = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                    $obj->reason         = $reason;
                                    $obj->error_upload   = true;
                                    $obj->remark         = 'ERROR, ID SUDAH TIDAK AKTIF';
                                    $array []            = $obj;

                                }

                            }
                            else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, ID '.$id.' TIDAK DITEMUKAN';
                                $array [] = $obj;
                            }
                        }
                        else
                        {
                            $obj                 = new stdClass();
                            $obj->supplier_name  = '-';
                            $obj->document_no    = '-';
                            $obj->item_code      = '-';
                            $obj->uom            = '-';
                            $obj->qty_available  = '-';
                            $obj->warehouse_name = '-';
                            $obj->reason         = $reason;
                            $obj->error_upload   = true;
                            $obj->remark         = 'ERROR, '.$id.' REASON STOCK HARUS DI ISI';
                            $array []                   = $obj;

                        }
                    }
                    else
                    {
                        $obj                 = new stdClass();
                        $obj->supplier_name  = '-';
                        $obj->document_no    = '-';
                        $obj->item_code      = '-';
                        $obj->uom            = '-';
                        $obj->qty_available  = '-';
                        $obj->warehouse_name = '-';
                        $obj->reason         = $reason;
                        $obj->error_upload   = true;
                        $obj->remark         = 'ERROR, '.$id.'  ACTUAL STOCK HARUS DI ISI';
                        $array []                   = $obj;

                    }
                }
            }
        }
        return response()->json($array,'200');
    } 

    public function storeChangeTypeBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                $upload_date                = Carbon::now()->toDateTimeString();
                try 
                {
                    DB::beginTransaction();
                    
                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id  = $value->material_stock_id;
                        $type_stock_new     = strtoupper($value->type_stock_new);
                        $reason             = strtoupper($value->reason);
                        $qty_change         =  sprintf('%0.8f',$value->qty_change);
                        
                        if($type_stock_new == 'SLT') $type_stock_erp_code_new = '1';
                        else if($type_stock_new == 'REGULER') $type_stock_erp_code_new = '2';
                        else if($type_stock_new == 'PR/SR' || $type_stock_new == 'PR') $type_stock_erp_code_new = '3';
                        else if($type_stock_new == 'MTFC') $type_stock_erp_code_new = '4';
                        else if($type_stock_new == 'NB') $type_stock_erp_code_new = '5';
                        else if($type_stock_new == 'ALLOWANCE') $type_stock_erp_code_new = '6';
                        else $type_stock_erp_code_new = null;
                        
                        if($material_stock_id)
                        {
                            if($reason)
                            {
                                if($type_stock_erp_code_new)
                                {
                                    $id                         = $material_stock_id; 
                                    $material_stock             = MaterialStock::find($id);
                                
                                    $new_type_stock             = $type_stock_new;
                                    $qty_change                 = $qty_change;

                                    $old_type_stock             = $material_stock->type_stock;
                                    $old_type_stock_erp_code    = $material_stock->type_stock_erp_code;
                                    $locator_id                 = $material_stock->locator_id;
                                    
                                    
                                    $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                                    ->where('material_stock_id',$id)
                                    ->exists();
                            
                                    if(!$has_outstanding_approval_exists)
                                    {
                                        if($old_type_stock != $new_type_stock)
                                        {
                                            $old_available          = sprintf('%0.8f',$material_stock->available_qty);
                                            $curr_available_qty     = sprintf('%0.8f',$material_stock->available_qty);
                                            $cur_reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);

                                            if ($qty_change/$curr_available_qty >= 1) $_supplied = $curr_available_qty;
                                            else $_supplied = $qty_change;

                                            $mapping_stock              = MappingStocks::where('type_stock',$new_type_stock)->first();
                                            $new_type_stock_erp_code    = ($mapping_stock)? $mapping_stock->type_stock_erp_code : null;

                                            if($_supplied > 0 && $curr_available_qty > 0)
                                            {
                                                $date_now = carbon::now();
                                                //insert new allocataion
                                                AllocationItem::FirstOrCreate([
                                                    'material_stock_id'         => $material_stock->id,
                                                    'c_order_id'                => $material_stock->c_order_id,
                                                    'document_no'               => $material_stock->document_no,
                                                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                                                    'supplier_code'             => $material_stock->supplier_code,
                                                    'supplier_name'             => $material_stock->supplier_name,
                                                    'item_id_source'            => $material_stock->item_id_source,
                                                    'item_id_book'              => $material_stock->item_id,
                                                    'item_code'                 => $material_stock->item_code,
                                                    'item_code_source'          => $material_stock->item_code,
                                                    'item_desc'                 => $material_stock->item_desc,
                                                    'category'                  => $material_stock->category,
                                                    'type_stock_material'       => $material_stock->type_stock,
                                                    'uom'                       => $material_stock->uom,
                                                    'warehouse'                 => auth::user()->warehouse,
                                                    'qty_booking'               => sprintf('%0.8f',$_supplied),
                                                    'is_not_allocation'         => true,
                                                    'is_from_allocation_buyer'  => false,
                                                    'mm_approval_date'          => carbon::now(),
                                                    'mm_user_id'                => auth::user()->id,
                                                    'accounting_approval_date'  => carbon::now(),
                                                    'accounting_user_id'        => auth::user()->id,
                                                    'user_id'                   => auth::user()->id,
                                                    'confirm_user_id'           => auth::user()->id,
                                                    'confirm_by_warehouse'      => 'approved',
                                                    'confirm_date'              => carbon::now(),
                                                    'created_at'                => $date_now,
                                                    'remark'                    => 'DIPINDAHKAN KE TIPE STOCK '.$new_type_stock.' '.$reason,
                                                ]);

                                            
                                                //insert new stock
                                                $is_exists = MaterialStock::whereNull('deleted_at')
                                                ->where([
                                                    [db::raw("upper(document_no)"),$material_stock->document_no],
                                                    [db::raw('upper(item_code)'),strtoupper($material_stock->item_code)],
                                                    ['type_po', 2],
                                                    ['uom',$material_stock->uom],
                                                    ['locator_id',$locator_id],
                                                    ['warehouse_id',$material_stock->warehouse_id],
                                                    ['is_material_others',$material_stock->is_material_others],
                                                    ['is_closing_balance',false],
                                                    ['type_stock',$new_type_stock],
                                                ])
                                                ->whereNotNull('approval_date');

                                                if($material_stock->po_detail_id) $is_exists = $is_exists->where('po_detail_id',$material_stock->po_detail_id);
                                                else $is_exists = $is_exists->whereNull('po_detail_id');

                                                if($material_stock->c_bpartner_id || $material_stock->c_bpartner_id != '') $is_exists = $is_exists->where('c_bpartner_id',$material_stock->c_bpartner_id);
                                                else $is_exists = $is_exists->whereNull('c_bpartner_id');

                                                if($material_stock->c_order_id || $material_stock->c_order_id != '') $is_exists = $is_exists->where('c_order_id',$material_stock->c_order_id);
                                                else $is_exists = $is_exists->whereNull('c_order_id');

                                                if($material_stock->po_buyer || $material_stock->po_buyer != '') $is_exists = $is_exists->where('po_buyer',$material_stock->po_buyer);
                                                else $is_exists = $is_exists->whereNull('po_buyer');

                                                if($material_stock->source || $material_stock->source != '') $is_exists = $is_exists->where('source',$material_stock->source);
                                                else $is_exists = $is_exists->whereNull('source');

                                                $is_exists = $is_exists->first();
                                                
                                                if(!$is_exists)
                                                {
                                                    $new_material_stock = MaterialStock::FirstOrCreate([
                                                        'po_detail_id'          => $material_stock->po_detail_id,
                                                        'document_no'           => $material_stock->document_no,
                                                        'supplier_code'         => $material_stock->supplier_code,
                                                        'supplier_name'         => $material_stock->supplier_name,
                                                        'c_bpartner_id'         => $material_stock->c_bpartner_id,
                                                        'c_order_id'            => $material_stock->c_order_id,
                                                        'locator_id'            => $locator_id,
                                                        'item_id'               => $material_stock->item_id,
                                                        'item_code'             => $material_stock->item_code,
                                                        'item_desc'             => $material_stock->item_desc,
                                                        'category'              => $material_stock->category,
                                                        'type_po'               => $material_stock->type_po,
                                                        'warehouse_id'          => $material_stock->warehouse_id,
                                                        'uom'                   => $material_stock->uom,
                                                        'po_buyer'              => $material_stock->po_buyer,
                                                        'qty_carton'            => $material_stock->qty_carton,
                                                        'stock'                 => sprintf('%0.8f',$_supplied),
                                                        'reserved_qty'          => 0,
                                                        'available_qty'         => sprintf('%0.8f',$_supplied),
                                                        'is_active'             =>  $material_stock->is_active,
                                                        'is_material_others'    => $material_stock->is_material_others,
                                                        'upc_item'              => $material_stock->upc_item,
                                                        'source'                => $material_stock->source,
                                                        'type_stock_erp_code'   => $new_type_stock_erp_code,
                                                        'type_stock'            => $new_type_stock,
                                                        'source'                => $material_stock->source,
                                                        'user_id'               => $material_stock->user_id,
                                                        'uom_source'            => $material_stock->uom_source,
                                                        'created_at'            => $upload_date,
                                                        'updated_at'            => $upload_date,
                                                    ]);

                                                    $new_material_stock_id      = $new_material_stock->id;
                                                    $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                                    $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock.' '.$reason;
                                                
                                                    HistoryStock::approved($new_material_stock_id
                                                    ,$_supplied
                                                    ,'0'
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,$remark_mutasi
                                                    ,auth::user()->name
                                                    ,auth::user()->id
                                                    ,$new_type_stock_erp_code
                                                    ,$new_type_stock
                                                    ,false
                                                    ,true);
                                                    
                                                    DetailMaterialStock::FirstOrCreate([
                                                        'material_stock_id'       => $new_material_stock->id,
                                                        'remark'                  => $remark_mutasi,
                                                        'uom'                     => $new_material_stock->uom,
                                                        'qty'                     => $_supplied,
                                                        'locator_id'              => $material_stock->locator_id,
                                                        'user_id'                 => Auth::user()->id,
                                                        'created_at'              => $upload_date,
                                                        'updated_at'              => $upload_date,
                                                        'approve_date_mm'         => carbon::now(),
                                                        'mm_user_id'              => auth::user()->id,
                                                        'approve_date_accounting' => carbon::now(),
                                                        'accounting_user_id'      => auth::user()->id,
                                                    ]);

                                                }else
                                                {
                                                    $new_material_stock_id      = $is_exists->id;
                                                    $new_stock                  = sprintf('%0.8f',$is_exists->stock);
                                                    $old_available_qty_curr     = sprintf('%0.8f',$is_exists->available_qty);
                                                    $new_reserved_qty           = sprintf('%0.8f',$is_exists->reserved_qty);
                                                    $_new_stock                 = sprintf('%0.8f',$new_stock + $_supplied);
                                                    $new_availabilty_qty        = sprintf('%0.8f',$_new_stock - $new_reserved_qty);

                                                    $is_exists->stock           = sprintf('%0.8f',$_new_stock);
                                                    $is_exists->available_qty   = sprintf('%0.8f',$new_availabilty_qty);
                                                    
                                                
                                                    if($new_availabilty_qty > 0)
                                                    {
                                                        $is_exists->is_allocated    = false;
                                                        $is_exists->is_active       = true;
                                                    }

                                                    $_old_stock                 = ($old_type_stock)? $old_type_stock : '(-)';
                                                    $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                                    $remark_mutasi              = 'DIPINDAHKAN DARI DARI TIPE '.$_old_stock.' KE TIPE '.$_new_type_stock.' '.$reason;
                                                    
                                                    HistoryStock::approved($new_material_stock_id
                                                    ,$new_availabilty_qty
                                                    ,$old_available_qty_curr
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,null
                                                    ,$remark_mutasi
                                                    ,auth::user()->name
                                                    ,auth::user()->id
                                                    ,$new_type_stock_erp_code
                                                    ,$new_type_stock
                                                    ,false
                                                    ,true);
                                                    
                                                    $is_exists->save();

                                                    DetailMaterialStock::FirstOrCreate([
                                                        'material_stock_id'       => $is_exists->id,
                                                        'remark'                  => $remark_mutasi,
                                                        'uom'                     => $is_exists->uom,
                                                        'qty'                     => $_supplied,
                                                        'locator_id'              => $is_exists->locator_id,
                                                        'user_id'                 => Auth::user()->id,
                                                        'created_at'              => $upload_date,
                                                        'updated_at'              => $upload_date,
                                                        'approve_date_mm'         => carbon::now(),
                                                        'mm_user_id'              => auth::user()->id,
                                                        'approve_date_accounting' => carbon::now(),
                                                        'accounting_user_id'      => auth::user()->id,
                                                    ]);
                                                }

                                                $curr_available_qty -= $_supplied;
                                                
                                                $new = $old_available - $_supplied;
                                                if($new <= 0) $new = '0';
                                                else $new = $new;

                                                $_new_type_stock            = ($new_type_stock)? $new_type_stock : '(-)';
                                                $remark_mutasi              = 'DI PINDAHKAN KE TIPE '. $_new_type_stock.' '.$reason;
                                                
                                                HistoryStock::approved($id
                                                ,$new
                                                ,$old_available
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,null
                                                ,$remark_mutasi
                                                ,auth::user()->name
                                                ,auth::user()->id
                                                ,$old_type_stock_erp_code
                                                ,$old_type_stock
                                                ,false
                                                ,true);
                                                
                                                $material_stock->reserved_qty   = sprintf('%0.8f',$cur_reserved_qty + $_supplied);
                                                $material_stock->available_qty  = sprintf('%0.8f',$curr_available_qty);
                                                if($curr_available_qty <= 0)
                                                {
                                                    $material_stock->is_allocated   = true;
                                                    $material_stock->is_active      = false;
                                                }

                                                $material_stock->save();
                                                
                                            }

                                            $locator = Locator::find($locator_id);
                                            if($locator)
                                            {
                                                $counter_locator = MaterialStock::where([
                                                    ['locator_id',$locator_id],
                                                    ['is_allocated',false],
                                                    ['is_closing_balance',false],
                                                ])
                                                ->whereNull('deleted_at')
                                                ->count();

                                                $locator->counter_in = $counter_locator ;
                                                $locator->save();
                                            }
                                            

                                        }

                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = $material_stock->supplier_name;
                                        $obj->document_no       = $material_stock->document_no;
                                        $obj->item_code         = $material_stock->item_code;
                                        $obj->uom               = $material_stock->uom;
                                        $obj->qty_available     = $qty_change;
                                        $obj->warehouse_name    = $material_stock->warehouse_id;
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = false;
                                        $obj->remark            = 'success';
                                        $array [] = $obj;
                                    }else
                                    {
                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = null;
                                        $obj->document_no       = null;
                                        $obj->item_code         = null;
                                        $obj->uom               = null;
                                        $obj->qty_available     = null;
                                        $obj->warehouse_name    = null;
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = true;
                                        $obj->remark            = 'ERROR, ID ADA OUTSTANDING APPROVAL STOCK, SILAHKAN APPROVE TERLEBIH DAHULU';
                                        $array [] = $obj;
                                    }
                                }else
                                {
                                    $obj                    = new stdClass();
                                    $obj->supplier_name     = null;
                                    $obj->document_no       = null;
                                    $obj->item_code         = null;
                                    $obj->uom               = null;
                                    $obj->qty_available     = null;
                                    $obj->warehouse_name    = null;
                                    $obj->reason            = $reason;
                                    $obj->error_upload      = true;
                                    $obj->remark            = 'ERROR, TYPE STOCK NOT FOUND';
                                    $array [] = $obj;
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, SILAHKAN MASUKAN ALASANNYA';
                                $array [] = $obj;
                            }
                            
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }

                DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                
            }
        }

        return response()->json($array,'200');
    }

    public function editBulk()
    {
        return view('accessories_report_material_stock.edit_bulk');
    }

    public function exportFormImportEditBulk()
    {
        return Excel::create('edit_material_stock',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','NEW_AVAILABLE_QTY_STOCK');
                $sheet->setCellValue('C1','UPDATE_REASON');
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                ));
            });
            $excel->setActiveSheetIndex(0);
        })
        ->export('xlsx');
    }

    public function storeEditBulk(request $request)
    {
        $array = array();
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $path = $request->file('upload_file')->getRealPath();
            $data = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                try 
                {
                    DB::beginTransaction();
                    $upload_date                = Carbon::now()->toDateTimeString();
                    foreach ($data as $key => $value) 
                    {
                        $material_stock_id       = $value->material_stock_id;
                        $new_available_qty_stock = strtoupper($value->new_available_qty_stock);
                        $reason                  = strtoupper($value->update_reason);
                        
                        if($material_stock_id)
                        {
                            
                            $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                            ->where('material_stock_id',$material_stock_id)
                            ->exists();
                    
                            if(!$has_outstanding_approval_exists)
                            {
                                if($reason)
                                {
                                    $material_stock             = MaterialStock::find($material_stock_id);
                                    $curr_type_stock            = $material_stock->type_stock;
                                    $curr_type_stock_erp_code   = $material_stock->type_stock_erp_code;
                                    $curr_available_qty         = sprintf('%0.8f',$material_stock->available_qty);
                                    $locator_id                 = $material_stock->locator_id;
                                    $operator                   = $new_available_qty_stock - $curr_available_qty;
                                    $remark                     = trim(strtoupper($request->update_reason));
                                    
                                    if($operator > 0)
                                    {
                                        $new_stock = ($material_stock->stock + $operator);
                                        $material_stock->stock = sprintf('%0.8f',$new_stock);
    
                                        DetailMaterialStock::FirstOrCreate([
                                            'material_stock_id'     => $material_stock->id,
                                            'remark'                => $remark,
                                            'uom'                   => $material_stock->uom,
                                            'qty'                   => $operator,
                                            'locator_id'            => $material_stock->locator_id,
                                            'user_id'               => Auth::user()->id,
                                            'created_at'            => $upload_date,
                                            'updated_at'            => $upload_date,
                                        ]);
                                    }else
                                    {
                                        $new_reserved_qty = $material_stock->reserved_qty + (-1*$operator);
                                        AllocationItem::FirstOrCreate([
                                            'material_stock_id'         => $material_stock->id,
                                            'c_order_id'                => $material_stock->c_order_id,
                                            'document_no'               => $material_stock->document_no,
                                            'c_bpartner_id'             => $material_stock->c_bpartner_id,
                                            'supplier_code'             => $material_stock->supplier_code,
                                            'supplier_name'             => $material_stock->supplier_name,
                                            'item_id_source'            => $material_stock->item_id_source,
                                            'item_id_book'              => $material_stock->item_id,
                                            'item_code'                 => $material_stock->item_code,
                                            'item_code_source'          => $material_stock->item_code,
                                            'item_desc'                 => $material_stock->item_desc,
                                            'type_stock_material'       => $material_stock->type_stock,
                                            'category'                  => $material_stock->category,
                                            'uom'                       => $material_stock->uom,
                                            'warehouse'                 => auth::user()->warehouse,
                                            'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                                            'is_not_allocation'         => true,
                                            'user_id'                   => auth::user()->id,
                                            'confirm_user_id'           => auth::user()->id,
                                            'confirm_by_warehouse'      => 'approved',
                                            'confirm_date'              => carbon::now(),
                                            'remark'                    => $remark,
                                        ]);
    
                                        $material_stock->reserved_qty   = sprintf('%0.8f',$new_reserved_qty);
                                        
                                    }
    
                                    $material_stock->available_qty      = sprintf('%0.8f',$new_available_qty_stock);
                                    if($new_available_qty_stock <= 0)
                                    {
                                        $material_stock->is_allocated   = true;
                                        $material_stock->is_active      = false;
                                    }else
                                    {
                                        $material_stock->is_allocated   = false;
                                        $material_stock->is_active      = true;
                                    }
    
                                    HistoryStock::approved($material_stock_id
                                    ,$new_available_qty_stock
                                    ,$curr_available_qty
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,null
                                    ,auth::user()->name
                                    ,auth::user()->id
                                    ,$curr_type_stock_erp_code
                                    ,$curr_type_stock
                                    ,false
                                    ,false);
                                                
                                    $count_item_on_locator = MaterialStock::where([
                                        ['locator_id',$locator_id],
                                        ['is_allocated',false],
                                        ['is_closing_balance',false],
                                    ])
                                    ->whereNull('deleted_at')
                                    ->count();
    
                                    $locator = Locator::find($locator_id);
                                    if($locator)
                                    {
                                        $locator->counter_in = $count_item_on_locator;
                                        $locator->save();
                                    }
    
                                    $material_stock->save();
                                
                                        $obj                    = new stdClass();
                                        $obj->supplier_name     = $material_stock->supplier_name;
                                        $obj->document_no       = $material_stock->document_no;
                                        $obj->item_code         = $material_stock->item_code;
                                        $obj->uom               = $material_stock->uom;
                                        $obj->qty_available     = $curr_available_qty;
                                        $obj->warehouse_name    = ($material_stock->warehouse_id == '1000013') ? 'WAREHOUSE ACC AOI 2': 'WAREHOUSE ACC AOI 1';
                                        $obj->reason            = $reason;
                                        $obj->error_upload      = false;
                                        $obj->remark            = 'DATA BERHASIL DI EDIT';
                                    $array [] = $obj;
                                }else
                                {
                                    $obj                    = new stdClass();
                                    $obj->supplier_name     = null;
                                    $obj->document_no       = null;
                                    $obj->item_code         = null;
                                    $obj->uom               = null;
                                    $obj->qty_available     = null;
                                    $obj->warehouse_name    = null;
                                    $obj->reason            = $reason;
                                    $obj->error_upload      = true;
                                    $obj->remark            = 'ERROR, SILAHKAN ISI ALASANNYA TERLEBIH DAHULU';
                                    $array [] = $obj;
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_name     = null;
                                $obj->document_no       = null;
                                $obj->item_code         = null;
                                $obj->uom               = null;
                                $obj->qty_available     = null;
                                $obj->warehouse_name    = null;
                                $obj->reason            = $reason;
                                $obj->error_upload      = true;
                                $obj->remark            = 'ERROR, ID ADA OUTSTANDING APPROVAL STOCK, SILAHKAN APPROVE TERLEBIH DAHULU';
                                $array [] = $obj;
                            }
                        }else
                        {
                            $obj                    = new stdClass();
                            $obj->supplier_name     = null;
                            $obj->document_no       = null;
                            $obj->item_code         = null;
                            $obj->uom               = null;
                            $obj->qty_available     = null;
                            $obj->warehouse_name    = null;
                            $obj->reason            = $reason;
                            $obj->error_upload      = true;
                            $obj->remark            = 'ERROR, ID TIDAK TIDAK DITEMUKAN';
                            $array [] = $obj;
                        }
                        
                    }
                
                    DB::commit();
                }catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
                    
            }
        }

        return response()->json($array,'200');
    }

    public function allocatingStock($id)
    {
        $confirm_date                           = Carbon::now()->todatetimestring();
       
        //return view('errors.503');
        $material_stock_id      = $id;
        $material_stock         = MaterialStock::where([
            ['id',$material_stock_id],
            ['is_allocated',false],
            ['is_closing_balance',false],
            ['is_stock_on_the_fly',false],
        ])
        //->whereNull('last_status')
        ->whereNotNull('approval_date')
        ->whereNull('deleted_at')
        ->first();

        $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
        ->where('material_stock_id',$material_stock_id)
        ->exists();

        if($has_outstanding_approval_exists)
        {
            return response()->json('This data has outstanding approval stock, please approved it first',422);
        }

        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        if($material_stock)
        {
            if($material_stock->last_status)
            {
                return response()->json('This stock is locked',422);
            }

            $this->doAllocation($material_stock,auth::user()->id,$confirm_date);
        }
        

        return response()->json(200);
    }

    public function cancelAllocation($id)
    {
        $material_stock_id  = $id;
        $_material_stock    = MaterialStock::find($material_stock_id);
        if(!auth::user()->hasRole(['admin-ict-acc']))
        {
            $warehouse_user = auth::user()->warehouse;
            if($warehouse_user != $_material_stock->warehouse_id)
            {
                return response()->json('You dont have permission to update this data',422);
            }

        }

        try 
        {
            DB::beginTransaction();

            $material_preparations   = MaterialPreparation::where('material_stock_id',$material_stock_id)
            ->whereNotNull('auto_allocation_id')
            ->get();

            $movement_date = carbon::now()->toDateTimeString();

            foreach ($material_preparations as $key => $material_preparation) 
            {
                Temporary::Create([
                    'barcode'       => $material_preparation->po_buyer,
                    'status'        => 'mrp',
                    'user_id'       => Auth::user()->id,
                    'created_at'    => $movement_date,
                    'updated_at'    => $movement_date,
                    
                ]);

                $auto_allocation_id                 = $material_preparation->auto_allocation_id;
                $material_preparation_id            = $material_preparation->id;
                $last_status_movement               = $material_preparation->last_status_movement;

                if($last_status_movement !='reroute'
                    && $last_status_movement !='out'
                    && $last_status_movement !='out-subcont'
                    && $last_status_movement !='cancel order'
                    && $last_status_movement !='cancel item'
                    && $last_status_movement !='out switch'
                    && $last_status_movement !='hold'
                )
                {
                    $qty_booking                        = sprintf('%0.8f',$material_preparation->qty_conversion);
                    $po_buyer                           = $material_preparation->po_buyer;
                    $style                              = $material_preparation->style;
                    $article_no                         = $material_preparation->article_no;
                    $lc_date                            = $material_preparation->lc_date;
                    
                    $auto_allocation                    = AutoAllocation::find($auto_allocation_id);
                    $qty_allocated                      = sprintf('%0.8f',$auto_allocation->qty_allocated);
                    $qty_outstanding                    = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                    
                    $new_qty_outstanding                = sprintf('%0.8f',$qty_outstanding + $qty_booking);
                    $new_qty_allocated                  = sprintf('%0.8f',$qty_allocated - $qty_booking);

                    $auto_allocation->qty_outstanding   = $new_qty_outstanding;
                    $auto_allocation->qty_allocated     = $new_qty_allocated;
                    
                    if($new_qty_allocated <=0 )
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }

                    $material_stock     = MaterialStock::find($material_stock_id);
                    $stock              = sprintf('%0.8f',$material_stock->stock);
                    $reserved_qty       = sprintf('%0.8f',$material_stock->reserved_qty);
                    $new_reserverd_qty  = sprintf('%0.8f',$reserved_qty - $qty_booking);
                    $new_available_qty  = sprintf('%0.8f',$stock - $new_reserverd_qty);
                    $old_available_qty  = sprintf('%0.8f',$material_stock->available_qty);
                    $_new_available_qty = sprintf('%0.8f',$old_available_qty + $material_preparation->qty_conversion);

                    if($material_stock->is_closing_balance == false)
                    {
                        $material_stock->reserved_qty   = $new_reserverd_qty;
                        $material_stock->available_qty  = $new_available_qty;

                        if($new_available_qty > 0)
                        {
                            $material_stock->is_allocated = false;
                            $material_stock->is_active = true;
                        }

                        HistoryStock::approved($material_stock_id
                        ,$_new_available_qty
                        ,$old_available_qty
                        ,(-1*$material_preparation->qty_conversion)
                        ,$po_buyer
                        ,$style
                        ,$article_no
                        ,$lc_date
                        ,'CANCEL ALLOCATION'
                        ,auth::user()->name
                        ,auth::user()->id
                        ,$material_stock->type_stock_erp_code
                        ,$material_stock->type_stock
                        ,false
                        ,true);
                            
                        $material_stock->save();
                    }

                    $auto_allocation->save();
                }
                
                
                $material_preparation->delete();
             }
            
            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }

        return response()->json(200);
    }

    public function changeSourceStock($id)
    {
        $material_stock         = MaterialStock::find($id);
        $obj                    = new stdClass();
        $obj->id                = $id;
        $obj->supplier_name     = $material_stock->supplier_name;
        $obj->document_no       = $material_stock->document_no;
        $obj->item_code         = $material_stock->item_code;
        $obj->po_buyer          = $material_stock->po_buyer;
        $obj->stock             = $material_stock->stock;
        $obj->available_qty     = $material_stock->available_qty;
        $obj->uom               = $material_stock->uom;
        
        return response()->json($obj,200);
    }

    public function storeChangeSourceStock(request $request)
    {
        try 
        {
            DB::beginTransaction();

            $id                               = $request->id;
            $reason                           = strtoupper(trim($request->change_source_stock_reason));
            $actual_stock                     = sprintf('%0.8f',$request->change_source_stock_actual);
            
            $material_stock                   = MaterialStock::find($id);
            $available_qty                    = sprintf('%0.8f',$material_stock->available_qty);
            
            $material_stock->source           = 'NON RESERVED STOCK';
            $material_stock->is_running_stock = false;
            $material_stock->save();
            
            $system                     = User::where([
                ['name','system'],
                ['warehouse',$material_stock->warehouse_id]
            ])
            ->first();

            if($available_qty != $actual_stock)
            {
                $operator                         = sprintf('%0.8f',$actual_stock - $available_qty);
            
                HistoryStock::approved($material_stock->id
                    ,$actual_stock
                    ,'0'
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,'PEMINDAHAN DARI RESERVED STOCK KE NON RESERVED STOCK'
                    ,auth::user()->name
                    ,auth::user()->id
                    ,$material_stock->type_stock_erp_code
                    ,$material_stock->type_stock
                    ,false
                    ,true
                );

                AllocationItem::Create([
                    'material_stock_id'         => $material_stock->id,
                    'c_order_id'                => $material_stock->c_order_id,
                    'document_no'               => $material_stock->document_no,
                    'c_bpartner_id'             => $material_stock->c_bpartner_id,
                    'supplier_code'             => $material_stock->supplier_code,
                    'supplier_name'             => $material_stock->supplier_name,
                    'item_id_source'            => $material_stock->item_id,
                    'item_id_book'              => $material_stock->item_id,
                    'item_code'                 => $material_stock->item_code,
                    'item_code_source'          => $material_stock->item_code,
                    'item_desc'                 => $material_stock->item_desc,
                    'category'                  => $material_stock->category,
                    'uom'                       => $material_stock->uom,
                    'warehouse'                 => $material_stock->warehouse_id,
                    'type_stock_material'       => $material_stock->type_stock,
                    'qty_booking'               => sprintf('%0.8f',(-1*$operator)),
                    'is_not_allocation'         => true,
                    'is_from_allocation_buyer'  => false,
                    'user_id'                   => auth::user()->id,
                    'confirm_user_id'           => auth::user()->id,
                    'confirm_by_warehouse'      => 'approved',
                    'confirm_date'              => carbon::now(),
                    'mm_approval_date'          => carbon::now(),
                    'accounting_approval_date'  => carbon::now(),
                    'mm_user_id'                => $system->id,
                    'accounting_user_id'        => $system->id,
                    'remark'                    => $reason,
                ]);

                $new_reserved_qty               = $material_stock->reserved_qty + (-1*$operator);
                $material_stock->reserved_qty   = $new_reserved_qty;
                $material_stock->available_qty  = $actual_stock;
            }else
            {
                HistoryStock::approved($material_stock->id
                    ,$material_stock->available_qty
                    ,'0'
                    ,null
                    ,null
                    ,null
                    ,null
                    ,null
                    ,'PEMINDAHAN DARI RESERVED STOCK KE NON RESERVED STOCK'
                    ,auth::user()->name
                    ,auth::user()->id
                    ,$material_stock->type_stock_erp_code
                    ,$material_stock->type_stock
                    ,false
                    ,true)
                ;
            }
            
            $material_stock->save();

            DB::commit();
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function doRecalculate($id)
    {
        try
        {
            DB::beginTransaction();
            //return view('errors.503');
            $movement_date          = carbon::now();
            $material_stock         = MaterialStock::find($id);
            $_stock                 = sprintf('%0.8f',$material_stock->stock);
            $type_stock             = $material_stock->type_stock;
            $type_stock_erp_code    = $material_stock->type_stock_erp_code;
            $po_buyer               = $material_stock->po_buyer;
            $warehouse_id           = $material_stock->warehouse_id;
            $po_detail_id           = $material_stock->po_detail_id;
            $remark                 = $material_stock->remark;
            $remark                 = $material_stock->remark;
            $old_available_qty      = sprintf('%0.8f',$material_stock->available_qty);
            $total_detail_stock     = sprintf('%0.8f',DetailMaterialStock::where('material_stock_id',$id)
            ->whereNotNull('approve_date_mm')
            ->whereNotNull('approve_date_accounting')
            ->sum('qty'));

            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
            {
                $c_order_id             = 'FREE STOCK';
                $no_packing_list        = '-';
                $no_invoice             = '-';
                $c_orderline_id         = '-';
            }else
            {
                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
            }

            $inventory_erp = Locator::with('area')
            ->where([
                ['is_active',true],
                ['rack','ERP INVENTORY'],
            ])
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','ERP INVENTORY');
            })
            ->first();

            if($total_detail_stock != $_stock) $stock = $total_detail_stock;
            else $stock = $_stock;

            $total_allocation_item  = AllocationItem::where('material_stock_id',$id)
            ->whereNull('deleted_at')
            ->where(function($query){
                $query->where('confirm_by_warehouse','approved')
                ->orWhereNull('confirm_by_warehouse');
            })
            ->whereNotNull('mm_approval_date')
            ->whereNotNull('accounting_approval_date')
            ->sum('qty_booking');

            $total_preparation_item  = MaterialPreparation::where([
                ['material_stock_id',$id],
                ['is_reroute',false],
                //['warehouse',$warehouse_id],
            ])
            ->sum(db::raw("(qty_conversion + COALESCE(adjustment,0) + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

            $new_available_qty = sprintf('%0.8f',$stock - ($total_allocation_item + $total_preparation_item ));

            if($new_available_qty <=0)
            {
                $material_stock->is_allocated   = true;
                $material_stock->is_active      = false;
            }else
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }

            $locator_id = $material_stock->locator_id;

            if($locator_id)
            {
                $count_item_on_locator = MaterialStock::where([
                    ['locator_id',$locator_id],
                    ['is_allocated',false],
                    ['is_closing_balance',false],
                ])
                ->whereNull('deleted_at')
                ->whereNotNull('approval_date')
                ->count();

                $locator = Locator::find($locator_id);
                if($locator)
                {
                    $locator->counter_in = $count_item_on_locator ;
                    $locator->save();
                }
            }

            if ($new_available_qty == 0) $new_available_qty = '0';
            else $new_available_qty = sprintf('%0.8f',$new_available_qty);

            if($new_available_qty != $old_available_qty)
            {
                if(Auth::check())
                {
                    $name = auth::user()->name;
                    $user_id = auth::user()->id;
                }
                else
                {
                    $name = 'system';
                    $user_id = '92';
                }
                $operator = sprintf('%0.8f',$new_available_qty - $old_available_qty);
                
                HistoryStock::approved($id
                ,$new_available_qty
                ,$old_available_qty
                ,null
                ,null
                ,null
                ,null
                ,null
                ,'RECALCULATE STOCK'
                ,$name
                ,$user_id
                ,$type_stock_erp_code
                ,$type_stock
                ,false
                ,false);
                
                // masukin intergasi buat adj erp
                $is_movement_integration_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['po_buyer',$po_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();
                
                if(!$is_movement_integration_exists)
                {
                    $movement_integration = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'po_buyer'              => $po_buyer,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $movement_date,
                        'updated_at'            => $movement_date,
                    ]);

                    $movement_integration_id = $movement_integration->id;
                }else
                {
                    $is_movement_integration_exists->updated_at = $movement_date;
                    $is_movement_integration_exists->save();

                    $movement_integration_id = $is_movement_integration_exists->id;
                }

                $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_stock_id')
                ->where([
                    ['material_movement_id',$movement_integration_id],
                    ['material_stock_id',$id],
                    ['qty_movement',sprintf('%0.8f',$operator)],
                    ['date_movement',$movement_date],
                    ['is_integrate',false],
                    ['is_active',true],
                ]) 
                ->exists();

                if(Auth::check())
                {
                    $user_id= auth::user()->id;
                } 
                else
                {
                    $user_id = '92';
                }

                if(!$is_material_movement_line_integration_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $movement_integration_id,
                        'material_stock_id'             => $id,
                        'item_code'                     => $material_stock->item_code,
                        'item_id'                       => $material_stock->item_id,
                        'c_order_id'                    => $material_stock->c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $material_stock->c_bpartner_id,
                        'supplier_name'                 => $material_stock->supplier_name,
                        'type_po'                       => 2,
                        'is_active'                     => true,
                        'is_integrate'                  => false,
                        'uom_movement'                  => $material_stock->uom,
                        'qty_movement'                  => sprintf('%0.8f',$operator),
                        'date_movement'                 => $movement_date,
                        'created_at'                    => $movement_date,
                        'updated_at'                    => $movement_date,
                        'date_receive_on_destination'   => $movement_date,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $material_stock->warehouse_id,
                        'document_no'                   => $material_stock->document_no,
                        'note'                          => 'RECALCULATE STOCK',
                        'is_active'                     => true,
                        'user_id'                       => $user_id,
                    ]);
                }
                
            
            } 

            $material_stock->stock          = sprintf('%0.8f',$stock);
            $material_stock->reserved_qty   = sprintf('%0.8f',($total_allocation_item + $total_preparation_item ));
            $material_stock->available_qty  = sprintf('%0.8f',$new_available_qty);
            $material_stock->save();

            DB::commit();

        } catch (Exception $e)
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }

    static function doAllocation($material_stock,$created_by,$confirm_date)
    {

        // dd($material_stock);
        $concatenate                            = '';
        $recalculate_stock                      = array();
        $recalculate                            = array();
        $insert_cancel_order                    = array();
        $insert_cancel_item                     = array();
       
        $allocation_stock                       = Locator::with('area')
        ->whereHas('area',function ($query) use ($material_stock)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$material_stock->warehouse_id);
            $query->where('name','FREE STOCK');
        })
        ->where('rack','ALLOCATION STOCK')
        ->first();
    
        $receiving_location = Locator::with('area')
        ->whereHas('area',function ($query) use ($material_stock)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$material_stock->warehouse_id);
            $query->where('name','RECEIVING');
        })
        ->first();
    
        $supplier_location = Locator::with('area')
        ->whereHas('area',function ($query) use ($material_stock)
        {
            $query->where('is_destination',false);
            $query->where('is_active',true);
            $query->where('warehouse',$material_stock->warehouse_id);
            $query->where('name','SUPPLIER');
        })
        ->first();

        $reroute_destination = Locator::with('area') 
        ->whereHas('area',function ($query) use ($material_stock)
        {
            $query->where([
                ['warehouse',$material_stock->warehouse_id],
                ['name','REROUTE'],
                ['is_destination',true]
            ]);

        })
        ->first();

        $cancel_order_destination = Locator::with('area')
        ->whereHas('area',function ($query) use ($material_stock)
        {
            $query->where([
                ['warehouse',$material_stock->warehouse_id],
                ['name','CANCEL ORDER'],
                ['is_destination',true]
            ]);

        })
        ->first();
        // dd("1");
        $material_stock->last_status    = 'prepared';
        $material_stock->save();
        DB::commit();

        $material_stock_id              = $material_stock->id;
        $type_stock                     = strtoupper($material_stock->type_stock);
        $type_stock_erp_code            = $material_stock->type_stock_erp_code;
        $document_no                    = strtoupper($material_stock->document_no);
        $item_code                      = strtoupper($material_stock->item_code);
        $category                       = $material_stock->category;
        $po_buyer_source                = $material_stock->po_buyer;
        $c_order_id                     = $material_stock->c_order_id;
        $item_id                        = $material_stock->item_id;
        $po_detail_id                   = $material_stock->po_detail_id;
        $uom_source                     = ($material_stock->uom_source ? $material_stock->uom_source : $material_stock->uom);
        $supplier_name                  = strtoupper($material_stock->supplier_name);
        $c_bpartner_id                  = $material_stock->c_bpartner_id;
        $_supplier                      = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
        $supplier_code                  = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
        $warehouse_id                   = $material_stock->warehouse_id;
        $is_running_stock               = $material_stock->is_running_stock;
        $material_subcont_id            = $material_stock->remark;
        $reserved_qty                   = sprintf('%0.8f',$material_stock->reserved_qty);
        $available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
        $is_bom_exists                  = 0;

        // dd($available_qty);
        $system                     = User::where([
            ['name','system'],
            ['warehouse',$warehouse_id]
        ])
        ->first();

        $conversion = UomConversion::where([
            ['item_id',$item_id],
            ['uom_from',$uom_source]
        ])
        ->first();
    
        if($conversion)
        {
            $multiplyrate   = $conversion->multiplyrate;
            $dividerate     = $conversion->dividerate;
        }else
        {
            $dividerate     = 1;
            $multiplyrate   = 1;
        }
        // dd($material_subcont_id);

        if($material_subcont_id)
        {
            $allocation_source      = 'PINDAH TANGAN';
            $receiving_location = Locator::with('area')
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','RECEIVING');
            })
            ->first();

            $handover_location = Locator::with('area')
            ->whereHas('area',function ($query) use($warehouse_id)
            {
                $query->where('is_destination',false);
                $query->where('is_active',true);
                $query->where('warehouse',$warehouse_id);
                $query->where('name','HANDOVER');
            })
            ->first();

            // dd($available_qty);

            if($available_qty > 0 )
            {
                MaterialArrival::whereNotNull('material_subcont_id')
                ->where([
                    ['po_detail_id',$po_detail_id],
                    ['warehouse_id',$warehouse_id],
                    ['material_subcont_id',$material_subcont_id],
                ])
                ->whereNotIn('id',function ($query)
                {
                    $query->select('material_arrival_id')
                    ->from('detail_material_preparations')
                    ->whereNotNull('material_arrival_id')
                    ->groupby('material_arrival_id');
                })
                ->update([
                    'preparation_date' => null
                ]);
            }
            

            $material_arrivals   = MaterialArrival::whereNotNull('material_subcont_id')
            ->where([
                ['po_detail_id',$po_detail_id],
                ['warehouse_id',$warehouse_id],
                ['material_subcont_id',$material_subcont_id],
            ])
            ->whereNotIn('id',function ($query)
            {
                $query->select('material_arrival_id')
                ->from('detail_material_preparations')
                ->whereNotNull('material_arrival_id')
                ->groupby('material_arrival_id');
            })
            ->whereNull('preparation_date')
            ->get();

            // dd($material_arrivals);

            $system                     = User::where([
                ['name','system'],
                ['warehouse',$warehouse_id]
            ])
            ->first();

            $movement_date = $confirm_date;
            foreach ($material_arrivals as $key => $material_arrival) 
            {
                try 
                {
                    DB::beginTransaction();

                    $material_subcont_id = $material_arrival->material_subcont_id;
                    $material_arrival_id = $material_arrival->id;
                    $user_id            = $material_arrival->user_id;
                    $username           = $material_arrival->user->name;

                    $copy_preparation   = MaterialPreparation::whereIn('id',function($query) use($material_subcont_id) 
                    {
                        $query->select('material_preparation_id')
                        ->from('material_movement_lines')
                        ->whereIn('id',function($query2) use ($material_subcont_id)
                        {
                            $query2->select('material_movement_line_id')
                            ->from('material_subconts')
                            ->where('id',$material_subcont_id);
                        })
                        ->groupby('material_preparation_id');
                    })
                    ->first();


                    $is_exists = MaterialPreparation::where([
                        'po_detail_id'          => $copy_preparation->po_detail_id,
                        'item_id'               => $copy_preparation->item_id,
                        'c_bpartner_id'         => $copy_preparation->c_bpartner_id,
                        'c_order_id'            => $copy_preparation->c_order_id,
                        'barcode'               => $copy_preparation->barcode,
                        'document_no'           => $copy_preparation->document_no,
                        'po_buyer'              => $copy_preparation->po_buyer,
                        'qty_conversion'        => $copy_preparation->qty_conversion,
                        'style'                 => $copy_preparation->style,
                        'warehouse'             => $warehouse_id,
                        'item_code'             => $copy_preparation->item_code,
                        'type_po'               => 2,
                    ])
                    ->first();

                    $so_id          = PoBuyer::select('so_id')->where('po_buyer', $copy_preparation->po_buyer)->first();

                    if(!$is_exists)
                    {
                        $master_po_buyer        = PoBuyer::where('po_buyer',$copy_preparation->po_buyer)->first();
                        $lc_date                = ($master_po_buyer ? $master_po_buyer->lc_date : null);
                        
                        $material_preparation   = MaterialPreparation::FirstOrCreate([
                            'material_stock_id'     => $material_stock_id,
                            'auto_allocation_id'    => $copy_preparation->auto_allocation_id,
                            'item_id'               => $copy_preparation->item_id,
                            'c_order_id'            => $copy_preparation->c_order_id,
                            'c_bpartner_id'         => $copy_preparation->c_bpartner_id,
                            'supplier_name'         => $copy_preparation->supplier_name,
                            'barcode'               => $copy_preparation->barcode,
                            'po_detail_id'          => $copy_preparation->po_detail_id,
                            'document_no'           => $copy_preparation->document_no,
                            'po_buyer'              => $copy_preparation->po_buyer,
                            'is_moq'                => $copy_preparation->is_moq,
                            'is_backlog'            => $copy_preparation->is_backlog,
                            'uom_conversion'        => $copy_preparation->uom_conversion,
                            'qty_conversion'        => $copy_preparation->qty_conversion,
                            'qty_reconversion'      => $copy_preparation->qty_reconversion,
                            'job_order'             => $copy_preparation->job_order,
                            'style'                 => $copy_preparation->style,
                            'article_no'            => $copy_preparation->article_no,
                            'warehouse'             => $warehouse_id,
                            'item_code'             => $copy_preparation->item_code,
                            'item_desc'             => $copy_preparation->item_desc,
                            'category'              => $copy_preparation->category,
                            'qc_status'             => $copy_preparation->qc_status,
                            'total_carton'          => $copy_preparation->total_carton,
                            'is_allocation'         => $copy_preparation->is_allocation,
                            '_style'                => $copy_preparation->_style,
                            'referral_code'         => $copy_preparation->referral_code,
                            'sequence'              => $copy_preparation->sequence,
                            'first_print_date'      => $copy_preparation->first_print_date,
                            'type_po'               => 2,
                            'is_from_handover'      => true,
                            'is_need_to_be_switch'  => false,
                            'user_id'               => $user_id,
                            'last_status_movement'  => 'receive',
                            'last_locator_id'       => $receiving_location->id,
                            'last_movement_date'    => $movement_date,
                            'created_at'            => $movement_date,
                            'updated_at'            => $movement_date,
                            'last_user_movement_id' => $user_id,
                            'so_id'                 => $so_id
                        ]);

                        //insert detail
                        DetailMaterialPreparation::FirstOrCreate([
                            'material_preparation_id'   => $material_preparation->id,
                            'material_arrival_id'       => $material_arrival_id,
                            'material_subcont_id'       => $material_subcont_id,
                            'user_id'                   => $user_id
                        ]);

                        if($material_preparation->po_detail_id == 'FREE STOCK' || $material_preparation->po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        }
                        
                        $is_movement_receive_exists = MaterialMovement::where([
                            ['from_location',$handover_location->id],
                            ['to_destination',$receiving_location->id],
                            ['from_locator_erp_id',$handover_location->area->erp_id],
                            ['to_locator_erp_id',$receiving_location->area->erp_id],
                            ['po_buyer',$material_preparation->po_buyer],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','receive'],
                        ])
                        ->first();
                        
                        if(!$is_movement_receive_exists)
                        {
                            $material_receive_movement = MaterialMovement::firstorcreate([
                                'from_location'         => $handover_location->id,
                                'to_destination'        => $receiving_location->id,
                                'from_locator_erp_id'   => $handover_location->area->erp_id,
                                'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $material_preparation->po_buyer,
                                'status'                => 'receive',
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'created_at'            => $material_preparation->last_movement_date,
                                'updated_at'            => $material_preparation->last_movement_date,

                            ]);
                            $material_receive_movement_id = $material_receive_movement->id;
                        }else
                        {
                            $is_movement_receive_exists->updated_at = $movement_date;
                            $is_movement_receive_exists->save();

                            $material_receive_movement_id = $is_movement_receive_exists->id;
                        }
                        
                        $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_receive_movement_id],
                            ['material_preparation_id',$material_preparation->id],
                            ['qty_movement',$material_preparation->qty_conversion],
                            ['item_id',$material_preparation->item_id],
                            ['warehouse_id',$material_preparation->warehouse],
                            ['date_movement',$material_preparation->last_movement_date],
                            ['is_integrate',false],
                            ['is_active',true],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_exists)
                        {
                            $new_material_movement_line = MaterialMovementLine::Create([
                                'material_movement_id'          => $material_receive_movement_id,
                                'material_preparation_id'       => $material_preparation->id,
                                'item_code'                     => $material_preparation->item_code,
                                'item_id'                       => $material_preparation->item_id,
                                'c_order_id'                    => $material_preparation->c_order_id,
                                'c_orderline_id'                => $c_orderline_id,
                                'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                'supplier_name'                 => $material_preparation->supplier_name,
                                'type_po'                       => 2,
                                'is_integrate'                  => false,
                                'uom_movement'                  => $material_preparation->uom_conversion,
                                'qty_movement'                  => $material_preparation->qty_conversion,
                                'date_movement'                 => $material_preparation->last_movement_date,
                                'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                'nomor_roll'                    => '-',
                                'warehouse_id'                  => $material_preparation->warehouse,
                                'document_no'                   => $material_preparation->document_no,
                                'note'                          => null,
                                'is_active'                     => true,
                                'user_id'                       => $material_preparation->last_user_movement_id,
                            ]);

                            $material_preparation->last_material_movement_line_id  = $new_material_movement_line->id;
                            $material_preparation->save();
                        }

                        $concatenate                .= "'" .$material_preparation->barcode."',";
                        $temporary = Temporary::Create([
                            'barcode'       => $material_preparation->po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => $user_id,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);

                        $old = $available_qty;
                        $new = $available_qty - $material_preparation->qty_conversion;

                        if($new <= 0) $new = '0';
                        else $new = $new;

                        HistoryStock::approved($material_stock_id
                        ,$new
                        ,$old
                        ,$material_preparation->qty_conversion
                        ,$material_preparation->new_po_buyer
                        ,$material_preparation->style
                        ,$material_preparation->article_no
                        ,$lc_date
                        ,$allocation_source
                        ,$username
                        ,$user_id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,false
                        ,true);

                        $available_qty -= $material_preparation->qty_conversion;
                        $reserved_qty += $material_preparation->qty_conversion;
                        
                        if($available_qty <=0 )
                        {
                            $material_stock->is_allocated   = true;
                            $material_stock->is_active      = false;
                        }

                        $material_stock->available_qty      = sprintf('%0.8f',$available_qty);
                        $material_stock->reserved_qty       = sprintf('%0.8f',$reserved_qty);
                        $material_stock->save();

                        $material_arrival->preparation_date = $movement_date;
                        $material_arrival->save();
                    }else
                    {
                        $is_exists->last_status_movement        = 'receive';
                        $is_exists->last_locator_id             = $receiving_location->id;
                        $is_exists->last_movement_date          = $movement_date;
                        $is_exists->deleted_at                  = null;
                        $is_exists->last_user_movement_id       = $user_id;
                        $is_exists->save();

                        if($is_exists->po_detail_id == 'FREE STOCK' || $is_exists->po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_receive_id  = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$is_exists->po_detail_id)
                            ->first();
                            
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_receive_id = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        }
                        
                        $is_movement_receive_exists = MaterialMovement::where([
                            ['from_location',$handover_location->id],
                            ['to_destination',$receiving_location->id],
                            ['from_locator_erp_id',$handover_location->area->erp_id],
                            ['to_locator_erp_id',$receiving_location->area->erp_id],
                            ['po_buyer',$is_exists->po_buyer],
                            ['is_integrate',false],
                            ['is_active',true],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','receive'],
                        ])
                        ->first();
                        
                        if(!$is_movement_receive_exists)
                        {
                            $material_receive_movement = MaterialMovement::firstorcreate([
                                'from_location'         => $handover_location->id,
                                'to_destination'        => $receiving_location->id,
                                'from_locator_erp_id'   => $handover_location->area->erp_id,
                                'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                'is_integrate'          => false,
                                'is_active'             => true,
                                'po_buyer'              => $is_exists->po_buyer,
                                'status'                => 'receive',
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                'created_at'            => $movement_date,
                                'updated_at'            => $movement_date,

                            ]);
                            $material_receive_movement_id = $material_receive_movement->id;
                        }else
                        {
                            $is_movement_receive_exists->updated_at = $movement_date;
                            $is_movement_receive_exists->save();

                            $material_receive_movement_id = $is_movement_receive_exists->id;
                        }

                        $is_material_movement_line_receive_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_receive_movement_id],
                            ['material_preparation_id',$is_exists->id],
                            ['qty_movement',$is_exists->qty_conversion],
                            ['item_id',$is_exists->item_id],
                            ['warehouse_id',$is_exists->warehouse],
                            ['date_movement',$movement_date],
                            ['is_active',true],
                            ['is_integrate',false],
                        ])
                        ->exists();

                        if(!$is_material_movement_line_receive_exists)
                        {
                            $material_movement_line_receive = MaterialMovementLine::create([
                                'material_movement_id'      => $material_receive_movement_id,
                                'material_preparation_id'   => $is_exists->id,
                                'item_id'                   => $is_exists->item_id,
                                'item_code'                 => $is_exists->item_code,
                                'type_po'                   => $is_exists->type_po,
                                'c_order_id'                => $is_exists->c_order_id,
                                'c_bpartner_id'             => $is_exists->c_bpartner_id,
                                'supplier_name'             => $is_exists->supplier_name,
                                'c_orderline_id'            => $c_orderline_id,
                                'uom_movement'              => $is_exists->uom_conversion,
                                'qty_movement'              => $is_exists->qty_conversion,
                                'date_movement'             => $movement_date,
                                'created_at'                => $movement_date,
                                'updated_at'                => $movement_date,
                                'is_active'                 => true,
                                'is_integrate'              => false,
                                'warehouse_id'              => $is_exists->warehouse,
                                'document_no'               => $is_exists->document_no,
                                'user_id'                   => $user_id
                            ]);
                        }

                        $is_exists->last_material_movement_line_id  = $material_movement_line_receive->id;
                        $is_exists->save();

                        $temporary = Temporary::Create([
                            'barcode'       => $is_exists->po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => $user_id,
                            'created_at'    => $movement_date,
                            'updated_at'    => $movement_date,
                        ]);
                    }

                    DB::commit();
                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }
            }
            
           
            $material_stock->last_status            = null;
            $material_stock->save();
            AccessoriesReportMaterialStockController::doRecalculate($material_stock->id);


        }else
        {
            $material_arrival           = MaterialArrival::where([
                ['po_detail_id',$po_detail_id],
                ['warehouse_id',$warehouse_id],
            ])
            ->whereNull('material_subcont_id')
            ->first();
            //dd($material_arrival);
        
            if (strpos($document_no, "POAR") !== false) $is_poar = true;
            else $is_poar = false;

            $material_arrival_id        = ($material_arrival ? $material_arrival->id : null);
            $prepared_status            = ($material_arrival ? $material_arrival->prepared_status : null );
            $po_buyer                   = ($material_arrival ? $material_arrival->po_buyer : null );
            $user_receive_id            = ($material_arrival && $is_running_stock ? $material_arrival->user_id : $created_by );
            $uom                        = ($material_arrival ? $material_arrival->uom : null);

            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
            {
                $c_order_id             = 'FREE STOCK';
                $no_packing_list        = '-';
                $no_invoice             = '-';
                $c_orderline_id         = '-';
            }else
            {
                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
            }

            $auto_allocations = AutoAllocation::where([
                ['c_order_id',$c_order_id],
                ['item_id_source',$item_id],
                ['warehouse_id',$warehouse_id],
                ['is_fabric',false],
                //['is_additional',false],
                //['status_po_buyer','active'],
                //['qty_outstanding','>',0],
            ])
            ->whereNull('locked_date')
            ->whereNull('deleted_at');

            // dd($auto_allocations);

            //if($prepared_status != 'NO NEED PREPARED') $auto_allocations = $auto_allocations->where('qty_outstanding','>',0);

            if($is_running_stock)
            {
                if($uom == 'M' || $uom == 'YDS' || $uom == 'CNS')
                {
                    $auto_allocations = $auto_allocations->where('is_allocation_purchase',true);
                    $prepared_status  = 'NEED PREPARED';
                }
                else
                {
                    $auto_allocations = $auto_allocations->where('is_allocation_purchase',true);
                    if($prepared_status == 'NO NEED PREPARED' || $prepared_status == 'NEED PREPARED TOP & BOTTOM') $auto_allocations = $auto_allocations->where('po_buyer',$po_buyer);
                    else if ($prepared_status == 'NEED PREPARED')
                    {
                        $auto_allocations = $auto_allocations->where('status_po_buyer','active')
                        ->whereIn('po_buyer',function ($query) use ($po_detail_id,$warehouse_id)
                        {
                            $query->select('po_buyer')
                            ->from('material_ready_preparations')
                            ->whereIn('material_arrival_id',function($query2) use ($po_detail_id,$warehouse_id)
                            {
                                $query2->select('id')
                                ->from('material_arrivals')
                                ->where([
                                    ['po_detail_id',$po_detail_id],
                                    ['warehouse_id',$warehouse_id],
                                ])
                                ->whereNull('material_subcont_id');
                            })
                            ->groupby('po_buyer');
                        });
                    }
                }
            } 
            else
            {
                $auto_allocations = $auto_allocations->where([
                    ['is_allocation_purchase',false],
                    ['qty_outstanding','>',0],
                ]);
            } 
            
            $auto_allocations = $auto_allocations->orderby('promise_date','asc')
            ->orderby('qty_allocation','asc')
            ->whereNull('deleted_at')
            ->get();

            //dd($auto_allocations).'<br/>';
                
            //echo $available_qty;
            if($available_qty > 0.0000)
            {
                foreach ($auto_allocations as $key_2 => $auto_allocation) 
                {
                    try 
                    {
                        DB::beginTransaction();

                        //echo 'masuk if';
                        //echo $auto_allocation->locked_date;
                        if(!$auto_allocation->locked_date)
                        {
                            $auto_allocation->locked_date = $confirm_date;
                            $auto_allocation->save();
                            DB::commit();

                            $is_reroute             = $auto_allocation->is_reroute;
                            $is_reduce              = $auto_allocation->is_reduce;
                            $auto_allocation_id     = $auto_allocation->id;
                            $old_po_buyer           = $auto_allocation->old_po_buyer;
                            $item_id_book           = $auto_allocation->item_id_book;
                            $item_id_source         = $auto_allocation->item_id_source;
                            $item_code_source       = $auto_allocation->item_code_source;
                            $_item_code_book        = $auto_allocation->item_code;
                            $status_po_buyer        = $auto_allocation->status_po_buyer;
                            $is_additional          = $auto_allocation->is_additional;
                        
                            if($auto_allocation->is_upload_manual)
                            {
                                $allocation_source      = 'ALLOCATION MANUAL';
                                $user                   = $auto_allocation->user_id;
                                $username               = strtoupper($auto_allocation->user->name);
                                $is_integrate           = false;
                                $last_user_movement_id  = $created_by;
                            }else
                            {
                                $allocation_source  = ($auto_allocation->is_allocation_purchase)? 'ALLOCATION PURCHASE' : 'ALLOCATION ERP';
                                $user               = $system->id;
                                $username           = strtoupper($auto_allocation->user->name);
                                $is_integrate       = true;
                                
    
                                if($auto_allocation->is_allocation_purchase) $last_user_movement_id  = $user_receive_id;
                                else $last_user_movement_id  = $created_by;
                            }
    
                            $pos                    = strpos($_item_code_book, '|');
                            if ($pos === false)
                            {
                                $item_code_book     = $_item_code_book;
                                $_article_no        = null;
                            }else 
                            {
                                $split              = explode('|',$_item_code_book);
                                $item_code_book     = $split[0];
                                $_article_no        = $split[1];
                            }
                        
                            $po_buyer               = $auto_allocation->po_buyer;
                            $is_reduce              = $auto_allocation->is_reduce;
                            //$qty_outstanding        = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$auto_allocation->qty_outstanding));//sprintf('%0.8f',$auto_allocation->qty_outstanding);
                            
                            if($auto_allocation->qty_outstanding > 0)
                            {
                                if($prepared_status == 'NO NEED PREPARED' && $is_running_stock) $qty_outstanding    = sprintf('%0.8f',$available_qty);//sprintf('%0.8f',$auto_allocation->qty_outstanding);
                                else $qty_outstanding = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                            }else
                            {
                                if($is_running_stock && $prepared_status == 'NO NEED PREPARED') $qty_outstanding  = sprintf('%0.8f',$available_qty);
                                else $qty_outstanding  = sprintf('%0.8f',$auto_allocation->qty_outstanding);
                            }
                            
                            $qty_allocated          = sprintf('%0.8f',$auto_allocation->qty_allocated);
                            $qty_adjustment         = sprintf('%0.8f',$auto_allocation->qty_adjustment);
                            
                            $isMrExists = MaterialRequirement::where([
                                ['po_buyer',$po_buyer],
                                ['item_id',$item_id_book],
                            ])
                            ->exists();
                            
                            if(!$isMrExists)
                            {
                                //charis
                                $material_requirements = DB::connection('erp')->select(db::raw("
                                    SELECT style,
                                        item_id,
                                        item_code,
                                        item_desc,
                                        uom,
                                        po_buyer,
                                        job_order,
                                        qty_required,
                                        article_no,
                                        is_piping,
                                        season,
                                        garment_size,
                                        part_no,
                                        warehouse_id,
                                        warehouse_name,
                                        city_name,
                                        component,
                                        category,
                                        supp_code,
                                        supp_name,
                                        qty_ordered_garment 
                                    FROM get_wms_material_requirement_detail_new('$po_buyer') 
                                    where item_code like '$_item_code_book';"
                                 ));

                                if(count($material_requirements) > 0)
                                {
                                    DetailMaterialRequirement::where([
                                        ['po_buyer',$po_buyer],
                                        ['item_code',$_item_code_book],
                                    ])
                                    ->delete();
                                    
                                    foreach ($material_requirements as $key2 => $material_requirement) 
                                    {
                                        DetailMaterialRequirement::Create([
                                            'season'              => $material_requirement->season,
                                            'po_buyer'            => $material_requirement->po_buyer,
                                            'article_no'          => $material_requirement->article_no,
                                            'item_id'             => $material_requirement->item_id,
                                            'item_code'           => $material_requirement->item_code,
                                            'item_desc'           => $material_requirement->item_desc,
                                            'uom'                 => $material_requirement->uom,
                                            'style'               => $material_requirement->style,
                                            'category'            => $material_requirement->category,
                                            'job_order'           => $material_requirement->job_order,
                                           // 'statistical_date'    => $statistical_date,
                                            //'lc_date'             => ($data)? $data->lc_date : null,
                                            //'promise_date'        => ($data)? $data->promise_date : null,
                                            'is_piping'           => $material_requirement->is_piping,
                                            'garment_size'        => $material_requirement->garment_size,
                                            'part_no'             => $material_requirement->part_no,
                                            'qty_required'        => sprintf('%0.8f',$material_requirement->qty_required),
                                            'warehouse_id'        => $material_requirement->warehouse_id,
                                            'warehouse_name'      => $material_requirement->warehouse_name,
                                            'component'           => $material_requirement->component,
                                            'destination_name'    => $material_requirement->city_name,
                                            'supplier_code'       => $material_requirement->supp_code,
                                            'supplier_name'       => $material_requirement->supp_name,
                                            'qty_ordered_garment' => $material_requirement->qty_ordered_garment,
                                        ]);
                                    }

                                    EMRC::getSummaryBuyerPerPart($po_buyer, $_item_code_book);
                                    EMRC::getSummaryBuyer($po_buyer,$_item_code_book);
                                }
            

                                
                            }

                            $material_requirements  = MaterialRequirement::where([
                                ['po_buyer',$po_buyer],
                                ['item_id',$item_id_book],
                            ]);
    
                            if($_article_no) $material_requirements  = $material_requirements->where('article_no',$_article_no);
    
                            $material_requirements  = $material_requirements->orderby('article_no','asc')
                            ->orderby('style','asc')
                            ->get();
    
                            $count_mr               = count($material_requirements);

                            //echo 'masuk if2';
                            echo "totl mr : ".$count_mr."\n";
                            if($count_mr > 0)
                            {
                                //echo 'mxxxx';
                                if($prepared_status == 'NO NEED PREPARED' && $is_running_stock) $__qty_outstanding = sprintf('%0.8f',$available_qty);
                                else $__qty_outstanding = sprintf('%0.8f',$qty_outstanding);
                                
                                //echo 'asdsd';
                                //echo $__qty_outstanding;
                                if($__qty_outstanding > 0.0000)
                                {
                                    if ($available_qty/$qty_outstanding >= 1) $_supply  = $qty_outstanding;
                                    else $_supply   = $available_qty;
                                    
                                    $qty_supply     = $_supply;
                                    
                                    foreach ($material_requirements as $key => $material_requirement) 
                                    {
                                        $lc_date        = $material_requirement->lc_date;
                                        //$month_lc       = $material_requirement->lc_date->format('m');
                                        $item_desc      = strtoupper($material_requirement->item_desc);
                                        $uom            = $material_requirement->uom;
                                        $category       = $material_requirement->category;
                                        $style          = $material_requirement->style;
                                        $job_order      = $material_requirement->job_order;
                                        $article_no     = $material_requirement->article_no;
                                        $_style         = explode('::',$job_order)[0];
                                       // dd("2");
                                        if($status_po_buyer == 'active')
                                        {
                                            if($is_poar) $qty_required   = $qty_outstanding;
                                            else $qty_required   = ($prepared_status == 'NO NEED PREPARED' && $is_running_stock  ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code_book,$style,$article_no)));
                                        } 
                                        else if($status_po_buyer == 'cancel') $qty_required   = $qty_outstanding;
    
    
                                        //if($month_lc <= '02') $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
                                        //else $qty_required = sprintf('%0.8f',$material_requirement->qty_required);
            
                                        //echo $po_buyer.' '.$style.' '.$qty_supply.' '.$material_requirement->qty_required.' '.$qty_required.'<br/>';
                                        
                                        
                                        if(($key+1) != $count_mr) $__supplied = sprintf('%0.8f',$qty_required);
                                        else $__supplied = sprintf('%0.8f',$qty_supply);

                                        // dd($__supplied > 0.0000 && $qty_supply > 0.0000);
                                        
                                        if($__supplied > 0.0000 && $qty_supply > 0.0000)
                                        {
                                            if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                            else $_supplied = $qty_supply;
            
                                            if($prepared_status == 'NO NEED PREPARED' && $is_running_stock) $_supplied = sprintf('%0.8f',$available_qty);
                                            else $_supplied = $_supplied;
    
                                            $__supplied     = sprintf('%0.8f',$_supplied);
                                        
                                            $is_exists = MaterialPreparation::where([
                                                ['material_stock_id',$material_stock_id],
                                                ['auto_allocation_id',$auto_allocation_id],
                                                ['c_order_id',$c_order_id],
                                                ['po_buyer',$po_buyer],
                                                ['item_id',$item_id_book],
                                                ['uom_conversion',$uom],
                                                ['warehouse',$warehouse_id],
                                                ['style',$style],
                                                ['article_no',$article_no],
                                                ['qty_conversion',$__supplied],
                                                ['is_backlog',false],
                                            ]);
    
                                            if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
    
                                            $is_exists = $is_exists->exists();
    
                                            //echo $po_buyer.' '.$item_code.' '.$is_exists.'</br>';
                                    
                                            if(!$is_exists && $_supplied > 0.0000)
                                            {
                                                $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);
                                                $so_id                  = PoBuyer::where('po_buyer', $po_buyer)->first();

                                                $material_preparation = MaterialPreparation::FirstOrCreate([
                                                    'material_stock_id'         => $material_stock_id,
                                                    'auto_allocation_id'        => $auto_allocation_id,
                                                    'po_detail_id'              => $po_detail_id,
                                                    'barcode'                   => ($prepared_status == 'NO NEED PREPARED' && $is_running_stock ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'referral_code'             => ($prepared_status == 'NO NEED PREPARED' && $is_running_stock ? $po_detail_id : 'BELUM DI PRINT'),
                                                    'sequence'                  => ($prepared_status == 'NO NEED PREPARED' && $is_running_stock ? $po_detail_id : -1),
                                                    'item_id'                   => $item_id_book,
                                                    'c_order_id'                => $c_order_id,
                                                    'c_bpartner_id'             => $c_bpartner_id,
                                                    'supplier_name'             => $supplier_name,
                                                    'document_no'               => $document_no,
                                                    'po_buyer'                  => $po_buyer,
                                                    'uom_conversion'            => $uom,
                                                    'qty_conversion'            => $__supplied,
                                                    'qty_reconversion'          => $qty_reconversion,
                                                    'job_order'                 => $job_order,
                                                    'style'                     => $style,
                                                    '_style'                    => $_style,
                                                    'article_no'                => $article_no,
                                                    'warehouse'                 => $warehouse_id,
                                                    'is_additional'             => $is_additional,
                                                    'item_code'                 => $item_code_book,
                                                    'item_desc'                 => $item_desc,
                                                    'category'                  => $category,
                                                    'is_backlog'                => false,
                                                    'total_carton'              => 1,
                                                    'type_po'                   => 2,
                                                    'user_id'                   => ($user_receive_id ? $user_receive_id : $system->id),
                                                    'last_status_movement'      => 'receive',
                                                    'is_need_to_be_switch'      => false,
                                                    'last_locator_id'           => $receiving_location->id, 
                                                    'last_movement_date'        => $confirm_date,
                                                    'created_at'                => $confirm_date,
                                                    'updated_at'                => $confirm_date,
                                                    'first_print_date'          => ($prepared_status == 'NO NEED PREPARED' && $is_running_stock ? $confirm_date : null),
                                                    'deleted_at'                => null,
                                                    'last_user_movement_id'     => ($user_receive_id ? $user_receive_id : $last_user_movement_id),
                                                    'is_stock_already_created'  => false,
                                                    'is_reduce'                 => $is_reduce,
                                                    'so_id'                     => $so_id->so_id
                                                ]);
    
                                                if($is_running_stock)
                                                {
                                                    $from_location_id       = $supplier_location->id;
                                                    $from_location_erp_id   = $supplier_location->area->erp_id;
                                                }else
                                                {
                                                    $from_location_id       = $allocation_stock->id;
                                                    $from_location_erp_id   = $allocation_stock->area->erp_id;
                                                }

                                                $is_movement_receive_exists = MaterialMovement::where([
                                                    ['from_location',$from_location_id],
                                                    ['to_destination',$receiving_location->id],
                                                    ['from_locator_erp_id',$from_location_erp_id],
                                                    ['to_locator_erp_id', $receiving_location->area->erp_id],
                                                    ['po_buyer',$material_preparation->po_buyer],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                    ['no_packing_list',$no_packing_list],
                                                    ['no_invoice',$no_invoice],
                                                    ['status','receive'],
                                                ])
                                                ->first();
    
                                                if(!$is_movement_receive_exists)
                                                {
                                                    $material_receive_movement = MaterialMovement::firstorcreate([
                                                        'from_location'         => $from_location_id,
                                                        'to_destination'        => $receiving_location->id,
                                                        'from_locator_erp_id'   => $from_location_erp_id,
                                                        'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                        'is_integrate'          => false,
                                                        'is_active'             => true,
                                                        'po_buyer'              => $material_preparation->po_buyer,
                                                        'status'                => 'receive',
                                                        'no_packing_list'       => $no_packing_list,
                                                        'no_invoice'            => $no_invoice,
                                                        'created_at'            => $material_preparation->last_movement_date,
                                                        'updated_at'            => $material_preparation->last_movement_date,
                        
                                                    ]);
                                                    $material_receive_movement_id = $material_receive_movement->id;
                                                }else
                                                {
                                                    $is_movement_receive_exists->updated_at = $confirm_date;
                                                    $is_movement_receive_exists->save();
    
                                                    $material_receive_movement_id = $is_movement_receive_exists->id;
                                                }
    
                                                $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                ->where([
                                                    ['material_movement_id',$material_receive_movement_id],
                                                    ['material_preparation_id',$material_preparation->id],
                                                    ['qty_movement',$material_preparation->qty_conversion],
                                                    ['date_movement',$material_preparation->last_movement_date],
                                                    ['is_integrate',false],
                                                    ['is_active',true],
                                                ])
                                                ->exists();
            
                                                if(!$is_material_movement_line_exists)
                                                {
                                                    $new_material_movement_line = MaterialMovementLine::Create([
                                                        'material_movement_id'          => $material_receive_movement_id,
                                                        'material_preparation_id'       => $material_preparation->id,
                                                        'item_code'                     => $material_preparation->item_code,
                                                        'item_id'                       => $material_preparation->item_id,
                                                        'c_order_id'                    => $material_preparation->c_order_id,
                                                        'c_orderline_id'                => $c_orderline_id,
                                                        'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                        'supplier_name'                 => $material_preparation->supplier_name,
                                                        'type_po'                       => 2,
                                                        'is_integrate'                  => false,
                                                        'uom_movement'                  => $material_preparation->uom_conversion,
                                                        'qty_movement'                  => $material_preparation->qty_conversion,
                                                        'date_movement'                 => $material_preparation->last_movement_date,
                                                        'created_at'                    => $material_preparation->last_movement_date,
                                                        'updated_at'                    => $material_preparation->last_movement_date,
                                                        'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                                        'nomor_roll'                    => '-',
                                                        'warehouse_id'                  => $material_preparation->warehouse,
                                                        'document_no'                   => $material_preparation->document_no,
                                                        'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                        'is_active'                     => true,
                                                        'user_id'                       => $material_preparation->last_user_movement_id,
                                                    ]);
            
                                                    $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                    $material_preparation->save();
                                                }
    
                                                DetailMaterialPreparation::FirstOrCreate([
                                                    'material_preparation_id'   => $material_preparation->id,
                                                    'material_arrival_id'       => $material_arrival_id,
                                                    'user_id'                   => $material_preparation->user_id
                                                ]);
    
                                                if($is_reroute && $prepared_status == 'NO NEED PREPARED' && $is_running_stock)
                                                {
                                                    $reroute_date   = carbon::now();
                                                    $is_old_exists = MaterialPreparation::where([
                                                        ['material_stock_id',$material_preparation->material_stock_id],
                                                        ['auto_allocation_id',$material_preparation->auto_allocation_id],
                                                        ['c_order_id',$material_preparation->c_order_id],
                                                        ['po_buyer',$old_po_buyer],
                                                        ['item_id',$material_preparation->item_id],
                                                        ['uom_conversion',$material_preparation->uom_conversion],
                                                        ['warehouse',$material_preparation->warehouse],
                                                        ['style',$material_preparation->style],
                                                        ['article_no',$material_preparation->article_no],
                                                        ['qty_conversion',$material_preparation->qty_conversion],
                                                        ['is_backlog',$material_preparation->is_backlog],
                                                    ]);
            
                                                    if($material_preparation->po_detail_id) $is_old_exists = $is_old_exists->where('po_detail_id',$material_preparation->po_detail_id);
            
                                                    $is_old_exists = $is_old_exists->exists();
    
                                                    if(!$is_old_exists)
                                                    {
                                                        $old_material_preparation = MaterialPreparation::FirstOrCreate([
                                                            'material_stock_id'         => $material_preparation->material_stock_id,
                                                            'auto_allocation_id'        => $material_preparation->auto_allocation_id,
                                                            'po_detail_id'              => $material_preparation->po_detail_id,
                                                            'barcode'                   => 'REROUTE_'.$material_preparation->barcode,
                                                            'referral_code'             => $material_preparation->referral_code,
                                                            'sequence'                  => $material_preparation->sequence,
                                                            'item_id'                   => $material_preparation->item_id,
                                                            'c_order_id'                => $material_preparation->c_order_id,
                                                            'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                                            'supplier_name'             => $material_preparation->supplier_name,
                                                            'document_no'               => $material_preparation->document_no,
                                                            'po_buyer'                  => $old_po_buyer,
                                                            'uom_conversion'            => $material_preparation->uom_conversion,
                                                            'qty_conversion'            => $material_preparation->qty_conversion,
                                                            'qty_reconversion'          => $material_preparation->qty_reconversion,
                                                            'job_order'                 => $material_preparation->job_order,
                                                            'style'                     => $material_preparation->style,
                                                            '_style'                    => $material_preparation->_style,
                                                            'article_no'                => $material_preparation->article_no,
                                                            'warehouse'                 => $material_preparation->warehouse,
                                                            'item_code'                 => $material_preparation->item_code,
                                                            'item_desc'                 => $material_preparation->item_desc,
                                                            'category'                  => $material_preparation->category,
                                                            'is_backlog'                => $material_preparation->is_backlog,
                                                            'total_carton'              => 1,
                                                            'type_po'                   => 2,
                                                            'user_id'                   => $material_preparation->user_id,
                                                            'last_status_movement'      => 'reroute',
                                                            'is_reroute'                => true,
                                                            'is_need_to_be_switch'      => false,
                                                            'last_locator_id'           => $reroute_destination->id, 
                                                            'last_movement_date'        => $material_preparation->last_movement_date,
                                                            'created_at'                => $material_preparation->created_at,
                                                            'updated_at'                => $material_preparation->updated_at,
                                                            'first_print_date'          => $material_preparation->first_print_date,
                                                            'deleted_at'                => $material_preparation->created_at,
                                                            'last_user_movement_id'     => $material_preparation->last_user_movement_id,
                                                            'is_stock_already_created'  => $material_preparation->is_stock_already_created,
                                                            'is_reduce'                 => $material_preparation->is_reduce
                                                        ]);
                                                        
                                                        $is_old_movement_receive_exists = MaterialMovement::where([
                                                            ['from_location',$supplier_location->id],
                                                            ['to_destination',$receiving_location->id],
                                                            ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                            ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                            ['po_buyer',$old_material_preparation->po_buyer],
                                                            ['is_integrate',false],
                                                            ['is_active',true],
                                                            ['no_packing_list',$no_packing_list],
                                                            ['no_invoice',$no_invoice],
                                                            ['status','receive'],
                                                        ])
                                                        ->first();
    
                                                        if(!$is_old_movement_receive_exists)
                                                        {
                                                            $old_material_receive_movement = MaterialMovement::firstorcreate([
                                                                'from_location'         => $supplier_location->id,
                                                                'to_destination'        => $receiving_location->id,
                                                                'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                                'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                                'is_integrate'          => false,
                                                                'is_active'             => true,
                                                                'po_buyer'              => $old_material_preparation->po_buyer,
                                                                'status'                => 'receive',
                                                                'no_packing_list'       => $no_packing_list,
                                                                'no_invoice'            => $no_invoice,
                                                                'created_at'            => $old_material_preparation->last_movement_date,
                                                                'updated_at'            => $old_material_preparation->last_movement_date,
                                
                                                            ]);
    
                                                            $old_material_receive_movement_id = $old_material_receive_movement->id;
                                                        }else
                                                        {
                                                            $is_old_movement_receive_exists->updated_at = $confirm_date;
                                                            $is_old_movement_receive_exists->save();
    
                                                            $old_material_receive_movement_id = $is_old_movement_receive_exists->id;
                                                        }
    
                                                        $is_old_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                        ->where([
                                                            ['material_movement_id',$old_material_receive_movement_id],
                                                            ['material_preparation_id',$old_material_preparation->id],
                                                            ['qty_movement',$old_material_preparation->qty_conversion],
                                                            ['date_movement',$old_material_preparation->last_movement_date],
                                                            ['warehouse_id',$old_material_preparation->warehouse],
                                                            ['item_id',$old_material_preparation->item_id],
                                                            ['is_integrate',false],
                                                            ['is_active',true],
                                                        ])
                                                        ->exists();
                    
                                                        if(!$is_old_material_movement_line_exists)
                                                        {
                                                            $new_old_material_movement_line = MaterialMovementLine::Create([
                                                                'material_movement_id'          => $old_material_receive_movement_id,
                                                                'material_preparation_id'       => $old_material_preparation->id,
                                                                'item_code'                     => $old_material_preparation->item_code,
                                                                'item_id'                       => $old_material_preparation->item_id,
                                                                'c_order_id'                    => $old_material_preparation->c_order_id,
                                                                'c_orderline_id'                => $c_orderline_id,
                                                                'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                                'supplier_name'                 => $old_material_preparation->supplier_name,
                                                                'type_po'                       => 2,
                                                                'is_integrate'                  => false,
                                                                'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                                'qty_movement'                  => $old_material_preparation->qty_conversion,
                                                                'date_movement'                 => $old_material_preparation->last_movement_date,
                                                                'created_at'                    => $old_material_preparation->last_movement_date,
                                                                'updated_at'                    => $old_material_preparation->last_movement_date,
                                                                'date_receive_on_destination'   => $old_material_preparation->last_movement_date,
                                                                'nomor_roll'                    => '-',
                                                                'warehouse_id'                  => $old_material_preparation->warehouse,
                                                                'document_no'                   => $old_material_preparation->document_no,
                                                                'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                                'is_active'                     => true,
                                                                'user_id'                       => $old_material_preparation->last_user_movement_id,
                                                            ]);
                    
                                                            $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_line->id;
                                                            $old_material_preparation->save();
                                                        }
    
                                                        $is_old_movement_reroute_exists = MaterialMovement::where([
                                                            ['from_location',$receiving_location->id],
                                                            ['to_destination',$reroute_destination->id],
                                                            ['from_locator_erp_id',$receiving_location->area->erp_id],
                                                            ['to_locator_erp_id',$reroute_destination->area->erp_id],
                                                            ['po_buyer',$old_material_preparation->po_buyer],
                                                            ['is_integrate',false],
                                                            ['is_active',true],
                                                            ['no_packing_list',$no_packing_list],
                                                            ['no_invoice',$no_invoice],
                                                            ['status','reroute'],
                                                        ])
                                                        ->first();
    
                                                        if(!$is_old_movement_reroute_exists)
                                                        {
                                                            $old_movement_reroute = MaterialMovement::firstorcreate([
                                                                'from_location'         => $receiving_location->id,
                                                                'to_destination'        => $reroute_destination->id,
                                                                'from_locator_erp_id'   => $receiving_location->area->erp_id,
                                                                'to_locator_erp_id'     => $reroute_destination->area->erp_id,
                                                                'is_integrate'          => false,
                                                                'is_active'             => true,
                                                                'po_buyer'              => $old_material_preparation->po_buyer,
                                                                'status'                => 'reroute',
                                                                'no_packing_list'       => $no_packing_list,
                                                                'no_invoice'            => $no_invoice,
                                                                'created_at'            => $reroute_date,
                                                                'updated_at'            => $reroute_date,
                                
                                                            ]);
    
                                                            $old_movement_reroute_id = $old_movement_reroute->id;
                                                        }else
                                                        {
                                                            $is_old_movement_reroute_exists->updated_at = $reroute_date;
                                                            $is_old_movement_reroute_exists->save();
    
                                                            $old_movement_reroute_id = $is_old_movement_reroute_exists->id;
                                                        }
    
                                                        $is_old_material_movement_reroute_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                        ->where([
                                                            ['material_movement_id',$old_movement_reroute_id],
                                                            ['material_preparation_id',$old_material_preparation->id],
                                                            ['item_id',$old_material_preparation->item_id],
                                                            ['qty_movement',$old_material_preparation->qty_conversion],
                                                            ['warehouse_id',$old_material_preparation->warehouse],
                                                            ['date_movement',$reroute_date],
                                                            ['is_integrate',false],
                                                            ['is_active',true],
                                                        ])
                                                        ->exists();
                    
                                                        if(!$is_old_material_movement_reroute_line_exists)
                                                        {
                                                            $new_old_material_movement_reroute_line = MaterialMovementLine::Create([
                                                                'material_movement_id'          => $old_movement_reroute_id,
                                                                'material_preparation_id'       => $old_material_preparation->id,
                                                                'item_code'                     => $old_material_preparation->item_code,
                                                                'item_id'                       => $old_material_preparation->item_id,
                                                                'c_order_id'                    => $old_material_preparation->c_order_id,
                                                                'c_orderline_id'                => $c_orderline_id,
                                                                'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                                'supplier_name'                 => $old_material_preparation->supplier_name,
                                                                'type_po'                       => 2,
                                                                'is_integrate'                  => false,
                                                                'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                                'qty_movement'                  => $old_material_preparation->qty_conversion,
                                                                'date_movement'                 => $reroute_date,
                                                                'created_at'                    => $reroute_date,
                                                                'updated_at'                    => $reroute_date,
                                                                'date_receive_on_destination'   => $reroute_date,
                                                                'nomor_roll'                    => '-',
                                                                'warehouse_id'                  => $old_material_preparation->warehouse,
                                                                'document_no'                   => $old_material_preparation->document_no,
                                                                'note'                          => 'REROUTE KE PO BUYER '.$material_preparation->po_buyer,
                                                                'is_active'                     => true,
                                                                'user_id'                       => $old_material_preparation->last_user_movement_id,
                                                            ]);
    
                                                            $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_reroute_line->id;
                                                            $old_material_preparation->save();
                                                        }
            
                                                        DetailMaterialPreparation::FirstOrCreate([
                                                            'material_preparation_id'   => $old_material_preparation->id,
                                                            'material_arrival_id'       => $material_arrival_id,
                                                            'user_id'                   => $old_material_preparation->user_id
                                                        ]);
    
                                                        $concatenate .= "'" .$old_material_preparation->barcode."',";
                                                    }
                                                }
            
                                                $concatenate .= "'" .$material_preparation->barcode."',";
    
                                                $qty_supply         -= $_supplied;
                                                $qty_required       -= $_supplied;
                                                $qty_outstanding    -= $_supplied;
                                                $qty_allocated      += $_supplied;
    
                                                $old = $available_qty;
                                                $new = $available_qty - $_supplied;
    
                                                if($new <= 0) $new = '0';
                                                else $new = $new;
    
                                                HistoryStock::approved($material_stock_id
                                                ,$new
                                                ,$old
                                                ,$__supplied
                                                ,$po_buyer
                                                ,$style
                                                ,$article_no
                                                ,$lc_date
                                                ,$allocation_source
                                                ,$username
                                                ,$user
                                                ,$type_stock_erp_code
                                                ,$type_stock
                                                ,$is_integrate,
                                                true);
    
                                                $available_qty      -= $_supplied;
                                                $reserved_qty       += $_supplied;
                                                $is_bom_exists++;
                                                
                                            }
                                        }
            
                                    }
                                }
                                
            
                                if($qty_outstanding<=0)
                                {
                                    $auto_allocation->is_already_generate_form_booking  = true;
                                    $auto_allocation->generate_form_booking             = carbon::now();
                                }
                                
                                $recalculate[]                                          = $auto_allocation->id;
                                $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                                $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                                $auto_allocation->save();
                            }else
                            {
                                if($category != 'CT')
                                {
                                    if ($available_qty/$qty_outstanding >= 1) $_supply  = $qty_outstanding;
                                    else $_supply   = $available_qty;
                                    
                                    $qty_supply     = $_supply;

                                   
                                    $__qty_outstanding = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$qty_outstanding));
                                    if($__qty_outstanding > 0.0000)
                                    {

                                        foreach ($material_requirements as $key => $material_requirement) 
                                        {
            

                                            $lc_date        = null;
                                            $item_desc      = $material_stock->item_desc;
                                            $uom            = $material_stock->uom;
                                            $category       = $material_stock->category;
                                            $style          = 'BOM TIDAK DITEMUKAN';
                                            $job_order      = 'BOM TIDAK DITEMUKAN';
                                            $article_no     = 'BOM TIDAK DITEMUKAN';
                                            $_style         = 'BOM TIDAK DITEMUKAN';
                                            
                                            if($status_po_buyer == 'active')
                                            {
                                                if($is_poar) $qty_required   = $qty_outstanding;
                                                else $qty_required   = ($prepared_status == 'NO NEED PREPARED' ? sprintf('%0.8f',$available_qty) : sprintf('%0.8f',$material_requirement->qty_required - $material_requirement->qtyAllocated($po_buyer,$item_code_book,$style,$article_no)));
                                            } 
                                            else if($status_po_buyer == 'cancel') $qty_required   = $qty_outstanding;
        
                                            $__supplied = sprintf('%0.8f',$qty_supply);
                                            
                                            if($__supplied > 0.0000 && $qty_supply > 0.0000)
                                            {
                                                if ($qty_supply/$__supplied >= 1) $_supplied = $__supplied;
                                                else $_supplied = $qty_supply;
                
                                                if($prepared_status == 'NO NEED PREPARED') $_supplied = sprintf('%0.8f',$available_qty);
                                                else $_supplied = $_supplied;
        
                                                $__supplied     = sprintf('%0.8f',$_supplied);
                                            
                                                $is_exists = MaterialPreparation::where([
                                                    ['material_stock_id',$material_stock_id],
                                                    ['auto_allocation_id',$auto_allocation_id],
                                                    ['c_order_id',$c_order_id],
                                                    ['po_buyer',$po_buyer],
                                                    ['item_id',$item_id_book],
                                                    ['uom_conversion',$uom],
                                                    ['warehouse',$warehouse_id],
                                                    ['style',$style],
                                                    ['article_no',$article_no],
                                                    ['qty_conversion',$__supplied],
                                                    ['is_backlog',false],
                                                ]);
        
                                                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);
        
                                                $is_exists = $is_exists->exists();
        
                                                //echo $po_buyer.' '.$item_code.' '.$is_exists.'</br>';
                                        
                                                if(!$is_exists && $_supplied > 0.0000)
                                                {
                                                    $qty_reconversion       = sprintf('%0.8f',$__supplied * $multiplyrate);

                                                    $so_id                  = PoBuyer::where('po_buyer', $po_buyer)->first();

                                                    $material_preparation = MaterialPreparation::FirstOrCreate([
                                                        'material_stock_id'         => $material_stock_id,
                                                        'auto_allocation_id'        => $auto_allocation_id,
                                                        'po_detail_id'              => $po_detail_id,
                                                        'barcode'                   => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                        'referral_code'             => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : 'BELUM DI PRINT'),
                                                        'sequence'                  => ($prepared_status == 'NO NEED PREPARED' ? $po_detail_id : -1),
                                                        'item_id'                   => $item_id_book,
                                                        'c_order_id'                => $c_order_id,
                                                        'c_bpartner_id'             => $c_bpartner_id,
                                                        'supplier_name'             => $supplier_name,
                                                        'document_no'               => $document_no,
                                                        'po_buyer'                  => $po_buyer,
                                                        'uom_conversion'            => $uom,
                                                        'qty_conversion'            => $__supplied,
                                                        'qty_reconversion'          => $qty_reconversion,
                                                        'job_order'                 => $job_order,
                                                        'style'                     => $style,
                                                        '_style'                    => $_style,
                                                        'article_no'                => $article_no,
                                                        'warehouse'                 => $warehouse_id,
                                                        'item_code'                 => $item_code_book,
                                                        'item_desc'                 => $item_desc,
                                                        'category'                  => $category,
                                                        'is_backlog'                => false,
                                                        'total_carton'              => 1,
                                                        'type_po'                   => 2,
                                                        'user_id'                   => ($user_receive_id ? $user_receive_id : $system->id),
                                                        'last_status_movement'      => 'receive',
                                                        'is_need_to_be_switch'      => false,
                                                        'last_locator_id'           => $receiving_location->id, 
                                                        'last_movement_date'        => $confirm_date,
                                                        'created_at'                => $confirm_date,
                                                        'updated_at'                => $confirm_date,
                                                        'first_print_date'          => ($prepared_status == 'NO NEED PREPARED' ? $confirm_date : null),
                                                        'deleted_at'                => null,
                                                        'last_user_movement_id'     => ($user_receive_id ? $user_receive_id : $last_user_movement_id),
                                                        'is_stock_already_created'  => false,
                                                        'is_reduce'                 => $is_reduce,
                                                        'so_id'                     => $so_id->so_id
                                                    ]);
        
                                                    $is_movement_receive_exists = MaterialMovement::where([
                                                        ['from_location',$supplier_location->id],
                                                        ['to_destination',$receiving_location->id],
                                                        ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                        ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                        ['po_buyer',$material_preparation->po_buyer],
                                                        ['is_integrate',false],
                                                        ['is_active',true],
                                                        ['no_packing_list',$no_packing_list],
                                                        ['no_invoice',$no_invoice],
                                                        ['status','receive'],
                                                    ])
                                                    ->first();
        
                                                    if(!$is_movement_receive_exists)
                                                    {
                                                        $material_receive_movement = MaterialMovement::firstorcreate([
                                                            'from_location'         => $supplier_location->id,
                                                            'to_destination'        => $receiving_location->id,
                                                            'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                            'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                            'is_integrate'          => false,
                                                            'is_active'             => true,
                                                            'po_buyer'              => $material_preparation->po_buyer,
                                                            'status'                => 'receive',
                                                            'no_packing_list'       => $no_packing_list,
                                                            'no_invoice'            => $no_invoice,
                                                            'created_at'            => $material_preparation->last_movement_date,
                                                            'updated_at'            => $material_preparation->last_movement_date,
                            
                                                        ]);
                                                        $material_receive_movement_id = $material_receive_movement->id;
                                                    }else
                                                    {
                                                        $is_movement_receive_exists->updated_at = $confirm_date;
                                                        $is_movement_receive_exists->save();
        
                                                        $material_receive_movement_id = $is_movement_receive_exists->id;
                                                    }
        
                                                    $is_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                    ->where([
                                                        ['material_movement_id',$material_receive_movement_id],
                                                        ['material_preparation_id',$material_preparation->id],
                                                        ['qty_movement',$material_preparation->qty_conversion],
                                                        ['date_movement',$material_preparation->last_movement_date],
                                                        ['item_id',$material_preparation->item_id],
                                                        ['warehouse_id',$material_preparation->warehouse],
                                                        ['is_integrate',false],
                                                        ['is_active',true],
                                                    ])
                                                    ->exists();
                
                                                    if(!$is_material_movement_line_exists)
                                                    {
                                                        $new_material_movement_line = MaterialMovementLine::Create([
                                                            'material_movement_id'          => $material_receive_movement_id,
                                                            'material_preparation_id'       => $material_preparation->id,
                                                            'item_code'                     => $material_preparation->item_code,
                                                            'item_id'                       => $material_preparation->item_id,
                                                            'c_order_id'                    => $material_preparation->c_order_id,
                                                            'c_orderline_id'                => $c_orderline_id,
                                                            'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                                            'supplier_name'                 => $material_preparation->supplier_name,
                                                            'type_po'                       => 2,
                                                            'is_integrate'                  => false,
                                                            'uom_movement'                  => $material_preparation->uom_conversion,
                                                            'qty_movement'                  => $material_preparation->qty_conversion,
                                                            'date_movement'                 => $material_preparation->last_movement_date,
                                                            'created_at'                    => $material_preparation->last_movement_date,
                                                            'updated_at'                    => $material_preparation->last_movement_date,
                                                            'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                                            'nomor_roll'                    => '-',
                                                            'warehouse_id'                  => $material_preparation->warehouse,
                                                            'document_no'                   => $material_preparation->document_no,
                                                            'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                            'is_active'                     => true,
                                                            'user_id'                       => $material_preparation->last_user_movement_id,
                                                        ]);
                
                                                        $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                                                        $material_preparation->save();
                                                    }
        
                                                    DetailMaterialPreparation::FirstOrCreate([
                                                        'material_preparation_id'   => $material_preparation->id,
                                                        'material_arrival_id'       => $material_arrival_id,
                                                        'user_id'                   => $material_preparation->user_id
                                                    ]);
        
                                                    if($is_reroute && $prepared_status == 'NO NEED PREPARED')
                                                    {
                                                        $reroute_date   = carbon::now();
                                                        $is_old_exists = MaterialPreparation::where([
                                                            ['material_stock_id',$material_preparation->material_stock_id],
                                                            ['auto_allocation_id',$material_preparation->auto_allocation_id],
                                                            ['c_order_id',$material_preparation->c_order_id],
                                                            ['po_buyer',$old_po_buyer],
                                                            ['item_id',$material_preparation->item_id],
                                                            ['uom_conversion',$material_preparation->uom_conversion],
                                                            ['warehouse',$material_preparation->warehouse],
                                                            ['style',$material_preparation->style],
                                                            ['article_no',$material_preparation->article_no],
                                                            ['qty_conversion',$material_preparation->qty_conversion],
                                                            ['is_backlog',$material_preparation->is_backlog],
                                                        ]);
                
                                                        if($material_preparation->po_detail_id) $is_old_exists = $is_old_exists->where('po_detail_id',$material_preparation->po_detail_id);
                
                                                        $is_old_exists = $is_old_exists->exists();
        
                                                        if(!$is_old_exists)
                                                        {
                                                            $old_material_preparation = MaterialPreparation::FirstOrCreate([
                                                                'material_stock_id'         => $material_preparation->material_stock_id,
                                                                'auto_allocation_id'        => $material_preparation->auto_allocation_id,
                                                                'po_detail_id'              => $material_preparation->po_detail_id,
                                                                'barcode'                   => 'REROUTE_'.$material_preparation->barcode,
                                                                'referral_code'             => $material_preparation->referral_code,
                                                                'sequence'                  => $material_preparation->sequence,
                                                                'item_id'                   => $material_preparation->item_id,
                                                                'c_order_id'                => $material_preparation->c_order_id,
                                                                'c_bpartner_id'             => $material_preparation->c_bpartner_id,
                                                                'supplier_name'             => $material_preparation->supplier_name,
                                                                'document_no'               => $material_preparation->document_no,
                                                                'po_buyer'                  => $old_po_buyer,
                                                                'uom_conversion'            => $material_preparation->uom_conversion,
                                                                'qty_conversion'            => $material_preparation->qty_conversion,
                                                                'qty_reconversion'          => $material_preparation->qty_reconversion,
                                                                'job_order'                 => $material_preparation->job_order,
                                                                'style'                     => $material_preparation->style,
                                                                '_style'                    => $material_preparation->_style,
                                                                'article_no'                => $material_preparation->article_no,
                                                                'warehouse'                 => $material_preparation->warehouse,
                                                                'item_code'                 => $material_preparation->item_code,
                                                                'item_desc'                 => $material_preparation->item_desc,
                                                                'category'                  => $material_preparation->category,
                                                                'is_backlog'                => $material_preparation->is_backlog,
                                                                'total_carton'              => 1,
                                                                'type_po'                   => 2,
                                                                'user_id'                   => $material_preparation->user_id,
                                                                'last_status_movement'      => 'reroute',
                                                                'is_reroute'                => true,
                                                                'is_need_to_be_switch'      => false,
                                                                'last_locator_id'           => $reroute_destination->id, 
                                                                'last_movement_date'        => $material_preparation->last_movement_date,
                                                                'created_at'                => $material_preparation->created_at,
                                                                'updated_at'                => $material_preparation->updated_at,
                                                                'first_print_date'          => $material_preparation->first_print_date,
                                                                'deleted_at'                => $material_preparation->created_at,
                                                                'last_user_movement_id'     => $material_preparation->last_user_movement_id,
                                                                'is_stock_already_created'  => $material_preparation->is_stock_already_created,
                                                                'is_reduce'                 => $material_preparation->is_reduce
                                                            ]);
                                                            
                                                            $is_old_movement_receive_exists = MaterialMovement::where([
                                                                ['from_location',$supplier_location->id],
                                                                ['to_destination',$receiving_location->id],
                                                                ['from_locator_erp_id',$supplier_location->area->erp_id],
                                                                ['to_locator_erp_id',$receiving_location->area->erp_id],
                                                                ['po_buyer',$old_material_preparation->po_buyer],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                                ['no_packing_list',$no_packing_list],
                                                                ['no_invoice',$no_invoice],
                                                                ['status','receive'],
                                                            ])
                                                            ->first();
        
                                                            if(!$is_old_movement_receive_exists)
                                                            {
                                                                $old_material_receive_movement = MaterialMovement::firstorcreate([
                                                                    'from_location'         => $supplier_location->id,
                                                                    'to_destination'        => $receiving_location->id,
                                                                    'from_locator_erp_id'   => $supplier_location->area->erp_id,
                                                                    'to_locator_erp_id'     => $receiving_location->area->erp_id,
                                                                    'is_integrate'          => false,
                                                                    'is_active'             => true,
                                                                    'po_buyer'              => $old_material_preparation->po_buyer,
                                                                    'status'                => 'receive',
                                                                    'no_packing_list'       => $no_packing_list,
                                                                    'no_invoice'            => $no_invoice,
                                                                    'created_at'            => $old_material_preparation->last_movement_date,
                                                                    'updated_at'            => $old_material_preparation->last_movement_date,
                                    
                                                                ]);
        
                                                                $old_material_receive_movement_id = $old_material_receive_movement->id;
                                                            }else
                                                            {
                                                                $is_old_movement_receive_exists->updated_at = $confirm_date;
                                                                $is_old_movement_receive_exists->save();
        
                                                                $old_material_receive_movement_id = $is_old_movement_receive_exists->id;
                                                            }
        
                                                            $is_old_material_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                            ->where([
                                                                ['material_movement_id',$old_material_receive_movement_id],
                                                                ['material_preparation_id',$old_material_preparation->id],
                                                                ['qty_movement',$old_material_preparation->qty_conversion],
                                                                ['date_movement',$old_material_preparation->last_movement_date],
                                                                ['item_id',$old_material_preparation->item_id],
                                                                ['warehouse_id',$old_material_preparation->warehouse],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                            ])
                                                            ->exists();
                        
                                                            if(!$is_old_material_movement_line_exists)
                                                            {
                                                                $new_old_material_movement_line = MaterialMovementLine::Create([
                                                                    'material_movement_id'          => $old_material_receive_movement_id,
                                                                    'material_preparation_id'       => $old_material_preparation->id,
                                                                    'item_code'                     => $old_material_preparation->item_code,
                                                                    'item_id'                       => $old_material_preparation->item_id,
                                                                    'c_order_id'                    => $old_material_preparation->c_order_id,
                                                                    'c_orderline_id'                => $c_orderline_id,
                                                                    'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                                    'supplier_name'                 => $old_material_preparation->supplier_name,
                                                                    'type_po'                       => 2,
                                                                    'is_integrate'                  => false,
                                                                    'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                                    'qty_movement'                  => $old_material_preparation->qty_conversion,
                                                                    'date_movement'                 => $old_material_preparation->last_movement_date,
                                                                    'created_at'                    => $old_material_preparation->last_movement_date,
                                                                    'updated_at'                    => $old_material_preparation->last_movement_date,
                                                                    'date_receive_on_destination'   => $old_material_preparation->last_movement_date,
                                                                    'nomor_roll'                    => '-',
                                                                    'warehouse_id'                  => $old_material_preparation->warehouse,
                                                                    'document_no'                   => $old_material_preparation->document_no,
                                                                    'note'                          => 'PEMOTONGAN STOCK KARENA BARANG SUDAH DATANG',
                                                                    'is_active'                     => true,
                                                                    'user_id'                       => $old_material_preparation->last_user_movement_id,
                                                                ]);
                        
                                                                $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_line->id;
                                                                $old_material_preparation->save();
                                                            }
        
                                                            $is_old_movement_reroute_exists = MaterialMovement::where([
                                                                ['from_location',$receiving_location->id],
                                                                ['to_destination',$reroute_destination->id],
                                                                ['from_locator_erp_id',$receiving_location->area->erp_id],
                                                                ['to_locator_erp_id',$reroute_destination->area->erp_id],
                                                                ['po_buyer',$old_material_preparation->po_buyer],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                                ['no_packing_list',$no_packing_list],
                                                                ['no_invoice',$no_invoice],
                                                                ['status','reroute'],
                                                            ])
                                                            ->first();
        
                                                            if(!$is_old_movement_reroute_exists)
                                                            {
                                                                $old_movement_reroute = MaterialMovement::firstorcreate([
                                                                    'from_location'         => $receiving_location->id,
                                                                    'to_destination'        => $reroute_destination->id,
                                                                    'from_locator_erp_id'   => $receiving_location->area->erp_id,
                                                                    'to_locator_erp_id'     => $reroute_destination->area->erp_id,
                                                                    'is_integrate'          => false,
                                                                    'is_active'             => true,
                                                                    'po_buyer'              => $old_material_preparation->po_buyer,
                                                                    'status'                => 'reroute',
                                                                    'no_packing_list'       => $no_packing_list,
                                                                    'no_invoice'            => $no_invoice,
                                                                    'created_at'            => $reroute_date,
                                                                    'updated_at'            => $reroute_date,
                                    
                                                                ]);
        
                                                                $old_movement_reroute_id = $old_movement_reroute->id;
                                                            }else
                                                            {
                                                                $is_old_movement_reroute_exists->updated_at = $reroute_date;
                                                                $is_old_movement_reroute_exists->save();
        
                                                                $old_movement_reroute_id = $is_old_movement_reroute_exists->id;
                                                            }
        
                                                            $is_old_material_movement_reroute_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                                            ->where([
                                                                ['material_movement_id',$old_movement_reroute_id],
                                                                ['material_preparation_id',$old_material_preparation->id],
                                                                ['qty_movement',$old_material_preparation->qty_conversion],
                                                                ['item_id',$old_material_preparation->item_id],
                                                                ['warehouse_id',$old_material_preparation->warehouse],
                                                                ['date_movement',$reroute_date],
                                                                ['is_integrate',false],
                                                                ['is_active',true],
                                                            ])
                                                            ->exists();
                        
                                                            if(!$is_old_material_movement_reroute_line_exists)
                                                            {
                                                                $new_old_material_movement_reroute_line = MaterialMovementLine::Create([
                                                                    'material_movement_id'          => $old_movement_reroute_id,
                                                                    'material_preparation_id'       => $old_material_preparation->id,
                                                                    'item_code'                     => $old_material_preparation->item_code,
                                                                    'item_id'                       => $old_material_preparation->item_id,
                                                                    'c_order_id'                    => $old_material_preparation->c_order_id,
                                                                    'c_orderline_id'                => $c_orderline_id,
                                                                    'c_bpartner_id'                 => $old_material_preparation->c_bpartner_id,
                                                                    'supplier_name'                 => $old_material_preparation->supplier_name,
                                                                    'type_po'                       => 2,
                                                                    'is_integrate'                  => false,
                                                                    'uom_movement'                  => $old_material_preparation->uom_conversion,
                                                                    'qty_movement'                  => $old_material_preparation->qty_conversion,
                                                                    'date_movement'                 => $reroute_date,
                                                                    'created_at'                    => $reroute_date,
                                                                    'updated_at'                    => $reroute_date,
                                                                    'date_receive_on_destination'   => $reroute_date,
                                                                    'nomor_roll'                    => '-',
                                                                    'warehouse_id'                  => $old_material_preparation->warehouse,
                                                                    'document_no'                   => $old_material_preparation->document_no,
                                                                    'note'                          => 'REROUTE KE PO BUYER '.$material_preparation->po_buyer,
                                                                    'is_active'                     => true,
                                                                    'user_id'                       => $old_material_preparation->last_user_movement_id,
                                                                ]);
        
                                                                $old_material_preparation->last_material_movement_line_id = $new_old_material_movement_reroute_line->id;
                                                                $old_material_preparation->save();
                                                            }
                
                                                            DetailMaterialPreparation::FirstOrCreate([
                                                                'material_preparation_id'   => $old_material_preparation->id,
                                                                'material_arrival_id'       => $material_arrival_id,
                                                                'user_id'                   => $old_material_preparation->user_id
                                                            ]);
        
                                                            $concatenate .= "'" .$old_material_preparation->barcode."',";
                                                        }
                                                    }
                
                                                    $concatenate .= "'" .$material_preparation->barcode."',";
        
                                                    $qty_supply         -= $_supplied;
                                                    $qty_required       -= $_supplied;
                                                    $qty_outstanding    -= $_supplied;
                                                    $qty_allocated      += $_supplied;
        
                                                    $old = $available_qty;
                                                    $new = $available_qty - $_supplied;
        
                                                    if($new <= 0) $new = '0';
                                                    else $new = $new;
        
                                                    HistoryStock::approved($material_stock_id
                                                    ,$new
                                                    ,$old
                                                    ,$__supplied
                                                    ,$po_buyer
                                                    ,$style
                                                    ,$article_no
                                                    ,$lc_date
                                                    ,$allocation_source
                                                    ,$username
                                                    ,$user
                                                    ,$type_stock_erp_code
                                                    ,$type_stock
                                                    ,$is_integrate,
                                                    true);
        
                                                    $available_qty      -= $_supplied;
                                                    $reserved_qty       += $_supplied;
                                                    $is_bom_exists++;
                                                    
                                                }
                                            }
                                        }
                                    }
                                    
                
                                    if($qty_outstanding<=0)
                                    {
                                        $auto_allocation->is_already_generate_form_booking  = true;
                                        $auto_allocation->generate_form_booking             = carbon::now();
                                    }
                                    
                                    $recalculate[]                                          = $auto_allocation->id;
                                    $auto_allocation->qty_outstanding                       = sprintf('%0.8f',$qty_outstanding);
                                    $auto_allocation->qty_allocated                         = sprintf('%0.8f',$qty_allocated);
                                    $auto_allocation->save();
                                }
                            }

                            $auto_allocation->locked_date = null;
                            $auto_allocation->save();
                        }

                        DB::commit();
                    } catch (Exception $e) 
                    {
                        DB::rollBack();
                        $message = $e->getMessage();
                        ErrorHandler::db($message);
                    }
                }
                
            }

            if($is_bom_exists > 0)
            {
                if($available_qty <=0 )
                {
                    $material_stock->is_allocated       = true;
                    $material_stock->is_active          = false;
                    $locator                            = Locator::find($material_stock->locator_id);
                    
                    if($locator)
                    {
                        $counter                        = $locator->counter_in;
                        $locator->counter_in            = $counter -1 ;
                        $locator->save();
                    }

                }
                
                $material_stock->available_qty          = sprintf('%0.8f',$available_qty);
                $material_stock->reserved_qty           = sprintf('%0.8f',$reserved_qty);
            
            }

            $material_stock->last_status                = null;
            $material_stock->save();
            AccessoriesReportMaterialStockController::doRecalculate($material_stock->id);

            $concatenate = substr_replace($concatenate, '', -1);
            if($concatenate !='')
            {
                DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
                DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
            }

            //recalculate
            foreach ($recalculate as $key => $value) 
            {
                $auto_allocation        = AutoAllocation::find($value);

                if(!$auto_allocation->is_reduce && $auto_allocation->status_po_buyer =='active')
                {
                    if($auto_allocation->is_fabric)
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);

                        $qty_allocated  = AllocationItem::where([
                            ['auto_allocation_id',$value],
                        ])
                        ->sum('qty_booking');

                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));

                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);

                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();

                    }else 
                    {
                        $qty_allocation         = sprintf('%0.8f',$auto_allocation->qty_allocation);

                        $qty_allocated  = MaterialPreparation::where([
                            ['auto_allocation_id',$value],
                            //['last_status_movement','!=','out-handover'],
                            ['last_status_movement','!=','reroute'],
                            ['last_status_movement','!=','adjustment'],
                            ['is_from_handover',false],
                        ])
                        ->sum(db::raw("(qty_conversion + COALESCE(qty_borrow,0) + COALESCE(qty_reject,0))"));

                        $new_oustanding = sprintf('%0.8f',$qty_allocation - ($qty_allocated));

                        if($new_oustanding <= 0)
                        {
                            $auto_allocation->is_already_generate_form_booking  = true;
                            $auto_allocation->generate_form_booking             = carbon::now();
                        }else
                        {
                            $auto_allocation->is_already_generate_form_booking  = false;
                            $auto_allocation->generate_form_booking             = null;
                        }
                    
                        if ($new_oustanding == 0) $new_oustanding = '0';
                        else $new_oustanding = sprintf('%0.8f',$new_oustanding);

                        $auto_allocation->qty_allocated     = sprintf('%0.8f',($qty_allocated));
                        $auto_allocation->qty_outstanding   = sprintf('%0.8f',$new_oustanding);
                        $auto_allocation->save();

                        
                    }
                }
                
            }
        }
    }
}
