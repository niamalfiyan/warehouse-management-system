<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\MasterDataFabricTesting;
use App\Models\DetailMasterDataFabricTesting;
use App\Models\Lab\MasterTrfTesting;
use App\Models\Lab\MasterTrfTestingLab;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;

use App\Models\Lab\TrfTesting;
use App\Models\Lab\TrfTestingDocument;
use App\Models\Lab\TrfTestingMethods;

class FabricReportOverviewTestingLabController extends Controller
{
    public function index()
    {
        return view('fabric_report_overview_testing_lab.index'); 
    }

    public function data(Request $request)
    {
        if ($request->ajax())
        {

            $fabric_testing = TrfTesting::whereNull('deleted_at')
            ->wherein('status', ['VERIFIED','CLOSED','ONPROGRESS'])
            ->where('platform', 'WMS')
            ->orderBy('created_at','desc')->get();
            // dd($fabric_testing);

            return DataTables::of($fabric_testing)
            ->editColumn('nik',function ($fabric_testing)
            {
                $users = User::where('nik', $fabric_testing->nik)->first();
                return $users->name;
            })
            ->editColumn('lab_location',function ($fabric_testing)
            {
                if($fabric_testing->lab_location == 'AOI1') return 'AOI 1';
                elseif($fabric_testing->lab_location == 'AOI2') return 'AOI 2';
                else return '-';
            })
            ->editColumn('date_information_remark',function ($fabric_testing)
            {
                if($fabric_testing->date_information_remark == 'buy_ready') return 'Buy Ready - '. $fabric_testing->date_information ;
                elseif($fabric_testing->date_information_remark == 'output_sewing') return '1st Output Sewing - '. $fabric_testing->date_information ;
                else return 'PODD - '. $fabric_testing->date_information ;
            })
            ->editColumn('test_required',function ($fabric_testing)
            {
                if($fabric_testing->test_required == 'developtesting_t1') return 'Develop Testing T1';
                elseif($fabric_testing->test_required == 'developtesting_t2') return 'Develop Testing T2';
                elseif($fabric_testing->test_required == 'developtesting_selective') return 'Develop Testing Selective';
                elseif($fabric_testing->test_required == 'bulktesting_m') return '1st Bulk Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'bulktesting_a') return '1st Bulk Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'bulktesting_selective') return '1st Bulk Testing Selective';
                elseif($fabric_testing->test_required == 'reordertesting_m') return 'Re-Order Testing M (Model Level)';
                elseif($fabric_testing->test_required == 'reordertesting_a') return 'Re-Order Testing A (Article Level)';
                elseif($fabric_testing->test_required == 'reordertesting_selective') return 'Re-Order Testing Selective';
                else return 'Re-Test';
            })
            // ->editColumn('testing_method_id',function ($fabric_testing)
            // {
            //     $lab = DB::connection('lab');

            //     $testings = explode(",",$fabric_testing->testing_method_id);
                
            //     $master_method = $lab->table('master_method')
            //     ->select('master_method.method_name')
            //     ->wherein('method_code', $testings)
            //     ->get();
            //     $methods = $master_method->implode('method_name', ', ');
            //      //   dd($methods);
            //     return $methods; 
            
            // })
            ->addColumn('orderno',function ($fabric_testing)
            {
                $trf_document = TrfTestingDocument::where('trf_id', $fabric_testing->trf_id)->first();
                if($trf_document->document_type == "PO SUPPLIER")
                {
                    return '
                    <b>Barcode</b> : '.$trf_document->barcode_supplier.'<br>
                    <b>PO Supplier</b> : '.$trf_document->document_no.'<br>
                    <b>Supplier Name </b>    : '.$trf_document->manufacture_name.'<br>
                    <b>Item</b>     : '.$trf_document->item.'<br>
                    <b>Color</b>     : '.$trf_document->item.'<br>
                    <b>Composition</b>     : '.$trf_document->fibre_composition.'<br>
                    <b>No Roll</b>     : '.$trf_document->nomor_roll.'<br>
                    <b>Batch</b>     : '.$trf_document->batch_number.'<br>     
                    ';
                }else if($trf_document->document_type == "PO BUYER")
                {
                    return '
                    <b>PO Buyer</b> : '.$trf_document->document_no.'<br>
                    <b>Item</b>     : '.$trf_document->item.'<br>
                    <b>Composition</b>     : '.$trf_document->fibre_composition.'<br>
                    <b>Size</b> : '.$trf_document->size.'<br>
                    <b>Style</b> : '.$trf_document->style.'<br>
                    <b>Article</b> : '.$trf_document->article_no.'<br>
                    <b>Export To</b> : '.$trf_document->export_to.'<br>
                    ';
                }
            })
            ->editColumn('t2_result', function($fabric_testing){
                return $fabric_testing->t2_result;
            })
            ->editColumn('t1_result', function($fabric_testing){
                $lab = DB::connection('lab');
                $checkResult  = $lab->table('result_testings')->where('master_trf_testing_id', $fabric_testing->id)->get();
                // dd($checkResult);
                return $fabric_testing->t2_result;
            })
            ->editColumn('return_test_sample',function ($fabric_testing)
            {
                if($fabric_testing->return_test_sample == 't')
                {
                    return '<span class="label bg-success">YES</span>';
                }else {
                    return '<span class="label bg-danger">NO</span>';
              
                }
            
            })
            ->editColumn('verified_lab_date',function ($fabric_testing)
            {
                
                if($fabric_testing->verified_lab_date == null) 
                {
                    return '<span class="label bg-grey-400">NOT YET</span>';
                }
                else{
       
                    $tgl = date_diff(date_create($fabric_testing->created_at),date_create($fabric_testing->verified_lab_date));
                    if($tgl->d == 0) return '<span class="label bg-success-400">ON TIME</span>';
                    elseif($tgl->d == 1)return '<span class="label bg-blue">LATE!</span>';
                    elseif($tgl->d > 1)return '<span class="label bg-danger">VERY LATE!</span>';
                    else 
                    return '-';

                }
               
        
            })
            ->editColumn('status',function ($fabric_testing)
            {
               
                $absensi = DB::connection('absence_aoi');
               
                if($fabric_testing->status == 'OPEN')
                {
                    return '<span class="label bg-grey-400">open</span>';
                } 
                elseif($fabric_testing->status == 'VERIFIED') 
                {
                    $user_verified = $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->verified_lab_by)->first();
                    return '<span class="label bg-blue">verified by '.$user_verified->name.'</span>';
                }
                
                elseif($fabric_testing->status == 'REJECT')
                {
                    $user_rejected =  $absensi->table('adt_bbigroup_emp_new')->where('nik', $fabric_testing->reject_by)->first();

                    return '<span class="label bg-danger">rejected by '.$user_rejected->name.'</span>';
                }
               
                elseif($fabric_testing->status == 'ONPROGRESS') 
                {
                    return '<span class="label bg-success-400">onprogress</span>';
                }
                elseif($fabric_testing->status == 'CLOSED') 
                {
                    return '<span class="label bg-danger">closed</span>';
                }
                else
                {
                    return 'eror';
                }





            })
            ->addColumn('action', function($fabric_testing) {
                return view('master_data_fabric_testing._action', [
                    'model'         => $fabric_testing,
                    // 'edit'          => route('masterDataFabricTesting.edit',$fabric_testing->id),
                    // 'print_notrf'   => route('masterDataFabricTesting.barcode', $fabric_testing->id),
                    // 'print_detail'   => route('masterDataFabricTesting.print_detail', $fabric_testing->id),
                ]);
            })
            ->rawColumns(['status','action','verified_lab_date','orderno','return_test_sample'])
            ->make(true);
        }
    }

    function statusTime($date1, $date2, $format = false) 
    {
	$diff = date_diff( date_create($date1), date_create($date2) );
	if ($format)
		return $diff->format($format);
	
	return array('y' => $diff->y,
				'm' => $diff->m,
				'd' => $diff->d,
				'h' => $diff->h,
				'i' => $diff->i,
				's' => $diff->s
			);
    }
}
