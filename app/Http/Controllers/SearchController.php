<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Config;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Models\MaterialCheck;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\DetailMaterialPreparation;

class SearchController extends Controller
{
    public function index()
    {
        return view('search.index');
    }

    public function detail(Request $request)
    {
        $barcode                = ($request->barcode)? $request->barcode : $request->po_buyer_id;
        $material_preparation   = MaterialPreparation::where('barcode',strtoupper($barcode))->first();
        
        
        $movement_history = MaterialMovementLine::whereIn('material_preparation_id',function($query) use($barcode)
        {
            $query->select('id')
            ->from('material_preparations')
            ->where('barcode',$barcode);
        })
        ->orderby('date_movement','asc')
        ->get();

        $material_check = MaterialCheck::whereIn('material_preparation_id',function($query) use($barcode)
        {
            $query->select('id')
            ->from('material_preparations')
            ->where([
                ['barcode',$barcode],
                ['warehouse',auth::user()->warehouse]
            ]);
        })
        ->whereNull('deleted_at')
        ->first();

        $array = array();
        foreach ($movement_history as $key => $value) 
        {
            if($value->movement->status == 'cancel item') $_status = 'cancel item';
            elseif($value->movement->status == 'print') $_status = 'cetak barcode';
            elseif($value->movement->status == 'out') $_status = 'supplai';
            elseif($value->movement->status == 'cancel order') $_status = 'cancel order';
            elseif($value->movement->status == 'hold') $_status = 'ditahan untuk dilakukan pengecekan oleh bag. qc';
            elseif($value->movement->status == 'in') $_status = 'ready prepare';
            elseif($value->movement->status == 'change') $_status = 'pindah rak';
            elseif($value->movement->status == 'reroute') $_status = 'berubah po buyer';
            elseif($value->movement->status == 'adjusment') $_status = 'penyesuaian qty';
            elseif($value->movement->status == 'reject') $_status = 'reject';
            elseif($value->movement->status == 'check') $_status = 'pengecekan qc';
            elseif($value->movement->status == 'receive') $_status = 'penerimaan';
            else $_status = $value->movement->status;

            $obj                    = new StdClass();
            $obj->status            = strtoupper($_status);
            $obj->from_location     = $value->movement->fromLocator->code;
            $obj->to_destination    = $value->movement->destinationLocator->code;
            $obj->user_name         = $value->user->name;
            $obj->movement_date         = $value->date_movement->format('d/m/Y H:i:s');
            $array [] = $obj;
        }

        $obj                    = new stdClass();
        $obj->id                = $material_preparation->id;
        $obj->item_code         = $material_preparation->item_code;
        $obj->item_desc         = $material_preparation->item_desc;
        $obj->category          = $material_preparation->category;
        $obj->po_buyer          = $material_preparation->po_buyer;
        $obj->document_no       = $material_preparation->document_no;
        $obj->article_no        = $material_preparation->article_no;
        $obj->qc_status         = ($material_check)? $material_check->status : null ;
        $obj->last_qc_date      = ($material_check)? $material_check->created_at->format('d/m/Y H:i:s') : null;
        $obj->style             = $material_preparation->style;
        $obj->job_order         = $material_preparation->job_order;
        $obj->article_no        = $material_preparation->article_no;
        $obj->qc_status         = $material_preparation->qc_status;
        $obj->movement_history  = $array;
        $obj->url_reprint_barcode  = route('search.print',$material_preparation->id);
        
        return response()->json($obj,200);
    }

    public function poBuyerPicklist(Request $request)
    {
        $po_buyer   = $request->po_buyer;
        $item_code  = $request->item_code;
        $lists      = DB::table('search_picklist_v')
        ->where('po_buyer','LIKE',"%$po_buyer%")
        ->Where('item_code','LIKE',"%$item_code%")
        ->paginate(10);
        
        return view('search._po_buyer_picklist',compact('lists'));
    }


    public function print($id)
    {
        $items = MaterialPreparation::where('id',$id)->get();

        foreach ($items as $key => $material_preparation) 
        {
            if($material_preparation->last_status_movement =='out' 
            || $material_preparation->last_status_movement =='cancel order'
                || $material_preparation->last_status_movement =='out-cutting' 
                || $material_preparation->last_status_movement =='out-subcont'
                || $material_preparation->last_status_movement =='reroute' 
                || $material_preparation->last_status_movement =='cancel item' 
                || $material_preparation->last_status_movement =='cancel backlog'
                || $material_preparation->last_status_movement =='delete' 
                || $material_preparation->last_status_movement =='reject' 
            ) return response()->json('barcode ini sudah di supplai',422);
    
            if($material_preparation->qc_status == 'FULL REJECT') return response()->json('barcode sudah tidak digunakan karna barcode ini sudah di reject qc',422);
    
            $reprint_counter                        = $material_preparation->reprint_counter;
            $material_preparation->reprint_counter  = $reprint_counter+1;
            $material_preparation->reprint_date     = carbon::now();
            $material_preparation->updated_at       = carbon::now();
            $material_preparation->reprint_user_id  = auth::user()->id;
            $material_preparation->save();
        }
       

        return view('search.barcode', compact('items'));

    }
}
