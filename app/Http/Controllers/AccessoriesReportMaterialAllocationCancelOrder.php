<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;


class AccessoriesReportMaterialAllocationCancelOrder extends Controller
{
    public function index()
    {
        return view('accessories_report_material_allocation_cancel_order.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $start_date         = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null);
            $end_date           = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : null);

            $material_cancel_order = db::table('accessories_report_allocation_cancel_order')
            ->whereBetween('lc_date', [$start_date, $end_date])
            ->orderby('lc_date','desc')
            ->take(100);


            return DataTables::of($material_cancel_order)
            ->editColumn('stock',function ($material_cancel_order)
            {
                return number_format($material_cancel_order->stock, 4, '.', ',');
            })
            ->editColumn('reserved_qty',function ($material_cancel_order)
            {
                return number_format($material_cancel_order->reserved_qty, 4, '.', ',');
            })
            ->editColumn('available_qty',function ($material_cancel_order)
            {
                return number_format($material_cancel_order->available_qty, 4, '.', ',');
            })
            ->editColumn('qty',function ($material_cancel_order)
            {
                return number_format($material_cancel_order->qty, 4, '.', ',');
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {
        $start_date         = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : null);
        $end_date           = ( $request->start_date ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : null);
        
        $material_cancel_order = db::table('accessories_report_allocation_cancel_order')
            ->whereBetween('lc_date', [$start_date, $end_date])
            ->orderby('lc_date','desc')
            ->get();

        $file_name = 'accessories_report_allocation_cancel_order lc date'.$start_date.'-'.$end_date;
        return Excel::create($file_name,function($excel) use ($material_cancel_order)
        {
            $excel->sheet('active',function($sheet)use($material_cancel_order)
            {
                $sheet->setCellValue('A1','MATERIAL_STOCK_ID');
                $sheet->setCellValue('B1','PO_BUYER_OLD');
                $sheet->setCellValue('C1','LC_DATE');
                $sheet->setCellValue('D1','DOCUMENT_NO');
                $sheet->setCellValue('E1','ITEM_CODE');
                $sheet->setCellValue('F1','STOCK');
                $sheet->setCellValue('G1','RESERVED_QTY');
                $sheet->setCellValue('H1','AVAILABLE_QTY');
                $sheet->setCellValue('I1','CREATED_AT');
                $sheet->setCellValue('J1','WAREHOUSE_STOCK');
                $sheet->setCellValue('K1','AUTO_ALLOCATION_ID');
                $sheet->setCellValue('L1','BARCODE');
                $sheet->setCellValue('M1','PO_BUYER_NEW');
                $sheet->setCellValue('N1','QTY');
                $sheet->setCellValue('O1','DATE_ALOKASI');
                $sheet->setCellValue('P1','WAREHOUSE_ALOKASI');
                $sheet->setCellValue('Q1','LAST_STATUS_MOVEMENT');

            $row=2;
            foreach ($material_cancel_order as $i) 
            {  
                $sheet->setCellValue('A'.$row,$i->material_stock_id);
                $sheet->setCellValue('B'.$row,$i->po_buyer_old);
                $sheet->setCellValue('C'.$row,$i->lc_date);
                $sheet->setCellValue('D'.$row,$i->document_no);
                $sheet->setCellValue('E'.$row,$i->item_code);
                $sheet->setCellValue('F'.$row,$i->stock);
                $sheet->setCellValue('G'.$row,$i->reserved_qty);
                $sheet->setCellValue('H'.$row,$i->available_qty);
                $sheet->setCellValue('I'.$row,$i->date_stock);
                $sheet->setCellValue('J'.$row,$i->warehouse_stock);
                $sheet->setCellValue('K'.$row,$i->allocation_id);
                $sheet->setCellValue('L'.$row,$i->barcode);
                $sheet->setCellValue('M'.$row,$i->po_buyer_new);
                $sheet->setCellValue('N'.$row,$i->qty);
                $sheet->setCellValue('O'.$row,$i->date_alokasi);
                $sheet->setCellValue('P'.$row,$i->warehouse_alokasi);
                $sheet->setCellValue('Q'.$row,$i->last_status_movement);
                $row++;
            }
            });

        })
        ->export('xlsx');

    }

}
