<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FabricPrintSuratJalanController extends Controller
{
    public function index()
    {
        return view('fabric_print_surat_jalan.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax()) 
        {
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y', $request->start_date)->format('Y-m-d') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y', $request->end_date)->format('Y-m-d') : Carbon::now();

            
            $list_surat_jalans     = DB::table('fabric_print_surat_jalan')
            ->where('warehouse_id','LIKE',"%$warehouse_id%")
            ->whereBetween('tanggal_kirim',[$start_date,$end_date])
            ->orderby('tanggal_kirim','desc');
            
            return DataTables::of($list_surat_jalans)
            ->editColumn('warehouse_id',function ($list_surat_jalans)
            {
                if($list_surat_jalans->warehouse_id ='1000011')
                {
                    return  'AOI 2';
                }
                else
                {
                    return  'AOI 1';
                }
            })
            ->editColumn('tanggal_kirim',function ($list_surat_jalans)
            {
                return  Carbon::createFromFormat('Y-m-d', $list_surat_jalans->tanggal_kirim)->format('d/M/Y');
            })
            ->addColumn('action',function ($list_surat_jalans)
            {
                return view('fabric_print_surat_jalan._action',[
                    'model' => $list_surat_jalans,
                    'print' => route('fabricPrintSuratJalan.print', ['nomor_surat_jalan' => $list_surat_jalans->no_surat_jalan, 'warehouse_id' => $list_surat_jalans->warehouse_id, 'tanggal_kirim' => $list_surat_jalans->tanggal_kirim])
                ]);
            })
            ->make(true);
        }
    }

    public function print(Request $request)
    {
        $nomor_surat_jalan = $request->nomor_surat_jalan;
        $warehouse_id = $request->warehouse_id;
        $tanggal_kirim = $request->tanggal_kirim;
        //dd($nomor_surat_jalan);
        $prints = DB::table('detail_surat_jalan_v')
                  ->where('no_surat_jalan', $nomor_surat_jalan)
                  ->get();

        //dd($prints);
        return view('fabric_print_surat_jalan.print', compact('prints','nomor_surat_jalan', 'warehouse_id','tanggal_kirim'));
    }
}
