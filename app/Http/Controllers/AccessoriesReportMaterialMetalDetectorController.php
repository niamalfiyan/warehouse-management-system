<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use Config;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AccessoriesReportMaterialMetalDetectorController extends Controller
{
    public function index()
    {
        return view('accessories_report_material_metal_detector.index');
    }

    public function data(Request $request)
    {
        if(request()->ajax())
        {
            $status             = $request->status;
            $warehouse_id       = $request->warehouse;
            $start_date         = ($request->start_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->start_date.'00:00:00')->format('Y-m-d H:i:s') : Carbon::today()->subDays(30);
            $end_date           = ($request->end_date) ? Carbon::createFromFormat('d/m/Y H:i:s', $request->end_date.'23:59:59')->format('Y-m-d H:i:s') : Carbon::now();

            $material_checks = db::table('material_metal_detector')
            ->where('warehouse', $warehouse_id)
            ->whereBetween('inspection_date', [$start_date, $end_date])
            ->orderby('inspection_date','desc');

            //dd($warehouse_id, $start_date, $end_date, $material_checks->get()->toArray());
            if($status) $material_checks = $material_checks->where('status',$status);

            return DataTables::of($material_checks)
            ->editColumn('qty_ordered',function ($material_checks)
            {
                if($material_checks->qty_order != 0) return number_format($material_checks->qty_order, 4, '.', ',');
            })
            ->editColumn('qty_release',function ($material_checks)
            {
                return number_format($material_checks->qty_release, 4, '.', ',');
            })
            ->editColumn('qty_reject',function ($material_checks)
            {
                return number_format($material_checks->qty_reject, 4, '.', ',');
            })
            ->editColumn('inspection_date',function ($material_checks)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_checks->inspection_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('arrival_date',function ($material_checks)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $material_checks->arrival_date)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse',function ($material_checks)
            {
                if($material_checks->warehouse == '1000002') return 'Warehouse accessories AOI 1';
                elseif($material_checks->warehouse == '1000013') return 'Warehouse accessories AOI 2';
            })
            ->make(true);
        }
    }

    public function export(Request $request)
    {

        if(auth::user()->warehouse == '1000013') $filename = 'report_md_aoi2.csv';
        else if(auth::user()->warehouse == '1000002') $filename = 'report_md_aoi1.csv';
        
        $file = Config::get('storage.report') . '/' . $filename;

        if(!file_exists($file)) return 'file yang anda cari tidak ditemukan';

        $resp = response()->download($file);
        $resp->headers->set('Pragma', 'no-cache');
        $resp->headers->set('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate');
        $resp->headers->set('X-Content-Type-Options', 'nosniff');
        return $resp;
    }
}
