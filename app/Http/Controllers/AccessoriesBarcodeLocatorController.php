<?php namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;

use App\Models\Area;
use App\Models\Locator;

class AccessoriesBarcodeLocatorController extends Controller
{
    public function index()
    {
        $areas = Area::where([
            ['is_active','=',true],
            ['warehouse','=',auth::user()->warehouse],
            ['name','<>','RECEIVING'],
            ['name','<>','ADJUSTMENT'],
            ['name','<>','GENERAL'],
            ['is_destination','=',false],
        ])
        ->paginate(10);
        
        return view('accessories_barcode_locator.index',compact('areas'));
    }

    public function printOut(request $request,$area,$rack)
    {
        $locators = Locator::select('barcode','rack','y_row','z_column','area_id')
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc');

        if($area != 'null')  $locators = $locators->where('area_id',$area);

        if($rack != 'null') $locators = $locators->where('rack',$rack);

        $locators = $locators->where('is_active',true)
        ->get();

       return view('accessories_barcode_locator.barcode', compact('locators','rack'));
    }
}
