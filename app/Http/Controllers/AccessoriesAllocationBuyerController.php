<?php namespace App\Http\Controllers;

use DB;
use Auth;
use Excel;
use StdClass;
use Validator;
use Carbon\Carbon;
use Webpatser\Uuid\Uuid;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Yajra\Datatables\Html\Builder;
use Illuminate\Support\Facades\Session;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\Locator;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\MaterialCheck;
use App\Models\AllocationItem;
use App\Models\AutoAllocation;
use App\Models\ReroutePoBuyer;
use App\Models\MaterialBacklog;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialStock;
use App\Models\HistoryAutoAllocations;
use App\Models\DetailMaterialPreparation;


use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\MasterDataAutoAllocationController as MasterAutoAllocation;

class AccessoriesAllocationBuyerController extends Controller
{

    
    public function __construct()
    {
        // parent::__construct();
        ini_set('max_execution_time', '3000');
    }
    

    public function index()
    {
        
        $flag = Session::get('flag');
        return view('accessories_allocation_buyer.index',compact('flag'));
    }

    public function data(Request $request)
    {

        if(request()->ajax()) 
        {
            $warehouse_id   = $request->warehouse;
            $data           = db::table('material_allocation_preparations_v')->where('warehouse','LIKE',"%$warehouse_id%")
            ->orderby('created_at','desc');


            return DataTables::of($data)
            ->editColumn('barcode',function ($data)
            {
                if($data->barcode == 'BELUM DI PRINT')return '<span class="label label-default">Not Printed</span>';
                else return '<span class="label label-success">Printed [NO SERI: '.$data->barcode.']</span>';
            })
            ->editColumn('created_at',function ($data)
            {
                return  Carbon::createFromFormat('Y-m-d H:i:s', $data->created_at)->format('d/M/Y H:i:s');
            })
            ->editColumn('warehouse',function ($data)
            {
                if($data->warehouse == '1000002') return 'Warehouse Accessories AOI 1';
                else return 'Warehouse Accessories AOI 2';
            })
            ->addColumn('qty_booking',function ($data)
            {
                return number_format($data->qty_booking, 4, '.', ',');
            })
            ->editColumn('is_additional',function ($data)
            {
                if ($data->is_additional) return '<div class="checker border-info-600 text-info-800">
                    <span class="checked"><input type="checkbox" class="control-info" checked="checked"></span>
                </div>';
                else return null;
                
            })
            ->addColumn('action', function($data) 
            {
                if($data->barcode == 'BELUM DI PRINT')
                {
                    return view('_action', [
                        'model' => $data,
                        'delete' => route('accessoriesAllocationBuyer.destroy',$data->id)
                    ]);
                }
            })
            ->setRowAttr([
                'style' => function($data) 
                {
                    if($data->status_po_buyer == 'cancel') return  'background-color: #fab1b1;';
                },
            ])
            ->rawColumns(['style','barcode','is_additional','action'])
            ->make(true);
        }
    }

    public function upload()
    {
        
        //return view('errors.503');
        return view('accessories_allocation_buyer.upload');
    }

    public function exportFileUpload()
    {
        return Excel::create('upload_alokasi',function ($excel){
            $excel->sheet('active', function($sheet){
                $sheet->setCellValue('A1','SUPPLIER_CODE');
                $sheet->setCellValue('B1','PO_SUPPLIER_SOURCE');
                $sheet->setCellValue('C1','PO_BUYER_SOURCE');
                $sheet->setCellValue('D1','PO_BUYER_BOOKING');
                $sheet->setCellValue('E1','ITEM_CODE_SOURCE');
                $sheet->setCellValue('F1','ITEM_CODE_BOOK');
                $sheet->setCellValue('G1','CATEGORY');
                $sheet->setCellValue('H1','UOM_BOOKING');
                $sheet->setCellValue('I1','QTY_BOOKING');
                $sheet->setCellValue('J1','IS_ADDITIONAL');
                $sheet->setCellValue('K1','REMARK_ADDITIONAL');
                $sheet->setCellValue('L1','STYLE');

                $sheet->setWidth('A', 15);
                $sheet->setColumnFormat(array(
                    'A' => '@',
                    'B' => '@',
                    'C' => '@',
                    'D' => '@',
                    'E' => '@',
                    'F' => '@',
                    'G' => '@',
                    'H' => '@',
                    'I' => '@'
                ));

            });

            $excel->setActiveSheetIndex(0);



        })->export('xlsx');
    }

    public function import(Request $request)
    {
        //return view('errors.503');
        if($request->hasFile('upload_file'))
        {
            $validator = Validator::make($request->all(), [
                'upload_file' => 'required|mimes:xls'
            ]);

            $array              = array();
            $array_allocations  = array();
            $path               = $request->file('upload_file')->getRealPath();
            $data               = Excel::selectSheets('active')->load($path,function($render){})->get();

            if(!empty($data) && $data->count())
            {
                $warehouse_id   = $request->warehouse_id;
                if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                
                $movement_date = carbon::now();
                try 
                {
                    DB::beginTransaction();

                    foreach ($data as $key => $value) 
                    {
                        $supplier_code          = trim(strtoupper($value->supplier_code));
                        $document_no            = trim(strtoupper($value->po_supplier_source));
                        $item_code_book         = trim(strtoupper($value->item_code_book));
                        $item_code_source       = trim(strtoupper($value->item_code_source)) != null? trim(strtoupper($value->item_code_source)):$item_code_book;
                        $po_buyer_source        = trim(strtoupper($value->po_buyer_source));
                        $old_po_buyer           = trim(strtoupper($value->po_buyer_booking));
                        $uom_booking            = trim(strtoupper($value->uom_booking));
                        $qty_booking            = sprintf('%0.8f',$value->qty_booking);
                        $is_additional          = trim(strtoupper($value->is_additional));
                        $remark_additional      = trim(strtoupper($value->remark_additional));
                        $style                  = trim(strtoupper($value->style));
                        
                        if($supplier_code != null 
                            && $document_no != null 
                            && $item_code_source != null 
                            && $item_code_book != null 
                            && $old_po_buyer != null 
                            && $qty_booking != null)
                        {
                            if($supplier_code == 'FREE STOCK' || $supplier_code == 'FREE STOCK-CELUP')
                            {
                                $c_bpartner_id = 'FREE STOCK';
                                $supplier_name = 'FREE STOCK';
                            }else
                            {
                                $supplier           = Supplier::where('supplier_code',$supplier_code)->first();
                                $c_bpartner_id      = ($supplier ? $supplier->c_bpartner_id : null);
                                $supplier_name      = ($supplier ? $supplier->supplier_name : null);
                            }
                            
                            $is_reroute         = ReroutePoBuyer::where('old_po_buyer',$old_po_buyer)->first();
                            $new_po_buyer       = ($is_reroute) ? $is_reroute->new_po_buyer : $old_po_buyer;
                            
                            if($c_bpartner_id)
                            {
                                $master_po_buyer    = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                                $master_item        = Item::where(db::raw('item_code'),$item_code_book)->first();
                                $item_desc          = ($master_item ? $master_item->item_desc : 'master item not found');
                                $category           = ($master_item ? $master_item->category : 'master item not found');

                                if($master_po_buyer)
                                {
                                    if(!$master_po_buyer->cancel_date)
                                    {
                                        $lc_date                = $master_po_buyer->lc_date;
                                        $season                 = $master_po_buyer->season;
                                        $type_stock_erp_code    = $master_po_buyer->type_stock_erp_code;
                                        $type_stock             = $master_po_buyer->type_stock;
                                        $so_id                  = $master_po_buyer->so_id;

                                        if($master_po_buyer->statistical_date) $promise_date  = $master_po_buyer->statistical_date->format('Y-m-d');
                                        else $promise_date                              = $master_po_buyer->promise_date->format('Y-m-d');

                                            $is_bom_exists = MaterialRequirement::where([
                                                ['po_buyer',$new_po_buyer],
                                                ['item_code',$item_code_book],
                                            ])
                                            ->exists();
                                        
                                       

                                        if($is_additional == 'TRUE' || $is_additional == 'T')
                                        {
                                            if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                                            {
                                                $c_order_id = 'FREE STOCK';
                                            }else
                                            {
                                                $po_supplier  = DB::connection('erp')
                                                ->table('wms_po_supplier')
                                                ->where(db::raw('upper(documentno)'),$document_no)
                                                ->first();

                                                $c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
                                            }

                                            $item_source = DB::connection('erp')
                                            ->table('wms_master_items')
                                            ->where(db::raw('upper(item_code)'),$item_code_source)
                                            ->first();

                                            $item_book = DB::connection('erp')
                                            ->table('wms_master_items')
                                            ->where(db::raw('upper(item_code)'),$item_code_source)
                                            ->first();

                                            $item_id_source = ($item_source ? $item_source->item_id : null);
                                            $item_id_book   = ($item_book ? $item_book->item_id : null);
                                            
                                            $new_allocation = AutoAllocation::Create([
                                                'document_allocation_number'        => null,
                                                'lc_date'                           => $lc_date,
                                                'season'                            => $season,
                                                'document_no'                       => $document_no,
                                                'c_order_id'                        => $c_order_id,
                                                'c_bpartner_id'                     => $c_bpartner_id,
                                                'supplier_name'                     => $supplier_name,
                                                'po_buyer_source'                   => ($po_buyer_source ? $po_buyer_source: null),
                                                'po_buyer'                          => $new_po_buyer,
                                                'old_po_buyer'                      => $old_po_buyer,
                                                'item_code_source'                  => $item_code_source,
                                                'item_id_source'                    => $item_id_source,
                                                'item_code'                         => $item_code_book,
                                                'item_code_book'                    => $item_code_book,
                                                'item_id_book'                      => $item_id_book,
                                                'item_desc'                         => $item_desc,
                                                'category'                          => $category,
                                                'uom'                               => $uom_booking,
                                                'warehouse_name'                    => $warehouse_name,
                                                'warehouse_id'                      => $warehouse_id,
                                                'qty_allocation'                    => $qty_booking,
                                                'qty_outstanding'                   => $qty_booking,
                                                'qty_allocated'                     => 0,
                                                'remark'                            => 'UPLOAD ADDITIONAL MANUAL',
                                                'is_fabric'                         => false,
                                                'type_stock_erp_code'               => $type_stock_erp_code,
                                                'type_stock'                        => $type_stock,
                                                'is_already_generate_form_booking'  => false,
                                                'is_from_menu_allocation_buyer'     => true,
                                                'is_upload_manual'                  => true,
                                                'is_additional'                     => true,
                                                'remark_additional'                 => $remark_additional,
                                                'promise_date'                      => $promise_date,
                                                'status_po_buyer'                   => 'active',
                                                'user_id'                           => auth::user()->id,
                                                'created_at'                        => $movement_date,
                                                'updated_at'                        => $movement_date,
                                                'so_id'                             => $so_id
                                            ]);

                                            $array_allocations [] = $new_allocation->id;
                                            
                                            $obj                    = new stdClass();
                                            $obj->supplier_code     = $supplier_code;
                                            $obj->document_no       = $document_no;
                                            $obj->item_code         = $item_code_book;
                                            $obj->po_buyer_source   = $po_buyer_source;
                                            $obj->po_buyer          = $new_po_buyer;
                                            $obj->uom               = $uom_booking;
                                            $obj->qty_booking       = $qty_booking;
                                            $obj->style             = $style;
                                            $obj->is_error          = false;
                                            $obj->result            = 'SUCCESS';
                                            $array []               = $obj;
                                        }else
                                        {
                                            
                                                $is_exists = AutoAllocation::where([
                                                    ['lc_date',$lc_date],
                                                    ['season',$season],
                                                    [db::raw('upper(document_no)'),$document_no],
                                                    ['c_bpartner_id',$c_bpartner_id],
                                                    ['po_buyer',$new_po_buyer],
                                                    [db::raw('upper(item_code_source)'),$item_code_source],
                                                    [db::raw('upper(item_code)'),$item_code_book],
                                                    ['warehouse_id',$warehouse_id],
                                                    ['qty_allocation',$qty_booking],
                                                    ['is_fabric',false],
                                                    ['is_additional',false],
                                                    ['type_stock',$type_stock],
                                                ])
                                                ->exists();
                                            
                                            

                                            if(!$is_exists)
                                            {
                                                if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                                                {
                                                    $c_order_id = 'FREE STOCK';
                                                }else
                                                {
                                                    $po_supplier  = DB::connection('erp')
                                                    ->table('wms_po_supplier')
                                                    ->where(db::raw('upper(documentno)'),$document_no)
                                                    ->first();

                                                    $c_order_id = ($po_supplier ? $po_supplier->c_order_id : null);
                                                }

                                                $item_source = DB::connection('erp')
                                                ->table('wms_master_items')
                                                ->where(db::raw('upper(item_code)'),$item_code_source)
                                                ->first();

                                                $item_book = DB::connection('erp')
                                                ->table('wms_master_items')
                                                ->where(db::raw('upper(item_code)'),$item_code_source)
                                                ->first();

                                                $item_id_source = ($item_source ? $item_source->item_id : null);
                                                $item_id_book   = ($item_book ? $item_book->item_id : null);
                                            
                                                
                                                $new_allocation = AutoAllocation::firstorcreate([
                                                    'document_allocation_number'        => null,
                                                    'lc_date'                           => $lc_date,
                                                    'season'                            => $season,
                                                    'document_no'                       => $document_no,
                                                    'c_order_id'                        => $c_order_id,
                                                    'c_bpartner_id'                     => $c_bpartner_id,
                                                    'supplier_name'                     => $supplier_name,
                                                    'po_buyer_source'                   => ($po_buyer_source ? $po_buyer_source: null),
                                                    'po_buyer'                          => $new_po_buyer,
                                                    'old_po_buyer'                      => $old_po_buyer,
                                                    'item_code_source'                  => $item_code_source,
                                                    'item_id_source'                    => $item_id_source,
                                                    'item_code'                         => $item_code_book,
                                                    'item_code_book'                    => $item_code_book,
                                                    'item_id_book'                      => $item_id_book,
                                                    'item_desc'                         => $item_desc,
                                                    'category'                          => $category,
                                                    'uom'                               => $uom_booking,
                                                    'warehouse_name'                    => $warehouse_name,
                                                    'warehouse_id'                      => $warehouse_id,
                                                    'qty_allocation'                    => $qty_booking,
                                                    'qty_outstanding'                   => $qty_booking,
                                                    'qty_allocated'                     => 0,
                                                    'remark'                            => 'UPLOAD MANUAL',
                                                    'is_fabric'                         => false,
                                                    'type_stock_erp_code'               => $type_stock_erp_code,
                                                    'type_stock'                        => $type_stock,
                                                    'is_already_generate_form_booking'  => false,
                                                    'is_from_menu_allocation_buyer'     => true,
                                                    'is_upload_manual'                  => true,
                                                    'is_additional'                     => false,
                                                    'promise_date'                      => $promise_date,
                                                    'status_po_buyer'                   => 'active',
                                                    'user_id'                           => auth::user()->id,
                                                    'created_at'                        => $movement_date,
                                                    'updated_at'                        => $movement_date,
                                                    'so_id'                             => $so_id
                                                ]);

                                                $array_allocations [] = $new_allocation->id;
                                            
                                                $obj                    = new stdClass();
                                                $obj->supplier_code     = $supplier_code;
                                                $obj->document_no       = $document_no;
                                                $obj->item_code         = $item_code_book;
                                                $obj->po_buyer_source   = $po_buyer_source;
                                                $obj->po_buyer          = $new_po_buyer;
                                                $obj->uom               = $uom_booking;
                                                $obj->qty_booking       = $qty_booking;
                                                $obj->style             = $style;
                                                $obj->is_error          = false;
                                                $obj->result            = 'SUCCESS';
                                                $array []               = $obj;
                                            }else
                                            {
                                                $obj                    = new stdClass();
                                                $obj->supplier_code     = $supplier_code;
                                                $obj->document_no       = $document_no;
                                                $obj->item_code         = $item_code_book;
                                                $obj->po_buyer_source   = $po_buyer_source;
                                                $obj->po_buyer          = $new_po_buyer;
                                                $obj->uom               = $uom_booking;
                                                $obj->qty_booking       = $qty_booking;
                                                $obj->style             = $style;
                                                $obj->is_error          = true;
                                                $obj->result            = 'FAILED, ALOKASI SUDAH ADA';
                                                $array []               = $obj;
                                            }
                                        }
                                    }else
                                    {
                                        $obj                    = new stdClass();
                                        $obj->supplier_code     = $supplier_code;
                                        $obj->document_no       = $document_no;
                                        $obj->item_code         = $item_code_book;
                                        $obj->po_buyer_source   = $po_buyer_source;
                                        $obj->po_buyer          = $new_po_buyer;
                                        $obj->uom               = $uom_booking;
                                        $obj->qty_booking       = $qty_booking;
                                        $obj->style             = $style;
                                        $obj->is_error          = true;
                                        $obj->result            = 'FAILED, PO BUYER CANCEL';
                                        $array []               = $obj;
                                    }
                                }else
                                {
                                    $obj                    = new stdClass();
                                    $obj->supplier_code     = $supplier_code;
                                    $obj->document_no       = $document_no;
                                    $obj->item_code         = $item_code_book;
                                    $obj->po_buyer_source   = $po_buyer_source;
                                    $obj->po_buyer          = $new_po_buyer;
                                    $obj->uom               = $uom_booking;
                                    $obj->qty_booking       = $qty_booking;
                                    $obj->style             = $style;
                                    $obj->is_error          = true;
                                    $obj->result            = 'FAILED, PO BUYER TIDAK DITEMUKAN';
                                    $array []               = $obj;
                                }
                            }else
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $new_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->is_error          = true;
                                $obj->result            = 'FAILED, SUPPLIER TIDAK DITEMUKAN';
                                $array []               = $obj;
                            }
                            
                        }else if($supplier_code == null 
                            || $document_no == null 
                            || $item_code_book == null 
                            || $po_buyer_source == null 
                            || $old_po_buyer == null 
                            || $qty_booking == null)
                        {
                            if($supplier_code == null)
                            {   
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $old_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->result            = 'FAILED, KODE SUPPLIER WAJIB DI ISI';
                                $array []               = $obj;
                            }else if($document_no == null)
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $old_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->is_error          = true;
                                $obj->result            = 'FAILED, PO SUPPLIER WAJIB DI ISI';
                                $array []               = $obj;
                            }else if($item_code_book == null)
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $old_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->is_error          = true;
                                $obj->result            = 'FAILED, ITEM CODE BOOK WAJIB DI ISI';
                                $array []               = $obj;
                            }else if($old_po_buyer == null)
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $old_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->is_error          = true;
                                $obj->result            = 'FAILED, PO BUYER WAJIB DI ISI';
                                $array []               = $obj;
                            }else if($qty_booking == null)
                            {
                                $obj                    = new stdClass();
                                $obj->supplier_code     = $supplier_code;
                                $obj->document_no       = $document_no;
                                $obj->item_code         = $item_code_book;
                                $obj->po_buyer_source   = $po_buyer_source;
                                $obj->po_buyer          = $old_po_buyer;
                                $obj->uom               = $uom_booking;
                                $obj->qty_booking       = $qty_booking;
                                $obj->style             = $style;
                                $obj->is_error          = true;
                                $obj->result            = 'FAILED, QTY BOOKING WAJIB DI ISI';
                                $array []               = $obj;
                            }
                        }
                    }

                    MasterAutoAllocation::updateAllocationAccessoriesItems($array_allocations);
                    DB::commit();
                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

               
                
                return response()->json($array,200);
            }else{
                  return response()->json('import gagal, silahkan cek file anda',422);
            }


        }

    }

    public function materialStockPickList(Request $request)
    {
        $document_no    = trim(strtoupper($request->document_no));
        $item_code      = trim(strtoupper($request->item_code));
        $po_buyer       = trim(strtoupper($request->po_buyer));
        $warehouse_id   = $request->warehouse_id;

        $lists = MaterialStock::where([
            ['warehouse_id',$warehouse_id],
            ['is_allocated',false],
            ['is_stock_on_the_fly',false],
            ['is_closing_balance',false],
            ['is_running_stock',false],
            ['available_qty','>','0'],
        ])
        ->whereNotNull('approval_date');

        if($document_no)
            $lists = $lists->where(function($query) use ($document_no){
                $query->where('document_no','like',"%$document_no%");
            });

        if($item_code)
            $lists = $lists->where(function($query) use ($item_code){
                $query->where('item_code','like',"%$item_code%");
            });

         if($po_buyer)
            $lists = $lists->where(function($query) use ($po_buyer){
                $query->where('po_buyer','like',"%$po_buyer%");
            });

        $lists = $lists->paginate('10');

        return view('accessories_allocation_buyer._material_stock_list',compact('lists'));

    }

    public function poBuyerPickList(Request $request)
    {
        $q = strtoupper($request->q);

        
        $lists = MaterialRequirement::select('po_buyer','item_code','category','article_no','style','uom','qty_required')
        ->where([
            ['item_code',$request->item_code]
        ])
        ->whereIn('po_buyer',function($query){
            $query->select('po_buyer')
            ->from('po_buyers')
            ->whereNull('cancel_date')
            ->groupby('po_buyer');
        })
        ->where('po_buyer','like',"%$q%")
        ->groupby('po_buyer','article_no','style','uom','qty_required','item_code','category')
        ->paginate('10');

        return view('accessories_allocation_buyer._po_buyer_list',compact('lists'));
    }

    public function store(Request $request)
    {
        //return view('errors.503');
        $validator =  Validator::make($request->all(), [
            'po_buyer_allocation_id'    => 'required',
            'item_allocation_id'        => 'required'
        ]);

        if ($validator->passes()) 
        {
            try 
            {
                DB::beginTransaction();
                
                $confirm_date           = Carbon::now()->todatetimestring();
                $material_stock_id      = $request->item_allocation_id;
                $update_allocations_acc = array();
                $_po_buyer              = trim($request->po_buyer_allocation_id);
                $is_additional          = ($request->exists('is_additional') ? true:false );
                $style                  = $request->__style;
                $article_no             = $request->__article;
                $remark_additional      = trim($request->remark_additional);
                $qty_booking            = sprintf('%0.8f',$request->qty_booking);

                $is_reroute             = ReroutePoBuyer::where('old_po_buyer',$_po_buyer)->first();
                $po_buyer              = ($is_reroute)? $is_reroute->new_po_buyer : $_po_buyer;
            
                $master_po_buyer        = PoBuyer::where('po_buyer',$po_buyer)->first();
                
                if($master_po_buyer)
                {
                    if($master_po_buyer->cancel_date) return response()->json('Po Buyer cancel',422);

                    if($master_po_buyer->statistical_date) $promise_date  = $master_po_buyer->statistical_date->format('Y-m-d');
                    else $promise_date                              = $master_po_buyer->promise_date->format('Y-m-d');

                    $season                 = $master_po_buyer->season;
                    $type_stock_erp_code    = $master_po_buyer->type_stock_erp_code;
                    $type_stock             = $master_po_buyer->type_stock;
                    $lc_date                = $master_po_buyer->lc_date->format('Y-m-d');
                    
                    
                }else
                {
                    return response()->json('Po Buyer not found',422);
                    $promise_date           = null;
                    $season                 = null; 
                    $type_stock_erp_code    = null;
                    $type_stock             = null;
                    $lc_date                = null;

                    
                }
                
                //$_po_buyer = $po_buyer;
                $material_stock         = MaterialStock::find($material_stock_id);
                $document_no            = $material_stock->document_no;
                $c_bpartner_id          = $material_stock->c_bpartner_id;
                $c_order_id             = $material_stock->c_order_id;
                $supplier_name          = $material_stock->supplier_name;
                $item_code              = $material_stock->item_code;
                $item_id                = $material_stock->item_id;
                $item_desc              = $material_stock->item_desc;
                $warehouse_id           = $material_stock->warehouse_id;
                $category               = $material_stock->category;
                $uom                    = $material_stock->uom;

                $has_outstanding_approval_exists =  DB::table('accessories_material_stock_approval_v')
                ->where('material_stock_id',$material_stock_id)
                ->exists();
        
                if($has_outstanding_approval_exists)
                {
                    return response()->json('This data has outstanding approval stock, please approved it first',422);
                }
                
                if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                else if($warehouse_id == '1000002' ) $warehouse_name = 'ACCESSORIES AOI 1';
                
                if($warehouse_id == '1000011' || $warehouse_id == '1000001') $is_fabric = true;
                $is_fabric = false;

                if($is_additional)
                {
                    $auto_allocation = AutoAllocation::create([
                        'document_allocation_number'        => null,
                        'lc_date'                           => $lc_date,
                        'c_order_id'                        => $c_order_id,
                        'season'                            => $season,
                        'document_no'                       => $document_no,
                        'c_bpartner_id'                     => $c_bpartner_id,
                        'supplier_name'                     => $supplier_name,
                        'po_buyer'                          => $po_buyer,
                        'old_po_buyer'                      => $_po_buyer,
                        'item_code_source'                  => $item_code,
                        'item_id_source'                    => $item_id,
                        'item_id_book'                      => $item_id,
                        'item_code'                         => $item_code,
                        'item_code_book'                    => $item_code,
                        'article_no'                        => null,
                        'item_desc'                         => $item_desc,
                        'category'                          => $category,
                        'uom'                               => $uom,
                        'warehouse_name'                    => $warehouse_name,
                        'warehouse_id'                      => $warehouse_id,
                        'qty_allocation'                    => $qty_booking,
                        'qty_outstanding'                   => $qty_booking,
                        'qty_allocated'                     => 0,
                        'is_fabric'                         => false,
                        'remark'                            => 'UPLOAD MANUAL',
                        'remark_update'                     => 'UPLOAD MANUAL',
                        'is_already_generate_form_booking'  => false,
                        'is_upload_manual'                  => true,
                        'is_additional'                     => true,
                        'is_from_menu_allocation_buyer'     => true,
                        'remark_additional'                 => $remark_additional,
                        'type_stock'                        => $type_stock,
                        'type_stock_erp_code'               => $type_stock_erp_code,
                        'promise_date'                      => $promise_date,
                        'status_po_buyer'                   => 'active',
                        'user_id'                           => auth::user()->id,
                        'created_at'                        => $confirm_date,
                        'updated_at'                        => $confirm_date,
                    ]);
                }else
                {
                    $is_exists = AutoAllocation::where([
                        ['lc_date',$lc_date],
                        ['season',$season],
                        [db::raw('upper(document_no)'),$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['po_buyer',$po_buyer],
                        [db::raw('upper(item_code_source)'),$item_code],
                        [db::raw('upper(item_code)'),$item_code],
                        ['warehouse_id',$warehouse_id],
                        ['qty_allocation',$qty_booking],
                    ])
                    ->whereNull('deleted_at');

                    if($type_stock_erp_code) $is_exists = $is_exists->where('type_stock_erp_code',$type_stock_erp_code);
                    $is_exists = $is_exists->exists();

                    if(!$is_exists)
                    {
                        $auto_allocation = AutoAllocation::create([
                            'document_allocation_number'        => null,
                            'lc_date'                           => $lc_date,
                            'c_order_id'                        => $c_order_id,
                            'season'                            => $season,
                            'document_no'                       => $document_no,
                            'c_bpartner_id'                     => $c_bpartner_id,
                            'supplier_name'                     => $supplier_name,
                            'po_buyer'                          => $po_buyer,
                            'old_po_buyer'                      => $_po_buyer,
                            'item_code_source'                  => $item_code,
                            'item_id_source'                    => $item_id,
                            'item_id_book'                      => $item_id,
                            'item_code'                         => $item_code,
                            'item_code_book'                    => $item_code,
                            'article_no'                        => null,
                            'item_desc'                         => $item_desc,
                            'category'                          => $category,
                            'uom'                               => $uom,
                            'warehouse_name'                    => $warehouse_name,
                            'warehouse_id'                      => $warehouse_id,
                            'qty_allocation'                    => $qty_booking,
                            'qty_outstanding'                   => $qty_booking,
                            'qty_allocated'                     => 0,
                            'is_fabric'                         => false,
                            'remark'                            => 'UPLOAD MANUAL',
                            'remark_update'                     => 'UPLOAD MANUAL',
                            'is_already_generate_form_booking'  => false,
                            'is_upload_manual'                  => true,
                            'is_additional'                     => false,
                            'remark_additional'                 => null,
                            'is_from_menu_allocation_buyer'     => true,
                            'type_stock'                        => $type_stock,
                            'type_stock_erp_code'               => $type_stock_erp_code,
                            'promise_date'                      => $promise_date,
                            'status_po_buyer'                   => 'active',
                            'user_id'                           => auth::user()->id,
                            'created_at'                        => $confirm_date,
                            'updated_at'                        => $confirm_date,
                        ]);
                    }
                }

                $note = 'TERJADI PENAMBAHAN ALOKASI';
                $note_code = '1';
            
                HistoryAutoAllocations::FirstOrCreate([
                    'auto_allocation_id'        => $auto_allocation->id,
                    'c_bpartner_id_new'         => $c_bpartner_id,
                    'supplier_name_new'         => $supplier_name,
                    'document_no_new'           => $document_no,
                    'item_id_new'               => $item_id,
                    'item_code_new'             => $item_code,
                    'item_source_id_new'        => $item_id,
                    'item_code_source_new'      => $item_code,
                    'po_buyer_new'              => $po_buyer,
                    'uom_new'                   => $uom,
                    'qty_allocated_new'         => $qty_booking,
                    'warehouse_id_new'          => $warehouse_id,
                    'type_stock_new'            => $type_stock,
                    'type_stock_erp_code_new'   => $type_stock_erp_code,
                    'note_code'                 => $note_code,
                    'note'                      => $note,
                    'operator'                  => $qty_booking,
                    'remark'                    => $remark_additional,
                    'is_integrate'              => true,
                    'user_id'                   => auth::user()->id
                ]);
                
                $update_allocations_acc [] = $auto_allocation->id;

                MasterAutoAllocation::updateAllocationAccessoriesItems($update_allocations_acc);
                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

            return response()->json('success', 200);

        }else{
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
        }

    }

    public function destroy(Request $request,$id)
    {
        //return view('errors.503');
        try 
        {
            DB::beginTransaction();

            $material_preparation          = MaterialPreparation::find($id);

            if($material_preparation->warehouse != auth::user()->warehouse)
            {
                return response()->json('Your warehouse doesn\'t same with data. please contact pic on it to delete allocation',422); 
            }

            $material_stock_id                  = $material_preparation->material_stock_id;
            $auto_allocation_id                 = $material_preparation->auto_allocation_id;
            $material_stock                     = MaterialStock::find($material_stock_id);

            $old_available_qty                  = sprintf('%0.8f',$material_stock->available_qty);
            $stock                              = sprintf('%0.8f',$material_stock->stock);
            $reserved_qty                       = sprintf('%0.8f',$material_stock->reserved_qty);
            $type_stock                         = $material_stock->type_stock;
            $type_stock_erp_code                = $material_stock->type_stock_erp_code;

            $qty_booking                        = sprintf('%0.8f',$material_preparation->qty_conversion);
            $po_buyer                           = $material_preparation->po_buyer;
            $master_po_buyer                    = PoBuyer::where('po_buyer',$po_buyer)->first();
            $style                              = $material_preparation->style;
            $article_no                         = $material_preparation->article_no;
            $lc_date                            = ( $master_po_buyer ? $master_po_buyer->lc_date : null );
            
            $new_reserverd_qty                  = sprintf('%0.8f',$reserved_qty - $qty_booking);
            $new_available_qty                  = sprintf('%0.8f',$stock - $new_reserverd_qty);
            $_new_available_qty                 = sprintf('%0.8f',$old_available_qty + $qty_booking);

            $material_stock->reserved_qty       = $new_reserverd_qty;
            $material_stock->available_qty      = $new_available_qty;

            if($new_available_qty > 0)
            {
                $material_stock->is_allocated   = false;
                $material_stock->is_active      = true;
            }

            $note_cancel = 'CANCEL ALLOCATION, ATAS PO BUYER '.$material_preparation->po_buyer.', STYLE '.$material_preparation->style.', ARTICLE '.$material_preparation->article_no.',QTY '.$qty_booking;
            $material_stock->save();

            if($auto_allocation_id)
            {
                $auto_allocation                = AutoAllocation::find($auto_allocation_id);
                $is_from_menu_allocation_buyer  = $auto_allocation->is_from_menu_allocation_buyer;
                $qty_allocation                 = sprintf('%0.8f',$auto_allocation->qty_allocation);
                $qty_allocated                  = sprintf('%0.8f',$auto_allocation->qty_allocated);
                
                $new_qty_allocated              = sprintf('%0.8f',$qty_allocated - $qty_booking);
                $new_qty_outstanding            = sprintf('%0.8f',$qty_allocation - $new_qty_allocated);
                
                if($is_from_menu_allocation_buyer)
                {
                    $auto_allocation->delete();
                }else
                {
                    $auto_allocation->qty_allocated     = $new_qty_allocated;
                    $auto_allocation->qty_outstanding   = $new_qty_outstanding;

                    if($new_qty_outstanding > 0)
                    {
                        $auto_allocation->is_already_generate_form_booking  = false;
                        $auto_allocation->generate_form_booking             = null;
                    }

                    $auto_allocation->save();
                }
            }

            HistoryStock::approved($material_stock_id
                ,$_new_available_qty
                ,$old_available_qty
                ,(-1*$qty_booking)
                ,$po_buyer
                ,$style
                ,$article_no
                ,$lc_date
                ,$note_cancel
                ,auth::user()->name
                ,auth::user()->id
                ,$type_stock_erp_code
                ,$type_stock
                ,false
                ,true
            );

            $material_preparation->delete();
            $material_stock->save();

            DB::commit();

            
        } catch (Exception $e) 
        {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
        
        return response()->json(200);
    }

    public function export()
    {
        
        echo 'weeek fungsi belum di buat :D';
    }
}
