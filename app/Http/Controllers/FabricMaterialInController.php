<?php namespace App\Http\Controllers;

use DB;
use Auth;
use StdClass;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Models\Item;
use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\MovementStockHistory;
use App\Models\MaterialPreparationFabric;
use App\Models\DetailMaterialPreparationFabric;

class FabricMaterialInController extends Controller
{
    public function index()
    {
        return view('fabric_material_in.index');
        // return view('errors.migration');
    }

    public function create(Request $request)
    {
        $_warehouse_id  =  $request->warehouse_id;
        $barcode        = trim($request->barcode);

        $is_barcode_locator = substr($barcode,0, 1);
        if($is_barcode_locator != 'R')
        {
            $array = $this->getDataBarcode($barcode,$_warehouse_id);

            if(isset($array->original))
                return response()->json($array->original,422);

            return response()->json(['data' => $array],200);
        }else
        {
            $locator = Locator::where('barcode',$barcode)
            ->whereHas('area',function($query) use ($_warehouse_id)
            {
                $query->where('warehouse',$_warehouse_id)
                ->where('is_active',true)
                ->where('is_destination',false)
                ->whereNotIn('name',['ADJUSTMENT','RECEIVING','SUPPLIER','RELAX']);
            })
            ->first();
            if(!$locator) return response()->json('Locator not found',422);
 
            return response()->json(['locator' => $locator],200);
        }

       
    }

    public function locatorPicklist(Request $request)
    {
        $q          = (isset($request->q) ? strtoupper($request->q) : null );
        $warehouse  = $request->warehouse_id;
        $lists      = Locator::whereHas('area',function($query) use($warehouse)
        {
            $query->where('warehouse',$warehouse)
            ->where('is_active',true)
            ->where('is_destination',false)
            ->whereNotIn('name',['ADJUSTMENT','RECEIVING','SUPPLIER','RELAX']);
        })
        ->where('is_active',true)
        ->where(function($query) use($q)
        {
            $query->where('barcode','like',"%$q%")
            ->orWhere('rack','like',"%$q%")
            ->orWhere('code','like',"%$q%");
        })
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->paginate('10');

        return view('fabric_material_in._rack_fabric_list',compact('lists'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'locator_in_fabric_id'  => 'required',
            'barcode_products'      => 'required|not_in:[]'
        ]);

        if($validator->passes())
        {
            $last_locator_used  = $request->last_locator_used;
            $locator_id         = $request->locator_in_fabric_id;
            $locator            = Locator::find($locator_id);
            $locator_name       = $locator->code;
            $items              = json_decode($request->barcode_products);
            $return_error       = array();
            $locator_source     = array();

            foreach ($items as $key => $value) 
            {
                try 
                {
                    DB::beginTransaction();
                    
                    $is_barcode_preparation_fabric  = $value->is_barcode_preparation_fabric;
                    $active_source                  = $locator->last_po_buyer;
                    $source_rak_id                  = $value->source_rak_id;

                    if($source_rak_id) 
                        $locator_source [] = $source_rak_id;

                    if($active_source)
                    {
                        if($active_source == 'INV' && !$is_barcode_preparation_fabric)
                        {
                            $this->insertMovement($value,$locator);
                        }else if($active_source == 'RLX' && $is_barcode_preparation_fabric)
                        {
                            $this->insertMovement($value,$locator);
                        }else
                        {
                            if($active_source == 'INV') $_active_note = 'barcode material stock';
                            else if($active_source == 'RLX') $_active_note = 'barcode material relax';

                            $item->error_note = 'Locator '.$locator_name.' has filled by '.$_active_note;
                            $return_error [] = $item;
                        }
                    }else
                    {

                        $locator->last_po_buyer = ($is_barcode_preparation_fabric ? 'RLX' : 'INV');
                        $locator->save();
                        DB::commit();

                        $this->insertMovement($value,$locator);
                    }
                    
                    DB::commit();

                } catch (Exception $e) 
                {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

                
            
            }

            $total_item_on_locator_1  = MaterialStock::where('locator_id',$locator_id)
            ->whereNull('deleted_at')
            ->count();

            $total_item_on_locator_2  = DetailMaterialPreparationFabric::where('last_locator_id',$locator_id)
            ->whereNull('deleted_at')
            ->count();

            $locator->counter_in    = $total_item_on_locator_1 + $total_item_on_locator_2;
            $locator->save();


            if(count($locator_source) > 0)
            {
                foreach ($locator_source as $key => $value) 
                {
                    $total_item_on_locator = MaterialStock::where('locator_id',$value)
                    ->whereNull('deleted_at')
                    ->count();
    
                    $source_locator                = Locator::find($value);

                    if($source_locator)
                    {
                        if($total_item_on_locator == 0) $source_locator->last_po_buyer = null;
                        $source_locator->counter_in    = $total_item_on_locator;
                        $source_locator->save();
                   
                    }
                    
                }
            }
            
            return response()->json($return_error,200);
        }else
        {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ],400);
         }
    }

    static function getDataBarcode($barcode,$_warehouse_id)
    {
        $data_found = 0; 
        $material_stock = MaterialStock::where([
            ['barcode_supplier',strtoupper(trim($barcode))],
            ['warehouse_id',$_warehouse_id],
            ['is_allocated',false],
        ])
        ->whereNull('deleted_at')
        ->first();

        if($material_stock)
        {
            $id                             = $material_stock->id;
            $source_rak_id                  = $material_stock->locator_id;
            $source_rak_code                = ($material_stock->locator_id)? $material_stock->locator->code : null;
            $barcode                        = $material_stock->barcode_supplier;
            $uom                            = $material_stock->uom;
            $warehouse_id                   = $material_stock->warehouse_id;
            $item_code                      = $material_stock->item_code;
            $category                       = $material_stock->category;
            $qty                            = sprintf('%0.8f',$material_stock->available_qty);
            $batch_number                   = $material_stock->batch_number;
            $nomor_roll                     = $material_stock->nomor_roll;
            $document_no                    = $material_stock->document_no;
            $no_invoice                     = $material_stock->no_invoice;
            $lot_actual                     = $material_stock->load_actual;
            $actual_length                  = $material_stock->actual_length;
            $actual_width                   = $material_stock->actual_width;
            $color                          = $material_stock->color;
            $is_barcode_preparation_fabric  = false;
            $note                   = null;  
            $data_found++;       
        }else
        {
            $detail_material_preparation = DetailMaterialPreparationFabric::where([
                ['barcode',strtoupper(trim($barcode))],
                ['warehouse_id',$_warehouse_id],
            ])
            ->whereNull('deleted_at')
            ->first();
            

            if($detail_material_preparation)
            {
                if(!$detail_material_preparation->is_closing)
                {
                    $id                             = $detail_material_preparation->id;
                    $material_stock_id              = $detail_material_preparation->material_stock_id;
                    $source_rak_id                  = $detail_material_preparation->last_locator_id;
                    $source_rak_code                = ($detail_material_preparation->last_locator_id ? $detail_material_preparation->locator->code : null);
                    $barcode                        = $detail_material_preparation->barcode;
                    $uom                            = $detail_material_preparation->uom;
                    $warehouse_id                   = $detail_material_preparation->warehouse_id;
                    $item_code                      = $detail_material_preparation->item_code;
                    $category                       = $detail_material_preparation->materialStock->category;
                    $qty                            = $detail_material_preparation->reserved_qty;
                    $batch_number                   = $detail_material_preparation->materialStock->batch_number;
                    $nomor_roll                     = $detail_material_preparation->materialStock->nomor_roll;
                    $document_no                    = $detail_material_preparation->materialStock->document_no;
                    $actual_length                  = $detail_material_preparation->actual_length;
                    $no_invoice                     = $detail_material_preparation->materialStock->no_invoice;
                    $lot_actual                     = $detail_material_preparation->actual_lot;
                    $actual_width                   = $detail_material_preparation->actual_width;
                    $color                          = $detail_material_preparation->color;
                    $is_barcode_preparation_fabric  = true;

                    if($detail_material_preparation->materialPreparationFabric->is_from_additional) $note = 'Roll ini digunakan untuk keperluan additional';
                    else $note = 'Roll ini digunakan untuk planning tanggal '.$detail_material_preparation->materialPreparationFabric->planning_date->format('d/M/Y');
    
                    $data_found++;
                }else
                {
                    return response()->json('Barcode not found',422); 
                }
                
            }
        }
       
        if($data_found > 0)
        {
            $obj                                = new StdClass();
            $obj->id                            = $id;
            $obj->barcode                       = $barcode;
            $obj->source_rak_id                 = $source_rak_id;
            $obj->source_rak_code               = $source_rak_code;
            $obj->warehouse_id                  = $warehouse_id;
            $obj->item_code                     = $item_code;
            $obj->color                         = $color;
            $obj->category                      = $category;
            $obj->barcode_supplier              = $barcode;
            $obj->uom                           = trim($uom);
            $obj->qty                           = $qty;
            $obj->batch_number                  = $batch_number;
            $obj->nomor_roll                    = $nomor_roll;
            $obj->document_no                   = $document_no;
            $obj->no_invoice                    = $no_invoice;
            $obj->lot_actual                    = $lot_actual;
            $obj->actual_length                 = $actual_length;
            $obj->actual_width                  = $actual_width;
            $obj->note                          = $note;
            $obj->is_barcode_preparation_fabric = $is_barcode_preparation_fabric;
            $obj->movement_date                 = Carbon::now()->toDateTimeString();
            $obj->error_note                    = null;
            return $obj;
        }
        else
        {
            return response()->json('Barcode not found',422); 
        }
        
    }

    static function insertMovement($value,$locator)
    {
        $id                             = $value->id;
        $source_rak_id                  = $value->source_rak_id;
        $movement_date                  = $value->movement_date;
        $uom                            = $value->uom;
        $available_qty                  = $value->qty;
        
        if(!$value->is_barcode_preparation_fabric)
        {
            $material_stock             = MaterialStock::find($id);
            $material_stock->locator_id = $locator->id;
            $material_stock->save();

            MovementStockHistory::create([
                'material_stock_id'     => $material_stock->id,
                'from_location'         => $source_rak_id,
                'to_destination'        => $locator->id,
                'status'                => 'move',
                'uom'                   => $material_stock->uom,
                'qty'                   => $material_stock->available_qty,
                'movement_date'         => $movement_date,
                'user_id'               => auth::user()->id
            ]);;
        }else
        {
            $detail_material_preparation    = DetailMaterialPreparationFabric::find($id);
            if(!$detail_material_preparation->deleted_at)
            {
                $old_locator                                        = $detail_material_preparation->last_locator_id;               
                $material_stock_id                                  = $detail_material_preparation->material_stock_id;
                $uom                                                = $detail_material_preparation->uom;
                $reserved_qty                                       = $detail_material_preparation->reserved_qty;
                
                $detail_material_preparation->last_locator_id       = $locator->id;
                $detail_material_preparation->last_movement_date    = $movement_date;
                $detail_material_preparation->last_user_movement_id = auth::user()->id;
                $detail_material_preparation->save();

                MovementStockHistory::create([
                    'material_stock_id' => $detail_material_preparation->material_stock_id,
                    'from_location'     => $old_locator,
                    'to_destination'    => $locator->id,
                    'status'            => 'move-relax',
                    'uom'               => $uom,
                    'qty'               => $reserved_qty,
                    'movement_date'     => $movement_date,
                    'note'              => 'Barcode prepare dengan nomor seri '.$detail_material_preparation->barcode.' dipindahkan kedalam locator '.$locator->code,
                    'user_id'           => auth::user()->id
                ]);
            }
            
        }
    }
}
