<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryMaterialPlanningFabric extends Model
{
    use Uuids;
    public $timestamps = false;
    public $incrementing = false;
    protected $dates = ['created_at','planning_date','updated_at'];
    protected $guarded = ['id'];
    protected $fillable = ['article_no'
        ,'item_id'
        ,'item_code'
        , 'po_buyer'
        , 'note'
        ,'style'
        ,'material_preparation_fabric_id'
        ,'planning_date'
        ,'warehouse_id'
        ,'user_id'
        ,'deleted_at'
        ,'color'
        ,'job_order'
        ,'qty_consumtion'
        ,'uom'
        ,'remark'
        ,'created_at'
        ,'updated_at'
        ,'is_piping'
    ];

}
