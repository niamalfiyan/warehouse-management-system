<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialCheck extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $dates        = ['created_at','confirm_date','integration_date','planning_qc_date','insert_to_locator_reject_erp_date'];
    protected $guarded      = ['id'];
    protected $fillable     = ['po_detail_id'
    ,'created_at'
    ,'updated_at'
    ,'qty_on_barcode'
    ,'qty_on_barcode'
    ,'material_preparation_id'
    ,'category'
    ,'nomor_roll'
    ,'po_buyer'
    ,'uom_po'
    ,'uom_conversion'
    ,'document_no'
    ,'item_code'
    ,'item_desc'
    ,'qty_sample'
    ,'c_orderline_id'
    ,'planning_qc_date'
    ,'qty_booking_planning'
    ,'type_po'
    ,'integration_date'
    ,'is_integrate'
    ,'locator_id'
    ,'barcode_preparation'
    ,'status'
    ,'qty_reject'
    ,'user_id'
    ,'detail_material_arrival_id'
    ,'remark'
    ,'already_created_rma'
    ,'material_stock_id'
    ,'batch_number'
    ,'color'
    ,'no_invoice'
    ,'begin_width'
    ,'middle_width'
    ,'end_width'
    ,'qty_order'
    ,'actual_width'
    ,'kg'
    ,'actual_length'
    ,'point_1'
    ,'point_2'
    ,'point_3'
    ,'point_4'
    ,'total_point'
    ,'percentage'
    ,'jumlah_defect_a'
    ,'jumlah_defect_b'
    ,'jumlah_defect_c'
    ,'jumlah_defect_d'
    ,'jumlah_defect_e'
    ,'jumlah_defect_f'
    ,'jumlah_defect_g'
    ,'jumlah_defect_h'
    ,'jumlah_defect_i'
    ,'jumlah_defect_j'
    ,'jumlah_defect_k'
    ,'jumlah_defect_l'
    ,'jumlah_defect_l'
    ,'jumlah_defect_m'
    ,'jumlah_defect_n'
    ,'jumlah_defect_o'
    ,'jumlah_defect_p'
    ,'jumlah_defect_q'
    ,'jumlah_defect_r'
    ,'jumlah_defect_s'
    ,'jumlah_defect_t'
    ,'total_linear_point'
    ,'total_yds'
    ,'total_formula_1'
    ,'total_formula_2'
    ,'final_result'
    ,'confirm_date'
    ,'c_bpartner_id'
    ,'supplier_name'
    ,'confirm_user_id'
    ,'warehouse_id'
    ,'different_yard'
    ,'is_already_inspect'
    ,'width_on_barcode'
    ,'is_reject'
    ,'insert_to_locator_reject_erp_date'
    ,'is_from_metal_detector'
    ];

    public function materialPreparation()
    {
        return $this->belongsTo('App\Models\MaterialPreparation');
    }

    public function detailMaterialArrival()
    {
        return $this->belongsTo('App\Models\DetailMaterialArrival');
    }

    public function locator()
    {
        return $this->belongsTo('App\Models\Locator');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function confirmUser()
    {
        return $this->belongsTo('App\Models\User','confirm_user_id','id');
    }
}
