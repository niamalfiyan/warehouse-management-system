<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MonitoringMaterialPerBatchFabric extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at','receive_date'];
    protected $guarded = ['id'];
    protected $fillable = ['document_no','no_invoice','c_bpartner_id','supplier_name','item_code','item_desc','color','total_batch_number','warehouse_id','receive_date'];

}
