<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialRollHandoverFabric extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at','receive_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_stock_id'
        ,'barcode'
        ,'c_bpartner_id'
        ,'supplier_name'
        ,'c_order_id'
        ,'no_invoice'
        ,'document_no'
        ,'item_id'
        ,'item_code'
        ,'color'
        ,'nomor_roll'
        ,'batch_number'
        ,'actual_lot'
        ,'actual_width'
        ,'no_packing_list'
        ,'uom'
        ,'qty_handover'
        ,'po_detail_id'
        ,'user_id'
        ,'warehouse_to_id'
        ,'warehouse_from_id'
        ,'receive_date'
        ,'user_receive_id'
        ,'referral_code'
        ,'sequence'
        ,'summary_handover_material_id'
        ,'begin_width'
        ,'middle_width'
        ,'end_width'
        ,'actual_length'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function summaryHandoverMaterial()
    {
        return $this->belongsTo('App\Models\SummaryHandoverMaterial');
    }

     public function userReceive()
     {
        return $this->belongsTo('App\Models\User','user_receive_id');
    }

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

}
