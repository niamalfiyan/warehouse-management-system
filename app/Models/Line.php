<?php namespace App\models;
use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Line extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['code', 'name', 'user_id'];
    protected $dates = ['created_at'];
}
