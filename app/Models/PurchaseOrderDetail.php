<?php namespace App\Models;

use DB;
use Config;
use StdClass;
use Carbon\Carbon;

use App\Http\Controllers\ErpMaterialRequirementController;
use App\Models\Temporary;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrderDetail extends Model
{
    protected $connection   = 'web_po';
    protected $guarded      = ['id'];
    protected $fillable     = ['flag_wms','user_warehouse_receive','wms_is_locked'];
    protected $table        = 'po_detail';
    
    
    public function Purchase()
    {
        return $this->belongsTo('App\Models\PurchaseOrder', 'c_order_id');
    }

    public function PurchaseLines()
    {
        return $this->hasMany('App\Models\PurchaseOrderDetailLine', 'c_orderline_id');
    }

    public function getInformationOnWms($po_detail_id = null)
    {
        $is_exists = DB::connection('pgsql')
        ->table('stock_lines')
        ->where('po_detail_id',$po_detail_id)
        ->count();
        
        return $is_exists;
    }

    static function getBom($po_buyer, $item_code)
    {
        // ErpMaterialRequirementController::getPobuyer($po_buyer, '');
        // ErpMaterialRequirementController::getSummaryBuyerPerPart($po_buyer, '');
        // ErpMaterialRequirementController::getSummaryBuyer($po_buyer, '');

        $material_requirements = DB::connection('pgsql')
                                ->table('material_requirements')
                                ->select('style','article_no', db::raw('promise_date::date as promise_date'))
                                ->where([
                                    ['po_buyer',$po_buyer],
                                    ['item_code',$item_code]
                                ])
                                ->groupby('style','article_no', 'promise_date')
                                ->get();
        
        $flag           = 0;
        $style          = null;
        $article_no     = null;
        $promise_date   = null;

        foreach ($material_requirements as $key => $value) 
        {
            $style      = $value->style;
            $article_no = $value->article_no;
            $promise_date = $value->promise_date;
            $flag++;
        }

        if($flag > 1)
        {
            $obj                = new stdClass();
            $obj->style         = 'HAS MANY STYLE';
            $obj->article_no    = 'HAS MANY ARTICLE';
            $obj->promise_date  = 'HAS MANY PROMISE DATE';

        }else
        {
            $obj                = new stdClass();
            $obj->style         = $style;
            $obj->article_no    = $article_no;
            $obj->promise_date  = $promise_date;

        }

        return $obj;
    }

    // static function getBom($po_buyer, $item_code)
    // {
    //     ini_set('max_execution_time', 300);
    //     cekUlang :
    //     $material_requirements = DB::connection('pgsql')
    //                             ->table('material_requirements')
    //                             ->select('style','article_no', db::raw('promise_date::date as promise_date'))
    //                             ->where([
    //                                 ['po_buyer',$po_buyer],
    //                                 ['item_code',$item_code]
    //                             ])
    //                             ->groupby('style','article_no', 'promise_date')
    //                             ->get();
        
    //     $cek_erp = DB::connection('erp')->select(db::raw("SELECT po_buyer, item_code 
    //                 FROM get_wms_material_requirement_detail_new('".$po_buyer."') 
    //                 WHERE item_code like '%".$item_code."%';"
    //                 ));
        
    //     foreach ($material_requirements as $key => $value) {
    //             $style      = $value->style;
    //             $article_no = $value->article_no;
    //             $promise_date = $value->promise_date;
    //         }

    //     if ($material_requirements->count() > 0 && COUNT($cek_erp) > 0) {

    //         $obj                = new stdClass();
    //         $obj->style         = $style;
    //         $obj->article_no    = $article_no;
    //         $obj->promise_date  = $promise_date;

    //     } elseif ($material_requirements->count() < 1 && COUNT($cek_erp) > 0) {

    //         $movement_date  = carbon::now()->toDateTimeString();
    //         try {

    //         DB::beginTransaction();

    //         ErpMaterialRequirementController::getPobuyer($po_buyer, '');
    //         ErpMaterialRequirementController::getSummaryBuyerPerPart($po_buyer, '');
    //         ErpMaterialRequirementController::getSummaryBuyer($po_buyer, '');

    //         Temporary::FirstOrCreate([
    //             'barcode'       => $po_buyer,
    //             'status'        => 'mrp',
    //             'user_id'       => 1,
    //             'created_at'    => $movement_date,
    //             'updated_at'    => $movement_date,
    //         ]);


    //         DB::commit();
    //         } catch (Exception $e) {
    //                 DB::rollBack();
    //                 $message = $e->getMessage();
    //                 ErrorHandler::db($message);
    //         }

    //         goto cekUlang;
            
    //     } elseif ($material_requirements->count() > 0) {
            
    //         $obj                = new stdClass();
    //         $obj->style         = $style;
    //         $obj->article_no    = $article_no;
    //         $obj->promise_date  = $promise_date;

    //     } else {
            
    //         $obj                = new stdClass();
    //         $obj->style         = 'UPDATE BOM';
    //         $obj->article_no    = 'UPDATE BOM';
    //         $obj->promise_date  = 'UPDATE BOM';
    //     }

    //     return $obj;
    // }

    static function getSeason($po_buyer)
    {
        $season = DB::connection('pgsql')
        ->table('po_buyers')
        ->select('season')
        ->where('po_buyer',$po_buyer)
        ->groupby('season')
        ->first();
        
        if($season)return $season->season;
        else return '-1';
    }

    static function checkFlagWms($po_detail_id)
    {
        $data = DB::connection('web_po')
        ->table('po_detail')
        ->where('po_detail_id',$po_detail_id)
        ->first();

        return $data->flag_wms;
    }

    static function getRecycle($item_code)
    {
        $item = DB::connection('pgsql')
        ->table('items')
        ->where('item_code',$item_code)
        ->where('recycle', true)
        ->first();
        
        if($item)return '-Recycle';
        else return '';
    }

    static function getSewingPlace($item_id, $po_buyer)
    {
         //alokasi subcont / beda whs 
         $erp = DB::connection('erp');
         $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
             ['poreference',$po_buyer],
             ['componentid',$item_id],
             // ['style',$style],
             // ['kst_articleno',$article_no]
         ])
         ->select('warehouse_place')
         ->first();
        //  dd($item_id.'+'.$po_buyer);
        if($sewing_place != null){
            if($sewing_place->warehouse_place == 'Pihak Ketiga') return 'BBIS';
            elseif($sewing_place->warehouse_place =='Packing AOI 1') return 'AOI 1';
            elseif($sewing_place->warehouse_place =='Packing AOI 2') return 'AOI 2';
            else return '';
        } else{
            return '';  
        }
        
    }
}
