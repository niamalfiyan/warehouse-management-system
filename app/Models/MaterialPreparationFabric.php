<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialPreparationFabric extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $dates        = ['created_at','preparation_date','eta_date','relax_date','planning_date','preparation_date'];
    protected $guarded      = ['id'];
    protected $fillable     = [
        'material_stock_id'
        ,'c_order_id'
        ,'is_need_to_be_change'
        ,'planning_date'
        ,'item_id_book'
        ,'item_id_source'
        ,'additional_note'
        ,'relax_date'
        ,'created_at'
        ,'updated_at'
        ,'uom'
        ,'eta_date'
        ,'document_no'
        ,'item_code'
        ,'item_code_source'
        ,'c_bpartner_id'
        ,'supplier_name'
        ,'no_invoice'
        ,'total_qty_rilex'
        ,'total_qty_outstanding'
        ,'user_id'
        ,'total_reserved_qty'
        ,'warehouse_id'
        ,'is_piping'
        ,'is_from_additional'
        ,'remark'
        ,'deleted_at'
        ,'delete_user_id'
        ,'remark_planning'
        ,'article_no'
        ,'_style'
        ,'preparation_date'
        ,'preparation_by'
        ,'delete_reason'
        ,'po_buyer'
        ,'machine'
        ,'is_balance_marker'
        ,'auto_allocation_id'
    ];


    public function detailMaterialPreparationFabric()
    {
        return $this->hasMany('App\Models\DetailMaterialPreparationFabric');
    }

}
