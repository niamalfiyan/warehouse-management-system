<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;
use Auth;

class Locator extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['code'
    ,'area_id'
    ,'rack'
    ,'y_row'
    ,'erp_id'
    ,'z_column'
    ,'user_id'
    ,'is_active'
    ,'has_many_po_buyer'
    ,'barcode'
    ,'counter_in'
    ,'sequence'
    ,'referral_code'
    ,'last_sto_date'
    ,'last_user_sto_id'
    ,'created_at'
    ,'updated_at'
    ,'deleted_at'
    ,'last_po_buyer'];

    protected $dates = ['created_at'];
  
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
    public function area()
    {
        return $this->belongsTo('App\Models\Area','area_id');
    }

    public function materialPreparation()
    {
        return $this->hasMany('App\Models\MaterialPreparation','last_locator_id','id')
        ->whereNull('deleted_at');
    }

    public function stockMovementFroms()
    {
        return $this->hasMany('App\Models\StockMovement','from_location','id')
        ->where('is_active',true);
    }

    public function stockMovementDestinations()
    {
        return $this->hasMany('App\Models\StockMovement','to_destination','id')
        ->where('is_active',true);
    }

    static function getActiveBuyer($id,$has_many_po_buyer){

        if($has_many_po_buyer) return 'HAS MANY PO BUYER';

        $warehouse_id = auth::user()->warehouse;
        $data = DB::table('material_movements')
        ->select('material_movement_lines.po_buyer')
        ->join(DB::raw('
            (
                select material_movement_id,
                    material_preparations.po_buyer
                from material_movement_lines 
                join material_preparations on material_preparations.id = material_movement_lines.material_preparation_id
                where (material_movement_lines.is_active = true and material_preparations.deleted_at is null)
                and material_preparations.warehouse = \''.$warehouse_id.'\'
                group by material_movement_id,material_preparations.po_buyer
            )material_movement_lines
        '),'material_movement_lines.material_movement_id','material_movements.id')
        ->where('material_movements.to_destination',$id)
        ->where(function($query){
            $query->where('material_movements.status','in')
            ->orWhere('material_movements.status','change');
        })
        ->groupby('material_movement_lines.po_buyer')
        ->first();

        if($data)
            return $data->po_buyer;
        else 
            return null;
    }

    public function listLocator($area_id = null,$rack = null){
        $locator = DB::table('locators')
            ->select('area_id','locators.id','rack','y_row','z_column',
            db::raw("string_agg(q1.po_buyer, ', ') as po_buyer"))
            ->leftJoin(DB::raw('(select to_destination,po_buyer
            from material_movements
            where is_active = true
            and status in (\'in\',\'change\',\'reject\')
            group by to_destination,po_buyer) q1'),
            'q1.to_destination','locators.id')
            ->where('locators.is_active',true)
            ->where('locators.area_id',$area_id)
            ->where('locators.rack',$rack)
            ->orderBy('rack','asc')
            ->orderBy('y_row','asc')
            ->orderBy('z_column','asc')
            ->groupBy('area_id','locators.id','rack','y_row','z_column')
            ->get();
            
        return $locator;
    }
}
