<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialPlanningFabric extends Model
{
    use Uuids;
    public $timestamps = false;
    public $incrementing = false;
    protected $dates = ['created_at','planning_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_planning_fabric_id'
        ,'c_order_id'
        ,'item_id_book'
        ,'item_code'
        ,'item_id_source'
        ,'item_code_source'
        ,'style'
        ,'_style'
        ,'article_no'
        ,'is_piping'
        ,'c_bpartner_id'
        ,'allocation_item_id'
        ,'document_no'
        ,'uom'
        ,'supplier_name'
        ,'no_invoice'
        ,'eta_date'
        ,'is_from_allocation'
        ,'material_preparation_fabric_id'
        ,'po_buyer'
        ,'is_from_subconts'
        ,'is_from_additional'
        ,'qty_booking'
        ,'planning_date'
        ,'user_id'
        ,'warehouse_id'
        ,'created_at'
        ,'updated_at'
        ,'deleted_at'
        ,'remark'
        ,'actual_lot'
    ];

    public function materialPlanningFabric()
    {
        return $this->belongsTo('App\Models\MaterialPlanningFabric');
    }

    public function allocationItem()
    {
        return $this->belongsTo('App\Models\AllocationItem');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    static function qtyAllocated($id)
    {
        return DetailPoBuyerPerRoll::where('detail_material_planning_fabric_id',$id)
        ->whereNull('deleted_at')
        ->sum('qty_booking');
    }

    static function checkSaldo($_style,$article_no,$c_order_id,$item_id_book,$item_id_source,$planning_date,$is_piping,$warehouse_id,$po_buyer)
    {
        return DB::select(db::raw("
            select exists
            (
                select * From (
                    select id,
                    round(float8(COALESCE(detail_material_planning_fabrics.qty_booking, 0::numeric))::numeric, 6) AS qty_booking,
                    round(float8(COALESCE(dtl.total_booked, 0::numeric))::numeric, 6) AS total_booked
                    from detail_material_planning_fabrics
                    left join (
                        select detail_material_planning_fabric_id,sum(qty_booking) as total_booked
                        From detail_po_buyer_per_rolls
                        where detail_material_planning_fabric_id is not null
                        and exists (
                                select 1 from detail_material_planning_fabrics
                                where detail_material_planning_fabrics.id = detail_material_planning_fabric_id
                                and  _style = '".$_style."'
                                and article_no = '".$article_no."'
                                and deleted_at is null
                                and c_order_id = '".$c_order_id."'
                                and item_id_book = '".$item_id_book."'
                                
                        )
                        GROUP BY detail_material_planning_fabric_id
                    )dtl on dtl.detail_material_planning_fabric_id = detail_material_planning_fabrics.id
                    where _style = '".$_style."'
                    and article_no = '".$article_no."'
                    and deleted_at is null
                    and c_order_id = '".$c_order_id."'
                    and item_id_book = '".$item_id_book."'
                    and item_id_source = '".$item_id_source."'
                    and planning_date = '".$planning_date."' 
                    and is_piping = '".($is_piping ? 1 : 0)."' 
                    and warehouse_id = '".$warehouse_id."' 
                )tbl
                where tbl.total_booked < qty_booking
            );
        "));
    }

}
