<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;


class PlanningPullCutting extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at','planning_date'];
    protected $guarded = ['id'];
    protected $fillable = ['document_no', 'item_code','article_no','style','planning_date','actual_width','user_id','deleted_at','c_bpartner_id'];

    protected $dispatchesEvents = [
      'created' => CuttingHandoverObserver::class,
      'observe' => CuttingHandoverObserver::class,
      'updating' => CuttingHandoverObserver::class,
      'updated'  => CuttingHandoverObserver::class,
      'creating' => CuttingHandoverObserver::class,
      'saved'    => CuttingHandoverObserver::class,
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function detailMaterialPreparationFabric(){
        return $this->hasMany('App\Models\DetailMaterialPreparationFabric');
    }
}
