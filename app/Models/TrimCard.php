<?php

namespace App\Models;


use DB;
use Auth;
use StdClass;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class TrimCard extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['style', 'job_order', 'po_buyer', 'article_no', 'season', 'remark', 'user_id', 'lc_date'];
    protected $dates = ['created_at'];
}
