<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;


class HistoryMaterialStocks extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $dates        = ['created_at','deleted_at'];
    protected $guarded      = ['id'];
    protected $fillable     = ['material_stock_id'
    ,'lc_date'
    ,'c_order_id'
    ,'document_no'
    ,'c_bpartner_id'
    ,'supplier_name'
    ,'warehouse_id'
    ,'item_id'
    ,'item_code'
    ,'po_buyer'
    ,'item_desc'
    ,'source'
    ,'uom'
    ,'stock_old'
    ,'stock_new'
    ,'operator'
    ,'note'
    ,'po_buyer'
    ,'note'

    ,'lc_date_po_buyer_allocation'
    ,'po_buyer_allocation'
    ,'style_po_buyer_allocation'
    ,'article_po_buyer_allocation'
    ,'qty_allocation'
    ,'is_stock_allocation'

    ,'type_stock_erp_code'
    ,'type_stock'
    ,'mapping_stock_id'
    ,'is_integrate'
    ,'integration_date'
    ,'user_id'
    ,'actual_lot'
    ,'material_stock_per_lot_id'
    ,'deleted_at'];

    public function materialStock(){
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function materialStockPerLot()
    {
        return $this->belongsTo('App\Models\MaterialStockPerLot');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }


}
