<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Criteria extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','is_know_data_source','is_active','user_id','warehouse'];
    protected $dates = ['created_at'];

    public function MiscellaneousItems()
    {
        return $this->hasMany('App\Models\MiscellaneousItem');
    }
}
