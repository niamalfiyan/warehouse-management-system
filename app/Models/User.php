<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    protected $fillable = [
        'name'
        ,'nik'
        ,'email'
        ,'sex'
        ,'photo'
        ,'application'
        ,'department'
        ,'password'
        ,'warehouse'
        ,'deleted_at'
        ,'last_login'
        ,'session_id'
        ,'ip_address'
        ,'last_logout'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Stocks()
    {
        return $this->hasMany('App\Models\Stock');
    }

    public function Areas()
    {
        return $this->hasMany('App\Models\Area');
    }

    public function StockLines()
    {
        return $this->hasMany('App\Models\StockLine');
    }

    public function roles(){
        return $this->belongsToMany('App\Models\Role','role_user','user_id','role_id');
    }
}
