<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class Item extends Model
{
    use Uuids;
    protected $dates = ['created_at'];
    public $timestamps = false;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['item_code'
        ,'uom'
        ,'item_id'
        ,'category'
        ,'item_desc'
        ,'color'
        ,'composition'
        ,'upc'
        ,'category_name'
        ,'width'
        ,'required_lot'
        ,'created_at'
        ,'updated_at'
        ,'is_stock'
    ];
}
