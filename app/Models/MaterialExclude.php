<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialExclude extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at'];
    protected $guarded = ['id'];
    protected $fillable = ['item_code', 'category','user_id','is_ila','is_from_buyer','is_for_receiving_validation','is_paxar'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

}
