<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class ItemPackingList extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at'];
    protected $fillable = ['no_packing_list','no_resi','no_invoice','type_po','c_bpartner_id','total_item','total_item_receive','is_integration','integration_date'];

   
}
