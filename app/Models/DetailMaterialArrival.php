<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialArrival extends Model
{
    
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','preparation_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_arrival_id','po_buyer', 'barcode_supplier','uom', 'qty_delivered','qty_upload',
    'user_id','is_active','preparation_date','batch_number','nomor_roll','uom_conversion','jenis_po'];

    public function materialArrival(){
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialCheck(){
        return $this->hasMany('App\Models\MaterialCheck');
    }
}
