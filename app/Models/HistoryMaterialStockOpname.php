<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class HistoryMaterialStockOpname extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','deleted_at','reject_date','approval_date','sto_date','accounting_approval_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_stock_id'
    ,'available_stock_old'
    ,'locator_old_id'
    ,'locator_new_id'
    ,'available_stock_new'
    ,'operator'
    ,'note'
    ,'sto_note'
    ,'user_approval_id' // approval by mm
    ,'approval_date' // approval by mm
    ,'accounting_approval_date'
    ,'accounting_user_id'
    ,'user_reject_id'
    ,'source'
    ,'reject_date'
    ,'user_id'
    ,'deleted_at'
    ,'sto_date'];

    public function materialStock(){
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function mMApproval(){
        return $this->belongsTo('App\Models\User','user_approval_id');
    }

    public function accountingApproval(){
        return $this->belongsTo('App\Models\User','accounting_user_id');
    }

     public function userReject(){
        return $this->belongsTo('App\Models\User','user_reject_id');
    }
}
