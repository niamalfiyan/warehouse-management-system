<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Rma extends Model
{
    use Uuids;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $fillable = ['material_check_id', 
        'document_no',
        'no_packing_list',
        'no_invoice',
        'item_id',
        'item_code',
        'c_bpartner_id',
        'c_orderline_id',
        'uom',
        'qty_reject',
        'is_integrate',
        'integration_date',
        'remark',
        'is_complete_erp'
    ];
}
