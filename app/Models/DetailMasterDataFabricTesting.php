<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMasterDataFabricTesting extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['subtesting_name','require','master_data_fabric_testing_id','user_id'];

    public function masterDataFabricTesting()
    {
        return $this->belongsTo('App\Models\MasterDataFabricTesting');
    }
}
