<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class BarcodePreparationFabric extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['material_stock_id','barcode','actual_planning_warehouse_date','warehouse_id','total_qty_preparation','last_locator_id','last_status_movement','last_movement_date','last_user_movement_id','user_id','referral_code','sequence','qty_saving','upc_item','cancel_reason','cancel_user_id','deleted_at','begin_width','middle_width','end_width','actual_length','actual_width'];
    protected $dates = ['created_at','actual_planning_warehouse_date'];


    public function materialStock(){
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function locator(){
        return $this->belongsTo('App\Models\Locator','last_locator_id');
    }
}
