<?php namespace App\Models;

use DB;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MiscellaneousItem extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','approval_date','reject_date','deleted_at'];
    protected $fillable = ['material_arrival_id','criteria_id','category','system_note', 'warehouse','uom','rack','qty','approval_status',
                            'is_active','approval_date','reject_date','user_id','deleted_at','item_id','item_code','item_desc','is_know_datasource','user_approval_id','user_reject_id'];
    
    public function materialArrival(){
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function criteria(){
        return $this->belongsTo('App\Models\Criteria');
    }

    public function allocations(){
        return $this->hasMany('App\Models\AllocationItem','id','miscellaneous_item_id');
    }

    public function getQtyAllocation($id = null){
        $qty_allocated =  DB::table('allocation_items')
            ->where('miscellaneous_item_id',$id)
            ->whereNotNull('confirm_date')
            ->whereNotNull('confirm_by_warehouse')
            ->sum('qty_booking');
        
        return $qty_allocated;
    }
}
