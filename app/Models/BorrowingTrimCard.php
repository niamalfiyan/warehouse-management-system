<?php

namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BorrowingTrimCard extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['barcode', 'name', 'pic_trim_card_id', 'borrowed_date', 'returned_date'];
    protected $dates = ['created_at'];
}
