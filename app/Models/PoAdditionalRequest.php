<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;


class PoAdditionalRequest extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['document_no','po_buyer','item_code', 'qty','uom','category'];
    protected $dates = ['created_at'];


    static function conversion($item_code,$uom_from,$category){
        $conversion  = DB::table('uom_conversions')
        ->where([
            ['item_code', $item_code],
            ['category', $category],
            ['uom_from', $uom_from],
        ])
        ->first();
        
        if($conversion)
            return $conversion;
        else
            return null;
    }
}
