<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Models\Locator;
use App\Models\MaterialStock;
use App\Models\AllocationItem;
use App\Models\MaterialRequirement;
use App\Models\DetailMaterialPreparation;

class MaterialPreparation extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'
    ,'updated_at'
    ,'deleted_at'
    ,'first_inserted_to_locator_date'
    ,'insert_to_locator_inventory_erp_date'
    ,'insert_to_locator_free_stock_erp_date'
    ,'reprint_date'
    ,'first_print_date'
    ,'last_movement_date'
    ,'planning_cutting_date'
    ,'adjustment_date'
    ,'cancel_planning'];
    
    protected $fillable     = ['created_at'
    ,'updated_at'
    ,'qty_reject'
    ,'material_stock_id'
    ,'auto_allocation_id'
    ,'last_material_movement_line_id'
    ,'document_no'
    ,'adjustment'
    ,'reprint_date'
    ,'reprint_user_id'
    ,'type_po'
    ,'barcode'
    ,'is_movement_adjustment'
    ,'insert_to_locator_free_stock_erp_date'
    ,'insert_to_locator_inventory_erp_date'
    ,'is_from_closing'
    ,'first_inserted_to_locator_date'
    ,'po_buyer'
    ,'uom_conversion'
    ,'is_reduce'
    ,'total_carton'
    ,'is_allocation'
    ,'is_from_adjustment'
    ,'adjustment_date'
    ,'is_from_cancel_or_reroute'
    ,'is_need_inserted_to_locator_free_stock_for_adjustment_erp_date'
    ,'is_need_inserted_to_locator_free_stock_for_cancel_or_reroute_da'
    ,'remark'
    ,'po_detail_id'
    ,'is_additional'
    ,'c_bpartner_id'
    ,'supplier_name'
    ,'c_order_id'
    ,'item_id'
    ,'item_id_source'
    ,'ict_log'
    ,'is_from_barcode_bom'
    ,'is_reroute'
    ,'is_need_to_be_switch'
    ,'qty_borrow'
    ,'is_from_switch'
    ,'is_from_handover'
    ,'_style'
    ,'sto_user_id',
    'qty_reconversion'
    ,'qty_conversion'
    ,'job_order'
    ,'allocation_item_id'
    ,'article_no'
    ,'style'
    ,'warehouse'
    ,'qc_status'
    ,'user_id'
    ,'deleted_at'
    ,'item_code'
    ,'category'
    ,'item_desc'
    ,'is_additional'
    ,'is_moq'
    ,'first_print_date'
    ,'last_status_movement'
    ,'last_locator_id'
    ,'last_user_movement_id'
    ,'last_movement_date'
    ,'is_backlog'
    ,'referral_code'
    ,'sequence'
    ,'sto_result'
    ,'sto_date'
    ,'qty_switch'
    ,'is_need_inserted_to_locator_free_stock_erp'
    ,'is_need_inserted_to_locator_inventory_erp'
    ,'reprint_counter'
    ,'po_buyer_source'
    ,'total_switch'
    ,'planning_cutting_date'
    ,'is_stock_already_created'
    ,'so_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function stoUser()
    {
        return $this->belongsTo('App\Models\User','sto_user_id');
    }

    public function lastUserMovement()
    {
        return $this->belongsTo('App\Models\User','last_user_movement_id');
    }

    public function AllocationItem()
    {
        return $this->belongsTo('App\Models\AllocationItem');
    }

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function lastLocator()
    {
        return $this->belongsTo('App\Models\Locator','last_locator_id');
    }

    public function detailMaterialPreparation()
    {
        return $this->hasMany('App\Models\DetailMaterialPreparation');
    }

    public function materialAdjustments()
    {
        return $this->hasMany('App\Models\MaterialAdjustment');
    }

    static function checkPoCancel($po_buyer)
    {
        $is_exists = PoBuyer::where('po_buyer',$po_buyer)
        ->whereNotNull('cancel_date')
        ->exists();

        return $is_exists;
    }

    static function checkItemCancel($po_buyer,$item_id)
    {
        $is_exists = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id],
        ])
        ->exists();

        return $is_exists;
    }

    static function getRecycle($item_id)
    {
        $item = DB::table('items')
        ->where('item_id',$item_id)
        ->where('recycle', true)
        ->first();
        
        if($item)return '-Recycle';
        else return '';
    }

    static function Locator($po_buyer,$warehouse_id)
    {
        $locators = Locator::whereIn('id',function($query) use ($po_buyer,$warehouse_id)
        {
            $query->select('last_locator_id')
            ->from('material_preparations')
            ->where([
                ['po_buyer',$po_buyer],
                ['warehouse',$warehouse_id]
            ])
            ->whereNull('deleted_at')
            ->where(function($query){
                $query->where('last_status_movement','in')
                ->orWhere('last_status_movement','change');
            })
            ->whereNotNull('last_locator_id')
            ->groupby('last_locator_id');
        })
        ->get();

        $obj = new StdClass();
        if(count($locators) > 1)
        {
           $code = '';
            foreach ($locators as $key => $locator) 
            {
                $code       .= $locator->code.',';
            }

            $obj->id        = null;
            $obj->code      = substr($code,0,-1);
        }else
        {
            if(count($locators) == 1)
            {
                foreach ($locators as $key => $locator) 
                {
                    $obj->id    = $locator->id;
                    $obj->code  = $locator->code;
                }
            }else
            {
                $obj->id        = null;
                $obj->code      = null;
            }
        }

        return $obj;
    }

    static function getMaterialArrival($po_buyer,$item_code,$style)
    {
        $material_preparation = MaterialPreparation::select('id')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style]
        ])
        ->first();

        $material_arrivals = DetailMaterialPreparation::select('material_arrival_id')
        ->where('material_preparation_id',$material_preparation->id)
        ->groupby('material_arrival_id')
        ->get();

        $array = array();
        if($material_arrivals->count() != 0)
        {
            foreach ($material_arrivals as $key => $value) 
            {
                $array [] = $value->material_arrival_id;
            }
            return $array;
        }else
        {
            return null;
        }

    }

    static function getId($po_buyer,$item_code,$style)
    {
        $material_preparations = MaterialPreparation::select('id')
        ->where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style]
        ])
        ->get();

        if($material_preparation)
        {
            $array = array();
            foreach ($material_preparations as $key => $material_preparation) 
            {
                $array [] =  $material_preparation->id;
            }
        }else
        {
             return null;
        }

    }

    static function conversion($item_code,$category,$uom_from)
    {
        $conversion  = UomConversion::where([
            ['item_code', $item_code],
            ['uom_from', $uom_from],
        ])
        ->first();

        if($conversion) return $conversion;
        else return null;
    }

    static function reconversion($item_code,$category,$uom_from,$material_stock_id)
    {
        if($material_stock_id)
        {
            $material_stock = MaterialStock::find($material_stock_id);
            $uom_source = ($material_stock ? $material_stock->uom_source : null);

            if($uom_source)
            {
                $conversion  = UomConversion::where([
                    ['item_code', $item_code],
                    ['uom_from', $uom_source],
                ])
                ->first();
            }else
            {
                $conversion  = UomConversion::where([
                    ['item_code', $item_code],
                    ['uom_to', $uom_from],
                ])
                ->first();
            }
        }else
        {
            $conversion  = UomConversion::where([
                ['item_code', $item_code],
                ['uom_to', $uom_from],
            ])
            ->first();
        }
        
        

        if($conversion) return $conversion;
        else return null;
    }

    static function getQtyReject($id)
    {
        $qty_material_checks = MaterialCheck::where([
            ['material_preparation_id',$id],
            ['status','REJECT']
        ])
        ->leftjoin('material_preparations','material_preparations.id','material_checks.material_preparation_id')
        ->where('material_preparations.warehouse',auth::user()->warehouse)
        ->sum('qty_reject');

        return $qty_material_checks;
    }

    static function getSupplier($document_no,$item_code)
    {
        $material_stock = MaterialStock::where([
            ['document_no',$document_no],
            ['item_code',$item_code],
        ])
        ->first();

        if($material_stock)
        {
            if($material_stock->is_material_others) return null;
        }else
        {
            return null;
        }



        $material_arrival = MaterialArrival::where([
            ['document_no',$document_no],
            ['item_code',$item_code],
        ])
        ->first();

        if($material_arrival)
            return $material_arrival->supplier_name;
        else
            return null;

    }

    static function dataRequirement($po_buyer,$item_code,$style,$article_no)
    {
        $material_requirement = MaterialRequirement::where([
            ['po_buyer',$po_buyer],
            ['item_code',$item_code],
            ['style',$style],
            ['article_no',$article_no]
        ])
        ->first();

        return $material_requirement;
    }

    static function getMaterialArrivalId($id)
    {
        $material_arrivals = DetailMaterialPreparation::where([
            ['material_preparation_id',$id]
        ])
        ->get();

        $array = array();
        foreach ($material_arrivals as $key => $value) {
           $array [] = $value->material_arrival_id;
        }
        return $array;
    }

    static function getLocator($id)
    {
        $locators = Locator::select('code','rack')
      ->whereIn('id',function($query) use ($id)
      {
        $query->select('locator_id')
        ->from('material_stocks')
        ->where('id',$id);
      })
      ->groupby('code','rack')
      ->get();

      $_locator = '';
      foreach ($locators as $key => $value) 
      {
          $_locator .=$value->code.',';
      }
      return substr(trim($_locator), 0, -1);

    }

    static function getSewingPlace($item_id, $po_buyer)
    {
         //alokasi subcont / beda whs 
         $erp = DB::connection('erp');
         $sewing_place = $erp->table('bw_report_material_requirement_new_v1')->where([
             ['poreference',$po_buyer],
             ['componentid',$item_id],
             // ['style',$style],
             // ['kst_articleno',$article_no]
         ])
         ->select('warehouse_place')
         ->first();
        //  dd($item_id.'+'.$po_buyer);
        if($sewing_place != null){
            if($sewing_place->warehouse_place == 'Pihak Ketiga') return 'BBIS';
            elseif($sewing_place->warehouse_place =='Packing AOI 1') return 'AOI 1';
            elseif($sewing_place->warehouse_place =='Packing AOI 2') return 'AOI 2';
            else return '';
        } else{
            return '';  
        }
    }

    static function getBom($item_id, $po_buyer)
    {
        $material_requirements = DB::connection('pgsql')
        ->table('material_requirements')
        ->select(db::raw('promise_date::date as promise_date'))
        ->where([
            ['po_buyer',$po_buyer],
            ['item_id',$item_id]
        ])
        ->first();

        return $material_requirements->promise_date;
    }


}
