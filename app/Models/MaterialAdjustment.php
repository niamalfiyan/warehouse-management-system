<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialAdjustment extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['barcode','item_code','category','item_desc','material_preparation_id','po_buyer', 'uom_conversion','qty_before_adjustment','qty_adjustment','qty_after_adjustment',
    'type_po','user_id','deleted_at'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialPreparation(){
        return $this->belongsTo('App\Models\MaterialPreparation');
    }
}
