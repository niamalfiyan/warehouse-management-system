<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class PoSupplier extends Model
{
    use Uuids;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at'];
    protected $fillable     = ['c_order_id'
        ,'c_bpartner_id'
        ,'type_stock'
        ,'type_stock_erp_code'
        ,'supplier_id'
        ,'integration_date'
        ,'document_no'
        ,'periode_po'
    ];

    
    public function supplier()
    {
        return $this->belongsTo('App\Models\Supplier');
    }
}
