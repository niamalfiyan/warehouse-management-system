<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class HistoryReceive extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','etd_date','eta_date'];
    protected $guarded = ['id'];
    protected $fillable = ['c_orderline_id','no_packing_list','no_resi','no_surat_jalan','no_invoice','qty_ordered','qty_delivered',
                            'qty_entered','qty_upload','qty_carton','foc','etd_date','eta_date','user_id'];
    
    public function stockLine()
    {
        return $this->belongsTo('App\Models\StockLine','c_orderline_id','c_orderline_id');
    }

    public function MovementLines()
    {
        return $this->hasMany('App\Models\StockMovementLine','po_detail_id','po_detail_id')
        ->join('stocks','stocks.item_id','stock_movement_lines.item_id')
        ->join('stock_movements','stock_movements.id','stock_movement_lines.stock_movement_id')
        ->join('users','users.id','stock_movement_lines.user_id')
        ->where('stock_movements.is_active',true)
        ->where('stock_movement_lines.is_active',true)
        ->orderBy('stock_movement_lines.created_at','asc');
    }

    public function AllMovementLines()
    {
        return $this->hasMany('App\Models\StockMovementLine','po_detail_id','po_detail_id')
        ->join('stocks','stocks.item_id','stock_movement_lines.item_id')
        ->join('stock_movements','stock_movements.id','stock_movement_lines.stock_movement_id')
        ->join('users','users.id','stock_movement_lines.user_id')
        ->orderBy('stock_movement_lines.created_at','asc');
    }
    /*public function stock()
    {
        return $this->belongsToMany('App\Models\Stock','stock_lines','c_orderline_id','stock_id');
    }*/

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function checkStockLeft($stock_movement_id = null)
	{
        $total_subtraction = $this->AllMovementLines()
                            ->where('stock_movement_id',$stock_movement_id)
                            ->sum('subtraction');

		return $total_subtraction;
	}
}
