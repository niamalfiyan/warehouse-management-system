<?php

namespace App\Models;

use App\Uuids;
use Illuminate\Database\Eloquent\Model;
use App\Models\Perseroan;
use App\Models\KepalaProduksi;

class Kontrak extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $fillable=[
      'user_id',
      'no_kontrak',
      'tanggal',
      'perseroan_id',
      'kepala_produksi_id',
    ];
    public function perseroan()
    {
        return $this->belongsTo(Perseroan::class);
    }
    public function kepalaproduksi()
    {
        return $this->belongsTo(KepalaProduksi::class,'kepala_produksi_id','id');
    }
}
