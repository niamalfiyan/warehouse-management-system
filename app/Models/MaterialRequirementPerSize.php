<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Uuids;

class MaterialRequirementPerSize extends Model
{
    use Uuids;
    public $timestamps          = false;
    protected $table            = 'material_requirement_per_size_v';
}
