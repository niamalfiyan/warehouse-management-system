<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
class Contract extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $fillable=[
      'user_id',
      'no_kontrak',
      'po_buyer',
    ];
}
