<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MasterDataFabricTesting extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at'];
    protected $guarded = ['id'];
    protected $fillable = ['testing_classification','method','testing_name','testing_code','min','max','requirement', 'detail_requirement','calculation_method', 'operator', 'deleted_at','user_id'];

    public function detailMasterDataFabricTesting()
    {
        return $this->hasMany('App\Models\DetailMasterDataFabricTesting');
    }
}
