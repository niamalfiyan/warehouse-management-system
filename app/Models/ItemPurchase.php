<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class ItemPurchase extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at'];
    protected $fillable = ['po_supplier_id','document_no','item_code','category'];

}
