<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MaterialRequirementPerPart extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $dates = ['created_at','updated_at','statistical_date','lc_date','promise_date'];
    protected $fillable = ['po_buyer'
    ,'item_id'
    ,'item_code'
    ,'article_no'
    ,'is_piping'
    ,'style'
    ,'category'
    ,'uom'
    ,'part_no'
    ,'qty_required'
    ,'statistical_date'
    ,'lc_date',
    'warehouse_id',
    'component',
    'promise_date',
    'destination_name',
    'warehouse_name'];

    public function getTotalPreparationFabric($po_buyer,$item_id,$style,$article_no,$part_no,$is_piping)
    {
        $reserved_qty = DetailPoBuyerPerRoll::where( [
            ['po_buyer',$po_buyer],
            ['style',$style],
            ['article_no',$article_no],
            ['part_no',$part_no],
        ])
        ->whereIn('detail_material_preparation_fabric_id',function($query) use($item_id,$is_piping,$article_no,$style)
        {
            $query->select('id')
            ->from('detail_material_preparation_fabrics')
            ->where([
                ['item_id',$item_id]
            ])
            ->whereIn('material_preparation_fabric_id',function($query2) use($is_piping,$article_no,$item_id)
            {
                $query2->select('id')
                ->from('material_preparation_fabrics')
                ->where([
                    ['item_id_book',$item_id],
                    ['is_piping',$is_piping],
                    ['article_no',$article_no],
                ]);
            });
        })
        ->sum('qty_booking');

        return $reserved_qty;

    }

    static function getQtyAllocated($po_buyer,$item_code,$style,$part_no){
        $qty_allocated = DB::table('material_planning_fabric_pipings')
        ->where('part_no',$part_no)
        ->whereIn('material_planning_fabric_id',function($query) use ($po_buyer,$item_code,$style) {
            $query->select('id')
            ->from('material_planning_fabrics')
            ->where([
                ['po_buyer',$po_buyer],
                ['item_code',$item_code],
                ['style',$style],
            ]);
        })
        ->sum('qty_booking');

        return $qty_allocated;
    }

}
