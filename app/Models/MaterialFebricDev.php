<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaterialFebricDev extends Model
{
      protected $connection = 'dev_web_po';
      protected $guarded = ['id'];
      protected $table = 'm_material';
}
