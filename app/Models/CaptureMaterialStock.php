<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class CaptureMaterialStock extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['warehouse'
    ,'item_id'
    ,'planning_date'
    ,'document_no'
    ,'item_code'
    ,'po_buyer'
    ,'locator'
    ,'nomor_roll'
    ,'barcode'
    ,'qty'
    ,'uom'
    ,'status_stock'
    ,'category'
    ,'created_at'
    ,'updated_at'];

    protected $dates = ['created_at'];
}
