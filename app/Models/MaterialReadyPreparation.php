<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class MaterialReadyPreparation extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at','preparation_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_arrival_id', 'item_id', 'item_code','item_desc','category','document_no','po_buyer','c_bpartner_id','c_order_id','c_orderline_id','supplier_name','user_id','deleted_at','type_po','user_preparation_id'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function detailMaterialReadyPreparation(){
        return $this->hasMany('App\Models\DetailMaterialReadyPreparation');
    }

    public function MaterialPreparation(){
        return $this->hasMany('App\Models\MaterialPreparation');
    }

    public function materialArrival(){
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function getPOBuyer($document_no,$item_code,$category,$uom){
        $po_buyer = array();
        $material_arrival_id = array();
        $material_ready_preparations = db::table('material_ready_preparations')
        ->select('material_arrival_id','material_arrivals.po_detail_id','material_ready_preparations.po_buyer','material_requirements.statistical_date')
        ->leftjoin('material_arrivals','material_arrivals.id','material_ready_preparations.material_arrival_id')
        ->leftjoin('material_requirements','material_requirements.po_buyer','material_ready_preparations.po_buyer')
        ->where([
            ['material_ready_preparations.document_no',$document_no],
            ['material_ready_preparations.item_code',$item_code],
            ['material_ready_preparations.category',$category],
            ['material_arrivals.prepared_status','NEED PREPARED']
        ])
        ->groupby('material_arrival_id','material_ready_preparations.po_buyer','material_requirements.statistical_date','material_arrivals.po_detail_id')
        ->orderby('material_requirements.statistical_date','material_ready_preparations.po_buyer')
        ->get();

        foreach ($material_ready_preparations as $key => $value) {
           $material_preparation =  db::table('material_preparations')
            ->where('barcode',$value->po_detail_id)
            ->exists();

            if(!$material_preparation)
                $material_arrival_id [] = $value->material_arrival_id;
            
            //$po_detail_id [] = $value->po_detail_id;
            $po_buyer [] = $value->po_buyer;
        }

        $obj = new StdClass();
        $obj->material_arrival_id = $material_arrival_id;
        $obj->po_buyer = array_unique($po_buyer);
       // $obj->po_detail_id = $po_detail_id;
        $obj->uom_conversion = $this->conversion($item_code,$category,$uom)->uom_to;
        $obj->dividerate = $this->conversion($item_code,$category,$uom)->dividerate;
        $obj->multiplyrate = $this->conversion($item_code,$category,$uom)->multiplyrate;
        
        return $obj;
    }

    public function getPlanning($po_buyer,$style,$item_code,$document_no){
        $material_planning_fabric = db::table('material_planning_fabrics')
        ->where([
            ['document_no',$document_no],
            ['item_code',$item_code],
            ['po_buyer',$po_buyer],
            ['style',$style],
        ])
        ->whereNull('deleted_at')
        ->first();

        if($material_planning_fabric){
            $user = db::table('users')
            ->where('id',$material_planning_fabric->user_id)
            ->first();

            $obj = new stdClass();
            $obj->total_reserved_qty = $material_planning_fabric->total_reserved_qty;
            $obj->user_name = $user->name;
            $obj->created_at = $material_planning_fabric->created_at;
            return $obj;
        }else{
            return false;
        }
        
    }
   
    static function conversion($item_code,$category,$uom){
        $conversion  = DB::table('uom_conversions')
        ->select('uom_to','dividerate','multiplyrate')
        ->where([
            ['item_code', '=', $item_code],
            ['category', '=', $category],
            ['uom_from', '=', $uom],
        ])
        ->first();
        
        $obj = new StdClass();
        if($conversion){
            $obj->uom_to = $conversion->uom_to;
            $obj->dividerate = $conversion->dividerate;
            $obj->multiplyrate = $conversion->multiplyrate;
        }else{
            $obj->uom_to = $uom;
            $obj->dividerate = 1;
            $obj->multiplyrate = 1;
        }
        return $obj;
    }
}
