<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class RackAutomation extends Model
{
    use Uuids;
    protected $connection = 'wms_old';
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['locator_name','group_id','po_buyer'];
}
