<?php

namespace App\Models;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['data','hasCheck','read_at'];
}
