<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;

class UomConversion extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['item_id','item_desc','item_code','category',
                            'uom_from','uom_to','dividerate','multiplyrate'];
    protected $dates = ['created_at'];
}
