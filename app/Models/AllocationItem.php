<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class AllocationItem extends Model
{
    use Uuids;
    //public $timestamps = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','confirm_date','cancel_date','lc_date','mm_approval_date','accounting_approval_date'];
    protected $fillable     = ['item_id_book'
    ,'item_code'
    ,'is_from_allocation_fabric'
    ,'lc_date'
    ,'barcode'
    ,'allocation_id'
    ,'allocation_febric_date'
    ,'category'
    ,'item_desc'
    ,'uom'
    ,'job'
    ,'style'
    ,'_style'
    ,'po_buyer'
    ,'no_po_supplier'
    ,'warehouse'
    ,'qty_booking'
    ,'user_id'
    ,'po_detail_id'
    ,'is_from_purchase'
    ,'summary_stock_fabric_id'
    ,'po_buyer_source'
    ,'c_bpartner_id'
    ,'summary_handover_material_id'
    ,'material_preparation_id'
    ,'is_backlog'
    ,'is_material_switch'
    ,'is_cancel_order'
    ,'supplier_code'
    ,'material_switch_id'
    ,'qty_reject'
    ,'confirm_by_warehouse'
    ,'confirm_date'
    ,'mm_approval_date'
    ,'mm_user_id'
    ,'accounting_approval_date'
    ,'accounting_user_id'
    ,'miscellaneous_item_id'
    ,'barcode'
    ,'article_no'
    ,'confirm_user_id'
    ,'is_additional'
    ,'document_no'
    ,'material_arrival_id'
    ,'material_stock_id'
    ,'c_order_id'
    ,'is_from_allocation_buyer'
    ,'remark'
    ,'created_at'
    ,'updated_at'
    ,'cancel_date'
    ,'cancel_reason'
    ,'cancel_user_id'
    ,'is_planning_fabric_was_pulled'
    ,'is_need_to_handover'
    ,'color'
    ,'warehouse_inventory'
    ,'supplier_name'
    ,'purchase_item_id'
    ,'auto_allocation_id'
    ,'warehouse_cutting_id'
    ,'is_mo_already_created'
    ,'is_not_allocation'
    ,'type_stock_buyer'
    ,'type_stock_material'
    ,'is_reroute'
    ,'old_po_buyer_reroute'
    ,'created_at'
    ,'updated_at'
    ,'item_id_source'
    ,'item_code_source'
    ,'material_stock_per_lot_id'
    ];

    public function materialArrival()
    {
        return $this->belongsTo('App\Models\MaterialArrival');
    }

    public function summaryHandoverMaterial()
    {
        return $this->belongsTo('App\Models\SummaryHandoverMaterial');
    }

    public function summaryStockFabric()
    {
        return $this->belongsTo('App\Models\SummaryStockFabric');
    }

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function materialStockPerLot()
    {
        return $this->belongsTo('App\Models\MaterialStockPerLot');
    }

    public function materialPreparation()
    {
        return $this->hasMany('App\Models\MaterialPreparation');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function confirm_by()
    {
        return $this->belongsTo('App\Models\User','confirm_user_id');
    }

    public function getSupplier($document_no,$item_code,$po_buyer,$style){
        $supplier =  DB::table('allocation_items')
        ->leftjoin('suppliers','suppliers.c_bpartner_id','allocation_items.c_bpartner_id')
        ->select('allocation_items.c_bpartner_id','suppliers.supplier_name')
        ->where([
            [db::raw('upper(allocation_items.document_no)'),strtoupper($document_no)],
            ['allocation_items.item_code',$item_code],
            ['allocation_items.po_buyer',$po_buyer],
            ['allocation_items.style',$style],
        ])
        ->groupby('allocation_items.c_bpartner_id','suppliers.supplier_name')
        ->first();


        if($supplier){
            return $supplier;
        }else{
            return null;
        }
    }

    public function getLocator($document_no,$item_code){
        $stocks =  DB::table('material_stocks')
        ->select('locator_id')
        ->where(function($query){
            $query->where('is_general_item',true)
            ->orWhere('is_material_others',true);
        })
        ->where([
            ['is_allocated',false],
            ['item_code',$item_code],
            ['document_no',$document_no],
            ['warehouse_id',auth::user()->warehouse],
        ])
        ->whereNull('deleted_at')
        ->groupby('locator_id')
        ->get();

        $_locator = '';
        
        if($stocks->count() > 0){
            $_status_po = 'CLOSE';
            foreach ($stocks as $key_ms => $stock){
                $locator = Locator::where('id',$stock->locator_id)->first();
                if($locator)
                    $_locator .= $locator->code.',';
            }
        }else{
            $_status_po = 'OPEN';
        }
        
        $obj = new stdClass();
        $obj->locator = $_locator;
        $obj->status_po = $_status_po;

        return $obj;
    }

    public function getLocatorFabric($summary_stock_fabric_id){
        $stocks =  DB::table('material_stocks')
        ->select('locator_id')
        ->where([
            ['summary_stock_fabric_id',$summary_stock_fabric_id],
            ['is_allocated',false],
        ])
        ->whereNull('deleted_at')
        ->groupby('locator_id')
        ->get();

        $_locator = '';
        foreach ($stocks as $key => $value) {
            $id = $value->locator_id;
            $data = Locator::find($id);
            $_locator .= ($data)? $data->code:null .',';
        }

        return substr($_locator,0,-1);
    }

    public function getTotalAllocation($allocation_item_id)
    {
        $total_allocation =  DetailMaterialPlanningFabric::where('allocation_item_id',$allocation_item_id)
        ->sum('qty_booking');

        return $total_allocation;
    }
}
