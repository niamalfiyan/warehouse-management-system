<?php

namespace App\Models\Lab;
use DB;
use Auth;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class TrfTestingDocument extends Model
{
    use Uuids;
    protected $connection   = 'lab';
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $dates        = ['created_at','updated_at','deleted_at'];
    protected $fillable     = ['trf_id'
                            ,'document_type'
                            ,'document_no'
                            ,'style'
                            ,'article_no'
                            ,'size'
                            ,'color'
                            ,'fibre_composition'
                            ,'fabric_finish'
                            ,'gauge'
                            ,'fabric_weight'
                            ,'plm_no'
                            ,'care_instruction'
                            ,'manufacture_name'
                            ,'export_to'
                            ,'nomor_roll'
                            ,'batch_number'
                            ,'created_at'
                            ,'updated_at'
                            ,'deleted_at'
                            ,'is_repetation'
                            ,'item'
                            ,'barcode_supplier'];
    protected $table        = 'trf_testing_document';
}
