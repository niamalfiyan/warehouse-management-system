<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    use Uuids;
    public $timestamps      = false;
    public $incrementing    = false;
    protected $guarded      = ['id'];
    protected $fillable     = ['name'
    ,'user_id'
    ,'warehouse'
    ,'is_active'
    ,'is_destination'
    ,'is_subcont'
    ,'created_at'
    ,'updated_at'
    ,'deleted_at'
    ,'type_po'
    ,'erp_id'];

    protected $dates = ['created_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function locators()
    {
        return $this->hasMany('App\Models\Locator','area_id');
    }
    
     public function getLocatorsStoAttribute($area_id,$rack)
     {
       $locator_details = db::table('locators')
        ->where([
            ['locators.is_active',true],
            ['locators.rack',$rack],
            ['locators.area_id',$area_id],
        ])
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->get();

        $array = array();
        foreach ($locator_details as $key => $locator_detail) {
            $obj = new stdClass();
            $obj->id = $locator_detail->id;
            $obj->code = $locator_detail->code;
            $obj->area_id = $locator_detail->area_id;
            $obj->rack = $locator_detail->rack;
            $obj->y_row = $locator_detail->y_row;
            $obj->z_column = $locator_detail->z_column;
            $obj->barcode = $locator_detail->barcode;
            $obj->counter_in = $locator_detail->counter_in;
            $obj->last_sto_date = ($locator_detail->last_sto_date)? $locator_detail->last_sto_date : null;
            $array [] = $obj;
        }
        return $array;
    }

    public function getLocatorsAttribute($area_id,$rack,$has_many_po_buyer,$filtering_locator)
    {
       $locator_details = Locator::where([
            ['locators.is_active',true],
            ['locators.rack',$rack],
            ['locators.rack','<>','ERP INVENTORY'],
            ['locators.area_id',$area_id],
        ]);

        if($filtering_locator) $locator_details =  $locator_details->whereIn('id',$filtering_locator);

        $locator_details = $locator_details->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->get();

        $array = array();
        foreach ($locator_details as $key => $locator_detail) 
        {
            $obj                = new stdClass();
            $obj->id            = $locator_detail->id;
            $obj->code          = $locator_detail->code;
            $obj->area_id       = $locator_detail->area_id;
            $obj->rack          = $locator_detail->rack;
            $obj->y_row         = $locator_detail->y_row;
            $obj->z_column      = $locator_detail->z_column;
            $obj->barcode       = $locator_detail->barcode;
            $obj->counter_in    = $locator_detail->counter_in;
            $obj->po_buyer      = $locator_detail->last_po_buyer;
            $array [] = $obj;
        }

        return $array;
    }

    public function listLocator($area_id = null,$type_po = null){
        $locator = DB::table('locators')
            ->select('rack','has_many_po_buyer')
            ->where('locators.is_active',true)
            ->where('locators.area_id',$area_id)
            ->orderBy('rack','asc')
            ->groupBy('rack','has_many_po_buyer')
            ->get();

        $array = array();
        foreach ($locator as $key => $value_header) {

            $details = ($type_po == 2)? $this->locatorDetails($area_id,$value_header->rack): $this->locatorFebricDetails($area_id,$value_header->rack);
            $array [] = [
                'header' => $value_header->rack,
                'has_many_po_buyer' => $value_header->has_many_po_buyer,
                'details' => $details
            ];
        }
        return $array;
    }

    public function listLocatorFiltering($area_id = null,$type_po = null, $where = null)
    {
        if($type_po == 1)
        {
            $item_code  = $where;
            $locator    = Locator::where('is_active',true)
            ->where('area_id',$area_id)
            ->whereIn('id',function($query) use($item_code){
                $query->select('locator_id')
                ->from('material_stocks')
                ->where([
                    ['warehouse_id',auth::user()->warehouse],
                    ['item_code','LIKE',"%$item_code%"],
                ])
                ->whereNotNull('locator_id')
                ->groupby('locator_id');
            })
            ->orderBy('rack','asc')
            ->groupBy('rack')
            ->get();

            $array = array();
            foreach ($locator as $key => $value_header) 
            {
                $array [] = [
                    'header'            => $value_header->rack,
                    'has_many_po_buyer' => false,
                    'details'           => $this->locatorFabricDetailsFiltering($area_id,$value_header->rack, $item_code)
                ];
            }
            return $array;
        }else if ($type_po == 2)
        {
            $item_code = isset($where['item_code']) ? $where['item_code']: null;
            $po_buyer = isset($where['po_buyer']) ? $where['po_buyer']: null;

            $locator = DB::table('locators')
            ->select('rack','has_many_po_buyer')
            ->where([
                ['locators.is_active',true],
                ['locators.area_id',$area_id],
            ]);

            if($po_buyer){
                $locator = $locator->whereRaw('
                    id in (
                        select to_destination
                        from material_movements
                        where ( material_movements.status = \'in\' or material_movements.status = \'change\' )
                            and id in (
                                select material_movement_id
                                from material_movement_lines
                                where material_movement_lines.is_active = true
                                and material_preparation_id in (
                                    select id
                                    from material_preparations
                                    where po_buyer = \''.$po_buyer.'\'
                                    and deleted_at is null
                                    group by id
                                )
                                and material_movements.is_active = true
                                and deleted_at is null
                                group by material_movement_id

                            )
                        group by to_destination
                    )
                ');
            }

            if($item_code){
                $locator = $locator->whereRaw('
                    id in (
                        select to_destination
                        from material_movements
                        where ( material_movements.status = \'in\' or material_movements.status = \'change\' )
                            and id in (
                                select material_movement_id
                                from material_movement_lines
                                where material_movement_lines.is_active = true
                                and material_preparation_id in (
                                    select id
                                    from material_preparations
                                    where item_code = \''.$item_code.'\'
                                    and deleted_at is null
                                    group by id
                                )
                                and material_movements.is_active = true
                                and deleted_at is null
                                group by material_movement_id

                            )
                        group by to_destination
                    )
                ');
            }

            if($po_buyer && $item_code){
                $locator = $locator->whereRaw('
                    id in (
                        select to_destination
                        from material_movements
                        where ( material_movements.status = \'in\' or material_movements.status = \'change\' )
                            and id in (
                                select material_movement_id
                                from material_movement_lines
                                where material_movement_lines.is_active = true
                                and material_preparation_id in (
                                    select id
                                    from material_preparations
                                    where item_code = \''.$item_code.'\' and po_buyer = \''.$po_buyer.'\'
                                    and deleted_at is null
                                    group by id
                                )
                                group by material_movement_id

                            )
                        and material_movements.is_active = true
                        and deleted_at is null
                        group by to_destination
                    )
                ');
            }

            $locator = $locator->orderBy('rack','asc')
            ->groupBy('rack','has_many_po_buyer')
            ->get();

            $array = array();
            foreach ($locator as $key => $value_header) {
                $array [] = [
                    'header' => $value_header->rack,
                    'has_many_po_buyer' => $value_header->has_many_po_buyer,
                    'details' => $this->locatorDetails($area_id,$value_header->rack,$where)
                ];
            }
            return $array;
        }
    }

    public function locatorDetails($area_id = null, $rack = null){
        $locator_details = db::table('locators')
        ->where([
            ['locators.is_active',true],
            ['locators.area_id',$area_id],
            ['locators.rack',$rack],
            ['locators.rack','<>','ERP INVENTORY'],
        ])
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->get();

        $array = array();
        foreach ($locator_details as $key => $locator_detail) {
            $obj = new stdClass();
            $obj->id = $locator_detail->id;
            $obj->code = $locator_detail->code;
            $obj->area_id = $locator_detail->area_id;
            $obj->rack = $locator_detail->rack;
            $obj->y_row = $locator_detail->y_row;
            $obj->z_column = $locator_detail->z_column;
            $obj->barcode = $locator_detail->barcode;
            $obj->counter_in = $locator_detail->counter_in;
            $obj->po_buyer = ($locator_detail->has_many_po_buyer)? 'HAS MANY PO BUYER' : 'aaa' ;
            $array [] = $obj;
        }
        return $array;
    }

    static function get_po_buyer($id){
        $material_preparation =  DB::table('material_preparations')
        ->select('po_buyer')
        ->where([
            ['last_locator_id',$id],
            ['last_status_movement','in']
        ])
        ->groupby('po_buyer')
        ->first();

        if($material_preparation)
            return $material_preparation->po_buyer;
        
    }

    static function locatorFabricDetailsFiltering($area_id = null, $rack = null, $item_code = null)
    {
        $locator_detail =Locator::select('locators.area_id'
        ,'locators.id'
        ,'locators.rack'
        ,'locators.y_row'
        ,'locators.z_column'
        ,db::raw("count(material_stocks.item_code) as total_item"))
        ->leftJoin('material_stocks', 'material_stocks.locator_id','locators.id')
        ->where([
            ['locators.is_active',true],
            ['locators.area_id',$area_id],
            ['locators.rack',$rack],
            ['material_stocks.warehouse_id',auth::user()->warehouse],
            ['material_stocks.item_code','LIKE',"%$item_code%"]
        ])
        ->whereNotNull('material_stocks.locator_id')
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->groupBy('locators.id'
        ,'locators.area_id'
        ,'locators.rack'
        , 'locators.y_row'
        ,'locators.z_column')
        ->get();

        return $locator_detail;
    }

    static function locatorFebricDetails($area_id = null, $rack = null){
        $locator_detail = DB::table('locators')
        ->selectRaw('locators.area_id, locators.id, locators.rack, locators.y_row,
            locators.z_column, count(material_stocks.item_code) as total_item')
        ->leftjoin('material_stocks', 'material_stocks.locator_id', 'locators.id')
        ->where([
            ['locators.is_active',true],
            ['locators.area_id',$area_id],
            ['locators.rack',$rack]
        ])
        ->orderBy('rack','asc')
        ->orderBy('y_row','asc')
        ->orderBy('z_column','asc')
        ->groupBy('locators.id','locators.area_id','locators.rack', 'locators.y_row','locators.z_column')
        ->get();

        return $locator_detail;
        
    }

    public function getRowLocator($area_id = null){
        $locator = DB::table('locators')
            ->select('locators.rack','locators.y_row as row','locator_column.start_colum','locator_column.end_colum','locator_column.end_colum as system_colum','locator_column.has_many_po_buyer')
            ->leftjoin(DB::raw("(
                SELECT
                    locators.rack,
                    locators.y_row,
                    min(locators.z_column) as start_colum,
                    max(locators.z_column) as end_colum,
                    has_many_po_buyer
                FROM locators
                WHERE area_id = '".$area_id."'
                GROUP BY locators.rack, locators.y_row,has_many_po_buyer
                order by locators.rack) as locator_column"
            ),function($join){
                $join->on("locator_column.rack","=","locators.rack");
                $join->on("locator_column.y_row","=","locators.y_row");
            })
            ->where('locators.is_active',true)
            ->where('locators.area_id',$area_id)
            ->orderBy('locators.rack','asc')
            ->orderBy('locators.y_row','desc')
            ->groupBy('locators.rack','locators.y_row','locator_column.start_colum','locator_column.end_colum','locator_column.has_many_po_buyer')
            ->get();
        return $locator;

    }

     public function getRowDestination($area_id = null){
        $locator = DB::table('locators')
            ->select('locators.area_id','locators.rack',db::raw('count(0) as total_y_row'),db::raw('count(0) as system_total'))
            ->where('locators.is_active',true)
            ->where('locators.area_id',$area_id)
            ->orderBy('locators.rack','asc')
            ->groupBy('locators.area_id','locators.rack')
            ->get();

        return $locator;

    }

}
