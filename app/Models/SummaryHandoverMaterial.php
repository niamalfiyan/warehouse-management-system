<?php namespace App\Models;

use DB;
use Auth;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class SummaryHandoverMaterial extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','updated_at','deleted_at','complete_date'];
    protected $guarded = ['id'];
    protected $fillable = ['c_bpartner_id'
    ,'deleted_user_id'
    ,'c_order_id'
    ,'item_id'
    ,'document_no'
    ,'supplier_name'
    ,'item_code'
    ,'total_qty_handover'
    ,'total_qty_outstanding'
    ,'handover_document_no'
    ,'is_move_order'
    ,'total_qty_supply'
    ,'deleted_at'
    ,'complete_date'
    ,'warehouse_id'
    ,'uom'
    ,'no_bc'
    ,'no_kk'
    ,'no_surat_jalan'
    ,'tanggal_kirim'
    ,'document_no_out'
    ,'is_subcont'];

    public function materialRollHandoverFabric()
    {
        return $this->hasMany('App\Models\MaterialRollHandoverFabric');
    }

}
