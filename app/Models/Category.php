<?php namespace App\Models;

use App\Uuids;
use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['name','uom','code','is_prepared','user_id','deleted_at'];
    protected $dates = ['created_at','deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
