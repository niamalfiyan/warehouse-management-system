<?php namespace App\Models;



use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialStock extends Model
{
    use Uuids;
    public $incrementing    = false;
    public $timestamps      = false;
    protected $dates        = ['created_at','deleted_at'];
    protected $guarded      = ['id'];
    protected $fillable     = ['material_stock_id'
    ,'material_arrival_id'
    ,'material_preparation_id'
    ,'remark'
    ,'uom'
    ,'qty'
    ,'availability_qty'
    ,'reserved_qty'
    ,'user_id'
    ,'created_at'
    ,'created_at'
    ,'updated_at'
    ,'locator_id'
    ,'mm_user_id'
    ,'accounting_user_id'
    ,'approve_date_mm'
    ,'approve_date_accounting'
    ,'reject_user_id'
    ,'reject_date'
    ,'source'];

    public function materialStock()
    {
        return $this->belongsTo('App\Models\MaterialStock');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
