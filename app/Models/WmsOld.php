<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Uuids;
use DB;

class WmsOld extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $guarded = ['id'];
    protected $fillable = ['po_buyer','status','style', 'locator','user_name','item_code','checkout_date','qty_in'];
    protected $dates = ['created_at','checkout_date'];
}
