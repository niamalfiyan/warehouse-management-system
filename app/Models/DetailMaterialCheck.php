<?php namespace App\Models;

use DB;
use StdClass;
use App\Uuids;
use Illuminate\Database\Eloquent\Model;

class DetailMaterialCheck extends Model
{
    use Uuids;
    public $incrementing = false;
    protected $dates = ['created_at','preparation_date'];
    protected $guarded = ['id'];
    protected $fillable = ['material_check_id','start_point_check', 'end_point_check','defect_code', 'defect_value','is_selected_1',
    'is_selected_2','is_selected_3','is_selected_4','user_id','deleted_at','multiply','total_point_defect','remark'];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function materialCheck(){
        return $this->belongsTo('App\Models\MaterialCheck');
    }
}
