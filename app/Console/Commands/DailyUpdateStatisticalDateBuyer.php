<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\ErpMaterialRequirementController;

class DailyUpdateStatisticalDateBuyer extends Command
{
    protected $signature = 'dailyStatisticalDate:update';
    protected $description = 'Update Statistical Date From of Po Buyers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->info('START SCHEDULE UPDATE STATISTICAL DATE AT '.carbon::now());
        ErpMaterialRequirementController::dailyUpdateStatisticalDate();
        $this->info('SCHEDULE UPDATE STATISTICAL DATE HAS BEEN SUCCESSFULLY AT '.carbon::now());
    }
}
