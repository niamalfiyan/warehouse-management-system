<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\MasterDataAutoAllocationController;

use App\Models\Scheduler;

class ExportFormUpdateAllocation extends Command
{
    protected $signature = 'exportFormUpdate:allocation';
    protected $description = 'Export Form Update Allocation';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_export_acc = Scheduler::where('job','SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_ACC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_export_acc)){
            $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION ACC JOB AT '.carbon::now());
            $this->setStatus($scheduler_export_acc,'ongoing');
            $this->setStartJob($scheduler_export_acc);
            MasterDataAutoAllocationController::exportUpdateFileAccExcel();
            $this->setStatus($scheduler_export_acc,'done');
            $this->setEndJob($scheduler_export_acc);
            $this->info('DONE EXPORT FORM UPLOAD ALLOCATION ACC JOB AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_ACC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_ACC',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION ACC JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                MasterDataAutoAllocationController::exportUpdateFileAccExcel();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE EXPORT FORM UPLOAD ALLOCATION ACC JOB AT '.carbon::now());
            }else{
                $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION ACC SEDANG BERJALAN');
            }
        }

        $scheduler_export_fab = Scheduler::where('job','SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_ACC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_export_fab)){
            $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION FAB JOB AT '.carbon::now());
            $this->setStatus($scheduler_export_fab,'ongoing');
            $this->setStartJob($scheduler_export_fab);
            MasterDataAutoAllocationController::exportUpdateFileFabExcel();
            $this->setStatus($scheduler_export_fab,'done');
            $this->setEndJob($scheduler_export_fab);
            $this->info('DONE EXPORT FORM UPLOAD ALLOCATION FAB JOB AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_FAB')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_EXPORT_FORM_UPLOAD_ALLOCATION_FAB',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION FAB JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                MasterDataAutoAllocationController::exportUpdateFileFabExcel();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE EXPORT FORM UPLOAD ALLOCATION FAB JOB AT '.carbon::now());
            }else{
                $this->info('SYNC EXPORT FORM UPLOAD ALLOCATION FAB SEDANG BERJALAN');
            }
        }
       
        
        $this->info('Export has been successfully');
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
