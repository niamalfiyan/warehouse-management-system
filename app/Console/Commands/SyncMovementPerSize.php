<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Locator;
use App\Models\PoBuyer;
use App\Models\Scheduler;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\MaterialMovementPerSize;
use App\Models\MaterialRequirementPerSize;
use App\Models\DetailMaterialRequirement;

class SyncMovementPerSize extends Command
{
    protected $signature    = 'sync:movementPerSize';
    protected $description  = 'Sinkron movement per size';

    public function __construct()
    {
        parent::__construct();
    }

    
    public function handle()
    {
        //$this->doJob();
        $is_schedule_on_going = Scheduler::where('job','SYNC_MOVEMENT_PER_SIZE')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going){
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_MOVEMENT_PER_SIZE',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC MOVEMENT PER SIZE START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->doJob();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC MOVEMENT PER SIZE END JOB AT '.carbon::now());
        } 
    }

    private function doJob()
    {
        // $data           = DB::table('outstanding_material_movement_per_size_v')->take(20)->get();
        $data           = DB::select(db::raw("
            select* From outstanding_material_movement_per_size_v
            where to_char(date_movement,'YYYYMM') = '202109'
            and total_per_size < qty_movement
            order by date_movement asc
        "
        ));

        $insert_date    = Carbon::now()->todatetimestring();
        $today          = Carbon::now()->format('Y-m-d');

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_movement_line_id  = $value->id;;
                $material_movement_line     = MaterialMovementLine::find($material_movement_line_id);

                if($material_movement_line)
                {
                    $po_buyer       = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->po_buyer : null);
                    $item_id        = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id : null);
                    $item_id_source = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id_source : null);
                    $qty_movement   = sprintf('%0.8f',$material_movement_line->qty_movement - $material_movement_line->totalQtyAllocatedPerSized($material_movement_line->id));
                    $created_at     = $material_movement_line->created_at;
                    $movement_out   = $material_movement_line->created_at->format('Y-m-d');

                    $_master_po_buyer = PoBuyer::select('statistical_date','lc_date','promise_date')
                    ->where('po_buyer',$po_buyer)
                    ->groupby('statistical_date','lc_date','promise_date')
                    ->first();

                    
                    // if($movement_out < $today) 
                    // {
                    //     $integration_date   = $insert_date;
                    //     $is_integrate       = true;
                    //     $remark_integration = 'lgsg di true kan karna tanggal out lebih kecil dari hari ini';
                    // }else
                    // {
                        $integration_date   = null;
                        $is_integrate       = false;
                        $remark_integration = null;
                    // }

                    echo $material_movement_line_id.' 
                    ';

                    if($po_buyer && $item_id)
                    {
                        $detail_material_requirements = DB::select(db::raw("
                            SELECT detail_material_requirements.po_buyer,
                                detail_material_requirements.item_id,
                                detail_material_requirements.item_code,
                                detail_material_requirements.garment_size,
                                sum(detail_material_requirements.qty_required) AS total_qty
                            FROM detail_material_requirements
                            WHERE po_buyer = '$po_buyer'
                            AND item_id = '$item_id'
                            GROUP BY detail_material_requirements.po_buyer, 
                                detail_material_requirements.item_id, 
                                detail_material_requirements.item_code, 
                                detail_material_requirements.garment_size
                        "));

                        $total_line = count($detail_material_requirements);
                        echo 'total line '.$total_line.'
                        ';
                        if($total_line > 0)
                        {
                            foreach ($detail_material_requirements as $key_line => $detail_material_requirement) 
                            {
                                $size                   = $detail_material_requirement->garment_size;
                                $total_qty              = sprintf('%0.8f',$detail_material_requirement->total_qty);
                                
                                $flag                   = 0;
                                $size_has_outstanding   =  DB::select(db::raw("SELECT EXISTS 
                                    (
                                        SELECT * FROM get_outstanding_per_size
                                        (
                                            '".$po_buyer."',
                                            '".$item_id."'
                                        )
                                        WHERE outstanding > 0 
                                    )"
                                ));
    
                                if($size_has_outstanding[0]->exists)
                                {
                                    $total_allocated    = MaterialMovementPerSize::where([
                                        ['size',$size],
                                        ['po_buyer',$po_buyer],
                                        ['item_id',$item_id],
                                    ])
                                    ->whereNull('reverse_date')
                                    ->sum('qty_per_size');

                                    $total_outstanding = sprintf('%0.8f',$total_qty - $total_allocated);
                                }else $total_outstanding = sprintf('%0.8f',$qty_movement);

                                if($total_outstanding > 0 && $qty_movement >0)
                                {
                                    if ($qty_movement/$total_outstanding >= 1) $supplied = $total_outstanding;
                                    else  $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['created_at',$created_at],
                                            ['updated_at',$insert_date],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
    
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'integration_date'          => $integration_date,
                                                'is_integrate'              => $is_integrate,
                                                'remark_integration'        => $remark_integration,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $insert_date,
                                            ]);
                                        }
    
                                        $qty_movement       -= $supplied;
                                        $total_outstanding  -= $supplied;
                                    }
                                }
                            }
    
                            $material_movement_line->inserted_to_material_movement_per_size_date = $insert_date;
                            $material_movement_line->save();
                        }else
                        {
                            $material_requirements = DB::connection('erp')
                            ->select(db::raw("SELECT style,
                                item_id,
                                item_code,
                                item_desc,
                                uom,
                                po_buyer,
                                job_order,
                                qty_required,
                                article_no,
                                is_piping,
                                season,
                                garment_size,
                                part_no,
                                warehouse_id,
                                warehouse_name,
                                city_name,
                                component,
                                category,
                                supp_code,
                                supp_name,
                                qty_ordered_garment 
                                FROM get_wms_material_requirement_detail_new('".$po_buyer."') 
                                where item_id = '$item_id';"
                            ));

                            if(count($material_requirements) > 0)
                            {
                                DetailMaterialRequirement::where('po_buyer',$po_buyer)
                                ->where('item_id',$item_id)
                                ->delete();
                                
                                foreach ($material_requirements as $key2 => $material_requirement) 
                                {
                                    DetailMaterialRequirement::FirstOrCreate([
                                        'season'              => $material_requirement->season,
                                        'po_buyer'            => $material_requirement->po_buyer,
                                        'article_no'          => $material_requirement->article_no,
                                        'item_id'             => $material_requirement->item_id,
                                        'item_code'           => $material_requirement->item_code,
                                        'item_desc'           => $material_requirement->item_desc,
                                        'uom'                 => $material_requirement->uom,
                                        'style'               => $material_requirement->style,
                                        'category'            => $material_requirement->category,
                                        'job_order'           => $material_requirement->job_order,
                                        'statistical_date'    => $_master_po_buyer ? $_master_po_buyer->statistical_date : null,
                                        'lc_date'             => $_master_po_buyer ? $_master_po_buyer->lc_date : null,
                                        'promise_date'        => $_master_po_buyer ? $_master_po_buyer->promise_date : null,
                                        'is_piping'           => $material_requirement->is_piping,
                                        'garment_size'        => $material_requirement->garment_size,
                                        'part_no'             => $material_requirement->part_no,
                                        'qty_required'        => sprintf('%0.8f',$material_requirement->qty_required),
                                        'warehouse_id'        => $material_requirement->warehouse_id,
                                        'warehouse_name'      => $material_requirement->warehouse_name,
                                        'component'           => $material_requirement->component,
                                        'destination_name'    => $material_requirement->city_name,
                                        'supplier_code'       => $material_requirement->supp_code,
                                        'supplier_name'       => $material_requirement->supp_name,
                                        'qty_ordered_garment' => $material_requirement->qty_ordered_garment,
                                    ]);
                                }
                            }
                        }

                        /*
                            di comment dlu biar di usahaiin pake mo issue
                            else
                            {
                                // klo ga ketemu bom per itemnya pake issue
                                $c_order_id                 = $material_movement_line->materialPreparation->c_order_id;
                                $po_detail_id               = $material_movement_line->materialPreparation->po_detail_id;
                                $po_buyer                   = $material_movement_line->materialPreparation->po_buyer;
                                $warehouse_id               = $material_movement_line->materialPreparation->warehouse;
                                $item_code                  = $material_movement_line->materialPreparation->item_code;
                                $c_bpartner_id              = $material_movement_line->materialPreparation->c_bpartner_id;
                                $supplier_name              = $material_movement_line->materialPreparation->supplier_name;
                                $type_po                    = $material_movement_line->materialPreparation->type_po;
                                $document_no                = $material_movement_line->materialPreparation->document_no;
                                $uom                        = $material_movement_line->materialPreparation->uom_conversion;
                                $last_user_movement_id      = $material_movement_line->materialPreparation->last_user_movement_id;
                                $material_preparation_id    = $material_movement_line->material_preparation_id;
                                
                                $inventory_erp = Locator::with('area')
                                ->where([
                                    ['is_active',true],
                                    ['rack','ERP INVENTORY'],
                                ])
                                ->whereHas('area',function ($query) use($warehouse_id)
                                {
                                    $query->where('is_active',true);
                                    $query->where('warehouse',$warehouse_id);
                                    $query->where('name','ERP INVENTORY');
                                })
                                ->first();

                                if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                                {
                                    $c_order_id             = 'FREE STOCK';
                                    $no_packing_list        = '-';
                                    $no_invoice             = '-';
                                    $c_orderline_id         = '-';
                                }else
                                {
                                    $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                    ->whereNull('material_roll_handover_fabric_id')
                                    ->where('po_detail_id',$po_detail_id)
                                    ->first();
                                    
                                    $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                    $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                    $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                                }

                                $is_movement_exists = MaterialMovement::where([
                                    ['from_location',$inventory_erp->id],
                                    ['to_destination',$inventory_erp->id],
                                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                    ['po_buyer',$po_buyer],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['no_packing_list',$no_packing_list],
                                    ['no_invoice',$no_invoice],
                                    ['status','integration-to-inventory-erp'],
                                ])
                                ->first();

                                if(!$is_movement_exists)
                                {
                                    $material_movement = MaterialMovement::firstOrCreate([
                                        'from_location'         => $inventory_erp->id,
                                        'to_destination'        => $inventory_erp->id,
                                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                        'po_buyer'              => $po_buyer,
                                        'is_integrate'          => false,
                                        'is_active'             => true,
                                        'no_packing_list'       => $no_packing_list,
                                        'no_invoice'            => $no_invoice,
                                        'status'                => 'integration-to-inventory-erp',
                                        'created_at'            => $created_at,
                                        'updated_at'            => $created_at,
                                    ]);
            
                                    $material_movement_id = $material_movement->id;
                                }else
                                {
                                    $is_movement_exists->updated_at = $created_at;
                                    $is_movement_exists->save();
            
                                    $material_movement_id = $is_movement_exists->id;
                                }

                                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                                ->where([
                                    ['material_movement_id',$material_movement_id],
                                    ['material_preparation_id',$material_preparation_id],
                                    ['qty_movement',-1*$qty_movement],
                                    ['item_id',$item_id],
                                    ['is_integrate',false],
                                    ['is_active',true],
                                    ['warehouse_id',$warehouse_id],
                                    ['date_movement',$created_at],
                                ])
                                ->exists();

                                if(!$is_line_exists)
                                {
                                    MaterialMovementLine::Create([
                                        'material_movement_id'          => $material_movement_id,
                                        'material_preparation_id'       => $material_preparation_id,
                                        'item_code'                     => $item_code,
                                        'item_id'                       => $item_id,
                                        'c_order_id'                    => $c_order_id,
                                        'c_orderline_id'                => $c_orderline_id,
                                        'c_bpartner_id'                 => $c_bpartner_id,
                                        'supplier_name'                 => $supplier_name,
                                        'type_po'                       => $type_po,
                                        'is_integrate'                  => false,
                                        'is_active'                     => true,
                                        'uom_movement'                  => $uom,
                                        'integration_date'              => $integration_date,
                                        'is_integrate'                  => $is_integrate,
                                        'remark_integration'            => $remark_integration,
                                        'qty_movement'                  => -1*$qty_movement,
                                        'date_movement'                 => $created_at,
                                        'created_at'                    => $created_at,
                                        'updated_at'                    => $created_at,
                                        'date_receive_on_destination'   => $created_at,
                                        'nomor_roll'                    => '-',
                                        'warehouse_id'                  => $warehouse_id,
                                        'document_no'                   => $document_no,
                                        'note'                          => 'INTEGRASI INTERNAL USE, KARENA KETIKA OUT ISSUE TIDAK KETEMU DI BOM',
                                        'is_active'                     => true,
                                        'user_id'                       => $last_user_movement_id,
                                    ]);
                                }

                                $material_movement_line->is_inserted_to_material_movement_per_size = false;
                                $material_movement_line->save();
                            }

                        */
                        
                    }
                    
                }

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
