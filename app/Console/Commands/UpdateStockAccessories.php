<?php namespace App\Console\Commands;

use DB;
use Illuminate\Console\Command;

use Carbon\Carbon;

use App\Models\User;
use App\Models\Item;
use App\Models\PoBuyer;
use App\Models\Supplier;
use App\Models\MappingStocks;
use App\Models\UomConversion;
use App\Models\MaterialStock;
use App\Models\AutoAllocation;
use App\Models\MaterialSubcont;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialStock;
use App\Models\MaterialPreparation;

class UpdateStockAccessories extends Command
{
    protected $signature = 'updateStockAcc:update';
    protected $description = 'Update stock Acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try 
        {
            DB::beginTransaction();
            $data                                   = DB::table('incase_stock_acc')->get();
            $stock_reserved_accessories             = array();

            foreach ($data as $key => $value) 
            {
                $material_arrival_id    = $value->id;
                $warehouse_id           = $value->warehouse_id;
                $material_arrivals = MaterialArrival::select('id','po_detail_id','created_at','warehouse_id','item_id','material_subcont_id','po_buyer','supplier_name','document_no','c_bpartner_id','item_code','c_order_id','uom','qty_upload','qty_carton')
                ->where([
                    ['warehouse_id',$warehouse_id],
                    ['id',$material_arrival_id],
                ])
                ->groupby('id','warehouse_id','document_no','po_detail_id','created_at','c_bpartner_id','item_id','item_code','material_subcont_id','po_buyer','supplier_name','c_order_id','uom','qty_upload','qty_carton')
                ->get();

                foreach ($material_arrivals as $key => $value) 
                {
                    $upload_date            = $value->created_at; 
                    $po_detail_id           = $value->po_detail_id; 
                    $po_buyer               = $value->po_buyer; 
                    $material_subcont_id    = $value->material_subcont_id; 
                    $material_arrival_id    = $value->id; 
                    $qty_upload             = $value->qty_upload; 
                    $qty_carton             = $value->qty_carton; 
                    $warehouse_id           = $value->warehouse_id; 
                    $document_no            = $value->document_no; 
                    $c_bpartner_id          = $value->c_bpartner_id; 
                    $item_code              = strtoupper($value->item_code); 
                    $c_order_id             = $value->c_order_id; 
                    $uom                    = $value->uom;
                    $has_many_po_buyer      = strpos($po_buyer, ",");

                    $is_running_stock       = false;

                    if($material_subcont_id)
                    {
                        $material_subcont       = MaterialSubcont::find($material_subcont_id);
                        $source                 = 'RESERVED HANDOVER STOCK';
                        $remark_detail_stock    = 'KEDATANGAN DARI PINDAH TANGAN';
                        $is_running_stock       = true;
                        $po_detail_id           = $material_subcont->po_detail_id; 
                    }else
                    {
                        if($po_buyer != '' || $po_buyer != null)
                        {
                            $source                 = 'RESERVED STOCK';
                            $is_running_stock       = true;
                        }else
                        {
                            $source                 = 'NON RESERVED STOCK';
                        }
                        $remark_detail_stock    = 'KEDATANGAN DARI SUPPLIER';
                    }

                    $system = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();

                    $is_pfao         = substr($document_no,0, 4); 
                    $_item           = Item::where(db::raw('upper(item_code)'),$item_code)->first();
                    $item_id         = ($_item)? $_item->item_id : $value->item_id;
                    $upc_item        = ($_item)? $_item->upc : 'MASTER ITEM TIDAK DITEMUKAN';
                    $item_desc       = ($_item)? $_item->item_desc : 'MASTER ITEM TIDAK DITEMUKAN';
                    $category        = ($_item)? $_item->category : 'MASTER ITEM TIDAK DITEMUKAN';
                    $supplier        = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                    $supplier_code   = ($supplier)? strtoupper($supplier->supplier_code) : 'MASTER SUPPLIER NOT FOUND';
                    $supplier_name   = ($supplier)? strtoupper($supplier->supplier_name) : $value->supplier_name;
                
                    if($document_no == 'FREE STOCK' || $document_no == 'FREE STOCK-CELUP')
                    {
                        $mapping_stock_id       = null;
                        $type_stock_erp_code    = '2';
                        $type_stock             = 'REGULER';
                    }else
                    {
                        $get_4_digit_from_beginning = substr($document_no,0, 4);
                        $_mapping_stock_4_digt = MappingStocks::where('document_number',$get_4_digit_from_beginning)->first();
                        if($_mapping_stock_4_digt)
                        {
                            $mapping_stock_id       = $_mapping_stock_4_digt->id;
                            $type_stock_erp_code    = $_mapping_stock_4_digt->type_stock_erp_code;
                            $type_stock             = $_mapping_stock_4_digt->type_stock;
                        }else
                        {
                            $get_5_digit_from_beginning = substr($document_no,0, 5);
                            $_mapping_stock_5_digt = MappingStocks::where('document_number',$get_5_digit_from_beginning)->first();
                            if($_mapping_stock_5_digt)
                            {
                                $mapping_stock_id       = $_mapping_stock_5_digt->id;
                                $type_stock_erp_code    = $_mapping_stock_5_digt->type_stock_erp_code;
                                $type_stock             = $_mapping_stock_5_digt->type_stock;
                            }else
                            {
                                $mapping_stock_id       = null;
                                $type_stock_erp_code    = null;
                                $type_stock             = null;
                            }
                            
                        }
                    }

                    $conversion = UomConversion::where([
                        ['item_code',$item_code],
                        ['uom_from',$uom]
                    ])
                    ->first();
                
                    if($conversion)
                    {
                        $multiplyrate   = $conversion->multiplyrate;
                        $dividerate     = $conversion->dividerate;
                        $uom_conversion = $conversion->uom_to;
                    }else
                    {
                        $dividerate     = 1;
                        $multiplyrate   = 1;
                        $uom_conversion = $uom;
                    }
                    
                    $qty_conversion = sprintf("%0.4f",$qty_upload*$dividerate);

                    if(!$is_running_stock)
                    {
                        $is_stock_already_created = MaterialStock::where([
                            ['document_no',$document_no],
                            ['item_code',$item_code],
                            ['warehouse_id',$warehouse_id],
                            ['is_stock_on_the_fly',true],
                        ])
                        ->exists();
                    }else
                    {
                        $is_stock_already_created = false;
                    }
                    
                    $is_exists = MaterialStock::where([
                        ['po_detail_id',$po_detail_id],
                        ['document_no',$document_no],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['item_code',$item_code],
                        ['type_po',2],
                        ['warehouse_id',$warehouse_id],
                        ['c_order_id',$c_order_id],
                        ['type_stock',$type_stock],
                        ['uom',$uom_conversion],
                        ['is_material_others',false],
                        ['is_stock_on_the_fly',false],
                    ]);

                    if(!$is_running_stock) $is_exists = $is_exists->where('locator_id',$free_stock_destination->id);
                    else $is_exists= $is_exists->whereNull('locator_id');

                    $is_exists = $is_exists->whereNotNull('approval_date')
                    ->whereNull('po_buyer')
                    ->first();

                    if(!$is_exists)
                    {
                        $material_stock = MaterialStock::FirstOrCreate([
                            'po_detail_id'              => $po_detail_id,
                            'locator_id'                => (!$is_running_stock ? $free_stock_destination->id : null),
                            'mapping_stock_id'          => $mapping_stock_id,
                            'type_stock_erp_code'       => $type_stock_erp_code,
                            'type_stock'                => $type_stock,
                            'document_no'               => $document_no,
                            'supplier_code'             => $supplier_code,
                            'supplier_name'             => $supplier_name,
                            'c_order_id'                => $c_order_id,
                            'c_bpartner_id'             => $c_bpartner_id,
                            'item_id'                   => $item_id,
                            'item_code'                 => $item_code,
                            'item_desc'                 => $item_desc,
                            'category'                  => $category,
                            'type_po'                   => 2,
                            'warehouse_id'              => $warehouse_id,
                            'uom'                       => $uom_conversion ,
                            'uom_source'                => $uom ,
                            'qty_carton'                => intval($qty_carton),
                            'qty_arrival'               => $qty_conversion,
                            'stock'                     => $qty_conversion,
                            'reserved_qty'              => 0,
                            'available_qty'             => $qty_conversion,
                            'is_general_item'           => true,
                            'is_material_others'        => false,
                            'is_active'                 => true,
                            'is_running_stock'          => $is_running_stock,
                            'source'                    => $source,
                            'upc_item'                  => $upc_item,
                            'created_at'                => $upload_date,
                            'updated_at'                => $upload_date,
                            'approval_date'             => $upload_date,
                            'approval_user_id'          => $system->id,
                            'user_id'                   => $system->id
                        ]);

                        $material_stock_id  = $material_stock->id;
                        $locator_id         = $material_stock->locator_id;

                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;
                    
                        /*HistoryStock::approved($material_stock_id
                        ,$qty_conversion
                        ,'0'
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,$remark_detail_stock
                        ,Auth::user()->name
                        ,Auth::user()->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,$is_integrate
                        ,true
                        );*/

                        
                    }else
                    {
                        $locator_id         = $is_exists->locator_id;
                        $material_stock_id  = $is_exists->id;
                        $stock              = $is_exists->stock;
                        $reserved_qty       = $is_exists->reserved_qty;
                        $old_available_qty  = $is_exists->available_qty;
                        $new_stock          = $stock + $qty_conversion;
                        $availability_stock = $new_stock - $reserved_qty;

                        $is_exists->stock           = sprintf("%0.4f",$new_stock);
                        $is_exists->available_qty   = sprintf("%0.4f",$availability_stock);

                        if($availability_stock > 0)
                        {
                            $is_exists->is_allocated = false;
                            $is_exists->is_active = true;
                        }

                        if($is_stock_already_created) $is_integrate = true;
                        else $is_integrate = false;

                        /*HistoryStock::approved($material_stock_id
                        ,$new_stock
                        ,$old_available_qty
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,null
                        ,Auth::user()->name
                        ,Auth::user()->id
                        ,$type_stock_erp_code
                        ,$type_stock
                        ,$is_integrate
                        ,true
                        );*/
                        
                        $is_exists->save();
                    }

                    DetailMaterialStock::FirstOrCreate([
                        'material_stock_id'     => $material_stock_id,
                        'material_arrival_id'   => $material_arrival_id,
                        'remark'                => $remark_detail_stock,
                        'uom'                   => $uom_conversion,
                        'qty'                   => $qty_conversion,
                        'user_id'               => $system->id,
                        'created_at'            => $upload_date,
                        'updated_at'            => $upload_date,
                        'locator_id'            => $locator_id,
                    ]);

                    if(!$is_running_stock) $stock_accessories [] = $material_stock_id;
                    else 
                    {
                        if(!$material_subcont_id) $stock_reserved_accessories [] = $material_stock_id;
                        else $stock_reserved_handover_accessories [] = $material_stock_id;
                    }
                }
            }
           
            $get_stocks             = MaterialStock::whereIn('id',$stock_reserved_accessories)
            ->orderby('available_qty','asc')
            ->get();

            foreach ($get_stocks as $key => $value) 
            {
                $material_stock_id  = $value->id;
                $material_stock     = MaterialStock::where([
                    ['id',$material_stock_id],
                    ['is_running_stock',true],
                    ['is_closing_balance',false],
                    ['is_allocated',false],
                ])
                ->whereNull('last_status')
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->first();

                if($material_stock)
                {
                    $material_stock->last_status = 'prepared';
                    $material_stock->save();

                    $document_no                = strtoupper($material_stock->document_no);
                    $item_code                  = strtoupper($material_stock->item_code);
                    $po_detail_id               = $material_stock->po_detail_id;
                    $po_buyer_source            = $material_stock->po_buyer;
                    $type_stock_erp_code        = $material_stock->type_stock_erp_code;
                    $type_stock                 = $material_stock->type_stock;
                    $c_order_id_stock           = $material_stock->c_order_id;
                    $item_id_stock              = $material_stock->item_id;
                    $c_bpartner_id              = $material_stock->c_bpartner_id;
                    $uom_source_stock           = $material_stock->uom_source;
                    $uom_stock                  = $material_stock->uom;
                    $supplier_name              = strtoupper($material_stock->supplier_name);
                    $_supplier                  = Supplier::where('c_bpartner_id',$c_bpartner_id)->first();
                    $supplier_code              = ($_supplier)? $_supplier->supplier_code : 'FREE STOCK';
                    $warehouse_id               = $value->warehouse_id;
                    $reserved_qty               = sprintf("%0.4f",$material_stock->reserved_qty);
                    $available_qty              = sprintf("%0.4f",$material_stock->available_qty);
                    $is_bom_exists              = 0;
                    $material_arrival           = MaterialArrival::where([
                        ['po_detail_id',$po_detail_id],
                        ['warehouse_id',$warehouse_id],
                    ])
                    ->first();

                    $prepared_status            = $material_arrival->prepared_status;
                    $material_arrival_id        = $material_arrival->id;
                    $is_moq                     = $material_arrival->is_moq;

                    $system                     = User::where([
                        ['name','system'],
                        ['warehouse',$warehouse_id]
                    ])
                    ->first();
                    
                    $conversion = UomConversion::where([
                        ['item_code',$item_code],
                        ['uom_to',$uom_stock]
                    ])
                    ->first();
                
                    if($conversion)
                    {
                        $multiplyrate   = $conversion->multiplyrate;
                        $dividerate     = $conversion->dividerate;
                    }else
                    {
                        $dividerate     = 1;
                        $multiplyrate   = 1;
                    }

                    $auto_allocations = AutoAllocation::where([
                        ['item_id_source',$item_id_stock],
                        ['c_order_id',$c_order_id_stock],
                        ['c_bpartner_id',$c_bpartner_id],
                        ['warehouse_id',$warehouse_id],
                        ['qty_outstanding','>',0],
                        ['is_fabric',false],
                        ['is_allocation_purchase',true],
                    ])
                    ->whereNull('deleted_at')
                    ->orderby('promise_date','asc')
                    ->orderby('qty_allocation','asc')
                    ->get();

                    if($available_qty > 0)
                    {
                        foreach ($auto_allocations as $key_2 => $auto_allocation)
                        {
                            $auto_allocation_id = $auto_allocation->id;
                            $c_order_id         = $auto_allocation->c_order_id;
                            $lc_date            = $auto_allocation->lc_date;
                            $item_desc          = $auto_allocation->item_desc;
                            $category           = $auto_allocation->category;
                            $item_id_book       = $auto_allocation->item_id_book;
                            $item_id_source     = $auto_allocation->item_id_source;
                            $new_po_buyer       = $auto_allocation->po_buyer;
                            $old_po_buyer       = $auto_allocation->old_po_buyer;
                            $status_po_buyer    = $auto_allocation->status_po_buyer;
                            $_item_code_book    = $auto_allocation->item_code;
                            $pos                = strpos($_item_code_book, '|');

                            if ($pos === false)
                            {
                                $item_code_book = $_item_code_book;
                                $_article_no    = null;
                            }else 
                            {
                                $split          = explode('|',$_item_code_book);
                                $item_code_book = $split[0];
                                $_article_no    = $split[1];
                            }
                            

                            if($auto_allocation->is_upload_manual)
                            {
                                $allocation_source  = 'ALLOCATION MANUAL';
                                $user               = $auto_allocation->user_id;
                                $username           = strtoupper($auto_allocation->user->name);
                                $is_integrate       = false;
                            }else
                            {
                                $allocation_source  = ($auto_allocation->is_allocation_purchase) ? 'ALLOCATION PURCHASE' : 'ALLOCATION ERP';
                                $user               = $system->id;
                                $username           = strtoupper($auto_allocation->user->name);
                                if($auto_allocation->is_allocation_purchase)  $is_integrate = false;
                                else $is_integrate = true;
                            }

                            $qty_outstanding        = sprintf("%0.4f",$auto_allocation->qty_outstanding);
                            $qty_allocated          = sprintf("%0.4f",$auto_allocation->qty_allocated);
                            
                            $_po_buyer              = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                            
                            if ($available_qty/$qty_outstanding >= 1) $_supply = $qty_outstanding;
                            else $_supply = $available_qty;
                            
                            $qty_supply     = $_supply;
                            $qty_required   = $qty_outstanding;
                            
                            if ($qty_supply/$qty_required >= 1) $_supplied = $qty_required;
                            else $_supplied = $qty_supply;

                            $__supplied = sprintf("%0.4f",$_supplied);
                        
                            if($_supplied > 0.0000)
                            {
                                $is_exists = MaterialPreparation::where([
                                    ['barcode',$po_detail_id],
                                    ['c_order_id',$c_order_id],
                                    ['c_bpartner_id',$c_bpartner_id],
                                    ['po_buyer',$new_po_buyer],
                                    ['item_id',$item_id_book],
                                    ['warehouse',$warehouse_id],
                                    ['is_backlog',false],
                                    ['is_additional',false],
                                ]);

                                if($po_detail_id) $is_exists = $is_exists->where('po_detail_id',$po_detail_id);

                                $is_exists = $is_exists->first();

                                if($is_exists)
                                {
                                    $is_exists->material_stock_id = $material_stock_id;
                                    $is_exists->auto_allocation_id = $auto_allocation_id;
                                    $is_exists->save();

                                }
                                
                                $qty_supply             -= $_supplied;
                                $qty_required           -= $_supplied;
                                $qty_outstanding        -= $_supplied;
                                $qty_allocated          += $_supplied;

                                $old = $available_qty;
                                $new = $available_qty - $_supplied;
                                
                                if($new <= 0) $new = '0';
                                else $new = $new;

                                /*HistoryStock::approved($material_stock_id
                                ,$new
                                ,$old
                                ,$__supplied
                                ,$new_po_buyer
                                ,'TIDAK DITEMUKAN'
                                ,'TIDAK DITEMUKAN'
                                ,$lc_date
                                ,$allocation_source
                                ,$username
                                ,$user
                                ,$type_stock_erp_code
                                ,$type_stock
                                ,$is_integrate
                                ,true);*/
                                
                                $available_qty -= $_supplied;
                                $reserved_qty += $_supplied;
                                
                                $is_bom_exists++;
                            }

                            if($qty_outstanding<=0)
                            {
                                $auto_allocation->is_upload_manual                  = true;
                                $auto_allocation->is_already_generate_form_booking  = true;
                                $auto_allocation->generate_form_booking             = carbon::now();
                            }
    
                            $auto_allocation->qty_outstanding                       = sprintf("%0.4f",$qty_outstanding);
                            $auto_allocation->qty_allocated                         = sprintf("%0.4f",$qty_allocated);
                            $auto_allocation->save();
                        }
                        
                    }

                    if($is_bom_exists > 0)
                    {
                        if($available_qty <=0 ){
                            $material_stock->is_allocated   = true;
                            $material_stock->is_active      = false;
                        }

                        $material_stock->available_qty      = sprintf("%0.4f",$available_qty);
                        $material_stock->reserved_qty       = sprintf("%0.4f",$reserved_qty);
                    
                    }

                    $material_stock->last_status            = null;
                    $material_stock->save();
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
            ErrorHandler::db($message);
        }
    }
}
