<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\AutoAllocation;

class resyncMasterItemInAutoAllocation extends Command
{
    protected $signature    = 'resync:ItemAutoAllocation';
    protected $description  = 'resync master item in auto allocation';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_IN_AUTO_ALLOCATION')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_ITEM_IN_AUTO_ALLOCATION',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER ITEM AUTO ALLOCATION  START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM AUTO ALLOCATION  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        } 
    }

    private function updateData()
    {
        $data1 = DB::table('resync_master_item_book_in_auto_allocations_v')->get();
        foreach ($data1 as $key => $value) 
        {
            $id             = $value->id;
            $article_no     = $value->article_no;
            $item           = AutoAllocation::find($id);

            if($article_no) $item_code = $value->item_code_master.'|'.$article_no;
            else $item_code = $value->item_code_master;

            $item->item_code        = $item_code;
            $item->item_code_book   = $value->item_code_master;
            $item->save();
        }

        $data2 = DB::table('resync_master_item_source_in_auto_allocations_v')->get();
        foreach ($data2 as $key => $value) 
        {
            $id     = $value->id;
            $item   = AutoAllocation::find($id);

            $item->item_code_source = $value->item_code_master;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
