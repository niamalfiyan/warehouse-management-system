<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\Temporary;

use App\Http\Controllers\ErpMaterialRequirementController;

class SyncMaterialRequirement extends Command
{
    protected $signature = 'sync:materialRequirement';
    protected $description = 'Sinkron material requirement';
    
    public function __construct(){
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_supplier_on_going = Scheduler::where('job','SYNC_MATERIAL_REQUIREMENT')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_supplier_on_going){
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_MATERIAL_REQUIREMENT',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC MATERIAL REQUIREMENT START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            ErpMaterialRequirementController::syncMaterialRequirement();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC MATERIAL REQUIREMENT END JOB AT '.carbon::now());
        }else{
            $this->info('SYNC MATERIAL REQUIREMENT SEDANG BERJALAN');
        }  
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
