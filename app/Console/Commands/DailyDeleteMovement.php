<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\MovementOutController;

class DailyDeleteMovement extends Command
{
    protected $signature = 'dailymovement:delete';
    protected $description = 'set movement active into non active';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        //MovementOutController::deleteMaterialMovement();
        MovementOutController::deleteMaterialMovement();
        MovementOutController::delete_material_arrival();
        $this->info('delete movement success');
    }
}
