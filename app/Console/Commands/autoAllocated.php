<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\MaterialCheckController;
use App\Http\Controllers\FabricMaterialPlanningController;
use App\Http\Controllers\AreaController;
use App\Http\Controllers\MaterialArrivalFabricController;
use App\Http\Controllers\ERPController;
use App\Http\Controllers\AccessoriesMaterialArrivalController as InsertMaterialReceive;
use App\Http\Controllers\MasterDataAutoAllocationController;
use App\Http\Controllers\MovementOutController;
use App\Http\Controllers\MaterialPreparationFabricController as PreparationFabric;
use App\Http\Controllers\FabricReportDailyMaterialPreparationController as ReportPreparationFabric;
use App\Http\Controllers\MaterialStockFabricController;
use App\Http\Controllers\PoBuyerController;
use App\Http\Controllers\MasterDataPoBuyerController;
use App\Http\Controllers\MaterialStockController;
use App\Http\Controllers\ErpMaterialRequirementController;
use App\Http\Controllers\AccessoriesMaterialStockApprovalController as HistoryStock;
use App\Http\Controllers\MasterDataAutoAllocationController as AllocationFabric;
use App\Http\Controllers\FabricReportMaterialMonitoringController;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;
use App\Http\Controllers\FabricMaterialStockController as HistoryStockPerLot;
use App\Http\Controllers\FabricReportMaterialMonitoringController as Dashboard;
use App\Http\Controllers\AccessoriesMaterialStockOnTheFlyController as SOTF;

use Carbon\Carbon;
use DB;
use Config;
use StdClass;

use App\Models\PurchaseOrderDetail;
use App\Models\MaterialRequirementPerSize;
use App\Models\Temporary;
use App\Models\MaterialMovementPerSize;
use App\Models\MovementStockHistory;
use App\Models\UomConversion;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\MaterialSubcont;
use App\Models\SummaryHandoverMaterial;
use App\Models\MonitoringReceivingFabric;
use App\Models\AutoAllocation;
use App\Models\MaterialStock;
use App\Models\CuttingInstruction;
use App\Models\Locator;
use App\Models\Barcode;
use App\Models\MaterialPlanningFabric;
use App\Models\User;
use App\Models\MaterialArrival;
use App\Models\DetailMaterialArrival;
use App\Models\MaterialRequirementPerPart;
use App\Models\MaterialRequirement;
use App\Models\Supplier;
use App\Models\PoSupplier;
use App\Models\DetailMaterialStock;
use App\Models\PoBuyer;
use App\Models\AllocationItem;
use App\Models\MaterialRollHandoverFabric;
use App\Models\DetailMaterialPlanningFabric;
use App\Models\DetailMaterialPreparationFabric;
use App\Models\DetailMaterialPreparation;
use App\Models\MaterialPreparation;
use App\Models\MaterialPreparationFabric;
use App\Models\MappingStocks;
use App\Models\SummaryStockFabric;
use App\Models\DetailPoBuyerPerRoll;
use App\Models\MaterialStockPerLot;
use App\Models\HistoryMaterialStockPerLot;
use App\Models\HistoryPreparationFabric;
use App\Models\HistoryAutoAllocations;
use App\Models\ReroutePoBuyer;
use App\Models\Item;

class autoAllocated extends Command
{
    protected $signature = 'autoAllocated:fixed';

    protected $description = 'Auto Allocation';

    public function __construct()
    {
        parent::__construct();
    }

    static function insertAllocation() {

        $uuidz = [
            'fd2fe7b5-bfaf-4fd8-818b-e02739643f35',
            '005a0ddc-04ba-4ab3-a047-d39b9d92ed15',
            '67bd10f7-5602-4d9f-a826-68e6e972b7d3',
            'c0411582-fe2b-45b6-bed7-a488877b9314',
            '4fb12218-7fe3-4011-a139-2184d746bcbe',
            '1fe5beff-78f8-49ec-8b11-327bd0d276f2',
            '11600280-c794-42fb-b83e-c7eaa66b421a',
            'eb081a3c-b42a-4a6f-9eb8-3433a63f098a',
            '8072e8e6-b300-42a8-91e9-ae38edc8487e',
            '720bf509-a29b-4966-8167-6d0017296fe1',
            'f29a0780-99ed-4194-9fc1-51654d141e06',
            'a163747d-80b6-4d66-ba98-cb746eaef84f',
            '621e121b-7eda-48ff-9395-83b10c0e8d0a',
            '5d0a3a5b-2bc5-4dec-8dc4-7e1131bfacb1',
            'fe8cd9e9-0e89-4af2-9c4f-ff0f82d04728',
            'e61e378d-6dfc-4cc6-bfea-8e5fc28735a1',
            'b0c33a8d-588e-4484-a058-20433c9d8b1e',
            '456c57a6-39df-415b-b7a1-022d5e61530e',
            '0312ea65-6df8-4d95-8ec9-5442c48e6896',
            'e36e0bcd-eb9e-4352-8f4b-b71159cc277a',
            '7cbdeca0-2eff-4a3e-bfab-f816840b3783',
            'bc14e5d8-9062-4f57-a1bf-5969ee7d4dd5',
            '37aade55-8015-444d-8305-af93fba94b68',
            '09d45e1f-46d0-4f8c-ae26-a9a1ec01d971',
            '53b4fe2e-8186-4d22-a130-b89e8558a27e',
            '5e776ddf-9b2b-4a71-84c4-3934a0a94024',
            'cbbb5e39-2ccd-4e69-8072-30a7000ecafe',
            '17cc32a9-e5bb-45b0-b86d-53f9a8ad8819',
            'b7dbe4c4-c04a-41f4-9c15-935a337f9245',
            '0438f684-40a0-4196-91e1-c88180817057',
            '820060b7-d4f0-4c9e-bbc4-b51a06096526',
            'a9f76877-fb13-4f6f-adea-4cb438f7ad52',
            '173a87e5-4485-453f-ad32-723c7a1894d8',
            '0ad4101e-0704-43f6-8f6e-fdea7f5330eb',
            'a71f5b30-87ea-4cd4-8178-236e123c64f3',
            'cf87d05a-ad0f-4af6-8021-c3a9221e6839',
            '126e04f8-604f-4147-93ab-373c32dd3b8e',
            'a488765f-3db9-43b8-b420-e07f0ffe5274',
            'd2a037d5-fee0-4e53-b0cb-c474edd2bce8',
            '79a99a87-6d2e-44f5-b28e-f54ecf015ebe',
            'ec193979-d861-49ca-b62b-4ca6853ded24',
            'dd015f38-7588-4114-9ffd-8294dc26fc42',
            '4f4c63da-4750-4dd9-ba5d-aab39768eb60',
            'dffc19e2-05d6-40a4-a9ef-3d9304ca041e',
            'e11c82c0-fd24-4ca6-8e87-4ab4ed50f039',
            '89d14d91-16f8-48eb-942c-2f78efe263cd',
            '84646c37-f706-4925-a395-23674f560934',
            '5e73cd0a-f772-47ac-9ef0-bc82f3d1622a',
            '42405b98-dc12-4cce-9988-6e8e780ce76e',
            '432739a1-45e5-42e1-baa6-966acb62d50f',
            'ca8cb0b6-a4d6-4de9-859b-da7250d0e37c',
            '72ec6a9a-1116-4747-ac1e-b45d21dd8887',
            '50bca4a9-a0de-4b2d-b594-1b9f7549f69b',
            '1a0b6a58-4eca-4022-8aba-26eb33b9c785',
            'f090ca7f-cc4c-4c2f-bfa4-a05a40d1f251',
            '98ec504a-f099-48c8-ab7c-51eb339f52d0',            
        ];

        $movement_date = Carbon::now();
        $erp_id        = array();
        $urutan        = 1;

        $purchase_items = DB::connection('erp') //dev_erp //erp
            ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
            // ->where([
            //     ['isintegrate',false],
            //     ['status_lc','PR'],
            //     ['is_additional', 'f'],
            //     ['isfabric', 'Y'],
            // ])
            // ->whereDate('created', '=', '2022-04-03')
            ->whereIN('uuid', $uuidz)
            // ->where('uuid', 'dd21ce6b-513f-4e87-90ea-68f436be8bcb')
            ->get();

        foreach ($purchase_items as $key => $purchase_item) {
            
            echo $urutan . ". " . $purchase_item->uuid ." == ". $movement_date . "\n";

            $check_wms = DB::table('auto_allocations')
                            ->whereNull('deleted_at')
                            ->Where('erp_allocation_id', $purchase_item->uuid)
                            ->exists();

                            // dd($check_wms);
            
            if(!$check_wms) {

                $c_order_id        = $purchase_item->c_order_id;
                $item_id           = $purchase_item->item_id;
                $c_bpartner_id     = $purchase_item->c_bpartner_id;
                $supplier_name     = strtoupper($purchase_item->supplier_name);
                $item_code         = strtoupper($purchase_item->item_code);
                $document_no       = strtoupper($purchase_item->document_no);
                $purchase_po_buyer = trim(str_replace('-S', '',$purchase_item->po_buyer));

                $qty_sum = DB::connection('erp') //dev_erp //erp
                ->table('rma_wms_planning_alokasi') // rma_wms_planning_v2_cron -->viewnya
                ->where([
                    // ['status_lc','PR'],
                    // ['is_additional', 'f'],
                    ['c_order_id', $c_order_id],
                    ['po_buyer', 'like', '%'.$purchase_po_buyer.'%'],
                    ['document_no', $document_no],
                    ['item_code', $item_code],
                    ['item_id', $item_id],
                    ['c_bpartner_id', $c_bpartner_id]
                ])
                ->sum('qty');
                
                $warehouse_id      = $purchase_item->m_warehouse_id;
                $uom               = $purchase_item->uom;
                $category          = $purchase_item->category;
                $qty               = sprintf('%0.8f',$qty_sum);
                $erp_allocation_id = $purchase_item->uuid;
                $is_fabric         = $purchase_item->isfabric;

                if ($purchase_item->is_additional == 'f') {
                    $is_additional = false;
                } else {
                    $is_additional = true;
                }
                
                if($warehouse_id == '1000011') $warehouse_name = 'FABRIC AOI 2';
                else if($warehouse_id == '1000001') $warehouse_name = 'FABRIC AOI 1';
                else if($warehouse_id == '1000013') $warehouse_name = 'ACCESSORIES AOI 2';
                else if($warehouse_id == '1000002') $warehouse_name = 'ACCESSORIES AOI 1';

                $reroute            = ReroutePoBuyer::where('old_po_buyer',$purchase_po_buyer)->first();

                if ($reroute) {
                    $new_po_buyer = $reroute->new_po_buyer;
                    $old_po_buyer = $reroute->old_po_buyer;
                } else {
                    $new_po_buyer = $purchase_po_buyer;
                    $old_po_buyer = $purchase_po_buyer;
                }

                $buyer_detail = PoBuyer::where('po_buyer',$new_po_buyer)->first();
                
                $document_allocation_number = NULL;

                if ($buyer_detail) {
                    $lc_date                    = $buyer_detail->lc_date;
                    $promise_date               = ($buyer_detail->statistical_date ? $buyer_detail->statistical_date : $buyer_detail->promise_date);
                    $season                     = $buyer_detail->season;
                    $type_stock_erp_code        = $buyer_detail->type_stock_erp_code;
                    $cancel_date                = $buyer_detail->cancel_date;

                    if($is_fabric == 'N')
                    {
                        $tgl_date                   = ($buyer_detail->lc_date ? $buyer_detail->lc_date->format('d') : 'LC NOT FOUND');
                        $month_lc                   = ($buyer_detail->lc_date ? $buyer_detail->lc_date->format('my')  : 'LC NOT FOUND');

                        if($warehouse_id == '1000002') $warehouse_sequnce = '001';
                        else if($warehouse_id == '1000013') $warehouse_sequnce = '002';
                    
                        if($purchase_item->status_lc == 'BO') $document_allocation_number = 'BOLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                        elseif($purchase_item->status_lc == 'PR') $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                        // $document_allocation_number = 'PRLC'.$tgl_date.'-'.$month_lc.'-'.$warehouse_sequnce;
                    }
                    
                } else {
                    $promise_date               = null;
                    $lc_date                    = null;
                    $season                     = null;
                    $type_stock_erp_code        = null;
                    $cancel_date                = null;
                    $document_allocation_number = null;
                }
                
                if ($type_stock_erp_code == 1) $type_stock      = 'SLT';
                else if($type_stock_erp_code == 2) $type_stock  = 'REGULER';
                else if($type_stock_erp_code == 3) $type_stock  = 'PR/SR';
                else if($type_stock_erp_code == 4) $type_stock  = 'MTFC';
                else if($type_stock_erp_code == 5) $type_stock  = 'NB';
                else $type_stock                                = null;

                
                if($cancel_date != null) $status_po_buyer = 'cancel';
                else $status_po_buyer                     = 'active';
                
                $system             = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();
    
                $item               = Item::where('item_id',$item_id)->first();
                $item_desc          = ($item)? strtoupper($item->item_desc): 'data not found';
                $is_exists          = null;

                try {
                    
                    DB::beginTransaction();

                    //alokasi FABRIC
                    if($lc_date >= '2019-08-15' && $category == 'FB') {
                        
                            $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                            ->where([
                                ['c_order_id',$c_order_id],
                                ['c_bpartner_id',$c_bpartner_id],
                                ['item_id_book',$item_id],
                                ['item_id_source',$item_id],
                                ['po_buyer',$new_po_buyer],
                                ['erp_allocation_id',$erp_allocation_id],
                                ['warehouse_id',$warehouse_id],
                                ['is_fabric',true]
                            ])
                            ->exists();

                            if(!$is_exists)
                            {
                                $newAllocation = AutoAllocation::create([
                                    'type_stock_erp_code'               => $type_stock_erp_code,
                                    'type_stock'                        => $type_stock,
                                    'lc_date'                           => $lc_date,
                                    'promise_date'                      => $promise_date,
                                    'season'                            => $season,
                                    'document_no'                       => $document_no,
                                    'c_bpartner_id'                     => $c_bpartner_id,
                                    'supplier_name'                     => $supplier_name,
                                    'po_buyer'                          => $new_po_buyer,
                                    'c_order_id'                        => $c_order_id,
                                    'item_id_book'                      => $item_id,
                                    'item_id_source'                    => $item_id,
                                    'old_po_buyer'                      => $old_po_buyer,
                                    'item_code_source'                  => $item_code,
                                    'item_code_book'                    => $item_code,
                                    'item_code'                         => $item_code,
                                    'item_desc'                         => $item_desc,
                                    'category'                          => $category,
                                    'uom'                               => $uom,
                                    'warehouse_name'                    => $warehouse_name,
                                    'warehouse_id'                      => $warehouse_id,
                                    'qty_allocation'                    => $qty,
                                    'qty_outstanding'                   => $qty,
                                    'qty_allocated'                     => 0,
                                    'status_po_buyer'                   => $status_po_buyer,
                                    'is_fabric'                         => true,
                                    'is_already_generate_form_booking'  => false,
                                    'is_upload_manual'                  => false,
                                    'is_allocation_purchase'            => true,
                                    'user_id'                           => $system->id,
                                    'created_at'                        => $movement_date,
                                    'updated_at'                        => $movement_date,
                                    'erp_allocation_id'                 => $erp_allocation_id,
                                    'is_additional'                     => $is_additional,
                                ]);

                                $erp_id [] = $newAllocation->erp_allocation_id;   
                            }
                            else
                            {
                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();

                                $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                                $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                                $remark              = $is_exists->remark;
            
                                if ($old_qty_allocation-$qty == 0) {
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();
                                } else {
                                    $is_exists->qty_allocation    = $qty;
                                    $is_exists->qty_outstanding   = $qty - $old_qty_outstanding;
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.($qty - $old_qty_outstanding);
                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();
                                }

                                $erp_id [] = $erp_allocation_id;
                            }

                    //alokasi acc
                    } else if($category != 'FB' && ($new_po_buyer != null || $new_po_buyer != '')) {

                        $is_exists = AutoAllocation::whereNotNull('erp_allocation_id')
                        ->where([
                            ['c_order_id',$c_order_id],
                            ['c_bpartner_id',$c_bpartner_id],
                            ['item_id_book',$item_id],
                            ['item_id_source',$item_id],
                            ['po_buyer',$new_po_buyer],
                            ['warehouse_id',$warehouse_id],
                            ['is_fabric',false],
                        ])
                        ->whereNull('deleted_at')
                        ->first();

                        if(!$is_exists)
                        {
                            $newAllocation = AutoAllocation::create([
                                'document_allocation_number'        => $document_allocation_number,
                                'type_stock_erp_code'               => $type_stock_erp_code,
                                'type_stock'                        => $type_stock,
                                'lc_date'                           => $lc_date,
                                'promise_date'                      => $promise_date,
                                'season'                            => $season,
                                'c_order_id'                        => $c_order_id,
                                'document_no'                       => $document_no,
                                'c_bpartner_id'                     => $c_bpartner_id,
                                'supplier_name'                     => $supplier_name,
                                'po_buyer'                          => $new_po_buyer,
                                'old_po_buyer'                      => $old_po_buyer,
                                'item_code_source'                  => $item_code,
                                'item_code_book'                    => $item_code,
                                'item_id_book'                      => $item_id,
                                'item_id_source'                    => $item_id,
                                'item_code'                         => $item_code,
                                'item_desc'                         => $item_desc,
                                'category'                          => $category,
                                'uom'                               => $uom,
                                'warehouse_name'                    => $warehouse_name,
                                'warehouse_id'                      => $warehouse_id,
                                'qty_allocation'                    => $qty,
                                'qty_outstanding'                   => $qty,
                                'qty_allocated'                     => 0,
                                'status_po_buyer'                   => $status_po_buyer,
                                'is_fabric'                         => false,
                                'is_already_generate_form_booking'  => false,
                                'is_upload_manual'                  => false,
                                'is_allocation_purchase'            => true,
                                'created_at'                        => $movement_date,
                                'updated_at'                        => $movement_date,
                                'user_id'                           => $system->id,
                                'erp_allocation_id'                 => $erp_allocation_id,
                                'is_additional'                     => $is_additional,
                            ]);

                            $erp_id [] = $newAllocation->erp_allocation_id;

                        } else {

                                $system = User::where([
                                    ['name','system'],
                                    ['warehouse',$warehouse_id]
                                ])
                                ->first();

                                $old_qty_allocation  = sprintf('%0.8f',$is_exists->qty_allocation);
                                $old_qty_outstanding = sprintf('%0.8f',$is_exists->qty_outstanding);
                                $remark              = $is_exists->remark;
                                
                                // $bagi = number_format($old_qty_allocation, 0, '.', '') / number_format($qty, 0, '.', '');
                                // dd($qty);
                                if($qty == 0){
                                    $bagi = 0;
                                } else {
                                    $bagi = $old_qty_allocation/$qty;
                                }

                                // dd($bagi);

                                if($bagi == 1) {

                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();

                                } else {

                                    $is_exists->qty_allocation    = $old_qty_allocation + $qty;
                                    $is_exists->qty_outstanding   = $old_qty_outstanding + $qty;
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '.$qty;
                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();

                                }

                                if ($old_qty_allocation-$qty == 0) {
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $is_exists->save();
                                } else {
                                    $is_exists->qty_allocation    = $qty;
                                    $is_exists->qty_outstanding   = $old_qty_outstanding;
                                    
                                    $is_exists->erp_allocation_id = $erp_allocation_id;
                                    $tot = number_format($qty, 0, '.', '') - number_format($old_qty_outstanding, 0, '.', '');
                                    $is_exists->remark            = $remark. 'PENAMBAHAN QTY ALLOCATION SEBESAR '. strval($tot);
                                    $is_exists->user_id           = $system->id;
                                    $is_exists->save();
                                }

                                $erp_id [] = $erp_allocation_id;

                        }
                        
                    }

                    DB::connection('erp') //dev_erp //erp
                    ->table('rma_wms_planning_alokasi')
                    ->whereIn('uuid',$erp_id)
                    ->update([
                        'isintegrate' => true,
                        'updated'     => $movement_date,
                        'source'    => 'test'
                    ]);

                    DB::commit();

                } catch (Exception $e) {
                    DB::rollBack();
                    $message = $e->getMessage();
                    ErrorHandler::db($message);
                }

            }

            $urutan++;

        }
 
        DB::delete("DELETE 
        FROM
            auto_allocations A USING auto_allocations b 
        WHERE
            A.created_at < b.created_at 
            AND A.erp_allocation_id = b.erp_allocation_id 
            AND a.deleted_at IS NULL 
            AND a.lc_date > '2021-10-01' 
            AND a.qty_allocated = 0");
        

        // $checkAllocations =  DB::table('auto_allocations') //double check alokasi yg dimasukin pake erp_allocation_id
        //             ->whereIn('erp_allocation_id', $erp_id)
        //             ->whereNull('deleted_at')
        //             ->whereDate('created_at', $movement_date)
        //             ->get();

        // $groupByErpId  =  $checkAllocations->groupBy('erp_allocation_id'); //gatau knp kalo di grouping diatas tetep double, jadi tak grouping disini

        // foreach ($groupByErpId as $key => $del){

        //     if(count($del) >1) {

        //         $id = $del[0]->id;
        //         $t = $del->where('id','!=', $id)->pluck('id');
        //         AutoAllocation::whereIn('id', $t)->delete();

        //     }

        // }

    }

    private function doAllocated() 
    {
    
        $ids = [
            'b2644480-9bf4-11ec-8634-cbe5bb3e7349',
            '4dddab30-9e5c-11ec-b4dd-cf243fcfd5fb',
            '52864250-9e5c-11ec-941a-67fadf926e73',
            '53bd1200-9e5c-11ec-bed5-2d383c482470',
            '4f07d2f0-9e5c-11ec-b441-4508afa21d92',
            'a1303cf0-9e5b-11ec-b997-9387fedb725c',
            'a25cefc0-9e5b-11ec-9fe7-c19cbd2eed90',
            '81016420-9e5c-11ec-b68f-e5ac48569aa8',
            '7c58eaa0-9e5c-11ec-92f1-4b84fa2bf04e',
            '7fd820a0-9e5c-11ec-983f-11f5fe773694',
            '4b89f480-9e5c-11ec-b616-6be782a6fa1c',
            '4cb430e0-9e5c-11ec-bc8b-b7e8918867ac',
            '7eafc8b0-9e5c-11ec-98fd-6510bddb860c',
            '7bcef100-8eab-11ec-9e91-8db729b2fea8',
            '502fb790-9e5c-11ec-8a38-c3b600fb36cc',
            '51592c30-9e5c-11ec-8b6e-2ff647ac8642',
            '4ad24320-8eab-11ec-91ee-c5ff71ea1b75',
            'ac7aeb50-b232-11ec-8e29-7523555a5e70',
            'f5b88b00-b232-11ec-a744-8fd0b1c42a92',
            '4804ad80-9e5c-11ec-b792-e3e42affcb7e',
            '492ef020-9e5c-11ec-9b10-8fab648d3508',
            '4a5882f0-9e5c-11ec-9120-d79a417bc0ed',
            '7d828a30-9e5c-11ec-a0b8-21bf7e02621f',
            '9a23f520-9e5b-11ec-970a-679c7c9b33b6',
            '9b4d8500-9e5b-11ec-879b-7fbbf2817490',
            '9c77be60-9e5b-11ec-9ec5-49d8e7657896',
            '9db0c930-9e5b-11ec-9ff0-613f63166c15',
            '9ed8c480-9e5b-11ec-ad9f-53fa66c425b1',
            'a0021410-9e5b-11ec-80a0-53f6b09c4777',
            'd9b95b50-9bf4-11ec-acbb-6f01883941bc',
            '61f00260-c83a-11ec-8086-cde49c3e69d4',
            '7487b680-c83a-11ec-a241-2d4ef63a49a2'
        ];

        $urut = 1;

        $material_stocks = MaterialStock::where([
            ['is_allocated',false],
            ['is_closing_balance',false],
            ['is_stock_on_the_fly',false]
        ])
        ->whereNotNull('approval_date')
        ->whereNull('deleted_at')
        ->whereIn(db::raw("(c_order_id, item_id, warehouse_id)"),function($query) use($ids)
        {
            $query->select('c_order_id','item_id_source','warehouse_id')
            ->from('auto_allocations')
            ->whereNull('deleted_at')
            // ->whereDate('created_at', '>=', '2022-01-01')
            // ->where('is_fabric', false)
            // ->where('qty_allocated', '=', 0)
            // ->whereIn('id',$ids)
            ->whereIn('id',$ids)                    
            ->groupby('c_order_id','item_id_source','warehouse_id');
        })
        ->orderBy('warehouse_id', 'DESC')
        ->get();

        dd($material_stocks);

        $movement_date = carbon::now();

        foreach ($material_stocks as $key => $material_stock )
        {
            echo $urut . ". " . $material_stock->warehouse_id . $material_stock->document_no . " " . $material_stock->item_code. " Stock = ". $material_stock->available_qty . " == " .Carbon::now() ."\n";
            if(!$material_stock->last_status) ReportStock::doAllocation($material_stock, '91', $movement_date);
            $urut++;
        }

    }

    public function alokasifabric()
    {
        $id = [
            'fd2fe7b5-bfaf-4fd8-818b-e02739643f35',
            '005a0ddc-04ba-4ab3-a047-d39b9d92ed15',
            '67bd10f7-5602-4d9f-a826-68e6e972b7d3',
            'c0411582-fe2b-45b6-bed7-a488877b9314',
            '4fb12218-7fe3-4011-a139-2184d746bcbe',
            '1fe5beff-78f8-49ec-8b11-327bd0d276f2',
            '11600280-c794-42fb-b83e-c7eaa66b421a',
            'eb081a3c-b42a-4a6f-9eb8-3433a63f098a',
            '8072e8e6-b300-42a8-91e9-ae38edc8487e',
            '720bf509-a29b-4966-8167-6d0017296fe1',
            'f29a0780-99ed-4194-9fc1-51654d141e06',
            'a163747d-80b6-4d66-ba98-cb746eaef84f',
            '621e121b-7eda-48ff-9395-83b10c0e8d0a',
            '5d0a3a5b-2bc5-4dec-8dc4-7e1131bfacb1',
            'fe8cd9e9-0e89-4af2-9c4f-ff0f82d04728',
            'e61e378d-6dfc-4cc6-bfea-8e5fc28735a1',
            'b0c33a8d-588e-4484-a058-20433c9d8b1e',
            '456c57a6-39df-415b-b7a1-022d5e61530e',
            '0312ea65-6df8-4d95-8ec9-5442c48e6896',
            'e36e0bcd-eb9e-4352-8f4b-b71159cc277a',
            '7cbdeca0-2eff-4a3e-bfab-f816840b3783',
            'bc14e5d8-9062-4f57-a1bf-5969ee7d4dd5',
            '37aade55-8015-444d-8305-af93fba94b68',
            '09d45e1f-46d0-4f8c-ae26-a9a1ec01d971',
            '53b4fe2e-8186-4d22-a130-b89e8558a27e',
            '5e776ddf-9b2b-4a71-84c4-3934a0a94024',
            'cbbb5e39-2ccd-4e69-8072-30a7000ecafe',
            '17cc32a9-e5bb-45b0-b86d-53f9a8ad8819',
            'b7dbe4c4-c04a-41f4-9c15-935a337f9245',
            '0438f684-40a0-4196-91e1-c88180817057',
            '820060b7-d4f0-4c9e-bbc4-b51a06096526',
            'a9f76877-fb13-4f6f-adea-4cb438f7ad52',
            '173a87e5-4485-453f-ad32-723c7a1894d8',
            '0ad4101e-0704-43f6-8f6e-fdea7f5330eb',
            'a71f5b30-87ea-4cd4-8178-236e123c64f3',
            'cf87d05a-ad0f-4af6-8021-c3a9221e6839',
            '126e04f8-604f-4147-93ab-373c32dd3b8e',
            'a488765f-3db9-43b8-b420-e07f0ffe5274',
            'd2a037d5-fee0-4e53-b0cb-c474edd2bce8',
            '79a99a87-6d2e-44f5-b28e-f54ecf015ebe',
            'ec193979-d861-49ca-b62b-4ca6853ded24',
            'dd015f38-7588-4114-9ffd-8294dc26fc42',
            '4f4c63da-4750-4dd9-ba5d-aab39768eb60',
            'dffc19e2-05d6-40a4-a9ef-3d9304ca041e',
            'e11c82c0-fd24-4ca6-8e87-4ab4ed50f039',
            '89d14d91-16f8-48eb-942c-2f78efe263cd',
            '84646c37-f706-4925-a395-23674f560934',
            '5e73cd0a-f772-47ac-9ef0-bc82f3d1622a',
            '42405b98-dc12-4cce-9988-6e8e780ce76e',
            '432739a1-45e5-42e1-baa6-966acb62d50f',
            'ca8cb0b6-a4d6-4de9-859b-da7250d0e37c',
            '72ec6a9a-1116-4747-ac1e-b45d21dd8887',
            '50bca4a9-a0de-4b2d-b594-1b9f7549f69b',
            '1a0b6a58-4eca-4022-8aba-26eb33b9c785',
            'f090ca7f-cc4c-4c2f-bfa4-a05a40d1f251',
            '98ec504a-f099-48c8-ab7c-51eb339f52d0'
        ];

        // $auto_allocations = Temporary::select('barcode AS id')
        //                     ->where('status', 'update_auto_allocation_fabric')
        //                     ->whereIN('barcode', $id)
        //                     ->orderBy('created_at', 'DESC')
        //                     ->get();
        $auto_allocations = AutoAllocation::where('id', $id)->get();

        foreach ($auto_allocations as $key => $allocation) {

            $auto_allocation_id = $allocation->id;

            AllocationFabric::updateAllocationFabricItems([$auto_allocation_id],$auto_allocation_id);

            echo $allocation->id .' - '. Carbon::now() . "\n";
        }
         
    }

    public function copySo()
    {
        $po_buyer = PoBuyer::whereNull('cancel_date')
                    ->whereNull('cancel_user_id')
                    ->wherenull('cancel_reason')
                    ->whereNotNull('requirement_date')
                    // ->whereDate('requirement_date', '>=', '2020-01-01')
                    ->get();

        foreach ($po_buyer as $key => $value) {
            
            $erp = DB::connection('erp')
                    ->table('wms_lc_buyers')
                    ->where('poreference',$value->po_buyer)
                    ->first();

            if ($erp) {
                PoBuyer::where('po_buyer',$value->po_buyer)
                ->update([
                    'so_id' => $erp->so_id
                ]);

            echo "po_buyer :" . $value->po_buyer . " == So ID : ". $erp->so_id . "\n";
            } else {
                echo "po_buyer :" . $value->po_buyer . " == So ID : tidak ada" . "\n";
            }
            
        }
    }

    public function handle()
    {
        $this->info('FIXER START AT '.carbon::now());
        // $this->insertAllocation();
        // $this->doAllocated();
        // $this->alokasifabric();
        $this->copySo();
        $this->info('FIXER END AT '.carbon::now());
    }
}
