<?php
namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use App\Models\Temporary;
use App\Models\Scheduler;
use App\Models\AutoAllocation;
use App\Models\MaterialStock;

use Illuminate\Console\Command;
use App\Http\Controllers\FabricReportMaterialMonitoringController;

class DailyUpdateDashboardMonitoringMaterial extends Command
{protected $signature = 'dailyDashboardMonitoringMaterial:update';
    protected $description = 'update dashboard monitoring material';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_DASHBOARD_MONITORING_MATERIAL')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_DASHBOARD_MONITORING_MATERIAL',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            //$this->doJob();
            //calculating fabric
            $this->info('SYNC DASHBOARD MONITORING MATERIAL FABRIC START JOB AT '.carbon::now());
            FabricReportMaterialMonitoringController::updateMonitoringMaterial();
            $this->info('SYNC DASHBOARD MONITORING MATERIAL FABRIC END JOB AT '.carbon::now());
            //insert to erp
            $this->info('SYNC INSERT DASHBOARD TO ERP START JOB AT '.carbon::now());
            FabricReportMaterialMonitoringController::insertToDashboardErp();
            $this->info('SYNC INSERT DASHBOARD TO ERP END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC DASHBOARD MONITORING FABRIC MATERIAL SEDANG BERJALAN');
        } 
    }

    // static function doJob()
    // {
        
    // } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
