<?php

namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Http\Controllers\FabricReportMaterialMonitoringController;

class DailyMonitoringMaterial extends Command
{
    protected $signature = 'dailyMonitoringMaterial:update';
    protected $description = 'Daily Monitoring Material';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_planning = Scheduler::where('job','DAILY_MONITORING_MATERIAL_ACC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_planning)){
            $this->setStatus($scheduler_planning,'ongoing');
            $this->setStartJob($scheduler_planning);

            $this->info('DAILY MONITORING MATERIAL JOB AT '.carbon::now());
            FabricReportMaterialMonitoringController::updateMonitoringMaterialACC();
            $this->info('DAILY MONITORING MATERIAL JOB JOB AT '.carbon::now());

            $this->setStatus($scheduler_planning,'done');
            $this->setEndJob($scheduler_planning);
            
        }else{
            $is_schedule_on_going = Scheduler::where('job','DAILY_MONITORING_MATERIAL_ACC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'DAILY_MONITORING_MATERIAL_ACC',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('DAILY MONITORING MATERIAL JOB AT '.carbon::now());
                FabricReportMaterialMonitoringController::updateMonitoringMaterialACC();
                $this->info('DAILY MONITORING MATERIAL JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                
            }else{
                $this->info('DAILY MONITORING MATERIAL JOB IS RUNNING');
            }
        }

        
        $this->info('Daily update has been successfully');
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }

}
