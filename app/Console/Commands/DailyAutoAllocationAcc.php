<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\MasterDataAutoAllocationController;

use App\Models\Scheduler;

class DailyAutoAllocationAcc extends Command
{
    protected $signature = 'autoAllocationAcc:insert';
    protected $description = 'insert auto allocation for Acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $schedular = Scheduler::where('job','SYNC_AUTO_ALLOCATION_ACC')
        ->where('status','queue')
        ->first();

        if(!empty($schedular)){
            $this->info('SYNC AUTO ALLOCATION ACCESSORIES JOB AT '.carbon::now());
            $this->setStatus($schedular,'ongoing');
            $this->setStartJob($schedular);
            MasterDataAutoAllocationController::insertAllocationAccessoriesItems();
            MasterDataAutoAllocationController::updateTotalAllocationSOTF();
            $this->setStatus($schedular,'done');
            $this->setEndJob($schedular);
            $this->info('DONE SYNC AUTO ALLOCATION ACCESSORIES AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_AUTO_ALLOCATION_ACC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_AUTO_ALLOCATION_ACC',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC AUTO ALLOCATION ACCESSORIES JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                MasterDataAutoAllocationController::insertAllocationAccessoriesItems();
                MasterDataAutoAllocationController::updateTotalAllocationSOTF();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC AUTO ALLOCATION ACCESSORIES JOB AT '.carbon::now());
            }else{
                $this->info('SYNC AUTO ALLOCATION ACCESSORIES SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
