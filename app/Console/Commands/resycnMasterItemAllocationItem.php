<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\AllocationItem;

class resycnMasterItemAllocationItem extends Command
{
    protected $signature    = 'resync:ItemAllocationItem';
    protected $description  = 'resync master item in allocation item';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_IN_ALLOCATION_ITEM')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_ITEM_IN_ALLOCATION_ITEM',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER ITEM ALLOCATION ITEM START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM ALLOCATION ITEM END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        } 
    }

    private function updateData()
    {
        $data1 = DB::table('resync_master_item_source_in_allocation_items_v')->get();
        foreach ($data1 as $key => $value) 
        {
            $id                     = $value->id;
            $item                   = AllocationItem::find($id);
            $item->item_code_source = $value->item_code_master;
            $item->item_id_source   = $value->item_id_master;
            $item->save();
        }



        $data2 = DB::table('resync_master_item_book_in_allocation_items_v')->get();
        foreach ($data2 as $key => $value) 
        {
            $id                     = $value->id;
            $item                   = AllocationItem::find($id);
            $item->item_code        = $value->item_code_master;
            $item->item_id_book     = $value->item_id_master;
            $item->save();
        }

        $data3 = DB::table('resync_auto_allocation_vs_allocation_item_v')->get();
        foreach ($data3 as $key => $value) 
        {
            $id                     = $value->id;
            $item                   = AllocationItem::find($id);
            $item->c_order_id       = $value->auto_allocation_c_order_id;
            $item->item_id_book     = $value->auto_allocation_item_id_book;
            $item->item_id_source   = $value->auto_allocation_item_id_source;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
