<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Locator;
use App\Models\Scheduler;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialMovementLine;
use App\Models\MaterialMovementPerSize;

class IntegrationConvertFromOutIssueToInternalUse extends Command
{
    protected $signature    = 'integrationFromIssueToInternalUse:update';
    protected $description  = 'Convert From Issue to Internal Use';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_INTEGRATION_CONVERT_FROM_OUT_ISSUE_TO_INTERNAL_USE')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_INTEGRATION_CONVERT_FROM_OUT_ISSUE_TO_INTERNAL_USE',
                'status'    => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC DAILY INTEGRATION CONVERT FROM OUT ISSUE TO INTERNAL USE START JOB AT '.carbon::now());
            //$this->doJob(); ini jangan di jalanin dlu, lupa ini fungsi buat apa. charis
            $this->info('SYNC DAILY INTEGRATION CONVERT FROM OUT ISSUE TO INTERNAL USE AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC DAILY INTEGRATION CONVERT FROM OUT ISSUE TO INTERNAL USE IS RUNNING');
        }
    }

    private function doJob()
    {
        $integration_date   = carbon::now()->todatetimestring();

        $data               =  DB::select(db::raw("select * from material_out_issue_v
            where is_integrate = false
            and to_char(movement_date, 'YYYYMMDD') < '20200406'"
        ));
       
        
        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_movement_line_id      = $value->material_movement_line_id;
                $material_movement_per_size_id  = $value->material_movement_per_size_id;
                $material_movement_line         = MaterialMovementLine::find($material_movement_line_id);
                $c_order_id                     = $value->c_order_id;
                $item_code                      = $material_movement_line->item_code;
                $supplier_name                  = $material_movement_line->supplier_name;
                $c_bpartner_id                  = $material_movement_line->c_bpartner_id;
                $document_no                    = $material_movement_line->document_no;
                $material_preparation_id        = $material_movement_line->material_preparation_id;
                $po_detail_id                   = $material_movement_line->materialPreparation->po_detail_id;
                $type_po                        = $material_movement_line->materialPreparation->type_po;
                $item_id                        = ( $value->item_id_source ? $value->item_id_source : $value->item_id);
                $po_buyer                       = $value->po_buyer;
                $movement_date                  = $value->movement_date;
                $warehouse_id                   = $value->warehouse_id;
                $size                           = $value->size;
                $uom                            = $value->uom;
                $qty_per_size                   = sprintf('%0.8f',$value->qty_per_size);
                $note                           = 'INTEGRASI OUT ISSUE YANG DIKELUARKAN TANGGAL '.$movement_date.' UNTUK PO BUYER '.$po_buyer.' UNTUK SIZE '.$size.' GAGAL, SUPAYA STOCK KEPOTONG MAKA DIGUNAKAN INTERNAL USE SEBAGAI SARANANYA.';
                $material_movement_per_size     = MaterialMovementPerSize::find($material_movement_per_size_id);
                
                
                $material_arrival           = MaterialArrival::whereNull('material_subcont_id')
                ->whereNull('material_roll_handover_fabric_id')
                ->where('po_detail_id',$po_detail_id)
                ->first();

                $no_invoice                 = ($material_arrival ? $material_arrival->no_invoice : '-');
                $no_packing_list            = ($material_arrival ? $material_arrival->no_packing_list : '-');
                $c_orderline_id             = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                
                $inventory_erp = Locator::whereHas('area',function ($query) use($warehouse_id)
                {
                    $query->where('is_active',true);
                    $query->where('warehouse',$warehouse_id);
                    $query->where('name','ERP INVENTORY');
                })
                ->first();

                $system = User::where([
                    ['name','system'],
                    ['warehouse',$warehouse_id]
                ])
                ->first();

                $is_movement_exists = MaterialMovement::where([
                    ['from_location',$inventory_erp->id],
                    ['to_destination',$inventory_erp->id],
                    ['from_locator_erp_id',$inventory_erp->area->erp_id],
                    ['to_locator_erp_id',$inventory_erp->area->erp_id],
                    ['po_buyer',$po_buyer],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['no_packing_list',$no_packing_list],
                    ['no_invoice',$no_invoice],
                    ['status','integration-to-inventory-erp'],
                ])
                ->first();

                if(!$is_movement_exists)
                {
                    $material_movement = MaterialMovement::firstOrCreate([
                        'from_location'         => $inventory_erp->id,
                        'to_destination'        => $inventory_erp->id,
                        'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                        'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                        'po_buyer'              => $po_buyer,
                        'is_integrate'          => false,
                        'is_active'             => true,
                        'no_packing_list'       => $no_packing_list,
                        'no_invoice'            => $no_invoice,
                        'status'                => 'integration-to-inventory-erp',
                        'created_at'            => $integration_date,
                        'updated_at'            => $integration_date,
                    ]);

                    $material_movement_id = $material_movement->id;
                }else
                {
                    $is_movement_exists->updated_at = $integration_date;
                    $is_movement_exists->save();

                    $material_movement_id = $is_movement_exists->id;
                }

                $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                ->where([
                    ['material_movement_id',$material_movement_id],
                    ['material_preparation_id',$material_preparation_id],
                    ['qty_movement',-1*$qty_per_size],
                    ['item_id',$item_id],
                    ['is_integrate',false],
                    ['is_active',true],
                    ['warehouse_id',$warehouse_id],
                    ['date_movement',$integration_date],
                ])
                ->exists();

                if(!$is_line_exists)
                {
                    MaterialMovementLine::Create([
                        'material_movement_id'          => $material_movement_id,
                        'material_preparation_id'       => $material_preparation_id,
                        'item_code'                     => $item_code,
                        'item_id'                       => $item_id,
                        'c_order_id'                    => $c_order_id,
                        'c_orderline_id'                => $c_orderline_id,
                        'c_bpartner_id'                 => $c_bpartner_id,
                        'supplier_name'                 => $supplier_name,
                        'type_po'                       => $type_po,
                        'is_integrate'                  => false,
                        'is_active'                     => true,
                        'uom_movement'                  => $uom,
                        'qty_movement'                  => -1*$qty_per_size,
                        'date_movement'                 => $integration_date,
                        'created_at'                    => $integration_date,
                        'updated_at'                    => $integration_date,
                        'date_receive_on_destination'   => $integration_date,
                        'nomor_roll'                    => '-',
                        'warehouse_id'                  => $warehouse_id,
                        'document_no'                   => $document_no,
                        'note'                          => $note,
                        'is_active'                     => true,
                        'user_id'                       => $system->id,
                    ]);
                }
                
                $material_movement_per_size->is_integrate           = true;
                $material_movement_per_size->integration_date       = $integration_date;
                $material_movement_per_size->remark_integration     = 'CONVERT USING INTERNAL USE DUE ISSUE ON ERP';
                $material_movement_per_size->save();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }

            
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
