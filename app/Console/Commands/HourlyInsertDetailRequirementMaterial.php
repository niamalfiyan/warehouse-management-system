<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\ErpMaterialRequirementController;

class HourlyInsertDetailRequirementMaterial extends Command
{
    protected $signature = 'hourlyrequirementdetailmaterial:insert';
    protected $description = 'Insert to get latest data from erp for detail preperation material hourly';

    public function __construct(){
        parent::__construct();
    }

    public function handle(){
        ErpMaterialRequirementController::daily_detail_cron_insert();
        $this->info('Hourly Insert has been successfully');
    }
}
