<?php namespace App\Console\Commands;

use DB;
use StdClass;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MaterialPreparation;
use App\Models\DetailPoBuyerPerRoll;
use App\Http\Controllers\FabricReportDailyMaterialPreparationController as FabricReportDailyMaterialPreparation;


class DailyInsertPoBuyerPerRoll extends Command
{
    protected $signature    = 'dailyInsertPoBuyerPerRoll:insert';
    protected $description  = 'Insert PO Buyer per roll fabric';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_DAILY_INSERT_PO_BUYER_PER_ROLL_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_DAILY_INSERT_PO_BUYER_PER_ROLL_FABRIC',
                'status' => 'ongoing'
            ]);
            $this->info('SYNC_DAILY_INSERT_PO_BUYER_PER_ROLL_FABRIC JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->updateDetailPoBuyerPerRoll();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC_DAILY_INSERT_PO_BUYER_PER_ROLL_FABRIC JOB AT '.carbon::now());
        }
    }

    static function updateDetailPoBuyerPerRoll()
    {
        $data           = db::select(db::raw("
            select material_preparation_fabric_id from detail_preparation_vs_detail_roll_per_buyer_fabric
            where total_relax::numeric != total_qty_booking_per_roll::numeric
            GROUP BY material_preparation_fabric_id
            order by count(0) desc
        "));

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();
                $material_preparation_fabric_id         = $value->material_preparation_fabric_id;
                
                MaterialPreparation::whereIn('id',function($query) use($material_preparation_fabric_id)
                {
                    $query->select('material_preparation_id')
                    ->from('detail_material_preparations')
                    ->whereNotNull('detail_material_preparation_fabric_id')
                    ->whereIn('detail_material_preparation_fabric_id',function($query2) use ($material_preparation_fabric_id)
                    {
                        $query2->select('id')
                        ->from('detail_material_preparation_fabrics')
                        ->where('material_preparation_fabric_id',$material_preparation_fabric_id);
                    })
                    ->groupby('material_preparation_id');
                })
                ->delete();
                
                DetailPoBuyerPerRoll::whereIn('detail_material_preparation_fabric_id',function($query) use($material_preparation_fabric_id)
                {
                    $query->select('id')
                    ->from('detail_material_preparation_fabrics')
                    ->where('material_preparation_fabric_id',$material_preparation_fabric_id);
                })
                ->delete();
                
                FabricReportDailyMaterialPreparation::insertRePrepareRollToBuyer($material_preparation_fabric_id,0);

                DB::commit();
                
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }

    /*

    GA BISA KEPOTONG ADA APA TUH
17322b90-ef1a-11e9-b9e7-f961aee47bed
cc3c1250-e63d-11e9-82f7-8bac25e1789a
f8507430-ee1a-11e9-a9ce-9125a996fc71
2099ba20-27da-11ea-87a2-8de2ebb21226
6de7b7c0-1566-11ea-a8a0-2b74f6af1101
a776ac40-ebe2-11e9-a0f8-b503bf03c013
463ba790-ef18-11e9-9be6-39123288fa9a
10cd8b90-514d-11ea-bb95-d10b500c7872
40895e30-72ed-11ea-8846-3b59026cea11
2ec07820-f608-11e9-b943-b17a9bc8c2e8
f2bee6e0-ebce-11e9-b93e-178bdc9498fd
e49de250-efc6-11e9-a5e5-af7e14fc136f
71636ef0-f571-11e9-a7aa-8b03eda0e6ba
f27958b0-10bc-11ea-9dfb-59e66df3f3c8
7c1e1f10-1048-11ea-bbee-e1c6b27900e1
6c0117f0-208f-11ea-88ee-1f1a862d79cd
7d919b40-f09f-11e9-bdb0-c5349d0d225a
1ad313e0-ef1a-11e9-a5a3-cf616a6ad758
7e562890-3601-11ea-aff9-771ccb8dcf6f
0d400670-7aaa-11ea-8720-dd395a769d81
bc4606f0-f147-11e9-832f-ef6557d6a8d3
5fc43f10-3601-11ea-b4ba-d58e7b5135cc
1687f5a0-ef1a-11e9-8eb2-f1d1309cca39
dc332d70-5393-11ea-8425-99ce4dcbb70c
70226380-0774-11ea-a906-69227ee1083d
c90adbd0-e63d-11e9-8ced-f52458af09bb
d697a060-5393-11ea-a596-f7c8c3c468e1
3ba26390-015b-11ea-b189-f1771ebd8dad
fe1d27e0-2200-11ea-a526-ddbab4a16b3b
a66ac600-f56d-11e9-9ac5-e19a95f4cdc9
ca497690-fca3-11e9-885e-6d348bbe7606
c3d662f0-2534-11ea-9624-8b7a5370520d
9af7b360-2536-11ea-9f1e-c7b03c2c7797
7f088760-2535-11ea-a2c1-315e9d2cc181
b4ee8810-1da3-11ea-b517-e7e7050343b3
ba4f6020-652f-11ea-a668-d995101170d2
12b19e20-5ed2-11ea-b415-e511704e62a5
632279f0-252d-11ea-92f2-77f3f4eace98
7d3ae880-252c-11ea-90d2-2368003722f8
cfe8ab90-4818-11ea-8daf-cb9b602536a6
902f6750-4818-11ea-979b-d16d0505b3d2
1a8c7a90-ef1a-11e9-9792-e782d2496b07
d26d4680-ef20-11e9-8547-7597873946ad
a9727160-f084-11e9-99ac-b1a681d03884
94636650-6512-11ea-9915-f35ff897dc8d
0d0a3850-385d-11ea-b08f-cf365a8511c3
c0308190-4818-11ea-92d0-cb15b6757294
56a61660-efe8-11e9-b169-93e87d250784
6df5a1e0-1b10-11ea-ad34-c990b8269c12
5d855a30-652f-11ea-a374-41548a22279d
9e0a4f90-514d-11ea-a21b-07bf3e0f9738
bd3a5830-2526-11ea-90c5-418c2d945196
966e9620-3c48-11ea-a6ee-bb9f4649b328
68844ae0-015b-11ea-950b-d5498a747b86
64c95020-3023-11ea-aa39-fb64b5f32bfc
8431aa20-251c-11ea-97a6-170330280fea
b0db81a0-f079-11e9-96fd-cf6df7c3754c
896bdce0-31cc-11ea-aa2e-2102b24a59d7
9efd7580-0761-11ea-bb1a-87a691ebc943
a3776a00-ebe2-11e9-a2db-41566d30fe47
16f398d0-5ed2-11ea-acf0-639f56f46f4a
8183e6f0-05e9-11ea-9d64-3b235626570a
b79ad5b0-3c08-11ea-9ff2-fd5c989588bb
20c7c1b0-5ed2-11ea-83b6-c3575b5f9f42
22661540-2203-11ea-81ea-57d476084ab1
b1d6b950-f792-11e9-975a-db64d8874eaa
f8111f90-f52f-11e9-a8c1-27ffc9e85d73
a20d8a20-0761-11ea-a53e-39ace8c328a9
884a31b0-046b-11ea-a481-8156a4ad8c9f
b69573d0-652f-11ea-b410-ad6f64ce458a
841bae50-3080-11ea-9e5c-111f8a1570df
19c7f300-5ed2-11ea-ae66-278d9631264d
140c6ff0-514d-11ea-8eb5-8f5f691acedf
b2a4b940-f792-11e9-85c0-cf84eb368705
8e48f8d0-251f-11ea-9926-118254b7bc0b
3b771b90-21fe-11ea-a890-3196e51d885c
4291bb20-225a-11ea-a004-5f41b1c76d73
df6c3500-5393-11ea-bcfa-4f2a27e58520
30a8e0a0-41a3-11ea-9393-cdd77e44d952
35891730-225a-11ea-bb49-4b1faa406aca
f096abd0-e41f-11e9-add4-251e59fdddff
b2c7f480-f792-11e9-93a9-3161b6b83e66
c7a4b450-fca3-11e9-b40f-f57495c03fd4
c8194900-1da3-11ea-9092-532be2f0cab6
0d991260-2850-11ea-addb-392779368c33
beca0790-fb7c-11e9-b1f6-8197962bc91c
a113d6a0-0761-11ea-bb68-d98aa5b87cda
9be4db40-302a-11ea-ab24-7f1dadaa6f12
8e218ee0-feff-11e9-9926-bfb7e06cebfd
6a32e680-652f-11ea-bdaa-99c36fda4683
a3ca10f0-4ee2-11ea-a4fb-3fae0ca317db
b1af1960-f079-11e9-82a3-27cbe6b3c40c
c70b47c0-652f-11ea-bf26-81af338e5882
51b8dde0-015b-11ea-bf00-d142c29d2078
97f70200-302a-11ea-b4e7-c1b559dcf657
ac529c20-09c3-11ea-b175-89a2b2f63348
8249b2d0-6852-11ea-ae8e-317ef20e8619
da0d71a0-206d-11ea-98cf-b773b938dba7
a6120510-ebe2-11e9-8ee2-b57680c85022
45ff2f60-3833-11ea-9a9d-f39a636c3bee
c23d8560-fb7c-11e9-95eb-b505462bafbc
d3a64a70-e410-11e9-93d6-4f0374e0c73e
b95f78e0-462c-11ea-9dd2-c9e7cb3d9128
efb1ecd0-f095-11e9-8c92-f99b19440b8e
98e6c7d0-7247-11ea-9b0d-276523fd2939
3b777960-72ed-11ea-a321-f34e8f0fa5f3
e75971a0-7252-11ea-8a65-d508b8b556d9
c7671110-0b91-11ea-b9fc-63302f9cee34
92aa25a0-0d03-11ea-b168-7b065bcea0d0
6f7bcb00-e709-11e9-97dc-bf05a8ebc722
e1ac1650-0b6c-11ea-a590-a1c339991c8c
709deb90-0530-11ea-99b1-c7fc2753c9c8
    */
}
