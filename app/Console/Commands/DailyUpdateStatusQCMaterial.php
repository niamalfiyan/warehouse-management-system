<?php namespace App\Console\Commands;

use App\Http\Controllers\MaterialCheckController;
use Illuminate\Console\Command;

class DailyUpdateStatusQCMaterial extends Command
{
    protected $signature = 'dailystatusqc:update';
    protected $description = 'Update Status QC to all material preparation.';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        echo MaterialCheckController::update_qc_status();
        $this->info('Daily Insert has been successfully');
    }
}
