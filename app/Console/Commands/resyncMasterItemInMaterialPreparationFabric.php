<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MaterialPreparationFabric;

class resyncMasterItemInMaterialPreparationFabric extends Command
{
    protected $signature    = 'resync:ItemMaterialPreparationFabric';
    protected $description  = 'resync master item in material preparation fabric';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_ITEM_MATERIAL_PREPARATION_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_ITEM_MATERIAL_PREPARATION_FABRIC',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER ITEM MATERIAL PREPARATION FABRIC START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER ITEM MATERIAL PREPARATION FABRIC END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }
    }

    private function updateData()
    {
        $data1 = DB::table('resync_master_item_book_in_material_preparation_fabrics_v')->get();
        foreach ($data1 as $key => $value) 
        {
            $id     = $value->id;
            $item   = MaterialPreparationFabric::find($id);

            $item->item_code = $value->item_code_master;
            $item->save();
        }

        $data2 = DB::table('resync_master_item_source_in_material_preparation_fabrics_v')->get();
        foreach ($data2 as $key => $value) 
        {
            $id     = $value->id;
            $item   = MaterialPreparationFabric::find($id);

            $item->item_code_source = $value->item_code_master;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
