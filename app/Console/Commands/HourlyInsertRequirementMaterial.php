<?php namespace App\Console\Commands;

use App\Http\Controllers\ErpMaterialRequirementController;
use Illuminate\Console\Command;

class HourlyInsertRequirementMaterial extends Command
{
    protected $signature = 'hourlyRequirementMaterial:insert';
    protected $description = 'Insert to get latest data from erp for preperation material hourly';

    public function __construct(){
        parent::__construct();
    }

    public function handle(){
        ErpMaterialRequirementController::daily_detail_cron_insert();
        ErpMaterialRequirementController::daily_cron_insert();
        $this->info('Hourly Insert has been successfully');
    }
}
