<?php namespace App\Console\Commands;

use Carbon\Carbon;
use DB;
use Config;
use StdClass;
use Illuminate\Console\Command;
use App\Http\Controllers\AccessoriesMaterialArrivalController as InsertMaterialReceive;

class DailyPackingList extends Command
{
    protected $signature = 'dailyPackingList:insert';
    protected $description = 'Insert Packing List For Integration MR';

    public function __construct(){
        parent::__construct();
    }

    public function handle(){
        $this->info('START SCHEDULE INSERT PACKING LIST AT '.carbon::now());
        $item_packing_lists = db::table('insert_item_packing_list_v')
                                      ->get();

        foreach($item_packing_lists as $key => $value)
        {
            $packing_list[]         = $value->no_packing_list;
        }

        InsertMaterialReceive::getItemPackingList($packing_list);

        $this->info('SCHEDULE INSERT PACKING LIST HAS BEEN SUCCESSFULLY AT '.carbon::now());
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }


}
