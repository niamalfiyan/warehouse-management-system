<?php namespace App\Console\Commands;
use DB;
use Illuminate\Console\Command;

class DailyUpdateLcDateMrp extends Command
{
    protected $signature = 'dailyUpdateLcDateMrp:update';
    protected $description = 'Update LC date on MRP';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        DB::select(db::raw("SELECT * FROM update_lc_in_mrps();"));
    }
}
