<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\FabricReportMaterialRejectController;

use App\Models\Scheduler;

class DailySendEmailMoveLocatorReject extends Command
{
    protected $signature = 'dailySendEmailMoveLocatorReject:mail';
    protected $description = 'Sending email outstanding move locator reject';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_planning = Scheduler::where('job','SEND_EMAIL_OUTSTANDING_MOVE_LOCATOR_REJECT')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_planning)){
            $this->setStatus($scheduler_planning,'ongoing');
            $this->setStartJob($scheduler_planning);

            $this->info('SEND EMAIL OUTSTANDING MOVE LOCATOR FABRIC JOB AT '.carbon::now());
            FabricReportMaterialRejectController::sendEmailMoveLocatorReject();
            $this->info('DONE SEND EMAIL OUTSTANDING MOVE LOCATOR FABRIC JOB AT '.carbon::now());

            $this->setStatus($scheduler_planning,'done');
            $this->setEndJob($scheduler_planning);
            
        }else{
            $is_schedule_on_going = Scheduler::where('job','SEND_EMAIL_OUTSTANDING_MOVE_LOCATOR_REJECT')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SEND_EMAIL_OUTSTANDING_MOVE_LOCATOR_REJECT',
                    'status' => 'ongoing'
                ]);
                $this->setStartJob($new_scheduler);
                
                $this->info('SEND EMAIL OUTSTANDING MOVE LOCATOR FABRIC FABRIC JOB AT '.carbon::now());
                FabricReportMaterialRejectController::sendEmailMoveLocatorReject();
                $this->info('DONE SEND EMAIL OUTSTANDING MOVE LOCATOR FABRIC FABRIC JOB AT '.carbon::now());

                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                
            }else{
                $this->info('SEND EMAIL OUTSTANDING MOVE LOCATOR FABRIC FABRIC JOB IS RUNNING');
            }
        }

        
        $this->info('Daily Insert has been successfully');
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
