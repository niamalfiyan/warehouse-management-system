<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\ERPController;
use App\Http\Controllers\WebSupplierController;
use App\Http\Controllers\ErpMaterialRequirementController;

use App\Models\Scheduler;

class DailyInsertPoBuyer extends Command
{
    protected $signature = 'dailyPobuyer:insert';
    protected $description = 'Insert PO Buyer from ERP';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $scheduler_lc = Scheduler::where('job','SYNC_PO_BUYER_PER_LC')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_lc))
        {
            $this->info('SYNC PO BUYER PER LC JOB AT '.carbon::now());
            $this->setStatus($scheduler_lc,'ongoing');
            $this->setStartJob($scheduler_lc);
            ErpMaterialRequirementController::getPobuyerLc();
            $this->setStatus($scheduler_lc,'done');
            $this->setEndJob($scheduler_lc);
            $this->info('DONE SYNC PO BUYER PER LC JOB AT '.carbon::now());
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','SYNC_PO_BUYER_PER_LC')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_PO_BUYER_PER_LC',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC PO BUYER PER LC JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ErpMaterialRequirementController::getPobuyerLc();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC PO BUYER PER LC JOB AT '.carbon::now());
            }else{
                $this->info('SYNC PO BUYER PER LC SEDANG BERJALAN');
            }
        }

        $scheduler_update_statistical_date = Scheduler::where('job','SYNC_UPDATE_STATISTICAL_DATE')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_update_statistical_date))
        {
            $this->info('SYNC UPDATE STATISTICAL DATE JOB AT '.carbon::now());
            $this->setStatus($scheduler_update_statistical_date,'ongoing');
            $this->setStartJob($scheduler_update_statistical_date);
            ErpMaterialRequirementController::dailyUpdateStatisticalDate();
            $this->setStatus($scheduler_update_statistical_date,'done');
            $this->setEndJob($scheduler_update_statistical_date);
            $this->info('DONE SYNC UPDATE STATISTICAL DATE JOB AT '.carbon::now());
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','SYNC_UPDATE_STATISTICAL_DATE')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_UPDATE_STATISTICAL_DATE',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC UPDATE STATISTICAL DATE JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ErpMaterialRequirementController::dailyUpdateStatisticalDate();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC UPDATE STATISTICAL DATE JOB AT '.carbon::now());
            }else{
                $this->info('SYNC UPDATE STATISTICAL DATE SEDANG BERJALAN');
            }
        }

        $scheduler_detail_material_requirement = Scheduler::where('job','SYNC_DETAIL_MATERIAL_REQUIREMENT')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_detail_material_requirement))
        {
            $this->info('SYNC DETAIL MATERIAL REQUIREMENT JOB AT '.carbon::now());
            $this->setStatus($scheduler_detail_material_requirement,'ongoing');
            $this->setStartJob($scheduler_detail_material_requirement);
            ErpMaterialRequirementController::dailyDetailCronInsert();
            $this->setStatus($scheduler_detail_material_requirement,'done');
            $this->setEndJob($scheduler_detail_material_requirement);
            $this->info('DONE SYNC DETAIL MATERIAL REQUIREMENT  AT '.carbon::now());
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','SYNC_DETAIL_MATERIAL_REQUIREMENT')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_DETAIL_MATERIAL_REQUIREMENT',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC DETAIL MATERIAL REQUIREMENT JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ErpMaterialRequirementController::dailyDetailCronInsert();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC DETAIL MATERIAL REQUIREMENT JOB AT '.carbon::now());
            }else{
                $this->info('SYNC DETAIL MATERIAL REQUIREMENT SEDANG BERJALAN');
            }
        }

        $scheduler_material_requirement_per_part = Scheduler::where('job','SYNC_MATERIAL_REQUIREMENT_PER_PART')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_material_requirement_per_part))
        {
            $this->info('SYNC MATERIAL REQUIREMENT PER PART JOB AT '.carbon::now());
            $this->setStatus($scheduler_material_requirement_per_part,'ongoing');
            $this->setStartJob($scheduler_material_requirement_per_part);
            ErpMaterialRequirementController::dailyCronInsertPerPart();
            $this->setStatus($scheduler_material_requirement_per_part,'done');
            $this->setEndJob($scheduler_material_requirement_per_part);
            $this->info('DONE SYNC MATERIAL REQUIREMENT PER PART   AT '.carbon::now());
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','SYNC_MATERIAL_REQUIREMENT_PER_PART')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_MATERIAL_REQUIREMENT_PER_PART',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC MATERIAL REQUIREMENT PER PART  JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ErpMaterialRequirementController::dailyCronInsertPerPart();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC MATERIAL REQUIREMENT PER PART  JOB AT '.carbon::now());
            }else{
                $this->info('SYNC MATERIAL REQUIREMENT PER PART  SEDANG BERJALAN');
            }
        }

        $scheduler_material_requirement = Scheduler::where('job','SYNC_MATERIAL_REQUIREMENT')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler_material_requirement)){
            $this->info('SYNC MATERIAL REQUIREMENT JOB AT '.carbon::now());
            $this->setStatus($scheduler_material_requirement,'ongoing');
            $this->setStartJob($scheduler_material_requirement);
            ErpMaterialRequirementController::dailyCronInsert();
            $this->setStatus($scheduler_material_requirement,'done');
            $this->setEndJob($scheduler_material_requirement);
            $this->info('DONE SYNC MATERIAL REQUIREMENT AT '.carbon::now());
        }else{
            $is_schedule_on_going = Scheduler::where('job','SYNC_MATERIAL_REQUIREMENT')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going){
                $new_scheduler = Scheduler::create([
                    'job' => 'SYNC_MATERIAL_REQUIREMENT',
                    'status' => 'ongoing'
                ]);
                $this->info('SYNC MATERIAL REQUIREMENT JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                ErpMaterialRequirementController::dailyCronInsert();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE SYNC MATERIAL REQUIREMENT JOB AT '.carbon::now());
            }else{
                $this->info('SYNC MATERIAL REQUIREMENT SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
