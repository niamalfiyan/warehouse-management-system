<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Http\Controllers\FabricMaterialOutCuttingController;

use App\Models\Locator;
use App\Models\Scheduler;
use App\Models\Temporary;
use App\Models\MaterialArrival;
use App\Models\MaterialMovement;
use App\Models\MaterialPreparation;
use App\Models\MaterialMovementLine;
use App\Models\MaterialMovementPerSize;
use App\Models\DetailMaterialPreparation;
use App\Models\MaterialRequirementPerSize;
use App\Models\DetailMaterialPreparationFabric;

class DailyInsertPreparationFabric extends Command
{
    protected $signature = 'dailyPreparationFabric:insert';
    protected $description = 'Insert Preparation fabric to integration';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_prepartion_on_going = Scheduler::where('job','SYNC_PREPARATION_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_prepartion_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_PREPARATION_FABRIC',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC PREPARATION FABRIC JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->insertMovementPrepareIntegration();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC PREPARATION FABRIC JOB AT '.carbon::now());
        }
        
        $is_schedule_out_on_going = Scheduler::where('job','SYNC_OUT_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_out_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_OUT_FABRIC',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC OUT FABRIC JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->insertMovementOutIntegration();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('DONE SYNC OUT FABRIC JOB AT '.carbon::now());
        }

        $is_schedule_size_on_going = Scheduler::where('job','SYNC_MOVEMENT_PER_SIZE')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_size_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job'       => 'SYNC_MOVEMENT_PER_SIZE',
                'status'    => 'ongoing'
            ]);
            $this->info('SYNC MOVEMENT PER SIZE START JOB AT '.carbon::now());
            $this->setStartJob($new_scheduler);
            $this->doMovementPerSize();
            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
            $this->info('SYNC MOVEMENT PER SIZE END JOB AT '.carbon::now());
        } 

        
    }

    static function insertMovementPrepareIntegration()
    {
        $data = DetailMaterialPreparationFabric::whereHas('materialPreparationFabric',function($query)
        {
            $query->whereNull('deleted_at');
        })
        ->where([
            ['is_status_prepare_inserted_to_material_preparation',false],
           // ['material_preparation_fabric_id','44975ab0-5f74-11ea-b75e-b99c84b7d36e'],
        ])
        ->where(function($query)
        {
            $query->where('last_status_movement','out')
            ->orwhere('last_status_movement','relax');
        })
        ->take(20)
        ->get();
        
        $concatenate = '';

        foreach ($data as $key => $value) 
        {
            try
            {
                db::beginTransaction();
                
                $is_closing                             = $value->is_closing;
                $warehouse_id                           = $value->materialPreparationFabric->warehouse_id;
                $detail_material_preparation_fabric_id  = $value->id;

                $inventory = Locator::where('rack','INVENTORY')
                ->whereHas('area',function ($query) use ($warehouse_id)
                {
                    $query->where('name','INVENTORY')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();

                $relax = Locator::whereHas('area',function ($query) use ($warehouse_id){
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();
                
                if(!$is_closing)
                {
                    $data_preparations =  DB::select(db::raw("SELECT * FROM get_movement_relax_fabric(
                        '".$detail_material_preparation_fabric_id."'
                        )"
                    ));
                }else
                {
                    $data_preparations =  DB::select(db::raw("SELECT * FROM get_movement_relax_closing_fabric(
                        '".$detail_material_preparation_fabric_id."'
                        )"
                    ));
                }
            

                
                foreach ($data_preparations as $key_2 => $data_preparation) 
                {
                    $is_preparation_exists = MaterialPreparation::where([
                        ['barcode',$data_preparation->barcode.'-'.$data_preparation->po_buyer],
                        ['po_buyer',$data_preparation->po_buyer],
                        ['qty_conversion',sprintf('%0.8f',$data_preparation->qty_booking)],
                        ['warehouse',$data_preparation->warehouse_id]
                    ])
                    ->whereNull('cancel_planning')
                    ->exists();

                    if(!$is_preparation_exists)
                    {
                        if($data_preparation->po_detail_id == 'FREE STOCK' || $data_preparation->po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$data_preparation->po_detail_id)
                            ->first();
                            
                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            $c_order_id             = ($material_arrival ? $material_arrival->c_order_id : $data_preparation->c_order_id);
                        }

                        $_item_id_source = ($data_preparation->item_id_book != $data_preparation->item_id_source ? $data_preparation->item_id_source : null);

                        $material_preparation = MaterialPreparation::Create([
                            'barcode'               => $data_preparation->barcode.'-'.$data_preparation->po_buyer,
                            'referral_code'         => $data_preparation->referral_code,
                            'sequence'              => $data_preparation->sequence,
                            'item_id'               => $data_preparation->item_id_book,
                            'item_id_source'        => $_item_id_source,
                            'c_order_id'            => $c_order_id,
                            'c_bpartner_id'         => $data_preparation->c_bpartner_id,
                            'supplier_name'         => $data_preparation->supplier_name,
                            'document_no'           => $data_preparation->document_no,
                            'po_buyer'              => $data_preparation->po_buyer,
                            'uom_conversion'        => $data_preparation->uom,
                            'po_detail_id'          => $data_preparation->po_detail_id,
                            'qty_conversion'        => sprintf('%0.8f',$data_preparation->qty_booking),
                            'qty_reconversion'      => sprintf('%0.8f',$data_preparation->qty_booking),
                            'job_order'             => $data_preparation->job_order,
                            'style'                 => $data_preparation->style,
                            'article_no'            => $data_preparation->article_no,
                            'warehouse'             => $data_preparation->warehouse_id,
                            'item_code'             => $data_preparation->item_code,
                            'item_desc'             => $data_preparation->item_desc,
                            'category'              => $data_preparation->category,
                            'cancel_planning'       => null,
                            'is_backlog'            => false,
                            'is_from_closing'       => $is_closing,
                            'ict_log'               => $data_preparation->remark_closing,
                            'total_carton'          => 1,
                            'type_po'               => 1,
                            'planning_cutting_date' => $data_preparation->planning_date,
                            'user_id'               => $data_preparation->user_id,
                            'last_status_movement'  => 'relax',
                            'is_need_to_be_switch'  => false,
                            'last_locator_id'       => $relax->id,
                            'last_movement_date'    => $data_preparation->preparation_date,
                            'created_at'            => $data_preparation->preparation_date,
                            'updated_at'            => $data_preparation->preparation_date,
                            'last_user_movement_id' => $data_preparation->user_id
                        ]);
    
                        $is_movement_exists = MaterialMovement::where([
                            ['is_active',true],
                            ['is_integrate',false],
                            ['from_location',$inventory->id],
                            ['from_locator_erp_id',$inventory->area->erp_id],
                            ['to_destination',$relax->id],
                            ['to_locator_erp_id',$relax->area->erp_id],
                            ['po_buyer',$data_preparation->po_buyer],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status','relax'],
                        ])
                        ->first();

                        if($is_movement_exists)
                        {
                            $is_movement_exists->updated_at = $data_preparation->preparation_date;
                            $is_movement_exists->save();

                            $material_movement_id = $is_movement_exists->id;
                        }else
                        {
                            $material_movement = MaterialMovement::FirstOrCreate([
                                'from_location'         => $inventory->id,
                                'to_destination'        => $relax->id,
                                'from_locator_erp_id'   => $inventory->area->erp_id,
                                'to_locator_erp_id'     => $relax->area->erp_id,
                                'is_active'             => true,
                                'po_buyer'              => $data_preparation->po_buyer,
                                'status'                => 'relax',
                                'created_at'            => $data_preparation->preparation_date,
                                'updated_at'            => $data_preparation->preparation_date,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                                
                            ]);

                            $material_movement_id = $material_movement->id;
                        }
                    
                        $is_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['is_active',true],
                            ['is_integrate',false],
                            ['item_id',$material_preparation->item_id],
                            ['warehouse_id',$material_preparation->warehouse],
                            ['material_preparation_id',$material_preparation->id],
                            ['qty_movement',sprintf('%0.8f',$material_preparation->qty_conversion)],
                        ])
                        ->exists();

                        if(!$is_movement_line_exists)
                        {
                            $new_material_movement_line = MaterialMovementLine::Create([
                                'material_movement_id'          => $material_movement_id,
                                'material_preparation_id'       => $material_preparation->id,
                                'item_id'                       => $material_preparation->item_id,
                                'item_id_source'                => $material_preparation->item_id_source,
                                'item_code'                     => $material_preparation->item_code,
                                'type_po'                       => 1,
                                'c_order_id'                    => $material_preparation->c_order_id,
                                'c_bpartner_id'                 => $material_preparation->c_bpartner_id,
                                'supplier_name'                 => $material_preparation->supplier_name,
                                'c_orderline_id'                => $c_orderline_id,
                                'warehouse_id'                  => $material_preparation->warehouse,
                                'document_no'                   => $material_preparation->document_no,
                                'uom_movement'                  => $material_preparation->uom_conversion,
                                'qty_movement'                  => sprintf('%0.8f',$material_preparation->qty_conversion),
                                'date_movement'                 => $material_preparation->last_movement_date,
                                'created_at'                    => $material_preparation->last_movement_date,
                                'updated_at'                    => $material_preparation->last_movement_date,
                                'date_receive_on_destination'   => $material_preparation->last_movement_date,
                                'is_active'                     => true,
                                'is_integrate'                  => false,
                                'user_id'                       => $material_preparation->last_user_movement_id
                            ]);

                            $material_preparation->last_material_movement_line_id = $new_material_movement_line->id;
                            $material_preparation->save();
                        }
                        
    
                        DetailMaterialPreparation::Create([
                            'material_preparation_id'               => $material_preparation->id,
                            'detail_material_preparation_fabric_id' => $value->id,
                            'user_id'                               => $data_preparation->user_id
                        ]);
    
                        Temporary::Create([
                            'barcode'       => $data_preparation->po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => $data_preparation->user_id,
                            'created_at'    => $data_preparation->preparation_date,
                            'updated_at'    => $data_preparation->preparation_date,
                        ]);
    
                        $concatenate .= "'" .$material_preparation->barcode."',";

                        
                    }
                }

                $value->is_status_prepare_inserted_to_material_preparation = true;
                $value->save();
                
                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
            }
        }

        $concatenate = substr_replace($concatenate, '', -1);
        if($concatenate !='')
        {
            DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
            DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
        }
    }
    
    static function insertMovementOutIntegration()
    {   
        $data = DetailMaterialPreparationFabric::whereHas('materialPreparationFabric',function($query)
        {
            $query->whereNull('deleted_at');
        })
        ->where([
            ['last_status_movement','out'],
            ['is_status_out_inserted_to_material_preparation',false],
           // ['material_preparation_fabric_id','44975ab0-5f74-11ea-b75e-b99c84b7d36e'],
        ])
        ->take(100)
        ->get();
        
        $concatenate = '';
        $today          = Carbon::now()->format('Y-m-d');

        foreach ($data as $key => $value) 
        {
            try
            {
                db::beginTransaction();
                
                echo $value->id.'</br>';
                $closing_reason                         = $value->remark_closing;
                $is_closing                             = $value->is_closing;
                $warehouse_id                           = $value->materialPreparationFabric->warehouse_id;
                $detail_material_preparation_fabric_id  = $value->id;

                $relax = Locator::whereHas('area',function ($query) use ($warehouse_id){
                    $query->where('name','RELAX')
                    ->where('warehouse',$warehouse_id);
                })
                ->first();
                
                if(!$is_closing)
                {
                    $data_preparations =  DB::select(db::raw("SELECT * FROM get_movement_out_fabric(
                        '".$detail_material_preparation_fabric_id."'
                        )"
                    ));
    
                }else
                {
                    $data_preparations =  DB::select(db::raw("SELECT * FROM get_movement_out_closing_fabric(
                        '".$detail_material_preparation_fabric_id."'
                        )"
                    ));
    
                }
            
            
                foreach ($data_preparations as $key_2 => $data_preparation) 
                {
                    $is_preparation_exists = MaterialPreparation::where([
                        ['barcode',$data_preparation->barcode.'-'.$data_preparation->po_buyer],
                        ['po_buyer',$data_preparation->po_buyer],
                        ['qty_conversion',sprintf('%0.8f',$data_preparation->qty_booking)],
                        ['warehouse',$data_preparation->warehouse_id]
                    ])
                    ->whereNull('cancel_planning')
                    ->first();

                    if($is_preparation_exists)
                    {
                        $warehouse_id = $is_preparation_exists->warehouse;

                        if($is_preparation_exists->po_detail_id == 'FREE STOCK' || $is_preparation_exists->po_detail_id == 'FREE STOCK-CELUP')
                        {
                            $c_order_id             = 'FREE STOCK';
                            $no_packing_list        = '-';
                            $no_invoice             = '-';
                            $c_orderline_id         = '-';
                        }else
                        {
                            $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                            ->whereNull('material_roll_handover_fabric_id')
                            ->where('po_detail_id',$is_preparation_exists->po_detail_id)
                            ->first();

                            $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                            $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                            $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                        }
                        
                        $is_out_to_distribution     = Locator::where('id',$data_preparation->last_locator_id)
                        ->whereHas('area',function($query) use ($warehouse_id)
                        {
                            $query->where('name','DISTRIBUTION')
                            ->where('warehouse',$warehouse_id);
                        })
                        ->exists();

                        $is_preparation_exists->deleted_at              = carbon::now();
                        $is_preparation_exists->last_status_movement    = ($is_out_to_distribution ? 'out-distribution' : 'out-cutting');
                        $is_preparation_exists->last_locator_id         = $data_preparation->last_locator_id;
                        $is_preparation_exists->last_movement_date      = $data_preparation->movement_out_date;
                        $is_preparation_exists->last_user_movement_id   = $data_preparation->user_id;
                        $is_preparation_exists->save();

                        $material_preparation_id    = $is_preparation_exists->id;
                        $barcode                    = $is_preparation_exists->barcode;

                        $is_movement_exists = MaterialMovement::where([
                            ['is_active',true],
                            ['is_integrate',false],
                            ['from_location',$relax->id],
                            ['from_locator_erp_id',$relax->area->erp_id],
                            ['to_destination',$is_preparation_exists->last_locator_id],
                            ['to_locator_erp_id',($is_preparation_exists->last_locator_id ? $is_preparation_exists->lastLocator->area->erp_id : null)],
                            ['po_buyer',$is_preparation_exists->po_buyer],
                            ['no_packing_list',$no_packing_list],
                            ['no_invoice',$no_invoice],
                            ['status',($is_out_to_distribution ? 'out-distribution' : 'out-cutting')],
                        ])
                        ->first();

                        if($is_movement_exists)
                        {
                            $is_movement_exists->updated_at = $is_preparation_exists->last_movement_date;
                            $is_movement_exists->save();

                            $material_movement_id = $is_movement_exists->id;
                        }else
                        {
                            $material_movement = MaterialMovement::FirstOrCreate([
                                'from_location'         => $relax->id,
                                'to_destination'        => $is_preparation_exists->last_locator_id,
                                'from_locator_erp_id'   => $relax->area->erp_id,
                                'to_locator_erp_id'     => ($is_preparation_exists->last_locator_id ? $is_preparation_exists->lastLocator->area->erp_id : null),
                                'is_active'             => true,
                                'po_buyer'              => $is_preparation_exists->po_buyer,
                                'status'                => ($is_out_to_distribution ? 'out-distribution' : 'out-cutting'),
                                'created_at'            => $is_preparation_exists->last_movement_date,
                                'updated_at'            => $is_preparation_exists->last_movement_date,
                                'no_packing_list'       => $no_packing_list,
                                'no_invoice'            => $no_invoice,
                            ]);

                            $material_movement_id = $material_movement->id;
                        }
                        
                        $is_movement_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                        ->where([
                            ['material_movement_id',$material_movement_id],
                            ['is_active',true],
                            ['is_integrate',false],
                            ['item_id',$is_preparation_exists->item_id],
                            ['warehouse_id',$is_preparation_exists->warehouse],
                            ['material_preparation_id',$is_preparation_exists->id],
                            ['qty_movement',sprintf('%0.8f',$is_preparation_exists->qty_conversion)],
                        ])
                        ->exists();

                        if(!$is_movement_line_exists)
                        {
                            $movement_out   = Carbon::createFromFormat('Y-m-d H:i:s', $is_preparation_exists->last_movement_date)->format('Y-m-d');
                            $new_material_movement_line = MaterialMovementLine::Create([
                                'material_movement_id'                          => $material_movement_id,
                                'material_preparation_id'                       => $is_preparation_exists->id,
                                'item_id'                                       => $is_preparation_exists->item_id,
                                'item_id_source'                                => $is_preparation_exists->item_id_source,
                                'item_code'                                     => $is_preparation_exists->item_code,
                                'type_po'                                       => 1,
                                'c_order_id'                                    => $is_preparation_exists->c_order_id,
                                'c_bpartner_id'                                 => $is_preparation_exists->c_bpartner_id,
                                'supplier_name'                                 => $is_preparation_exists->supplier_name,
                                'warehouse_id'                                  => $is_preparation_exists->warehouse,
                                'document_no'                                   => $is_preparation_exists->document_no,
                                'nomor_roll'                                    => '-',
                                'c_orderline_id'                                => $c_orderline_id,
                                'uom_movement'                                  => $is_preparation_exists->uom_conversion,
                                'qty_movement'                                  => sprintf('%0.8f',$is_preparation_exists->qty_conversion),
                                'date_movement'                                 => $is_preparation_exists->last_movement_date,
                                'created_at'                                    => $is_preparation_exists->last_movement_date,
                                'updated_at'                                    => $is_preparation_exists->last_movement_date,
                                'date_receive_on_destination'                   => $is_preparation_exists->last_movement_date,
                                'is_inserted_to_material_movement_per_size'     => true,
                                'inserted_to_material_movement_per_size_date'   => ($movement_out == $today ? null : $is_preparation_exists->last_movement_date),
                                'is_active'                                     => true,
                                'is_integrate'                                  => false,
                                'user_id'                                       => $is_preparation_exists->last_user_movement_id
                            ]);

                            $is_preparation_exists->last_material_movement_line_id = $new_material_movement_line->id;
                            $is_preparation_exists->save();
                        }
        
                        Temporary::Create([
                            'barcode'       => $is_preparation_exists->po_buyer,
                            'status'        => 'mrp',
                            'user_id'       => $is_preparation_exists->user_id,
                            'created_at'    => $is_preparation_exists->movement_out_date,
                            'updated_at'    => $is_preparation_exists->movement_out_date,
                            
                        ]);


                        if($is_closing)
                        {
                            $inventory_erp = Locator::with('area')
                            ->where([
                                ['is_active',true],
                                ['rack','ERP INVENTORY'],
                            ])
                            ->whereHas('area',function ($query) use($warehouse_id)
                            {
                                $query->where('is_active',true);
                                $query->where('warehouse',$warehouse_id);
                                $query->where('name','ERP INVENTORY');
                            })
                            ->first();
                            
                            $is_movement_integration_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$is_preparation_exists->po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list','CLOSING'],
                                ['no_invoice','CLOSING'],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();
                            
                            if(!$is_movement_integration_exists)
                            {
                                $material_integration_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $is_preparation_exists->po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => 'CLOSING',
                                    'no_invoice'            => 'CLOSING',
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $is_preparation_exists->last_movement_date,
                                    'updated_at'            => $is_preparation_exists->last_movement_date,
                                ]);
            
                                $material_integration_movement_id = $material_integration_movement->id;
                            }else
                            {
                                $is_movement_integration_exists->updated_at = $is_preparation_exists->last_movement_date;
                                $is_movement_integration_exists->save();
            
                                $material_integration_movement_id = $is_movement_integration_exists->id;
                            }
            
                            $is_material_movement_line_integration_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_integration_movement_id],
                                ['material_preparation_id',$is_preparation_exists->id],
                                ['warehouse_id',$is_preparation_exists->warehouse],
                                ['item_id',$is_preparation_exists->item_id],
                                ['is_active',true],
                                ['is_integrate',false],
                                ['qty_movement',$is_preparation_exists->qty_conversion],
                                ['date_movement',$is_preparation_exists->last_movement_date],
                            ])
                            ->exists();
            
                            if(!$is_material_movement_line_integration_exists)
                            {
                                $new_material_movement_line = MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_integration_movement_id,
                                    'material_preparation_id'       => $is_preparation_exists->id,
                                    'item_code'                     => $is_preparation_exists->item_code,
                                    'item_id'                       => $is_preparation_exists->item_id,
                                    'item_id_source'                => $is_preparation_exists->item_id_source,
                                    'c_order_id'                    => $is_preparation_exists->c_order_id,
                                    'c_orderline_id'                => '-',
                                    'c_bpartner_id'                 => $is_preparation_exists->c_bpartner_id,
                                    'supplier_name'                 => $is_preparation_exists->supplier_name,
                                    'type_po'                       => 1,
                                    'is_active'                     => true,
                                    'is_integrate'                  => false,
                                    'uom_movement'                  => $is_preparation_exists->uom_conversion,
                                    'qty_movement'                  => $is_preparation_exists->qty_conversion,
                                    'date_movement'                 => $is_preparation_exists->last_movement_date,
                                    'created_at'                    => $is_preparation_exists->last_movement_date,
                                    'updated_at'                    => $is_preparation_exists->last_movement_date,
                                    'date_receive_on_destination'   => $is_preparation_exists->last_movement_date,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $is_preparation_exists->warehouse,
                                    'document_no'                   => $is_preparation_exists->document_no,
                                    'note'                          => 'PROSES INTEGRASI DIKARNAKAN CLOSING DARI MENU PREPARATION FABRIC (ALASAN DI CLOSE KARNA '.$closing_reason.')',
                                    'is_active'                     => true,
                                    'user_id'                       => $is_preparation_exists->last_user_movement_id,
                                ]);
            
                                $is_preparation_exists->last_material_movement_line_id = $new_material_movement_line->id;
                                $is_preparation_exists->save();
                            }
                        }
        
                        $concatenate .= "'" .$barcode."',";
                    }
                }

                $value->is_status_out_inserted_to_material_preparation = true;
                $value->save();

                db::commit();
            }catch (Exception $ex){
                db::rollback();
                $message = $ex->getMessage();
            }
        }

        $concatenate = substr_replace($concatenate, '', -1);
        if($concatenate !='')
        {
            DB::select(db::raw("SELECT * FROM delete_duplicate_preparation_acc(array[".$concatenate."]);"));
            DB::select(db::raw("SELECT * FROM delete_duplicate_movement(array[".$concatenate."]);"));
        }
    }

    private function doMovementPerSize()
    {
        $data           = DB::table('outstanding_material_movement_per_size_v')
        ->where(db::raw("to_char(date_movement, 'YYYY')"),'2020')
        ->take(20)
        ->get();

        $insert_date    = Carbon::now();
        $today          = Carbon::now()->format('Y-m-d');

        foreach ($data as $key => $value) 
        {
            try 
            {
                DB::beginTransaction();

                $material_movement_line_id  = $value->id;;
                $material_movement_line     = MaterialMovementLine::find($material_movement_line_id);

                if($material_movement_line)
                {
                    $po_buyer       = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->po_buyer : null);
                    $item_id        = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id : null);
                    $item_id_source = ($material_movement_line->material_preparation_id ? $material_movement_line->materialPreparation->item_id_source : null);
                    $qty_movement   = sprintf('%0.8f',$material_movement_line->qty_movement);
                    $created_at     = $material_movement_line->created_at;
                    $movement_out   = $material_movement_line->created_at->format('Y-m-d');

                    if($movement_out < $today) 
                    {
                        $integration_date   = $insert_date;
                        $is_integrate       = true;
                        $remark_integration = 'lgsg di true kan karna tanggal out lebih kecil dari hari ini';
                    }else
                    {
                        $integration_date   = null;
                        $is_integrate       = false;
                        $remark_integration = null;
                    }

                    if($po_buyer && $item_id)
                    {
                        $detail_material_requirements = MaterialRequirementPerSize::where([
                            ['po_buyer',$po_buyer],
                            ['item_id',$item_id],
                        ])
                        ->orderby('garment_size','asc')
                        ->orderby('total_qty','asc')
                        ->get();

                        $total_line = count($detail_material_requirements);
                        if($total_line > 0)
                        {
                            foreach ($detail_material_requirements as $key_line => $detail_material_requirement) 
                            {
                                $size                   = $detail_material_requirement->garment_size;
                                $total_qty              = sprintf('%0.8f',$detail_material_requirement->total_qty);
                                
                                $flag                   = 0;
                                $data_saldo_per_lines    =  DB::select(db::raw("SELECT * FROM get_outstanding_per_size(
                                    '".$po_buyer."',
                                    '".$item_id."'
                                    )"
                                ));
    
                                foreach ($data_saldo_per_lines as $key_1 => $data_saldo_per_line) 
                                {
                                    if($data_saldo_per_line->outstanding > 0) $flag++;
                                }
    
                                if($total_line == 1) $total_outstanding = sprintf('%0.8f',$qty_movement);
                                else
                                {
                                    $total_allocated    = MaterialMovementPerSize::where([
                                        ['size',$size],
                                        ['po_buyer',$po_buyer],
                                        ['item_id',$item_id],
                                    ])
                                    ->whereNull('reverse_date')
                                    ->sum('qty_per_size');
                                    
                                    if($flag == 1)$total_outstanding = sprintf('%0.8f',$qty_movement);
                                    else $total_outstanding = sprintf('%0.8f',$total_qty - $total_allocated);
                                }
                                
                                if($total_outstanding > 0 && $qty_movement >0)
                                {
                                    if ($qty_movement/$total_outstanding >= 1) $supplied = $total_outstanding;
                                    else  $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
    
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'integration_date'          => $integration_date,
                                                'is_integrate'              => $is_integrate,
                                                'remark_integration'        => $remark_integration,
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                            ]);
                                        }
    
                                        $qty_movement       -= $supplied;
                                        $total_outstanding  -= $supplied;
                                    }
                                }else if($total_outstanding == 0 && $flag == 0)
                                {
                                    $supplied = $qty_movement;
    
                                    if($supplied > 0)
                                    {
                                        $is_exists              = MaterialMovementPerSize::where([
                                            ['material_movement_line_id',$material_movement_line_id],
                                            ['size',$size],
                                            ['po_buyer',$po_buyer],
                                            ['item_id',$item_id],
                                            ['qty_per_size',sprintf('%0.8f',$supplied)],
                                        ])
                                        ->whereNull('reverse_date')
                                        ->exists();
        
                                        if(!$is_exists)
                                        {
                                            MaterialMovementPerSize::FirstOrCreate([
                                                'material_movement_line_id' => $material_movement_line_id,
                                                'size'                      => $size,
                                                'po_buyer'                  => $po_buyer,
                                                'item_id'                   => $item_id,
                                                'integration_date'          => $integration_date,
                                                'is_integrate'              => $is_integrate,
                                                'remark_integration'        => $remark_integration,
                                                'item_id_source'            => $item_id_source,
                                                'qty_per_size'              => sprintf('%0.8f',$supplied),
                                                'reverse_date'              => null,
                                                'created_at'                => $created_at,
                                                'updated_at'                => $created_at,
                                            ]);
                                        }
                                    }
                                    
    
                                    $qty_movement       -= $supplied;
                                }
                            }
    
                            $material_movement_line->inserted_to_material_movement_per_size_date = $insert_date;
                            $material_movement_line->save();
                        }else
                        {
                            // klo ga ketemu bom per itemnya pake globlan aja
                            $c_order_id                 = $material_movement_line->materialPreparation->c_order_id;
                            $po_detail_id               = $material_movement_line->materialPreparation->po_detail_id;
                            $po_buyer                   = $material_movement_line->materialPreparation->po_buyer;
                            $warehouse_id               = $material_movement_line->materialPreparation->warehouse;
                            $item_code                  = $material_movement_line->materialPreparation->item_code;
                            $c_bpartner_id              = $material_movement_line->materialPreparation->c_bpartner_id;
                            $supplier_name              = $material_movement_line->materialPreparation->supplier_name;
                            $uom                        = $material_movement_line->materialPreparation->uom_conversion;
                            $type_po                    = $material_movement_line->materialPreparation->type_po;
                            $document_no                = $material_movement_line->materialPreparation->document_no;
                            $last_user_movement_id      = $material_movement_line->materialPreparation->last_user_movement_id;
                            $material_preparation_id    = $material_movement_line->material_preparation_id;
                            
                            $inventory_erp = Locator::with('area')
                            ->where([
                                ['is_active',true],
                                ['rack','ERP INVENTORY'],
                            ])
                            ->whereHas('area',function ($query) use($warehouse_id)
                            {
                                $query->where('is_active',true);
                                $query->where('warehouse',$warehouse_id);
                                $query->where('name','ERP INVENTORY');
                            })
                            ->first();

                            if($po_detail_id == 'FREE STOCK' || $po_detail_id == 'FREE STOCK-CELUP')
                            {
                                $c_order_id             = 'FREE STOCK';
                                $no_packing_list        = '-';
                                $no_invoice             = '-';
                                $c_orderline_id         = '-';
                            }else
                            {
                                $material_arrival       = MaterialArrival::whereNull('material_subcont_id')
                                ->whereNull('material_roll_handover_fabric_id')
                                ->where('po_detail_id',$po_detail_id)
                                ->first();
                                
                                $no_packing_list        = ($material_arrival ? $material_arrival->no_packing_list : '-');
                                $no_invoice             = ($material_arrival ? $material_arrival->no_invoice : '-');
                                $c_orderline_id         = ($material_arrival ? $material_arrival->c_orderline_id : '-');
                            }

                            $is_movement_exists = MaterialMovement::where([
                                ['from_location',$inventory_erp->id],
                                ['to_destination',$inventory_erp->id],
                                ['from_locator_erp_id',$inventory_erp->area->erp_id],
                                ['to_locator_erp_id',$inventory_erp->area->erp_id],
                                ['po_buyer',$po_buyer],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['no_packing_list',$no_packing_list],
                                ['no_invoice',$no_invoice],
                                ['status','integration-to-inventory-erp'],
                            ])
                            ->first();

                            if(!$is_movement_exists)
                            {
                                $material_movement = MaterialMovement::firstOrCreate([
                                    'from_location'         => $inventory_erp->id,
                                    'to_destination'        => $inventory_erp->id,
                                    'from_locator_erp_id'   => $inventory_erp->area->erp_id,
                                    'to_locator_erp_id'     => $inventory_erp->area->erp_id,
                                    'po_buyer'              => $po_buyer,
                                    'is_integrate'          => false,
                                    'is_active'             => true,
                                    'no_packing_list'       => $no_packing_list,
                                    'no_invoice'            => $no_invoice,
                                    'status'                => 'integration-to-inventory-erp',
                                    'created_at'            => $created_at,
                                    'updated_at'            => $created_at,
                                ]);
        
                                $material_movement_id = $material_movement->id;
                            }else
                            {
                                $is_movement_exists->updated_at = $created_at;
                                $is_movement_exists->save();
        
                                $material_movement_id = $is_movement_exists->id;
                            }


                            $is_line_exists = MaterialMovementLine::whereNotNull('material_preparation_id')
                            ->where([
                                ['material_movement_id',$material_movement_id],
                                ['material_preparation_id',$material_preparation_id],
                                ['qty_movement',-1*$qty_movement],
                                ['item_id',$item_id],
                                ['is_integrate',false],
                                ['is_active',true],
                                ['warehouse_id',$warehouse_id],
                                ['date_movement',$created_at],
                            ])
                            ->exists();

                            if(!$is_line_exists)
                            {
                                MaterialMovementLine::Create([
                                    'material_movement_id'          => $material_movement_id,
                                    'material_preparation_id'       => $material_preparation_id,
                                    'item_code'                     => $item_code,
                                    'item_id'                       => $item_id,
                                    'c_order_id'                    => $c_order_id,
                                    'c_orderline_id'                => $c_orderline_id,
                                    'c_bpartner_id'                 => $c_bpartner_id,
                                    'supplier_name'                 => $supplier_name,
                                    'type_po'                       => $type_po,
                                    'is_integrate'                  => false,
                                    'is_active'                     => true,
                                    'uom_movement'                  => $uom,
                                    'qty_movement'                  => -1*$qty_movement,
                                    'date_movement'                 => $created_at,
                                    'created_at'                    => $created_at,
                                    'updated_at'                    => $created_at,
                                    'date_receive_on_destination'   => $created_at,
                                    'integration_date'              => $integration_date,
                                    'is_integrate'                  => $is_integrate,
                                    'remark_integration'            => $remark_integration,
                                    'nomor_roll'                    => '-',
                                    'warehouse_id'                  => $warehouse_id,
                                    'document_no'                   => $document_no,
                                    'note'                          => 'INTEGRASI INTERNAL USE, KARENA KETIKA OUT ISSUE TIDAK KETEMU DI BOM',
                                    'is_active'                     => true,
                                    'user_id'                       => $last_user_movement_id,
                                ]);
                            }

                            $material_movement_line->is_inserted_to_material_movement_per_size = false;
                            $material_movement_line->save();
                        }
                        
                    }
                    
                }

                DB::commit();

            } catch (Exception $e) {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }
        }
    }

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
