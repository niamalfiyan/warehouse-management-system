<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Scheduler;
use App\Models\Temporary;
use App\Models\MaterialStock;
use App\Http\Controllers\AccessoriesReportMaterialStockController as ReportStock;


class DailyUpdateStockReserveAccAoi2 extends Command
{
    protected $signature = 'updateStockReserveAccAoi2:update';
    protected $description = 'update stock reserve acc';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','SYNC_UPDATE_RESERVE_STOCK_AOI_2')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'SYNC_UPDATE_RESERVE_STOCK_AOI_2',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC UPDATE RESERVE STOCK AOI 2 START JOB AT '.carbon::now());
            $this->doJob();
            $this->info('SYNC UPDATE RESERVE STOCK AOI 2 END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }else{
            $this->info('SYNC UPDATE RESERVE STOCK AOI 2 SEDANG BERJALAN');
        } 
    }

    static function doJob()
    {

        $temporaries            = Temporary::where('status','reserved_stock')
                                  ->where('warehouse', '1000013')
                                  ->orderBy('created_at', 'asc')
                                  ->take(10)
                                  ->get();
        
        //$update_status = $temporaries->update(['string_1' => 'running']);

        // if(count($temporaries) == 0)
        // {
        //     $temporaries        = Temporary::where('status','reserved_stock')
        //                         ->where('warehouse', '1000002')
        //                         ->whereNull('string_1')
        //                         ->orderBy('created_at', 'desc')
        //                         ->take(10)
        //                         ->get();
        // }
        $confirm_date           = Carbon::now();
            
        foreach ($temporaries as $key => $temporary) 
        {
            try 
            {
                DB::beginTransaction();
                $material_stock_id = $temporary->barcode;

                $material_stock     = MaterialStock::where([
                    ['id',$material_stock_id],
                    ['is_running_stock',true],
                    ['is_closing_balance',false],
                    ['is_allocated',false],
                    ['category','!=','CT'],
                ])
                ->whereNull('last_status')
                ->whereNotNull('approval_date')
                ->whereNull('deleted_at')
                ->first();

                if($material_stock)
                {
                    $material_stock->last_status    = 'prepared';
                    $material_stock->save();

                    $system                     = User::where([
                        ['name','system'],
                        ['warehouse',$material_stock->warehouse_id]
                    ])
                    ->first();

                    ReportStock::doAllocation($material_stock,$system->id,$confirm_date);
    
                    $material_stock->last_status                = null;
                    $material_stock->save();
                }

                $temporary->delete();

                DB::commit();
            } catch (Exception $e) 
            {
                DB::rollBack();
                $message = $e->getMessage();
                ErrorHandler::db($message);
            }

        }

    } 

    private function setStatus($scheduler,$status)
    {
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler)
    {
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler)
    {
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
