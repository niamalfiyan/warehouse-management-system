<?php namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Http\Controllers\FabricMaterialStockOtherController;

use App\Models\Scheduler;

class DailyInsertStockFabricMM extends Command
{
    protected $signature = 'dailyMaterialStockFabricMM:insert';
    protected $description = 'Insert material stock mm';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $scheduler = Scheduler::where('job','SYNC_MATERIAL_STOCK_MM')
        ->where('status','queue')
        ->first();

        if(!empty($scheduler))
        {
            $this->info('SYNC MATERIAL STOCK MM JOB AT '.carbon::now());
            $this->setStatus($scheduler,'ongoing');
            $this->setStartJob($scheduler);
            FabricMaterialStockOtherController::insertStockAllocationMM();
            $this->setStatus($scheduler,'done');
            $this->setEndJob($scheduler);
            $this->info('DONE SYNC MATERIAL STOCK MM AT '.carbon::now());
        }else
        {
            $is_schedule_on_going = Scheduler::where('job','SYNC_MATERIAL_STOCK_MM')
            ->where('status','ongoing')
            ->exists();

            if(!$is_schedule_on_going)
            {
                $new_scheduler = Scheduler::create([
                    'job'       => 'SYNC_MATERIAL_STOCK_MM',
                    'status'    => 'ongoing'
                ]);

                $this->info('SYNC MATERIAL STOCK MM JOB AT '.carbon::now());
                $this->setStartJob($new_scheduler);
                FabricMaterialStockOtherController::insertStockAllocationMM();
                $this->setStatus($new_scheduler,'done');
                $this->setEndJob($new_scheduler); 
                $this->info('DONE MATERIAL STOCK MM JOB AT '.carbon::now());
            }else
            {
                $this->info('SYNC MATERIAL STOCK MM SEDANG BERJALAN');
            }
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=> $status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=> Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=> Carbon::now()
        ]);
    }
}
