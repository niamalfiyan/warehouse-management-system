<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WmsOldController;
use DB;

class DailyWmsOld extends Command
{
    protected $signature = 'dailywmsold:insert';
    protected $description = 'insert movement from old wms';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        WmsOldController::daily_cron_insert();
        DB::select(db::raw("
            DELETE FROM wms_olds
            WHERE id IN
            (SELECT id
            FROM 
            (SELECT id,
                ROW_NUMBER() OVER( PARTITION BY po_buyer,
                status,item_code
            ORDER BY  id ) AS row_num
            FROM wms_olds ) t
            WHERE t.row_num > 1 )   
        "));
        $this->info('insert wms old success');
    }
}
