<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WmsOldController;

class HourlyInserWmsOld extends Command
{
    protected $signature = 'hourlywmsold:insert';
    protected $description = 'Hourly Insert Wms Old';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        WmsOldController::hourly_cron_insert();
        $this->info('Hourly Insert WMS Old success');
    }
}
