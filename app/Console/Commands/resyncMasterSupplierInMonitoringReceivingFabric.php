<?php namespace App\Console\Commands;

use DB;
use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\Scheduler;
use App\Models\MonitoringReceivingFabric;

class resyncMasterSupplierInMonitoringReceivingFabric extends Command
{
    protected $signature    = 'resync:supplierMonitoringReceivingFabric';
    protected $description  = 'resync master supplier in monitoring receiving fabric';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $is_schedule_on_going = Scheduler::where('job','RESYNC_SUPPLIER_MONITORING_RECEIVING_FABRIC')
        ->where('status','ongoing')
        ->exists();

        if(!$is_schedule_on_going)
        {
            $new_scheduler = Scheduler::create([
                'job' => 'RESYNC_SUPPLIER_MONITORING_RECEIVING_FABRIC',
                'status' => 'ongoing'
            ]);
            $this->setStartJob($new_scheduler);
            
            $this->info('SYNC MASTER SUPPLIER IN MONITORING RECEIVING FABRIC  START JOB AT '.carbon::now());
            $this->updateData();
            $this->info('SYNC MASTER SUPPLIER IN MONITORING RECEIVING FABRIC  END JOB AT '.carbon::now());

            $this->setStatus($new_scheduler,'done');
            $this->setEndJob($new_scheduler); 
        }
    }

    private function updateData()
    {
        $data = DB::table('resync_master_supplier_in_monitoring_receiving_fabrics_v')->get();
        foreach ($data as $key => $value) 
        {
            $id     = $value->id;
            $item   = MonitoringReceivingFabric::find($id);

            $item->c_bpartner_id = $value->c_bpartner_id_master;
            $item->supplier_name = $value->supplier_name_master;
            $item->save();
        }
    }

    private function setStatus($scheduler,$status){
        $scheduler->update([
            'status'=>$status
        ]);
    }

    private function setStartJob($scheduler){
        $scheduler->update([
            'start_job'=>Carbon::now()
        ]);
    }

    private function setEndJob($scheduler){
        $scheduler->update([
            'end_job'=>Carbon::now()
        ]);
    }
}
