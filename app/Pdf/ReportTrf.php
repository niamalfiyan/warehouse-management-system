<?php

namespace App\Pdf;

use TJGazel\LaraFpdf\LaraFpdf;
use Carbon\Carbon;
use Auth;
use DB;

class ReportTrf extends LaraFpdf
{
    //
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
        parent::__construct('P','mm', array(210, 297)); //A4 aslinya 210x297
        $this->SetTitle('Detail TRF', true);
        $this->SetAuthor('TJGazel', true);
        $this->AddPage();
        $this->AliasNbPages();
        $this->Body();
    }

    public function Header($from=0, $to=1000)
    {
        $this->SetFont('Arial', 'B', '14','C');
        $this->Ln(5);
        $this->Image(public_path('/images/bbi_logo.png'),10,5,45,9,'PNG');
        $this->Image(public_path('/images/logo_aoi.jpg'),165,5,32,12,'JPG');
        $this->setXY(100,10);
        $this->Cell(17, -2, 'APPAREL MATERIAL / GARMENT' , 0, 0,'C');
        $this->Ln(8);
        $this->setXY(100,15);
        $this->SetFont('Arial','B','14','C');
        $this->Cell(17, -2, 'TEST REQUISITION FORM' , 0, 0,'C');
        $this->Ln(10);
        $this->setX(120);
        $this->SetFont('Arial','','12','C');
        $this->Cell(40, 10, 'Submitted by :', 0, 0,'R');
        $this->Ln(10);
        $this->setX(120);
        $this->Cell(40, 10, 'Date of Submitted :', 0, 0,'R');
        $this->Ln(10);
        $this->SetFont('Arial','','10','C');
        $this->Cell(40, 10, 'Buyer', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Lab Location', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Origin of Specimen', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Category Specimen', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Type Specimen', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Test Required', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Date Information', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'PO Buyer', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Size', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Style', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Article', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Color', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Item', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Destination', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Testing Method', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Part of speciment', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Return Test', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Status Specimen', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(15);
        $this->Cell(40, 10, 'Remarks', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
        $this->Ln(10);
        $this->Cell(40, 10, 'Date of Validate by ', 0, 0,'L');
        $this->setX(50);
        $this->Cell(5, 10, ':', 0, 0,'L');
    }

    public function Body()
    {
        $this->Detail();
        if(count($this->data) > 500){
            $page_count = ceil((count($this->data) - 500) / 500);
            for($i = 0; $i < $page_count; $i++){
                $this->AddPage('P');
                $this->Detail(500 + ($i * 500), 500);
            }
        }
    }

    public function Detail($from = 0, $to = 500)
    {
        $this->SetFont('Arial', '', '15');
        $no = $from+1;
        $this->setXY(10, 40);
        $rows = array();
        // $num = 1;

        foreach ($this->data as $val) {
                // $val->id = $id;
                // dd($val->nik);
                $rows[] = $val;
                // $num++;
              
        }
        //BODY
       
        foreach($rows as $key2 => $row){
        
            $this->setXY(160,25);
            $this->SetFont('Arial','','12','C');
            $absensi = DB::connection('absence_aoi');
            $user = $absensi->table('adt_bbigroup_emp_new')->where('nik', $row->nik)->first();
                   
            $this->Cell(40, 10, $user->name , 0, 0,'L');
            $this->Ln(10);
            $this->setXY(160,35);
            $this->Cell(40, 10, $row->created_at , 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,45);
            $this->Cell(40, 10, $row->buyer, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,55);
            $this->Cell(40, 10, $row->lab_location, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,65);
            $this->Cell(40, 10, $row->asal_specimen, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,75);
            $this->Cell(40, 10, $row->category_specimen, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,85);
            $this->Cell(40, 10, $row->type_specimen, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,95);
            
            if($row->test_required == 'bulktesting_m')
            {
                $this->Cell(40, 10, 'Bulk Testing M (Model Level)', 0, 0,'L');
            }elseif ($row->test_required == 'bulktesting_a') {
                $this->Cell(40, 10, 'Bulk Testing A (Article Level)', 0, 0,'L');
            }elseif($row->test_required == 'bulktesting_selective')
            {
                $this->Cell(40, 10, 'Bulk Testing Selective', 0, 0,'L');
            }elseif($row->test_required == 'reordertesting_m')
            {
                $this->Cell(40, 10, 'Re-Order Testing M (Model Level)', 0, 0,'L');
            }elseif ($row->test_required == 'reordertesting_a') {
                $this->Cell(40, 10, 'Re-Order Testing A (Article Level)', 0, 0,'L');
            }elseif($row->test_required == 'reordertesting_selective')
            {
                $this->Cell(40, 10, 'Re-Order Testing Selective', 0, 0,'L');
            }else{
                $this->Cell(40, 10, 'Re-Test '. $row->previous_trf , 0, 0,'L');
            }

           
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,105);
            if($row->date_information_remark == 'buy_ready')
            {
                $this->Cell(40, 10, 'Buy Ready at '. $row->date_information, 0, 0,'L');
            }elseif($row->date_information_remark == 'output_sewing')
            {
                $this->Cell(40, 10, '1st Output Sewing at '. $row->date_information, 0, 0,'L');
            }else{
                $this->Cell(40, 10, 'PODD at '. $row->date_information, 0, 0,'L');
            }
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,115);
            $this->Cell(40, 10, $row->orderno, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,125);
            $this->Cell(40, 10, $row->size, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,135);
            $this->Cell(40, 10, $row->style, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,145);
            $this->Cell(40, 10, $row->article_no, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,155);
            $this->Cell(40, 10, $row->color, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,165);
            $this->Cell(40, 10, $row->item, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,175);
            $this->Cell(40, 10, $row->destination, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,185);
            $lab = DB::connection('lab');
            $testings = explode(",",$row->testing_method_id);
            $master_method = $lab->table('master_requirements')
            ->leftjoin('master_method', 'master_method.id', '=','master_requirements.master_method_id')
            ->select('master_method.method_name')
            ->wherein('master_requirements.id', $testings)
            ->get();
            $methods = $master_method->implode('method_name', ',');
            $this->Cell(40, 10, $methods, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,195);
            $this->Cell(40, 10, $row->part_of_specimen, 0, 0,'L');
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,205);
            if($row->return_test_sample == true)
            {
                $this->Cell(40, 10, 'YES', 0, 0,'L');
            } else{
                $this->Cell(40, 10, 'NO', 0, 0,'L');
            }
           
            $this->Ln(10);
            $this->SetFont('Arial','','10','C');
            $this->setXY(55,215);
            $this->Cell(40, 10, $row->last_status, 0, 0,'L');
        }
        
    }

    public function Footer()
    {
        // Go to 1.0 cm from bottom
        $this->SetY(-10);
        $this->SetFont('Arial','I',8);
        $this->SetY(-40);
        $this->SetFont('Arial', '', '11');
        $this->Cell(49);
        $this->SetFont('Arial','B',15);
        $this->Cell(340,50,'Halaman '.$this->PageNo().'/{nb}',0,0,'C');
        $this->SetFont('Arial','I',14);
        $this->SetTextColor(0,0,255);
        $this->SetTextColor(0,0,0);
        $this->SetFont('Arial','',14);
        $this->SetFont('Arial','',15);
    }
}
