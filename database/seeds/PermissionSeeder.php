<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'name' => 'menu-user',
                'display_name' => 'menu user',
                'description' => 'menu user'
            ],
            [
                'name' => 'menu-role',
                'display_name' => 'menu role',
                'description' => 'menu role'
            ],
            [
                'name' => 'menu-permission',
                'display_name' => 'menu permission',
                'description' => 'menu permission'
            ],
           
            [
                'name' => 'menu-material-receive-accessories',
                'display_name' => 'menu material receive accessories',
                'description' => 'menu material receive accessories'
            ],
            
            [
                'name' => 'menu-material-receive-febric',
                'display_name' => 'menu material receive febric',
                'description' => 'menu material receive febric'
            ],

            [
                'name' => 'menu-material-check-accessories',
                'display_name' => 'menu material check accessories',
                'description' => 'menu material check accessories'
            ],

             [
                'name' => 'menu-material-check-febric',
                'display_name' => 'menu material check febric',
                'description' => 'menu material check febric'
            ],
            
            [
                'name' => 'menu-print-barcode-receive-accessories',
                'display_name' => 'menu print barcode receive accessories',
                'description' => 'menu print barcode receive accessories'
            ],

            [
                'name' => 'menu-print-barcode-allocation-accessories',
                'display_name' => 'menu print barcode allocation accessories',
                'description' => 'menu print barcode allocation accessories'
            ],

            [
                'name' => 'menu-print-barcode-preparation-febric',
                'display_name' => 'menu print barcode preparation febric',
                'description' => 'menu print barcode preparation febric'
            ],

            [
                'name' => 'menu-reprint-barcode-supplier-febric',
                'display_name' => 'menu reprint barcode supplier febric',
                'description' => 'menu reprint barcode supplier febric'
            ],
           
            [
                'name' => 'menu-material-in-accessories',
                'display_name' => 'menu material in accessories',
                'description' => 'menu material in accessories'
            ],

            [
                'name' => 'menu-material-in-transit-febric',
                'display_name' => 'menu material in transit febric',
                'description' => 'menu material in transit febric'
            ],

            [
                'name' => 'menu-material-change-accessories',
                'display_name' => 'menu material change accessories',
                'description' => 'menu material change accessories'
            ],
            


            [
                'name' => 'menu-material-out-accessories',
                'display_name' => 'menu material out accessories',
                'description' => 'menu material out accessories'
            ],

           
            [
                'name' => 'menu-material-partial-accessories',
                'display_name' => 'menu material partial accessories',
                'description' => 'menu material partial accessories'
            ],

           
            //material other 
            [
                'name' => 'menu-material-other-accessories',
                'display_name' => 'menu material other accessories',
                'description' => 'menu material other accessories'
            ],

            // approval material other 
            [
                'name' => 'menu-approval-material-other-accessories',
                'display_name' => 'menu approval material other accessories',
                'description' => 'menu approval material other accessories'
            ],

            // locator 
            [
                'name' => 'menu-febric-locator',
                'display_name' => 'menu febric locator',
                'description' => 'menu febric locator'
            ],

            [
                'name' => 'menu-febric-print-barcode-locator',
                'display_name' => 'menu febric print barcode locator',
                'description' => 'menu febric print barcode locator'
            ],

            [
                'name' => 'menu-accessories-locator',
                'display_name' => 'menu accessories locator',
                'description' => 'menu accessories locator'
            ],

            [
                'name' => 'menu-accessories-print-barcode-locator',
                'display_name' => 'menu accessories print barcode locator',
                'description' => 'menu accessories print barcode locator'
            ],

            // report 
            [
                'name' => 'menu-report-receivement',
                'display_name' => 'menu report receivement',
                'description' => 'menu report receivement'
            ],

            [
                'name' => 'menu-report-movement',
                'display_name' => 'menu report movement',
                'description' => 'menu report movement'
            ],

            [
                'name' => 'menu-report-stock',
                'display_name' => 'menu report stock',
                'description' => 'menu report stock'
            ],

            [
                'name' => 'menu-report-other-material',
                'display_name' => 'menu report other material',
                'description' => 'menu report other material'
            ],

            [
                'name' => 'menu-report-check-material',
                'display_name' => 'menu report check material',
                'description' => 'menu report check material'
            ],

            //area
            [
                'name' => 'menu-area',
                'display_name' => 'menu area',
                'description' => 'menu list area'
            ],
           

            //destination
            [
                'name' => 'menu-destination',
                'display_name' => 'menu destination',
                'description' => 'menu destination'
            ],
           
             //criteria
            [
                'name' => 'menu-criteria',
                'display_name' => 'menu criteria',
                'description' => 'menu criteria'
            ],
            
            //allocation
            [
                'name' => 'menu-allocation-accessories',
                'display_name' => 'menu allocation accessories',
                'description' => 'menu allocation accessories'
            ],
            
            [
                'name' => 'menu-approve-allocation-accessories',
                'display_name' => 'menu approve allocation accessories',
                'description' => 'menu approve allocation accessories'
            ],
        ];

        foreach ($permissions as $key => $permission) {
            Permission::create([
                'name' => $permission['name'],
                'display_name' => $permission['display_name'],
                'description' => $permission['description']
            ]);
        }
    }
}
