<?php use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    public function run()
    {
        $array = [
            [
                'name' => 'Button',
                'code' => 'BU',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Button',
                'code' => 'BU',
                'uom' => 'SET',
                'is_prepared' => false
            ],
            [
                'name' => 'Carton',
                'code' => 'CT',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Clip',
                'code' => 'CP',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Decorative Heat Transfer Label',
                'code' => 'DH',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Decorative Heat Transfer Label',
                'code' => 'DH',
                'uom' => 'YDS',
                'is_prepared' => false
            ],
            [
                'name' => 'Drawcord',
                'code' => 'DC',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Drawcord',
                'code' => 'DC',
                'uom' => 'CM',
                'is_prepared' => true
            ],
            [
                'name' => 'Drawstring',
                'code' => 'DS',
                'uom' => 'CM',
                'is_prepared' => true
            ],
            [
                'name' => 'Drawstring',
                'code' => 'DS',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Elastic',
                'code' => 'EL',
                'uom' => 'CM',
                'is_prepared' => true
            ],
            [
                'name' => 'Elastic',
                'code' => 'EL',
                'uom' => 'YDS',
                'is_prepared' => true
            ],
            [
                'name' => 'Eyelets',
                'code' => 'EY',
                'uom' => 'SET',
                'is_prepared' => false
            ],
            [
                'name' => 'Fabric',
                'code' => 'FB',
                'uom' => 'YDS',
                'is_prepared' => true
            ],
            [
                'name' => 'Fabric',
                'code' => 'FB',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Fabric',
                'code' => 'FB',
                'uom' => 'M',
                'is_prepared' => true
            ],
            [
                'name' => 'Fabric Lokal',
                'code' => 'FBL',
                'uom' => 'Each',
                'is_prepared' => true
            ],
            [
                'name' => 'Fabric Lokal',
                'code' => 'FBL',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Flag Label',
                'code' => 'FL',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Hang Tag',
                'code' => 'HT',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Hanger',
                'code' => 'HG',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Heat Transfer',
                'code' => 'HE',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Heat Transfer',
                'code' => 'HE',
                'uom' => 'SET',
                'is_prepared' => false
            ],
            [
                'name' => 'Heat Transfer',
                'code' => 'HE',
                'uom' => 'PRS',
                'is_prepared' => false
            ],
            [
                'name' => 'Hook & Loop',
                'code' => 'HL',
                'uom' => 'CM',
                'is_prepared' => null
            ],
            [
                'name' => 'Hook & Loop',
                'code' => 'HL',
                'uom' => 'PCS',
                'is_prepared' => null
            ],
            [
                'name' => 'Inner Box',
                'code' => 'IB',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Kertas Minyak',
                'code' => 'KM',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Label',
                'code' => 'LB',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Label',
                'code' => 'LB',
                'uom' => 'KIT',
                'is_prepared' => false
            ],
            [
                'name' => 'Paper',
                'code' => 'PP',
                'uom' => 'PCS',
                'is_prepared' => true
            ],
            [
                'name' => 'Patch',
                'code' => 'PA',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Polybag',
                'code' => 'PB',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Puller',
                'code' => 'PL',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Snap',
                'code' => 'SN',
                'uom' => 'SET',
                'is_prepared' => false
            ],
            [
                'name' => 'Snap',
                'code' => 'SN',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Stopper',
                'code' => 'SP',
                'uom' => 'SET',
                'is_prepared' => false
            ],
            [
                'name' => 'Stopper',
                'code' => 'SP',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Tagpin',
                'code' => 'TA',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Tape',
                'code' => 'TP',
                'uom' => 'CM',
                'is_prepared' => true
            ],
            [
                'name' => 'Tape',
                'code' => 'TP',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Thread',
                'code' => 'TH',
                'uom' => 'M',
                'is_prepared' => false
            ],
            [
                'name' => 'Thread',
                'code' => 'TH',
                'uom' => 'CNS',
                'is_prepared' => false
            ],
            [
                'name' => 'Tissue Paper',
                'code' => 'TI',
                'uom' => 'PCS',
                'is_prepared' => false
            ],
            [
                'name' => 'Zipper',
                'code' => 'ZP',
                'uom' => 'PCS',
                'is_prepared' => false
            ]
        ];

        foreach ($array as $key => $value) {
            Category::create([
                'name' => $value['name'],
                'uom' => $value['uom'],
                'code' => $value['code'],
                'is_prepared' => $value['is_prepared']
            ]);
        }
    }
}
