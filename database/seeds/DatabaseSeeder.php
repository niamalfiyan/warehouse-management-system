<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
   public function run()
    {
        $tables = [
            'locators',
            'areas',
            'criterias',
            'material_requirements',
            'categories'
        ];

    	Model::unguard();
        DB::statement("TRUNCATE permissions RESTART IDENTITY CASCADE;");
        DB::statement("TRUNCATE roles RESTART IDENTITY CASCADE;");
        DB::statement("TRUNCATE users RESTART IDENTITY CASCADE;");
        
        foreach ($tables as $key => $table) {
            DB::statement("TRUNCATE " . $table . " CASCADE ;");
        }

        $this->call(PermissionSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(AreaSeeder::class);
        $this->call(LocatorSeeder::class);
        $this->call(CriteriaSeeder::class);
        $this->call(CategorySeeder::class);
       

       /* foreach ($tables as $key => $table) {
           
        }*/
        //DB::statement("SELECT SETVAL('users_id_seq'::regclass, 1)"); 
        //static::ResetAutoincrement('users');
        Model::reguard();
    }

    public static function ResetAutoincrement($table) 
    { 
        $max = 0; 
        
        return DB::statement("SELECT SETVAL('{$table}_id_seq'::regclass, {$max})"); 
    }
}
