<?php

use Illuminate\Database\Seeder;
use App\Models\Line;

class LineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $line = Line::Create([
            'code' => '3',
            'name' => 'LINE 3',
            'user_id' => '1'
        ]);
    }
}
