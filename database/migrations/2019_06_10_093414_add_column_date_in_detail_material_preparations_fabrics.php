<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDateInDetailMaterialPreparationsFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->datetime('preparation_date')->nullable();
            $table->datetime('movement_out_date')->nullable();
            $table->boolean('is_status_prepare_inserted_to_material_preparation')->default(false);
            $table->boolean('is_status_out_inserted_to_material_preparation')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
