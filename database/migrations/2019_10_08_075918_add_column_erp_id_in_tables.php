<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnErpIdInTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monitoring_receiving_fabrics', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
        });

        Schema::table('summary_stock_fabrics', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
        });

        Schema::table('summary_handover_materials', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('item_id')->nullable();
        });

        Schema::table('material_roll_handover_fabrics', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('material_planning_fabrics', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('material_movement_lines', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('material_requirements', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('detail_material_requirements', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('material_requirement_per_parts', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('purchase_items', function (Blueprint $table) {
            $table->string('item_id')->nullable();
        });

        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('item_id_book')->nullable();
            $table->string('item_id_source')->nullable();
        });

        Schema::table('allocation_items', function (Blueprint $table) {
            $table->string('item_id_book')->nullable();
            $table->string('item_id_source')->nullable();
        });

        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('item_id_book')->nullable();
            $table->string('item_id_source')->nullable();
        });

        Schema::table('detail_material_planning_fabrics', function (Blueprint $table) {
            $table->string('c_order_id')->nullable();
            $table->string('item_id_book')->nullable();
            $table->string('item_id_source')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
