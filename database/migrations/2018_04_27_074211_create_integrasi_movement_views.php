<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIntegrasiMovementViews extends Migration
{
    public function up()
    {
        /*
        DIPAKE
        DB::statement("
            CREATE OR REPLACE VIEW public.integrasi_movements AS
                SELECT material_movements.id AS material_movement_id,
                    material_movement_lines.id AS material_movement_line_id,
                    material_movements.po_buyer,
                    material_ready_preparations.document_no,
                    material_arrivals.item_id,
                    material_ready_preparations.item_code,
                    material_ready_preparations.item_desc,
                    material_ready_preparations.category,
                    from_location.name AS from_locator,
                    destination.name AS to_locator,
                    material_preparations.uom_conversion AS uom,
                    material_movement_lines.qty_movement AS qty,
                    material_movements.status,
                    material_movement_lines.created_at AS movement_date,
                    material_preparations.warehouse,
                    material_movement_lines.is_integrate,
                    material_movement_lines.integration_date
                FROM material_movement_lines
                    JOIN material_movements ON material_movements.id = material_movement_lines.material_movement_id
                    JOIN material_preparations ON material_preparations.id = material_movement_lines.material_preparation_id
                    JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
                    JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id AND material_arrivals.is_ila = false
                    JOIN material_ready_preparations ON material_ready_preparations.material_arrival_id = material_arrivals.id
                    JOIN ( SELECT areas.name,
                            locators.id,
                            locators.code
                        FROM locators
                            JOIN areas ON areas.id = locators.area_id
                        WHERE locators.is_active = true
                        GROUP BY locators.id, locators.code, areas.name) from_location ON from_location.id = material_movements.from_location
                    JOIN ( SELECT areas.name,
                            locators.id,
                            locators.code
                        FROM locators
                            JOIN areas ON areas.id = locators.area_id
                        WHERE locators.is_active = true
                        GROUP BY locators.id, locators.code, areas.name) destination ON destination.id = material_movements.to_destination
                WHERE material_movements.status::text <> 'in'::text and material_movements.status::text <> 'change'::text
                GROUP BY material_preparations.warehouse, material_movement_lines.created_at, material_movements.po_buyer, material_ready_preparations.document_no, material_arrivals.item_id, material_ready_preparations.item_code, material_ready_preparations.item_desc, material_ready_preparations.category, from_location.name, destination.name, material_preparations.uom_conversion, material_movement_lines.qty_movement, material_movements.status, material_movement_lines.id, material_movement_lines.is_integrate, material_movement_lines.integration_date, material_movements.id
                ORDER BY material_movements.po_buyer DESC, material_ready_preparations.item_desc DESC, (
                        CASE
                            WHEN material_movements.status::text = 'in'::text THEN 1
                            WHEN material_movements.status::text = 'out'::text THEN 2
                            ELSE NULL::integer
                        END);
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW integrasi_movements CASCADE');
    }
}
