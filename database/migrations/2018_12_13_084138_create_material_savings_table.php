<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialSavingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_savings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('planning_date')->nullable();
            $table->string('style')->nullable();
            $table->string('article_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('po_supplier')->nullable();
            $table->string('supplier_name')->nullable();
            $table->double('qty_saving')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_savings');
    }
}
