<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKontraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kontraks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->integer('user_id');
            $table->string('no_kontrak',30);
            $table->date('tanggal');
            $table->integer('perseroan_id');
            $table->integer('kepala_produksi_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('perseroan_id')->references('id')->on('perseroans')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('kepala_produksi_id')->references('id')->on('kepala_produksis')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kontraks');
    }
}
