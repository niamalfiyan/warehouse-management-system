<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInTemporariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temporaries', function (Blueprint $table) {
            $table->string('string_1')->nullable();
            $table->string('string_2')->nullable();
            $table->string('string_3')->nullable();
            $table->string('string_4')->nullable();
            $table->string('string_5')->nullable();

            $table->double('double_1')->nullable();
            $table->double('double_2')->nullable();
            $table->double('double_3')->nullable();
            $table->double('double_4')->nullable();
            $table->double('double_5')->nullable();

            $table->integer('integer_1')->nullable();
            $table->integer('integer_2')->nullable();
            $table->integer('integer_3')->nullable();
            $table->integer('integer_4')->nullable();
            $table->integer('integer_5')->nullable();

            $table->text('text_1')->nullable();
            $table->text('text_2')->nullable();
            $table->text('text_3')->nullable();
            $table->text('text_4')->nullable();
            $table->text('text_5')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
