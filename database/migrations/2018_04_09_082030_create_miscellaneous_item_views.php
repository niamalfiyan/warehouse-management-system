<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMiscellaneousItemViews extends Migration
{
    public function up()
    {
        /*DB::statement("
        CREATE OR REPLACE VIEW miscellaneous_item AS
            SELECT
                miscellaneous_items.id,
                material_arrivals.document_no,
                miscellaneous_items.item_id,
                miscellaneous_items.item_code,
                miscellaneous_items.item_desc,
                miscellaneous_items.category,
                miscellaneous_items.uom,
                miscellaneous_items.qty,
                miscellaneous_items.approval_status,
                criteria_id,
                criterias.name,
                miscellaneous_items.rack,
                miscellaneous_items.system_note
            FROM miscellaneous_items
                LEFT JOIN material_arrivals ON material_arrivals.id = miscellaneous_items.material_arrival_id
                LEFT JOIN criterias ON criterias.id = miscellaneous_items.criteria_id AND criterias.is_active = true
            WHERE miscellaneous_items.is_active = true
            GROUP BY miscellaneous_items.id,
                material_arrivals.document_no,
                miscellaneous_items.item_id,
                miscellaneous_items.item_code,
                miscellaneous_items.item_desc,
                miscellaneous_items.category,
                miscellaneous_items.uom,
                miscellaneous_items.qty,
                miscellaneous_items.approval_status,
                criteria_id,
                criterias.name,
                miscellaneous_items.rack,
                miscellaneous_items.system_note;
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW miscellaneous_item CASCADE');
    }
}
