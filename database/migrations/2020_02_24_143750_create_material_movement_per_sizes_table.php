x`<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialMovementPerSizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_movement_lines', function (Blueprint $table) 
        {
            $table->boolean('is_inserted_to_material_movement_per_size')->default(false);
            $table->timestamp('inserted_to_material_movement_per_size_date')->default(false);
        });

        Schema::create('material_movement_per_sizes', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_movement_line_id',36)->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_id')->nullable();
            $table->string('size')->nullable();
            $table->double('qty_per_size',15,8)->default(0);
            $table->boolean('is_integrate')->default(false);
            $table->timestamp('integration_date')->nullable();
            $table->timestamps();
            $table->foreign('material_movement_line_id')->references('id')->on('material_movement_lines')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_movement_per_sizes');
    }
}
