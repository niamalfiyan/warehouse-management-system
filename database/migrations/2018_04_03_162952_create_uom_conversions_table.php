<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUomConversionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uom_conversions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('item_id')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('item_code')->nullable();
            $table->string('category')->nullable();
            $table->string('uom_from')->nullable();
            $table->string('uom_to')->nullable();
            $table->double('dividerate',15,8)->nullable();
            $table->double('multiplyrate',15,8)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uom_conversions');
    }
}
