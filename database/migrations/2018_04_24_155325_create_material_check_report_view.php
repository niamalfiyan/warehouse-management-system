<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialCheckReportView extends Migration
{
    public function up()
    {
        //  DB::statement("
        //     CREATE OR REPLACE VIEW public.material_check_report AS
        //         SELECT to_char(material_arrivals.created_at, 'dd/mm/yyyy HH24:MI:SS'::text) AS arrival_date,
        //             material_arrivals.created_at AS _arrival_date,
        //             material_arrivals.supplier_name,
        //             material_arrivals.document_no,
        //             users.name AS inspector_name,
        //             to_char(material_checks.created_at, 'dd/mm/yyyy HH24:MI:SS'::text) AS inspection_date,
        //             material_checks.created_at AS _inspection_date,
        //             material_arrivals.item_code,
        //             material_arrivals.item_desc,
        //             material_arrivals.category,
        //             material_preparations.po_buyer,
        //             material_preparations.job_order,
        //             material_preparations.style,
        //             material_arrivals.qty_ordered,
        //             material_preparations.qty_conversion,
        //             material_checks.status,
        //             material_checks.qty_reject,
        //             material_preparations.warehouse,
        //                 CASE
        //                     WHEN material_checks.status::text = 'RELEASE'::text THEN material_preparations.qty_conversion
        //                     ELSE 0::double precision
        //                 END AS qty_release
        //         FROM material_checks
        //             LEFT JOIN material_preparations ON material_preparations.id = material_checks.material_preparation_id::bpchar
        //             LEFT JOIN detail_material_preparations ON detail_material_preparations.material_preparation_id = material_preparations.id
        //             LEFT JOIN material_arrivals ON detail_material_preparations.material_arrival_id = material_arrivals.id
        //             LEFT JOIN users ON users.id = material_checks.user_id
        //         GROUP BY material_arrivals.qty_ordered,material_arrivals.created_at, material_preparations.po_buyer, material_arrivals.supplier_name, material_arrivals.document_no, users.name, material_arrivals.item_code, material_arrivals.item_desc, material_arrivals.category, material_preparations.job_order, material_preparations.style, material_checks.status, material_preparations.qty_conversion, material_checks.qty_reject, material_preparations.warehouse, material_checks.created_at;
        // ");
    }

    public function down()
    {
         DB::statement('DROP VIEW material_check_report CASCADE');
    }
}
