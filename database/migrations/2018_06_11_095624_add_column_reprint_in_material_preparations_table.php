<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnReprintInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->datetime('reprint_date')->nullable();
            $table->integer('reprint_user_id')->unsigned()->nullable();
            $table->foreign('reprint_user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
           
        });
    }

    public function down()
    {
        $table->dropForeign('material_preparations_reprint_user_id_foreign');
        $table->dropColumn('confirm_user_id'); 
        $table->dropColumn('reprint_date'); 
    }
}
