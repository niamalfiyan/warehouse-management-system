<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRequestDeleteInAutoAllocationsTable extends Migration
{
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->boolean('is_request_delete')->default(false);
        });
    }

    public function down()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->dropColumn('is_request_delete');
        });
    }
}
