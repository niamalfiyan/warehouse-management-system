<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMrpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mrps', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('category')->nullable();
            $table->string('article_no')->nullable();
            $table->string('uom')->nullable();
            $table->string('style')->nullable();
            $table->string('job_order')->nullable();
            $table->double('qty_need',15,8)->nullable();
            $table->string('status')->nullable();
            $table->string('location')->nullable();
            $table->datetime('statistical_date')->nullable();
            $table->datetime('movement_date')->nullable();
            $table->string('user_name')->nullable();
            $table->string('warehouse')->nullable();
            $table->double('qty_in',15,8)->nullable();
            $table->string('remark')->nullable();
            $table->string('note')->nullable();
            $table->string('document_no')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mrps');
    }
}
