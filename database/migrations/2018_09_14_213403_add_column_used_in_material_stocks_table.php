<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsedInMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->datetime('last_planning_date')->nullable();
            $table->integer('last_user_planning_id')->nullable();
            $table->foreign('last_user_planning_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        

            $table->datetime('last_date_used')->nullable();
            $table->string('last_status')->nullable();
            $table->integer('last_user_used_id')->nullable();
            $table->foreign('last_user_used_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
