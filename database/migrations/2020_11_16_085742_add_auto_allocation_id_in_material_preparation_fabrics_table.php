<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAutoAllocationIdInMaterialPreparationFabricsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->char('auto_allocation_id',36)->nullable();
            $table->foreign('auto_allocation_id')->references('id')->on('auto_allocations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->dropColumn('auto_allocation_id');
        });
    }
}
