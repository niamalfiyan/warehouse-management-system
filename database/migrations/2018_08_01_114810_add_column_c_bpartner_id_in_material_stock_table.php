<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCBpartnerIdInMaterialStockTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->dropColumn('c_bpartner_id'); 
            $table->dropColumn('supplier_name'); 
        });
    }
}
