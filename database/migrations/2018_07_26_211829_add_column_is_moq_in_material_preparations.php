<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsMoqInMaterialPreparations extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
           $table->boolean('is_moq')->nullable()->default(false);
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('is_moq'); 
        });
    }
}
