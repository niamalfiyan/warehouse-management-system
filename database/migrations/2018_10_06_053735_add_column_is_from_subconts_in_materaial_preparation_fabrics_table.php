<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsFromSubcontsInMateraialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->boolean('is_from_subconts')->nullable()->default(false);
            $table->boolean('is_from_additional')->nullable()->default(false);
            $table->string('locator_from_erp_id')->nullable();
            $table->string('locator_to_erp_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
