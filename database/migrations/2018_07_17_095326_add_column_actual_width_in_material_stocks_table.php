<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnActualWidthInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->double('actual_width',15,8)->nullable();
            
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->dropColumn('actual_width'); 
            
        });
    }
}
