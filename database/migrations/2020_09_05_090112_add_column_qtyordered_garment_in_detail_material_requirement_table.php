<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnQtyorderedGarmentInDetailMaterialRequirementTable extends Migration
{
    
    public function up()
    {
        Schema::table('detail_material_requirements', function (Blueprint $table) 
        {
            $table->double('qty_ordered_garment',18,2)->nullable();

        });
    }

    public function down()
    {
        Schema::table('detail_material_requirements', function (Blueprint $table) {
            $table->dropColumn('qty_ordered_garment');
        });
    }
}
