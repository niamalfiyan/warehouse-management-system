<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsInsertedInMaterialStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->timestamp('insert_to_locator_reject_erp_date_from_inspect')->nullable()->after('insert_to_locator_reject_erp_date_from_rma');
            $table->timestamp('insert_to_locator_reject_erp_date_from_lot')->nullable()->after('insert_to_locator_reject_erp_date_from_inspect');
            $table->timestamp('insert_to_locator_reject_erp_date_from_short_roll')->nullable()->after('insert_to_locator_reject_erp_date_from_lot');

            $table->timestamp('movement_to_locator_reject_date_from_inspect')->nullable()->after('movement_to_locator_reject_date_from_rma');
            $table->timestamp('movement_to_locator_reject_date_from_lot')->nullable()->after('movement_to_locator_reject_date_from_inspect');
            $table->timestamp('movement_to_locator_reject_date_from_short_roll')->nullable()->after('movement_to_locator_reject_date_from_lot');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
