<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInStyleInDetailMaterialPlanningAndMaterialPreparattionFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_planning_fabrics', function (Blueprint $table) 
        {
            $table->string('_style')->nullable();
        });

        Schema::table('material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->string('_style')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
