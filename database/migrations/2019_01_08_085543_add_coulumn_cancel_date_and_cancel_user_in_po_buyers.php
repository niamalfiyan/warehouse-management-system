<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnCancelDateAndCancelUserInPoBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            $table->integer('cancel_user_id')->nullable();
            $table->foreign('cancel_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->datetime('cancel_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            //
        });
    }
}
