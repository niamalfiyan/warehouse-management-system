<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumAccountingApprovalDateInAllocationItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) 
        {
            $table->timestamp('mm_approval_date')->nullable();
            $table->integer('mm_user_id')->nullable();
            $table->foreign('mm_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamp('accounting_approval_date')->nullable();
            $table->integer('accounting_user_id')->nullable();
            $table->foreign('accounting_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
