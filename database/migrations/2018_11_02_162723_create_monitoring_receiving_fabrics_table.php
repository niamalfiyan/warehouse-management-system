<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonitoringReceivingFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monitoring_receiving_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->datetime('arrival_date')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('document_no')->nullable();
            $table->string('no_invoice')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('color')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->integer('total_roll')->nullable();
            $table->double('total_yard',15,8)->nullable();
            $table->string('user_receive')->nullable();
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monitoring_receiving_fabrics');
    }
}
