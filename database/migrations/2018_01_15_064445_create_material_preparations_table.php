<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPreparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_preparations', function (Blueprint $table) {
            $table->char('id',36)->primary();
            //$table->char('material_ready_preparation_id',36)->nullable()->unsigned();
            $table->string('barcode');
            $table->string('po_buyer')->nullable();
            $table->string('uom_conversion')->nullable();
            $table->double('qty_reconversion',15,8)->nullable();
            $table->double('qty_conversion',15,8)->nullable();
            $table->double('adjustment',15,8)->default(0);
            $table->string('job_order')->nullable();
            $table->string('article_no')->nullable();
            $table->string('style')->nullable();
            $table->string('qc_status')->nullable();
            $table->integer('total_carton')->nullable();
            $table->string('type_po');
            $table->boolean('is_allocation')->default(false);
            $table->integer('user_id')->unsigned();
            //$table->foreign('material_ready_preparation_id')->references('id')->on('material_ready_preparations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_preparations');
    }
}
