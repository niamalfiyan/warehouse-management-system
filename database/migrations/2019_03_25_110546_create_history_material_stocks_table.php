<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_material_stocks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36)->nullable();
            $table->date('lc_date')->nullable();
            $table->string('c_order_id')->nullable();
            $table->string('document_no')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('uom')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->double('stock_old',15,8)->nullable();
            $table->double('stock_new',15,8)->nullable();
            $table->double('operator',15,8)->nullable();
            $table->text('note')->nullable();
            $table->text('source')->nullable();
            $table->boolean('is_integrate')->default(false);
            $table->datetime('integration_date')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_material_stocks');
    }
}
