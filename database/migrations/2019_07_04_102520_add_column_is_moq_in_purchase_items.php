<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsMoqInPurchaseItems extends Migration
{
    public function up()
    {
        Schema::table('purchase_items', function (Blueprint $table) {
            $table->double('qty_order',15,8)->default(0);
            $table->double('qty_moq',15,8)->default(0);
            $table->double('_qty_moq',15,8)->default(0);
            $table->text('remark_moq')->nullable();
            $table->boolean('is_moq')->default(false);
            $table->boolean('is_stock_moq_already_created')->default(false);
        });
    }

    public function down()
    {
        //
    }
}
