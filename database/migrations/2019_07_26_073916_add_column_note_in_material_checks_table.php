<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoteInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
           $table->string('note')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('note'); 
        });
    }
}
