<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRecycleInItemsTable extends Migration
{
    public function up()
    {
        Schema::table('items', function (Blueprint $table) 
        {
            $table->boolean('recycle')->default(false);
        });
    }

    public function down()
    {
        Schema::table('items', function (Blueprint $table) 
        {
            $table->dropColumn('recycle');
        });
    }
}
