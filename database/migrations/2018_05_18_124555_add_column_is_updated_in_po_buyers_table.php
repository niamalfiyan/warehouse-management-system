<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsUpdatedInPoBuyersTable extends Migration
{
    public function up()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            $table->boolean('is_changed')->nullable()->default(false);
        });
    }

    public function down()
    {
        Schema::table('po_buyers', function (Blueprint $table) {
            $table->dropColumn('is_changed');
        });
    }
}
