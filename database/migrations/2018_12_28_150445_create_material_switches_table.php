<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialSwitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_switches', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('document_no_source')->nullable();
            $table->string('document_no_new')->nullable();
            $table->string('item_code')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('style')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->integer('type_po');
            $table->double('qty_switch',15,8)->nullable();
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_switches');
    }
}
