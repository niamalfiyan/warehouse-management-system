<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCBpartnerIdInMaterialReadyPreparationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_ready_preparations', function (Blueprint $table) {
            $table->string('c_bpartner_id')->nullable();
            $table->string('c_order_id')->nullable();
            $table->string('c_orderline_id')->nullable();
            $table->string('supplier_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
