<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtherMaterialReportView extends Migration
{
    public function up()
    {
        /*DB::statement("
          CREATE OR REPLACE VIEW public.material_other_report AS
            select 
        			null::bigint as po_detail_id,
            		document_no,
	            item_code,
            		item_desc,
            		category,
            		locators.code,
	            uom,
	            type,
	            sum(qty) as qty,
	            'BARANG ADA DI GUDANG' as warehouse_status,
	            warehouse
            from material_others
            left join locators on locators.id = locator_id
            group by 
            		document_no,
	            item_code,
            		item_desc,
            		category,
            		locators.code,
	            uom,
	            type,
	            warehouse
	        union all
            SELECT a.po_detail_id,
            		a.document_no,
                	a.item_code,
                	a.item_desc,
                	a.category,
                null as code,
               	case 
               		when uom_conversion.uom_to is null then a.uom
               		else uom_conversion.uom_to 
               	end as uom,
                'PFAO' as type,
   				case 
               		when uom_conversion.uom_to is null then a.qty_upload
               		else uom_conversion.dividerate * a.qty_upload
               	end as total,
               'BARANG BELUM ADA DI GUDANG' as warehouse_status,
               a.m_warehouse_id as warehouse
            FROM dblink('dbname=import port=5432 host=192.168.51.52 user=postgres password=Becarefulwithme'::text, '
               select
			    po_detail_id,
				documentno as document_no,
			    item as item_code,
			    desc_product as item_desc,
			    category as category,
			    uomsymbol as uom,
			    qty_upload,
				m_warehouse_id
			from po_detail
			where flag_wms = false 
			and pobuyer is null
			and type_po = 2::text
			and flag_erp = true
			and is_locked = true
			and isactive = true
            '::text) a 
            (po_detail_id bigint, document_no text, item_code text, item_desc text, category text, 
            uom text, qty_upload decimal,m_warehouse_id text)
            left join(
            		 select item_code,category,uom_to,uom_from,dividerate
   				 from uom_conversions     
            )uom_conversion on uom_conversion.item_code = a.item_code and uom_conversion.category = a.category 
            and uom_conversion.uom_from = a.uom
            
		");*/
		
		DB::statement("
			CREATE OR REPLACE VIEW public.material_other_report AS
				SELECT material_arrivals.document_no,
					material_arrivals.item_code,
					material_arrivals.item_desc,
					material_arrivals.category,
					locators.id AS locator_id,
					locators.code,
					material_arrivals.uom,
					sum(material_arrivals.qty_upload) AS qty,
					coalesce(sum(allocation.qty_booking),0)  as total_booking,
					(sum(material_arrivals.qty_upload)  -  coalesce(sum(allocation.qty_booking),0)) as curr_stock,
					material_arrivals.warehouse_id
				FROM material_arrivals
					LEFT JOIN locators ON locators.id = material_arrivals.locator_id
					left join (
						SELECT material_arrival_id,sum(qty_booking) as qty_booking
						FROM allocation_items
						GROUP by material_arrival_id 
					)allocation on allocation.material_arrival_id = material_arrivals.id
				WHERE material_arrivals.is_material_other = true 
				GROUP BY material_arrivals.document_no, locators.id, material_arrivals.item_code, material_arrivals.item_desc, material_arrivals.category, locators.code, material_arrivals.uom, material_arrivals.warehouse_id;
				--HAVING ( sum(material_arrivals.qty_upload)  -  coalesce(sum(allocation.qty_booking),0)  ) > 0;
							
            
        ");
    }

    public function down()
    {
        DB::statement('DROP VIEW material_other_report CASCADE');
    }
}
