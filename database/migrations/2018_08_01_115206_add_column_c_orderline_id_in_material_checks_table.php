<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCOrderlineIdInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->string('c_orderline_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('c_orderline_id'); 
        });
    }
}
