<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockItemsReportViews extends Migration
{
    public function up()
    {
        /*DB::statement("
            CREATE OR REPLACE VIEW public.stocks_items_report AS
                SELECT document_no,
                    supplier_name,
                    uom,
                    uom_conversion,
                    qty_upload,
                    qty_conversion,
                    stock_id,
                    CASE 
                        WHEN is_active = true THEN 'IN STOCK'
                        ELSE 'SUPPLIED'
                    end as status
                FROM stock_lines
                GROUP BY 
                    document_no,
                    supplier_name,
                    uom,
                    uom_conversion,
                    qty_upload,
                    qty_conversion,
                    is_active,
                    stock_id
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW stocks_items_report CASCADE');
    }
}
