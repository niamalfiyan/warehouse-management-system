<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDeleteReasonInMaterialPreparationFabricsTable extends Migration
{   
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
           $table->text('delete_reason')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->dropColumn('delete_reason'); 
        });
    }
}
