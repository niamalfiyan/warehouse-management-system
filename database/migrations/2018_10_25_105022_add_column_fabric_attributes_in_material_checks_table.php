<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFabricAttributesInMaterialChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
           $table->string('nomor_roll')->nullable();
           $table->string('category')->nullable();
           $table->string('c_bpartner_id')->nullable();
           $table->string('supplier_name')->nullable();
           $table->string('color')->nullable();
           $table->string('no_invoice')->nullable();
           $table->double('begin_width',15,8)->nullable();
           $table->double('middle_width',15,8)->nullable();
           $table->double('end_width',15,8)->nullable();
           $table->double('actual_width',15,8)->nullable();
           $table->double('kg',15,8)->nullable();
           $table->double('qty_on_barcode',15,8)->nullable();
           $table->double('actual_length',15,8)->nullable();
           $table->integer('point_1')->default(0);
           $table->integer('point_2')->default(0);
           $table->integer('point_3')->default(0);
           $table->integer('point_4')->default(0);
           $table->integer('total_point')->default(0);
           $table->double('percentage',15,8)->default(0);

           $table->integer('jumlah_defect_a')->default(0);
           $table->integer('jumlah_defect_b')->default(0);
           $table->integer('jumlah_defect_c')->default(0);
           $table->integer('jumlah_defect_d')->default(0);
           $table->integer('jumlah_defect_e')->default(0);
           $table->integer('jumlah_defect_f')->default(0);
           $table->integer('jumlah_defect_g')->default(0);
           $table->integer('jumlah_defect_h')->default(0);
           $table->integer('jumlah_defect_i')->default(0);
           $table->integer('jumlah_defect_j')->default(0);
           $table->integer('jumlah_defect_k')->default(0);
           $table->integer('jumlah_defect_l')->default(0);
           $table->integer('jumlah_defect_m')->default(0);
           $table->integer('jumlah_defect_n')->default(0);
           $table->integer('jumlah_defect_o')->default(0);
           $table->integer('jumlah_defect_p')->default(0);
           $table->integer('jumlah_defect_q')->default(0);
           $table->integer('jumlah_defect_r')->default(0);
           $table->integer('jumlah_defect_s')->default(0);
           $table->integer('jumlah_defect_t')->default(0);
           $table->double('total_linear_point')->default(0);
           $table->double('total_yds')->default(0);
           $table->double('total_formula_1')->default(0);
           $table->double('total_formula_2')->default(0);
           $table->string('final_result')->nullable();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
