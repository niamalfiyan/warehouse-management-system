<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovementStockHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movement_stock_histories', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_stock_id',36);
            $table->char('from_location',36);
            $table->char('to_destination',36);
            $table->string('status')->nullable();

            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('from_location')->references('id')->on('locators')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to_destination')->references('id')->on('locators')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movement_stock_histories');
    }
}
