<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMasterDataFabricTestingsTable extends Migration
{
   
    public function up()
    {
        Schema::create('master_data_fabric_testings', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('testing_classification')->nullable();
            $table->string('method')->nullable();
            $table->string('testing_name')->nullable();
            $table->string('testing_code')->nullable();
            $table->string('requirement')->nullable();
            $table->string('calculation_method')->nullable();
            $table->double('min',15,4)->nullable();
            $table->double('max',15,4)->nullable();
            $table->string('operator')->nullable();
            $table->string('detail_requirement')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    public function down()
    {
        Schema::dropIfExists('master_data_fabric_testings');
    }
}
