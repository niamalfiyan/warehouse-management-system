<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCuttingInstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cutting_instructions', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('planning_date')->nullable();
            $table->text('po_buyer')->nullable();
            $table->text('style')->nullable();
            $table->text('article_no')->nullable();
            $table->text('item_code')->nullable();
            $table->text('uom')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->double('qty_need',15,8)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cutting_instructions');
    }
}
