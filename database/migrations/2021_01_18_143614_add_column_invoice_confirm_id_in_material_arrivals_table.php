<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInvoiceConfirmIdInMaterialArrivalsTable extends Migration
{
    public function up()
    {
        Schema::table('material_arrivals', function (Blueprint $table) 
        {
            $table->string('invoice_confirm_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_arrivals', function (Blueprint $table) 
        {
            $table->dropColumn('invoice_confirm_id');
        });
    }
}
