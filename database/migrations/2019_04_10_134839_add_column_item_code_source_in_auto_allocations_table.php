<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnItemCodeSourceInAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->string('item_code_source')->nullable();
        });

        Schema::table('history_auto_allocations', function (Blueprint $table) {
            $table->string('item_code_source_old')->nullable();
            $table->string('item_code_source_new')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
