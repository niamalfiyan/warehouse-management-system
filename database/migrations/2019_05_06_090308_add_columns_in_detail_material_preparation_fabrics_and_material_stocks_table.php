<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInDetailMaterialPreparationFabricsAndMaterialStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->string('actual_lot')->nullable();
        });

        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->boolean('is_need_to_be_change')->default(false);
        });

        Schema::table('material_stocks', function (Blueprint $table) {
            $table->string('no_packing_list')->nullable();
        });

        Schema::table('monitoring_receiving_fabrics', function (Blueprint $table) {
            $table->string('no_packing_list')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
