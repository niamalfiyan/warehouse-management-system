<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoSuratJalanInSummaryHandoverMaterialsTable extends Migration
{
    public function up()
    {
        Schema::table('summary_handover_materials', function (Blueprint $table) {
           $table->string('no_surat_jalan')->nullable();
           $table->date('tanggal_kirim')->nullable();
        });
    }

    public function down()
    {
        Schema::table('summary_handover_materials', function (Blueprint $table) {
            $table->dropColumn('no_surat_jalan');
            $table->dropColumn('tanggal_kirim');
        });
    }
}
