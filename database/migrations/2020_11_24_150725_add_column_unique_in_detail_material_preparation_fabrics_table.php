<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUniqueInDetailMaterialPreparationFabricsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->unique(['material_preparation_fabric_id','material_stock_id','barcode','reserved_qty']);
        });
    }

    public function down()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->dropunique(['material_preparation_fabric_id','material_stock_id','barcode','reserved_qty']);
        });
    }
}
