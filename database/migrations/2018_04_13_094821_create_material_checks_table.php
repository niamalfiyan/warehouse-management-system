<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::create('material_checks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_detail_id')->nullable()->unsigned();
            $table->string('material_preparation_id')->unsigned();
            $table->string('uom_po')->nullable()->unsigned();
            $table->string('uom_conversion')->nullable()->unsigned();
            $table->string('document_no')->nullable()->unsigned();
            $table->string('item_code')->nullable()->unsigned();
            $table->string('item_desc')->nullable()->unsigned();
            $table->string('po_buyer')->nullable()->unsigned();
            $table->string('barcode_preparation')->nullable();
            $table->string('status')->nullable()->unsigned();
            $table->double('qty_reject',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_checks');
    }
}
