<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInDetailMaterialArrivals extends Migration
{
    public function up()
    {
        Schema::table('detail_material_arrivals', function (Blueprint $table) {
            $table->string('uom')->nullable();
            $table->string('uom_conversion')->nullable();
        });
    }

    public function down()
    {
        Schema::table('detail_material_arrivals', function (Blueprint $table) {
            $table->dropColumn('uom');
            $table->dropColumn('uom_conversion');
        });
    }
}
