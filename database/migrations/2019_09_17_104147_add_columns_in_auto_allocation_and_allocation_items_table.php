<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInAutoAllocationAndAllocationItemsTable extends Migration
{
    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->text('remark_update')->nullable();
        });

        Schema::table('allocation_items', function (Blueprint $table) {
            $table->string('item_code_source')->nullable();
        });

        Schema::table('detail_material_planning_fabrics', function (Blueprint $table) {
            $table->string('item_code_source')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
