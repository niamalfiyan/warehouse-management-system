<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCaseInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->string('case')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->dropColumn('case');
        });
    }
}
