<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInWmsOldsTable extends Migration
{
    public function up()
    {
        Schema::table('wms_olds', function (Blueprint $table) {
            $table->string('user_name')->nullable();
            $table->string('item_code')->nullable();
            $table->timestamp('checkout_date')->nullable();
        });
    }

    public function down()
    {
        Schema::table('wms_olds', function (Blueprint $table) {
            $table->dropColumn('user_name');
            $table->dropColumn('item_code');
            $table->dropColumn('checkout_date');
        });
    }
}
