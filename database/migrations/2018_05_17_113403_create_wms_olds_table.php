<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWmsOldsTable extends Migration
{
    public function up()
    {
        Schema::create('wms_olds', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_buyer')->nullable();
            $table->string('status')->nullable();
            $table->string('locator')->nullable();
            $table->string('style')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('wms_olds');
    }
}
