<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_requirements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_buyer');
            $table->string('item_code');
            $table->string('item_desc');
            $table->string('job_order');
            $table->string('style');
            $table->string('uom');
            $table->string('category');
            $table->date('lc_date')->nullable();
            $table->double('qty_required',15,8)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_requirements');
    }
}
