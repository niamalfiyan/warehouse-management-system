<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrimCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trim_cards', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->Increments('barcode');
            $table->string('style')->nullable();
            $table->string('job_order')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('article_no')->nullable();
            $table->string('season')->nullable();
            $table->boolean('is_active')->default(true);
            $table->integer('user_id')->unsigned();
            $table->string('remark')->nullable();
            $table->date('lc_date')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
            $table->dropPrimary('trim_cards_barcode_primary');
            $table->unique(array('id'));
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trim_cards');
    }
}
