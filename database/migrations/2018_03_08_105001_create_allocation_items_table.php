<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllocationItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allocation_items', function (Blueprint $table) {
            $table->char('id',36)->primary();
            //$table->char('allocation_id',36)->nullable();
            $table->char('material_arrival_id',36)->unsigned()->nullable();
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('job')->nullable();
            $table->string('style')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('warehouse')->nullable();
            $table->double('qty_booking',15,8)->nullable();
            $table->string('confirm_by_warehouse')->nullable();
            $table->date('is_from_allocation_fabric')->nullable();
            $table->boolean('is_complate_erp')->nullable();
            $table->date('confirm_date')->nullable();
            $table->integer('user_id')->unsigned();

            $table->timestamps();
            $table->datetime('deleted_at')->nullable();

            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            //$table->foreign('allocation_id')->references('id')->on('allocations')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allocation_items');
    }
}
