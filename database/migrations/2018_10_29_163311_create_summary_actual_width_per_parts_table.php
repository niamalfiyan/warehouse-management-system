<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryActualWidthPerPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_actual_width_per_parts', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_planning_fabric_id',36);
            $table->string('uom')->nullable();
            $table->string('part_no')->nullable();
            $table->string('is_piping')->nullable();
            $table->string('actual_width')->nullable();
            $table->double('reserved_qty',15,8)->nullable();
            $table->foreign('material_planning_fabric_id')->references('id')->on('material_planning_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_actual_width_per_parts');
    }
}
