<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnForCuttingReceivingInDetailMaterialPreparationFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->integer('user_receive_cutting_id')->nullable();
            $table->foreign('user_receive_cutting_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->datetime('date_receive_cutting')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            //
        });
    }
}
