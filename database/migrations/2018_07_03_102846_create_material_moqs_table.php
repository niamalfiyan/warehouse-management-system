<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialMoqsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_moqs', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('document_no')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('category')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('warehouse')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_po',15,8)->nullable();
            $table->double('qty_need',15,8)->nullable();
            $table->double('qty_book',15,8)->nullable();
            $table->date('eta')->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_moqs');
    }
}
