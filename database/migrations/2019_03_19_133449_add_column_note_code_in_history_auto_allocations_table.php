<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNoteCodeInHistoryAutoAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_auto_allocations', function (Blueprint $table) {
            $table->char('note_code',2)->nullable();
        });
        
        Schema::table('auto_allocations', function (Blueprint $table) {
            $table->boolean('is_upload_manual')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
