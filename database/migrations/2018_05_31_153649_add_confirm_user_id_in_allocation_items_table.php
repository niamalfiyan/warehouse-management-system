<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConfirmUserIdInAllocationItemsTable extends Migration
{
    public function up()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->integer('confirm_user_id')->nullable();
            $table->foreign('confirm_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('allocation_items', function (Blueprint $table) {
            $table->dropForeign('allocation_items_confirm_user_id_foreign');
            $table->dropColumn('confirm_user_id'); 
        });
    }
}
