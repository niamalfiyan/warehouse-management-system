<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypePoInMaterialSwitchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_switches', function (Blueprint $table) {
            $table->string('item_id')->nullable();
            $table->string('c_order_id_source')->nullable();
            $table->string('c_bpartner_id_source')->nullable();
            $table->string('supplier_name_source')->nullable();
            $table->string('c_order_id_new')->nullable();
            $table->string('c_bpartner_id_new')->nullable();
            $table->string('supplier_name_new')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_need_source_outstanding')->default(0);
            $table->double('qty_need_source_switched')->default(0);
            $table->double('qty_need_new_outstanding')->default(0);
            $table->double('qty_need_new_switched')->default(0);
        });

        Schema::table('material_preparations', function (Blueprint $table) {
            $table->boolean('is_need_to_be_switch')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
