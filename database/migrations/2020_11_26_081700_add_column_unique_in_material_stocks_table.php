<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUniqueInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->unique(['barcode_supplier','locator_id', 'po_buyer', 'type_stock', 'summary_stock_fabric_id','c_order_id','item_id','stock', 'source', 'po_detail_id', 'warehouse_id', 'is_master_roll', 'nomor_roll', 'is_material_others', 'is_closing_balance', 'uom', 'is_stock_on_the_fly', 'case']);
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) 
        {
            $table->dropunique(['barcode_supplier','locator_id', 'po_buyer', 'type_stock', 'summary_stock_fabric_id','c_order_id','item_id','stock', 'source', 'po_detail_id', 'warehouse_id', 'is_master_roll', 'nomor_roll', 'is_material_others', 'is_closing_balance', 'uom', 'is_stock_on_the_fly', 'case']);
        });
    }
}
