<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockReportView extends Migration
{
    public function up()
    {
//         DB::statement("
//            CREATE OR REPLACE VIEW public.stocks_report as
// SELECT
//  material_arrivals.item_code,
//  material_arrivals.item_desc,
//  material_arrivals.category,
//  uom,
// CASE
//  WHEN CONVERSION.uom_to IS NULL THEN
//  uom ELSE CONVERSION.uom_to
//  END AS uom_conversion,
//  COALESCE ( item_all.total_all * COALESCE ( CONVERSION.dividerate, 1 )  ) AS total_all,
//  COALESCE ( item_rcv.total_rcv * COALESCE ( CONVERSION.dividerate, 1 )  ) AS total_receive,
//  COALESCE ( item_out.total_out, 0 ) AS total_out,
//  COALESCE ( item_free.total_free, 0 ) AS total_free,
//  material_arrivals.warehouse_id
// FROM
//  material_arrivals
//  LEFT JOIN (
// SELECT
//  item_code,
//  SUM ( qty_movement ) AS total_out
// FROM
//  material_movement_lines
// WHERE
//  material_movement_lines.material_movement_id IN ( SELECT ID FROM material_movements WHERE status = 'out' )
// GROUP BY
//  item_code
//  ) item_out ON item_out.item_code = material_arrivals.item_code
//  LEFT JOIN ( SELECT item_code, SUM ( qty_upload ) AS total_free FROM material_arrivals WHERE is_material_other = TRUE GROUP BY item_code ) item_free ON item_free.item_code = material_arrivals.item_code
//  LEFT JOIN ( SELECT item_code, SUM ( qty_upload ) AS total_all FROM material_arrivals GROUP BY item_code ) item_all ON item_all.item_code = material_arrivals.item_code
//  LEFT JOIN ( SELECT item_code, SUM ( qty_upload ) AS total_rcv FROM material_arrivals WHERE is_material_other = FALSE AND is_general_item = FALSE GROUP BY item_code ) item_rcv ON item_rcv.item_code = material_arrivals.item_code
//  LEFT JOIN ( SELECT item_code, uom_to, dividerate FROM uom_conversions GROUP BY item_code, uom_to, dividerate ) CONVERSION ON CONVERSION.item_code = material_arrivals.item_code
// GROUP BY
//  material_arrivals.item_code,
//  material_arrivals.item_desc,
//  material_arrivals.warehouse_id,
//  item_out.total_out,
//  item_free.total_free,
//  CONVERSION.uom_to,
//  uom,
//  CONVERSION.dividerate,
//  material_arrivals.category,
//  item_rcv.total_rcv,
//  item_all.total_all;
//         ");
    }

    public function down()
    {
        DB::statement('DROP VIEW stocks_report CASCADE');
    }
}
