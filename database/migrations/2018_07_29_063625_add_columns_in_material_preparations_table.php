<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->string('last_status_movement')->nullable();
            $table->datetime('last_movement_date')->nullable();

            $table->char('last_locator_id',36)->nullable()->unsigned();
            $table->foreign('last_locator_id')->references('id')->on('locators')->onDelete('cascade')->onUpdate('cascade');
            
            $table->integer('last_user_movement_id')->nullable();
            $table->foreign('last_user_movement_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('last_status_movement'); 
            $table->dropForeign('material_preparations_last_locator_id_foreign');
            $table->dropColumn('last_locator_id'); 
            $table->dropForeign('material_preparations_last_user_movement_id_foreign');
            $table->dropColumn('last_user_movement_id'); 
        });
    }
}
