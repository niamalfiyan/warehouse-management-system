<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnFocRmaInMaterialArrivalsTable extends Migration
{
    public function up()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->double('foc_available_rma',15,8)->default(0)->nullable();
            $table->double('foc_reserved_rma',15,8)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->dropColumn('foc_available_rma'); 
            $table->dropColumn('foc_reserved_rma'); 
        });
    }
}
