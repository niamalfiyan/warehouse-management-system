<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDetailRequirementInDetailMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->string('item_code')->nullable();
            $table->string('color')->nullable();
            $table->date('statistical_date')->nullable();
            $table->date('lc_date')->nullable();
            $table->string('part_no')->nullable();
            $table->string('job_order')->nullable();
            $table->boolean('is_piping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
