<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_preparation_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->date('planning_date')->nullable();
            $table->string('uom')->nullable();
            $table->double('total_reserved_qty',15,8)->nullable();
            $table->string('warehouse_id')->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->boolean('is_from_allocation')->nullable()->default(false);
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->double('total_qty_rilex',15,8)->nullable();
            $table->double('total_qty_outstanding',15,8)->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_preparation_fabrics');
    }
}
