<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDocumentNoInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->string('document_no')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->dropColumn('document_no');

        });
    }
}
