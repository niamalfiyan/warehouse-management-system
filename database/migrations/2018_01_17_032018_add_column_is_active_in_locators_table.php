<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsActiveInLocatorsTable extends Migration
{
    public function up()
    {
        Schema::table('locators', function (Blueprint $table) {
            $table->boolean('is_active')->default(true);
        });
    }

    public function down()
    {
        Schema::table('locators', function (Blueprint $table) {
            $table->dropColumn('is_active');
        });
    }
}
