<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsRemoveFromDashboardInAutoAllocationsTable extends Migration
{

    public function up()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->boolean('is_remove_from_dashboard')->default(false);
            $table->string('remark_remove')->nullable();
            $table->integer('user_id_remove')->nullable();
            $table->foreign('user_id_remove')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    public function down()
    {
        Schema::table('auto_allocations', function (Blueprint $table) 
        {
            $table->dropColumn('is_remove_from_dashboard');
            $table->string('remark_remove')->nullable();
            $table->integer('user_id_remove')->nullable();
        });
    }
}
