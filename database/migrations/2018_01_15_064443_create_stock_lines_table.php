<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::create('stock_lines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('stock_id',36);
            $table->char('material_arrival_id',36);
            $table->enum('status',['allocated','free']);
            $table->string('uom_conversion')->nullable();
            $table->string('po_buyer')->nullable();
            $table->double('qty_reconversion',15,8)->nullable();
            $table->double('qty_conversion',15,8)->nullable();
           // $table->double('foc',15,8)->default(0)->nullable();
            $table->string('job_order')->nullable();
            $table->string('article_no')->nullable();
            $table->string('style')->nullable();
            $table->integer('user_id')->unsigned();
           
            $table->foreign('material_arrival_id')->references('id')->on('material_arrivals')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('stock_id')->references('id')->on('stocks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->timestamps();
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('stock_lines');
    }
}
