<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInvoiceConfirmIdInItemPackingListsTable extends Migration
{
    public function up()
    {
        Schema::table('item_packing_lists', function (Blueprint $table) 
        {
            $table->string('invoice_confirm_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('item_packing_lists', function (Blueprint $table) 
        {
            $table->dropColumn('invoice_confirm_id');
        });
    }
}
