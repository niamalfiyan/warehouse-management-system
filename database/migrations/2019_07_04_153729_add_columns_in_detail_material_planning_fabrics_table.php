<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInDetailMaterialPlanningFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_material_planning_fabrics', function (Blueprint $table) {
            $table->string('po_buyer')->nullable();
            $table->string('style')->nullable();
            $table->string('article_no')->nullable();
            $table->boolean('is_piping')->default(false);
            $table->string('item_code')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->date('planning_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
