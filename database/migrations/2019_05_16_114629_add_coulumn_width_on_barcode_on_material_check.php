<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoulumnWidthOnBarcodeOnMaterialCheck extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->double('width_on_barcode',15,8)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('width_on_barcode');
        });
    }
}
