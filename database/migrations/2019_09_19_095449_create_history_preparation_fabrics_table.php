<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_preparation_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_preparation_fabric_id',36)->nullable();
            $table->text('remark')->nullable();
            $table->double('qty_before',15,8)->default(0);
            $table->double('qty_after',15,8)->default(0);
            $table->timestamps();

            $table->foreign('material_preparation_fabric_id')->references('id')->on('material_preparation_fabrics')->onUpdate('cascade')->onDelete('cascade');
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_preparation_fabrics');
    }
}
