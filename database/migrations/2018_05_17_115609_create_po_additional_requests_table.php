<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoAdditionalRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_additional_requests', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('document_no')->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('qty')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('po_additional_requests');
    }
}
