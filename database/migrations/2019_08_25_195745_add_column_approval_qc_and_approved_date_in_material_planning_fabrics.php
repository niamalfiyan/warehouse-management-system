<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnApprovalQcAndApprovedDateInMaterialPlanningFabrics extends Migration
{
    
    public function up()
    {
        Schema::table('material_planning_fabrics', function (Blueprint $table) {
            $table->string('approval_qc')->nullable();
            $table->datetime('approved_date')->nullable();
            $table->datetime('request_approval_date')->nullable();
         });
    }

    
    public function down()
    {
        $table->dropColumn('approval_qc');
        $table->dropColumn('approved_date');
        $table->dropColumn('request_approval_date');
    }
}
