<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialStockPerLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_stock_per_lots', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_order_id')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('item_id')->nullable();
            $table->string('warehouse_id')->nullable();
            $table->string('no_invoice')->nullable();
            $table->string('no_packing_list')->nullable();
            $table->string('document_no')->nullable();
            $table->string('supplier_code')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('actual_lot')->nullable();
            $table->string('type_stock_erp_code')->nullable();
            $table->string('type_stock')->nullable();
            $table->string('uom')->nullable();
            $table->double('qty_stock',15,8)->default(0);
            $table->double('qty_reserved',15,8)->default(0);
            $table->double('qty_available',15,8)->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::table('material_stocks', function (Blueprint $table) {
            $table->char('material_stock_per_lot_id',36)->nullable();
            $table->foreign('material_stock_per_lot_id')->references('id')->on('material_stock_per_lots')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('allocation_items', function (Blueprint $table) {
            $table->char('material_stock_per_lot_id',36)->nullable();
            $table->foreign('material_stock_per_lot_id')->references('id')->on('material_stock_per_lots')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('history_material_stocks', function (Blueprint $table) 
        {
            $table->string('item_id')->nullable();
            $table->string('actual_lot')->nullable();
            $table->char('material_stock_per_lot_id',36)->nullable();
            $table->foreign('material_stock_per_lot_id')->references('id')->on('material_stock_per_lots')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_stock_per_lots');
    }
}
