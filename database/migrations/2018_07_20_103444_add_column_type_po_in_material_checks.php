<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTypePoInMaterialChecks extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->string('type_po')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('type_po');

        });
    }
}
