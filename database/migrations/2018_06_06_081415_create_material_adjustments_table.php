<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialAdjustmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_adjustments', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('barcode');
            $table->string('item_code')->nullable();
            $table->string('item_desc')->nullable();
            $table->string('category')->nullable();
            $table->string('material_preparation_id')->unsigned();
            $table->string('po_buyer')->nullable();
            $table->string('uom_conversion')->nullable();
            $table->double('qty_before_adjustment',15,8)->nullable();
            $table->double('qty_adjustment',15,8)->nullable();
            $table->double('qty_after_adjustment',15,8)->nullable();
            $table->string('type_po')->nullable();
            $table->string('warehouse')->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_adjustments');
    }
}
