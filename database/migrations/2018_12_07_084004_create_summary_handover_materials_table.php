<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSummaryHandoverMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('summary_handover_materials', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('c_bpartner_id')->nullable();
            $table->string('document_no')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('item_code')->nullable();
            $table->string('uom')->nullable();
            $table->double('total_qty_handover',15,8)->nullable();
            $table->double('total_qty_outstanding',15,8)->nullable();
            $table->double('total_qty_supply',15,8)->nullable();
            $table->datetime('complete_date')->nullable();
            $table->string('warehouse_id')->nullable();
           
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('summary_handover_materials');
    }
}
