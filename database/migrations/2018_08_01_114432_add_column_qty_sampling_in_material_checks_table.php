<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnQtySamplingInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->double('qty_sample',15,8)->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('qty_sample'); 
        });
    }
}
