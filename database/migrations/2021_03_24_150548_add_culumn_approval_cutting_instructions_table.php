<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCulumnApprovalCuttingInstructionsTable extends Migration
{
    public function up()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->string('ci_approval_status')->nullable();
            $table->timestamp('ci_approval_date')->nullable();
            $table->string('ci_approval_user_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('detail_material_preparation_fabrics', function (Blueprint $table) 
        {
            $table->dropColumn('ci_approval_status');
            $table->dropColumn('ci_approval_date');
            $table->dropColumn('ci_approval_user_id');
        });
    }
}
