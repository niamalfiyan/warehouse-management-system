<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRmaInMaterialArrivals extends Migration
{
    public function up()
    {
        Schema::table('material_arrivals', function (Blueprint $table) {
            $table->double('qty_available_rma',15,8)->nullable();
            $table->double('qty_reserved_rma',15,8)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('material_arrivals', function (Blueprint $table) {
            $table->dropColumn('qty_available_rma'); 
            $table->dropColumn('qty_reserved_rma'); 
        });
    }
}
