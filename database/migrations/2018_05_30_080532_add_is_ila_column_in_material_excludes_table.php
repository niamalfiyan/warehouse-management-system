<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsIlaColumnInMaterialExcludesTable extends Migration
{
   public function up()
    {
        Schema::table('material_excludes', function (Blueprint $table) {
            $table->boolean('is_ila')->nullable()->default(false);
            $table->boolean('is_from_buyer')->nullable()->default(false);
        });
    }

    public function down()
    {
         Schema::table('material_excludes', function (Blueprint $table) {
            $table->dropColumn('is_ila');
            $table->dropColumn('is_from_buyer');
        });
    }
}
