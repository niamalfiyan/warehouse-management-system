<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBarcodeInLocatorsTable extends Migration
{
    public function up()
    {
        Schema::table('locators', function (Blueprint $table) {
            $table->string('barcode')->nullable();
        });
    }

    public function down()
    {
         Schema::table('locators', function (Blueprint $table) {
            $table->dropColumn('qty_in');
            
        });
    }
}
