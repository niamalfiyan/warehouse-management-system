<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPersentageMetalDetectorInMaterialPreparationsTable extends Migration
{
    
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->double('persentage_metal_detector',5,2)->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->dropColumn('persentage_metal_detector');
        });
    }
}
