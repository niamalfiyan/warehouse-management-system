<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSummaryColumnInMaterialPreparationFabrics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->date('eta_date')->nullable();
            $table->string('document_no')->nullable();
            $table->string('item_code')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('supplier_name')->nullable();
            $table->string('no_invoice')->nullable();
            $table->double('total_qty_rilex',15,8)->default(0)->nullable();
            $table->double('total_qty_outstanding',15,8)->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
