<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMaterialOthersTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->double('reserved_qty',15,8)->nullable();
            $table->boolean('is_from_material_other')->nullable()->default(false);
            $table->boolean('is_general_item')->nullable()->default(false);
            
            $table->integer('approval_user_id')->nullable();
            $table->date('approval_date')->nullable();
            $table->foreign('approval_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            
            $table->integer('reject_user_id')->nullable();
            $table->date('reject_date')->nullable();
            $table->foreign('reject_user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->dropColumn('available_qty'); 
            $table->dropColumn('is_from_material_other'); 
            $table->dropColumn('is_general_item'); 
            $table->dropColumn('reject_date'); 
            $table->dropColumn('approval_date'); 

            $table->dropForeign('material_stocks_approval_user_id_foreign');
            $table->dropColumn('approval_user_id'); 

            $table->dropForeign('material_stocks_reject_user_id_foreign');
            $table->dropColumn('reject_user_id'); 
        });
    }
}
