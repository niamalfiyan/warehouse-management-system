<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRilexDateInMaterialPreparationFabricsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->date('rilex_date')->nullable();
            $table->integer('user_rilex_id')->nullable()->unsigned();
            $table->foreign('user_rilex_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
       
        });
    }

    public function down()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->dropColumn('rilex_date');

            $table->dropForeign('users_user_rilex_id_foreign');
            $table->dropColumn('user_rilex_id'); 
        });
    }
}
