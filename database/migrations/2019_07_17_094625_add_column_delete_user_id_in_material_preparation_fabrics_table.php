<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDeleteUserIdInMaterialPreparationFabricsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparation_fabrics', function (Blueprint $table) {
            $table->integer('delete_user_id')->nullable();
            $table->foreign('delete_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    public function down()
    {
        
    }
}
