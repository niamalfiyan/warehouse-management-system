<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMappingStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mapping_stocks', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('type_stock_erp_code')->nullable();
            $table->string('type_stock')->nullable();
            $table->string('document_number')->nullable();
            $table->timestamps();
        });

        Schema::table('material_stocks', function (Blueprint $table) {
            $table->string('type_stock_erp_code')->nullable();
            $table->string('type_stock')->nullable();
        });

        Schema::table('history_material_stocks', function (Blueprint $table) {
            $table->string('type_stock_erp_code')->nullable();
            $table->string('type_stock')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mapping_stocks');
    }
}
