<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialPreparationFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_preparation_fabrics', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_preparation_fabric_id',36);
            $table->char('material_planning_fabric_id',36);
            $table->string('po_buyer')->nullable();
            $table->string('style')->nullable();
            $table->string('uom')->nullable();
            $table->double('reserved_qty',15,8)->nullable();
            $table->integer('user_id')->unsigned();
            $table->datetime('deleted_at')->nullable();
            $table->string('remark')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_preparation_fabric_id')->references('id')->on('material_preparation_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_planning_fabric_id')->references('id')->on('material_planning_fabrics')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_preparation_fabrics');
    }
}
