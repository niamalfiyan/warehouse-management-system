<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStoUserIdInMaterialPreparationsTable extends Migration
{
    
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) {
            $table->integer('sto_user_id')->nullable();
            $table->foreign('sto_user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
