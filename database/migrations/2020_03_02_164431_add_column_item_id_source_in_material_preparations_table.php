<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnItemIdSourceInMaterialPreparationsTable extends Migration
{
    public function up()
    {
        Schema::table('material_preparations', function (Blueprint $table) 
        {
            $table->string('item_id_source')->nullable();
        });


        Schema::table('material_movement_lines', function (Blueprint $table) 
        {
            $table->string('item_id_source')->nullable();
        });

        Schema::table('material_movement_per_sizes', function (Blueprint $table) 
        {
            $table->string('item_id_source')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
