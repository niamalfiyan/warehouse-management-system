<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialMovementLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_movement_lines', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_preparation_id',36);
            $table->string('item_code')->nullable();
            $table->string('type_po')->nullable();
            $table->datetime('date_movement');
            $table->double('qty_movement',15,8);
            $table->integer('user_id')->unsigned();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_inserted_to_subcont')->default(false);
            $table->boolean('is_exclude')->default(false);
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_movement_lines');
    }
}
