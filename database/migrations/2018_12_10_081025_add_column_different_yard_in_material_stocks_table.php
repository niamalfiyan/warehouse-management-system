<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDifferentYardInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->double('different_yard',15,8)->nullable();
        });
    }

    
    public function down()
    {
        //
    }
}
