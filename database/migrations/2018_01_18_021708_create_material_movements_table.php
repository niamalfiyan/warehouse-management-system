<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialMovementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_movements', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('from_location',36);
            $table->char('to_destination',36);
            $table->string('po_buyer')->nullable();
            $table->boolean('is_active')->default(true);
            $table->string('status'); 
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            

            $table->foreign('from_location')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('to_destination')->references('id')->on('locators')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_movements');
    }
}
