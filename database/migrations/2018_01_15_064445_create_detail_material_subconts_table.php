<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialSubcontsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_subconts', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('material_subcont_id',36);
            $table->string('barcode')->nullable();
            $table->string('qty')->nullable();
            $table->string('batch_number')->nullable();
            $table->string('nomor_roll')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
            $table->foreign('material_subcont_id')->references('id')->on('material_subconts')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_subconts');
    }
}
