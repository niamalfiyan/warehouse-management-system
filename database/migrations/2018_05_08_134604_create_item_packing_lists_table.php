<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPackingListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_packing_lists', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('no_packing_list')->nullable();
            $table->string('no_resi')->nullable();
            $table->string('no_invoice')->nullable();
            $table->string('c_bpartner_id')->nullable();
            $table->string('type_po')->nullable();
            $table->integer('total_item')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_packing_lists');
    }
}
