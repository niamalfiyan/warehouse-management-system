<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBorrowingTrimsCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('borrowing_trim_cards', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('barcode');
            $table->integer('pic_trim_card_id');
            $table->char('line_id',36)->nullable();
            $table->string('name');
            $table->datetime('borrowed_date')->nullable();
            $table->datetime('returned_date')->nullable();
            $table->string('remark')->nullable();
            $table->datetime('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('line_id')->references('id')->on('lines')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('borrowing_trim_cards');
    }
}
