<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReroutePoBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reroute_po_buyers', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('old_po_buyer')->nullable();
            $table->string('new_po_buyer')->nullable();
            $table->integer('user_id')->unsigned();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reroute_po_buyers');
    }
}
