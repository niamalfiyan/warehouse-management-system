<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialOpnameFabricSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_opname_fabric_summaries', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->char('summary_stock_fabric_id',36)->nullable();
            $table->foreign('summary_stock_fabric_id')->references('id')->on('summary_stock_fabrics')->onUpdate('cascade')->onDelete('cascade');
            $table->double('qty_sto',18,2)->nullable();
            $table->string('remark')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_opname_fabric_summaries');
    }
}
