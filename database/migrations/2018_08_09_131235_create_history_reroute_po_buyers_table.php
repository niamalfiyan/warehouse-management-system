<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryReroutePoBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_reroute_po_buyers', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('material_preparation_id')->unsigned()->nullable();
            $table->string('old_po_buyer')->nullable();
            $table->string('new_po_buyer')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->foreign('material_preparation_id')->references('id')->on('material_preparations')->onUpdate('cascade')->onUpdate('cascade')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_reroute_po_buyers');
    }
}
