<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInMaterialChecksTable extends Migration
{
    public function up()
    {
        Schema::table('material_checks', function (Blueprint $table) {
           $table->text('remark')->nullable();
           $table->boolean('already_created_rma')->nullable()->default(false);
        });
    }

    public function down()
    {
        Schema::table('material_checks', function (Blueprint $table) {
            $table->dropColumn('remark'); 
            $table->dropColumn('already_created_rma'); 
        });
    }
}
