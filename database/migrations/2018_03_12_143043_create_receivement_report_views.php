<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceivementReportViews extends Migration
{
    public function up(){
        /*DB::statement("
            CREATE OR REPLACE VIEW receivement_report AS
                select
                    stock_lines.document_no,
                    stock_lines.category,
                    stock_lines.no_packing_list,
                    stock_lines.no_invoice,
                    stock_lines.supplier_name,
                    stock_lines.po_buyer,
                    to_char(stock_lines.created_at, 'dd-mm-yyyy  HH24:MI:SS') as created_at,
                    trim(upper(stocks.item_code)) as item_code,
                    trim(upper(stocks.item_desc)) as item_desc,
                    stock_lines.qty_conversion
                from stock_lines
                join stocks on stocks.id = stock_lines.stock_id
                where stock_lines.is_active = true
                and stock_lines.is_scanned = true;
        ");*/
    }

    public function down()
    {
        DB::statement('DROP VIEW receivement_report CASCADE');
    }
}
