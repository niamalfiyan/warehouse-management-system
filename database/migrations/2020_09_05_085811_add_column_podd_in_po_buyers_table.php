<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPoddInPoBuyersTable extends Migration
{

    public function up()
    {
        Schema::table('po_buyers', function (Blueprint $table) 
        {
            $table->date('podd')->nullable();
        });
    }

    public function down()
    {
        Schema::table('po_buyers', function (Blueprint $table) 
        {
            $table->dropColumn('podd');
        });
    }
}
