<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsIdInDetailMaterialStocksTable extends Migration
{
   public function up()
    {
        Schema::table('detail_material_stocks', function (Blueprint $table) {
            $table->double('availability_qty',15,8)->default(0)->nullable();
            $table->double('reserved_qty',15,8)->default(0)->nullable();
        });
    }

    public function down()
    {
        
        Schema::table('detail_material_stocks', function (Blueprint $table) {
            $table->dropColumn('availability_qty'); 
            $table->dropColumn('reserved_qty'); 
        });
    }
}
