<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsInMaterialStocksTable extends Migration
{
    public function up()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
           $table->string('source')->nullable();
        });
    }

    public function down()
    {
        Schema::table('material_stocks', function (Blueprint $table) {
            $table->dropColumn('source'); 
            
        });
    }
}
