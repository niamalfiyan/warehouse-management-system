<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialRequirementPerPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_requirement_per_parts', function (Blueprint $table) {
            $table->char('id',36)->primary();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('article_no')->nullable();
            $table->boolean('is_piping')->nullable();
            $table->string('style')->nullable();
            $table->string('category')->nullable();
            $table->string('uom')->nullable();
            $table->string('part_no')->nullable();
            $table->double('qty_required',15,8)->nullable();
            $table->date('statistical_date')->nullable();
            $table->date('lc_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_requirement_per_parts');
    }
}
