<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailMaterialRollHandoverFabricsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_material_roll_handover_fabrics', function (Blueprint $table) {
            /*$table->char('id',36)->primary();
            $table->char('material_stock_source_id',36)->nullable();
            $table->char('material_roll_handover_fabric_id',36)->nullable();
            $table->string('po_buyer')->nullable();
            $table->string('item_code')->nullable();
            $table->string('color')->nullable();
            $table->string('style')->nullable();
            $table->string('article_no')->nullable();
            $table->string('job_order')->nullable();
            $table->string('part_no')->nullable();
            $table->boolean('is_piping')->nullable();
            $table->double('reserved_qty',18,2)->nullable();
            $table->string('uom')->nullable();
            $table->date('statistical_date')->nullable();
            $table->date('lc_date')->nullable();
            $table->date('planning_date')->nullable();
            $table->date('actual_planning_date')->nullable();

            $table->integer('user_id')->nullable();
            $table->datetime('deleted_at')->nullable();
            

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('material_stock_source_id')->references('id')->on('material_stocks')->onUpdate('cascade')->onDelete('cascade');
           
            $table->timestamps();*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_material_roll_handover_fabrics');
    }
}
