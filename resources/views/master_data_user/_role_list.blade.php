<table class="table table-striped table-hover">
	<thead>
	  <tr>
		<th>Name</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->display_name }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button" data-id="{{ $list->id }}" 
						data-name="{{ $list->name }}" >Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>
{!! $lists->appends(Request::except('page'))->render() !!}
