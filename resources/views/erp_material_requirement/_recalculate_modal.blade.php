<div id="recalculateModal" data-backdrop="static" data-keyboard="false" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Recalculate</h5>
				<button type="button" class="close" data-dismiss="modal">×</button>
			</div>
				{!!
					Form::open([
						'role'    => 'form',
						'url'     => route('erpMaterialRequirement.recalculate'),
						'method'  => 'get',
						'class'   => 'form-horizontal',
						'enctype' => 'multipart/form-data',
						'id'      => 'recalculate_download'
					])
				!!}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							@include('form.text', [
								'field' => 'batch_code',
								'label' => 'Batch Code',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'attributes' => [
									'id' => 'batch_code'
								]
							])
						</div>
						{{-- {!! Form::hidden('close_user_id',null, array('id' => 'close_user_id')) !!} --}}
						{{-- {!! Form::hidden('mappings', '[]', array('id' => 'update_mappings')) !!} --}}
										
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Download</button>
				</div>
				{!! Form::close() !!}	
			
		</div>
	</div>
</div>