@extends('layouts.app', ['active' => 'erp_material_requiremet'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Requirement</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Requirement</li>
        </ul>
		<ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				<ul class="dropdown-menu dropdown-menu-right">
				@role(['admin-ict-acc', 'mm-staff-acc', 'mm-staff'])
					<li><a href="{{ route('erpMaterialRequirement.uploadManualexport') }}"><i class="icon-download pull-right"></i> Download Manual</a></li>
					<li><a id="upload_manual_button"><i class="icon-upload pull-right"></i> Upload Manual</a></li>
					{{-- {!! Form::hidden('batch_id',, array('id' => 'batch_id')) !!} --}}
					<!-- <li><a href="#" onclick="recalculate()"><i class="icon-download pull-right"></i> Get Qty PR</a></li> -->
				@endrole
					<li><a href="{{ route('erpMaterialRequirement.syncPerItem') }}"><i class="icon-upload pull-right"></i> Sync Per Item</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection


@section('page-modal')
	@include('erp_material_requirement._recalculate_modal')
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		{!!
			Form::open([
				'role' 		=> 'form',
				'url' 		=> route('erpMaterialRequirement.store'),
				'method' 	=> 'post',
				'enctype' 	=> 'multipart/form-data',
				'class' 	=> 'form-horizontal',
				'id'		=> 'form'
			])
		!!}
		
		<div class="panel-body">
			@include('form.text', [
				'field' 			=> 'po_buyer',
				'placeholder'		=> 'Please type po buyer here',
				'form_col' 			=> 'col-xs-12',
				'mandatory' 		=> '* Required',
				
			])

			@include('form.checkbox', [
				'field' 		=> 'is_reduce',
				'label' 		=> 'Is Reduce',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'style_checkbox' => 'checkbox checkbox-switchery',
				'class' 		=> 'switchery',
				'attributes' 	=> [
					'id' 		=> 'checkbox_is_reduce'
				]
			])
			
			<button type="submit" class="btn btn-blue-success col-xs-12" >Search <i class="icon-search4 position-left"></i></button>
		</div>

		{!! Form::close() !!}
	</div>
	
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
			<a class="btn btn-primary btn-icon" href="{{ route('erpMaterialRequirement.export')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
			<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				
			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('erpMaterialRequirement.import'),
					'method' 	=> 'post',
					'id' 		=> 'upload_file_allocation',
					'enctype' 	=> 'multipart/form-data'
				])
			!!}
				<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
				<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
			{!! Form::close() !!}

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('erpMaterialRequirement.importUploadManual'),
					'method' 	=> 'post',
					'id' 		=> 'upload_file_manual_allocation',
					'enctype' 	=> 'multipart/form-data'
				])
			!!}
				<input type="file" class="hidden" id="upload_manual_file" name="upload_manual_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
				<button type="submit" class="btn btn-success hidden btn-lg" id="submit_manual_button" >Submit</button>
			{!! Form::close() !!}
			
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic table-striped table-hover table-responsive','id'=> 'material-requirement-datatable']) !!}
			</div>
		</div>
	</div>
	{!! Form::hidden('is_from_home',false, array('id' => 'is_from_home')) !!}
	<a href="{{ route('erpMaterialRequirement.index') }}" id="url_material_requirement_index"></a>
@endsection

@section('page-js')
	{!! $html->scripts() !!}
	<script src="{{ mix('js/switch.js') }}"></script>
	<script src="{{ mix('js/erp_material_requirement.js') }}"></script>
@endsection
