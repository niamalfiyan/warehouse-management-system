@extends('layouts.app', ['active' => 'fabric_material_out_cutting'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Out Cutting</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Out Cutting</li>
        </ul>

		@role(['admin-ict-fabric'])
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('fabricMaterialOutCutting.downloadFormUpload')}}"><i class="icon-download pull-right"></i> Download</a></li>
						<li><a href="#" id="upload_button"><i class="icon-upload pull-right"></i> Upload</a></li>
					</ul>
				</li>
			</ul>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialOutCutting.uploadFormUpload'),
					'method' 	=> 'POST',
					'id' 		=> 'upload_file_form',
					'enctype' 	=> 'multipart/form-data'
				])
			!!}
				{!! Form::hidden('upload_warehouse_id',auth::user()->warehouse , array('id' => 'upload_warehouse_id')) !!}
				<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
			{!! Form::close() !!}
		
		@endrole

    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-fabric'])
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Planning Date</th>
							<th>Source</th>
							<th>No Roll</th>
							<th>Actual Width</th>
							<th>Actual Lot</th>
							<th>Total Reserved Qty</th>
							<th>Destination</th>
							<th>Remark ICT</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-material-out-fabric">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' => 'form',
					'url' => route('fabricMaterialOutCutting.store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data',
					'class' => 'form-horizontal',
					'id'=> 'form'
				])
			!!}
				{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}

				<a href="{{ route('fabricMaterialOutCutting.create') }}" id="url_get_data" class="hidden"></a>
			
				<div class="form-group text-right" style="margin-top: 10px;">
					<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection


@section('page-js')
	@include('fabric_material_out_cutting._item')
	<script src="{{ mix('js/fabric_material_out_cutting.js') }}"></script>
@endsection
