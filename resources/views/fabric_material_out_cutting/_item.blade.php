<script type="x-tmpl-mustache" id="out-fabric-table">
	{% #list %}
		<tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %} {% #is_already_have_actual_lot %} style="background-color:#fab1b1" {% /is_already_have_actual_lot %} {% #is_for_material_saving %} style="background-color:#e8f5e9;" {% /is_for_material_saving %}>
			<td>{% no %}</td>
			<td>{% planning_date %}</td>
			<td>
				<p>Locator<br/><b>{% locator_from_code %}</b></p>
				<p>Supplier Name<br/><b>{% supplier_name %}</b></p>
				<p>Po Supplier<br/><b>{% document_no %}</b></p>
				<p>Item Code <br/><b>{% item_code %}</b></p>
				<p>Color <br/><b>{% color %}</b></p>
				<p>Batch Number <br/><b>{% batch_number %}</b></p>
				<p>Article No <br/><b>{% article_no %}</b></p>
			</td>
			<td>{% nomor_roll %}</td>
			<td>{% actual_width %}</td>
			<td><input type="text" class="form-control input-sm input-item input-actual-lot-edit" id="actualLotInput_{% _id %}" value="{% actual_lot %}" data-id="{% _id %}" data-row="" {% ^is_already_have_actual_lot %} readonly="readonly" {% /is_already_have_actual_lot %}></td>
			<td>{% reserved_qty %} ({% uom %})</td>
			<td>{% destination_name %}</td>
			<td>{% error_note %} ({% remark_ict %})</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
			
		</tr>
	{%/list%}
	<tr>
		<td colspan="10">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
		</td>

	</tr>
	
</script>
