<script type="x-tmpl-mustache" id="accessories_material_in_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>
				<p id="no_{% _id %}">{% no %}</p>
			</td>
			<td>
				<p id="statistical_date_{% _id %}">{% statistical_date %}</p>
			</td>
			<td>
				<p id="season{% _id %}">{% season %}</p>
			</td>
			<td>
				<p id="source_{% _id %}">{% source_rak_code %}</p>
			</td>
			<td>
				<p id="pobuyer_{% _id %}">{% po_buyer %}</p>
				<input type="hidden" class="form-control input-sm hidden input-item input-edit" id="poBuyerInput_{% _id %}" value="{% po_buyer %}" data-id="{% _id %}" data-row="">
				<input type="hidden" class="form-control input-sm hidden input-item input-edit" id="poBuyerInput_{% _id %}" value="{% locator %}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<p id="itemCode_{% _id %}">{% code %}</p>
			</td>
			<td>
				{% counter %}/{% total_carton %}
			</td>
			<td>
				<p id="style_{% _id %}">{% style %}</p>
			</td>
			<td>
				<p id="uom_conversion_{% _id %}">{% uom_conversion %}</p>
			</td>
			<td>
				<p>{% qty_need %}</p>
			</td>
			<td>
				<p>{% qty_receive %}</p>
			</td>
			<td>
				<p>{% error_note %}</p>
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="13">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
			<span class="text-danger hidden" id="errorInput"></span>
		</td>
		
	</tr>
</script>