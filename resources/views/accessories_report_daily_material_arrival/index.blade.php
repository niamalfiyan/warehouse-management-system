@extends('layouts.app', ['active' => 'accessories_report_daily_material_arrival'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Arrival</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Daily Material Arrival</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		@include('form.select', [
			'field' => 'warehouse',
			'label' => 'Warehouse',
			'default' => auth::user()->warehouse,
			'label_col' => 'col-md-2 col-lg-2 col-sm-12',
			'form_col' => 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				'' => '-- Select Warehouse --',
				'1000002' => 'Warehouse Accessories AOI 1',
				'1000013' => 'Warehouse Accessories AOI 2',
				'1000001' => 'Warehouse Fabric AOI 1',
				'1000011' => 'Warehouse Fabric AOI 2',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_warehouse'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'start_date',
			'label' 		=> 'Arrival Date From (00:00)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes'	=> [
				'id' 			=> 'start_date',
				'autocomplete' 	=> 'off',
				'readonly' 		=> 'readonly'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'end_date',
			'label' 		=> 'Arrival Date To (23:59)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes' 	=> [
				'id' 			=> 'end_date',
				'readonly' 		=> 'readonly',
				'autocomplete' 	=> 'off'
			]
		])

		{!!
			Form::open(array(
					'class' => 'heading-form',
					'role' => 'form',
					'url' => route('accessoriesReportDailyMaterialArrival.export'),
					'method' => 'get',
					'target' => '_blank'		
			))
		!!}
		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_material_arrival_table">
				<thead>
					<tr>
						<th>ID</th>
						<th>Arrival Date</th>
						<th>Warehouse Arrival</th>
						<th>Pic Arrival</th>
						<th>No Invoice</th>
						<th>Po Supplier</th>
						<th>Supplier Name</th>
						<th>Item Code</th>
						<th>Item Desc</th>
						<th>Category</th>
						<th>Uom</th>
						<th>Total Carton</th>
						<th>Total Arrival Carton</th>
						<th>Total Qty Arrival</th>
						<th>Prepared Status</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/accessories_report_daily_material_arrival.js'))}}"></script>
@endsection
