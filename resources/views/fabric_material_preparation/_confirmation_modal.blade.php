<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false" aria-labelledby="confirmationModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title"><p>Confirmation Modal For <span id="_supplier_name"></span></p> <p><span id="_po_supplier"></span> <b>-</b> <span id="_item_code"></span></p> </h5>
			</div>
		
			<div class="modal-body">
				<div class="tabbable tab-content-bordered">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#regular" data-toggle="tab" aria-expanded="false" onclick="changeTab('reguler')">Reguler</a></li>
						<li><a href="#additional" data-toggle="tab" aria-expanded="true" onclick="changeTab('additional')">Additional</a></li>
						<li><a href="#balance_marker" data-toggle="tab" aria-expanded="true" onclick="changeTab('balance_marker')">Balance Marker</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="regular">
							<br/>
							{{ 
								Form::open([
									'method'	=> 'post'
									,'id'		=> 'FormPlanning'
									,'class' 	=> 'form-horizontal'
									,'url' 		=> route('fabricMaterialPreparation.storePlanning')
								]) 
							}}
								<div class="shade-screen" style="text-align: center;">
									<div class="shade-screen hidden">
										<img src="/images/ajax-loader.gif">
									</div>
								</div>
								<div id="modal_body">
									<div class="row">
										<div class="col-md-6 col-lg-6 col-sm-12">
											@include('form.select', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'is_piping',
												'label' 		=> 'Piping',
												'options'		=> [
													''	=>'Please Select',
													'0'	=>'Bukan Piping',
													'1'	=>'Piping'
												],
												'attributes' => [
													'id' => 'is_piping'
												]
											])
											
											@include('form.date', [
												'field' 		=> 'start_date',
												'label' 		=> 'Planning Date',
												'class' 		=> 'start_date',
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'placeholder' 	=> 'dd/mm/yyyy',
												'attributes' 	=> [
													'id' 			=> 'start_date',
													'autocomplete' 	=> 'off',
													'readonly' 		=> 'readonly'
												]
											])

											@include('form.select', [
												'field' => 'article',
												'label' => 'Article No',
												'label_col' => 'col-md-2 col-lg-2 col-sm-12',
												'form_col' => 'col-md-10 col-lg-10 col-sm-12',
												'options' => [
													'' => '-- Select Article --',
												],
												'class' => 'select-search',
												'attributes' => [
													'id' => 'select_article'
												]
											])

											@include('form.select', [
												'field' => 'style',
												'label' => 'Style',
												'label_col' => 'col-md-2 col-lg-2 col-sm-12',
												'form_col' => 'col-md-10 col-lg-10 col-sm-12',
												'options' => [
													'' => '-- Select Style --',
												],
												'class' => 'select-search',
												'attributes' => [
													'id' => 'select_style'
												]
											])
										</div>
										<div class="col-md-6 col-lg-6 col-sm-12">
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 			=> 'qty_preparation',
												'label' 			=> 'Qty Preparation (YDS)',
												'placeholder' 		=> 'Silahkan masukan Total Qty yang dipersiapkan',
												'attributes' 		=> [
													'id' 			=> 'qty_preparation',
													'readonly' 		=> 'readonly',
												]
											])
											
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'qty_relax',
												'label' 		=> 'Qty Relax (YDS)',
												'attributes' 	=> [
													'id' 		=> 'qty_relax',
													'readonly' => 'readonly',
												]
											])
											
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'qty_outstanding',
												'label' 		=> 'Qty Outstanding (YDS)',
												'attributes' 	=> [
													'id' 		=> 'qty_outstanding',
													'readonly' => 'readonly',
												]
											])
										</div>
									</div>
									
									{!! Form::hidden('warehouse_id', auth::user()->warehouse , array('id' => 'planning_warehouse_id')) !!}
									{!! Form::hidden('is_edit', null , array('id' => 'is_edit')) !!}
									{!! Form::hidden('material_preparation_fabric_id', null , array('id' => 'material_preparation_fabric_id')) !!}
									{!! Form::hidden('supplier_name', null , array('id' => '__supplier_name')) !!}
									{!! Form::hidden('c_bpartner_id', null , array('id' => '__c_bpartner_id')) !!}
									{!! Form::hidden('item_code', null, array('id' => '__item_code')) !!}
									{!! Form::hidden('item_code_source', null, array('id' => '__item_code_source')) !!}
									{!! Form::hidden('item_id_book', null, array('id' => '__item_id_book')) !!}
									{!! Form::hidden('item_id_source', null, array('id' => '__item_id_source')) !!}
									{!! Form::hidden('document_no', null , array('id' => '__document_no')) !!}
									{!! Form::hidden('c_order_id', null , array('id' => '__c_order_id')) !!}
									{!! Form::hidden('planning_date', null , array('id' => '__planning_date')) !!}
									{!! Form::hidden('is_additional', -1 , array('id' => '_is_additional_add')) !!}
									{!! Form::hidden('is_tab', 'reguler' , array('id' => 'reguler')) !!}

									<div class="row">
										<div class="col-md-12">
											<div class="col-xs-12">
												<h6 class="text-semibold" style="text-align: left;">Information</h6>
												<p style="text-align: left;"><b>Qty Preparation </b> yang dimasukan harus sesuai dengan total yang diberikan admin fabric.
												</p>
											</div>
										</div>
									</div>

									<div class="text-right">
										<button type="button" class="btn btn-default" onClick="dismisModal()" >Cancel <i class="icon-x position-left"></i></button>
										<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
									</div>
									{{ Form::close() }}
								</div>
								<br/>
						</div>
						<div class="tab-pane" id="additional">
							<br/>
							{{ 
								Form::open([
									'method' 	=> 'post'
									,'id' 		=> 'FormPlanningAdd'
									,'class' 	=> 'form-horizontal'
									, 'url' 	=> route('fabricMaterialPreparation.storePlanningAdd')
								]) 
							}}

							@include('form.select', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'is_piping',
								'label' 		=> 'Piping',
								'options'	 	=> [
									''	=>'Please Select',
									'0'	=>'Bukan Piping',
									'1'	=>'Piping'
								],
								'attributes' => [
									'id' => 'additional_is_piping'
								]
							])

							@include('form.date', [
								'field' 		=> 'start_date',
								'label' 		=> 'Allocation Date',
								'class' 		=> 'start_date',
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'placeholder' 	=> 'dd/mm/yyyy',
								'attributes' 	=> [
									'id' 			=> 'start_date',
									'autocomplete' 	=> 'off',
									'readonly' 		=> 'readonly'
								]
							])

							@include('form.select', [
								'field' => 'booking_number',
								'label' => 'Booking Number',
								'label_col' => 'col-md-2 col-lg-2 col-sm-12',
								'form_col' => 'col-md-10 col-lg-10 col-sm-12',
								'options' => [
									'' => '-- Select Booking Number --',
								],
								'class' => 'select-search',
								'attributes' => [
									'id' => 'select_booking_number'
								]
							])
							
							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 			=> 'qty_preparation',
								'label' 			=> 'Qty Preparation (YDS)',
								'placeholder' 		=> 'Silahkan masukan Total Qty yang dipersiapkan',
								'attributes' 		=> [
									'id' 			=> 'qty_preparation_additional',
									'readonly' 		=> 'readonly',
								]
							])
							
							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'qty_relax',
								'label' 		=> 'Qty Relax (YDS)',
								'attributes' 	=> [
									'id' 		=> 'qty_relax_additional',
									'readonly' => 'readonly',
								]
							])
							
							@include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'qty_outstanding',
								'label' 		=> 'Qty Outstanding (YDS)',
								'attributes' 	=> [
									'id' 		=> 'qty_outstanding_additional',
									'readonly' => 'readonly',
								]
							])

							<!-- @include('form.text', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'qty_preparation',
								'label' 		=> 'Qty Preparation (YDS)',
								'placeholder' 	=> 'Silahkan masukan Total Qty yang dipersiapkan',
								'attributes' 	=> [
									'id' 			=> 'qty_preparation_add',
									'autocomplate' 	=> 'off'
								]
							]) -->

							@include('form.textarea', [
								'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
								'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
								'field' 		=> 'additional_note',
								'label'			=> 'Additional Note',
								'placeholder' 	=> 'Please type reason here',
								'attributes' 	=> [
									'id' 		=> 'additional_note',
									'style' 	=> 'resize:none'
								]
							])

							{!! Form::hidden('warehouse_id', auth::user()->warehouse , array('id' => 'additional_warehouse_id')) !!}
							{!! Form::hidden('material_preparation_fabric_id', null , array('id' => 'material_preparation_fabric_id_additional')) !!}
							{!! Form::hidden('supplier_name', null , array('id' => '__supplier_name_add')) !!}
							{!! Form::hidden('c_bpartner_id', null , array('id' => '__c_bpartner_id_add')) !!}
							{!! Form::hidden('item_code_source', null, array('id' => '__item_code_source_add')) !!}
							{!! Form::hidden('item_code', null, array('id' => '__item_code_add')) !!}
							{!! Form::hidden('item_id_source', null, array('id' => '__item_id_source_add')) !!}
							{!! Form::hidden('item_id_book', null, array('id' => '__item_id_add')) !!}
							{!! Form::hidden('c_order_id', null , array('id' => '__c_order_id_add')) !!}
							{!! Form::hidden('document_no', null , array('id' => '__document_no_add')) !!}
							{!! Form::hidden('is_additional', 1 , array('id' => '_is_additional_add')) !!}
							{!! Form::hidden('is_tab', 'additional' , array('id' => 'additional')) !!}
							
							<div class="row">
								<div class="col-md-12">
									<div class="col-xs-12">
										<h6 class="text-semibold" style="text-align: left;">Information</h6>
										<p style="text-align: left;"><b>Qty Preparation </b> yang dimasukan harus sesuai dengan total yang diberikan admin fabric.
										</p>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="button" class="btn btn-default" onClick="dismisModal()" >Cancel  <i class="icon-x position-left"></i></button>
								<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
							</div>
						{{ Form::close() }}
						<br/>
						</div>
						<div class="tab-pane" id="balance_marker">
							<br/>
							{{ 
								Form::open([
									'method'	=> 'post'
									,'id'		=> 'FormPlanningBalanceMarker'
									,'class' 	=> 'form-horizontal'
									,'url' 		=> route('fabricMaterialPreparation.storePlanningBalanceMarker')
								]) 
							}}
								<div class="shade-screen" style="text-align: center;">
									<div class="shade-screen hidden">
										<img src="/images/ajax-loader.gif">
									</div>
								</div>
								<div id="modal_body">
									<div class="row">
										<div class="col-md-6 col-lg-6 col-sm-12">
											@include('form.select', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'is_piping',
												'label' 		=> 'Piping',
												'options'		=> [
													'0'	=>'Bukan Piping',
												],
												'attributes' => [
													'id' => 'is_piping'
												]
											])
											
											@include('form.date', [
												'field' 		=> 'start_date',
												'label' 		=> 'Planning Date',
												'class' 		=> 'start_date',
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'placeholder' 	=> 'dd/mm/yyyy',
												'attributes' 	=> [
													'id' 			=> 'start_date',
													'autocomplete' 	=> 'off',
													'readonly' 		=> 'readonly'
												]
											])

											@include('form.select', [
												'field' => 'article',
												'label' => 'Article No',
												'label_col' => 'col-md-2 col-lg-2 col-sm-12',
												'form_col' => 'col-md-10 col-lg-10 col-sm-12',
												'options' => [
													'' => '-- Select Article --',
												],
												'class' => 'select-search',
												'attributes' => [
													'id' => 'select_article_balance_marker'
												]
											])

											@include('form.select', [
												'field' => 'style',
												'label' => 'Style',
												'label_col' => 'col-md-2 col-lg-2 col-sm-12',
												'form_col' => 'col-md-10 col-lg-10 col-sm-12',
												'options' => [
													'' => '-- Select Style --',
												],
												'class' => 'select-search',
												'attributes' => [
													'id' => 'select_style_balance_marker'
												]
											])
										</div>
										<div class="col-md-6 col-lg-6 col-sm-12">
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 			=> 'qty_preparation',
												'label' 			=> 'Qty Preparation (YDS)',
												'placeholder' 		=> 'Silahkan masukan Total Qty yang dipersiapkan',
												'attributes' 		=> [
													'id' 			=> 'qty_preparation_balance_marker',
													'readonly' 		=> 'readonly',
												]
											])
											
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'qty_relax',
												'label' 		=> 'Qty Relax (YDS)',
												'attributes' 	=> [
													'id' 		=> 'qty_relax_balance_marker',
													'readonly' => 'readonly',
												]
											])
											
											@include('form.text', [
												'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
												'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
												'field' 		=> 'qty_outstanding',
												'label' 		=> 'Qty Outstanding (YDS)',
												'attributes' 	=> [
													'id' 		=> 'qty_outstanding_balance_marker',
													'readonly' => 'readonly',
												]
											])
										</div>
									</div>
									
									{!! Form::hidden('warehouse_id', auth::user()->warehouse , array('id' => 'bm_warehouse_id')) !!}
									{!! Form::hidden('is_edit', null , array('id' => 'is_edit')) !!}
									{!! Form::hidden('material_preparation_fabric_id', null , array('id' => 'material_preparation_fabric_id_bm')) !!}
									{!! Form::hidden('supplier_name', null , array('id' => '__supplier_name_bm')) !!}
									{!! Form::hidden('c_bpartner_id', null , array('id' => '__c_bpartner_id_bm')) !!}
									{!! Form::hidden('item_code', null, array('id' => '__item_code_bm')) !!}
									{!! Form::hidden('item_code_source', null, array('id' => '__item_code_source_bm')) !!}
									{!! Form::hidden('item_id_book', null, array('id' => '__item_id_book_bm')) !!}
									{!! Form::hidden('item_id_source', null, array('id' => '__item_id_source_bm')) !!}
									{!! Form::hidden('document_no', null , array('id' => '__document_no_bm')) !!}
									{!! Form::hidden('c_order_id', null , array('id' => '__c_order_id_bm')) !!}
									{!! Form::hidden('planning_date', null , array('id' => '__planning_date_bm')) !!}
									{!! Form::hidden('is_additional', -1 , array('id' => '_is_additional_bm')) !!}
									{!! Form::hidden('is_tab', 'balance_marker' , array('id' => 'is_tab')) !!}
									
									<div class="row">
										<div class="col-md-12">
											<div class="col-xs-12">
												<h6 class="text-semibold" style="text-align: left;">Information</h6>
												<p style="text-align: left;"><b>Qty Preparation </b> yang dimasukan harus sesuai dengan total yang diberikan admin fabric.
												</p>
											</div>
										</div>
									</div>

									<div class="text-right">
										<button type="button" class="btn btn-default" onClick="dismisModal()" >Cancel <i class="icon-x position-left"></i></button>
										<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
									</div>
									{{ Form::close() }}
								</div>
								<br/>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>