<script type="x-tmpl-mustache" id="material-planning-fabric-table">
	{% #list %}
		<tr {% #is_selected %} style="background-color:#d9ffde" {% /is_selected %}>
			<td>{% planning_date %}</td>
			<td>{% supplier_name %}</td>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% item_code_source %}</td>
			<td>{% article_no %}</td>
			<td> {% _total_qty_outstanding %} /{% total_qty_outstanding %} ({% uom %})</td>
			<td>
				{% #is_piping %}
					<span class="label label-warning">Piping</span>
				{% /is_piping %}
				{% ^is_piping %}
					<span class="label label-info">Not Piping</span>
				{% /is_piping %}
			</td>
			<td>
				{% #is_additional %}
					<span class="label label-warning">Additional</span>
				{% /is_additional %}
				{% ^is_additional %}
					<span class="label label-info">Reguler</span>
				{% /is_additional %}
			</td>
			<td> <button type="button" data-id="{% _id %}" class="btn btn-info btn-show-roll"><i class="fa icon-eye"></i></button> </td>
		</tr>
	{%/list%}
	
</script>