<script type="x-tmpl-mustache" id="material-preparation-fabric-table">
	{% #list %}
		<tr {% #is_selected %} style="background-color:#d9ffde" {% /is_selected %}>
			<td>{% no %}</td>
			<td>{% nomor_roll %}</td>
			<td>{% batch_number %}</td>
			<td>
				{% load_actual %}
				<!--<input type="text" class="form-control input-sm input-item input-edit input-new-actualload" id="loadActualInput_{% _id %}" value="{% load_actual %}" data-id="{% _id %}" placeholder="INPUT ACTUAL LOAD">-->
			</td>
			<td>{% uom %}</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number input-new-actualwidth set-focus-to-center set-zero-begin" id="beginWidhtInput_{% _id %}" value="{% begin_width %}" data-id="{% _id %}" autocomplete="off">
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number input-new-actualwidth set-focus-to-center set-zero-middle" id="middleWidhtInput_{% _id %}" value="{% middle_width %}" data-id="{% _id %}" autocomplete="off">
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number input-new-actualwidth set-focus-to-center set-zero-end" id="endWidhtInput_{% _id %}" value="{% end_width %}" data-id="{% _id %}" autocomplete="off">
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number input-new-actualwidth" id="actualWidhtInput_{% _id %}" value="{% actual_width %}" data-id="{% _id %}" readonly="readonly" autocomplete="off"> 
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number input-new-actual-length set-focus-to-center set-zero-actual-length" id="actualLengthInput_{% _id %}" value="{% actual_length %}" data-id="{% _id %}" autocomplete="off" {% #is_actual_not_null %} readonly="readonly" {% /is_actual_not_null %}>
			</td>
			<td>{% _available_qty %}/{% available_qty %}</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number" id="qtyBookingInput_{% _id %}" value="{% qty_booking %}" data-id="{% _id %}" placeholder="INPUT QTY BOOKING" readonly="readonly">
			</td>
			<td>
				<span><button type="button" id="edit_{% _id %}" data-id="{% _id %}" class="btn btn-default  btn-edit-item"><i class="icon-pencil"></i></button>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button></span>
			</td>
			
			
		</tr>
	{%/list%}
	<tr>
		<td colspan="14">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>