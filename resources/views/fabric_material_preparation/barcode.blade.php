
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: BARCODE PREPARATION</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach($barcode_preparation_fabrics as $key => $barcode_preparation_fabric)
			@php
				$is_recycle = $barcode_preparation_fabric->getRecycle($barcode_preparation_fabric->item_id);
			@endphp
			<div class="outer">
					<div class="title">
						<span style="@if(strlen($barcode_preparation_fabric->materialStock->document_no)>40) font-size: 12px @else font-size: 34px @endif">{{ $barcode_preparation_fabric->materialStock->document_no }}</span>
					</div>
					<div class="isi">
						<div class="isi1">
							<span style="
							@if(strlen($barcode_preparation_fabric->item_code)>40) 
								font-size: 12px
						 	@else
								font-size: 20px 
							@endif">{{ $barcode_preparation_fabric->item_code.$is_recycle }}</span>
						</div>
						<div class="isi1" style="
						@if(strlen($barcode_preparation_fabric->color)>100 && strlen($barcode_preparation_fabric->color)<150) 
							font-size: 12px;line-height:0.3cm;
						@elseif(strlen($barcode_preparation_fabric->color)>150 && strlen($barcode_preparation_fabric->color)<175) 
							height:40px; 
						@elseif(strlen($barcode_preparation_fabric->color)>175 && strlen($barcode_preparation_fabric->color)<200) 
							height:75px  
						@endif">
						<span style="@if(strlen($barcode_preparation_fabric->color)>15) 
							font-size: 7px;word-wrap: break-word;
							@else 
								font-size: 30px 
							@endif">{{ $barcode_preparation_fabric->color }}</span>
						</div>
						
						<div class="isi1">
							<span style="@if(strlen($barcode_preparation_fabric->materialStock->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $barcode_preparation_fabric->materialStock->batch_number }}</span>
						</div>
						
						<div class="isi2">
							<div class="block-kiri" style="justify-content: center; height: 45px;">
								<span style="font-size: 12px;word-wrap: break-word;">{{ $barcode_preparation_fabric->materialStock->supplier_name }}  </span>
							</div>
							<div class="block-kiri" style="justify-content: center; height: 45px;">
								<span style="font-size: 20px;word-wrap: break-word;">{{ sprintf("%0.4f",$barcode_preparation_fabric->reserved_qty) }} {{ strtoupper($barcode_preparation_fabric->materialStock->uom) }}   </span>
							</div>
							<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/><span style="@if(strlen($barcode_preparation_fabric->materialStock->nomor_roll)>=4) font-size: 35px @else font-size: 70px @endif">{{ $barcode_preparation_fabric->materialStock->nomor_roll }}</p>
						
						</div>
						<div class="isi3">
							<div class="block-kiri" style="height:30px;font-size:25px;">
								@if($barcode_preparation_fabric->materialStock->jenis_po){{ $barcode_preparation_fabric->materialStock->jenis_po }} @else - @endif
							</div>
							<div class="block-kiri" style="height: 120px;">
								<img style="height: 2.5cm; width: 4.3cm; margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$barcode_preparation_fabric->barcode", 'C128') }}" alt="barcode"   />			
								<br/>
								<span style="font-size: 12px;word-wrap: break-word;">{{ $barcode_preparation_fabric->barcode }}</span>
					
							</div>
							<div class="block-kiri">
								Tgl. Preparation {{ $barcode_preparation_fabric->created_at->format('d/M/y H:i:s') }}
							</div>
							<div class="block-kiri">
								Di persiapkan oleh {{ $barcode_preparation_fabric->user->name}}
							</div>
							
						</div>
					</div>
							
				</div>
			@endforeach
    </body>
</html>