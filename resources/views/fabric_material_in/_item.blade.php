<script type="x-tmpl-mustache" id="fabric_material_in_table">
	{% #list %}
		<tr {% #is_barcode_preparation_fabric %} style="background-color:#fffeba" {% /is_barcode_preparation_fabric %}>
			<td>{% no %}</td>
			<td>
				<ul class="list list-unstyled">
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Locator Source </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% source_rak_code %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> No Invoice </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{%no_invoice%}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Po Supplier </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{%document_no%}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> No Roll </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% nomor_roll %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Item </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% item_code %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Color </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% color %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Batch Number </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% batch_number %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Actual Lot </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% lot_actual %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Actual Width </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% actual_width %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Qty Available </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% qty %} ({% uom %})" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Note </b></label>
							<div class="col-lg-9">
								<input type="text" class="form-control" readonly="readonly" value="{% note %}" >
							</div>
						</div>
					</li>
					<li>
						<div class="row">
							<label class="col-lg-3"><b> Error Note </b></label>
							<div class="col-lg-9">
								<textarea class="form-control input-edit-remark" readonly="readonly" style="resize:none" id="error_note_{% _id %}" data-id="{% _id %}"">{% error_note %}</textarea>
							</div>
						</div>
					</li>
				</ul>
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
	<td colspan="3">
		<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
	</td>

	</tr>
</script>
