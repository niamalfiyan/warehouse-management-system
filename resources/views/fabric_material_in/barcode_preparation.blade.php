<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: PREPERATION</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/printbarcodewebpo.css')) }}">

</head>
    <body>
		@php $flag = 0 ; @endphp
		@foreach ($items as $key => $value)
			@for ($i = 0; $i < $value->qty_carton; $i++)
				@if (($flag+1) % 3 === 0)
					<p style="page-break-after: always;">&nbsp;</p>
				@endif

				<div class="outer" style="height:10cm;">
					<div class="title">BARCODE PREPARATION</div>

					<div class="isi" style="height:8.6cm">
						<div class="isi1">
							NO CARTON : <span style="font-size: 24px">{{ $i+1 }}</span> / {{ $value->qty_carton }}
						</div>
                        <div class="isi1" style="font-size:12px;">
                            <span style="font-size:10px">ROLL NUMBER :</span> {{ $value->nomor_roll }} |
                            <span style="font-size:10px">BATCH NUMBER :</span> {{ $value->batch_number }}
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_code)>40) font-size: 12px @endif">{{ $value->item_code }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_desc)>40) font-size: 12px @endif">{{ $value->item_desc }}</span>
						</div>

						<div class="isi2" style="height:180px;">
							<div class="block-kiri" style="justify-content: center; height: 30px; padding-top:10px">
								<span style="font-size: 13px; word-wrap: break-word;">{{ number_format($value->qty_upload, 2, '.', ',') }} {{ strtoupper($value->uom) }}</span>
							</div>
							<!--<div class="block-kiri" style="height:35px;line-height: 35px">
								<span style="font-size: px; word-wrap: break-word;">PO BUYER: {{ $value->po_buyer }}  </span>
							</div-->
							<div class="block-kiri" style="height: 39px;">
								<span style="font-size: 10px; word-wrap: break-word;">{{ $value->document_no }}<br/>{{ $value->supplier_name }}</span>
							</div>
							<div class="block-kiri" style="margin-top: 7px">
								<span style="font-size: 10px; word-wrap: break-word;"><br/></span>
							</div>
						</div>
						<div class="isi3" style="height:180px;">
							<div class="block-kiri" style="height: 120px;">
								@if ($value->qty_carton == 1)
									<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$value->barcode_supplier", 'C128') }}" alt="barcode"   />
								 	<!--<text style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $value->barcode_supplier }}</text>-->
								@else
									@php
										$_barcode = $value->barcode_supplier.'-'.($i+1);
									@endphp

									<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />
									<!--<text style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</text>-->
								@endif
							</div>
							<div class="block-kiri" style="margin-top: 6px;" >
								PO BUYER: <span style="font-size: 16px">
								{{ $value->po_buyer }}
								</span>
							</div>
						</div>
					</div>
				</div>
				@php $flag++;@endphp

			@endfor
		@endforeach

    </body>
</html>
