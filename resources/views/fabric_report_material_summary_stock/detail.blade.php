@extends('layouts.app', ['active' => 'fabric_report_summary_material_stock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Detail Stock Per Item</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportMaterialSummaryStock.index') }}">Detail Stock Per Item</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Summary<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12">
                    <p>Warehouse <b>{{ $summary_stock_fab->warehouse }}</b></p>
					<p>Item Code <b>{{ $summary_stock_fab->item_code }}</b></p>
					<p>Category <b> {{ $summary_stock_fab->category }}</b></p>
				</div>
				<div class="col-lg-6 col-md-6 col-sm-12">
					<p>Uom <b>{{ $summary_stock_fab->uom }}</b></p>
					<p>Total Qty <b>{{ number_format($summary_stock_fab->stock, 4, '.', ',') }} </b></p> 
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="detail_stock_table">
					<thead>
						<tr>
							<th>No</th>
							<th>Warehuose</th>
							<th>Locator</th>
							<th>Item Id</th>
							<th>Item Code</th>
							<th>Document No</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Remark STO</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','detail', array('id' => 'page')) !!}
	{!! Form::hidden('url_material_stock_detail_data',route('fabricReportMaterialSummaryStock.dataDetail'), array('id' => 'url_material_stock_detail_data')) !!}
	{!! Form::hidden('item_id',$summary_stock_fab->item_id, array('id' => 'item_id')) !!}
    {!! Form::hidden('warehuose_id',$summary_stock_fab->warehouse_id, array('id' => 'warehuose_id')) !!}
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/fabric_report_summary_material_stock.js'))}}"></script>
@endsection
