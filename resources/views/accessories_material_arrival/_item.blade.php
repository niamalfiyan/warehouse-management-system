<script type="x-tmpl-mustache" id="material_arrival_accessories_table">
	{% #item %}
		<tr id="panel_{% _idx %}">
			<td>{% nox %}</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% show_po_buyer %}
			</td>
			<td>
				{% qty_ordered %} ({% uom_conversion %})
			</td>
			<td>
				{% qty_conversion %} ({% uom_conversion %})
			</td>
			<td>
				{% qty_moq %} ({% uom_moq %})
			</td>
			<td>
				{% prepared_status %}
			</td>
			<td>
				{% counter %} /{% qty_carton %}
			</td>
		</tr>		
	{%/item%}
	<tr class="{{ $errors->has('roles') ? 'has-error' : '' }}">
		<td colspan="9">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>