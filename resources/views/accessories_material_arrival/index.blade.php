@extends('layouts.app', ['active' => 'accessories_material_arrival'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Arrival</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Arrival</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')

	

	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'' 				=> '-- Select Warehouse --',
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Po Supplier</th>
							<th>Item Code</th>
							<th>Po Buyer</th>
							<th>Qty Order</th>
							<th>Qty Rcv</th>
							<th>Qty Moq</th>
							<th>Prepared Status</th>
							<th>Total Package</th>
						</tr>
					</thead>
					<tbody id="tbody_material_arrival">
					</tbody>
				</table>
			</div>

		{!!
			Form::open([
				'role' 		=> 'form',
				'url' 		=> route('accessoriesMaterialArrival.store'),
				'method' 	=> 'post',
				'enctype' 	=> 'multipart/form-data',
				'id'		=> 'form'
			])
		!!}
			{!! Form::hidden('url_material_arrival_accessories_create',route('accessoriesMaterialArrival.create') , array('id' => 'url_material_arrival_accessories_create')) !!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('material_arrival','[]' , array('id' => 'material_arrival')) !!}
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
@endsection

@section('page-js')
	@include('accessories_material_arrival._item')
	<script src="{{ mix('js/accessories_material_arrival.js') }}"></script>
@endsection
