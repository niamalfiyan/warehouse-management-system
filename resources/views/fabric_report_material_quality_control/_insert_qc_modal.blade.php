<div class="modal fade" id="insertModal" tabindex="-1" role="dialog" aria-labelledby="insertModalLabel">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'GET', 'id' => 'insertqcform', 'class' => 'form-horizontal']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">CUTTABLE WIDTH</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
                           <input type="hidden" id="cuttable_width_id" value="">
						    @include('form.text', [
								'field' => 'begin',
								'label' => 'BEGIN',
								'placeholder' => 'BEGIN Width',
								'mandatory' => '*Required',
								'attributes' => [
									'id' => 'begin',
								]
							])
							@include('form.text', [
								'field' => 'middle',
								'label' => 'MIDDLE',
								'placeholder' => 'MIDDLE Width',
								'mandatory' => '*Required',
								'attributes' => [
									'id' => 'middle',

								]
                            ])
                            @include('form.text', [
								'field' => 'end',
								'label' => 'END',
								'placeholder' => 'END Width',
								'mandatory' => '*Required',
								'attributes' => [
									'id' => 'end',
								]
							])
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>