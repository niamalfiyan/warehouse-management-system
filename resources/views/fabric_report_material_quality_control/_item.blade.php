<script type="x-tmpl-mustache" id="list_confirmation_table">
    {% #item %}
        <tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
            <td>
                {% no %}
            </td>
            <td>
                {% supplier_name %}
            </td>
            <td>
                {% no_invoice %}
            </td>
            <td>
                {% document_no %}
            </td>
            <td>
                {% item_code %}
            </td>
            <td>
                {% nomor_roll %}
            </td>
            <td>
                {% batch_number %}
            </td>
            <td>
                {% final_result %}
            </td>
            <td>
                {% confirm_by %}
            </td>
            <td>
                {% confirm_date %}
            </td>
            <td>
                {% status %}
            </td>
        </tr>
    {%/item%}
</script>
    