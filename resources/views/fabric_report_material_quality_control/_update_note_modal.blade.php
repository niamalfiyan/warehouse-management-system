<div id="updateNoteModal" class="modal fade">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Remark FIR </h5>
			</div>
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> '#',
						'method' 	=>'post',
						'class' 	=> 'form-horizontal',
						'id' 		=> 'update_note_form'
					])
				!!}
				<div class="modal-body">
				<div class="row">  
					@include('form.textarea', [
						'field' 		=> 'note',
						'label' 		=> 'Remark',
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'placeholder' 	=> 'Note',
						'attributes' 	=> [
							'id' 		=> 'note',
							'rows' 		=> 3,
							'resize' 	=> false
						]
					])

					{!! Form::hidden('id','', array('id' => 'update_note_id')) !!}

				</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
					<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>