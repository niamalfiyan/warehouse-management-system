@extends('layouts.app', ['active' => 'fabric_report_material_quality_control'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Quality Control (FIR)</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('masterDataMaterialPrintBom.index') }}" id="url_master_data_material_print_bom_index">Material Quality Control (FIR)</a></li>
				<li class="active">Import</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<button id="upload_button" class="btn btn-success" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
					{!!
						Form::open([
							'role'		 	=> 'form',
							'url' 		=> route('fabricReportMaterialQualityControl.store'),
							'method' 	=> 'post',
							'id' 		=> 'upload_file_confirmation',
							'enctype' 	=> 'multipart/form-data'
						])
						!!}
							<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
							<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
						{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<td>No</td>
							<td>Supplier Name</td>
							<td>No Invoice</td>
							<td>Document No</td>
							<td>Item Code</td>
							<td>No Roll</td>
							<td>Batch Number</td>
							<td>Final Result</td>
							<td>Confirm By</td>
							<td>Confirm Date</td>
							<td>Status Upload</td>
						</tr>
					</thead>
					<tbody id="tbody_list_confirmation">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','import', array('id' => 'page')) !!}
	{!! Form::hidden('list_confirmation','[]',array('id' => 'list_confirmation')) !!}
@endsection

@section('page-js')
	@include('fabric_report_material_quality_control._item')
	<script src="{{ mix('js/fabric_report_material_quality_control.js') }}"></script>
@endsection
