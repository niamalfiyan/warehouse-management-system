<script type="x-tmpl-mustache" id="defect-code-table">
	{% #item %}
        <tr>
            <td>
                <input type="text" class="form-control input-edit-point-from input-number" id="point_check_from_{% _id %}" data-id="{% _id %}"  value="{% start_point_check %}"></input>
            </td>
            <td>
                <input type="text" class="form-control input-edit-point-to input-number" id="point_check_to_{% _id %}" data-id="{% _id %}"  value="{% end_point_check %}"></input>
            </td>
            <td>
                <input type="text" class="form-control input-edit-point-defect" id="defect_code_{% _id %}" data-id="{% _id %}"  value="{% defect_code %}" ></input>
            </td>
            <td>
                <textarea class="form-control input-edit-remark" style="resize:none" id="remark_{% _id %}" data-id="{% _id %}" value="{% remark %}">{% remark %}</textarea>
            </td>
            <td>
                <input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_1" value="1" data-id="{% _id %}"  {% #is_selected_1 %}checked="checked"{% /is_selected_1 %}>
            </td>
            <td>
                <input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_2" value="2" data-id="{% _id %}"  {% #is_selected_2 %}checked="checked"{% /is_selected_2 %}>
            </td>
            <td>
                <input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_3" value="3" data-id="{% _id %}"  {% #is_selected_3 %}checked="checked"{% /is_selected_3 %}>
            </td>
            <td>
                <input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_4" value="4" data-id="{% _id %}"  {% #is_selected_4 %}checked="checked"{% /is_selected_4 %}>
            </td>
            <td>
                <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
            </td>
        </tr>
    {%/item%}
</script>
