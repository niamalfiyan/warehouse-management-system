
@if($action_view =='action')
    <ul class="icons-list">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-menu9"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                @if (isset($detail))
                    <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
                @endif
                @if (isset($print))
                    <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
                @endif
                @if (isset($note))
                    <li><a href="#" onclick="note('{!! $note !!}')" ><i class="icon-pencil6"></i> Note</a></li>
                @endif
            </ul>
        </li>
    </ul>
@elseif($action_view == 'actual_width')
    @php
        $url = route('fabricReportMaterialQualityControl.dataDetailCuttableWidth',$model->id);
    @endphp
    <a href="#" onClick="showWidth('{{ $url }}')" >{{ $model->actual_width }}</a>
@elseif($action_view == 'final_result')

    {!! Form::hidden('qty_on_roll_'.$model->id,$model->qty_on_barcode, array('id' => 'qty_on_roll_'.$model->id)) !!}
    {!! Form::hidden('formula_1_'.$model->id,$model->total_formula_1, array('id' => 'formula_1_'.$model->id)) !!}
    {!! Form::hidden('formula_2_'.$model->id,$model->total_formula_2, array('id' => 'formula_2_'.$model->id)) !!}

    @php $final_result = $model->final_result; @endphp
    @if($model->confirm_date && ($final_result == 'REJECT' || $final_result == 'SELEKSI PANEL' || $final_result == 'SPESIAL MARKER'))
        <select class="form-control status" disabled style="width: 100px;" id="id_{{ $model->id }}">
            <option value="">Select Result</option>
            <option value="QUARANTINE">Quarantine</option>
            <option value="RELEASE" >Release</option>
            <option value="SELEKSI PANEL" >Seleksi Panel</option>
            <option value="SPESIAL MARKER">Spesial Marker</option>
            <option value="HOLD">Hold</option>
            <option value="REJECT">Reject</option>
        </select>
    @else
        <select class="form-control status" style="width: 100px;" onChange="selectStatus('{{ $model->id }}')" id="id_{{ $model->id }}">
            <option value="">Select Result</option>
            <option value="QUARANTINE">Quarantine</option>
            <option value="RELEASE" >Release</option>
            <option value="SELEKSI PANEL" >Seleksi Panel</option>
            <option value="SPESIAL MARKER">Spesial Marker</option>
            <option value="HOLD">Hold</option>
            <option value="REJECT">Reject</option>
        </select>
    @endif
@endif
