<html>
<head>
	<title>PRINT :: MONITORING RECEIVING</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_mrp.css')) }}">

</head>
<body>
	<div class="outer">
		<div class="header">
			<div class="header-tengah">LAPORAN MONITORING RECEIVING - APPAREL WAREHOUSE MANAGEMENT SYSTEM</div>
		</div>
		<div class="isi">
			<table border="1px" cellspacing="0" style="width: 100%">
				<thead style="background-color: #2196F3;color:black">
					<tr>
						<th class="text-center">#</th>
						<th>RECEIVING DATE</th>
						<th>NO PO SUPPLIER</th>
						<th>NAMA SUPPLIER</th>
						<th>NO PO BUYER</th>
						<th>KODE ITEM</th>
						<th>STYLE</th>
						<th>KATEGORI</th>
						<th>UOM</th>
						<th>QTY</th>
						<th>QC STATUS</th>
						<th>QC CHECKER</th>
						<th>STATUS TERAKHIR</th>
						<th>LOKASI TERAKHIR</th>
						<th>TANGGAL PINDAH TERKAHIR</th>
						<th>PIC RECEIVE</th>
					</tr>
				</thead>
				<tbody>
					@foreach($monitor_receivements as $key => $data)
						@php
							if($data->last_status_movement == 'receiving')
								$_last_status  ='DITERIMA GUDANG';
							elseif($data->last_status_movement == 'in')
								$_last_status  ='MASUK RAK';
							elseif($data->last_status_movement == 'out' || $data->last_status_movement == 'out-handover')
								$_last_status  ='SUPPLIED';	
							elseif($data->last_status_movement == 'reject')
								$_last_status  ='REJECT';	
							elseif($data->last_status_movement == 'change')
								$_last_status  ='PINDAH RAK';
							elseif($data->last_status_movement == 'hold')
								$_last_status  ='DITAHAN QC';
							elseif($data->last_status_movement == 'adjustment')
								$_last_status  ='ADJ. QTY';
							elseif($data->last_status_movement == 'print')
								$_last_status  ='CETAK BARCODE';
							elseif($data->last_status_movement == 'reroute')
								$_last_status  ='BERUBAH PO BUYER';
							elseif($data->last_status_movement == 'cancel item')
								$_last_status  ='CANCEL ITEM';
							elseif($data->last_status_movement == 'cancel order')
								$_last_status  ='CANCEL ORDER';
							elseif($data->last_status_movement == 'check')
								$_last_status  ='PENGECEKAN QC';
							else
								$_last_status  = $data->last_status_movement;
						@endphp
					
						<tr>
							<td>{{ $key+1}}</td>
							<td>{{ $data->created_at }}</td>
							<td>{{ $data->document_no }}</td>
							<td>{{ $data->supplier_name }}</td>
							<td>{{ $data->po_buyer }}</td>
							<td>{{ $data->item_code }}</td>
							<td>{{ $data->style }}</td>
							<td>{{ $data->category }}</td>
							<td>{{ $data->uom_conversion }}</td>
							<td>{{ $data->qty_conversion }}</td>
							<td>{{ $data->qc_status }}</td>
							<td>{{ $data->qc_checker }}</td>
							<td>{{ $_last_status }}</td>
							<td>{{ $data->code }}</td>
							<td>{{ $data->last_movement_date }}</td>
							<td>{{ $data->pic_receive }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
	</div>
</body>
</html>