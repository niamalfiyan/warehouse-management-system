<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
	  		<th>Type Stock</th>
			<th>Po Buyer</th>
			<th>Style</th>
			<th>Article No</th>
			<th>Uom</th>
			<th>Qty Need</th>
			<th>Qty Preparation</th>
			<th>Qty Outstanding</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			@php
				$data 				= $list->qtyTypeStock($list->po_buyer);
				$qty_allocated 		= $list->qtyAllocated($list->po_buyer,$list->item_code,$list->style,$list->article);
				$_qty_outstanding 	= $list->qty_required - $qty_allocated;
				$qty_outstanding 	= ($_qty_outstanding <=0 )? 0 : $_qty_outstanding;
				$type_stock 		= ($data)? $data->type_stock : null;
			@endphp
			<tr>
				<td>{{ $type_stock }}</td>
				<td>{{ $list->po_buyer }}</td>
				<td>{{ $list->style }}</td>
				<td>{{ $list->article_no }}</td>
				<td>{{ $list->uom }}</td>
				<td>{{ number_format($list->qty_required, 4, '.', ',') }}</td>
				<td>{{ $qty_allocated }}</td>
				<td>{{ $qty_outstanding }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->po_buyer }}" data-style="{{ $list->style }}" data-name="{{ $list->po_buyer }}" data-uom="{{ $list->uom }}" 
						data-joborder="{{ $list->article_no }}"  data-qty="{{ $qty_outstanding }}"
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
