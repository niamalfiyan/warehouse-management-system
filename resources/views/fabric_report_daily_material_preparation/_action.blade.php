
 @if (isset($checkbox))
    @if(!$model->is_closing)
        <input type='checkbox' onclick="selectedMoveRoll('{!! $model->id !!}')" class='mycheckbox' id="{!! $model->id !!}">
    @endif
 @else 
    <ul class="icons-list">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="icon-menu9"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                @if (isset($detail))
                    <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
                @endif
                @if (isset($history))
                    <li><a onclick="history('{!! $history !!}')" ><i class="icon-history"></i> History Planning</a></li>
                @endif
                @if(isset($unlock))
                    <li><a onclick="unlock('{!! $unlock !!}')"><i class="icon-unlocked"></i> Unlock</a></li>
                @endif
                @if (isset($remark))
                    <li><a onclick="remark('{!! $remark !!}')" ><i class="icon-pencil6"></i> Remark</a></li>
                @endif
                @if (isset($machine))
                    <li><a onclick="changeMachine('{!! $machine !!}')" ><i class="icon-pencil6"></i> Machine</a></li>
                @endif
                @if(isset($recalculate))
                    <li><a onclick="recalculate('{!! $recalculate !!}')"><i class="icon-calculator3"></i> Recalculate</a></li>
                @endif
                @if (isset($reprepare))
                    <li><a onclick="reprepare('{!! $reprepare !!}')" ><i class="icon-pie-chart7"></i> Reprepare Roll to Buyer</a></li>
                @endif
                @if (isset($print))
                    @if(!$model->is_closing)
                        <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
                    @endif
                @endif
                 @if (isset($cancel))
                    <li><a onclick="cancel('{!! $cancel !!}')"><i class="icon-x"></i> Cancel</a></li>
                @endif
                @if (isset($destroy))
                    <li><a onclick="destroy('{!! $destroy !!}')"><i class="icon-trash"></i> Delete</a></li>
                @endif
               
                @if (isset($close))
                    <li><a onclick="closePrepare('{!! $close !!}')"><i class="glyphicon glyphicon-folder-close"></i> Close</a></li>
                @endif

                @if (isset($deleteClosing))
                    <li><a onclick="deleteClosing('{!! $deleteClosing !!}')"><i class="icon-trash"></i> Delete Closing</a></li>
                @endif
            </ul>
        </li>
    </ul>
 @endif
 

