<div id="changePlanModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
                {!!
                    Form::open([
                        'role'          => 'form',
                        'url'           => '#',
                        'method'        => 'post',
                        'class'         => 'form-horizontal',
                        'id'            => 'store_change_plan'
                    ])
                !!}

			<div class="modal-header bg-blue">
				<h5 class="modal-title">Move Plan </h5>
			</div>
			<div class="modal-body">

                    @include('form.date', [
                        'field' 		=> 'planning_date',
                        'label' 		=> 'Planning Cutting',
                        'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
                        'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
                        'placeholder' 	=> 'dd/mm/yyyy',
                        'class' 		=> 'daterange-single',
                        'attributes'	=> [
                            'id' 			=> 'planning_date',
                            'autocomplete' 	=> 'off',
                            'readonly' 		=> 'readonly'
                        ]
                    ])
            </br>
            </br>
            </br>
            <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="selectAll" class="pull-right">SELECT ALL</button>
					<button type="button" class="btn btn-default hidden" id="unSelectAll" class="pull-right">UNSELECT ALL</button>
			</div>
                {!! Form::hidden('list_detail_material_preparation','', array('id' => 'list_detail_material_preparation')) !!}
					
					<table class="table datatable-basic table-striped table-hover table-responsive" id="change_plan_table">
						<thead>
							<tr>
                                <th>#</th>
								<th>Barcode</th>
								<th>Roll</th>
								<th>Item Code</th>
                                <th>Qty</th>
                                </tr>
						</thead>
					</table>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
                <button type="submit" class="btn btn-primary">Save</button>
			</div>
		</div>
        {{ Form::close() }}
	</div>
</div>
