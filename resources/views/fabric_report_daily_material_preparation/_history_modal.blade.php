
<div id="historyModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_daily_material_preparation_history_table">
						<thead>
							<tr>
								<th width="100%">Created At</th>
								<th width="100%">Remark</th>
								<th width="100%">Qty Before</th>
								<th width="100%">Qty After</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" onClick="closeModal()">Close  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
