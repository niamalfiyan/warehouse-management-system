<script type="x-tmpl-mustache" id="reject-table">
	<tr>
		<td colspan="12">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
	{% #item %}
		<tr>
			<td>
				<div class="row">
					<div class="col-xs-3">
						<div class="panel border-left-lg border-left-info">
							<div class="panel-body">
								<ul class="list list-unstyled">
									<li>
										<div class="row">
											<label class="col-lg-3">Document No</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% document_no %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Supplier Name</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% supplier_name %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">No Invovice</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% no_invoice %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Item Code</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% item_code %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Color</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% color %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Roll Number</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% nomor_roll %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Batch Number</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% batch_number %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Kg</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-number input-kg" id="kg_input_{% _id %}" data-id="{% _id %}" value="{% kg %}">
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Top Width</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-number input-begin-width" id="begin_width_{% _id %}" data-id="{% _id %}" value="{% begin_width %}">
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Middle Width</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-number input-midle-width" id="middle_width_{% _id %}" data-id="{% _id %}" value="{% middle_width %}">
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Bottom Width</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-number input-end-width" id="end_width_{% _id %}" data-id="{% _id %}" value="{% end_width %}">
											</div>
										</div>

									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Actual Width</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-number" readonly="readonly" id="actual_width_{% _id %}" value="{% actual_width %}">
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Qty On Barcode</label>
											<div class="col-lg-9">
												<input type="text" class="form-control" readonly="readonly" value="{% qty %}" >
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<label class="col-lg-3">Actual Yds</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-actual-length input-number" id="actual_length_{% _id %}" data-id="{% _id %}" value="{% actual_length %}">
											</div>
										</div>

									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-9">
						<div class="table-responsive">
							<table class="table table-striped table-hover" style="background-color:transparent">
								<thead>
									<tr>
										<th rowspan="2">Start Point Check</th>
										<th rowspan="2">End Point Check</th>
										<th rowspan="2">Defect Code</th>
										<th rowspan="2">Remark</th>
										<th colspan="4"style="text-align: center">Defect Code Value</th>
										<th rowspan="2">Action</th>
									</tr>
									<tr>
										<th>1</th>
										<th>2</th>
										<th>3</th>
										<th>4</th>
									</tr>
								</thead>
								<tbody>
									{% #detail %}
										<tr>
											<td>
												<input type="text" class="form-control input-edit-point-from input-number" id="point_check_from_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" value="{% point_check_from %}" readonly="readonly"></input>
											</td>
											<td>
												<input type="text" class="form-control input-edit-point-to input-number" id="point_check_to_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" value="{% point_check_to %}" readonly="readonly"></input>
											</td>
											<td>
												<input type="text" class="form-control input-edit-point-defect" id="defect_code_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}" value="{% defect_code %}" ></input>
											</td>
											<td>
												<textarea class="form-control input-edit-remark" style="resize:none" id="remark_{% _id %}_{% _idx %}" data-id="{% _id %}" data-idx="{% _idx %}">{% remark %}</textarea>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_1" value="1" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_1 %}checked="checked"{% /is_selected_1 %}>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_2" value="2" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_2 %}checked="checked"{% /is_selected_2 %}>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_3" value="3" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_3 %}checked="checked"{% /is_selected_3 %}>
											</td>
											<td>
												<input type="radio" class="input-edit-defect-value" name="defect_code_value_{% _id %}_{% _idx %}" id="defect_code_value_{% _id %}_{% _idx %}_4" value="4" data-id="{% _id %}" data-idx="{% _idx %}" {% #is_selected_4 %}checked="checked"{% /is_selected_4 %}>
											</td>
											<td>
												<button type="button" data-id="{% _id %}" data-idx="{% _idx %}" class="btn btn-dafault btn-delete-detail-point"><i class="fa icon-trash"></i></button>
											</td>
										</tr>
									{% /detail %}
									<tr>
										<td>
											<input type="text" class="form-control input-new input-number" id="point_check_from_{% _id %}" data-id="{% _id %}" ></input>
										</td>
										<td>
											<input type="text" class="form-control input-new input-number" id="point_check_to_{% _id %}" data-id="{% _id %}"></input>
										</td>
										<td>
											<input type="text" class="form-control input-new input-defect-code" id="defect_code_{% _id %}" data-id="{% _id %}"></input>
										</td>
										<td>
											<textarea class="form-control input-new input-remark" style="resize:none" id="remark_{% _id %}" data-id="{% _id %}"></textarea>
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_1" value="1" data-id="{% _id %}">
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_2" value="2" data-id="{% _id %}">
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_3" value="3" data-id="{% _id %}">
										</td>
										<td>
											<input type="radio" name="defect_code_value_{% _id %}" id="defect_code_value_{% _id %}_4" value="4" data-id="{% _id %}">
										</td>
										<td>
											<button type="button" data-id="{% _id %}" class="btn btn-blue-success btn-add-detail-point"><i class="icon-plus-circle2"></i></button>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<th>Point</th>
										<td><input type="text" class="form-control" readonly="readonly" id="point_1_{% _id %}" value="{% point_1 %}"></td>
										<td><input type="text" class="form-control" readonly="readonly" id="point_2_{% _id %}" value="{% point_2 %}"></td>
										<td><input type="text" class="form-control" readonly="readonly" id="point_3_{% _id %}" value="{% point_3 %}"></td>
										<td><input type="text" class="form-control" readonly="readonly" id="point_4_{% _id %}" value="{% point_4 %}"></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<th>Total Point</th>
										<td colspan="4"><input type="text" class="form-control" readonly="readonly" id="total_point{% _id %}" value="{% total_all_point %}" data-id="{% _id %}"></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<th>Percentage(%)</th>
										<td colspan="4"><input type="text" class="form-control" readonly="readonly" id="percentage_{% _id %}" value="{% percentage %}" data-id="{% _id %}"></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<th>Result</th>
										<td colspan="4">
											<select class="form-control result-status" id="status_option_{% _id %}" data-id="{% _id %}">
												{% #status_options %}
													<option value="{% id %}" {% selected %}>{% name %}</option>
												{% /status_options %}
											</select>

										</td>
									</tr>
									<tr>
										<td></td>
										<td></td>
										<th>Information! Jika persentage kurang dari 15% silahkan click tombol release pada bagian kanan atas. </th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group text-right">
						<button id="remove_row_{% _id %}" data-id="{% _id %}" class="btn btn-yellow-cancel btn-lg col-xs-12 btn-remove-row">REMOVE <i class="fa icon-trash position-left"></i></button>
					</div>
				</div>
			</td>
		</tr>
	{%/item%}
	
</script>
