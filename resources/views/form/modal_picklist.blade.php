<div id="{{ $name }}Modal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">{{ $title }}</h5>
			</div>

			<div class="modal-body">
				<fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="{{ $name }}Search"  class="form-control" placeholder="Please type a keywords..." autofocus="true">
								<span class="input-group-btn">
									<button class="btn btn-yellow-cancel legitRipple" type="button" id="{{ $name }}ButtonSrc"> <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
								</span>
							</div>
							<span class="help-block">{{ $placeholder }}</span>
								
						</div>
					</div>
				</fieldset>
				<br/>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/images/ajax-loader.gif">
					</div>
				</div>
				<div class="table table-striped table-hover table-responsive" id="{{ $name }}Table"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
