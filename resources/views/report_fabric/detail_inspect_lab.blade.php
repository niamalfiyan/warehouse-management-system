@extends('layouts.app', ['active' => 'inspect_lab_fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">DETAIL INSPECT LAB</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li><a href="{{ route('report.inspectLab') }}">Inspect Lab</a></li>
							<li class="active">Detail</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian Lab, untuk melihat detail laporan dari hasil inspect lab.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-4">
					<p>Document No : <b>{{$obj_header->document_no}}</b></p>
					<p>Item Code :  <b>{{$obj_header->item_code}}</b></p>
					<p>Date Receive :  <b>{{$obj_header->receive_date}}</b></p>
				</div>
				<div class="col-md-4">
					<p>Supplier Name :  <b>{{$obj_header->supplier_name}}</b></p>
					<p>Color :  <b>{{$obj_header->color}}</b></p>
					<p>Total All Roll :  <b>{{$obj_header->total_roll_all}} </b></p>
					<p>Total Inspect Roll :  <b>{{$obj_header->total_roll_inspect}}</b></p>

				</div>
				<div class="col-md-4">
					<p>No Invoice :  <b>{{$obj_header->no_invoice}}</b></p>
					<p>Style : <b>{{$obj_header->style}}</b></p> 
					<p>Total Qty All Roll :  <b>{{$obj_header->total_qty_roll_all}} ({{ $obj_header->uom_roll_all }})</b></p>
					<p>Total Qty Inspect Roll :  <b>{{$obj_header->total_qty_roll_inspect}} ({{ $obj_header->uom_roll_inspect }})</b></p>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
					<div class="from-group text-right">
						<a class="btn btn-success"
						href="{{route('report.detailInspectLabExport',[
						'c_bpartner_id'=>$obj_header->c_bpartner_id,
						'document_no' => $obj_header->document_no,
						'item_code' => $obj_header->item_code,
						'color' => $obj_header->color,
						'style' => $obj_header->style,
						'no_invoice' => $obj_header->no_invoice,
						'receive_date' => $obj_header->receive_date,
						'warehouse_id' => $obj_header->warehouse_id,
						'total_roll_all'=>$obj_header->total_roll_all,
						'total_qty_roll_all'=>$obj_header->total_qty_roll_all,
						'uom_roll_all'=>$obj_header->uom_roll_all,
						'total_roll_inspect'=>$obj_header->total_roll_inspect,
						'total_qty_roll_inspect'=>$obj_header->total_qty_roll_inspect,
						'uom_roll_inspect'=>$obj_header->uom_roll_inspect,
						'monitoring_receiving_fabric_id'=>$obj_header->monitoring_receiving_fabric_id,
						'supplier_name' => $obj_header->supplier_name]
						)}}"
						class="btn btn-default">
						Print <i class="icon-file-excel position-left"></i>
					</a>
					</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>BARCODE</th>
							<th>INSPECT LAB DATE</th>
							<th>NOMOR ROLL</th>
							<th>LOCATION</th>
							<th>BATCH NUMBER</th>
							<th>LOT ACTUAL</th>
							<th>QTY</th>
							<th>USER INSPECT</th>
							<th>REMARK</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($details as $key => $detail)
							<tr @if(!$detail->is_already_inspect)class="warning"@endif>
								<td>{{ $key+1 }}</td>
								<td>{{ $detail->barcode_supplier }}</td>
								<td>{{ $detail->inspect_lab_date }}</td>
								<td>{{ $detail->nomor_roll }}</td>
								<td>{{ $detail->location }}</td>
								<td>{{ $detail->batch_number }}</td>
								<td>{{ $detail->load_actual }}</td>
								<td>{{ $detail->stock }} ({{ $detail->uom }})</td>
								<td>{{ $detail->user_lab }}</td>
								<td>{{ $detail->inspect_lab_remark }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
