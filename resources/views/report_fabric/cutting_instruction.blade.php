@extends('layouts.app', ['active' => 'cutting_instruction_fabric'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">CUTTING INSTRUCTION REPORT</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li class="active">Cutting Instruction</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
					{!!
						Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.cuttingInstruction'),
							'method' => 'get',
							'id' => 'form-filter-planning'
						))
					!!}
					@include('form.date', [
						'field' => 'planning_date',
						'label' => 'PCD',
						'class' => ' datepicker',
						'placeholder' => 'dd/mm/yyyy',
						'label_col' => 'col-md-4',
						'form_col' => 'col-sm-8',
						'attributes' => [
							'id' => 'planning_date',
							'autocomplete' => 'off'
						]
					])
					
				{!! Form::close() !!}
			</div>
		</div>
		
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table ci-databale-basic','id' => 'ci-datatables']) !!}
			</div>
		</div>
@endsection

@section('content-js')
{!! $html->scripts() !!}
<script>
$('#planning_date').on('change',submitPlanningDate);
function submitPlanningDate(){
	$('#form-filter-planning').submit();
}

$(function(){
    var dtable = $('#ci-datatables').dataTable().api();
    dtable.page.len(100);
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
})
</script>
@endsection
