@extends('layouts.app', ['active' => 'preparation_fabric_report'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">DETAIL PREPARATION FABRIC REPORT</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
                    <li><a href="{{ route('report.preparationFabric') }}">Preparation Fabric</a></li>
                    <li class="active">Detail</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
				<a href="{{ route('report.preparationFabricDetailExcel',$preparation_id) }}" class="btn btn-default">Export Detail All <i class="icon-file-excel position-left"></i></a>
			</div>
		</div>
		
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=> 'preparation-datatable']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script>
	$('#start_date').on('change',submitFilter);
	$('#end_date').on('change',submitFilter);

	function submitFilter(){
		$('#form_filter').submit();
	}
	$(function(){
		var dtable = $('#preparation-datatable').dataTable().api();
		dtable.page.len(100);
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();
	})
	</script>
@endsection
