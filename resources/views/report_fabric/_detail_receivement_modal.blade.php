
<div id="detailModal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>

			<div class="modal-body">
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/img/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="detailTable"></div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">CLOSE  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
