@extends('layouts.app', ['active' => 'inspect_lab_fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.fabric.inspectQc') }}"></a><span class="text-semibold">INSPECT LAB</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li >Report</li>
							<li><a href="{{ route('report.fabric.inspectQc',[
								'daily_date'=>$inspect_date,
								'active_tab_daily'=>true
							]) }}">Summary Inspect Qc</a></li>
							<li class="active">Detail Inspect Qc</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian Inspect QC, untuk melihat data material yang discan oleh <code>{{$user_name}}</code> untuk di jadikan bahan sampling. 
				</div>
				
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'inspect-lab-datatables']) !!}
			</div>
		</div>
	</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/inspect_lab_fabric.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script>
	</script>
@endsection
