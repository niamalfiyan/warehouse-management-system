@extends('layouts.app')

@section('content')
<div class="page-header">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">QTY REJECT</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<a href="#" id="url_index" class="hidden"></a>
							<li><a href="{{ route('dashboard') }}">Report</a></li>
							<li class="active">Qty Reject</li>
                        </ul>
					</div>
			    </div>
		    </div>
	    </div>
    </div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
		<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
		<div class="heading-elements">
		</div>
	</div>
    <div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-show-all table-striped']) !!}
			</div>
        </div>
	</div>
@endsection

@section('content-js')
{!! $html->scripts() !!}

<script>
function confirm(url){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		
		$.ajax({
			type: "get",
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function (response) {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
			}
		}).done(function ($result) {
			$('#dataTableBuilder').DataTable().ajax.reload();
			$("#alert_success").trigger("click", 'Confirm Qty Reject Berhasil');
		});
	}
</script>

@endsection