@extends('layouts.app', ['active' => 'receivement-fabric'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">Material Receive</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">Material Receive</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat laporan kedatangan material.
				</div>
			</div>
		</div>
	</div>


	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#basic-tab1" data-toggle="tab" aria-expanded="true">List Data</a></li>
					<li class=""><a href="#basic-tab2" data-toggle="tab" aria-expanded="false">Export to Excel</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="basic-tab1">
						<br/>
						<div class="table-responsive">
							{!! $html->table(['class'=>'table datatable-basic','id'=>'receivement-datatable']) !!}
						</div>
					</div>

					<div class="tab-pane" id="basic-tab2">
						<br/>
						{!!
							Form::open(array(
								'class' => 'form-signin',
								'role' => 'form',
								'url' => route('report.exportReceivementReport'),
								'method' => 'get',
								#'id' => 'form'
							))
						!!}
						@include('form.date', [
							'field' => 'start_date',
							'label' => 'Start Date (00:00)',
							'class' => ' datepicker',
							'placeholder' => 'dd/mm/yyyy',
							'label_col' => 'col-md-4',
							'form_col' => 'col-sm-8',
							'attributes' => [
								'id' => 'start_date'
							]
						])
						
						@include('form.date', [
							'field' => 'end_date',
							'label' => 'End Date (23:59)',
							'class' => ' datepicker',
							'placeholder' => 'dd/mm/yyyy',
							'label_col' => 'col-md-4',
							'form_col' => 'col-sm-8',
							'attributes' => [
								'id' => 'end_date'
							]
						])
						<div class="from-group text-right">
							<button type="submit" class="btn btn-info btn-md" id="exportExcel" style="margin-top:15px">Export Excel <i class="icon-file-excel position-left"></i></button>
						</div>
						{!! Form::close() !!}
					</div>

					
				</div>
			</div>
		</div>
	</div>
@endsection


@section('content-modal')
	@include('report_fabric._detail_receivement_modal')
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_receivement_fabric.js'))}}"></script>
@endsection
