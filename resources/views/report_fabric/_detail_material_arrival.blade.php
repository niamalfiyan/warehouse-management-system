<table class="table">
	<thead>
	  <tr>
			<th>NO</th>
			<th>BARCODE SUPPLIER</th>
			<th>RECEIVE DATE</th>
			<th>BATCH NUMBER</th>
			<th>NOMOR ROLL</th>
			<th>QTY ROLL</th>
	</tr>
	</thead>
	
	<tbody>
		@foreach ($items as $key => $item)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ $item->barcode_supplier }}
				</td>
				<td>
					{{ $item->created_at->format('d/m/Y H:m:s') }}
				</td>
				<td>
					{{ $item->batch_number }}
				</td>
				<td>
					{{ $item->nomor_roll }}
				</td>
				<td>
					{{ $item->qty_upload }}
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $items->appends(Request::except('page'))->render() !!}
