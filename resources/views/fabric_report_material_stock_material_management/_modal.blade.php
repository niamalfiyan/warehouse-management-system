<div id="detailModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg" style="width:75%">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">History stock for <span id="histori_stock"></span></h5>
			</div>

			<div class="modal-body">
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/images/ajax-loader.gif">
					</div>
				</div>
				<div class="tabbable tab-content-bordered" id="tab" class="hidden">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#stock-origin" data-toggle="tab" aria-expanded="false" >Stock Origin</a></li>
						<li><a href="#allocation-perpo" data-toggle="tab" aria-expanded="false" >Allocation Per Pobuyer</a></li>
						<li><a href="#allocation-nonpo" data-toggle="tab" aria-expanded="true">Allocation Non Pobuyer</a></li>
						<li><a href="#non-allocation" data-toggle="tab" aria-expanded="true">Movement History</a></li>
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="stock-origin">
							<br/>
							<div class="table-responsive" id="sourceStockTable"></div>
						</div>
						<div class="tab-pane" id="allocation-perpo">
							<br/>
							<fieldset>
								<div class="form-group">
									<div class="col-xs-12">
										<div class="input-group">
											<input type="text" id="detailTableSearch"  class="form-control" placeholder="Input Keyword..." autofocus="true">
											<span class="input-group-btn">
												<button class="btn btn-yellow-cancel" type="button" id="detailTableButtonSrc">Search</button>
											</span>
										</div>
										<span class="help-block">Search based on po buyer</span>
											
									</div>
								</div>
							</fieldset>
							<br/>
							<div class="shade-screen-2" style="text-align: center;">
								<div class="shade-screen-2 hidden">
									<img src="/images/ajax-loader.gif">
								</div>
							</div>
							<div class="table-responsive" id="detailTable"></div>
						</div>
						<div class="tab-pane" id="allocation-nonpo">
							<br/>
							<div class="table-responsive" id="detailNonPoTable"></div>
						</div>
						<div class="tab-pane" id="non-allocation">
							<br/>
							<div class="table-responsive" id="detailNonAllocartinTable"></div>
						</div>
					</div>
				</div>
				
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
