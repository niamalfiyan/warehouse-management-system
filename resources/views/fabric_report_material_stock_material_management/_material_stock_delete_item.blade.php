<script type="x-tmpl-mustache" id="delete-bulk-table">
	{% #item %}
		<tr {% #error_upload %} style="background-color:#FF5722;color:#fff" {% /error_upload %}>
			<td>{% no %}</td>
			<td>{% supplier_name %}</td>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% qty_available %} ({% uom %})</td>
			<td>{% reason %}</td>
			<td>{% warehouse_name %}</td>
			<td>{% remark %}</td>
		</tr>
	{%/item%}
</script>
