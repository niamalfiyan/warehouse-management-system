@extends('layouts.app', ['active' => 'accessories_barcode_reroute'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Reroute</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Reroute</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@role(['admin-ict-acc'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-sm-12',
					'form_col' 			=> 'col-sm-12',
					'options' 			=> [
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole

			@include('form.picklist', [
				'label' 		=> 'Po Buyer',
				'field' 		=> 'po_buyer_reroute',
				'name' 			=> 'po_buyer_reroute',
				'placeholder' 	=> 'Please Select PO Buyer',
				'mandatory' 	=> '*Required',
				'readonly'		=> true,
				'label_col' 	=> 'col-sm-12',
				'form_col' 		=> 'col-sm-12',
			])
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
				<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					<div class="heading-btn">
						<button id="btn_check_all" class="btn btn-yellow-cancel" data-check="false">Check All <i class="icon-checkbox-checked position-left"></i></button>
						<button type="button" id="btn_uncheck_all" class="btn btn-default">Uncheck All <i class=" icon-checkbox-unchecked position-left"></i></button>
					</div>
				</div>
			</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Po Supplier</th>
							<th>Old Po Buyer</th>
							<th>New Po Buyer</th>
							<th>Item Code</th>
							<th>Style</th>
							<th>Article</th>
							<th>Qty On Barcode</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-print-barcode"></tbody>
				</table>
			</div>
			<br/>
			<div class="form-group text-right">
				{!!
					Form::open([
						'role' 		=> 'form',
						'url' 		=> route('accessoriesBarcodeReRoute.printout'),
						'method' 	=> 'post',
						'id' 		=> 'from_printout',
						'enctype' 	=> 'multipart/form-data',
						'target'	=> '_blank'
					])
				!!}
					{!! Form::hidden('print_barcode','[]',array('id' => 'print-barcode')) !!}
					{!! Form::hidden('url_get_item',route('accessoriesBarcodeReRoute.itemPicklist'),array('id' => 'url_get_item')) !!}
					<button id="btn_print" type="button" class="btn btn-blue-success btn-lg col-xs-12">Print <i class="icon-printer position-left"></i></button>
				{!! Form::close() !!}
			
			</div>
		</div>
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'po_buyer_reroute',
		'title' 		=> 'Filter PO Buyer',
		'placeholder' 	=> 'Search based on po buyer'
	])
@endsection

@section('page-js')
	@include('accessories_barcode_reroute._item')
	<script src="{{ mix('js/accessories_barcode_reroute.js') }}"></script>
@endsection
