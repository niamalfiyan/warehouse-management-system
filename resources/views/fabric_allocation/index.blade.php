@extends('layouts.app', ['active' => 'fabric_allocation'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Allocation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li class="active">Allocation</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('fabricAllocation.export') }}"><i class="icon-file-download2 pull-right"></i> Export All</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection


@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>

	<div class="panel-body">
		@include('form.date', [
			'field' 		=> 'start_date',
			'label' 		=> 'LC Date From (00:00)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes'	=> [
				'id' 			=> 'start_date',
				'autocomplete' 	=> 'off',
				'readonly' 		=> 'readonly'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'end_date',
			'label' 		=> 'LC Date To (23:59)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes' 	=> [
				'id' 			=> 'end_date',
				'readonly' 		=> 'readonly',
				'autocomplete' 	=> 'off'
			]
		])
	</div>
</div>

<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
	<ul class="nav navbar-nav visible-xs-block">
		<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
	</ul>

	<div class="navbar-collapse collapse" id="navbar-filter">
		<ul class="nav navbar-nav">
			<li class="active"><a href="#reguler" data-toggle="tab" onclick="changeTab('reguler')">Reguler <i class="icon-stack3 position-left"></i></a></li>
			<li><a href="#additional" data-toggle="tab" onclick="changeTab('additional')">Additional <i class="icon-stack2 position-left"></i></a></li>
		</ul>
	</div>
</div>

<div class="tabbable">
	<div class="tab-content">
		<div class="tab-pane fade in active" id="reguler">
			<div class="panel panel-default border-grey">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table datatable-basic table-striped table-hover" id="reguler_table">
							<thead>
								<tr>
									<th>ALLOCATION ITEM ID</th>
									<th>AUTO ALLOCATION ID</th>
									<th>Lc Date</th>
									<th>Style</th>
									<th>Article No</th>
									<th>Planning Date</th>
									<th>Po Buyer</th>
									<th>Warehouse Inventory</th>
									<th>Item Code</th>
									<th>Item Code Source</th>
									<th>Uom</th>
									<th>Qty Need</th>
									<th>Qty Booking</th>
									<th>Qty Plan</th>
									<th>Lot</th>
									<th>Po Supplier</th>
									<th>Supplier Name</th>
									<th>Item Desc</th>
									<th>Category</th>
									<th>Allocated By</th>
									<th>Warehouse Inventory Id</th>
									<th>Created At</th>
									<th>Remark</th>
									<th>Remark Additional</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>

		</div>

		<div class="tab-pane fade" id="additional">
			<div class="panel panel-default border-grey">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table datatable-basic table-striped table-hover table-responsive" id="additional_table">
							<thead>
								<tr>
								<th>ALLOCATION ITEM ID</th>
									<th>AUTO ALLOCATION ID</th>
									<th>Lc Date</th>
									<th>Style</th>
									<th>Article No</th>
									<th>Planning Date</th>
									<th>Po Buyer</th>
									<th>Warehouse Inventory</th>
									<th>Item Code</th>
									<th>Item Code Source</th>
									<th>Uom</th>
									<th>Qty Need</th>
									<th>Qty Booking</th>
									<th>Qty Plan</th>
									<th>Lot</th>
									<th>Po Supplier</th>
									<th>Supplier Name</th>
									<th>Item Desc</th>
									<th>Category</th>
									<th>Allocated By</th>
									<th>Warehouse Inventory Id</th>
									<th>Created At</th>
									<th>Remark</th>
									<th>Remark Additional</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_allocation.js') }}"></script>
@endsection