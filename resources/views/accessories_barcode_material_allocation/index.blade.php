@extends('layouts.app', ['active' => 'accessories_barcode_material_allocation'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Allocation</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Allocation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesBarcodeMaterialAllocation.export') }}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			@role(['admin-ict-acc'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-sm-12',
					'form_col' 			=> 'col-sm-12',
					'options' 			=> [
						'' 				=> '-- Select Warehouse --',
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole

			<div class="col-sm-6">
				@include('form.picklist', [
					'label' 		=> 'Po Buyer',
					'field' 		=> 'po_buyer_print_material_preparation',
					'name' 			=> 'po_buyer_print_material_preparation',
					'placeholder' 	=> 'Please Select PO Buyer',
					'mandatory' 	=> '*Required',
					'readonly'		=> true,
					'label_col' 	=> 'col-sm-12',
					'form_col' 		=> 'col-sm-12',
				])
			</div>
			<div class="col-sm-6">
				@include('form.picklist', [
					'label' 		=> 'Item Code',
					'field' 		=> 'item_code_print_material_preparation',
					'name' 			=> 'item_code_print_material_preparation',
					'placeholder' 	=> 'Please Select Item Code',
					'mandatory' 	=> '*Required',
					'readonly'		=> true,
					'label_col' 	=> 'col-sm-12',
					'form_col' 		=> 'col-sm-12',
				])
			</div>
			<button type="button" class="btn btn-blue-success col-xs-12 hidden" id="searchcriteria" >Search <i class="icon-search4 position-right"></i></button>
		</div>
	</div>
		
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<button type="button" id="btn_check_all" class="btn btn-yellow-cancel">Check All <i class="icon-checkbox-checked position-left"></i></button>
					<button type="button" id="btn_uncheck_all" class="btn btn-default">Uncheck All <i class=" icon-checkbox-unchecked position-left"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>No.</th>
							<th>Po Supplier</th>
							<th>Po Buyer</th>
							<th>Item</th>
							<th>Style</th>
							<th>Article</th>
							<th>Job Order</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Note</th>
							<th>Is Additional</th>
						</tr>
					</thead>
					<tbody id="tbody-allocation-preparation">
					</tbody>
				</table>
			</div>
			<br/>
			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('accessoriesBarcodeMaterialAllocation.store'),
					'method' 	=> 'post',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
			<a href="{{ route('accessoriesBarcodeMaterialAllocation.create') }}" id="url_material_allocation_list" class="hidden"></a>
			{!! Form::hidden('material_allocation_preparations','[]' , array('id' => 'material_allocation_preparations')) !!}
			{!! Form::hidden('style',null , array('id' => 'style')) !!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			<div class="form-group text-right">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Print <i class="icon-printer position-left"></i></button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
		
	<div class="alert alert-info alert-styled-right alert-bordered hidden" id="alert-duplicate">
		<span class="text-semibold">Info for you !</span> <span id="alert-duplicate-text">
	</div>


	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesBarcodeMaterialAllocation.import'),
			'method' 	=> 'post',
			'id' 		=> 'upload_file_allocation',
			'enctype' 	=> 'multipart/form-data'
		])
	!!}
		{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => '_warehouse_id')) !!}
		<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
		<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
	{!! Form::close() !!}

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesBarcodeMaterialAllocation.print'),
			'method' 	=> 'get',
			'class' 	=> 'form-horizontal',
			'id'		=> 'getBarcode',
			'target' 	=> '_blank'
		])
	!!}
		{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
		{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => '__warehouse_id')) !!}
	{!! Form::close() !!}

@endsection

@section('page-modal')
	@include('accessories_barcode_material_allocation._picklist', [
		'name' 			=> 'po_buyer_print_material_preparation',
		'title' 		=> 'List Po Buyer',
		'placeholder' 	=> 'Search based on po buyer',
	])	
	@include('accessories_barcode_material_allocation._picklist', [
		'name' 			=> 'item_code_print_material_preparation',
		'title' 		=> 'List Item Code',
		'placeholder' 	=> 'Search based on item code',
	])	
@endsection

@section('page-js')
	@include('accessories_barcode_material_allocation._item')
	<script src="{{ mix('js/accessories_barcode_material_allocation.js') }}"></script>
@endsection
