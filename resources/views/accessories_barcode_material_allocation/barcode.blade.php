<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: ALLOCATION</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_wpo.css')) }}">

</head>
    <body>
		@php $flag = 0 ; @endphp
		@foreach ($items as $key => $value)
			@for ($i = 0; $i < $value->total_carton ; $i++)
				@if (($flag+1) % 3 === 0)
					<p style="page-break-after: always;">&nbsp;</p>
				@endif

				@php 
					if($value->is_additional) $type_header = 'BARCODE ADDITIONAL';
					else $type_header = 'BARCODE ALLOCATION';

					if($value->reprint_counter && $value->reprint_counter > 0) $title = 'REPRINT '.$type_header.' (REPRINT KE - '.$value->reprint_counter.')';
					else $title = $type_header;

					$get_bom = $value->dataRequirement($value->po_buyer,$value->item_code,$value->style,$value->article_no);
					$season = ($get_bom)? $get_bom->season : '-';
					$qty_need = ($get_bom)? $get_bom->qty_required : '0';
					$uom = ($get_bom)? $get_bom->uom : '-';
					$is_recycle = $value->getRecycle($value->item_id);
				@endphp
				
				<div class="outer" style="height:8.95cm;">
					<div class="title">{{ $title }}</div>

					<div class="isi" style="height:7.9cm;">
						<div class="isi1" style="padding-top:0;">
							<div class="isi3" style="height:31px;float:left;">
									NO CARTON : <span style="font-size: 24px">{{ $i+1 }}</span> / {{ $value->total_carton }}
							</div>
							<div class="isi2" style="height:31px;width:40.37%;display:block;">
							<span style="font-size: 12px">SEASON {{ $season }}</span> 
							</div>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_code)>40) font-size: 12px @endif">{{ $value->item_code.$is_recycle }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_desc)>40) font-size: 12px @endif">{{ $value->item_desc }}</span>
						</div>

						<div class="isi2" style="height: 193px;">
							<div class="block-kiri" style="justify-content: center; height: 60px;">
								@php
									$conversion = $value->reconversion($value->item_code,$value->category,$value->uom_conversion,$value->material_stock_id);
									if($conversion){
										$multiplyrate = $conversion->multiplyrate;
										$uom_reconversion = $conversion->uom_from;
									}else{
										$multiplyrate = 1;
										$uom_reconversion = $value->uom_conversion;
									}
									$qty_reconversion = $multiplyrate * $value->qty_conversion;
									$is_po_cancel	  = $value->checkPoCancel($value->po_buyer);

								@endphp
								<span style="font-size: 12px; word-wrap: break-word;">{{ sprintf("%0.4f",$value->qty_conversion) }} {{ strtoupper($value->uom_conversion) }} <br/> ({{ number_format($qty_reconversion, 2, '.', ',') }} {{ strtoupper($uom_reconversion) }})</span>
								<br/><span style="font-size: 12px; word-wrap: break-word;">{{ sprintf("%0.4f",$qty_need) }} {{ strtoupper($uom) }} <br/> (NEED)</span>
							</div>
							<div class="block-kiri" style="height:45px;">
								<span style="font-size: 10px; word-wrap: break-word;">
									{{ $value->document_no }}
									<br/>{{ $value->supplier_name }}
								</span>
							</div>
							<div class="block-kiri" style="height:10px;">
								<span style="font-size: 9px; word-wrap: break-word;">Article No : {{ $value->article_no }}<br/>Style : {{ $value->style }}</span>
							</div>
						</div>
						<div class="isi3" style="height: 193px;">
							<div class="block-kiri" style="height: 120px;">
								@if($value->qc_status ==  'FULL REJECT')
									<span>BARCODE TIDAK DAPAT DIGUNAKAN KARNA SUDAH DI REJECT SEMUA OLEH QC</span>
								@else 
									@if ($value->total_carton == 1)
										<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$value->barcode", 'C128') }}" alt="barcode"   />
										<br/>
										<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $value->barcode }}</span>
									@else
										@php
											$_barcode = $value->barcode.'-'.($i+1);
										@endphp

										<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />
										<br/>
										<span style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</span>
									@endif
								@endif
							</div>
							<div class="block-kiri" style="height: 29px;" >
								PO BUYER: <span style="font-size: 16px">
								@if($is_po_cancel)^@endif {{ $value->po_buyer }} @if($value->is_additional)*@endif
								</span>
								<hr/>
								@php($locator = $value->getLocator($value->material_stock_id))
								<span style="font-size: 10px; word-wrap: break-word;">
									LOCATION : {{ $locator }}
								</span>
							</div>
							
						</div>
					</div>
				</div>
				@php $flag++;@endphp
			@endfor
		@endforeach

    </body>
</html>
