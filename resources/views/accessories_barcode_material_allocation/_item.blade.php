<script type="x-tmpl-mustache" id="allocation-preparation-table">
	{% #item %}
		<tr>
			<td>
				{% no %} 
				<input type="checkbox" class="checkbox_item" id="check_{% _id %}" data-id="{% _id %}" {% #selected %} checked="checked" {% /selected %}>
			</td>
			<td>{% document_no %}</td>
			<td>{% show_po_buyer %}</td>
			<td>{% item_code %}</td>
			<td>{% style %}</td>
			<td>{% article %}</td>
			<td>{% job_order %}</td>
			<td>{% uom_conversion %}</td>
			<td>{% qty_conversion %}</td>
			<td>{% note %}</td>
			<td>{% #is_additional %}
					TRUE
				{% /is_additional %}
			</td>
		</tr>
	{%/item%}
</script>