@extends('layouts.app', ['active' => 'master_data_reroute_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Reroute Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataReroute.index') }}">Reroute Buyer</a></li>
				<li class="active">Import</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp </h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="btn btn-primary btn-icon" href="{{ route('masterDataReroute.downloadFromImport') }}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
					{!!
						Form::open([
							'role' 	=> 'form',
							'url' 	=> route('masterDataReroute.uploadFromImport'),
							'method' 	=> 'post',
							'id' 		=> 'upload_file_reroute',
							'enctype' 	=> 'multipart/form-data'
						])
					!!}
							<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
							<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
					{!! Form::close() !!}
				
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>NO</th>
							<th>Old Po Buyer</th>
							<th>New Po Buyer</th>
							<th>Is Prefix</th>
              				<th>Note</th>
							<th>Result</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-reroute">
					</tbody>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','import', array('id' => 'page')) !!}
	{!! Form::hidden('data_reroutes','[]' , array('id' => 'data_reroutes')) !!}
@endsection

@section('page-js')
	@include('master_data_reroute_buyer._item')
	<script src="{{ mix('js/master_data_reroute_buyer.js') }}"></script>
@endsection
