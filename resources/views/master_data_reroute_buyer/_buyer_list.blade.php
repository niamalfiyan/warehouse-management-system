<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Lc Date</th>
			<th>Season</th>
			<th>Po Buyer</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->lc_date->format('d/M/y') }}</td>
				<td>{{ $list->season }}</td>
				<td>{{ $list->po_buyer }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->po_buyer }}" 
						data-name="{{ $list->po_buyer }}" 
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
