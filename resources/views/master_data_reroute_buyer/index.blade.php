@extends('layouts.app', ['active' => 'master_data_reroute_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Reroute Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Reroute Buyer</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('masterDataReroute.import') }}"><i class="icon-file-excel pull-right"></i> Import</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		{!!
			Form::open([
				'role' 		=> 'form',
				'url' 		=> route('masterDataReroute.store'),
				'method'	=> 'post',
				'enctype' 	=> 'multipart/form-data',
				'class' 	=> 'form-horizontal',
				'id'		=> 'form'
			])
		!!}

		<div class="panel-body">
			<div class="row">
				<div class="col-md-6">
					@include('form.picklist', [
						'field' 		=> 'old_buyer',
						'name' 			=> 'old_buyer',
						'div_class' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'placeholder' 	=> 'Searching Old PO Buyer',
						'mandatory' 	=> '*Required'
					])
				</div>
				<div class="col-md-6">
					@include('form.picklist', [
						'field' 		=> 'new_buyer',
						'name' 			=> 'new_buyer',
						'div_class' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'placeholder' 	=> 'Searching New PO Buyer',
						'mandatory' 	=> '*Required'
					])

					@include('form.checkbox', [
						'field' 		=> 'is_prefix',
						'label' 		=> 'Change Prefix',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'style_checkbox' => 'checkbox checkbox-switchery',
						'class' 		=> 'switchery',
						'attributes' 	=> [
							'id' 		=> 'is_prefix'
						]
					])
				</div>
			</div>
			
			@include('form.textarea', [
				'field' 		=> 'note',
				'label_col' 	=> 'col-xs-12',
				'form_col' 		=> 'col-xs-12',
				'label' 		=> 'Note',
				'placeholder' 	=> 'Please input note here',
				'attributes' => [
					'rows' =>'3'
				]
			])

			<button type="submit" class="btn btn-blue-success col-xs-12" >Save <i class="icon-floppy-disk position-left"></i></button>
		</div>

		{!! Form::close() !!}
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="master_data_reroute_buyer">
					<thead>
						<tr>
							<th>id</th>
							<th width="25%">Old Po Buyer</th>
							<th width="25%">New Po Buyer</th>
							<th width="50%">Note</th>
							<th>Prefix</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

		{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'old_buyer',
		'title' => 'List Of PO Buyer',
		'placeholder' => 'Search based on old po buyer',
	])

	@include('form.modal_picklist', [
		'name' => 'new_buyer',
		'title' => 'List Of PO Buyer',
		'placeholder' => 'Search based on new po buyer',
	])
@endsection

@section('page-js')
	<script src="{{ mix('js/switch.js') }}"></script>
	<script src="{{ mix('js/master_data_reroute_buyer.js') }}"></script>
@endsection
