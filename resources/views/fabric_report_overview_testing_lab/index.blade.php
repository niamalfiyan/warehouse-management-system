@extends('layouts.app', ['active' => 'report_overview_testing_lab'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Overview Testing Lab</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Material Testing Lab</li>
				<li class="active">Report Overview Testing</li>
			</ul>
		
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<input type="hidden" name="url_data" id="url_data" value ="{{ route('reportOverviewTestingLab.data') }}" class="form-control" >
								
				<table class="table datatable-basic table-striped table-hover table-responsive" id="master_data_fabric_testing_table">
					<thead>
						<tr>
							<th>Date Submit</th>
							<th>Test Required</th>
							<th>Date Information</th>
							<th>Buyer</th>
							<th>Lab Location</th>
							<th>No TRF</th>
							<th>User</th>
							<th>Details</th>
							<th>Category</th>
							<th>Category Specimen</th> 
							<th>Type Specimen</th>
							<th>Return Test Sample </th>
							<th>Status</th>
							<th>On Time Delivery</th>
							<th>T1 Testing Result</th>
							<th>T2 Testing Result</th>
							<th>Remark</th>
							{{-- <th>Action</th> --}}
							
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_overview_testing_lab.js') }}"></script>
@endsection
