<script type="x-tmpl-mustache" id="material-cancel-fabric-table">
	{% #list %}
		<tr>
			<td>{% no %}</td>
			<td>
				<p>Supplier </br><b>{% supplier_name %}</b></p>
				<p>Po Supplier </br><b>{% document_no %}</b></p>
				<p>Item Code </br><b>{% item_code %}</b></p>
				<p>Color </br><b>{% color %}</b></p>
				<p>Batch Number </br><b>{% batch_number %}</b></p>
				<p>No Roll </br><b>{% nomor_roll %}</b></p>
				<p>Article No </br><b>{% article_no %}</b></p>
			</td>
			<td>{% actual_lot %}</td>
			<td>{% begin_width %}</td>
			<td>{% middle_width %}</td>
			<td>{% end_width %}</td>
			<td>{% actual_width %}</td>
			<td>{% total_qty_preparation %} ({% uom %})</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button></span>
			</td>
			
			
		</tr>
	{%/list%}
	<tr>
		<td colspan="14">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>
