<script type="x-tmpl-mustache" id="area_locator_table">
	<tr>
		<td>
			#
		</td>
		<td>
			<input type="text" id="rack" name="rack" class="form-control input-new" placeholder="SILAHKAN ISI NAMA RAK"></input>
			<span class="text-danger hidden" id="errorInput">
				Rack Sudah ada di table.
			</span>
		</td>
		<td>
			<input type="text" class="form-control input-new input-number" id="row" name="row" placeholder="SILAHKAN ISI NOMOR BARIS">
		</td>
		<td>
			<input type="number" id="start_column" name="start_column" class="form-control input-new" placeholder="START KOLOM" readonly="readonly" value = "1"></input> 
			<input type="text" class="form-control input-new input-number" id="to_column" name="to_column" placeholder="TO KOLOM">
		</td>
		<td>
			<input type="checkbox" class="form-control input-new" id="has_many_po_buyer">
		</td>
		<td>
			<button type="button" class="btn btn-info btn-lg" id="AddItemButton" ><i class="icon-add"></i></button>
		</td>
		
	</tr>

	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}
				<input type="hidden" value="{% id %}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<div id="rack_{% _id %}" style="text-align:center;">{% rack %}</div>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="rackInput_{% _id %}" value="{% rack %}" data-id="{% _id %}" data-row="">
				
				<span class="text-danger hidden" id="errorInput_{% _id %}">
					Rack Sudah ada di table.
				</span>
			</td>
			<td>
				<p style="text-align:center;">{% row %}</p>
			</td>
			<td>
				<p style="text-align:center;">{% start_colum %} <b>S/D</b> <span id="zcolumn_{% _id %}"> {% end_colum %}</span></p>
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="FromZcolumnInput_{% _id %}" value="{% start_colum %}" data-id="{% _id %}" data-row="">
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="ToColumnInput_{% _id %}" value="{% end_colum %}" data-id="{% _id %}" data-row="">
				<input type="text" style="text-align:center;" class="form-control input-sm hidden input-item input-edit" id="SystemToColumnInput_{% _id %}" value="{% system_colum %}" data-id="{% _id %}" data-row="">
			</td>
			<td>
				<input type="checkbox" class="checkbox_item" id="check_{% _id %}" data-id="{% _id %}" {% #has_many_po_buyer %} checked="checked" {% /has_many_po_buyer %}>
			</td>
			<td>
				<button type="button" id="edit_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-item"><i class="icon-pencil"></i></button>
				<button type="button" id="simpan_{% _id %}" data-id="{% _id %}" class="btn btn-success btn-icon-anim btn-circle btn-save-item hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancel_{% _id %}" data-id="{% _id %}" class="btn btn-warning btn-icon-anim btn-circle btn-cancel-item hidden"><i class="icon-x""></i></button>
			</td>
		</tr>
	{%/item%}
</script>