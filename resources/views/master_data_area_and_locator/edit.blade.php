@extends('layouts.app',['active' => 'master_data_area_and_locator'])

@section('page-header')
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Area & Locator</span></h4>
            </div>
        </div>
        <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
            <ul class="breadcrumb">
                <li><i class="icon-home2 position-left"></i> Home</li>
                <li>Master Data</li>
                <li><a href="{{ route('masterDataAreaAndLocator.index') }}" id="url_master_data_area_index">Area & Locator</a></li>
                <li class="active">Edit</li>
            </ul>
        </div>
    </div>
@endsection

@section('page-content')
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-flat">
				<div class="panel-body">
					{!!
						Form::open([
							'role'      => 'form',
							'url'       => route('masterDataAreaAndLocator.update',$area->id),
							'method'    => 'post',
							'class'     => 'form-horizontal',
							'enctype'   => 'multipart/form-data',
							'id'        => 'form'
						])
					!!}
				
						@include('form.text', [
							'field'         => 'erp_id',
							'label'         => 'Erp ID',
							'default'		=> $area->erp_id,
							'label_col'     => 'col-sm-12',
							'form_col'      => 'col-sm-12',
							'attributes'    => [
								'id'        => 'erp_id',
							]
						])

						@include('form.text', [
							'field'         => 'name',
							'label'         => 'Name',
							'default'		=> $area->name,
							'label_col'     => 'col-sm-12',
							'form_col'      => 'col-sm-12',
							'attributes'    => [
								'id'        => 'name',
							]
						])

						@include('form.select', [
							'field' 		=> 'warehouse',
							'label' 		=> 'Warehouse',
							'default'		=> $area->warehouse,
							'label_col'     => 'col-sm-12',
							'form_col'      => 'col-sm-12',
							'options' => [
								'' 			=> '-- Select Warehouse --',
								'1000002' 	=> 'Warehouse Accessories AOI 1',
								'1000013' 	=> 'Warehouse Accessories AOI 2',
								'1000001' 	=> 'Warehouse Fabric AOI 1',
								'1000011' 	=> 'Warehouse Fabric AOI 2',
							],
							'class' 		=> 'select-search',
							'attributes' 	=> [
								'id' 		=> 'select_warehouse'
							]
						])

						{!! Form::hidden('page', 'create', array('id' => 'page')) !!}
						{!! Form::hidden('area_locator',$locators, array('id' => 'area_locator')) !!}
					<div class="text-right">
						<button type="submit" class="btn btn-primary legitRipple col-xs-12">Save <i class="icon-floppy-disk position-right"></i></button>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title text-semibold">List Locator</span></h6>
				</div>
				<div class="panel-body">
					<table class="table table-basic table-striped table-hover table-condensed">
						<thead>
							<tr>
								<th>No</th>
								<th>Rack</th>
								<th>Row</th>
								<th>Column</th>
								<th>Has Many Po Buyer</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody_area_locator">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('page-js')
	@include('master_data_area_and_locator._locator')
    <script src="{{ mix('js/master_data_area_and_locator.js') }}"></script>
@endsection
