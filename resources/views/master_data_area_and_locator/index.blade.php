@extends('layouts.app', ['active' => 'master_data_area_and_locator'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Area And Locator</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Area And Locator</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('masterDataAreaAndLocator.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')	
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			@include('form.select', [
				'field' => 'warehouse',
				'label' => 'Warehouse',
				'default' => auth::user()->warehouse,
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'1000002' => 'Warehouse Accessories AOI 1',
					'1000013' => 'Warehouse Accessories AOI 2',
					'1000001' => 'Warehouse Fabric AOI 1',
					'1000011' => 'Warehouse Fabric AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="master_data_area_and_locator_table">
					<thead>
						<tr>
							<th>No</th>
							<th>id</th>
							<th width="90%">Name</th>
							<th width="90%">Erp ID</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('message',$flag , array('id' => 'message')) !!}
	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_area_and_locator.js'))}}"></script>
@endsection
