<script type="x-tmpl-mustache" id="reject-table">
	{% #item %}
		<tr>
			<td>
				{% no %}.
				<input type="checkbox" class="checkbox_item hidden" id="check_{% _idx %}" data-id="{% _idx %}" {% #check_all %} checked="checked" {% /check_all %}>
			</td>
			<td>
				{% barcode %}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% batch_number %}
			</td>
			<td>
				{% nomor_roll %}
			</td>
			<td>
				{% inspect_lot_result %}
				<input type="text" value="{% inspect_lot_result %}" class="form-control hidden status" id="inspect_lot_result_{% _idx %}"  data-id="{% _idx %}" placeholder="Inspect lot result"  data-id="{% _idx %}" required>
			</td>
			<td>
				{% load_actual %}
				<input type="text" value="{% load_actual %}" class="form-control hidden load_Actual" id="load_Actual_{% _idx %}"  data-id="{% _idx %}" placeholder="Inspect Load Actual"  data-id="{% _idx %}" required>
			</td>
			<td>
				<textarea class="form-control inspect_lab_remark" id="inspect_lab_remark_{% _idx %}" data-id="{% _idx %}" placeholder="Please input remark here"  data-id="{% _idx %}" rows="4" cols="10" style="resize:none">{% inspect_lab_remark %}</textarea>
			</td>
			<td>
				<button id="hapus_{% _idx %}" data-id="{% _idx %}" data-barcode="{% barcode %}" class="btn btn-default btn-delete icon-trash"></button>
			</td>
		</tr>
	{%/item%}
	<tr>
		<td colspan="12">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>
