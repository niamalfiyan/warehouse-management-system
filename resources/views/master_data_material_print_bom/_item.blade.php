<script type="x-tmpl-mustache" id="material-exclude-table">
    {% #item %}
        <tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
            <td>
                {% item_code %}
            </td>
            <td>
                {% category %}
            </td>
            <td>
                {% is_ila %}
            </td>
            <td>
                {% is_from_buyer %}
            </td>
            <td>
                {% is_paxar %}
            </td>
            <td>
                {% status %}
            </td>
        </tr>
    {%/item%}
</script>
    