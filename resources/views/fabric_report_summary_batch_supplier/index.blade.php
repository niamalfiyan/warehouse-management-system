@extends('layouts.app', ['active' => 'fabric_report_summary_batch_supplier'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Summary Batch Supplier</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Summary Batch Supplier</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		{!!
			Form::open(array(
					'class' => 'form-horizontal',
					'role' => 'form',
					'url' => route('fabricReportSummaryBatchSupplier.export'),
					'method' => 'get',
					'target' => '_blank'		
			))
		!!}
			@include('form.select', [
				'field' 			=> 'year',
				'label' 			=> 'Year',
				'default' 			=> \Carbon\Carbon::now()->format('Y'),
				'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
				'options' 			=> $years,
				'class' 			=> 'select-search',
				'attributes' 		=> [
					'id' => 'select_year'
				]
			])

		@include('form.select', [
			'field' 			=> 'month',
			'label' 			=> 'Month',
			'default' 			=> \Carbon\Carbon::now()->format('m'),
			'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
			'options' 			=> $months,
			'class' 			=> 'select-search',
			'attributes'		=> [
				'id' => 'select_month'
			]
		])

		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_summary_batch_supplier_table">
				<thead>
					<tr>
						<th>Arrival Year</th>
						<th>Arrival Month</th>
						<th>Supplier Name</th>
						<th>Total Batch</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-modal')
	@include('fabric_report_summary_batch_supplier._detail_modal')
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_summary_batch_supplier.js') }}"></script>
@endsection
