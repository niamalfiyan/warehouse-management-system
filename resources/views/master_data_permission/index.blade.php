@extends('layouts.app', ['active' => 'master_data_permission'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Permission</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li>User Management</li>
				<li class="active">Permission</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('masterDataPermission.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover " id="master_data_permission_table">
						<thead>
							<tr>
								<th>id</th>
								<th width="50%">Name</th>
								<th width="50%">Description</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>

		{!! Form::hidden('page','index', array('id' => 'page')) !!}
		{!! Form::hidden('flag_msg',$flag, array('id' => 'flag_msg')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_permission.js') }}"></script>
@endsection
