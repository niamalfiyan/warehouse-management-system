@extends('layouts.app', ['active' => 'fabric_report_material_stock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Stock</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportMaterialStock.index') }}">Material Stock</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Information<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12">
					<p>Supplier name <b> {{$summary_stock_fabric->supplier_name}}</b></p>
					<p>Po supplier <b>{{$summary_stock_fabric->document_no}}</b></p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<p>Item code <b>{{$summary_stock_fabric->item_code}}</b></p>
					<p>Color <b>{{$summary_stock_fabric->color}}</b></p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-12">
					<p>Arrival Stock <b>{{ number_format($summary_stock_fabric->qty_order, 4, '.', ',')}} ({{ $summary_stock_fabric->uom }})</b></p>
					<p>Available Stock <b>{{ number_format($summary_stock_fabric->available_qty, 4, '.', ',') }} ({{ $summary_stock_fabric->uom }})</b></p> 
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="detail_stock_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>No</th>
							<th>Barcode Roll</th>
							<th>No Roll</th>
							<th>Batch Number</th>
							<th>Actual Lot</th>
							<th>Begin Width</th>
							<th>Middle Width</th>
							<th>End Width</th>
							<th>Actual Width</th>
							<th>Arrival Qty</th>
							<th>Available Qty</th>
							<th>Lot Status</th>
							<th>Inspect Status</th>
							<th>Origin Stock</th>
							<th>Last Locator</th>
							<th>Last Update</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','detail', array('id' => 'page')) !!}
	{!! Form::hidden('url_material_stock_detail_data',route('fabricReportMaterialStock.dataDetail',$summary_stock_fabric->id), array('id' => 'url_material_stock_detail_data')) !!}
	{!! Form::hidden('summar_stock_fabric_id',$summary_stock_fabric->id, array('id' => 'summar_stock_fabric_id')) !!}
@endsection

@section('page-modal')
	@include('fabric_report_material_stock._history_modal')
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/fabric_report_material_stock.js'))}}"></script>
@endsection
