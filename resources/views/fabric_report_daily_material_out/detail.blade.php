@extends('layouts.app', ['active' => 'fabric_report_daily_material_out'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Out</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportDailyMaterialOut.index') }}">Daily Material Out</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			<div class="row">
				<div class="col-md-6 col-lg-6 col-sm-12">
					<p>Planning Date <b>{{ ($header->planning_date ? \Carbon\Carbon::createFromFormat('Y-m-d', $header->planning_date)->format('d/M/Y') : '-') }}</b></p>
					<p>Warehouse <b>{{ ($header->warehouse_id =='1000011' ? 'Warehouse Fabric AOI 2': 'Warehouse Fabric AOI 1') }}</b></p>
					<p>Style <b>{{ $header->style }}</b></p>
					<p>Po Buyer <b>{{ $header->po_buyer }}</b></p>
					<p>Destination <b>{{ $header->destination }}</b></p>
					<p>Article No <b>{{ $header->article_no }}</b></p>
					<p>Qty Need <b>{{ number_format($header->qty_bom, 4, '.', ',') }} ({{$header->uom}})</b></p>
					<p>Is Piping <b> @if($header->is_piping) <span class="label label-info">Piping</span> @else <span class="label label-default">Non Piping</span> @endif</b></p>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-12">
					<p>Po Supplier :  <b>{{ $header->document_no }}</b></p>
					<p>Item Code Book :  <b>{{ $header->item_code_book }}</b></p>
					<p>Item Code Source :  <b>{{ $header->item_code_source }}</b></p>
					<p>Item Code Book Color :  <b>{{ $header->color }}</b></p>
					<p>Total Qty Prepare :  <b>{{ number_format($header->total_prepare, 4, '.', ',') }} ({{$header->uom}})</b></p>
					<p>Total Qty Supply :  <b>{{ number_format($header->total_supply, 4, '.', ',') }} ({{$header->uom}})</b></p>
					<p>Last Preparation Date :  <b>{{ ($header->last_scan_prepare_date?  \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $header->last_scan_prepare_date)->format('d/M/Y H:i:s')  : '-') }}</b></p>
					<p>Last Supply Date :  <b>{{ ($header->last_movement_out_date != null ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $header->last_movement_out_date)->format('d/M/Y H:i:s') : '-') }} </b></p>
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_daily_material_out_table">
					<thead>
						<tr>
							<th>No</th>
							<th>Barcode</th>
							<th>Nomor Roll</th>
							<th>Begin Width</th>
							<th>Middle Width</th>
							<th>End Width</th>
							<th>Actual Width</th>
							<th>Actual Lot</th>
							<th>Last Status Per Roll</th>
							<th>Total Qty</th>
						</tr>
					</thead>
					
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('planning_date',$header->planning_date , array('id' => 'planning_date')) !!}
	{!! Form::hidden('article_no',$header->article_no , array('id' => 'article_no')) !!}
	{!! Form::hidden('warehouse_id',$header->warehouse_id , array('id' => 'warehouse_id')) !!}
	{!! Form::hidden('style',$header->style , array('id' => 'style')) !!}
	{!! Form::hidden('po_buyer',$header->po_buyer , array('id' => 'po_buyer')) !!}
	{!! Form::hidden('is_piping',$header->is_piping , array('id' => 'is_piping')) !!}
	{!! Form::hidden('c_order_id',$header->c_order_id , array('id' => 'c_order_id')) !!}
	{!! Form::hidden('item_id_book',$header->item_id_book , array('id' => 'item_id_book')) !!}
	{!! Form::hidden('item_id_source',$header->item_id_source , array('id' => 'item_id_source')) !!}
	{!! Form::hidden('page','detail' , array('id' => 'page')) !!}
	{!! Form::hidden('user_id',auth::user()->id , array('id' => 'user_id')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_material_out.js') }}"></script>
@endsection
