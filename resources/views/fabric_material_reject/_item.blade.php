<script type="x-tmpl-mustache" id="fabric_material_reject_table">
	{% #list %}
		<tr {% #is_barcode_preparation_fabric %} style="background-color:#e8f5e9" {% /is_barcode_preparation_fabric %}>
			<td>{% no %}</td>
			<td>{% barcode %}</td>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% nomor_roll %}</td>
			<td>{% batch_number %}</td>
			<td>{% qty_reject %}</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
	<td colspan="8">
		<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
	</td>

	</tr>
</script>
