<html>
<head>
	<title>PRINT :: ALLOCATION</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_mrp.css')) }}">

</head>
<body>
	<div class="outer">
		<div class="header">
			<div class="header-tengah">LAPORAN APPROVAL ALLOCATION - APPAREL WAREHOUSE MANAGEMENT SYSTEM</div>
		</div>
		<div class="isi">
			<table border="1px" cellspacing="0" style="width: 100%">
				<thead style="background-color: #2196F3;color:white">
					<tr>
						<th class="text-center">#</th>
						<th>KODE ITEM</th>
						<th>DESKRIPSI ITEM</th>
						<th>CATEGORY</th>
						<th>PO BUYER</th>
						<th>UOM</th>
						<th>JOB ORDER</th>
						<th>STYLE</th>
						<th>QTY BOOK</th>
					</tr>
				</thead>
				<tbody>
					@foreach($allocations as $key => $data)
						<tr>
							<td>{{ $key+1}}</td>
							<td>{{ $data->item_code }}</td>
							<td>{{ $data->item_desc }}</td>
							<td>{{ $data->category }}</td>
							<td>{{ $data->po_buyer }}</td>
							<td>{{ $data->uom }}</td>
							<td>{{ $data->job }}</td>
							<td>{{ $data->style }}</td>
							<td>{{ $data->qty_booking }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
	</div>
</body>
</html>