<script type="x-tmpl-mustache" id="closing-mrp-table">
    {% #item %}
        <tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
            <td>
                {% po_buyer %}
            </td>
            <td>
                {% item_code %}
            </td>
            <td>
                {% warehouse %}
            </td>
            <td>
                {% reason %}
            </td>
            <td>
                {% result %}
            </td>
        </tr>
    {%/item%}
</script>
    