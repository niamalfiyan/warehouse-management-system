<div class="modal fade" id="closingMrpModal" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="updateReportStockModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open([
			'method' => 'POST'
			,'id' => 'closingMrpForm'
			,'class' => 'form-horizontal'
			,'url' => route('reportMemoRequestProduction.closing')])
		}}
		
		    <div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title">Closing for <span id="update_closing_mrp"></span></h5>
				</div>
				<div class="modal-body">
					@include('form.text', [
						'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
						'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
						'field' 		=> 'closing_uom',
						'label' 		=> 'Uom',
						'placeholder' 	=> 'UOM',
						'attributes' 	=> [
							'id' 		=> 'closing_uom',
							'readonly'	=> true
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
						'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
						'field' 		=> 'closing_need_qty',
						'label' 		=> 'Qty Need',
						'attributes' 	=> [
							'id' 		=> 'closing_need_qty',
							'readonly'	=> true
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
						'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
						'field' 		=> 'closing_outstanding_qty',
						'label' 		=> 'Qty Outstanding',
						'attributes' 	=> [
							'id' 		=> 'closing_outstanding_qty',
							'readonly'	=> true
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
						'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
						'field' 		=> 'closing_qty',
						'label' 		=> 'Qty Closing',
						'placeholder' 	=> 'Please qty closing here',
						'help' 			=> 'Qty closing must less than qty outstanding',
						'attributes' 	=> [
							'id' 		=> 'closing_qty'
						]
					])


					@include('form.select', [
						'field' 		=> 'warehouse_closing',
						'label' 		=> 'Warehouse Closing',
						'options' 		=> [
							'' 			=> '-- Please Select -- ',
							'1000002' 	=> 'Warehouse accessories aoi 1',
							'1000013' 	=> 'Warehouse accessories aoi 2',
							'1000001' 	=> 'Warehouse fabric aoi 1',
							'1000011' 	=> 'Warehouse fabric aoi 2'
						],
						'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
						'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
						'class' 		=> 'select-search',
						'attributes' 	=> [
							'id' 		=> 'warehouse_closing'
						]
					])

					@include('form.textarea', [
						'label_col' 	=> 'col-xs-12',
						'form_col' 		=> 'col-xs-12',
						'field' 		=> 'closing_reason',
						'label' 		=> 'Reason',
						'placeholder' 	=> 'Please type reason here',
						'attributes' 	=> [
							'id'		=> 'closing_reason'
						]
					])
					
					{!! Form::hidden('po_buyer','', array('id' => 'closing_po_buyer')) !!}
					{!! Form::hidden('item_code','', array('id' => 'closing_item_code')) !!}
					{!! Form::hidden('style','', array('id' => 'closing_style')) !!}
					{!! Form::hidden('article_no','', array('id' => 'closing_article')) !!}
					{!! Form::hidden('backlog_status','', array('id' => 'closing_backlog_status')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                	<button type="submit" class="btn btn-blue-success"> Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>