
<div id="historyModal" data-backdrop="static" data-keyboard="false"  class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">History for <span id="header_history_mrp"></span></h5>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="report_memo_request_production_history_tabel">
						<thead>
							<tr>
								<th>Barcode</th>
								<th>Po Supplier</th>
								<th>Supplier Name</th>
								<th>Uom</th>
								<th>Qty On Barcode</th>
								<th>Last Warehouse Movement</th>
								<th>Last Status Movement</th>
								<th>Last Date Movement</th>
								<th>Last Pic Movement</th>
								<th>Last Locator Movement</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			{!! Form::hidden('po_buyer','', array('id' => 'history_po_buyer')) !!}
			{!! Form::hidden('item_code','', array('id' => 'history_item_code')) !!}
			{!! Form::hidden('style','', array('id' => 'history_style')) !!}
			{!! Form::hidden('article_no','', array('id' => 'history_article_no')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
