@extends('layouts.app', ['active' => 'report_memo_request_production'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Memo Requst Production (MRP)</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Memo Request Production (MRP)</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
			<a class="btn btn-primary btn-icon" href="{{ route('reportMemoRequestProduction.exportClosing')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
			<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				
			{!!
				Form::open([
					'role' => 'form',
					'url' => route('reportMemoRequestProduction.importClosing'),
					'method' => 'post',
					'id' => 'upload_file_closing_mrp',
					'enctype' => 'multipart/form-data'
				])
			!!}
				<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
				<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
			{!! Form::close() !!}
			
			</div>
	</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<td>Po Buyer</td>
							<td>Item Code</td>
							<td>Warehouse</td>
							<td>Reason</td>
							<td>Status Upload</td>
						</tr>
					</thead>
					<tbody id="tbody-upload-closing-mrp">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','import', array('id' => 'page')) !!}
	{!! Form::hidden('closing-mrp','[]',array('id' => 'closing-mrp')) !!}
@endsection

@section('page-js')
	@include('report_memo_request_production._item')
	<script src="{{ mix('js/report_memo_request_production.js') }}"></script>
@endsection
