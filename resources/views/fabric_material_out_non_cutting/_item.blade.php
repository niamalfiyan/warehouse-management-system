<script type="x-tmpl-mustache" id="out-fabric-table">
	{% #list %}
		<tr}>
			<td>
				{% no %}
			</td>
			<td>
				{% locator_from_code %}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% nomor_roll %}
			</td>
			<td>
				{% batch_number %}
			</td>
			<td>
				{% actual_lot %}
			</td>
			<td>
				{% available_qty %}/{% _available_qty %}
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number" id="qtyBookingInput_{% _id %}" value="{% qty_booking %}" data-id="{% _id %}" readonly="readonly">
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>

		</tr>
	{%/list%}
	<tr>
		<td colspan="11">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
		</td>
	</tr>

</script>
