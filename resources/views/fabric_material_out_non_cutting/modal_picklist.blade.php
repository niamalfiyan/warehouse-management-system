<div id="documentConfirmationModal" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
				<h5 class="modal-title">{{ $title }}</h5>
			</div>
			<div class="modal-body">
				<div class="tabbable tab-content-bordered">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#handover" data-toggle="tab" onClick="changeTab('handover')" aria-expanded="false">Handover</a></li>
						<li><a href="#move_order" data-toggle="tab" aria-expanded="true" onClick="changeTab('move_order')">Purchase Roll</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="handover">
							<br/>
							<fieldset>
								<div class="form-group">
									<div class="col-xs-12">
										<div class="input-group">
											<input type="text" id="handoverSearch"  class="form-control" placeholder="Please search no PT here." autofocus="true">
											<span class="input-group-btn">
												<button class="btn btn-yellow-cancel" type="button" id="handoverButtonSrc" >Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
											</span>
										</div>
									</div>
								</div>
							</fieldset>
							<br/>
							<div class="shade-screen" style="text-align: center;">
								<div class="shade-screen hidden">
									<img src="/images/ajax-loader.gif">
								</div>
							</div>
							<div class="table-responsive" id="handoverTable"></div>
							<div class="row" style="padding-left: 20px;">
								<h6 class="text-semibold">Information</h6>
								<p>
									<a onClick="showInputPT()">Jika Nomor PT tidak ditemukan atau mau membuat NO PT baru dengan qty yang berbeda, silahkan tekan tulisan ini.</a>
								</p>
							</div>
							<div class="form-group text-right" style="margin-top: 10px;">
								<button type="button" class="btn btn-default text-rigth" onClick="dismisModalLov()">Close</button>
							</div>
							
							<br/>
						</div>
						<div class="tab-pane" id="move_order">
							<br/>
							<fieldset>
								<div class="form-group">
									<div class="col-xs-12">
										<div class="input-group">
											<input type="text" id="move_orderSearch"  class="form-control" placeholder="Please search destination here." autofocus="true">
											<span class="input-group-btn">
												<button class="btn btn-yellow-cancel" type="button" id="move_orderButtonSrc" >Search <span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
											</span>
										</div>
									</div>
								</div>
							</fieldset>
							<br/>
							<div class="shade-screen" style="text-align: center;">
								<div class="shade-screen hidden">
									<img src="/images/ajax-loader.gif">
								</div>
							</div>
							<div class="table-responsive" id="move_orderTable"></div>
							<div class="row" style="padding-left: 20px;">
								<h6 class="text-semibold">Information</h6>
								<p>
									<a onClick="showInputDestination()">Jika tujuan tidak ditemukan atau mau tujuan baru dengan qty yang berbeda, silahkan tekan tulisan ini.</a>
								</p>
							</div>
						</div>
					</div>
				</div>


				{!! Form::hidden('is_move_order', null , array('id' => '__is_move_order_1')) !!}
			</div>
		</div>
	</div>
</div>
