<div class="modal fade" id="confirmationMoveOrderModal" tabindex="-1" role="dialog" data-toggle="modal" data-backdrop="static" data-keyboard="false" aria-labelledby="confirmationModalLabel">
	<div class="modal-dialog" role="document">
		{{ 
			Form::open([
				'method' 	=> 'post',
				'id'		=> 'formSummaryHandoverMoveOrder',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('fabricMaterialOutNonCutting.storeSummaryHandoverMaterial')
			]) 
		}}
		    
			<div class="modal-content">
                <div class="modal-header bg-blue">
					<h5 class="modal-title"><p>Confirmation Modal For <span id="_supplier_name_move_order"></span></p><p><span id="_po_supplier_move_order"></span> <b>-</b> <span id="_item_code_move_order"></span></p> </h5>
					<small>Please input destination and Qty out here. </small> 
				</div>

				<div class="modal-body">
					@include('form.text', [
						'field' 		=> 'no_pt_input',
						'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
						'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
						'label' 		=> 'Destination',
						'placeholder' 	=> 'Please input destination here',
						'attributes' 	=> [
							'id' 			=> 'input_no_pt_move_order',
							'autocomplete'	=> 'off'
						]
					])
					
					@include('form.text', [
						'field' 		=> 'input_qty_handover',
						'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
						'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
						'label' 		=> 'Qty Move',
						'placeholder' 	=> 'Please input total qty here',
						'attributes' 	=> [
							'id' 			=> 'input_qty_handover_move_order',
							'autocomplete'	=> 'off'
						]
					])

					@include('form.text', [
						'field' 		=> 'no_bc',
						'label_col' 	=> 'col-lg-2 col-md-2 col-sm-12',
						'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
						'label' 		=> 'No BC',
						'placeholder' 	=> 'Please input no BC here',
						'attributes' 	=> [
							'id' 			=> 'input_no_bc_move_order',
							'autocomplete'	=> 'off'
						]
					])

					@include('form.text', [
						'field' 		=> 'no_kk',
						'label_col'		=> 'col-lg-2 col-md-2 col-sm-12',
						'form_col' 		=> 'col-lg-10 col-md-10 col-sm-12',
						'label' 		=> 'No KK',
						'placeholder' 	=> 'Please input no KK here',
						'attributes' 	=> [
							'id' 			=> 'input_no_kk_move_order',
							'autocomplete'	=> 'off'
						]
					])

						@include('form.select', [
						'field' => 'subcont',
						'label' => 'Is Subcont',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'options' => [
							'' => '-- Select Is Subcont --',
							'Y' => 'Subcont',
							'N' => 'Other',
						],
						'class' => 'select-search',
						'attributes' => [
							'id' => 'select_subcont'
						]
					])
				</div>

				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_move_order_id')) !!}
				{!! Form::hidden('summary_handover_id', null , array('id' => '__summary_handover_id_move_order')) !!}
				{!! Form::hidden('supplier_name', null , array('id' => '__supplier_name_move_order')) !!}
				{!! Form::hidden('c_bpartner_id', null , array('id' => '__c_bpartner_id_move_order')) !!}
				{!! Form::hidden('item_code', null, array('id' => '__item_code_move_order')) !!}
				{!! Form::hidden('document_no', null , array('id' => '__document_no_move_order')) !!}
				{!! Form::hidden('c_order_id', null , array('id' => '__c_order_id_move_order')) !!}
				{!! Form::hidden('item_id', null , array('id' => '__item_id_move_order')) !!}
				{!! Form::hidden('is_move_order', true , array('id' => '__check_move_order')) !!}
				
				{!! Form::hidden('no_pt_id', 'move_order' , array('id' => 'no_ptId_move_order')) !!}
				{!! Form::hidden('input_no_pt', 'move_order' , array('id' => 'input_no_pt_move_order')) !!}
				

                <div class="modal-footer">
					<div class="row">
						<button type="button" class="btn btn-default" onClick="dismisModal()" >Cancel</button>
						<button type="submit" class="btn btn-blue-success">Save</button>
					</div>
			    </div>
                
			</div>
		{{ Form::close() }}
	</div>
</div>
