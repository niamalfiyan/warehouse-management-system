<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Reset Password</h3>

		<div>
			<p style="margin: 1em 0;">Recently, you have been reset your password account.</p>
			<p style="margin: 1em 0;">If it is really you, ignore this email.</p>
			<p style="margin: 1em 0;">If you did not reset your password, please contact us soon.</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="margin-top: 10px">
			This email was sent automatically by WMS ACC System.
		</div>
	</body>
</html>
