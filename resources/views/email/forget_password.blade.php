<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h3>Forget Password</h3>

		<div>
			<p style="margin: 1em 0;">We have received your request for resetting your password account.</p>
			<p style="margin: 1em 0;">To reset your password, visit link below :</p>
			<p style="margin-left:1em 0 1em 15px">
				<a href="{{ action('Auth\ResetPasswordController@showResetForm', ['id' => $user->id,'date' => $datetime, 'has_token' => $token]) }}" target="_blank">
					{{ action('Auth\ResetPasswordController@showResetForm', [$user->id, $datetime, $token] ) }}
				</a>
			</p>
			<p style="margin: 1em 0;">If you did not reset the password, ignore this email.</p>
			<p style="margin: 1em 0;">This link will be expired on {{ Config::get('auth.reminder.expire', 60) }} minutes.</p>
			<p style="margin: 1em 0;">We will reply you as soon as we could and please do not reply to this email.</p>
		</div>

		<div style="margin-top: 10px">
			This email was sent automatically by WMS ACC System.
		</div>
	</body>
</html>

