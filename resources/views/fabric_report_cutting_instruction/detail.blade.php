@extends('layouts.app', ['active' => 'fabric_report_daily_cutting_instruction'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Cutting Instruction</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('fabricReportDailyCuttingInstruction.index') }}">Daily Cutting Instruction</a></li>
				<li class="active">Detail</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-4 col-lg-4 col-sm-12">
				<p>Planning Date : <b>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $cutting_instruction->planning_date)->format('d/M/Y') }}</b></p>
				<p>Article No : <b>{{ $cutting_instruction->article_no }}</b></p>
				<p>Style : <b>{{ $cutting_instruction->style }}</b></p>
				<p>Warehouse Preparation : <b>{{ ($cutting_instruction->warehouse_id == '1000001' ? 'Warehouse fabric AOI 1' : 'Warehouse fabric AOI 2') }}</b></p>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12">
				<p>Item Code :  <b>{{ $cutting_instruction->item_code }}</b></p>
				<p>Po Buyer :  <b>{{ $cutting_instruction->po_buyer }}</b></p>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12">
				<p>Total Need :  <b>{{ number_format($cutting_instruction->qty_need, 4, '.', ',') }} ({{ $cutting_instruction->uom }})</b></p>
				<p>Total Relax :  <b>{{ number_format($cutting_instruction->total_relax, 4, '.', ',') }} ({{ $cutting_instruction->uom }})</b></p>
				<p>Total Out :  <b>{{ number_format($cutting_instruction->total_out, 4, '.', ',') }} ({{ $cutting_instruction->uom }})</b></p>
			</div>
		</div>

		{!!
			Form::open(array(
				'class' => 'heading-form',
				'role' => 'form',
				'url' => route('fabricReportDailyCuttingInstruction.exportDetail',$cutting_instruction->id),
				'method' => 'get',
				'target' => '_blank'		
			))
		!!}
		<button type="submit" class="btn btn-default col-xs-12">Export Detail All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	{!!
				Form::open([
					'role'          => 'form',
					'url'           => route('fabricReportDailyCuttingInstruction.approve'),
					'method'        => 'post',
					'class'         => 'form-horizontal',
					'id'            => 'form_approve'
				])
        		!!}
				{!! Form::hidden('list_id_approve_qc','', array('id' => 'list_id_approve_qc')) !!}
	<button type="submit" class="btn btn-success col-xs-12">Approve<i class="icon-floppy-disk position-left"></i></button>
	{{ Form::close() }}
	{!!
				Form::open([
					'role'          => 'form',
					'url'           => route('fabricReportDailyCuttingInstruction.reject'),
					'method'        => 'post',
					'class'         => 'form-horizontal',
					'id'            => 'form_reject'
				])
        		!!}
				{!! Form::hidden('list_id_reject_qc','', array('id' => 'list_id_reject_qc')) !!}
				{!! Form::hidden('warehouse_id','', array('id' => 'warehouse_id')) !!}
				<button type="submit" class="btn btn-danger col-xs-12">Reject<i class="icon-floppy-disk position-left"></i></button>
				{{ Form::close() }}
	</div>
</div>

<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
						<button type="button" id="btn_check_all" class="btn btn-yellow-cancel">Check All <i class="icon-checkbox-checked position-left"></i></button>
					<button type="button" id="btn_uncheck_all" class="btn btn-default">Uncheck All <i class=" icon-checkbox-unchecked position-left"></i></button>
				</div>
			</div>
		</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_daily_cutting_instruction_detail_table">
				<thead>
					<tr>
						<th>#</th>
						<th>Preparation Date</th>
						<th>Preparation By</th>
						<th>Last Movement Date</th>
						<th>Last Movement By</th>
						<th>Last Status Movement</th>
						<th>Receive On Cutting Date</th>
						<th>Receive By</th>
						<th>Piping</th>
						<th>Po Supplier</th>
						<th>Item Code</th>
						<th>Color</th>
						<th>No Roll</th>
						<th>Barcode</th>
						<th>Location</th>
						<th>Actual Lot</th>
						<th>Top Width</th>
						<th>Middle Width</th>
						<th>End Width</th>
						<th>Actual Width</th>
						<th>Qty Prepare (In Yard)</th>
						<th>QC Status</th>
						<th>QC Date</th>
						<th>QC Name</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

{!! Form::hidden('style',$cutting_instruction->style, array('id' => 'style')) !!}
{!! Form::hidden('article_no',$cutting_instruction->article_no, array('id' => 'article_no')) !!}
{!! Form::hidden('planning_date',$cutting_instruction->planning_date, array('id' => 'planning_date')) !!}
{!! Form::hidden('warehouse_id',$cutting_instruction->warehouse_id, array('id' => 'warehouse_id')) !!}
{!! Form::hidden('page','detail', array('id' => 'page')) !!}
{!! Form::hidden('url_fabric_report_daily_cutting_instruction_detail',route('fabricReportDailyCuttingInstruction.dataDetail',$cutting_instruction->id), array('id' => 'url_fabric_report_daily_cutting_instruction_detail')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_cutting_instruction.js') }}"></script>
@endsection
