@extends('layouts.app', ['active' => 'contract'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">IMPORT</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Master Data</li>
							<li><a href="{{route('master.contract.index')}}">Contract</a></li>
							<li class="active">Import</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian KK, untuk melihat Kontrak kerja.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
			<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
						<a href="{{route('master.contract.exportToExcel')}}" class="btn btn-primary" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
						<button id="upload_button" class="btn btn-success" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<td>NO KONTRAK</td>
								<td>PO BUYER</td>
								<td>STATUS</td>
							</tr>
						</thead>
						<tbody id="tbody-upload-contract">

						</tbody>
					</table>
				</div>
			</div>
		</div>
		{!! Form::hidden('contract','[]',array('id' => 'contract')) !!}
		{!!
	    Form::open([
	        'role' => 'form',
	        'url' => route('master.contract.importFromExcel'),
	        'method' => 'post',
	        'id' => 'upload_file_contract',
	        'enctype' => 'multipart/form-data'
	    ])
		!!}
			<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
			<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
		{!! Form::close() !!}
@endsection
@include('contract._item')

@section('content-js')
	<script type="text/javascript">
	var url_loading = $('#loading_gif').attr('href');
	list_contract = JSON.parse($('#contract').val());

	function render() {
		$('#contract').val(JSON.stringify(list_contract));

		var tmpl = $('#contract-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_contract };
		var html = Mustache.render(tmpl, data);
		$('#tbody-upload-contract').html(html);
	}

	$('#upload_button').on('click', function () {
		$('#upload_file').trigger('click');
	});


	$('#upload_file').on('change', function () {
		$.ajax({
			type: "post",
			url: $('#upload_file_contract').attr('action'),
			data: new FormData(document.getElementById("upload_file_contract")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transaparant',
						border: 'none',
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				$("#alert_info_2").trigger("click", 'upload berhasil, silahkan cek kembali.');

				for (idx in response) {
					var data = response[idx];
					var input = {
						'status': data.status,
						'no_kontrak': data.no_kontrak,
						'po_buyer': data.po_buyer,
					};
					list_contract.push(input);
				}

			},
			error: function (response) {
				$.unblockUI();
				$('#upload_file_contract').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response.responseJSON);

			}
		})
		.done(function () {
			$('#upload_file_contract').trigger('reset');
			render();
		});

	})
	</script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
