@extends('layouts.app', ['active' => 'contract'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('report.inspectLab') }}"></a><span class="text-semibold">Kontrak Kerja</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Master Data</li>
							<li class="active">Contract</li>
							<a href="{{ route('master.contract.update') }}" id="url_permissionupdate" class="hidden"></a>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan oleh bagian KK, untuk melihat Kontrak kerja.
				</div>
			</div>
		</div>
	</div>

	<div class="panel panel-default border-grey">
			<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
						<a href="{{route('master.contract.importView')}}" class="btn btn-success"><i class="icon-file-excel"></i> IMPORT</a>
						<button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#insertModal"><i class="icon-add"></i> ADD NEW</button>
				</div>
			</div>
		</div>
			<div class="panel-body">
				<div class="table-responsive">
					{!! $html->table(['class'=>'table datatable-basic']) !!}
				</div>
			</div>
		</div>
@endsection

@section('content-modal')
	@include('contract._insert_modal')
	@include('contract._update_modal')
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript">
	var url_loading = $('#loading_gif').attr('href');

	function edit(url) {
		$.ajax({
			url: url,
			beforeSend: function () {
				$.blockUI({
					message: "<img src='" + url_loading + "' />",
					css: {
						backgroundColor: 'transaparant',
						border: 'none',
					}
				});
			},
			success: function (response) {
				$.unblockUI();
				var url = $('#url_permissionupdate').attr('href');
				$('#updateformnya').attr('action', url);
				$('#update_id').val(response['id']);
				$('#update_no_kontrak').val(response['no_kontrak']);
				$('#update_po_buyer').val(response['po_buyer']);
			},
		})
		.done(function (data) {
			$('#updateModal').modal();
		});
	}

	function hapus(url) {
		bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
			if (result) {
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					type: "DELETE",
					url: url,
					beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
					},
					success: function (response) {
						$.unblockUI();
					},
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data berhasil dihapus.');
					$('#dataTableBuilder').DataTable().ajax.reload();
				});
			}
		});


	}

	$('#insertformnya').submit(function (event) {
		event.preventDefault();
		var no_kontrak = $('#no_kontrak').val();
		var po_buyer = $('#po_buyer').val();

		if (!no_kontrak) {
			$("#alert_warning").trigger("click", 'Silahkan masukan No Kontrak  terlebih dahulu.');
			return false;
		}
		if (!po_buyer) {
			$("#alert_warning").trigger("click", 'Silahkan masukan PO Buyer terlebih dahulu.');
			return false;
		}

		$('#insertModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#insertformnya').attr('action'),
					data: $('#insertformnya').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});


					},
					success: function (response) {
						$.unblockUI();
						$('#insertformnya').trigger("reset");
						$('#insertModal').modal('hide');

					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500){
							$("#alert_error").trigger("click", 'Please Contact ICT');
						}else{
							$("#alert_error").trigger("click", response.responseJSON);
						}
						$('#insertModal').modal();
					}
				})
				.done(function ($result) {
					$('#dataTableBuilder').DataTable().ajax.reload();
					$("#alert_success").trigger("click", 'Data berhasil disimpan.');

				});
			}else{
				$('#insertModal').modal();
			}
		});
	});

	$('#updateformnya').submit(function (event) {
		event.preventDefault();
		var no_kontrak = $('#update_no_kontrak').val();
		var po_buyer = $('#update_po_buyer').val();

		if (!no_kontrak) {
			$("#alert_warning").trigger("click", 'Silahkan masukan No Kontrak  terlebih dahulu.');
			return false;
		}
		if (!po_buyer) {
			$("#alert_warning").trigger("click", 'Silahkan masukan PO Buyer terlebih dahulu.');
			return false;
		}

		$('#updateModal').modal('hide');
		bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
			if (result) {
				$.ajax({
					type: "PUT",
					url: $('#updateformnya').attr('action'),
					data: $('#updateformnya').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: "<img src='" + url_loading + "' />",
							css: {
								backgroundColor: 'transaparant',
								border: 'none',
							}
						});
						$('#updateModal').modal('hide');

					},
					success: function (response) {
						$.unblockUI();
						$('#updateformnya').trigger("reset");
						$('#updateModal').modal('hide');

					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500) {
							$("#alert_error").trigger("click", 'Please Contact ICT');
						} else {
							$("#alert_error").trigger("click", response.responseJSON);
							$('#insertModal').modal();
						}
						$('#updateModal').modal();
					}
				})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data berhasil diubah.');
					$('#dataTableBuilder').DataTable().ajax.reload();
				});
			} else {
				$('#updateModal').modal();
			}
		});
	});
	</script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
