<script type="x-tmpl-mustache" id="accessories_material_reverse_out_table">
	{% #list %}
		<tr id="panel_{% _id %}">
			<td>{% no %}.<input type="checkbox" class="checkbox_item" id="check_{% _idx %}" data-id="{% _idx %}" {% #is_unintegrated %} checked="checked" {% /is_unintegrated %}></td>
			<td>{% statistical_date %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %} ({% category %})</td>
			<td>{% style %}</td>
			<td>{% article_no %}</td>
			<td>{% uom_conversion %}</td>
			<td>{% qty_required %}</td>
			<td>{% qty_input %}</td>
			<td><textarea rows="3" class="form-control input-sm input-item input-edit ict-log" style="resize: none;" id="ict_log_{% _id %}" data-id="{% _id %}">{% ict_log %}</textarea></td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="11">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
		</td>
	</tr>
</script>
