<table class="table">
	<thead>
	  <tr>
			<th>NO. INVOICE</th>
			<th>NO. PACKING LIST</th>
			<th>NO. PO SUPPLIER</th>
			<th>NO. PO BUYER</th>
			<th>NAMA SUPPLIER</th>
			<th>KODE ITEM</th>
			<th>WAREHOUSE</th>
			<th>ACTION</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			@php
				$has_many_po_buyer = strpos(trim($list->pobuyer), ",");
				if($has_many_po_buyer){
					$po_buyer = 'HAS MANY BUYER';
				}else{
					$po_buyer = trim($list->pobuyer);
				}

				if($list->m_warehouse_id == '1000002')
					$warehouse = 'ACC AOI 1';
				elseif($list->m_warehouse_id == '1000013' )
					$warehouse = 'ACC AOI 2';
			@endphp
		
				<tr>
				<td>{{ $list->kst_invoicevendor  }}</td>
				<td>{{ $list->no_packinglist  }}</td>
				<td>{{ $list->documentno  }}</td>
				<td>{{ $list->supplier  }}</td>
				<td>{{ $po_buyer  }}</td>
				<td>{{ $list->item  }}</td>
				<td>{{ $warehouse  }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button" 
						data-documentno="{{ $list->documentno }}" data-buyer="{{ $list->pobuyer }}" data-packinglist="{{ $list->no_packinglist }}" data-invoice="{{ $list->kst_invoicevendor }}" data-item="{{ $list->item }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
