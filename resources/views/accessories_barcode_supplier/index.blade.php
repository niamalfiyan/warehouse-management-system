@extends('layouts.app', ['active' => 'accessories_barcode_supplier'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Reprint Barcode Supplier</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Reprint Barcode Supplier</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('accessoriesBarcodeSupplier.index'),
			'method' => 'get',
			'class' => 'form-horizontal',
			'target' => '_blank'
		])
	!!}
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="row">
					<div class="col-md-4">
						@include('form.text', [
								'field' => 'document_no',
								'placeholder' => 'Please type no. po supplier here',
								'form_col' => 'col-xs-12',
								'mandatory' => '* Optional',
								'attributes' => [
										'id' => 'document_no'
								]
						])
					</div>
					<div class="col-md-4">
						@include('form.text', [
								'field' => 'no_packing_list',
								'placeholder' => 'Please type no. packing list here',
								'form_col' => 'col-xs-12',
								'mandatory' => '* Optional',
								'attributes' => [
										'id' => 'no_packing_list'
								]
						])

						<!--@include('form.picklist', [
							'field' => 'no_packing_list',
							'name' => 'no_packing_list',
							'placeholder' => 'NO PACKING LIST'
						])-->
					</div>
					<div class="col-md-4">
						@include('form.text', [
							'field' => 'no_invoice',
							'placeholder' => 'Please type no. invoice here',
							'form_col' => 'col-xs-12',
							'mandatory' => '* Optional',
							'attributes' => [
									'id' => 'no_invoice'
							]
						])

						<!--@include('form.picklist', [
							'field' => 'no_invoice',
							'name' => 'no_invoice',
							'placeholder' => 'NO INVOICE'
						])-->
					</div>
			</div>
			<div class="row">
					<div class="col-md-4">
							@include('form.text', [
									'field' => 'po_buyer',
									'placeholder' => 'Please type no. po buyer here',
									'form_col' => 'col-xs-12',
									'mandatory' => '* Required',
									'attributes' => [
											'id' => 'po_buyer'
									]
							])
					</div>
					<div class="col-md-4">
							@include('form.text', [
									'field' => 'item_code',
									'placeholder' => 'Please type item code here',
									'form_col' => 'col-xs-12',
									'mandatory' => '* Optional',
									'attributes' => [
											'id' => 'item_code'
									]
							])
					</div>
			</div>
			<button type="submit" class="btn btn-blue-success col-xs-12" id="searchcriteria" >Search <i class="icon-search4 position-right"></i></button>
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('page-modal')
	@include('accessories_barcode_supplier._picklist', [
		'name' => 'document_supplier',
		'name2' => 'item_supplier',
		'name3' => 'no_packing_list_supplier',
		'name4' => 'no_invoice_supplier',
		'name5' => 'po_buyer_supplier',
		'title' => 'Filter Criteria',
	])	

	@include('form.modal_picklist', [
		'name' => 'no_packing_list',
		'title' => 'LIST PACKING LIST',
		'placeholder' => 'Cari berdasarkan no packling list',
	])

	@include('form.modal_picklist', [
		'name' => 'no_invoice',
		'title' => 'LIST NO INVOICE',
		'placeholder' => 'Cari berdasarkan no invoice',
	])
@endsection

@section('page-js')
	<script>
		//lov('no_packing_list','/barcode/accessories/reprint-barcode-supplier/packling-list?');
		//lov('no_invoice','/barcode/accessories/reprint-barcode-supplier/invoice-list?');
		
		//supplierLov('document_supplier', 'item_supplier', 'no_packing_list_supplier','no_invoice_supplier','po_buyer_supplier', '/barcode/accessories/reprint-barcode-supplier/supplier-list?');
		
		/*$('#document_supplierName').click(function () {
		});

		$('#document_supplierButton').click(function () {
			supplierLov('document_supplier', 'item_supplier', 'no_packing_list_supplier','no_invoice_supplier','po_buyer_supplier', '/barcode/accessories/reprint-barcode-supplier/supplier-list?');
		});*/

		/*function supplierLov(name, name2, name3,name4,name5, url) {
			var url_loading = $('#loading_gif').attr('href');
			var search = '#' + name + 'Search';
			var search2 = '#' + name2 + 'Search2';
			var search3 = '#' + name3 + 'Search3';
			var search4 = '#' + name4 + 'Search4';
			var search5 = '#' + name5 + 'Search5';
			var list = '#' + name + 'List';
			var item_id = '#' + name + 'Id';
			var item_name = '#' + name + 'Name';
			var modal = '#' + name + 'Modal';
			var table = '#' + name + 'Table';
			var buttonSrc = '#ButtonSrc';
			var buttonDel = '#' + name + 'ButtonDel';

			function itemAjax() {
				var q = $(search).val();
				var q2 = $(search2).val();
				var q3 = $(search3).val();
				var q4 = $(search4).val();
				var q5 = $(search5).val();

				$(table).addClass('hidden');
				$(modal).find('.shade-screen').removeClass('hidden');
				$(modal).find('.form-search').addClass('hidden');

				$.ajax({
					url: url + '&document_no=' + q + '&item_code=' + q2 + '&no_packing_list=' + q3 +'&no_invoice=' + q4 +'&po_buyer=' + q5
				})
				.done(function (data) {
					$(table).html(data);
					pagination(name);
					$(search).focus();
					//$(search2).val('');
					$(table).removeClass('hidden');
					$(modal).find('.shade-screen').addClass('hidden');
					$(modal).find('.form-search').removeClass('hidden');

					$(table).find('.btn-choose').on('click', chooseItem);
				});
			}

			function chooseItem() {
				var name = $(this).data('name');
				
				var document = $(this).data('documentno');
				var packing_list = $(this).data('packinglist');
				var item = $(this).data('item');
				var no_invoice = $(this).data('invoice');
				var po_buyer = $(this).data('buyer');
				
				$(item_name).val(name);
				$('#document_no').val(document);
				$('#no_packing_list').val(packing_list);
				$('#item_code').val(item);
				$('#no_invoice').val(no_invoice);
				$('#po_buyer').val(po_buyer);
				$('#formid').submit();
			}

			function pagination() {
				$(modal).find('.pagination a').on('click', function (e) {
					var params = $(this).attr('href').split('?')[1];
					url = $(this).attr('href') + (params == undefined ? '?' : '');

					e.preventDefault();
					itemAjax();
				});
			}

			//$(search).val("");
			$(buttonSrc).unbind();
			$(search).unbind();
			$(search2).unbind();
			$(search3).unbind();
			$(search4).unbind();
			$(search5).unbind();

			$(buttonSrc).on('click', itemAjax);

			$(search).on('keypress', function (e) {
				if (e.keyCode == 13)
					itemAjax();
			});

			$(search2).on('keypress', function (e) {
				if (e.keyCode == 13)
					itemAjax();
			});

			$(search3).on('keypress', function (e) {
				if (e.keyCode == 13)
					itemAjax();
			});

			$(search4).on('keypress', function (e) {
				if (e.keyCode == 13)
					itemAjax();
			});

			$(search5).on('keypress', function (e) {
				if (e.keyCode == 13)
					itemAjax();
			});

			$(buttonDel).on('click', function () {
				$(item_id).val('');
				$(item_name).val('');

			});

			itemAjax();
		}*/
	</script>
@endsection
