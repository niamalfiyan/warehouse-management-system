<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: @if($rack == 'null') Area & Locator @else Area @endif</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_rack_fabric.css')) }}">
</head>
<body>
    @foreach ($locators as $key => $locator)
       <div class="outer text-uppercase">
		<div class="title">WAREHOUSE MANAGEMENT SYSTEM - FABRIC</div>
		<div class="company">PT. APPAREL ONE INDONESIA</div><br>
		<div class="qrcode">
           <img style="height: 4cm; width: 8cm;	 margin-top: 18px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$locator->barcode", 'C128') }}" alt="barcode"   />	
        </div>
        <br/>
		<div class="footer">			
			<header>{{ $locator->rack }}.{{ $locator->y_row }}.{{ $locator->z_column }}</header>
		</div>
	</div>	
    @endforeach
</body>
</html>
