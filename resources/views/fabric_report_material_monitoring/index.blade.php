@extends('layouts.app', ['active' => 'fabric_report_monitoring'])


@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Monitoring Status Material</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Report Monitoring Status Material</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
		{!!
				Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'url' => route('fabricReportMaterialMonitoring.data'),
						'method' => 'get',
						'id' 	 => 'get_monitoring_material'		
				))
		!!}
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
		
		<div class="row">
		<div class="col-sm-6">
			@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'LC Date From',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'LC Date To',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])
			</div>
			<div class="col-sm-6">
			@include('form.date', [
				'field' 		=> 'start_statistical_date',
				'label' 		=> 'Statistical Date From ',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_statistical_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'end_statistical_date',
				'label' 		=> 'Statistical Date To ',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_statistical_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])
			
			</div>

			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top:10px;">Filter <i class="icon-filter3 position-left"></i></button>
			{!! Form::close() !!}
			{!!
                    Form::open([
                        'class' 	=> 'form-horizontal',
						'role' 		=> 'form',
						'url' 		=> route('fabricReportMaterialMonitoring.export'),
						'method' 	=> 'get',
						'target' 	=> '_blank'	
                    ])

			!!}
			{!! Form::hidden('lc_date_from','', array('id' => 'lc_date_from')) !!}
			{!! Form::hidden('lc_date_to','', array('id' => 'lc_date_to')) !!}
			{!! Form::hidden('_start_statistical_date','', array('id' => '_start_statistical_date')) !!}
			{!! Form::hidden('_end_statistical_date','', array('id' => '_end_statistical_date')) !!}
				<button type="submit" class="btn btn-default col-xs-12">Export<i class="icon-file-excel position-left"></i></button>
				{{ Form::close() }}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_material_monitoring_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>LC date</th>
							<th>Statistical Date</th>
							<th>Promise Date</th>
							<th>Po Buyer</th>
							<th>Style</th>
							<th>Brand</th>
							<th>Status</th>
							<th>Item Code</th>
							<th>PO Supplier</th>
							<th>Supplier Name</th>
							<th>Qty Allocation</th>
							<th>Qty In House</th>
							<th>Qty On Ship</th>
							<th>Qty Open</th>
							<th>Uom</th>
							<th>MRD</th>
							<th>DD PI</th>
							<th>ETD PI</th>
							<th>ETA PI</th>
							<th>ETD Actual</th>
							<th>ETA Actual</th>
							<th>ETA Delay</th>
							<th>ETA Max</th>
							<th>No invoice</th>
							<th>Recieve Date</th>
							<th>Warehouse</th>
							<th>Remark Alokasi</th>
							<th>Remark Delay</th>
							<th>Status</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_material_monitoring.js') }}"></script>
@endsection
