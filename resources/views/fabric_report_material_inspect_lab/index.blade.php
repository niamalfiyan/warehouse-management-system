@extends('layouts.app', ['active' => 'fabric_report_material_inspect_lab'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Inspect Lab</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Report</li>
            <li class="active">Material Inspect Lab</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			{!!
				Form::open(array(
					'class' => 'form-horizontal',
					'role' => 'form',
					'url' => route('fabricReportMaterialInspectLab.exportSummary'),
					'method' => 'get'		
				))
			!!}

				@include('form.select', [
					'field' => 'warehouse',
					'label' => 'Warehouse',
					'default' => auth::user()->warehouse,
					'label_col' => 'col-md-2 col-lg-2 col-sm-12',
					'form_col' => 'col-md-10 col-lg-10 col-sm-12',
					'options' => [
						'' => '-- Select Warehouse --',
						'1000001' => 'Warehouse Fabric AOI 1',
						'1000011' => 'Warehouse Fabric AOI 2',
					],
					'class' => 'select-search',
					'attributes' => [
						'id' => 'select_warehouse'
					]
				])
				
				@include('form.date', [
					'field' 		=> 'start_date',
					'label' 		=> 'Arrival Date From (00:00)',
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
					'placeholder' 	=> 'dd/mm/yyyy',
					'class' 		=> 'daterange-single',
					'attributes'	=> [
						'id' 			=> 'start_date',
						'autocomplete' 	=> 'off',
						'readonly' 		=> 'readonly'
					]
				])
				
				@include('form.date', [
					'field' 		=> 'end_date',
					'label' 		=> 'Arrival Date To (23:59)',
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
					'placeholder' 	=> 'dd/mm/yyyy',
					'class' 		=> 'daterange-single',
					'attributes' 	=> [
						'id' 			=> 'end_date',
						'readonly' 		=> 'readonly',
						'autocomplete' 	=> 'off'
					]
				])

				@include('form.select', [
					'field' => 'status',
					'label' => 'Status',
					'label_col' => 'col-md-2 col-lg-2 col-sm-12',
					'form_col' => 'col-md-10 col-lg-10 col-sm-12',
					'options' => [
						'' => '-- All --',
						'hold' => 'Hold',
					],
					'class' => 'select-search',
					'attributes' => [
						'id' => 'select_status'
					]
				])
					<button type="submit" class="btn btn-default col-xs-12">Export Header<i class="icon-file-excel position-left"></i></button>
					<a href="{{ route('fabricReportMaterialInspectLab.exportAll') }}" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></a>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="fabric_report_material_inspect_lab_table">
					<thead>
						<tr>
							<th>id</th>
							<th>Arrival Warehouse</th>
							<th>Arrival Date</th>
							<th>No Invoice</th>
							<th>Handover</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Item Code</th>
							<th>Style</th>
							<th>Color</th>
							<th>Total Roll</th>
							<th>Total Yard</th>
							<th>Total Inspected Roll</th>
							<th>Total Batch Number</th>
							<th>Total Lot Actual</th>
							<th>Batch Number</th>
							<th>Lot Actual</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_material_inspect_lab.js') }}"></script>
@endsection

