<script type="x-tmpl-mustache" id="material-other-table">
	{% #item %}
		<tr {% #is_error %} style="background-color:#FF5722;color:#fff" {% /is_error %} {% #is_warning %} style="background-color:#fffed9" {% /is_warning %}>
			<td>
				{% no %}
			</td>
			<td>
				{% type_stock %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% po_buyer %}
			</td>
			<td>
				{% locator_name %}
			</td>
			<td>
				{% item_code %} ({% category %})
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% qty %}
			</td>
			<td>
				{% source %}
			</td>
			<td>
				{% #is_error %} {% remark %} {% /is_error %}
				{% ^is_error %}
					<textarea class="form-control input-sm input-remark-edit" rows="5" style="resize:none" id="remarkInput_{% _id %}" data-id="{% _id %}" placeholder="Please insert note here">{% remark %}</textarea>
				{% /is_error %}
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}

	<tr>
		<td>#</td>
		<td>
			<select id="type_stock" name="type_stock" class="form-control">
				<option value="0"> </option>
				<option value="1">SLT</option>
				<option value="2">REGULER</option>
				<option value="3">PR/SR</option>
				<option value="4">MTFC</option>
				<option value="5">NB</option>
				<option value="6">ALLOWANCE</option>
			</select>
			<span class="help-block text-info">Optional</span>
		</td>
		<td>
			<div style="width:100%">
				@include('form.picklist', [
					'field' => 'supplier_name',
					'name' => 'supplier_name',
					'placeholder' => 'SUPPLIER NAME'
				])
				<input type="hidden" id="supplier_name" name="supplier_name" class="form-control input-new" readonly="readonly"></input>
				<span class="help-block text-info">Optional</span>
			</div>
		</td>
		<td>
			<div style="width:100%">
				<input type="text" 	id="document_noName" name="document_no_name" class="form-control input-new" value="FREE STOCK" readonly="readonly"></input>
				<input type="hidden" id="po_detail_id" name="po_detail_id" class="form-control input-new" value="FREE STOCK" readonly="readonly"></input>
				<input type="hidden" id="document_noId" name="document_no_id" class="form-control input-new" value="FREE STOCK" readonly="readonly"></input>
				<input type="hidden" id="c_bpartner_id" name="c_bpartner_id" class="form-control input-new" readonly="readonly"></input>
				<input type="hidden" id="supplier_code" name="supplier_code" class="form-control input-new" readonly="readonly"></input>
			</div>
		</td>
		<td>
			<div style="width:100%">
				<input type="text" id="po_buyer" name="po_buyer" class="form-control input-new" placeholder="PO BUYER"></input>
				<span class="help-block text-info">Optional</span>
			</div>
		</td>
		<td>
			<div style="width:100%">
				@include('form.picklist', [
					'field' 		=> 'locator',
					'name' 			=> 'locator',
					'placeholder' 	=> 'Locator'
				])
				<span class="help-block text-danger">*Required</span>
			</div>
		</td>
		<td>
			<div style="width:100%">
				@include('form.picklist', [
					'field' => 'item',
					'name' => 'item',
					'placeholder' => 'Silahkan Pilih Item'
				])
				<span class="help-block text-danger">*Required</span>
			</div>
		</td>
		<td>
			<input type="hidden" id="item_desc" name="item_desc" class="form-control input-new" readonly="readonly"></input>
			<input type="hidden" id="category" name="category" class="form-control input-new" readonly="readonly"></input>
			<input type="text" id="uom" name="uom" class="form-control input-new" readonly="readonly"></input>
			<span class="help-block text-info">Get From System</span>
		</td>
		<td>
			<input type="text" id="qty" name="qty" class="form-control input-new input-number"></input>
			<span class="help-block text-danger">*Required</span>
		</td>
		<td>
			<select id="source" name="source" class="form-control">
				<option value=""> </option>
				<option value="CANCEL ORDER">CANCEL ORDER</option>
				<option value="CANCEL ITEM">CANCEL ITEM</option>
				<option value="DOUBLE PR">DOUBLE PR</option>
			</select>
			<span class="help-block text-info">Optional</span>
		</td>
		<td>
			<div style="width:100%">
				<textarea class="form-control input-sm input-remark" style="resize:none" rows="5" id="remark" name="remark" data-id="{% _id %}" placeholder="Please insert remark here"></textarea>
				<span class="help-block text-info">Optional</span>
			</div>
		</td>
		<td>
			<button type="button" class="btn btn-default btn-lg" id="AddItemButton" ><i class="icon-add"></i></button>
		</td>

	</tr>
</script>
