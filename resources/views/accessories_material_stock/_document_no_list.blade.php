<table class="table datatable-basic table-striped table-hover">
	<thead>
	  <tr>
		<th>Type Stock</th>
		<th>Supplier Code</th>
		<th>Supplier Name</th>
		<th>Po Supplier</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->type_stock }}</td>
				<td>{{ $list->supplier_code }}</td>
				<td>{{ $list->supplier_name }}</td>
				<td>{{ $list->document_no }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
						type="button"  data-id="{{ $list->c_order_id }}" 
						data-name="{{ $list->document_no }}" 
						data-supplier="{{ $list->supplier_name  }}" 
						data-bpartner="{{ $list->c_bpartner_id }}"
						data-typestock="{{ $list->type_stock }}" 
						data-typestockerp="{{ $list->type_stock_erp_code }}" 
						data-suppliercode="{{$list->supplier_code}}"
					>Select</button>
				</td>
			</tr>

		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
