<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{ Form::open(['method' => 'DELETE', 'id' => 'formnya', 'class' => 'form-inline', 'url' => "#"]) }}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Remove this data?</h5>
			</div>

			<div class="modal-body"></div>
			
			<div class="modal-footer text-center">
				<button type="submit" class="btn btn-danger">Delete</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>