<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
		<th>Item Code</th>
		<th>Item Desc</th>
		<th>Category</th>
		<th>Uom</th>
		<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@if(count($lists)>0)
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->item_code }}</td>
				<td>{{ $list->item_desc }}</td>
				<td>{{ $list->category }}</td>
				<td>{{ $list->uom }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose"
					type="button" data-id="{{ $list->item_id }}" 
					data-code="{{ $list->item_code }}" 
					data-name="{{ $list->item_desc }}" 
					data-uom="{{ $list->uom }}"
					data-category="{{ $list->category }}"
					data-uom="{{ $list->uom }}"
					data-itemid="{{ $list->item_id }}"
					>Select</button>
				</td>
			</tr>

		@endforeach
		@endif
	</tbody>
</table>
{!! $lists->appends(Request::except('page'))->render() !!}
