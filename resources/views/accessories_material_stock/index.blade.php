@extends('layouts.app', ['active' => 'accessories_material_stock_data'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Material Stock</li>
        </ul>
		<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('accessoriesMaterialStock.create') }}"><i class="icon-pencil6 pull-right"></i> Create</a></li>
						<li><a href="{{ route('accessoriesMaterialStock.export') }}"><i class="icon-file-excel pull-right"></i> Export</a></li>
					</ul>
				</li>
			</ul
    </div>
</div>

{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="accessories_material_stock_data_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Created At</th>
							<th>Created By</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Po Buyer</th>
							<th>Item Code</th>
							<th>Item Desc</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Qty</th>
							<th>Remark</th>
						</tr>
					</thead>
				</table>
			</div>
			
		</div>
	</div>

	{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_material_stock.js') }}"></script>
@endsection
