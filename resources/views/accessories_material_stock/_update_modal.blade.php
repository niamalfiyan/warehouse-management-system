<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'PUT', 'id' => 'updateformnya', 'class' => 'form-horizontal', 'url' => '#']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">FORM</h5>
				</div>
				<div class="modal-body">
				   <fieldset>
					<div class="form-group">
						<div class="col-xs-12">
							<div class="input-group">
								<input type="text" id="UpdateSearch"  class="form-control" placeholder="Input Keyword..." autofocus="true">
								<span class="input-group-btn">
									<button class="btn bg-teal" type="button" id="UpdateButtonSrc">Search</button>
								</span>
							</div>
							<span class="help-block">Cari berdasarkan nama/code</span>
								
						</div>
					</div>
				</fieldset>
				</br>
				<div class="shade-screen" style="text-align: center;">
					<div class="shade-screen hidden">
						<img src="/img/ajax-loader.gif">
					</div>
				</div>
				<div class="table-responsive" id="UpdateTable"></div>
				 {!! Form::hidden('update_id','', array('id' => 'update_id')) !!}
				 {!! Form::hidden('locator_id','', array('id' => 'LocatorId')) !!}
				</div>
                <div class="modal-footer text-center">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>