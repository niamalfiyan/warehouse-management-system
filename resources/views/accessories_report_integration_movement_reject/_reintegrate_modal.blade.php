<div class="modal fade" id="reintegrateModal" tabindex="-1" data-backdrop="static" data-keyboard="false"  role="dialog" aria-labelledby="reintegrateModal">
        <div class="modal-dialog" role="document">
		{{ 
			Form::open([
				'method' 	=> 'POST', 
				'id' 		=> 'form', 
				'class' 	=> 'form-horizontal', 
				'url' 		=> route('accessoriesReportIntegrationMovementReject.store')
			]) 
		}}
		    <div class="modal-content">
                <div class="modal-body">
				
				@include('form.text', [
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 		=> 'document_movement_id',
					'label' 		=> 'Document Movement',
					'placeholder' 	=> 'Please insert document movement id',
					'attributes' 	=> [
						'id' 		=> 'document_movement_id',
					]
				])

				</div>
				<div class="modal-footer text-center">
					<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			{{ Form::close() }}
			
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		
        </div>
	</div>