<html>
<head>
	<title>PRINT :: Surat Jalan</title>
</head>
	<body>
	<table border="1px" cellspacing="0" style="width: 100%">
		<thead style="color:black">
			<tr>
				<!-- <th><img src="{{ asset('images/logo_aoi.png') }}" alt="" height=200 width=600></img></th> -->
				<th rowspan="2"><h2>PT. APPAREL ONE INDONESIA </h2></th>
				<th style="text-align:left">NO. SJ : {{ $nomor_surat_jalan }}</th>
			</tr>
			<tr>
				<th style="text-align:left">DATE : {{ $tanggal_kirim }}</th>
			</tr>
			<tr>
				<th colspan="2" style="text-align:center"><h2>SURAT JALAN</h2></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
	@php
		if($warehouse_id =='1000001')
		{
			$from  = 'PT. APPAREL ONE INDONESIA 1';
			$to = 'PT. APPAREL ONE INDONESIA 2';
			$alamat = '';
			$_alamat = '';
		}
		else
		{
			$from  = 'PT. APPAREL ONE INDONESIA 2';
			$to = 'PT. APPAREL ONE INDONESIA 1';
			$alamat = 'KAWASAN BERIKAT PT PUTRA WIJAYA KUSUMA SAKTI, KAWASAN INDUSTRI WIJAYAKUSUMA';
			$_alamat = 'JALAN RAYA SEMARANG-KENDAL KM 12 BLOK B-05, SEMARANG, JAWA TENGAH';
		}
		
	@endphp

	<p style="text-align:left;margin-bottom:0px;font-size:12px">
    Sold to :  {{$to}}
    <span style="float:right;font-size:12px">
    Shipped From : {{$from}}
    </span>
	</p>
	<p style="text-align:left;margin-top:0px;font-size:12px">
    {{$alamat}}<br>
	{{$_alamat}}
    <span style="float:right;">
    </span>
	</p>

		<table border="1px" cellspacing="0" style="width: 100%">
			<thead style="background-color: #ebae34;color:black">
				<tr>
					<th>DATE SHIPPED</th>
					<th>NO PT / KK</th>
					<th>DESCRIPTION</th>
					<th>ITEM CODE</th>
					<th>NO ROLL</th>
					<th>QTY</th>
					<th>UOM</th>
				</tr>
			</thead>
			<tbody>
			
			@php
				$total = 0;
			@endphp

			@foreach($prints as $key => $data)
			@php
				$total += $data->qty_handover;
			@endphp
					<tr>
						<td>{{ $data->tanggal_kirim }}</td>
						<td>{{ $data->no_kk }}</td>
						<td>{{ $data->item_desc }}</td>
						<td>{{ $data->item_code }}</td>
						<td>{{ $data->nomor_roll }}</td>
						<td>{{ $data->qty_handover }}</td>
						<td>{{ $data->uom }}</td>
					</tr>
			@endforeach
					<tr>
						<td colspan="4" style="text-align:center"><b>TOTAL</b></td>
						<td>{{ $key+1}} PACK</td>
						<td>{{$total}}</td>
						<td>YDS</td>
					</tr>
			</tbody>
		</table>
		<br>
		<br>
		<table border=0 width="100%">
			<tr>
				<td><center>DITERIMA OLEH </center></td>
				<td><center>DISETUJUI OLEH </center></td>
				<td><center>DIKIRIM OLEH </center></td>
			</tr>
			<tr>
				<td><br></td>
				<td><br></td>
				<td><br></td>
			</tr>
			<tr>
				<td><br></td>
				<td><br></td>
				<td><br></td>
			</tr>
			<tr>
				<td><br></td>
				<td><br></td>
				<td><br></td>
			</tr>
			<tr>
				<td><center>......................</center></td>
				<td><center>......................</center></td>
				<td><center>......................</center></td>
			</tr>
		</table>
	</body>
</html>