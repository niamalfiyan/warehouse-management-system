@extends('layouts.app', ['active' => 'accessories_barcode_non_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Allocation Non Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Allocation</li>
				<li class="active">Non Buyer</li>
			</ul>
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('accessoriesAllocationNonBuyer.upload') }}"><i class="icon-file-excel pull-right"></i>Upload</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<a href="{{ route('accessoriesAllocationNonBuyer.index') }}" id="url_allocation_non_buyer_index" class="hidden"></a>
<div class="panel panel-default border-grey">
	
	
	<div class="panel-body">
		@role(['admin-ict-acc'])
			@include('form.select', [
				'field' 			=> 'warehouse',
				'label' 			=> 'Warehouse',
				'default'			=> auth::user()->warehouse,
				'label_col' 		=> 'col-sm-12',
				'form_col' 			=> 'col-sm-12',
				'options' 			=> [
					'1000002' 		=> 'Warehouse Accessories AOI 1',
					'1000013' 		=> 'Warehouse Accessories AOI 2',
				],
				'class' 			=> 'select-search',
				'attributes' 		=> [
					'id' 			=> 'select_warehouse'
				]
			])
		@else 
			@include('form.select', [
				'field' 			=> 'warehouse',
				'label' 			=> 'Warehouse',
				'default'			=> auth::user()->warehouse,
				'label_col' 		=> 'col-sm-12',
				'form_col' 			=> 'col-sm-12',
				'options' 			=> [
					'1000002' 		=> 'Warehouse Accessories AOI 1',
					'1000013' 		=> 'Warehouse Accessories AOI 2',
				],
				'class' 			=> 'select-search',
				'attributes' 		=> [
					'id' 			=> 'select_export_warehouse'
				]
			])
			{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
		@endrole

		{!!
			Form::open([
				'role' 			=> 'form',
				'url' 			=> route('accessoriesAllocationNonBuyer.store'),
				'method' 		=> 'post',
				'enctype' 		=> 'multipart/form-data',
				'class' 		=> 'form-horizontal',
				'id'			=> 'form'
			])
		!!}
			<div class="col-xs-6">
				<div class="row">
					@include('form.picklist', [
						'label' 			=> 'Stock',
						'field' 			=> 'item_allocation',
						'name' 				=> 'item_allocation',
						'placeholder' 		=> 'Searching Stock',
						'label_col' 		=> 'col-sm-12',
						'form_col' 			=> 'col-sm-12',
						'mandatory' 		=> '*Required',
						'readonly' 			=> 'true',
					])
				</div>
				<div class="row">
					<p id="document_no" class="hidden"><b>Po Supplier: </b> <span id="_document_no"></span></p>
					<p id="category" class="hidden"><b>Category: </b> <span id="_category"></span></p>
					<!--<p id="locator" class="hidden"><b>LOCATOR: </b> <span id="_locator"></span></p>-->
					<p id="uom" class="hidden"><b>Uom: </b> <span id="_uom"></span></p>
					<p id="qty_stock" class="hidden"><b>Available Qty: </b> <span id="_qty_stock"></span></p>
					{!! Form::hidden('__document_no','', array('id' => '__document_no')) !!}
					{!! Form::hidden('__uom','', array('id' => '__uom')) !!}
					{!! Form::hidden('__category','', array('id' => '__category')) !!}
					{!! Form::hidden('_temp_qty','', array('id' => '_temp_qty')) !!}
					{!! Form::hidden('locator_id','', array('id' => '___locator')) !!}
					{!! Form::hidden('item_code','', array('id' => '___item_code')) !!}
				</div>
				
			</div>
			<div class="col-xs-6">
				@include('form.text', [
					'field' 		=> 'qty_booking',
					'label' 		=> 'Qty booking',
					'placeholder' 	=> 'Please input qty booking here',
					'mandatory' 	=> '*Required',
					'form_col' 		=> 'col-xs-12',
					'attributes' 	=> [
						'id' 		=> 'qty_booking'
					]
				])
				
				@include('form.textarea', [
					'field' 		=> 'remark',
					'label' 		=> 'Remark',
					'placeholder' 	=> 'Please input remark here',
					'mandatory' 	=> '*Required',
					'form_col' 			=> 'col-xs-12',
					'attributes' 	=> [
						'rows' 		=> '3',
						'id' 		=> 'remark',
						'style'		=> 'resize:none'
					]
				])
				
			</div>
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			
			<button type="submit" class="btn btn-blue-success col-xs-12" >Save <i class="icon-floppy-disk position-left"></i></button>
		{!! Form::close() !!}
		{!!
			Form::open(array(
					'class' 	=> 'heading-form',
					'role' 		=> 'form',
					'url' 		=> route('accessoriesAllocationNonBuyer.export'),
					'method' 	=> 'get',
					'target' 	=> '_blank'

				))
		!!}
			{!! Form::hidden('export_warehouse',auth::user()->warehouse , array('id' => 'export_warehouse')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Export All<i class="icon-file-excel position-left"></i></button>
		{!! Form::close() !!}
	</div>


</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover " id="allocationNonBuyerTable">
				<thead>
					<tr>
						<th>id</th>
						<th>Created At</th>
						<th>Username</th>
						<th>Supplier Code</th>
						<th>Supplier Name</th>
						<th>Po. Supplier</th>
						<th>Item Code</th>
						<th>Category</th>
						<th>Uom</th>
						<th>Qty Allocated</th>
						<th>Remark</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-modal')
	@include('accessories_allocation_non_buyer.modal_picklist', [
		'name' 	=> 'item_allocation',
		'name2' => 'document_no_allocation',
		'name3' => 'po_buyer_allocation',
		'title' => 'Filter Criteria',
	])	
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_allocation_non_buyer.js') }}"></script>
@endsection
