<script type="x-tmpl-mustache" id="allocation-non-buyer-upload-table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>
				<p>Supplier Code <b>{% supplier_code %}</b></p>
				<p>Po Supplier <b>{% document_no %}</b></p>
				<p>Po Buyer Source <b>{% po_buyer_source %}</b></p>
				<p>Locator <b>{% locator_name %}</b></p>
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% qty_booking %}
			</td>
			<td>
				{% note %}
			</td>
			<td>
				{% result %}
			</td>
			
		</tr>
	{%/item%}
</script>