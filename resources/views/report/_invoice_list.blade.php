<table class="table">
	<thead>
	  <tr>
			<th>NO. INVOICE</th>
			<th>ACTION</th>
		</tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->kst_invoicevendor  }}</td>
				<td>
					<button class="btn btn-info btn-xs btn-choose" type="button"
						data-dismiss="modal" data-invoice="{{ $list->kst_invoicevendor }}">Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
