@extends('layouts.app', ['active' => 'report_check_material'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MATERIAL CHECK REPORT</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li class="active">Material Check (QC Status)</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'id'=>'form_filter',
						'url' => route('report.materialcheck'),
						'method' => 'get',
					))
				!!}
				<div class="form-group">
					@include('form.date', [
						'field' => 'start_date',
						'label' => 'FROM (00:00)',
						'class' => ' datepicker',
						'default'=> $start_date,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'start_date',
							'autocomplete' => 'off'
						]
					])
				</div>
				<div class="form-group">
					@include('form.date', [
						'field' => 'end_date',
						'label' => 'TO (23:59)',
						'class' => ' datepicker',
						'default'=> $end_date,
						'placeholder' => 'dd/mm/yyyy',
						'attributes' => [
							'id' => 'end_date',
							'autocomplete' => 'off'
						]
					])
				</div>

				<select class="form-control" name="qc_status" id="qc_status">
					@if($qc_status==''||$qc_status=='ALL')
					<option value="ALL" selected>ALL</option>
					@else
					<option value="ALL">ALL</option>
					@endif

					@if($qc_status=='QUARANTINE')
					<option value="QUARANTINE" selected>QUARANTINE</option>
					@else
					<option value="QUARANTINE">QUARANTINE</option>
					@endif

					@if($qc_status=='HOLD')
					<option value="HOLD" selected>HOLD</option>
					@else
					<option value="HOLD">HOLD</option>
					@endif

					@if($qc_status=='RELEASE')
					<option value="RELEASE" selected>RELEASE</option>
					@else
					<option value="RELEASE">RELEASE</option>
					@endif

					@if($qc_status=='REJECT')
					<option value="REJECT" selected>REJECT</option>
					@else
					<option value="REJECT">REJECT</option>
					@endif
				</select>
				<div class="heading-btn">
					{{-- <button type="submit" class="btn btn-info">FILTER</button> --}}
				</div>
				{!! Form::close() !!}
					{!!
						Form::open(array(
								'class' => 'heading-form',
								'role' => 'form',
								'url' => route('report.expormaterialcheck'),
								'method' => 'get',
								'target' => '_blank'		
						))
					!!}
					<button type="submit" class="btn btn-default">Export All<i class="icon-file-excel position-left"></i></button>
				{!! Form::close() !!}
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'material_check']) !!}
			</div>
		</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_material_check.js'))}}"></script>
@endsection
