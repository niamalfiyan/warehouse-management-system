@extends('layouts.app', ['active' => 'monitor-receiving'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MONITORING RECEIVING</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">Monitoring Receiving</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk memonitor kedatangan perhari.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				
			<div class="heading-elements">
					{!!
						Form::open(array(
							'class' => 'heading-form',
							'id'=>'form_filter',
							'role' => 'form',
							'url' => route('report.monitorReceivement'),
							'method' => 'get',
						))
					!!}
					<div class="form-group">
						@include('form.date', [
							'field' => 'start_date',
							'label' => 'FROM (00:00)',
							'class' => ' datepicker',
							'default'=> $start_date,
							'placeholder' => 'dd/mm/yyyy',
							'attributes' => [
								'id' => 'start_date',
								'autocomplete' => 'off'
							]
						])
					</div>
					<div class="form-group">
						@include('form.date', [
							'field' => 'end_date',
							'label' => 'TO (23:59)',
							'class' => ' datepicker',
							'default'=> $end_date,
							'placeholder' => 'dd/mm/yyyy',
							'attributes' => [
								'id' => 'end_date',
								'autocomplete' => 'off'
							]
						])
					</div>

					
					<div class="heading-btn">
						{{-- <button type="submit" class="btn btn-default">FILTER</button> --}}
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.exportMonitorReceivement'),
							'method' => 'get',
							//'id' => 'form'
						))
				!!}
					{!! Form::hidden('__start_date',$start_date, array('id' => '__start_date')) !!}
					{!! Form::hidden('__end_date',$end_date, array('id' => '__end_date')) !!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-success" id="exportExcel">Export <i class="icon-file-excel position-left"></i></button>
					</div>
				{!! Form::close() !!}
				{!!
					Form::open(array(
							'class' => 'heading-form',
							'role' => 'form',
							'url' => route('report.exportMonitoringReceiving'),
							'method' => 'get',
							'target' => '_blank'
						))
				!!}
					{!! Form::hidden('_start_date',$start_date, array('id' => '_start_date')) !!}
					{!! Form::hidden('_end_date',$end_date, array('id' => '_end_date')) !!}
					<div class="from-group text-right">
						<button type="submit" class="btn btn-info" id="print">Print Preview <i class="icon-printer position-left"></i></button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=> 'mrp-datatable']) !!}
			</div>
		</div>
	</div>

@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_receiving.js'))}}"></script>
@endsection
