@extends('layouts.app', ['active' => 'report_inbound'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">DETIL STOCK</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li><a href="{{ route('report.stock') }}">Stocks</a></li>
					<li >Detail</li>
					<li class="active">{{ $stock->item_code }} - {{ $stock->item_desc}}</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<!--<div class="panel-heading">
			<h5 class="panel-title">STOCK(S)</h5>
		</div>-->

		<div class="panel-body">
			<div class="tabbable">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#basic-tab1" data-toggle="tab" aria-expanded="true">List Data</a></li>
					<li class=""><a href="#basic-tab2" data-toggle="tab" aria-expanded="false">Export to Excel</a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane active" id="basic-tab1">
						<br/>
						<div class="table-responsive">
							{!! $html->table(['class'=>'table datatable-basic']) !!}
						</div>
					</div>

					<div class="tab-pane" id="basic-tab2">
						<br/>
						{!!
							Form::open(array(
								'class' => 'form-signin',
								'role' => 'form',
								'url' => route('report.exportStockReport'),
								'method' => 'get',
								'id' => 'form'
							))
						!!}
						@include('form.date', [
							'field' => 'start_date',
							'label' => 'Start Date (00:00)',
							'class' => ' datepicker',
							'placeholder' => 'dd/mm/yyyy',
							'label_col' => 'col-md-4',
							'form_col' => 'col-sm-8',
							'attributes' => [
								'id' => 'start_date'
							]
						])
						
						@include('form.date', [
							'field' => 'end_date',
							'label' => 'End Date (23:59)',
							'class' => ' datepicker',
							'placeholder' => 'dd/mm/yyyy',
							'label_col' => 'col-md-4',
							'form_col' => 'col-sm-8',
							'attributes' => [
								'id' => 'end_date'
							]
						])
						<div class="from-group text-right">
							<button type="submit" class="btn btn-info btn-md" id="exportExcel" style="margin-top:15px">Export Excel <i class="icon-file-excel position-left"></i></button>
						</div>
						{!! Form::close() !!}
					</div>

					
				</div>
			</div>
		</div>
@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script>
		var url_loading = $('#loading_gif').attr('href');
		$('#form').submit(function (event) {
			$.ajax({
				type: "GET",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: "<img src='" + url_loading + "' />",
						css: {
							backgroundColor: 'transaparant',
							border: 'none',
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
				error: function (response) {
					$.unblockUI();
					$("#alert_error").trigger("click", 'Please Contact ICT');
				}
				
			});
			
		});
	</script>
@endsection
