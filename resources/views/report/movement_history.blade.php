@extends('layouts.app', ['active' => 'report_inbound'])

@section('content')
	<div class="page-title" style="padding-top:0px">
			<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">Report</span></h4>

		<ul class="breadcrumb breadcrumb-caret position-left">
			<li><a href="{{ route('dashboard') }}">Report</a></li>
			<li><a href="{{ route('report.stock') }}">Stock</a></li>
			<li><a href="{{ route('report.detilstock',$stock_id) }}">Detail Stock</a></li>
			<li class="active">History Movement</li>
		</ul>

	</div>
	@foreach ($stock_lines as $key_header => $stock_line)
		<div class="panel panel-default border-grey">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="panel-heading">
							<h5 class="panel-title">Item Information</h5>
						</div>
						<div class="panel-body panel-details">
							<b>No Packing List:</b> {{ $stock_line->no_packing_list }}
							<br/>
							<b>No Resi:</b> {{ $stock_line->no_resi }}
							<br/>
							<b>No Surat Jalan:</b> {{ $stock_line->no_surat_jalan }}
							<br/>
							<b>No Invoice:</b> {{ $stock_line->no_invoice }}
							<br/>
							<b>Qty Ordered:</b> {{ sprintf('%0.2f',$stock_line->qty_ordered) }}
							<br/>
							<b>UoM PO:</b> {{  $stock_line->uom }}
							<br/>
							<b>UoM Conversion:</b> {{  $stock_line->uom_conversion }}
							<br/>
							<b>Qty Received:</b> {{  sprintf('%0.2f',$stock_line->qty_conversion) }}
							<br/>
							<b>ETD:</b> {{ $stock_line->etd_date->format('d-M-Y') }}
							<br/>
							<b>ETA:</b> {{ $stock_line->eta_date->format('d-M-Y') }}
							<br/>
							<b>Receive Date:</b> {{ $stock_line->created_at->format('d-M-Y') }}
							<br/>
							<b>Status:</b> @if ($stock_line->is_active  == 1)
								<span class="label label-success">Stock Available</span>
							@else
								<span class="label label-danger">Stock Already Out</span>
							@endif
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12">
						<div class="panel-heading">
							<h5 class="panel-title">Movement History</h5>
						</div>
						<div class="panel-body panel-details">
							<div class="table-responsive">
								<table class="table">
									<tbody>
									@foreach ($stock_line->StockMovementLines()->orderBy('date_movement','asc')->get() as $key => $movement_history)
										<tr>
											@if ($movement_history->checkStockMovement($movement_history->stock_movement_id)->status == 'in')<!-- && $movement_history->checkStockMovement($movement_history->stock_movement_id)->from_location == $rcv_rack->id -->
												<td> Item diterima dari {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_from)[0] }} - {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_from)[1] }} dan di masukan kedalam area {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_destination)[0] }} - {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_destination)[1] }} sebanyak {{ $movement_history->qty_movement }} pada tanggal {{ $movement_history->date_movement->format('d/M/Y') }} oleh {{ $movement_history->user->name }}</td>
											@php $qty_rcv = $movement_history->qty_movement; @endphp
											@elseif ($movement_history->checkStockMovement($movement_history->stock_movement_id)->status == 'out')
												@php $qty_rcv -= $movement_history->qty_movement; @endphp
											
												<td> Item dikeluarkan dari area {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_from)[0] }} - {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_from)[1] }} ke {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_destination)[0] }} {{ explode('#',$movement_history->checkStockMovement($movement_history->stock_movement_id)->code_destination)[1] }} sebanyak {{ $movement_history->qty_movement }} untuk po buyer {{ $movement_history->checkStockMovement($movement_history->stock_movement_id)->po_buyer }} pada tanggal {{ $movement_history->date_movement->format('d/M/Y') }}  oleh {{ $movement_history->user->name }}
													sisa stok {{ $qty_rcv }}.
												</td>
											@endif	
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
		</div>
	@endforeach
@endsection

@section('content-js')
@endsection
