@extends('layouts.app', ['active' => 'movement'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MATERIAL MOVEMENTS</span></h4>

						<ul class="breadcrumb breadcrumb-caret position-left">
							<li>Report</li>
							<li class="active">Material Movements</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk melihat laporan pergerakan material.
				</div>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">MOVEMENT(S) <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				{!!
					Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'url' => route('report.movement'),
						'method' => 'get',
						))
				!!}
				<div class="form-group">
						<input type="text" value="{{ $po_buyer }}" class="form-control" name="po_buyer" id="po_buyer" placeholder="INPUT PO BUYER">
					</div>
				<div class="heading-btn">
					<button type="submit" class="btn btn-info">FILTER</button>
				{!! Form::close() !!}
				</div>
				
				{!!
					Form::open(array(
						'class' => 'heading-form',
						'role' => 'form',
						'url' => route('report.exportMovementByPoBuyer'),
						'method' => 'get',
						))
				!!}
				<div class="heading-btn">
					<input type="hidden" value="{{ $po_buyer }}" name="po_buyer" id="_po_buyer">
					<button type="submit" class="btn btn-success">EXPORT <i class="icon-file-excel position-left"></i></button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic','id'=>'movement']) !!}
			</div>
		</div>
	</div>

@endsection

@section('content-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_movement.js'))}}"></script>
@endsection
