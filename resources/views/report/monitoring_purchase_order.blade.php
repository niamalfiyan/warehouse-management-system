@extends('layouts.app', ['active' => 'monitor-purchase-order'])

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MONITORING PURCHASE ORDER</span></h4>
						
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li class="active"><a href="{{ route('dashboard') }}">Monitoring Purchase Order</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini untuk memonitor purchase order.
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title">FILTER CRITERIA<a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-xs-6">
						@include('form.picklist', [
							'field' => 'po_buyer',
							'name' => 'po_buyer',
							'placeholder' => 'Pilih Nomor PO Buyer',
							'title' => 'Pilih Nomor PO Buyer'
						])
					</div>
				</div>
				<br/>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h6 class="panel-title"> &NonBreakingSpace; <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
				<!--<span class="label label-info heading-text">Quantity sisa akan di masukan ke dalam Free Stock.</span>-->
					
				<div class="heading-elements">
					<div class="heading-btn">
						{!!
							Form::open([
								'role' => 'form',
								'url' => route('report.purchaseOrder.exportToExcel'),
								'method' => 'post',
								'enctype' => 'multipart/form-data',
								'class' => 'form-horizontal'
							])
						!!}
							<button type="submit" id="btn_export" class="btn btn-default"> Excel<i class="icon-file-excel position-left"></i></button>
							{!! Form::hidden('purchasing_order','[]' , array('id' => 'purchasing_order')) !!}
						{!! Form::close() !!}	
					</div>
				</div>
			</div>

			<div class="panel-body">
				<a href="{{ route('report.purchaseOrder.list') }}" id="url_get_purchase_order" class="hidden"></a>
							
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>PO SUPPLIER</th>
								<th>ITEM</th>
								<th>PO BUYER</th>
								<th>WAREHOUSE</th>
								<th>CATEGORY</th>
								<th>UOM</th>
								<th>QTY</th>
							</tr>
						</thead>
						<tbody id="tbody-monitoring-purchase-order-table">
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
@endsection

@section('content-modal')
	@include('form.modal_picklist', [
		'name' => 'po_buyer',
		'title' => 'List PO Buyer',
		'placeholder' => 'Silahkan cari berdasarkan PO Buyer'
	])
@endsection

@section('content-js')
	@include('report._monitoring_purchase_order')
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_purchase_order.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
