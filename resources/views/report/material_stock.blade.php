@extends('layouts.app', ['active' => 'report_other_material'])

@section('content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">MATERIAL STOCK</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li><a href="{{ route('dashboard') }}">Report</a></li>
					<li class="active">Material Stock</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="tabbable tab-content-bordered">
				<ul class="nav nav-tabs">
					<li class="@if($active_tab == 'active') active @endif"><a href="#basic-tab1" data-toggle="tab" aria-expanded="false" onclick="changeTab('active')">Active</a></li>
					<li class="@if($active_tab == 'inactive') active @endif"><a href="#basic-tab2" data-toggle="tab" aria-expanded="true" onclick="changeTab('inactive')">Inactive</a></li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane @if($active_tab == 'active') active @endif" id="basic-tab1">
						<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.materialstock'),
											'method' => 'get',
											'id' => 'form_mapping_stock'
									))
								!!}
								<select name="mapping_stock_id" id="mapping_stock_id" class="form-control">
									@foreach($array_mapping_stocks as $array_mapping_stock)
										@if($array_mapping_stock->value == $_mapping_stock)
											<option value="{{$array_mapping_stock->value}}" selected>{{ $array_mapping_stock->name }}</option>
										@else 
										<option value="{{$array_mapping_stock->value}}">{{ $array_mapping_stock->name }}</option>	
										@endif
									@endforeach
								</select>
								<input type="hidden" name="warehouse_id" id="_warehouse_id" value="{{$warehouse_id}}"/>
								{!! Form::close() !!}

								@if(Auth::user()->hasRole(['admin-ict-acc','mm-staff-acc','exim-acc']))
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.materialstock'),
											'method' => 'get',
											'id' => 'form_warehouse'
									))
								!!}
								<input type="hidden" name="mapping_stock_id" id="_mapping_stock" value="{{$_mapping_stock}}"/>
								
								<select name="warehouse_id" id="warehouse_id" class="form-control">
									<option value="1000002" @if($warehouse_id=='1000002') selected @endif>AOI 1</option>
									<option value="1000013" @if($warehouse_id=='1000013') selected @endif>AOI 2</option>
								</select>
									
								{!! Form::close() !!}
								@endif

								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.exportMaterialStock'),
											'method' => 'get',
											'target' => '_blank'		
									))
								!!}
								<button type="submit" class="btn btn-default">Export Stock All<i class="icon-file-excel position-left"></i></button>
								{!! Form::close() !!}

								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.exportMaterialAllocation'),
											'method' => 'get',
											'target' => '_blank'		
									))
								!!}
								<button type="submit" class="btn btn-default">Export Allocation All<i class="icon-file-excel position-left"></i></button>
								{!! Form::close() !!}
								
								@if(Auth::user()->hasRole(['admin-ict-acc']))
								<ul class="icons-list">
									<li class="dropdown open">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-popup="tooltip" title="Bulk Process" aria-expanded="true"><i class="icon-three-bars"></i></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="{{ route('report.materialstockdeleteBulk')}}">Delete</a></li>
											<li><a href="#">Change Type</a></li>
										</ul>
									</li>
								</ul>
								@endif
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="activeTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>SOURCE</th>
											<th>ITEM</th>
											<th>LOCATION</th>
											<th>QUANTITY</th>
											<th>ACTION</th>
											
											<th>SUPPLIER CODE</th>
											<th>DOCUMENT NO</th>
											<th>ITEM CODE</th>
											<th>PO BUYER</th>
											<th>WAREHOUSE ID</th>
											<th>WAREHOUSE NAME</th>
											<th>SOURCE</th>
											<th>TYPE STOCK</th>
											<th>CATEGORY</th>
											<th>AVAILABLE QTY</th>
											
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane @if($active_tab == 'inactive') active @else fade @endif" id="basic-tab2">
					<div class="panel-heading">
							<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
							<div class="heading-elements">
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.materialstock'),
											'method' => 'get',
											'id' => 'form_mapping_stock'
									))
								!!}
								<select name="mapping_stock_id" id="inactive_mapping_stock_id" class="form-control">
									@foreach($array_mapping_stocks as $array_mapping_stock)
										@if($array_mapping_stock->value == $_mapping_stock)
											<option value="{{$array_mapping_stock->value}}" selected>{{ $array_mapping_stock->name }}</option>
										@else 
										<option value="{{$array_mapping_stock->value}}">{{ $array_mapping_stock->name }}</option>	
										@endif
									@endforeach
								</select>
								<input type="hidden" name="warehouse_id" id="_inactive_warehouse_id" value="{{$warehouse_id}}"/>
								{!! Form::close() !!}

								@if(Auth::user()->hasRole(['admin-ict-acc','mm-staff-acc']))
								{!!
									Form::open(array(
											'class' => 'heading-form',
											'role' => 'form',
											'url' => route('report.materialstock'),
											'method' => 'get',
											'id' => 'form_warehouse'
									))
								!!}
								<input type="hidden" name="mapping_stock_id" id="_inactive_mapping_stock" value="{{$_mapping_stock}}"/>
								
								<select name="warehouse_id" id="inactive_warehouse_id" class="form-control">
									<option value="1000002" @if($warehouse_id=='1000002') selected @endif>AOI 1</option>
									<option value="1000013" @if($warehouse_id=='1000013') selected @endif>AOI 2</option>
								</select>
									
								{!! Form::close() !!}
								@endif

								<ul class="icons-list">
									<li><a href="#" data-action="move" data-popup="tooltip" title="Move panel"></a></li>
									<li><a href="#" data-action="modal" data-toggle="modal" data-target="#settings" data-popup="tooltip" title="Modal"></a></li>
									<li class="dropdown open">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-popup="tooltip" title="Menu" aria-expanded="true"><i class="icon-three-bars"></i></a>
										<ul class="dropdown-menu dropdown-menu-right">
											<li><a href="#">Action</a></li>
											<li><a href="#">Another action</a></li>
											<li><a href="#">Something else here</a></li>
											<li class="divider"></li>
											<li><a href="#">One more separated line</a></li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-basic table-condensed" id="inactiveTable">
									<thead>
										<tr>
											<th>ID</th>
											<th>SOURCE</th>
											<th>ITEM</th>
											<th>LOCATION</th>
											<th>QUANTITY</th>
											<th>ACTION</th>
											
											<th>SUPPLIER CODE</th>
											<th>DOCUMENT NO</th>
											<th>ITEM CODE</th>
											<th>PO BUYER</th>
											<th>LOCATOR</th>
											<th>WAREHOUSE</th>
											<th>SOURCE</th>
											<th>TYPE STOCK</th>
											<th>CATEGORY</th>
										</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
@endsection

@section('content-modal')
	@include('report._modal')
	@include('report._change_type_modal')
	@include('report._transfer_stock_modal')
	@include('report._move_locator_modal')
	@include('report._update_stock_modal')
	@include('report._delete_stock_modal')
	
	@include('form.modal_picklist', [
		'name' => 'locator',
		'title' => 'Locator',
		'placeholder' => 'Cari berdasarkan Locator',
	])
@endsection

@section('content-js')
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/monitoring_material_stock.js'))}}"></script>
@endsection
