@extends('layouts.app', ['active' => 'fabric_material_testing'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Testing</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Testing</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@role(['admin-ict-fabric'])
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 			=> [
							'1000001' 		=> 'Warehouse Fabric AOI 1',
							'1000011' 		=> 'Warehouse Fabric AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				@else 
					{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
				@endrole
				
			<button type="button" class="btn btn-default col-xs-12 hidden" style="margin-top: 15px" id="selectAll">Select All <i class="icon-checkmark position-right"></i></button>
		</div>
	</div>

	<div class="panel panel-default border-grey">
	<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover " id="fabric_report_material_inspect_lab_table">
					<tbody id="tbody-reject">
					</tbody>
				</table>
			</div>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('fabricMaterialTesting.store'),
					'method' 	=> 'post',
					'enctype' 	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}

			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('_selectAll',0, array('id' => '_selectAll')) !!}
			{!! Form::hidden('barcodes','[]' , array('id' => 'barcode-header')) !!}
			{!! Form::hidden('url_fabric_material_fabric_testing_create',route('fabricMaterialTesting.create') , array('id' => 'url_fabric_material_fabric_testing_create')) !!}
			{!! Form::hidden('status_release','', array('id' => 'status_release')) !!}
			{!! Form::hidden('_is_release','0', array('id' => '_is_release')) !!}

			<a href="{{ route('temporary.delete') }}" id="url_temporary_delete" class="hidden"></a>
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
	</div>
@endsection

@section('page-js')
	@include('fabric_material_testing._barcode')
	<script src="{{ mix('js/fabric_material_testing.js') }}"></script>
@endsection
