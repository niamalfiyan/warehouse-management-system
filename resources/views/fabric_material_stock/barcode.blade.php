
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: PRINT BARCODE STOCK OTHER</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach ($material_stocks as $key_1 => $material_stock)
			<div class="outer">
				<div class="title">
					<span style="@if(strlen($material_stock->document_no)>30) font-size: 12px @else font-size: 25px @endif">{{ $material_stock->document_no }}</span>
				</div>
				<div class="isi">
					<div class="isi1">
						<span style="
							@if(strlen($material_stock->item_code)>40) 
								font-size: 12px
						 	@else
								font-size: 20px 
							@endif">{{ $material_stock->item_code }}</span>
					</div>
					<div class="isi1" style="
						@if(strlen($material_stock->color)>100 && strlen($material_stock->color)<150) 
							font-size: 12px;line-height:0.3cm;
						@elseif(strlen($material_stock->color)>150 && strlen($material_stock->color)<175) 
							height:40px; 
						@elseif(strlen($material_stock->color)>175 && strlen($material_stock->color)<200) 
							height:75px  
						@endif">
						<span style="@if(strlen($material_stock->color)>19) 
							font-size: 7px;word-wrap: break-word;
							@else 
								font-size: 30px 
							@endif">{{ $material_stock->color }}</span>
					</div>
					<div class="isi1">
						<span style="@if(strlen($material_stock->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $material_stock->batch_number }}</span>
					</div>
					
					<div class="isi2">
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->supplier_name }}  </span>
						</div>
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 20px;word-wrap: break-word;">{{ number_format($material_stock->available_qty, 2, '.', ',') }} {{ strtoupper($material_stock->uom) }}   </span>
						</div>
						<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/><span style="@if(strlen($material_stock->nomor_roll)>=4) font-size: 35px @else font-size: 70px @endif">{{ $material_stock->nomor_roll }}</p>
					
					</div>
					<div class="isi3">
						<div class="block-kiri" style="height: 150px;">
							<img style="height: 2.5cm; width: 4.3cm; margin-top: 35px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_stock->barcode_supplier", 'C128') }}" alt="barcode"   />			
							<br/><span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->barcode_supplier }}</span>
				
						</div>
						
						<div class="block-kiri">
							Created At {{ $material_stock->created_at->format('d/M/y H:i:s') }}<br/>
							Created By {{ $material_stock->user->name }}
						</div>
						
					</div>
				</div>
						
			</div>
			<div class="outer_2">
				<div class="isi_2">
					<div class="block-kiri_2">
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->document_no }}::{{ $material_stock->item_code }}::{{ $material_stock->batch_number }}</span>
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->nomor_roll }}::{{ number_format($material_stock->available_qty, 2, '.', ',') }} ({{ strtoupper(trim($material_stock->uom)) }})</span>
					</div>
				</div>
				<div class="isi1_2">
					<center><span style="font-size: 7px;word-wrap: break-word;">{{ $material_stock->color }}</span></center>
					<img style="height: 1cm; width: 5.3cm;	 margin-top: 5px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_stock->barcode_supplier", 'C128') }}" alt="barcode"   />			
					<center><span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->barcode_supplier }}</span></center>
				
				</div>
			</div>
			<div class="outer_2">
				<div class="isi_2">
					<div class="block-kiri_2">
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->document_no }}::{{ $material_stock->item_code }}::{{ $material_stock->batch_number }}</span>
						<span style="font-size: 10px;word-wrap: break-word;">{{ $material_stock->nomor_roll }}::{{ number_format($material_stock->available_qty, 2, '.', ',') }} ({{ strtoupper(trim($material_stock->uom)) }})</span>
					</div>
				</div>
				<div class="isi1_2">
					<center><span style="font-size: 7px;word-wrap: break-word;">{{ $material_stock->color }}</span></center>
					<img style="height: 1cm; width: 5.3cm;	 margin-top: 5px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_stock->barcode_supplier", 'C128') }}" alt="barcode"   />			
					<center><span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->barcode_supplier }}</span></center>
				
				</div>
			</div>
		@endforeach
		
    </body>
</html>