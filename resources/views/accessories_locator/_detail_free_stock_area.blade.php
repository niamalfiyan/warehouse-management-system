<div id="detilFreeStockModal" data-backdrop="static" data-keyboard="false" class="modal fade" style="color:black !important">
	<div class="modal-dialog modal-full">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Detail Locator <span id="detail_header_free_stock_area"></span></h5>
			</div>

			<div class="modal-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover table-responsive" id="detail_free_stock_table">
						<thead>
							<tr>
								<th>Po Supplier</th>
								<th>Po Buyer</th>
								<th>Item Code</th>
								<th>Uom</th>
								<th>Qty Available</th>
							</tr>
						</thead>
					</table>
				</div>
				
			</div>
			
			{!! Form::hidden('locator_free_stock_area_id',null, array('id' => 'locator_free_stock_area_id')) !!}
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <i class="glyphicon glyphicon-remove"></i> </button>
			</div>
		</div>
	</div>
</div>
