<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Area</th>
			<th>Location</th>
			<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->area->name }}
				</td>
				<td>
					{{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-name="{{ $list->area->name }} - {{ $list->rack }}.{{ $list->y_row }}.{{ $list->z_column }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
