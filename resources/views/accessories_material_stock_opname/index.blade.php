@extends('layouts.app', ['active' => 'accessories_material_stock_opname'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock Opname</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Stock Opname</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'' 				=> '-- Select Warehouse --',
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole
		
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<th>#</th>
							<th>Locator</th>
							<th>Statistical Date</th>
							<th>Season</th>
							<th>Po Buyer</th>
							<th>Po Supplier</th>
							<th>Item</th>
							<th>Qty On Barcode</th>
						</tr>
					</thead>
					<tbody id="tbody_accessories_material_stock_opname">
					</tbody>
				</table>
			</div>
			{!!
				Form::open([
					'role' => 'form',
					'url' => route('accessoriesMaterialStockOpname.store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data',
					'class' => 'form-horizontal',
					'id'=> 'form'
				])
			!!}

			@include('form.picklist', [
						'field' 		=> 'locator_in_accesories',
						'name' 			=> 'locator_in_accesories',
						'label' 		=> 'Locator',
						'readonly' 		=> true,
						'placeholder' 	=> 'Please select locator',
						'title' 		=> 'Please select locator',
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
					])

				{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('url_sto_create_accessories',route('accessoriesMaterialStockOpname.create'), array('id' => 'url_sto_create_accessories')) !!}
				{!! Form::hidden('_to_destination_id', isset($hidden_value) ? $hidden_value : null, ['id' => '_to_destination_id']) !!}
				<div class="form-group text-right">
					<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'locator_in_accesories',
		'title' => 'List Locator',
		'placeholder' => 'Search based on name / Code',
	])
@endsection

@section('page-js')
	@include('accessories_material_stock_opname._item')
	<script src="{{ mix('js/accessories_material_stock_opname.js') }}"></script>
@endsection
