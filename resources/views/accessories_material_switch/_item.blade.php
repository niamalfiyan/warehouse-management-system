<script type="x-tmpl-mustache" id="allocation-accessories-table">
	{% #item %}
		<tr {% #flag_error %} style="background-color:#FF5722;color:#fff" {% /flag_error %}>
			<td>{% no %}</td>
			<td>{% po_supplier_from %}</td>
			<td>{% po_buyer_from %}</td>
			<td>{% po_supplier_to %}</td>
			<td>{% po_buyer_to %}</td>
			<td>{% item_code %}</td>
			<td>{% uom %}</td>
			<td>{% qty_switch %}</td>
			<td>{% result %}</td>
		</tr>
	{%/item%}
</script>