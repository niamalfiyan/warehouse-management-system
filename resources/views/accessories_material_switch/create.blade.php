@extends('layouts.app', ['active' => 'switch-allocation-accessories'])

@section('page-content')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title" style="padding-top:0px;">
				<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">UPLOAD</span></h4>

				<ul class="breadcrumb breadcrumb-caret position-left">
					<li>Allocation</li>
					<li><a href="{{ route('accessoriesMaterialSwitch.index') }}">Switch</a></li>
					<li class="active">Upload </li>
				</ul>
				</div>
		</div>
	</div>
	
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp </h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a class="hidden" href="{{ route('accessoriesMaterialSwitch.index') }}" id="url_switch_allocation_accessories_index"></a>
					<a class="btn btn-primary btn-icon" href="{{ route('accessoriesMaterialSwitch.exportFileUpload') }}"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-info btn-icon" id="upload_button"><i class="icon-upload"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>NO</th>
							<th>NO PO SUPPLIER FROM</th>
							<th>PO BUYER FROM</th>
							<th>NO PO SUPPLIER TO</th>
							<th>PO BUYER TO</th>
							<th>ITEM CODE</th>
							<th>UOM</th>
							<th>QTY SWITCH</th>
							<th>RESULT</th>
						</tr>
					</thead>
					<tbody id="tbody-upload-allocation-accessories">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<hr>
	

	{!!
    Form::open([
        'role' => 'form',
        'url' => route('accessoriesMaterialSwitch.import'),
        'method' => 'post',
        'id' => 'upload_file_allocation',
        'enctype' => 'multipart/form-data'
    ])
	!!}
		<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
		<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
	{!! Form::close() !!}

	{!! Form::hidden('allocations','[]',array('id' => 'allocations')) !!}
			
@endsection

@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/switch_allocation_accessories.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	@include('accessories_material_switch._item')
@endsection