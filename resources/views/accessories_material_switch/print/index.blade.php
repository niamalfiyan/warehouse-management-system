@extends('layouts.app', ['active' => 'accessories_barcode_switch'])

@section('page-content')
	<div class="page-header">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="page-header-content">
					<div class="page-title" style="padding-top:0px">
						<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">PRINT BARCODE SWITCH</span></h4>
						<ul class="breadcrumb breadcrumb-caret position-left">
							<li><a href="{{ route('dashboard') }}">Barcode</a></li>
							<li class="active">Print Barcode Switch</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12">
				<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
					<h6 class="alert-heading text-semibold">Information</h6>
					Menu ini digunakan untuk mencetak barcode baru untuk setiap material yang ditukar </code>
				</div>
			</div>
		</div>
	</div>



	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>DOCUMENT NO</th>
							<th>PO BUYER</th>
							<th>ITEM CODE</th>
							<th>UOM</th>
							<th>QTY SWITCH</th>
							<th>NOTE</th>
							<th>ACTION</th>
						</tr>
					</thead>
					<tbody id="tbody-print-barcode-switch">
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<hr>
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('printBarcodeSwitch.printout'),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
			'class' => 'form-horizontal',
			'id'=> 'form'
		])
	!!}
		<a href="{{ route('printBarcodeSwitch.create') }}" id="url_get_print_barcode_switch_accessories" class="hidden"></a>
		{!! Form::hidden('material_print_barcode_switches','[]' , array('id' => 'material_print_barcode_switch')) !!}
		<div class="form-group text-right" style="margin-top: 10px;">
			<button type="submit" class="btn btn-success btn-lg">SAVE <i class="icon-floppy-disk position-left"></i></button>
		</div>
	{!! Form::close() !!}
	{!!
		Form::open([
			'role' => 'form',
			'url' => route('printBarcodeSwitch.showBarcode'),
			'method' => 'post',
			'class' => 'form-horizontal',
			'id'=> 'getBarcode',
			'target' => '_blank'
		])
	!!}
	{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
	<div class="form-group text-right" style="margin-top: 10px;">
		<button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
	</div>
	{!! Form::close() !!}	
@endsection

@section('page-js')
	@include('accessories_material_switch.print._item')
	<script type="text/javascript" src="{{ asset(elixir('js/print_barcode_switch_accessories.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
@endsection
