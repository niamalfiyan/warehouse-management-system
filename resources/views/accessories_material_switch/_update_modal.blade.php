<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'updateformnya', 'class' => 'form-horizontal', 'url' => '#']) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">EDIT SWITCH FORM</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'po_supplier_source',
								'label' => 'PO SUPPLIER AWAL',
								'placeholder' => 'Silahkan masukan po supplier asal',
								'attributes' => [
									'id' => 'po_supplier_source'
								]
							])
						</div>
					   <div class="col-md-12">
						     @include('form.text', [
								'field' => 'po_supplier_new',
								'label' => 'PO SUPPLIER BARU',
								'placeholder' => 'Silahkan masukan po supplier baru',
								'attributes' => [
									'id' => 'po_supplier_new'
								]
							])
						</div>
						<div class="col-md-12">
						     @include('form.text', [
								'field' => 'item_code',
								'label' => 'KODE ITEM',
								'placeholder' => 'Silahkan masukan kode item',
								'attributes' => [
									'id' => 'item_code',
									//'readonly' => 'readonly'
								]
							])
						</div>
						<div class="col-md-12">
						     @include('form.text', [
								'field' => 'po_buyer',
								'label' => 'PO BUYER',
								'placeholder' => 'Silahkan masukan po buyer',
								'attributes' => [
									'id' => 'po_buyer',
									//'readonly' => 'readonly'
								]
							])
						</div>
						<div class="col-md-12">
						     @include('form.text', [
								'field' => 'qty_switch',
								'label' => 'QTY SWITCH',
								'placeholder' => 'Silahkan masukan qty yang ditukar',
								'attributes' => [
									'id' => 'qty_switch',
									//'readonly' => 'readonly'
								]
							])
						</div>
						{{--<div class="col-md-12">
						     @include('form.text', [
								'field' => 'warehouse_inventory_source',
								'label' => 'GUDANG INVENTORI AWAL',
								'placeholder' => 'Silahkan masukan gudang inventori awal',
								'attributes' => [
									'id' => 'warehouse_inventory_source',
									//'readonly' => 'readonly'
								]
							])
						</div>
						<div class="col-md-12">
						     @include('form.text', [
								'field' => 'warehouse_inventory_new',
								'label' => 'GUDANG INVENTORI BARU',
								'placeholder' => 'Silahkan masukan gudang inventori baru',
								'attributes' => [
									'id' => 'warehouse_inventory_new',
									//'readonly' => 'readonly'
								]
							])
						</div>--}}
					</div>
				</div>
                <div class="modal-footer text-center">
					{!! Form::hidden('id','', array('id' => 'update_id')) !!}
					<button type="submit" class="btn btn-success">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>