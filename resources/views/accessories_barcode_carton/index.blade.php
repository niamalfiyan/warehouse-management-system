@extends('layouts.app', ['active' => 'accessories_barcode_carton'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Carton</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Barcode Carton</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
		<div class="panel panel-default border-grey">
			<div class="panel-heading">
				<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
				<div class="heading-elements">
					<ul class="icons-list">
						<li><a data-action="collapse"></a></li>
					</ul>
				</div>
			</div>
			<div class="panel-body">
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'' 				=> '-- Select Warehouse --',
						'1000002' 		=> 'Warehouse Accessories AOI 1',
						'1000013' 		=> 'Warehouse Accessories AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			</div>
		</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole
	
	
	<div class="panel panel-default border-grey">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-responsive">
						<thead>
							<tr>
								<th>#</th>
								<th>Season</th>
								<th>Source</th>
								<th>Po Buyer</th>
								<th>Item</th>
								<th>Style</th>
								<th>Article</th>
								<th>Qty On Barcode</th>
								<th>Total Package</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody id="tbody_accessories_barcode_carton">
						</tbody>
					</table>
				</div>
				
			
			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('accessoriesBarcodeCarton.store'),
					'method' 	=> 'post',
					'enctype'	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}
			
			{!! Form::hidden('url_accessories_barcode_carton_create',route('accessoriesBarcodeCarton.create'), ['id' => 'url_accessories_barcode_carton_create']) !!}
			{!! Form::hidden('material_barcode_carton_accessories','[]' , array('id' => 'material_barcode_carton_accessories')) !!}
			
			<div class="form-group text-right" style="margin-top:10px">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
			
			{!! Form::close() !!}
		</div>


		{!!
			Form::open([
				'role' 		=> 'form',
				'url' 		=> route('accessoriesBarcodeCarton.barcode'),
				'method' 	=> 'post',
				'class' 	=> 'form-horizontal',
				'target' 	=> '_blank',
				'id'		=> 'getBarcode'
			])
		!!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('list_barcodes','[]',array('id' => 'list_barcodes')) !!}
		{!! Form::close() !!}
		
	</div>

@endsection

@section('page-js')
	@include('accessories_barcode_carton._item')
	<script src="{{ mix('js/accessories_barcode_carton.js') }}"></script>
@endsection
