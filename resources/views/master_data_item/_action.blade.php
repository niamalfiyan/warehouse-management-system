
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($uomConversion))
                <li><a href="#" onclick="uomConversion('{!! $uomConversion !!}')"><i class="icon-calculator3"></i> Uom Conversion</a></li>
            @endif

        </ul>
    </li>
</ul>
