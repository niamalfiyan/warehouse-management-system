@extends('layouts.app', ['active' => 'master_data_item'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Item</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li class="active">Item</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a data-toggle="modal" data-target="#erpItemPicklistModal"><i class="icon-sync pull-right"></i> Sync</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			@include('form.select', [
				'field' => 'category',
				'label' => 'Category',
				'default' => $selected_category,
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					'' => '-- Select Category --',
				]+$categories,
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_category'
				]
			])
			
		</div>
	</div>
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover " id="master_data_item_table">
					<thead>
						<tr>
							<th>id</th>
							<th>Item id</th>
							<th>Upc</th>
							<th>Item Code</th>
							<th>Item Desc</th>
							<th>Category</th>
							<th>Composition</th>
							<th>Uom</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection

@section('page-modal')
	@include('master_data_item._detail_modal')
	@include('form.modal_picklist', [
		'name' 			=> 'erpItemPicklist',
		'title' 		=> 'List Item Erp',
		'placeholder' 	=> 'Search based on code / name',
	])
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_item.js') }}"></script>
@endsection
