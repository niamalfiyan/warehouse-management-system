@extends('layouts.app', ['active' => 'master_data_auto_allocation'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Auto Allocation</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataAutoAllocation.index') }}">Auto Allocation</a></li>
				<li class="active">Create</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('masterDataAutoAllocation.poSupplierAll') }}"><i class="icon-file-download2 pull-right"></i>Po Supplier</a></li>
						<li><a href="{{ route('masterDataAutoAllocation.exportToExcel') }}"><i class="icon-file-download2 pull-right"></i> Export All</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
					<a class="btn btn-primary btn-icon" href="{{ route('masterDataAutoAllocation.exportFormInput')}}" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('masterDataAutoAllocation.uploadFormInput'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			{!!
			    Form::open([
					'role' 		=> 'form',
					'url' 		=> route('masterDataAutoAllocation.store'),
					'method' 	=> 'post',
					'enctype' 	=> 'multipart/form-data',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
			    ])
			!!}
			{!! Form::hidden('flag',null , array('id' => 'flag')) !!}
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>NO</th>
							<th>Booking Number</th>
							<th>Type Stock Based On Buyer</th>
							<th>Po Supplier</th>
							<th>Item Code Source</th>
							<th>Item Code Book</th>
							<th>Po Buyer</th>
							<th>Qty</th>
							<th>Warehouse Inventory</th>
							<th>Additional</th>
							<th>Remark</th>
							<th>Upload Result</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="tbody-auto-allocation">
					</tbody>
				</table>
			</div>

			<div class="form-group text-right" style="margin-top: 10px;">
				{!! Form::hidden('new_auto_allocations','[]' , array('id' => 'new_auto_allocations')) !!}
				<button type="submit" class="btn col-xs-12 btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
			
			{!! Form::close() !!}
		</div>
	</div>
	<a href="{{ route('masterDataAutoAllocation.index') }}" id="url_auto_allocation_index" class="hidden"></a>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' => 'item',
		'title' => 'List Item',
		'placeholder' => 'Search based on Name / Code',
	])

	@include('form.modal_picklist', [
		'name' => 'item_code_source',
		'title' => 'List Item Source',
		'placeholder' => 'Search based on Name / Code',
	])

	@include('form.modal_picklist', [
		'name' => 'document_no',
		'title' => 'List Po Supplier',
		'placeholder' => 'Search based on po supplier',
	])

	@include('form.modal_picklist', [
		'name' => 'po_buyer',
		'title' => 'List Po Buyer',
		'placeholder' => 'Search based on po buyer',
	])
@endsection

@section('page-js')
	@include('master_data_auto_allocation._item')
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_auto_allocation.js'))}}"></script>
@endsection
