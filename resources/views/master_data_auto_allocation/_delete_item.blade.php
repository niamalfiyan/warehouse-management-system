<script type="x-tmpl-mustache" id="auto-allocation-table">
	{% #item %}
		<tr {% #error_upload %} style="background-color:#fab1b1" {% /error_upload %}>
			<td>{% no %}</td>
			<td>{% document_no %}</td>
			<td>{% item_code %}</td>
			<td>{% po_buyer %}</td>
			<td>{% qty_allocation %} ({% uom %})</td>
			<td>{% warehouse_name %}</td>
			<td>{% remark %}</td>
		</tr>
	{%/item%}
</script>
