
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($historyModal))
                <li><a href="#" onclick="history('{!! $historyModal !!}')" ><i class="icon-history"></i> History</a></li>
            @endif

            @if (isset($recalculate))
                <li><a href="#" onclick="recalculate('{!! $recalculate !!}')"><i class="icon-calculator3"></i> Recalculate</a></li>
            @endif
            
            @if (isset($edit))
                <li><a href="#" onclick="edit('{!! $edit !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif

            @if (isset($delete))
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="icon-trash"></i> Delete</a></li>
            @endif

            @if (isset($cancelAutoAllocation))
                <li><a href="#" onclick="cancel('{!! $cancelAutoAllocation !!}')"><i class=" icon-x"></i> Cancel</a></li>
            @endif

            @if (isset($allocatingStock))
                <li><a href="#" onclick="allocating('{!! $allocatingStock !!}')"><i class=" icon-pie-chart"></i> Allocating</a></li>
            @endif

           

        </ul>
    </li>
</ul>
