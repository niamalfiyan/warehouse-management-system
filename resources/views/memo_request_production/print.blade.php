<html>
<head>
	<title>PRINT :: MRP</title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_mrp.css')) }}">

</head>
<body>
	<div class="outer">
		<div class="header">
			<div class="header-tengah">LAPORAN MRP</div>
		</div>
		<div class="isi">
			<table border="1px" cellspacing="0" style="width: 100%">
				<thead style="background-color: #2196F3;color:black">
					<tr>
						<th class="text-center">#</th>
						<th>TANGGAL PO DD</th>
						<th>STATUS BACKLOG</th>
						<th>STYLE</th>
						<th>ARTICLE</th>
						<th>PO BUYER</th>
						<th>KATEGORI ITEM</th>
						<th>KODE ITEM</th>
						<th>QTY NEED</th>
						<th>UOM</th>
						<th>STATUS TERAKHIR</th>
						<th>WAREHOUSE</th>
						<th>LOKASI TERAKHIR</th>
						<th>TANGGAL PERPINDAHAN TERAKHIR</th>
						<th>USER YANG MELAKUKAN PERPINDAHAN TERAKHIR</th>
						<th>QTY AVAILABLE</th>
						<th>QTY SUPPLY</th>
						<th>PO SUPPLIER</th>
						<th>REMARK</th>
						<th>SYSTEM LOG</th>
					</tr>
				</thead>
				<tbody>
					@foreach($mrp as $key => $data)
						@php
							if($data->last_status_movement == 'receiving')
								$last_status_movement = 'BARANG DITERIMA DIGUDANG';
							else if($data->last_status_movement == 'not receive')
								$last_status_movement = 'BARANG BELUM DITERIMA DIGUDANG';
							else if($data->last_status_movement == 'in')
								$last_status_movement = 'MATERIAL SIAP SUPLAI';
							elseif($data->last_status_movement == 'expanse')
								$last_status_movement = 'EXPANSE<';
							elseif($data->last_status_movement == 'out')
								$last_status_movement = 'MATERIAL SUDAH DISUPLAI';
							elseif($data->last_status_movement == 'out-handover')
								$last_status_movement = 'MATERIAL PINDAH TANGAN';
							elseif($data->last_status_movement == 'out-subcont')
								$last_status_movement = 'MATERIAL SUDAH DISUPLAI KE SUBCONT';
							elseif($data->last_status_movement == 'reject')
								$last_status_movement = 'REJECT';
							elseif($data->last_status_movement == 'change')
								$last_status_movement = 'MATERIAL PINDAH LOKASI RAK';
							elseif($data->last_status_movement == 'hold')
								$last_status_movement = 'MATERIAL DITAHAN QC UNTUK PENGECEKAN';
							elseif($data->last_status_movement == 'adjustment' || $data->last_status_movement == 'adjusment')
								$last_status_movement = 'PENYESUAIAN QTY';
							elseif($data->last_status_movement == 'print')
								$last_status_movement = 'PRINT BARCODE';
							elseif($data->last_status_movement == 'reroute')
								$last_status_movement = 'REROUTE';
							elseif($data->last_status_movement == 'cancel item')
								$last_status_movement = 'CANCEL ITEM';
							elseif($data->last_status_movement == 'cancel order')
								$last_status_movement = 'CANCEL ORDER';
							elseif($data->last_status_movement == 'check')
								$last_status_movement = 'MATERIAL DALAM PENGECEKAN QC';
							else if($data->last_status_movement == 'out-cutting')
								$last_status_movement = 'MATERIAL SUDAH DISUPLAI KE CUTTING';    
							else if($data->last_status_movement == 'relax')
								$last_status_movement = 'RELAX CUTTING';   
							else
								$last_status_movement = $data->last_status_movement;
									
						@endphp
						<tr>
							<td>{{ $key+1}}</td>
							<td>{{ $data->statistical_date }}</td>
							<td>{{ $data->backlog_status }}</td>
							<td>{{ $data->style }}</td>
							<td>{{ $data->article_no }}</td>
							<td>{{ $data->po_buyer }}</td>
							<td>{{ $data->category }}</td>
							<td>{{ $data->item_code }}</td>
							<td>{{ $data->qty_need }}</td>
							<td>{{ $data->uom }}</td>
							<td>{{ $last_status_movement }}</td>
							<td>{{ $data->warehouse }}</td>
							<td>{{ $data->last_locator_code }}</td>
							<td>{{ $data->last_movement_date }}</td>
							<td>{{ $data->last_user_movement_name }}</td>
							<td>{{ $data->qty_available }}</td>
							<td>{{ $data->qty_supply }}</td>
							<td>{{ $data->document_no }}</td>
							<td>{{ $data->remark }}</td>
							<td>{{ $data->system_log }}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>		
	</div>
</body>
</html>