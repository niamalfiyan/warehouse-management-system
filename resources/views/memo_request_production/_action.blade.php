
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($movement))
                <li><a href="#" onclick="history('{!! $model->po_buyer !!}','{!! $model->item_code !!}','{!! $model->article_no !!}','{!! $model->style !!}')" ><i class="icon-history"></i> History</a></li>
            @endif
        </ul>
    </li>
</ul>
