<div class="modal fade" id="closingMrpModal" tabindex="-1" role="dialog" aria-labelledby="closingMrpModal">
        <div class="modal-dialog" role="document">
		{{ Form::open(['method' => 'POST', 'id' => 'closingMrpForm', 'class' => 'form-horizontal', 'url' => route('report.mrpClosing')]) }}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">CLOSING MRP FOR <span id="update_closing_mrp"></span></h5>
				</div>
				<div class="modal-body">
					<div class="row">  
						<div class="col-md-12">
						    
                            @include('form.text', [
								'label_col' => 'col-xs-3',
								'form_col' => 'col-xs-9',
								'field' => 'closing_uom',
								'label' => 'UOM',
								'placeholder' => 'UOM',
								'attributes' => [
									'id' => 'closing_uom',
									'readonly' => true
								]
                            ])

                            @include('form.text', [
								'label_col' => 'col-xs-3',
								'form_col' => 'col-xs-9',
								'field' => 'closing_need_qty',
								'label' => 'QTY NEED',
								'placeholder' => 'QTY NEED',
								'attributes' => [
                                    'id' => 'closing_need_qty',
									'readonly' => true
								]
                            ])

							@include('form.text', [
								'label_col' => 'col-xs-3',
								'form_col' => 'col-xs-9',
								'field' => 'closing_outstanding_qty',
								'label' => 'QTY OUTSTANDING',
								'placeholder' => 'QTY OUTSTANDING',
								'attributes' => [
                                    'id' => 'closing_outstanding_qty',
									'readonly' => true
								]
                            ])

							@include('form.text', [
								'label_col' => 'col-xs-3',
								'form_col' => 'col-xs-9',
								'field' => 'closing_qty',
								'label' => 'QTY CLOSING',
								'placeholder' => 'QTY CLOSING',
								'attributes' => [
									'id' => 'closing_qty'
								],
								'help' => 'QTY CLOSING TIDAK BISA LEBIH DARI QTY OUTSTANDING'
							])

							@include('form.select', [
								'field' => 'warehouse_closing',
								'label' => 'WAREHOUSE PLACE',
								'options' => [
									'' => 'PLEASE SELECT',
									'1000002' => 'ACC AOI 1',
									'1000013' => 'ACC AOI 2',
									'1000001' => 'FABRIC AOI 1',
									'1000011' => 'FABRIC AOI 2'
								],
								'label_col' => 'col-xs-3',
								'form_col' => 'col-xs-9',
								'attributes' => [
									'id' => 'warehouse_closing'
								]
							])

                            {!! Form::hidden('po_buyer','', array('id' => 'closing_po_buyer')) !!}
                            {!! Form::hidden('item_code','', array('id' => 'closing_item_code')) !!}
                            {!! Form::hidden('style','', array('id' => 'closing_style')) !!}
                            {!! Form::hidden('article_no','', array('id' => 'closing_article')) !!}
                            {!! Form::hidden('backlog_status','', array('id' => 'closing_backlog_status')) !!}
						</div>
						<div class="col-md-12">
							@include('form.textarea', [
								'label_col' => 'col-xs-12',
								'form_col' => 'col-xs-12',
								'field' => 'closing_reason',
								'label' => 'ALASAN',
								'placeholder' => 'TULIS ALASAN ANDA',
								'attributes' => [
									'id' => 'closing_reason'
								]
							])
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-success"> SAVE <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>