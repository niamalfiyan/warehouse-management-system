<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModal">
        <div class="modal-dialog" role="document">
		{{ Form::open([		
				'method' => 'PUT', 
				'id' => 'updateformnya', 
				'class' => 'form-horizontal', 
				'url' => '#'
			]) 
		}}
		    <div class="modal-content">
                <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h5 class="modal-title">Critera Form</h5>
				</div>
				<div class="modal-body">
				   <div class="row">  
					   <div class="col-md-12">
						    @include('form.text', [
								'field' => 'update_name',
								'label' => 'NAME',
								'placeholder' => 'Criteria Name',
								'mandatory' => '* Required',
								'attributes' => [
									'id' => 'update_name'
								]
							])

							@include('form.checkbox', [
								'field' => 'update_is_know_data_source',
								'label' => 'DATA SOURCE',
								'checked' => false,
								'default' => 1,
								'help' => 'Jika anda tau sumber data silahkan di ceklis',
								'attributes' => [
									'id' => 'update_is_know_data_source'
								]
							])

							@include('form.select', [
								'field' => 'update_warehouse',
								'label' => 'WAREHOUSE',
								'options' => [
									'aoi1' => 'AOI 1',
									'aoi2' => 'AOI 2',
									'bbi' => 'BBI'
								],
								'attributes' => [
									'id' => 'update_warehouse'
								]
							])
							{!! Form::hidden('id','', array('id' => 'update_id')) !!}
						</div>
					</div>
				</div>
                <div class="modal-footer text-center">
					<button type="submit" class="btn btn-success">SAVE <i class="icon-floppy-disk position-left"></i></button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE <span class="glyphicon glyphicon-remove"></span></button>
                </div>
                
			</div>
		{{ Form::close() }}
        </div>
	</div>