<script type="x-tmpl-mustache" id="fabric_material_receive_piping_table">
	{% #item %}
		<tr>
			<td>{% no %}</td>
			<td>{% document_no %}</td>
			<td>{% supplier_name %}</td>
			<td>{% item_code %}</td>
			<td>{% color %}</td>
			<td>{% nomor_roll %}</td>
			<td>{% batch_number %}</td>
			<td>{% reserved_qty %} ({% uom %})</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
			
		</tr>	
		
	{%/item%}
	<tr>
		<td colspan="9">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>