@extends('layouts.app', ['active' => 'report_material_movement'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Movement</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Material Movement</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		{!!
			Form::open(array(
				'class' 	=> 'form-horizontal',
				'role' 		=> 'form',
				'url' 		=> route('reportMaterialMovement.export'),
				'method' 	=> 'get',
			))
		!!}	
			

			@include('form.picklist', [
				'field' 			=> 'po_buyer',
				'name' 				=> 'po_buyer',
				'placeholder' 		=> 'Select po buyer',
				'div_class' 		=> 'col-xs-12',
				'form_col' 			=> 'col-xs-12',
				'readonly'			=> true
			]) 

			<button type="submit" class="btn btn-default col-xs-12" style="margin-top: 15px">Export <i class="icon-file-excel position-right"></i></button>
		
			
		{!! Form::close() !!}

	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_material_movement_table">
				<thead>
					<tr>
						<th>Barcode</th>
						<th>Warehouse Location</th>
						<th>Backlog Status</th>
						<th>Po Buyer</th>
						<th>Po Supplier</th>
						<th>Item Code</th>
						<th>Category</th>
						<th>Style</th>
						<th>Article No</th>
						<th>Uom</th>
						<th>Qty Need</th>
						<th>Qty Movement</th>
						<th>Status</th>
						<th>Additional</th>
						<th>From</th>
						<th>Destination</th>
						<th>Movement Date</th>
						<th>PIC Movement</th>
						<th>Note</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 			=> 'po_buyer',
		'title' 		=> 'Filter PO Buyer',
		'placeholder' 	=> 'Search based on po buyer'
	])
@endsection


@section('page-js')
	<script type="text/javascript" src="{{ asset(elixir('js/report_material_movement.js'))}}"></script>
@endsection
