@extends('layouts.app', ['active' => 'accessories_material_reverse_quality_control'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Reverse Quality Control</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Material Reverse</li>
            <li class="active">Material Quality Control</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	@role(['admin-ict-acc'])
			<div class="panel panel-default border-grey">
				<div class="panel-heading">
					<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
					<div class="heading-elements">
						<ul class="icons-list">
							<li><a data-action="collapse"></a></li>
						</ul>
					</div>
				</div>
				<div class="panel-body">
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 			=> 'col-md-10 col-lg-10 col-sm-12',
						'options' 			=> [
							'' 				=> '-- Select Warehouse --',
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				</div>
			</div>
	@else 
		{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
	@endrole
		
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp;<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-form">
					<div class="heading-btn">
						<button type="button" class="btn btn-default" id="is_unintegrated" style="background-color: rgb(252, 252, 252); color: black;">UN INTEGRATED</button>
					</div>
				</div>
			</div>
		</div>
		<div class="panel-body">
			<table class="table table-striped table-hover table-responsive">
				<thead>
					<tr>
						<th>#</th>
						<th>Po Buyer</th>
						<th>Item</th>
						<th>Style</th>
						<th>Article</th>
						<th>Uom</th>
						<th>Qty Reject</th>
						<th>Log</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="tbody_accessories_material_reverse_quality_control">
				</tbody>
			</table>

			{!!
				Form::open([
					'role' => 'form',
					'url' => route('accessoriesMaterialReverseQualityControl.store'),
					'method' => 'post',
					'enctype' => 'multipart/form-data',
					'class' => 'form-horizontal',
					'id'=> 'form'
				])
			!!}
				{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('_is_unintegrated',0, array('id' => '_is_unintegrated')) !!}
				{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('url_accessories_material_reverse_quality_control',route('accessoriesMaterialReverseQualityControl.create'), array('id' => 'url_accessories_material_reverse_quality_control')) !!}
				<div class="form-group text-right">
					<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>

				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection


@section('page-js')
	@include('accessories_material_reverse_quality_control._item')
	<script src="{{ mix('js/accessories_material_reverse_quality_control.js') }}"></script>
@endsection
