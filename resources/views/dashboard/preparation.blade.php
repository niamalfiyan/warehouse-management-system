@extends('dashboard.index')

@section('page-css')
<style type="text/css">
    .title-fac{
        color: black;
        font-family:Cooper Black;
        font-size:  40px;
    }

    .highcharts-point {
        stroke: white;
    }

    .main-color .highcharts-graph {
        stroke: red;
    }
    .main-color, .main-color .highcharts-point {
        fill: red;
    }
    .highcharts-graph.highcharts-negative {
        stroke: blue;
    }
    .highcharts-area.highcharts-negative {
        fill: blue;
    }
    .highcharts-point.highcharts-negative {
        fill: blue;
    }
</style>
@endsection

@section('page-header')
<div class="page-header" style="height: 60px;">
    <div class="page-header-content">
        <div class="page-title" style="padding-top: 1px; padding-bottom: 5px;">
            <center><h1 style="font-weight: bold">DASHBOARD PREPARATION FABRIC</h1></center>
            <center><h2>Planning Date : {{ $planning_date}}</h2></center>
        	<input type="text" name="warehouse_id" id="warehouse_id" class="hidden" value="">
        </div>
    </div>
</div>
@endsection
@section('page-content')
<div class="panel panel-flat">
    <div class="panel-body" style="margin-top: 25px;" >
        <div class="row">
            <div id="chart" style="width:100%; height:550px;"></div>
        </div>
        <br />
    </div>
</div>
@endsection

@section('page-js')
<script type="text/javascript">
Highcharts.chart('chart', {
    chart: {
        zoomType: 'xy'
    },

    title: {
        text: ' '
    },

    xAxis: [{
        categories:[
            <?php echo $label; ?>
        ] ,
        crosshair: true
    }],

    yAxis: [{ // Primary yAxis
        labels: {
            format: '{value}',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        },
        title: {
            text: ' Yards',
            style: {
                color: Highcharts.getOptions().colors[1]
            }
        }
    }],
    plotOptions: {
        spline:{
                dataLabels:{
                    enabled: false,
                    color: '#32a832',
                },
                label: {
                    enabled: false,
                }
            },
        series: {
            label: {
                enabled: false,
            },
            borderWidth: 0,
            dataLabels: {
                
                    allowOverlap:true,
                    enabled: true,
                    // color: '#000',
                    color: '#000000',
                    // style: {fontWeight: 'bolder'},
                    formatter: function() {return this.y},
                    inside: true,
                    style: {
                        textOutline: false,
                        fontSize: '14px'
                        // borderWidth: 0
                    }
                    // rotation: 270
                },
        }
    },

    tooltip: {
        shared: true,
        dataLabels: {
            enabled: true
        },
    },
    
    legend: {
        enabled: true,
        itemStyle: {
            color: '#000000'
        }   
    },
    credits: {
        enabled: false
    },
    plotOptions: {
        // series: {

            spline:{
                dataLabels:{
                    enabled: true,
                    stacking: 'normal',
                    borderWidth: 0,
                    dataLabels: {
                    
                        allowOverlap:true,
                        enabled: true,
                        formatter: function() {return this.y},
                        // inside: true,
                        formatter: function(){
                            return (this.y!=0)?this.y:"";
                        }
                    }
                },
                label: {
                    enabled: false,
                }
            },
            column:{
                
                stacking: 'normal',
                borderWidth: 0,
                dataLabels: {
                
                    allowOverlap:true,
                    enabled: true,
                    formatter: function() {return this.y},
                    inside: true,
                    formatter: function(){
                        return (this.y!=0)?this.y:"";
                    }
                }
            }
    },

    series: [
        {
        name: 'Done',
        color: '#40eb34',
        type: 'column',
        stack: 'prepare',
        data: [

        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    },
    {
        name: 'Waiting Allocation MM',
        color: '#000000',
        type: 'column',
        stack: 'prepare',
        data: [
           <?php echo $waiting_mm; ?>
        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    },
    {
        name: 'Waiting Confirmation QC',
        color: '#f54242',
        type: 'column',
        stack: 'prepare',
        data: [
           <?php echo $outstanding_confirmation_qc; ?>
        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    },
        {
        name: 'Cancel Plan',
        color: '#FFAA00',
        type: 'column',
        stack: 'prepare',
        data: [
            <?php echo $qty_cancel; ?>
        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    },
    {
        name: 'Prepare',
        type: 'column',
        stack: 'prepare',
        data: [
            <?php echo $qty_prepare; ?>
        ],
        tooltip: {
            valueSuffix: ' Yards'
        },
        dataLabels: {
          enabled: true
        }

    },
     {
        name: 'Target',
        type: 'spline',
        color: '#eb34bd',
        data: [
            <?php echo $target; ?>
        ],
        tooltip: {
            valueSuffix: ' Yards',
        },
        dataLabels: {
            style:{
                textOutline: false,
                fontSize: '17px',
                color: '#eb34bd'
            }
        },
    }]
});

$(document).ready(function(){   
     setInterval(function(){window.location.href="/dashboard/preparation/"},600000);

});

</script>
@endsection