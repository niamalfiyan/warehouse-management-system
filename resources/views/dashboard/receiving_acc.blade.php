@extends('dashboard.template')

@section('css')
    <style>
        body {
            margin-top: 20px;
        }
    </style>
@endsection

@section('body')
<div class="container-fluid" id="element">
    <div class="row justify-content-md-center">
        <br>
        <h1 align='center' style="color: white; text-shadow: 2px 0px 5px red;"> RECEIVING STATUS ACC <?= $wh ?> <br> <?= $receiving_date2 ?> s/d <?= $receiving_date1 ?> </h1>
        <canvas id='myChart'></canvas>
    </div>
</div>
@endsection

@section('js')
<script>
    var xreceived = [<?= $receive ?>];
    var xscanin = [<?= $in ?>];
    var xqcrelease = [<?= $qc_release ?>];
    // var xscanmetal = [<?= $metal_check ?>];
    var xhold = [<?= $hold ?>];
    var xreject = [<?= $reject ?>];

    var ctx = document.getElementById('myChart').getContext('2d');
    ctx.canvas.width = 1300;
    ctx.canvas.height = 600;

    var myChart = new Chart(ctx, {
        type: 'bar',
        mode: 'index',
        barPercentage: 1.0,
        aspectRatio: 1,
        plugins: [ChartDataLabels],
        data: {
            labels:
            [<?= $date ?>],
            datasets: [
            {
                type: 'line',
                label: 'Received',
                data: xreceived,
                backgroundColor: 'white',
                borderColor: 'white',
                pointBorderWidth: 1,
                pointStyle: 'circle',
                fill: false,
                showLine: false,
                tension: 0.1,
                datalabels: {
                    display: true,
                    anchor: 'end',
                    align: 'end',
                    offset: 5,
                    color: 'white',
                    backgroundColor: 'black',
                    font: {
                        size: 17,
                        weight: 'bold'
                    },
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] !== 0; // or >= 1 or ...
                    }
                }
            }, {
                label: 'Scan Inventory',
                barPercentage: 1.0,
                data: xscanin,
                backgroundColor: [
                    '#06FF00'
                ],
                borderColor: 'black',
                borderWidth: 1
            }, {
                label: 'QC Release',
                barPercentage: 1.0,
                data: xqcrelease,
                backgroundColor: [
                    '#04009A',
                ],
                borderColor: 'black',
                borderWidth: 1
            }, 
            // {
            //     label: 'Scan Metal',
            //     barPercentage: 1.0,
            //     data: xscanmetal,
            //     backgroundColor: [
            //         '#FFCA03'
            //     ],
            //     borderColor: 'black',
            //     borderWidth: 1
            // },
             {
                label: 'Hold',
                barPercentage: 1.0,
                data: xhold,
                backgroundColor: [
                    '#F21170'
                ],
                borderColor: 'black',
                borderWidth: 1
            }, {
                label: 'Reject',
                barPercentage: 1.0,
                data: xreject,
                backgroundColor: [
                    '#FF1700'
                ],
                borderColor: 'black',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            interaction: {
                intersect: false,
                mode: 'index'
            },
            plugins: {
                legend: {
                    position: 'bottom',
                    display: true,
                    labels: {
                        usePointStyle: true,
                        pointStyle: 'rectRounded',
                        color: 'white'
                    }
                },
                datalabels: {
                    display: function(context) {
                        return context.dataset.data[context.dataIndex] >= 100; // or >= 1 or ...
                    },
                    formatter: function(value, context) {
                        return context.dataset.data[context.dataIndex].toLocaleString();
                    },
                    color: 'white',
                    backgroundColor: 'black',
                    anchor: 'center',
                    align: 'center',
                    font: {
                        size: 17,
                        weight: 'bold'
                    }
                }
            },
            scales: {
                x: {
                    stacked: true,
                    ticks: {
                        color: 'white'
                      }            
                },
                y: {
                    stacked: true,
                    grid: {
                        color: 'white'
                    },
                    ticks: {
                        color: 'white'
                      }  
                }
            }
        }
    });

    $(document).ready(function(){
        setTimeout(function() {
            location.reload();
          }, 60000);
     });
</script>
@endsection
