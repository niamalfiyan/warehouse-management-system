@extends('layouts.app', ['active' => 'master_data_defect'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Defect</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataDefect.index') }}" id="url_master_data_defect_index">Defect</a></li>
				<li class="active">Create</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{{ Form::open([
				'method' => 'POST',
				'id' => 'form',
				'class' => 'form-horizontal',
				'url' => route('masterDataDefect.store')]) 
			}}
				
			@include('form.text', [
				'field' 		=> 'code',
				'label' 		=> 'Code',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type code here',
				'attributes' 	=> [
					'id' 		=> 'code'
				]
			])

			@include('form.textarea', [
				'field' 		=> 'description_in_english',
				'label' 		=> 'Description In English',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type description here',
				'attributes' 	=> [
					'style' 	=> 'resize:none'
				]
			])

			@include('form.textarea', [
				'field' 		=> 'description_in_bahasa',
				'label' 		=> 'Description In Bahasa',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'placeholder' 	=> 'Please type description here',
				'attributes' 	=> [
					'style' 	=> 'resize:none'
				]
			])
            
			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
		{{ Form::close() }}
		</div>
	</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_defect.js') }}"></script>
@endsection
