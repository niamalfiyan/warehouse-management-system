@extends('layouts.app', ['active' => 'accessories_barcode_material_preparation'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Preparation</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Preparation</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{!!
				Form::open([
					'role' 		=> 'form',
					'url' 		=> route('accessoriesBarcodeMaterialPreparation.store'),
					'method' 	=> 'post',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form_store_barcode'
				])
			!!}

				@role(['admin-ict-acc'])
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-sm-12',
						'form_col' 			=> 'col-sm-12',
						'options' 			=> [
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				@else 
					{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
				@endrole

				@include('form.picklist', [
					'label' 		=> 'Criteria',
					'field' 		=> 'document_material_preparation',
					'name' 			=> 'document_material_preparation',
					'placeholder' 	=> 'Please Select Criteria',
					'title' 		=> 'Please Select Criteria',
					'readonly'		=> true,
					'label_col' 	=> 'col-sm-12',
					'form_col' 		=> 'col-sm-12',
				])


			<br/>
			
				{!! Form::hidden('c_bpartner_id',null , array('id' => 'c_bpartner_id')) !!}
				{!! Form::hidden('document_no',null , array('id' => 'document_no')) !!}
				{!! Form::hidden('item_code',null , array('id' => 'item_code')) !!}
				{!! Form::hidden('status',null , array('id' => 'status')) !!}
			{!! Form::close() !!}
		</div>
	</div>
	
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					@role(['admin-ict-acc','supervisor'])
						<button type="button" id="btn_check_all" class="btn btn-yellow-cancel">Check All <i class="icon-checkbox-checked position-left"></i></button>
					@endrole
					<button type="button" id="btn_uncheck_all" class="btn btn-default">Uncheck All <i class=" icon-checkbox-unchecked position-left"></i></button>
				</div>
			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th>#</th>
							<th>Po Supplier</th>
							<th>Po Buyer</th>
							<th>Item</th>
							<th>Style</th>
							<th>Article No</th>
							<th>Uom</th>
							<th>Need</th>
							<th>Rcv</th>
							<th>System Note</th>
						</tr>
					</thead>
					<tbody id="tbody-preparation">
					</tbody>
				</table>
			</div>
			

			{!!
				Form::open([
					'role' => 'form',
					'url' => route('accessoriesBarcodeMaterialPreparation.updateBarcodeStatus'),
					'method' => 'post',
					'class' => 'form-horizontal',
					'id'=> 'form'
				])
			!!}
			<a href="{{ route('accessoriesBarcodeMaterialPreparation.store') }}" id="url_store_material_preparation" class="hidden"></a>
			{!! Form::hidden('flag_msg',(Session::has('flag')) ? Session::get('flag'):null , array('id' => 'flag_msg')) !!}
			{!! Form::hidden('material_preparations','[]' , array('id' => 'material_preparations')) !!}
			{!! Form::hidden('warehouse_id',auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
			<div class="form-group text-right" style="margin-top: 10px;">
				<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Print <i class="icon-printer position-left"></i></button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>

{!!
	Form::open([
		'role' 		=> 'form',
		'url' 		=> route('accessoriesBarcodeMaterialPreparation.print'),
		'method' 	=> 'post',
		'class' 	=> 'form-horizontal',
		'id'		=> 'getBarcode',
		'target' 	=> '_blank'
	])
!!}
	{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
	<div class="form-group text-right" style="margin-top: 10px;">
		<button type="submit" class="btn hidden">Print <i class="icon-printer position-left"></i></button>
	</div>
{!! Form::close() !!}
@endsection

@section('page-modal')
	@include('accessories_barcode_material_preparation._picklist', [
		'name' => 'document_material_preparation',
		'name2' => 'item_material_preparation',
		'title' => 'Searching Criteria',
	])	
@endsection

@section('page-js')
	@include('accessories_barcode_material_preparation._item')
	<script src="{{ mix('js/accessories_barcode_material_preparation.js') }}"></script>
@endsection
