
<div id="changeTypeStockModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{{ Form::open(['method' => 'POST', 
			'id' => 'changeTypeStockForm', 
			'class' => 'form-horizontal', 
			'url' => route('accessoriesReportMaterialStock.storeChangeTypeStock')]) 
		}}
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Change type stock for <span id="header_change_type_stock"></span></h5>

			</div>

			<div class="modal-body">
				@include('form.select', [
					'field' => 'mapping_type_stock_option',
					'label' => 'Stock type',
					'label_col' => 'col-lg-3 col-md-3 col-sm-12',
					'form_col' => 'col-lg-9 col-md-9 col-sm-12',
					'options' => [
						'SLT' => 'SLT',
						'REGULER' => 'REGULER',
						'PR/SR' => 'PR/SR',
						'MTFC' => 'MTFC',
						'NB' => 'NB',
						'ALLOWANCE' => 'ALLOWANCE',
					],
					'class' => 'select-search',
					'attributes' => [
						'id' => 'mapping_type_stock_option'
					]
				])

				@include('form.text', [
					'label_col' 		=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 			=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 			=> 'uom',
					'label' 			=> 'Uom',
					'attributes' 		=> [
						'id' 			=> 'uom_change_type_stock',
						'readonly' 		=> 'readonly'
					]
				])

				@include('form.text', [
					'label_col' 		=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 			=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 			=> 'qty_available_change_stock',
					'label' 			=> 'Qty available',
					'attributes' 		=> [
						'id' 			=> 'qty_available_change_stock',
						'readonly' 		=> 'readonly'
					]
				])

				@include('form.text', [
					'label_col' 		=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 			=> 'col-lg-9 col-md-9 col-sm-12',
					'field' 			=> 'qty_change_stock',
					'label' 			=> 'Qty change',
					'placeholder' 		=> 'Please input qty change type stock here',
					'attributes' 		=> [
						'id' 			=> 'qty_change_stock'
					],
					'help' 				=> 'Qty change type stock must be less than qty available'
				])
				
				{!! Form::hidden('curr_type_stock','', array('id' => 'curr_type_stock')) !!}
				{!! Form::hidden('id','', array('id' => 'change_type_stock_id')) !!}
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
				<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
