
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            <li><a href="#" onclick="mutation('{!! $stockMutation !!}')" ><i class="icon-history"></i> Mutation</a></li>
            
            @if (isset($restore))
                <li><a href="#" onclick="restore('{!! $restore !!}')"><i class="icon-undo"></i> Restore </a></li>
            @endif

            @if (isset($edit_modal) && $model->is_running_stock == false)
                <li><a href="#" onclick="edit('{!! $edit_modal !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            
            @if (isset($delete) && $model->is_running_stock == false)
                <li><a href="#" onclick="hapus('{!! $delete !!}')"><i class="fa icon-trash"></i> Delete</a></li>
            @endif
            
            @if (isset($recalculate))
                <li><a href="#" onclick="recalculate('{!! $recalculate !!}')"><i class="icon-calculator3"></i> Recalculate</a></li>
            @endif

            @if (isset($unlock))
                <li><a href="#" onclick="unlocked('{!! $unlock !!}')"><i class="icon-unlocked"></i> Unlock</a></li>
            @endif

            @if (isset($allocatingStock))
                <li><a href="#" onclick="allocating('{!! $allocatingStock !!}')"><i class=" icon-pie-chart"></i> Allocating</a></li>
            @endif

            @if (isset($cancelAutoAllocation))
                <li><a href="#" onclick="cancel('{!! $cancelAutoAllocation !!}')"><i class=" icon-x"></i> Cancel Auto Allocation</a></li>
            @endif

            @if (isset($move) && $model->is_running_stock == false)
                <li><a href="#" onclick="move('{!! $move !!}')"><i class="icon-transmission"></i> Move Locator </a></li>
            @endif

            @if (isset($transfer) && $model->is_running_stock == false)
                <li><a href="#" onclick="transfer('{!! $transfer !!}')"><i class="icon-exit"></i> Transfer Stock </a></li>
            @endif

            @if (isset($change_type) && $model->is_running_stock == false)
                <li><a href="#" onclick="changeType('{!! $change_type !!}')"><i class="icon-copy3"></i> Change Type</a></li>
            @endif

            @if (isset($change_source) && $model->is_running_stock == true)
                <li><a href="#" onclick="changeSource('{!! $change_source !!}')"><i class="icon-copy3"></i> Change Source</a></li>
            @endif
         </ul>
    </li>
</ul>
