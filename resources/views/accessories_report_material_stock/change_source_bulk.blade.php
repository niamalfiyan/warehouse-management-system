@extends('layouts.app', ['active' => 'accessories_report_material_stock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Stock</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li><a href="{{ route('accessoriesReportMaterialStock.index') }}">Material Stock</a></li>
				<li class="active">Change Source Bulk</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<a href="{{ route('accessoriesReportMaterialStock.index') }}" id="url_material_stock_index" class="hidden"></a>

	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title">&nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<a class="btn btn-primary btn-icon" href="{{ route('accessoriesReportMaterialStock.exportFormImportChangeSourceBulk')}}" data-popup="tooltip" title="download form delete" data-placement="bottom" data-original-title="download form delete"><i class="icon-download"></i></a>
				<button type="button" class="btn btn-success btn-icon" id="upload_button" data-popup="tooltip" title="upload form change source" data-placement="bottom" data-original-title="upload form change source"><i class="icon-upload"></i></button>

				{!!
					Form::open([
						'role' => 'form',
						'url' => route('accessoriesReportMaterialStock.storeChangeSourceBulk'),
						'method' => 'POST',
						'id' => 'upload_file_allocation',
						'enctype' => 'multipart/form-data'
					])
				!!}
					<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
				{!! Form::close() !!}

			</div>
		</div>

		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Item Code</th>
							<th>Qty</th>
							<th>Reason</th>
							<th>Warehouse Inventory</th>
							<th>Upload Result</th>
						</tr>
					</thead>
					<tbody id="tbody-change-source-bulk">
					</tbody>
				</table>
			</div>
		</div>
	</div>
	{!! Form::hidden('change_source_bulk_items','[]' , array('id' => 'change_source_bulk_items')) !!}
		
@endsection

@section('page-js')
	@include('accessories_report_material_stock._material_stock_change_source_item')
	<script type="text/javascript" src="{{ asset(elixir('js/accessories_report_material_stock_change_source_bulk.js'))}}"></script>
@endsection
