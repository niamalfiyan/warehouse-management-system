
<div id="deleteStockModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		{{ 
			Form::open(['method' => 'POST'
			,'id' => 'deleteStockform'
			,'class' => 'form-horizontal'
			,'url' => route('accessoriesReportMaterialStock.storeDeleteStock')]) 
		}}
			<div class="modal-header bg-blue">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Delete stock for <span id="delete_header_stock"></span></h5>

			</div>

			<div class="modal-body">
			<div class="row">  
				<div class="col-md-12">
					@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'delete_uom',
						'label' 		=> 'Uom',
						'placeholder' 	=> 'Uom',
						'attributes' 	=> [
							'id' 		=> 'delete_uom',
							'readonly' 	=> 'readonly'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'delete_qty_available',
						'label' 		=> 'Qty available',
						'attributes' 	=> [
							'id' 		=> 'delete_qty_available',
							'readonly' 	=> 'readonly'
						]
					])

					@include('form.text', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'delete_qty',
						'label' 		=> 'Qty delete',
						'placeholder' 	=> 'Please input qty delete here',
						'attributes' 	=> [
							'id' 		=> 'delete_qty'
						],
						'help' => 'Qty delete must be less than qty available'
					])

					@include('form.textarea', [
						'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
						'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
						'field' 		=> 'delete_reason',
						'label'			=> 'Reason',
						'placeholder' 	=> 'Please type reason here',
						'attributes' 	=> [
							'id' 		=> 'delete_reason',
							'style' 	=> 'resize:none'
						]
					])
					
					{!! Form::hidden('id','', array('id' => 'delete_id')) !!}
				</div>
			</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close  <span class="glyphicon glyphicon-remove"></span></button>
				<button type="submit" class="btn btn-blue-success">Save <i class="icon-floppy-disk position-left"></i></button>
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
