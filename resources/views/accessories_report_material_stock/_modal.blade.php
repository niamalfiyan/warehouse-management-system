<div id="detailModal" class="modal fade" data-backdrop="static" data-keyboard="false"  style="color:black !important">
	<div class="modal-dialog modal-lg" style="width:75%">
		<div class="modal-content">
			<div class="modal-header bg-blue">
				<h5 class="modal-title">Mutation Stock <span id="histori_stock"></span></h5>
			</div>

			<div class="modal-body">
				<div class="row">
					<div class="table-responsive">
						<table class="table datatable-basic table-striped table-hover table-responsive" id="mutation_stock_table">
							<thead>
								<tr>
									<th>No</th>
									<th>Created At</th>
									<th>PIC</th>
									<th>Description</th>
									<th>Qty (DB)</th>
									<th>Qty (CR)</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close <span class="glyphicon glyphicon-remove"></span></button>
			</div>
		</div>
	</div>
</div>
