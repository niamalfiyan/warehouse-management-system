<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>No</th>
			<th>Created By</th>
			<th>Created At</th>
			<th>Po Buyer</th>
			<th>Article No</th>
			<th>Style</th>
			<th>Uom</th>
			<th>Qty Booking</th>
			<th>Remark</th>
	</tr>
	</thead>
	
	<tbody>
		@php $total = 0; @endphp
		@foreach ($allocation_per_pobuyer as $key => $item)
			<tr>
				<td>
					{{ $key+1 }}
				</td>
				<td>
					{{ strtoupper($item->user->name) }}
				</td>
				<td>
					{{ $item->created_at->format('d/M/Y H:i:s') }}
				</td>
				<td>
					{{ $item->po_buyer }}@if($item->is_additional)<span class="text-danger">*</span>@endif
				</td>
				<td>
					{{ $item->article_no }}
				</td>
				<td>
					{{ $item->style }}
				</td>
				<td>
					{{ $item->uom }}
				</td>
				<td>
					{{ number_format($item->qty_booking, 4, '.', ',') }}
				</td>
				<td>
					{{ $item->remark }}
				</td>
			</tr>
			@php $total += $item->qty_booking; @endphp
		@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6"></td>
			<td><b>TOTAL</b></td>
			<td>{{ number_format($total, 4, '.', ',') }}</td>
			<td></td>
		</tr>
	</tfoot>
</table>

