<script type="x-tmpl-mustache" id="accessories_material_quality_control_table">
	{% #item %}
		<tr>
			<td>
				{% no %}.
				<input type="checkbox" class="checkbox_item" id="check_{% _idx %}" data-id="{% _idx %}" {% #check_all %} checked="checked" {% /check_all %}>
			</td>
			<td>
				<p id="barcode_{% _idx %}">{% barcode %}</p>
			</td>
			<td>
				<p id="document_no_{% _idx %}">{% document_no %}</p>
			</td>
			<td>
				<p id="document_no_{% _idx %}">{% po_buyer %}</p>
			</td>
			<td>
				<p id="item_{% _idx %}">{% item_code %}</p>
			</td>
			<td>
				<p id="uom_{% _idx %}">{% uom_conversion %}</p>
			</td>
			<td>
				<p id="need_{% _idx %}">{% qty %} </p>
			</td>
			<td>
				<input type="text" class="form-control input-sm input-item input-edit input-number" id="qtyInput_{% _idx %}" value="{% qty_check %}" data-id="{% _idx %}" placeholder="Input Qty Reject"  data-row="" >
			</td>
			<td>
				<textarea class="form-control input-sm input-remark input-edit" rows="5"  id="remarkInput_{% _idx %}" data-id="{% _idx %}" placeholder="Please insert note here">{% remark %}</textarea>
			</td>
			<td>
				<span id="status_{% _idx %}" class="hidden"></span>
				<select class="form-control status_item" id="status_option_{% _idx %}" data-id="{% _idx %}">
					{% #status_options %}
						<option value="{% id %}" {% selected %}>{% name %}</option>
					{% /status_options %}
				</select>
			</td>
			<td>
				<button type="button" id="delete_{% _idx %}" data-id="{% _idx %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
				<!--<input type="hidden" id="_statusInput_{% _idx %}" value="{% status %}" data-id="{% _idx %}" data-row="">
				<input type="hidden" id="_temp_{% _idx %}" value="{% _qty %}" data-id="{% _idx %}" data-row="">
				<input type="hidden" id="qtyRcvInput_{% _idx %}" value="{% qty %}" data-id="{% _idx %}" data-row="">
				<button type="button" id="edit_{% _idx %}" data-id="{% _idx %}" class="btn btn-default btn-icon-anim btn-circle btn-edit-item"><i class="icon-pencil"></i></button>
				<button type="button" id="simpan_{% _idx %}" data-id="{% _idx %}" class="btn btn-success btn-icon-anim btn-circle btn-save-item hidden"><i class="icon-floppy-disk"></i></button>
				<button type="button" id="cancel_{% _idx %}" data-id="{% _idx %}" class="btn btn-warning btn-icon-anim btn-circle btn-cancel-item hidden"><i class="icon-x""></i></button>-->
			</td>
		</tr>
	{%/item%}	
	<tr>
		<td colspan="11">
			<input type="text" id="barcode_value" name="barcode_value" class="barcode_value form-control input-new" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>