<script type="x-tmpl-mustache" id="material-cancel-out-handover-fabric-table">
    {% #list %}
        <tr}>
            <td>{% no %} </td>
            <td>{% supplier_name %}</td>
            <td>{% document_no %}</td>
            <td>{% item_code %}</td>
            <td>{% nomor_roll %}</td>
            <td>{% batch_number %}</td>
            <td>{% actual_lot %}</td>
            <td>{% qty_handover %} </td>
            <td>
                <button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
            </td>

        </tr>
    {%/list%}
    <tr>
        <td colspan="11">
            <input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" placeholder="Please scan your barcode here" autocomplete="off"></input>
        </td>
    </tr>
</script>
    