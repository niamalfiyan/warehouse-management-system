@extends('layouts.app', ['active' => 'accessories_material_stock_on_the_fly'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock On The Fly ( SOTF )</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Material Stock On The Fly ( SOTF )</li>
        </ul>
		<ul class="breadcrumb-elements">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
					<i class="icon-three-bars position-left"></i>
					Actions
					<span class="caret"></span>
				</a>
				
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="{{ route('accessoriesMaterialStockOnTheFly.import') }}"><i class="icon-pencil6 pull-right"></i> Create</a></li>
					<li><a href="{{ route('accessoriesMaterialStockOnTheFly.export') }}"><i class="icon-file-excel pull-right"></i> Export</a></li>
					<li><a href="{{ route('accessoriesMaterialStockOnTheFly.deleteFromBulk') }}"><i class="icon-trash pull-right"></i> Delete</a></li>
				</ul>
			</li>
		</ul>
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table datatable-basic table-striped table-hover table-responsive" id="accessories_material_stock_on_the_fly_table">
					<thead>
						<tr>
							<th>ID</th>
							<th>Created At</th>
							<th>Created By</th>
							<th>Warehouse Arrival</th>
							<th>Supplier Code</th>
							<th>Supplier Name</th>
							<th>Po Supplier</th>
							<th>Type Stock</th>
							<th>Po Buyer</th>
							<th>Item Code</th>
							<th>Item Desc</th>
							<th>Category</th>
							<th>Uom</th>
							<th>Stock Begining</th>
							<th>Stock Arrival</th>
							<th>Stock Reserved</th>
							<th>Stock Available</th>
							<th>Status Arrival</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/accessories_material_stock_on_the_fly.js') }}"></script>
@endsection
