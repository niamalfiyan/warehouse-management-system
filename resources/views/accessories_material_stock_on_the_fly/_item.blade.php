<script type="x-tmpl-mustache" id="material-stock-on-the-fly-table">
	{% #item %}
		<tr {% #is_error %} style="background-color:#fab1b1" {% /is_error %}>
			<td>
				{% no %}
			</td>
			<td>
				{% type_stock %}
			</td>
			<td>
				{% supplier_name %}
			</td>
			<td>
				{% document_no %}
			</td>
			<td>
				{% item_code %}
			</td>
			<td>
				{% uom %}
			</td>
			<td>
				{% stock %}
			</td>
			<td>
				{% warehouse_name %}
			</td>
			<td>
				{% result %}
			</td>
		</tr>
	{%/item%}
</script>
