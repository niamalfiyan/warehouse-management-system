<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($delete))
                <li><a onclick="hapus('{!! $delete !!}')" ><i class="icon-trash"></i> Delete</a></li>
            @endif
        </ul>
    </li>
</ul>