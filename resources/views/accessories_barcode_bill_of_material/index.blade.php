@extends('layouts.app', ['active' => 'accessories_barcode_bill_of_material'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Barcode Bill Of Material</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li>Barcode</li>
            <li class="active">Bill Of Material</li>
        </ul>
		<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('accessoriesBarcodeBillOfMaterial.upload') }}"><i class="icon-file-excel pull-right"></i> Upload</a></li>
					</ul>
				</li>
			</ul>
    </div>
</div>
@endsection

@section('page-content')
	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesBarcodeBillOfMaterial.store'),
			'method' 	=> 'post',
			'class' 	=> 'form-horizontal',
			'id'		=> 'form'
		])
	!!}
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				@role(['admin-ict-acc'])
					@include('form.select', [
						'field' 			=> 'warehouse',
						'label' 			=> 'Warehouse',
						'default'			=> auth::user()->warehouse,
						'label_col' 		=> 'col-sm-12',
						'form_col' 			=> 'col-sm-12',
						'options' 			=> [
							'' 				=> '-- Select Warehouse --',
							'1000002' 		=> 'Warehouse Accessories AOI 1',
							'1000013' 		=> 'Warehouse Accessories AOI 2',
						],
						'class' 			=> 'select-search',
						'attributes' 		=> [
							'id' 			=> 'select_warehouse'
						]
					])
				@else 
					{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
				@endrole
				
				<div class="tabbable tab-content-bordered">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#default" data-toggle="tab" aria-expanded="false">Reguler</a></li>
						<li><a href="#additional" data-toggle="tab" aria-expanded="true">Additional</a></li>
					</ul>
						
					<div class="tab-content">
						<div class="tab-pane active" id="default">
							<div class="panel panel-default border">
								<div class="panel-body">
									<div class="col-md-6">
										@include('form.picklist', [
											'label' 		=> 'Po Buyer',
											'field' 		=> 'po_buyer',
											'name' 			=> 'po_buyer',
											'placeholder' 	=> 'Please Select Criteria',
											'title' 		=> 'Please Select Criteria',
											'readonly'		=> true,
											'label_col' 	=> 'col-sm-12',
											'form_col' 		=> 'col-sm-12',
										])
									</div>
									<div class="col-md-6">
										<p id="msg" class="msg text-center"> Please Select PO Buyer First !</p>
										<p id="po_buyer" class="po_buyer hidden"><b>PO BUYER: </b> <span id="_po_buyer"></span></p>
										<p id="item_code" class="item_code hidden"><b>ITEM CODE: </b> <span id="_item_code"></span></p>
										<p id="uom" class="uom hidden"><b>UOM: </b> <span id="_uom"></span></p>
										<p id="qty" class="qty hidden"><b>QTY NEED: </b> <span >{!! Form::number('__qty_required','', array('id' => '__qty_required','class' => '__qty_required')) !!}</span></p>

										{!! Form::hidden('__item_code','', array('id' => '__item_code','class' => '__item_code')) !!}
										{!! Form::hidden('__item_desc','', array('id' => '__item_desc','class' => '__item_desc')) !!}
										{!! Form::hidden('__category','', array('id' => '__category','class' => '__category')) !!}
										{!! Form::hidden('__po_buyer','', array('id' => '__po_buyer','class' => '__po_buyer')) !!}
										{!! Form::hidden('___article','', array('id' => '___article','class' => '___article')) !!}
										{!! Form::hidden('___job_order','', array('id' => '___job_order','class' => '___job_order')) !!}
										{!! Form::hidden('__style','', array('id' => '__style','class' => '__style')) !!}
										{!! Form::hidden('__uom','', array('id' => '__uom','class' => '__uom')) !!}
									</div>
									<button type="submit" class="btn btn-blue-success col-xs-12">Print <i class="icon-printer position-left"></i></button>

								</div>
							</div>
						</div>
						<div class="tab-pane" id="additional">
							<div class="panel panel-default border">
								<div class="panel-body">
									<div class="col-md-6">
										@include('form.picklist', [
											'label' 		=> 'Po Buyer',
											'field' 		=> 'po_buyerAdditional',
											'name' 			=> 'po_buyerAdditional',
											'placeholder' 	=> 'Please Select Criteria',
											'title' 		=> 'Please Select Criteria',
											'readonly'		=> true,
											'label_col' 	=> 'col-sm-12',
											'form_col' 		=> 'col-sm-12',
										])
									</div>

									<div class="col-md-6" id="item-additional">
										<p id="msg" class="msg text-center"> Please Select PO Buyer First ! </p>
										<p id="po_buyer" class="po_buyer hidden"><b>PO BUYER: </b> <span id="_po_buyer"></span></p>
										<p id="item_code" class="item_code hidden"><b>ITEM CODE: </b> <span id="_item_code"></span></p>
										<p id="uom" class="uom hidden"><b>UOM: </b> <span id="_uom"></span></p>
										<p id="qty" class="qty hidden"><b>QTY NEED : </b> <input type="number" id="additional_qty" name="additional_qty" placeholder="0" class="form-group"></p>

										{!! Form::hidden('additional_item_code','', array('id' => '__item_code','class' => '__item_code')) !!}
										{!! Form::hidden('additional_item_desc','', array('id' => '__item_desc','class' => '__item_desc')) !!}
										{!! Form::hidden('additional_category','', array('id' => '__category','class' => '__category')) !!}
										{!! Form::hidden('additional_po_buyer','', array('id' => '__po_buyer','class' => '__po_buyer')) !!}
										{!! Form::hidden('additional_article','', array('id' => '___article','class' => '___article')) !!}
										{!! Form::hidden('additional_job_order','', array('id' => '___job_order','class' => '___job_order')) !!}
										{!! Form::hidden('additional_style','', array('id' => '__style','class' => '__style')) !!}
										{!! Form::hidden('additional_uom','', array('id' => '__uom','class' => '__uom')) !!}
										{!! Form::hidden('additional_qty_required','', array('id' => '__qty_required','class' => '__qty_required')) !!}
										{!! Form::hidden('is_additional','', array('id' => '__is_additional','class' => '__is_additional')) !!}
									</div>
									<button type="submit" class="btn btn-blue-success col-xs-12">Print <i class="icon-printer position-left"></i></button>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}

	{!!
		Form::open([
			'role' 		=> 'form',
			'url' 		=> route('accessoriesBarcodeBillOfMaterial.print'),
			'method' 	=> 'get',
			'class' 	=> 'form-horizontal',
			'id'		=> 'getBarcode',
			'target' 	=> '_blank'
		])
	!!}
		{!! Form::hidden('list_barcodes','[]' , array('id' => 'list_barcodes')) !!}
	{!! Form::close() !!}

@endsection

@section('page-modal')
	@include('accessories_barcode_bill_of_material._picklist', [
		'name' => 'po_buyer',
		'name2' => 'item_code',
		'title' => 'List Of Data',
	])

	@include('accessories_barcode_bill_of_material._picklist', [
		'name' => 'po_buyerAdditional',
		'name2' => 'item_codeAdditional',
		'title' => 'List Of Data Additional',
	])
@endsection

@section('page-js')
<script src="{{ mix('js/accessories_barcode_bill_of_material.js') }}"></script>
@endsection
