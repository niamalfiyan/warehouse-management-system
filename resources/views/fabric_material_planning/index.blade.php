@extends('layouts.app', ['active' => 'fabric_material_planning'])


@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Planning</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
            <li class="active">Material Planning</li>
        </ul>
    </div>
</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>

		<div class="panel-body">
			{!!
				Form::open(array(
					'role' 			=> 'form',
					'url' 			=> route('fabricMaterialPlanning.store'),
					'method' 		=> 'post',
					'id' 			=> 'getPlanningCutting'
				))
			!!}
			
			@role(['admin-ict-fabric'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 		=> 'col-md-3 col-lg-3 col-sm-12',
					'form_col' 			=> 'col-md-9 col-lg-9 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole

			@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'Planning Cutting Date From (00:00)',
				'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
				'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
				
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'Planning Cutting Date To (23:59)',
				'label_col' 	=> 'col-md-3 col-lg-3 col-sm-12',
				'form_col' 		=> 'col-md-9 col-lg-9 col-sm-12',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])

				<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top:10px;">Filter <i class="icon-filter3 position-left"></i></button>
			{!! Form::close() !!}

			{!!
				Form::open(array(
					'role' 			=> 'form',
					'url' 			=> route('fabricMaterialPlanning.downloadSPK'),
					'method' 		=> 'get',
				))
			!!}
			{!! Form::hidden('warehouse_id',null , array('id' => 'warehouse_id')) !!}
			{!! Form::hidden('_start_date',null , array('id' => '_start_date')) !!}
			{!! Form::hidden('_end_date',null , array('id' => '_end_date')) !!}
			<button type="submit" class="btn btn-default col-xs-12">Download SPK <i class="icon-file-excel position-left"></i></button>
			{!! Form::close() !!}
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover " id="fabric_material_planning_table">
				<thead>
					<tr>
						<th>No</th>
						<th>ID</th>
						<th>Planning Date</th>
						<th>Po Buyer</th>
						<th>Item Code</th>
						<th>Color</th>
						<th>Article No</th>
						<th>Style</th>
						<th>Is Piping</th>
						<th>Qty Consumtion</th>
					</tr>
				</thead>
			</table>
			</div>
		</div>
	</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_material_planning.js') }}"></script>
@endsection
