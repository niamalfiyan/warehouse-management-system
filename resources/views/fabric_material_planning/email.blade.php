<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="utf-8">
</head>
<body>
    <h3>Daftar Outstanding Alokasi di WMS</h3>

    <div>
        <p style="margin: 1em 0;"><b>Mohon perhantian dan kerja samanya</b><br/>
        <br>Dibawah ini adalah list po buyer yang belum teralokasi di wms <br/>
        <b>(Total yang belum ter alokasikan {{ $total_outstanding }} baris. Periode {{ $start_date }} - {{ $end_date }})</b>
        </p>
        
        <p style="margin: 1em 0 1em 15px;">
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PLANNING CUTTING DATE</th>
                        <th>PIPING</th>
                        <th>PO BUYER</th>
                        <th>ITEM CODE</th>
                        <th>COLOR</th>
                        <th>STYLE</th>
                        <th>ARTICLE NO</th>
                        <th>WAREHOUSE</th>
                        <th>UOM</th>
                        <th>QTY CONSUMTION</th>
                        <th>TOTAL ALOKASI</th>
                    
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $datum)
                        @if($key < 100)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $datum->planning_date }}</td>
                                <td>
                                    @if( $datum->is_piping  )
                                        PIPING
                                    @else
                                        BUKAN PIPING
                                    @endif
                                </td>
                                <td>{{ $datum->po_buyer }}</td>
                                <td>{{ $datum->item_code }}</td>
                                <td>{{ $datum->color }}</td>
                                <td>{{ $datum->style }}</td>
                                <td>{{ $datum->article_no }}</td>
                                <td>{{ $datum->warehouse }}</td>
                                <td>{{ $datum->uom }}</td>
                                <td>{{ $datum->qty_consumtion }}</td>
                                <td>{{ $datum->total_allocation }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </p>
    </div>

    <div style="font-size: 15px; margin-top: 10px">
        <h2> Silahkan cek attachement untuk melihat keseluruhan data</h2>
        Email ini dikirimkan otomatis melalui aplikasi WMS. diharapkan untuk tidak membalas email ini.
        <br/>
        <b>Jangan Lupa untuk melakukan Alokasi di WMS sebelum proses relax fabric dimulai</b> 
        <p style="margin: 1em 0;" style="margin-top: 5px">Demikan yang dapat diinfokan, <br/>
        Terima kasih,<br/>
        WMS - Autobot</p>
    </div>
    <br>

    <hr>

</body>
</html>