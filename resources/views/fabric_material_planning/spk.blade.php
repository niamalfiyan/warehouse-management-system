<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>SPK PLANNING {{ $barcode_planning }}</title>

		<style type="text/css">
				body {
					font-family : 'Helvetica'
				}
				#header{
					margin:0;
					padding:0;
				}
				html, body {
					height: 100%;
					overflow: hidden;
				}
				#wrapper {
					position: absolute;
					overflow: auto;
					left: 0;
					right: 0;
					top: 0;
					bottom: 0;
					border: 4 solid black;
				}
				hr {
					display: block;
					height: 1px;
					border: 0;
					border-top: 3px solid black;
					margin: 1em 0 0px 0;
					padding: 0;
				}
				#data_content{
					font-size:8pt;
					padding-top:10px;
				}
				#data_content span{
					margin-left:5%;
					font-size:8pt;
				}
				#content{
					margin:0px;
					padding:0px;
				}
				

			</style>

		<body>
		<div id="wrapper">
			<div id="header">
				<table>
					<tr>
						<td style="width:500px;">
							<span>
								<b style="margin-left:5%;">SPK PLANNING</b>
							</span>
						</td>
                        <td>
						<!--<center>
								<img src="data:image/png;base64,{{DNS1D::getBarcodePNG("$barcode_planning", 'C39')}}" alt="barcode" height="70" width="200" />
							</center>-->
						</td>
				</table>
				<hr width="100%" style="margin:0;margin-top:10;">
			</div>
			<div id="content">
				<div id="data_content">
					<table class="head">
						<tr>
							<td style="width:105px;">Tanggal Planning</td>
							<td style="width:15px;">:</td>
							<td style="width:145px;">{{ $__planning_date }}</td>
                        </tr>
                    </table>
				</div>
				<div id="data_content">
					<table style="border-collapse:collapse;width:100%;text-align:left;padding-bottom:20px;padding-top:20px;border:solid 1px">
                    <tr>
						<th style="text-align:left;border:solid 1px;">#</th>
						<th style="text-align:left;border:solid 1px;">PO BUYER</th>
						<th style="text-align:left;border:solid 1px;">ITEM CODE</th>
						<th style="text-align:left;border:solid 1px;">COLOR</th>
						<th style="text-align:left;border:solid 1px;">TOTAL</th>
						<th style="text-align:left;border:solid 1px;">NO PO SUPPLIER</th>
						<th style="text-align:left;border:solid 1px;">SUPPLIER NAME</th>
						<th style="text-align:left;border:solid 1px;">ETA</th>
						<th style="text-align:left;border:solid 1px;">NO INVOICE</th>
						<th style="text-align:left;border:solid 1px;">STATUS</th>
						<th style="text-align:left;border:solid 1px;">LOCATION</th>
					</tr>
                    
                    @foreach($array as $key => $material_planning_fabric)
                        <tr>
                            <td style="border:solid 1px;">{{ $key+1 }}.</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->po_buyer }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->item_code }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->color }}</td>
                            <td style="border:solid 1px;">{{ number_format($material_planning_fabric->qty_need,2,'.',',') }} ({{ $material_planning_fabric->uom }})</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->document_no }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->supplier_name }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->eta_date }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->no_invoice }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->status }}</td>
                            <td style="border:solid 1px;">{{ $material_planning_fabric->location }}</td>
                            
                        </tr>
                    @endforeach
                    
					
					
					</table>
					
				</div>
				
			</div>
			
			
		</div>
		</body>
	</head>
</html>
