<table class="table">
	<thead>
	  <tr>
		<th>PO SUPPLIER</th>
		<th>ROLL NUMBER</th>
		<th>BATCH NUMBER</th>
		<th>RACK</th>
		<th>STOCK</th>
		<th>AVAILABLE QTY</th>
		<th>ACTION</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->document_no }}
				</td>
				<td>
					{{ $list->nomor_roll }}
				</td>
				<td>
					{{ $list->batch_number }}
				</td>
				<td>
					{{ $list->locator->code }}
				</td>
				<td>
					{{ $list->stock }}({{ $list->uom }})
				</td>
				<td>
					{{ $list->available_qty }}({{ $list->uom }})
				</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
						type="button" data-id="{{ $list->id }}" data-roll="{{ $list->nomor_roll }}" data-name="{{ $list->batch_number }}" data-rack="{{ $list->locator->code }}" 
						data-document="{{ $list->document_no }}" data-qty="{{ $list->available_qty }}" data-uom="{{ $list->uom }}" data-rowid="{{ $id }}" data-stockid = "{{ $list->id }}"
						data-arrival="{{ $list->material_arrival_id }}" data-barcode="{{ $list->barcode_supplier }}"
					>Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
