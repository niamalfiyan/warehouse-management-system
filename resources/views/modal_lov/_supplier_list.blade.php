<table class="table">
	<thead>
	  <tr>
		<th>NO. PO SUPPLIER</th>
		<th>NO. PO BUYER</th>
		<th>ACTION</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($items as $key => $item)
			<tr>
				<td>{{ $item->document_no }}</td>
				<td>{{ $item->po_buyer }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" type="button"
						data-id="{{ $item->id }}" data-name="{{ $item->document_no }} - {{ $item->po_buyer }}"
						data-obj="{{ $item->toJson() }}"
					>
						Select
					</button>
				</td>

				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $items->appends(Request::except('page'))->render() !!}
