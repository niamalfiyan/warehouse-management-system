<table class="table">
	<thead>
	  <tr>
		<th>ITEM CODE</th>
		<th>ITEM DESC</th>
		<th>CATEGORY</th>
		<th>ACTION</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>
					{{ $list->item_code }}
				</td>
				<td>
						{{ $list->item_desc }}
				</td>
				<td>
						{{ $list->category }}
				</td>
				<td>
					
					@if ($list->checkUsedBy($list->item_code,'preparation-febric'))
							@if ($list->checkUsedBy($list->item_code,'preparation-febric')->user_id != auth::user()->id)
									item ini sedang di gunakan oleh {{ $list->checkUsedBy($list->item_code)->user->name }}.
							@else
									<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
									type="button" data-id="{{ $list->item_code }}" data-name="{{ $list->item_desc }}" data-category="{{ $list->category }}">Select
								</button>
							@endif
							
					@else
							<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose" 
							type="button" data-id="{{ $list->item_code }}" data-name="{{ $list->item_desc }}" data-category="{{ $list->category }}">Select
						</button>
					@endif
					
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
