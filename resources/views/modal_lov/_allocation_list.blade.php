<table class="table">
	<thead>
	  <tr>
			<!--<th>Allocation Information</th>-->
			<th>ITEM</th>
			<th>NO. PO SUPPLIER</th>
			<th>PO BUYER</th>
			<th>KRITERIA</th>
			<th>UOM</th>
			<th>QTY BOOKING</th>
			<th>STATUS KETERSEDIAAN</th>
			<!--<th>Action</th>-->
		</tr>
	</thead>

	<tbody>
		@foreach ($items as $key => $item)
			<tr>
				<td>
					<b>KODE ITEM</b>
					<p>{{ $item->item_code  }}</p>

					<b>KATEGORI ITEM</b>
					<p>{{ $item->item_category  }}</p>
					<b>JOB</b>
					<p>{{ $item->job  }}</p>

					<b>STYLE</b>
					<p>{{ $item->style  }}</p>
				</td>
				<td>{{ $item->no_po_supplier  }}</td>
				<td>{{ $item->po_buyer  }}</td>
				<td>
					@if (!is_null($item->miscellaneous_item_id))
						{{ $item->miscellaneous->criteria->name }}		
					@endif
				</td>
				<td>{{ $item->uom  }}</td>
				<td>{{ number_format($item->qty_booking, 2, '.', ',')  }}</td>
				<td>
					@if (!is_null($item->miscellaneous_item_id))
						<span class="label label-success">BARANG ADA DI GUDANG</span>
					@else
						<span class="label label-danger">BARANG BELUM ADA DI GUDANG</span>		
					@endif
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $items->appends(Request::except('page'))->render() !!}
