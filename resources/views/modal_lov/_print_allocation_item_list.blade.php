<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Item code</th>
			<th>Item desc</th>
			<th>Action</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->item_code }}</td>
				<td>{{ $list->item_desc }}</td>
				<td>
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" type="button"
						data-id="{{ $list->item_code }}" data-name="{{ $list->item_desc }}" 
					>
						Select
					</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
