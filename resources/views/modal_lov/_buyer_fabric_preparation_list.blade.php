<table class="table">
	<thead>
	  <tr>
		<th>PO BUYER</th>
		<th>STYLE</th>
		<th>ACTION</th>
	  </tr>
	</thead>

	<tbody>
		@foreach ($lists as $key => $list)
			@php($qty_required_asal = $list->qtyRequiredNow($list->po_buyer, $list->item_code, $list->style, $list->qty_required))
			<tr>
				<td>
					{{ $list->po_buyer }}
				</td>
				<td>
					{{ $list->style }}
				</td>
				<td>

					<button data-dismiss="modal" class="btn btn-info btn-xs btn-choose"
						type="button" data-id="{{ $list->po_buyer }}" data-name="{{ $list->po_buyer }}"
						data-style="{{ $list->style}}" data-qty_required_asal="{{ $qty_required_asal }}"
						data-qty_required="{{ $qty_required_asal }}" data-uom="{{ $list->uom}}" data-rowid="{{ $id }}">
					Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
