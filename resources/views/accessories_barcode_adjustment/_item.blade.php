<script type="x-tmpl-mustache" id="material-adjustment-table">
	{% #item %}
		<tr id="panel_{% _id %}">
			<td>{% nox %}</td>
			<td>{% document_no %}</td>
			<td>{% po_buyer %}</td>
			<td>{% item_code %} ({% category %})</td>
			<td>{% uom_conversion %}</td>
			<td>{% qty_need %}</td>
			<td>{% qty_before_adj %}</td>
			<td>{% qty_adj %}</td>
			<td>{% qty_after_adj %}</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-icon-anim btn-circle btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/item%}
	<tr>
		<td colspan="11">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
		</td>
	</tr>
</script>