@extends('layouts.app', ['active' => 'master_data_role'])


@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
			<h4><a href="{{ route('dashboard') }}"></a><span class="text-semibold">Master Data Role</span></h4>
				{!! Form::hidden('flag_msg',$flag , array('id' => 'flag_msg')) !!}
				{!! Form::hidden('permissions', '[]', array('id' => 'permissions')) !!}
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
					<li><a href="{{ route('dashboard') }}">Master Data</a></li>
					<li><a href="{{ route('dashboard') }}">Authentication</a></li>
					<li class="active">Role</li>
			</ul>

			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('masterDataRole.create') }}"><i class="icon-plus2 pull-right"></i> Create</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

@endsection
@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<div class="table-responsive">
				{!! $html->table(['class'=>'table datatable-basic']) !!}
			</div>
		</div>
@endsection

@section('page-modal')

@endsection

@section('page-js')
	{!! $html->scripts() !!}
	<script type="text/javascript" src="{{ asset(elixir('js/bootbox.js'))}}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_role.js'))}}"></script>
	
@endsection
