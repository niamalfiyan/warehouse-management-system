<script type="x-tmpl-mustache" id="permission-table">
	<tr class="{{ $errors->has('permissions') ? 'has-error' : '' }}">
		<td>
			#
		</td>
		<td>
			<div style="width:100%">
				@include('form.picklist', [
					'field' => 'permission',
					'name' => 'permission',
					'placeholder' => 'Select Permission'
				])
			</div>
			<span class="text-danger hidden" id="errorInput">
				Permission Already Selected.
			</span>
		</td>
		<td>
			<textarea id="description" name="description" readonly class="form-control input-description input-new" ></textarea>
		</td>
		<td>
			<button type="button" id="AddItemButton"  class="btn btn-info btn-icon-anim btn-circle btn-add-item"><i class="icon-plus-circle2"></i></button>
		</td>
	</tr>

	{% #item %}
		<tr>
			<td style="text-align:center;">{% no %}</td>
			<td>
				<div id="item_{% _id %}">{% name %}</div>
				<input type="text" class="form-control input-sm hidden" id="itemInputId_{% _id %}" 
					value="{% id %}" data-id="{% _id %}"
				>
				<span class="text-danger hidden" id="errorInput_{% _id %}">
					Permission Already Selected.
				</span>
			</td>
			<td>
				<div id="description_{% _id %}" >{%description%}</div>
			</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-danger btn-icon-anim btn-delete-item"><i class="icon-close2"></i></button>
			</td>
		</tr>
	{%/item%}
</script>