@extends('layouts.app', ['active' => 'accessories_report_material_out'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Out</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Report Material Out</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
			<div class="heading-elements">
				<ul class="icons-list">
					<li><a data-action="collapse"></a></li>
				</ul>
			</div>
		</div>
		<div class="panel-body">
			{{-- {!!
				Form::open(array(
					'class' 	=> 'form-horizontal',
					'role' 		=> 'form',
					'url' 		=> ,
					'method' 	=> 'get',
					'target' 	=> '_blank'		
				))
			!!} --}}

			@include('form.select', [
				'field' 		=> 'warehouse',
				'label' 		=> 'Warehouse',
				'default' 		=> auth::user()->warehouse,
                // 'default'       => '1000013',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'options' 		=> [
					'1000002' 	=> 'Warehouse Accessories AOI 1',
					'1000013' 	=> 'Warehouse Accessories AOI 2',
                   
				],
				'class' 		=> 'select-search',
				'attributes' 	=> [
					'id' 		=> 'select_warehouse'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'start_date',
				'label' 		=> 'Last Movement Date From (00:00)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				// 'default'		=> \Carbon\Carbon::now()->subDays(7)->format('d/m/Y'),
                'default'		=> '21/05/2021',
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'end_date',
				'label' 		=> 'Last Movement Date To (23:59)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->addDays(30)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])
			{!! Form::hidden('active_tab_export','[]' , array('id' => 'active_tab_export')) !!}

			
			<button type="button" id="export" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
		{!! Form::close() !!}
		</div>
	</div>

	<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
		<ul class="nav navbar-nav visible-xs-block">
			<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-filter">
			{{-- <ul class="nav navbar-nav">
				<li class="active"><a href="#reguler" data-toggle="tab" onclick="changeTab('reguler')">Reguler <i class="icon-stack3 position-left"></i></a></li>
				<li><a href="#additional" data-toggle="tab" onclick="changeTab('additional')"><i class="icon-file-check position-left"></i> Additional <i class="icon-stack2 position-left"></i></a></li>
				<li><a href="#balance_marker" data-toggle="tab" onclick="changeTab('balance_marker')"><i class="icon-file-check position-left"></i> Balance Marker <i class="icon-stack2 position-left"></i></a></li>
			</ul> --}}
		</div>
	</div>

	<div class="tabbable">
		<div class="tab-content">
			<div class="tab-pane fade in active" id="reguler">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover" id="out_table">
								<thead>
									<tr>
										<th>No</th>
										<th>Movement Date</th>
                                        <th>Warehouse</th>
										<th>Po Supplier</th>
										<th>Item Code</th>
										<th>Po Buyer </th>
                                        <th>UOM</th>										
                                        <th>QTY</th>
                                        <th>Status</th>
										{{-- <th>Action</th> --}}
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>

		
		</div>
	</div>
	{{-- {!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!} --}}
	{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

{{-- @section('page-modal')
	@include('fabric_report_daily_material_preparation._history_modal')
	@include('fabric_report_daily_material_preparation._change_plan_modal')
	@include('fabric_report_daily_material_preparation._change_machine_modal')
@endsection --}}

@section('page-js')
	<script src="{{ mix('js/accessories_report_material_out.js') }}"></script>
@endsection
