<div class="navbar navbar-inverse bg-indigo">
		<div class="navbar-header">
            <a class="navbar-brand" href="/"><i class="position-left"> <img src="{{ asset('images/icon_wms.png') }}" width="30px" height="30px"></i> WAREHOUSE MANAGEMENT SYSTEM</a>

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<p class="navbar-text"><span class="label bg-yellow-400">Online</span></p>

			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						@if(Auth::check())
							@if(auth::user()->photo)
								<img src="{{ route('user.showAvatar', auth::user()->photo) }}" alt="profile_photo" id="avatar_image">
							@else
									@if(auth::user()->sex == 'laki')
										<img src="{{ asset('images/male_avatar.png') }}" alt="avatar_male">
									@elseif(auth::user()->sex == 'perempuan')
										<img src="{{ asset('images/female_avatar.png') }}" alt="avatar_female">
									@else
										<img src="{{ asset('images/male_avatar.png') }}" alt="">
								@endif
							@endif<span>{{ Auth::User()->name }}</span>
						@endif
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ (Auth::check() ? route('user.accountSetting',Auth::User()->id) : '#')  }}"><i class="icon-cog5"></i> Account settings</a></li>
						<li><a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
							<i class="icon-switch2"></i> Logout</a>
							<form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">
								{{ csrf_field() }}
							</form>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>