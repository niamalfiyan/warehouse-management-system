<!-- Second navbar -->
	<div class="navbar navbar-default" id="navbar-second">
		<ul class="nav navbar-nav no-border visible-xs-block">
			<li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-second-toggle">
			<ul class="nav navbar-nav">
				@permission(['menu-material-receive-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_arrival' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialArrival.index') }}"><i class="icon-database-check position-left"></i> Material Arrival</a>
					</li>
				@endpermission

				@permission(['menu-material-in-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_in' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialIn.index') }}"><i class="icon-database-insert position-left"></i> Material In</a>
					</li>
				@endpermission

				@permission(['menu-material-out-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_out' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialOut.index') }}"><i class="icon-database-export position-left"></i> Material Out</a>
					</li>
				@endpermission

				@permission(['menu-material-check-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_quality_control' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialQualityControl.index') }}"><i class="icon-database-time2 position-left"></i> Material Quality Control</a>
					</li>
				@endpermission

				@permission(['menu-material-metal-detector-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_metal_detector' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialMetalDetector.index') }}"><i class="icon-database-time2 position-left"></i> Material Metal Detector</a>
					</li>
				@endpermission

				@permission(['menu-material-stock-opname-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_stock_opname' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialStockOpname.index') }}"><i class=" icon-database-diff position-left"></i> Material Stock Opname</a>
					</li>
				@endpermission

				@permission(['menu-material-receive-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_arrival' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialArrival.index') }}"><i class="icon-database-check position-left"></i> Material Arrival</a>
					</li>
				@endpermission

				@permission(['menu-material-in-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_in' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialIn.index') }}"><i class="icon-database-insert position-left"></i> Material In</a>
					</li>
				@endpermission

				@permission(['menu-material-planning-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_planning' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialPlanning.index') }}"><i class=" icon-database-edit2 position-left"></i> Material Planning</a>
					</li>
				@endpermission
				
				@permission(['menu-material-preparation-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_preparation' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialPreparation.index') }}"><i class=" icon-database-edit2 position-left"></i> Material Preparation</a>
					</li>
				@endpermission

				@permission(['menu-material-out-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_out_cutting' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialOutCutting.index') }}"><i class="icon-database-export position-left"></i> Material Out Cutting</a>
					</li>
				@endpermission

				@permission(['menu-handover-roll-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_out_non_cutting' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialOutNonCutting.index') }}"><i class="icon-database-export position-left"></i> Material Out Non Cutting</a>
					</li>
				@endpermission

				@permission(['menu-material-receive-piping-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_receive_piping' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialReceivePiping.index') }}"><i class="icon-database-export position-left"></i> Material Receive Piping</a>
					</li>
				@endpermission

				@permission(['menu-material-steam-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_steam' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialSteam.index') }}"><i class="icon-database-export position-left"></i> Material Steam</a>
					</li>
				@endpermission

				@permission(['menu-material-check-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_quality_control' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialQualityControl.index') }}"><i class="icon-database-time2 position-left"></i> Material Quality Control</a>
					</li>
				@endpermission

				@permission(['menu-material-reject-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_reject' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialReject.index') }}"><i class="icon-database-remove position-left"></i> Material Reject</a>
					</li>
				@endpermission

				@permission(['menu-qc-lab-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_inspect_lab' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialInspectLab.index') }}"><i class=" icon-lab position-left"></i> Inspect Lab</a>
					</li>
				@endpermission

				<!-- @permission(['menu-qc-lab-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_inspect_lab' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialTesting.index') }}"><i class=" icon-lab position-left"></i> Fabric Testing </a>
					</li>
				@endpermission -->


				@permission(['menu-cancel-preparation-fabric'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-database-remove position-left"></i> Material Cancel <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-cancel-arrival-fabric')
							<li class="{{ isset($active) && $active == 'fabric_material_cancel_arrival' ? 'active' : '' }}"><a href="{{ route('fabricMaterialCancelArrival.index') }}"></i>Material Arrival</a></li>
							@endpermission
							
							@permission('menu-cancel-preparation-fabric')
							<li class="{{ isset($active) && $active == 'fabric_material_cancel_preparation' ? 'active' : '' }}"><a href="{{ route('fabricMaterialCancelPreparation.index') }}"></i>Material Preparation</a></li>
							@endpermission
							
							@permission('menu-cancel-out-handover-fabric')
							<li class="{{ isset($active) && $active == 'fabric_material_cancel_out_non_cutting' ? 'active' : '' }}"><a href="{{ route('fabricMaterialCancelOutNonCutting.index') }}"></i>Material Out Non Cutting</a></li>
							@endpermission
						</ul>
					</li>
				@endpermission

				@permission(['menu-material-reverse-accessories'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-database-refresh position-left"></i> Material Reverse <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-material-reverse-accessories')
								<li class="{{ isset($active) && $active == 'accessories_material_reverse_out' ? 'active' : '' }}"><a href="{{ route('accessoriesMaterialReverseOut.index') }}">Reverse Out</a></li>
								@role(['admin-ict-acc'])
								<li class="{{ isset($active) && $active == 'accessories_material_reverse_quality_control' ? 'active' : '' }}"><a href="{{ route('accessoriesMaterialReverseQualityControl.index') }}">Reverse Qc</a></li>
								@endrole
							@endpermission
						</ul>
					</li>
				@endpermission

				@permission(['menu-material-stock-on-the-fly-accessories'])
					<li class="{{ isset($active) && $active == 'accessories_material_stock_on_the_fly' ? 'active' : '' }}">
						<a href="{{ route('accessoriesMaterialStockOnTheFly.index') }}"><i class="icon-database-add position-left"></i> Material Stock (SOTF)</a>
					</li>
				@endpermission

				@permission(['menu-material-stock-accessories','menu-approval-material-stock-accessories'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-database-add position-left"></i> Material Stock <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-material-stock-accessories')
							<li class="{{ isset($active) && $active == 'accessories_material_stock_data' ? 'active' : '' }}"><a href="{{ route('accessoriesMaterialStock.create') }}">Create</a></li>
							@endpermission
							@permission('menu-approval-material-stock-accessories')
							<li class="{{ isset($active) && $active == 'accessories_material_stock_approval' ? 'active' : '' }}"><a href="{{ route('accessoriesMaterialStockApproval.index') }}">Approval</a></li>
							@endpermission
						</ul>
					</li>
				@endpermission

				@permission(['menu-material-stock-fabric','menu-material-stock-fabric-approval'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-database-add position-left"></i> Material Stock<span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-material-stock-fabric')
							<li class="{{ isset($active) && $active == 'fabric_material_stock' ? 'active' : '' }}"><a href="{{ route('fabricMaterialStock.index') }}"></i> Create</a></li>
							@endpermission
							@permission('menu-material-stock-fabric-approval')
							<li class="{{ isset($active) && $active == 'fabric_material_stock_approval' ? 'active' : '' }}"><a href="{{ route('fabricMaterialStockApproval.index') }}">Approval</a></li>
							@endpermission
						</ul>
					</li>
				@endpermission

				<li class="{{ isset($active) && $active == 'erp_material_requiremet' ? 'active' : '' }}">
					<a href="{{ route('erpMaterialRequirement.index') }}"><i class="icon-database-menu position-left"></i> Material Requirement</a>
				</li>
				
				@permission(['menu-print-barcode-receive-accessories','menu-print-barcode-carton','menu-print-barcode-preparation-accessories','menu-print-barcode-switch','menu-print-material-adjustment','menu-print-barcode-qc-accessories','menu-print-barcode-preparation-fabric','menu-reprint-barcode-supplier-fabric','menu-print-barcode-allocation-accessories','menu-reprint-barcode-supplier-acc','menu-fabric-print-barcode-locator','menu-accessories-print-barcode-locator','menu-print-ila-accessories','menu-print-barcode-reroute'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-barcode2 position-left"></i> Barcode <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-print-barcode-carton')
								<li class="{{ isset($active) && $active == 'accessories_barcode_carton' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeCarton.index') }}">Carton</a></li>
							@endpermission

							@permission('menu-print-barcode-preparation-accessories')
								<li class="{{ isset($active) && $active == 'accessories_barcode_material_preparation' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeMaterialPreparation.index') }}">Preparation</a></li>
							@endpermission
							
							@permission('menu-print-barcode-allocation-accessories')
								<li class="{{ isset($active) && $active == 'accessories_barcode_material_allocation' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeMaterialAllocation.index') }}">Allocation</a></li>
							@endpermission
							
							@permission('menu-reprint-barcode-supplier-acc')
								<li class="{{ isset($active) && $active == 'accessories_barcode_supplier' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeSupplier.index') }}">Reprint Barcode Supplier</a></li>
							@endpermission
							
							@permission('menu-accessories-print-barcode-locator')
								<li class="{{ isset($active) && $active == 'accessories_barcode_locator' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeLocator.index') }}">Locator</a></li>
							@endpermission
							
							@permission('menu-print-ila-accessories')
								<li class="{{ isset($active) && $active == 'accessories_barcode_bill_of_material' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeBillOfMaterial.index') }}">Bill of Material (BOM)</a></li>
							@endpermission
							
							@permission('menu-print-material-adjustment')
								<li class="{{ isset($active) && $active == 'accessories_barcode_adjustment' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeAdjustment.index') }}">Adjustment</a></li>
							@endpermission

							@permission('menu-print-barcode-reroute')
								<li class="{{ isset($active) && $active == 'accessories_barcode_reroute' ? 'active' : '' }}"><a href="{{ route('accessoriesBarcodeReRoute.index') }}">Reroute</a></li>
							@endpermission
							
							@permission('menu-print-barcode-switch')
								<li class="{{ isset($active) && $active == 'accessories_barcode_switch' ? 'active' : '' }}"><a href="{{ route('printBarcodeSwitch.index') }}">Switch</a></li>
							@endpermission

							@permission('menu-print-barcode-qc-accessories')
							<li class="{{ isset($active) && $active == 'accessories_barcode_material_quality_control' ? 'active' : '' }}"><a href="{{ route('accessoriesPrintBarcodeQualityControl.index') }}">Quality Control</a></li>
							@endpermission

							@permission('menu-reprint-barcode-supplier-fabric')
								<li class="{{ isset($active) && $active == 'fabric_barcode_supplier' ? 'active' : '' }}"><a href="{{ route('fabricBarcodeSupplier.index') }}">Reprint Barcode Supplier</a></li>
							@endpermission

							@permission('menu-reprint-barcode-supplier-fabric')
								<li class="{{ isset($active) && $active == 'fabric_print_surat_jalan' ? 'active' : '' }}"><a href="{{ route('fabricPrintSuratJalan.index') }}">Print Surat Jalan</a></li>
							@endpermission

							@permission('menu-fabric-print-barcode-locator')
								<li class="{{ isset($active) && $active == 'fabric_barcode_locator' ? 'active' : '' }}"><a href="{{ route('fabricBarcodeLocator.index') }}">Locator</a></li>
							@endpermission
							
							
						</ul>
					</li>
				@endpermission

				@permission(['menu-material-switch-accessories','menu-allocation-accessories','menu-allocation-non-buyer-accessories','menu-approve-allocation-accessories'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-pie-chart2 position-left"></i> Allocation <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-allocation-accessories')
							<li class="{{ isset($active) && $active == 'accessories_allocation_buyer' ? 'active' : '' }}"><a href="{{ route('accessoriesAllocationBuyer.index') }}">Buyer</a></li>
							@endpermission

							@permission('menu-material-switch-accessories')
							<li class="{{ isset($active) && $active == 'accessories_allocation_switch' ? 'active' : '' }}"><a href="{{ route('accessoriesMaterialSwitch.index') }}">Switch</a></li>
							@endpermission

							@permission('menu-allocation-non-buyer-accessories')
							<li class="{{ isset($active) && $active == 'accessories_barcode_non_buyer' ? 'active' : '' }}"><a href="{{ route('accessoriesAllocationNonBuyer.index') }}">Non Buyer</a></li>
							@endpermission

							@permission('menu-approve-allocation-accessories')
							<li class="{{ isset($active) && $active == 'accessories_allocation_buyer_approval' ? 'active' : '' }}"><a href="{{ route('accessoriesAllocationBuyerApproval.index') }}">Approval</a></li>
							@endpermission
						</ul>
					</li>
				@endpermission

				@permission(['menu-allocation-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_allocation' ? 'active' : '' }}">
						<a href="{{ route('fabricAllocation.index') }}"><i class="icon-pie-chart2 position-left"></i>Allocation</a>
					</li>
				@endpermission


				@permission(['menu-material-stock-opname-fabric'])
					<li class="{{ isset($active) && $active == 'fabric_material_stock_opname' ? 'active' : '' }}">
						<a href="{{ route('fabricMaterialStockOpname.index') }}"><i class="icon-database-diff position-left"></i> Material Stock Opname</a>
					</li>
				@endpermission

				@permission(['menu-accessories-locator'])
					<li class="{{ isset($active) && $active == 'accessories_locator' ? 'active' : '' }}">
						<a href="{{ route('accessoriesLocator.index') }}"><i class="icon-cabinet position-left"></i> Locator</a>
					</li>
				@endpermission

				@permission(['menu-fabric-locator'])
					<li class="{{ isset($active) && $active == 'fabric_locator' ? 'active' : '' }}">
						<a href="{{ route('fabricLocator.index') }}"><i class="icon-cabinet position-left"></i> Locator</a>
					</li>
				@endpermission

				@permission(['menu-report-fabric-inspect-qc','menu-report-integration-movement-reject-fabric','menu-report-integration-movement-reject-accessories','menu-report-integration-out-issue-fabric','menu-report-integration-out-issue-accessories','menu-report-planning-fabric','menu-report-receivement','menu-report-sto-locator','menu-report-stock-fabric','menu-report-summary-batch-per-supplier','menu-report-cutting-instruction','menu-report-actual-width','menu-report-fabric-inspect-lab','menu-monitoring-statistical-date','menu-monitoring-uncomplete-mr','menu-monitoring-receiving','menu-report-monitoring-handover','menu-monitoring-checkout','menu-report-movement','menu-report-stock','menu-report-material-stock','menu-report-check-material','menu-report-receivement-fabric','menu-monitoring-purchase-order','menu-report-out-fabric','menu-report-material-monitoring-allocation','menu-report-material-in','menu-report-material-out','menu-report-material-adjustment','menu-report-material-in-acc','menu-report-material-out-acc','menu-report-material-adjustment-acc','report-preparation-machine','report-material-out-subcont'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-archive position-left"></i> Report <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							<li class="{{ isset($active) && $active == 'report_material_movement' ? 'active' : '' }}"><a href="{{ route('reportMaterialMovement.index') }}">Material Movement</a></li>
							<li class="{{ isset($active) && $active == 'report_memo_request_production' ? 'active' : '' }}"><a href="{{ route('reportMemoRequestProduction.index') }}">Memo Request Production (MRP)</a></li>
							
							@permission('menu-report-receivement')
							<li class="{{ isset($active) && $active == 'accessories_report_daily_material_arrival' ? 'active' : '' }}"><a href="{{ route('accessoriesReportDailyMaterialArrival.index') }}">Daily Material Arrival</a></li>
							@endpermission
							
							@permission('menu-monitoring-receiving')
							<li class="{{ isset($active) && $active == 'accessories_report_daily_material_preparation' ? 'active' : '' }}"><a href="{{ route('accessoriesReportDailyMaterialPreparation.index') }}">Daily Material Preparation</a></li>
							@endpermission

							@permission('menu-report-material-ila')
							<li class="{{ isset($active) && $active == 'accessories_report_material_ila' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialIla.index') }}">Material Kelolosan Scan Out</a></li>
							@endpermission
						
							@permission('menu-monitoring-checkout')
							<li class="{{ isset($active) && $active == 'accessories_report_daily_material_out' ? 'active' : '' }}"><a href="{{ route('accessoriesReportDailyMaterialOut.index') }}">Daily Material Out </a></li>
							@endpermission

							@permission(['menu-report-summary-material-stock-acc'])
							<li class="{{ isset($active) && $active == 'accessories_report_summary_material_stock' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialSummaryStock.index') }}">Material Stock Per Item (BC)</a></li>
							@endpermission

							@permission(['menu-report-material-stock','menu-report-stock'])
							<li class="{{ isset($active) && $active == 'accessories_report_material_stock' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialStock.index') }}">Material Stock </a></li>
							@endpermission

							@permission('menu-report-check-material')
							<li class="{{ isset($active) && $active == 'accessories_report_material_quality_control' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialQualityControl.index') }}">Material Quality Control (QC)</a></li>
							@endpermission

							@permission('menu-report-metal-detector')
							<li class="{{ isset($active) && $active == 'accessories_report_material_metal_detector' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialMetalDetector.index') }}">Material Metal Detector</a></li>
							@endpermission

							@permission('menu-report-allocation-bapb')
							<li class="{{ isset($active) && $active == 'accessories_report_material_bapb' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialBAPB.index') }}">Material BAPB</a></li>
							@endpermission
							@permission('menu-report-material-stock-opname-acc')
							<li class="{{ isset($active) && $active == 'accessories_report_material_stock_opname' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialStockOpname.index') }}">Material Stock Opname</a></li>
							@endpermission
							@permission('menu-report-allocation-cancel-order-accessories')
							<li class="{{ isset($active) && $active == 'accessories_report_material_allocation_cancel_order' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialAllocationCancelOrder.index') }}">Material Allocation Cancel Order</a></li>
							@endpermission
							@permission('menu-report-allocation-cancel-order-accessories')
							<li class="{{ isset($active) && $active == 'accessories_report_material_allocation_carton' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialAllocationCarton.index') }}">Material Allocation Carton</a></li>
							@endpermission
							
							@permission('menu-report-integration-movement-reject-accessories')
							<li class="{{ isset($active) && $active == 'accessories_report_integration_movement_reject' ? 'active' : '' }}"><a href="{{ route('accessoriesReportIntegrationMovementReject.index')}}">Integration Movement Reject</a></li>
							@endpermission

							@permission('menu-report-integration-out-issue-accessories')
							<li class="{{ isset($active) && $active == 'accessories_report_integration_out_issue' ? 'active' : '' }}"><a href="{{ route('accessoriesReportIntegrationOutIssue.index')}}">Integration Out Issue</a></li>
							@endpermission

							@permission('menu-report-allocation-cancel-order-accessories')
							<li class="{{ isset($active) && $active == 'accessories_report_material_oustanding_cancel_order' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialOutstandingCancelOrder.index') }}">Material Outstanding Cancel Order</a></li>
							@endpermission

							
							@permission('report-material-out-subcont')
							<li class="{{ isset($active) && $active == 'accessories_report_material_out_subcont' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialOutSubcont.index') }}">Material Out Subcont</a></li>
							@endpermission

							@permission('menu-report-monitoring-handover')
							<!-- BELUM TAU PENTNING APA GAK <li class="{{ isset($active) && $active == 'monitor-handover' ? 'active' : '' }}"><a href="{{ route('accessoriesReportDailyHandover.index') }}">Daily Handover </a></li>-->
							@endpermission

							@permission('menu-monitoring-uncomplete-mr')
							<!-- BELUM TAU PENTNING APA GAK <li class="{{ isset($active) && $active == 'monitoring_uncomplete_mr' ? 'active' : '' }}"><a href="{{ route('report.monitorUnCompleteMR') }}">MONITORING UNCOMPLETE MR </a></li> -->
							@endpermission
							
							@permission('menu-monitoring-statistical-date')
							<!-- BELUM TAU PENTNING APA GAK <<li class="{{ isset($active) && $active == 'monitor-statistical-date' ? 'active' : '' }}"><a href="{{ route('accessoriesReportStatisticalDate.index') }}">Monitoring Statistical Date </a></li>-->
							@endpermission

							@permission('menu-monitoring-purchase-order')
							<!-- BELUM TAU PENTNING APA GAK <li class="{{ isset($active) && $active == 'monitor-purchase-order' ? 'active' : '' }}"><a href="{{ route('report.purchaseOrder.index') }}">MONITORING PURCHASE ORDER</a></li>-->
							@endpermission
							
							@permission('menu-monitoring-material-receive-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_material_arrival' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyMaterialArrival.index') }}">Daily Material Arrival </a></li>
							@endpermission
							
							@permission('menu-report-monitoring-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_monitoring' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialMonitoring.index') }}">Monitoring Material </a></li>
							@endpermission

							@permission('menu-report-preparation-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_material_preparation' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyMaterialPreparation.index')}}">Daily Material Preparation</a></li>
							@endpermission

							@permission('menu-report-out-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_material_out' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyMaterialOut.index')}}">Daily Material Out</a></li>
							@endpermission

							@permission('menu-report-cutting-instruction')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_cutting_instruction' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyCuttingInstruction.index')}}">Daily Cutting Instructions</a></li>
							@endpermission

							@permission('menu-report-cutting-instruction')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_request_fabric' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyRequestFabric.index')}}">Daily Request Fabric</a></li>
							@endpermission
							
							@permission('menu-report-handover-material-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_material_out_non_cutting' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyMaterialOutNonCutting.index')}}">Daily Material Out Non Cutting</a></li>
							@endpermission
							
							@permission('menu-report-fabric-inspect-qc')
							<li class="{{ isset($active) && $active == 'fabric_report_daily_material_quality_control' ? 'active' : '' }}"><a href="{{ route('fabricReportDailyMaterialQualityControl.index')}}">Daily Material Quality Control (Qc)</a></li>
							@endpermission

							@permission(['menu-report-summary-stock-fabric'])
							<li class="{{ isset($active) && $active == 'fabric_report_summary_material_stock' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialSummaryStock.index') }}">Material Stock Per Item (BC)</a></li>
							@endpermission

							@permission(['menu-report-stock-fabric'])
							<li class="{{ isset($active) && $active == 'fabric_report_material_stock' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialStock.index') }}">Material Stock </a></li>
							@endpermission

							@permission(['menu-report-stock-material-management-fabric'])
							<li class="{{ isset($active) && $active == 'fabric_report_material_stock_material_management' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialStockMaterialManagement.index') }}">Material Stock Material Management</a></li>
							@endpermission
							
							@permission('menu-report-fabric-inspect-lab')
							<li class="{{ isset($active) && $active == 'fabric_report_material_inspect_lab' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialInspectLab.index')}}">Material Inspect Lab</a></li>
							@endpermission
							
							@permission('menu-report-fabric-fir')
							<li class="{{ isset($active) && $active == 'fabric_report_material_quality_control' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialQualityControl.index')}}">Material Quality Control (FIR)</a></li>
							@endpermission

							@permission('menu-report-reject-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_material_reject' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialReject.index')}}">Material Reject Fabric</a></li>
							@endpermission

							@permission('menu-report-bapb-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_material_bapb' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialBapb.index')}}">Material Bapb</a></li>
							@endpermission

							@permission('menu-report-receive-piping-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_material_receive_piping' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialReceivePiping.index')}}">Material Receive Piping</a></li>
							@endpermission

							@permission('menu-report-request-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_material_balance_marker' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialBalanceMarker.index')}}">Material Balance Marker</a></li>
							@endpermission

							@permission('menu-report-request-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_outstanding_material_out' ? 'active' : '' }}"><a href="{{ route('fabricReportOutstandingMaterialOut.index')}}">Outstanding Material Out</a></li>
							@endpermission

							@permission('menu-report-material-stock-opname-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_material_stock_opname' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialStockOpname.index')}}">Material Stock Opname</a></li>
							@endpermission

							@permission('menu-report-summary-batch-per-supplier')
							<li class="{{ isset($active) && $active == 'fabric_report_summary_batch_supplier' ? 'active' : '' }}"><a href="{{ route('fabricReportSummaryBatchSupplier.index')}}">Summary Batch Supplier</a></li>
							@endpermission

							@permission('menu-report-integration-movement-reject-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_integration_movement_reject' ? 'active' : '' }}"><a href="{{ route('fabricReportIntegrationMovementReject.index')}}">Integration Movement Reject</a></li>
							@endpermission

							@permission('menu-report-integration-out-issue-fabric')
							<li class="{{ isset($active) && $active == 'fabric_report_integration_out_issue' ? 'active' : '' }}"><a href="{{ route('fabricReportIntegrationOutIssue.index')}}">Integration Out Issue</a></li>
							@endpermission

							@permission('material-report-in')
							<li class="{{ isset($active) && $active == 'fabric_report_material_in' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialIn.index')}}">Report Material In</a></li>
							@endpermission

							@permission('report-material-out')
							<li class="{{ isset($active) && $active == 'fabric_report_material_out' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialOut.index')}}">Report Material Out</a></li>
							@endpermission

							@permission('report-material-adjustment')
							<li class="{{ isset($active) && $active == 'fabric_report_material_adjustment' ? 'active' : '' }}"><a href="{{ route('fabricReportMaterialAdjustment.index')}}">Report Material Adjustment</a></li>
							@endpermission

							@permission('report-material-in-acc')
							<li class="{{ isset($active) && $active == 'accessories_report_material_in' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialIn.index')}}">Report Material In</a></li>
							@endpermission

							@permission('report-material-out-acc')
							<li class="{{ isset($active) && $active == 'accessories_report_material_out_acc' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialOut.index')}}">Report Material Out</a></li>
							@endpermission

							@permission('report-material-adjustment-acc')
							<li class="{{ isset($active) && $active == 'accessories_report_material_adjustment_acc' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialAdjustment.index')}}">Report Material Adjustment</a></li>
							@endpermission

							@permission('menu-report-material-monitoring-allocation')
							<li class="{{ isset($active) && $active == 'report_material_monitoring_allocation' ? 'active' : '' }}"><a href="{{ route('accessoriesReportMaterialMonitoringAllocation.index')}}">Material Monitoring Allocation</a></li>
							@endpermission
							
							@permission('report-preparation-machine')
							<li class="{{ isset($active) && $active == 'report_preparation_machine' ? 'active' : '' }}"><a href="{{ route('reportPreparationMachine.index')}}">Report Output Machine</a></li>
							@endpermission
						</ul>
					</li>
				@endpermission

				<!--
				KONTRAK KERJA
				-->
				@permission(['menu-kontrak-kerja'])
					<li class="{{ isset($active) && $active == 'kontrak-kerja' ? 'active' : '' }}">
						<a href="{{ route('kontrak.index') }}"><i class="icon-transmission position-left"></i> Kontrak Kerja</a>
					</li>
				@endpermission
				<!--
				END KONTRAK KERJA
				-->

				@permission(['menu-area','menu-reroute-buyer','menu-master-auto-allocation','menu-master-supplier','menu-master-purchase-item','menu-destination','menu-material-exclude','menu-user','menu-role','menu-permission','menu-criteria','menu-kontrak-kerja','menu-master-contract','menu-master-po-buyer','menu-master-mapping-stock'])
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="icon-database position-left"></i> Master Data <span class="caret"></span>
						</a>

						<ul class="dropdown-menu width-250">
							@permission('menu-master-auto-allocation')
								<li class="{{ isset($active) && $active == 'master_data_auto_allocation' ? 'active' : '' }}"><a href="{{ route('masterDataAutoAllocation.index') }}"> Auto Allocation </a></li>
							@endpermission

							@permission(['menu-material-saving-fabric'])
								<li class="{{ isset($active) && $active == 'master_data_material_saving' ? 'active' : '' }}"><a href="{{ route('masterDataMaterialSaving.index') }}"> Material Saving </a></li>
							@endpermission
							
							@permission('menu-master-purchase-item')
								<li class="{{ isset($active) && $active == 'master_data_purchase_item' ? 'active' : '' }}"><a href="{{ route('masterDataPurchaseItem.index') }}">Purchase Item</a></li>
							@endpermission

							@permission('menu-material-exclude')
								<li class="{{ isset($active) && $active == 'master_data_material_print_bom' ? 'active' : '' }}"><a href="{{ route('masterDataMaterialPrintBom.index') }}"> Material Print Bom </a></li>
							@endpermission

							@permission('menu-defect')
								<li class="{{ isset($active) && $active == 'master_data_defect' ? 'active' : '' }}"><a href="{{ route('masterDataDefect.index') }}"> Defect </a></li>
							@endpermission


							@permission('menu-master-po-buyer')
								<li class="{{ isset($active) && $active == 'master_data_po_buyer' ? 'active' : '' }}"><a href="{{ route('masterDataPoBuyer.index') }}"> Po Buyer </a></li>
							@endpermission

							@permission('menu-reroute-buyer')
								<li class="{{ isset($active) && $active == 'master_data_reroute_buyer' ? 'active' : '' }}"><a href="{{ route('masterDataReroute.index') }}"> Reroute Buyer </a></li>
							@endpermission

							@permission('menu-master-supplier')
								<li class="{{ isset($active) && $active == 'master_data_po_supplier' ? 'active' : '' }}"><a href="{{ route('masterDataPoSupplier.index') }}"> Po Supplier </a></li>
							@endpermission

							@permission('menu-master-mapping-stock')
								<li class="{{ isset($active) && $active == 'master_data_mapping_stock' ? 'active' : '' }}"><a href="{{ route('masterDataMappingStock.index') }}"> Mapping Stock </a></li>
							@endpermission

							@permission('menu-master-item')
								<li class="{{ isset($active) && $active == 'master_data_item' ? 'active' : '' }}"><a href="{{ route('masterDataItem.index') }}"> Item </a></li>
							@endpermission

							@permission('menu-area')
								<li class="{{ isset($active) && $active == 'master_data_area_and_locator' ? 'active' : '' }}"><a href="{{ route('masterDataAreaAndLocator.index') }}"> Area & Locator </a></li>
							@endpermission

							@permission('menu-destination')
								<li class="{{ isset($active) && $active == 'master_data_destination' ? 'active' : '' }}"><a href="{{ route('destination.index') }}"> Destination </a></li>
							@endpermission

							@permission(['menu-user','menu-permission','menu-role'])
								<li class="dropdown-submenu">
									<a href="#">User Management</a>
									<ul class="dropdown-menu width-200">
										@permission('menu-permission')
											<li class="{{ isset($active) && $active == 'master_data_permission' ? 'active' : '' }}"><a href="{{ route('masterDataPermission.index') }}">Permission</a></li>
										@endpermission

										@permission('menu-role')
											<li class="{{ isset($active) && $active == 'master_data_role' ? 'active' : '' }}"><a href="{{ route('masterDataRole.index') }}">Role</a></li>
										@endpermission
										
										@permission('menu-user')
											<li class="{{ isset($active) && $active == 'master_data_user' ? 'active' : '' }}"><a href="{{ route('masterDataUser.index') }}">User</a></li>
										@endpermission
									</ul>
								</li>
							@endpermission
						</ul>
					</li>
				@endpermission

				<li class="{{ isset($active) && $active == 'search' ? 'active' : '' }}">
					<a href="{{ route('search.index') }}"><i class="icon-folder-search position-left"></i> Search</a>
				</li>

				@permission(['menu-testing-lab'])
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="icon-database-remove position-left"></i> Material Testing Lab <span class="caret"></span>
					</a>

					<ul class="dropdown-menu width-250">
						@permission('menu-testing-lab')
						<li class="{{ isset($active) && $active == 'master_data_fabric_testing' ? 'active' : '' }}"><a href="{{ route('masterDataFabricTesting.index') }}"> Fabric Testing </a></li>
						@endpermission
				
						@permission('menu-testing-lab')
						<li class="{{ isset($active) && $active == 'report_overview_testing_lab' ? 'active' : '' }}"><a href="{{ route('reportOverviewTestingLab.index') }}"> Report Overview Testing </a></li>
						@endpermission 
					</ul>
				</li>
			@endpermission
			</ul>
		</div>
	</div>
	<!-- /second navbar -->