@extends('layouts.app', ['active' => 'master_data_destination'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Destination</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li><a href="{{ route('dashboard') }}">Master Data</a></li>
				<li><a href="{{ route('destination.index') }}">Destination</a></li>
				<li class="active">Edit</li>
			</ul>
		</div>
	</div>
@endsection
@section('page-content')
	{!!
		Form::model($destination,
		[
			'role' 		=> 'form',
			'url' 		=> route('destination.update', $destination->id),
			'method' 	=> 'post',
			'enctype' 	=> 'multipart/form-data',
			'class' 	=> 'form-horizontal',
			'id'		=> 'form'
		])
	!!}
	<div class="row">
		<div class="col-lg-5">
			<div class="panel panel-default border-grey">
				<div class="panel-body">
					@include('form.select', [
						'field' 	=> 'warehouse',
						'label' 	=> 'Warehouse',
						'default'		=> $destination->warehouse,
						'mandatory' 	=> '*Required',
						'options' 		=> [
							'' 			=> '-- Select Warehouse --',
							'1000002' 	=> 'Accessories AOI 1',
							'1000013' 	=> 'Accessories AOI 2',
							'1000001' 	=> 'Fabric AOI 1',
							'1000011' 	=> 'Fabric AOI 2',
						],
						'class' => 'select-search',
						'label_col' => 'col-md-2 col-lg-2 col-sm-12',
						'form_col' => 'col-md-10 col-lg-10 col-sm-12',
						'attributes' => [
							'id' => 'warehouse'
						]
					])
					
					@include('form.text', [
						'field' 		=> 'name',
						'label' 		=> 'Name',
						'default'		=> $destination->name,
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'placeholder' 	=> 'Please type name here',
						'mandatory' 	=> '*Required',
						'attributes' => [
							'id' 		=> 'name',
							'readonly' 	=> 'readonly'
						]
						
					])

					@include('form.checkbox', [
						'field' 		=> 'is_subcont',
						'label' 		=> 'Is Subcont',
						'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
						'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
						'style_checkbox'=> 'checkbox checkbox-switchery',
						'class' 		=> 'switchery',
						'attributes' 	=> [
							'id' 		=> 'checkbox_is_subcont'
						]
					])
					
					<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
				</div>
			</div>
		</div>
		<div class="col-lg-7">
			<div class="panel panel-default border-grey">
				<div class="panel-heading">
					<span class="label label-info heading-text">Detail Destination.</span>
				</div>

				<div class="panel-body">
					<div class="table-responsive" id="destination_error">
						<table class="table table-striped table-hover table-responsive">
							<thead>
								<tr>
									<th>NO.</th>
									<th>Name</th>
									<th>Ordering</th>
									<th>Erp Code Id</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody id="tbody-destination">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	{!! Form::hidden('url_destination_index',route('destination.index') , array('id' => 'url_destination_index')) !!}
	{!! Form::hidden('destination',$locators , array('id' => 'destination')) !!}
	{!! Form::hidden('page','edit' , array('id' => 'page')) !!}
	{!! Form::close() !!}
@endsection

@section('page-js')
	@include('master_data_destination._item')
	<script src="{{ mix('js/switch.js') }}"></script>
	<script type="text/javascript" src="{{ asset(elixir('js/master_data_destination.js'))}}"></script>
@endsection