<script type="x-tmpl-mustache" id="material-sto-table">
	{% #list %}
		<tr  {% #is_sto %} style="background-color:#d9ffde" {% /is_sto %} {% #is_error %} style="background-color:#fab1b1;" {% /is_error %}>
			<td>
				{% no %}
				<input type="checkbox" class="checkbox_item" id="check_{% id %}" data-id="{% _id %}" {% #check_all %} checked="checked" {% /check_all %}>
			
			</td>
			<td>
				<p><b>Last Sto Date </b> {% last_sto_date %} - {% last_sto_name %}</<p>
				<p><b>Source</b> {% source_rak_code %}</<p>
				<p><b>Po Supplier</b> {% document_no %}</<p>
				<p><b>Item</b> {% item_code %} ({% category %})</<p>
				<p><b>Color</b> {% color %}</<p>
				<p><b>Batch Number</b> {% batch_number %}</<p>
				<p><b>No Roll</b> {% nomor_roll %}</<p>
				<p><b>Uom</b> {% uom %}</<p>
				<p><b>Available Qty</b> {% available_qty_on_db %} -> {% available_qty %}</<p>
				<p>
					<div class="row">
						<label for="adj_stock" class="control-label text-semibold col-xs-4">
							<b>Sto Qty</b> 
						</label>
						<div class="col-xs-8">
							<input type="text" class="form-control input-sm input-item input-edit input-number adj-stock" id="adjStockInput_{% id %}" value="{% adjustment_stock %}" data-id="{% _id %}" placeholder="INPUT ADJ STOCK" {% #check_all %} readonly="readonly" {% /check_all %}> 
						</div>
					</div>
				</<p>
				<p>
					<div class="row">
						<label for="adj_stock" class="control-label text-semibold col-xs-4">
							<b>Remark</b> 
						</label>
						<div class="col-xs-8">
							<textarea rows="3" class="form-control input-sm input-item input-edit remark-user" style="resize: none;" id="remarkInput_{% _id %}" data-id="{% _id %}">{% remark %}</textarea>
						</div>
					</div>
				</p>
				</td>
			<td>
				<button type="button" id="delete_{% _id %}" data-id="{% _id %}" class="btn btn-default btn-delete-item"><i class="fa icon-trash"></i></button>
			</td>
		</tr>
	{%/list%}
	<tr>
		<td colspan="3">
			<input type="text" id="barcode_value" name="barcode_value" class="form-control input-new barcode_value" autocomplete="off" placeholder="Please scan your barcode here"></input>
		</td>
		
	</tr>
</script>