@extends('layouts.app', ['active' => 'fabric_material_stock_opname'])

@section('page-header')
<div class="page-header">
    <div class="page-header-content">
        <div class="page-title">
            <h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Material Stock Opname</span></h4>
        </div>
    </div>
    <div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
        <ul class="breadcrumb">
            <li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Material Stock Opname</li>
		</ul>
		@role(['admin-ict-fabric'])
			<ul class="breadcrumb-elements">
				<li class="dropdown">
					<a href="#" class="dropdown-toggle legitRipple breadcrumb-dropdown" data-toggle="dropdown">
						<i class="icon-three-bars position-left"></i>
						Actions
						<span class="caret"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="{{ route('fabricMaterialStockOpname.importRemoveStock') }}"><i class="icon-upload pull-right"></i>Import Remove Stock</a></li>
					</ul>
				</li>
			</ul>
		@endrole
    </div>
</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			@include('form.select', [
					'field'     => 'type',
					'label'     => 'Type',
					'label_col' => 'col-md-2 col-lg-2 col-sm-12',
					'form_col'  => 'col-md-10 col-lg-10 col-sm-12',
					'options'   => [
						'free stock' => 'Free Stock',
						'relax'      => 'Relax',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_type'
					]
				])

			@role(['admin-ict-fabric'])
				@include('form.select', [
					'field' 			=> 'warehouse',
					'label' 			=> 'Warehouse',
					'default'			=> auth::user()->warehouse,
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'options' 			=> [
						'1000001' 		=> 'Warehouse Fabric AOI 1',
						'1000011' 		=> 'Warehouse Fabric AOI 2',
					],
					'class' 			=> 'select-search',
					'attributes' 		=> [
						'id' 			=> 'select_warehouse'
					]
				])
			@else 
				{!! Form::hidden('warehouse',auth::user()->warehouse , array('id' => 'select_warehouse')) !!}
			@endrole

			@include('form.text', [
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'field' 		=> 'no_pt',
				'label' 		=> 'Remark All',
				'placeholder' 	=> 'If you want to set remark as global remark, please type in here',
				'attributes' 	=> [
					'id' 			=> 'remark_all',
					'autocomplete' 	=> 'off'
				]
			])
		
			<button type="button" class="btn btn-default col-xs-12" id="btn_check_all">BAPB ALL<i class="icon-trash position-left"></i></button>
		</div>
	</div>

	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<table class="table table-striped table-hover table-responsive">
				<thead>
					<tr>
						<th>#</th>
						<th>Scan</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody id="tbody-material-sto-fabric"></tbody>
			</table>

			{!!
				Form::open([
					'role' 		=> 'form',
					'url'	 	=> route('fabricMaterialStockOpname.store'),
					'method' 	=> 'post',
					'class' 	=> 'form-horizontal',
					'id'		=> 'form'
				])
			!!}

				@include('form.picklist', [
					'field' 		=> 'locator_in_fabric',
					'name' 			=> 'locator_in_fabric',
					'label' 		=> 'Locator',
					'readonly' 		=> true,
					'placeholder' 	=> 'Please select locator',
					'title' 		=> 'Please select locator',
					'label_col' 	=> 'col-lg-3 col-md-3 col-sm-12',
					'form_col' 		=> 'col-lg-9 col-md-9 col-sm-12',
				])

				{!! Form::hidden('_selectAll',0, array('id' => '_selectAll')) !!}
				{!! Form::hidden('warehouse_id', auth::user()->warehouse , array('id' => 'warehouse_id')) !!}
				{!! Form::hidden('type', '' , array('id' => 'type')) !!}
				{!! Form::hidden('barcode_products','[]' , array('id' => 'barcode_products')) !!}
				{!! Form::hidden('url_fabric_material_stock_opname_create',route('fabricMaterialStockOpname.create'), array('id' => 'url_fabric_material_stock_opname_create')) !!}
				{!! Form::hidden('url_remove_last_user',route('fabricMaterialStockOpname.removeLastUser'), array('id' => 'url_remove_last_user')) !!}
				
				<div class="form-group text-right" style="margin-top: 10px;">
					<button type="submit" class="btn btn-blue-success col-xs-12 btn-lg">Save <i class="icon-floppy-disk position-left"></i></button>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
@endsection

@section('page-modal')
	@include('form.modal_picklist', [
		'name' 				=> 'locator_in_fabric',
		'title' 			=> 'List Locator',
		'placeholder' 		=> 'Search based on name / Code',
	])
@endsection

@section('page-js')
	@include('fabric_material_stock_opname._item')
	<script src="{{ mix('js/fabric_material_stock_opname.js') }}"></script>
@endsection
