@extends('layouts.app', ['active' => 'master_data_mapping_stock'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Mapping Stock</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataMappingStock.index') }}" id="url_master_data_mapping_stock">Mapping Stock</a></li>
				<li class="active">Create</li>
			</ul>

			
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			{{ 
				Form::open([
				'method' 	=> 'POST',
				'id' 		=> 'form',
				'class' 	=> 'form-horizontal',
				'url' 		=> route('masterDataMappingStock.store')]) 
			}}
				
				@include('form.text', [
					'field' => 'document_number',
					'label' => 'Document No',
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'placeholder' => 'Please type document no here',
					'attributes' => [
						'id' => 'document_number'
					]
				])
				@include('form.text', [
					'field' => 'type_stock_erp_code',
					'label' => 'Type Stock Erp Code',
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'placeholder' => 'Please type type stock erp code here',
					'attributes' => [
						'id' => 'type_stock_erp_code'
					]
				])
				@include('form.text', [
					'field' => 'type_stock',
					'label' => 'Type Stock',
					'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
					'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
					'placeholder' => 'Please type type stock here',
					'attributes' => [
						'id' => 'type_stock'
					]
				])

			<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Save <i class="icon-floppy-disk position-right"></i></button>
		{{ Form::close() }}
		</div>
	</div>
	{!! Form::hidden('page','create', array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/master_data_mapping_stock.js') }}"></script>
@endsection
