<table class="table table-striped table-hover table-responsive">
	<thead>
	  <tr>
			<th>Barcode</th>
			<th>Po Supplier</th>
			<th>Po Buyer</th>
			<th>Item Code</th>
			<th>Style</th>
			<th>Article No</th>
			<th>Action</th>
	  </tr>
	</thead>
	
	<tbody>
		@foreach ($lists as $key => $list)
			<tr>
				<td>{{ $list->barcode }}</td>
				<td>{{ $list->document_no }}</td>
				<td>{{ $list->po_buyer }}</td>
				<td>{{ $list->item_code }}</td>
				<td>{{ $list->style }}</td>
				<td>{{ $list->article_no }}</td>
				<td class="text-center">
					<button data-dismiss="modal" class="btn btn-blue-success btn-xs btn-choose" 
						type="button" 
						data-id="{{ $list->barcode }}" >Select</button>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

{!! $lists->appends(Request::except('page'))->render() !!}
