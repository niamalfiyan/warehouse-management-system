<!DOCTYPE html>
<html>
<head>
	<title>BARCODE :: QC</title>
	<link href="{{ mix('css/print_barcode_wpo.css') }}" rel="stylesheet" type="text/css">
   
</head>
    <body>
		@php $flag = 0 ; @endphp
		@foreach ($items as $key => $value)
			@for ($i = 0; $i < $value->total_carton ; $i++)
				@if (($flag+1) % 3 === 0)
					<p style="page-break-after: always;">&nbsp;</p>
				@endif

				@php
					$get_bom = $value->dataRequirement($value->po_buyer,$value->item_code,$value->style,$value->article_no);
					$season = ($get_bom)? $get_bom->season : '-';
					$qty_need = ($get_bom)? $get_bom->qty_required : '0';
				@endphp

				<div class="outer">
					<div class="title">BARCODE REPRINT QC</div>
					
					<div class="isi">
						<div class="isi1" style="padding-top:0;">
							<div class="isi3" style="height:31px;float:left;width:58%;">
									NO CARTON : <span style="font-size: 24px">{{ $i+1 }}</span> / {{ $value->total_carton }}
							</div>
							<div class="isi2" style="height:31px;width:40.85%;display:block;">
							<span style="font-size: 12px">SEASON {{ $season }}</span> 
							</div>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_code)>40) font-size: 12px @endif">{{ $value->item_code }}</span>
						</div>
						<div class="isi1">
							<span style="@if(strlen($value->item_desc)>40) font-size: 12px @endif">{{ $value->item_desc }}</span>
						</div>

						<div class="isi2">
							<div class="block-kiri" style="justify-content: center; height: 60px;">
								@php 
									$conversion = $value->reconversion($value->item_code,$value->category,$value->uom_conversion,$value->material_stock_id);
									if($conversion){
										$multiplyrate = $conversion->multiplyrate;
										$uom_reconversion = $conversion->uom_from;
									}else{
										$multiplyrate = 1;
										$uom_reconversion = $value->uom_conversion;
									}
									$qty_reconversion = $multiplyrate * $value->qty_conversion; 
									$qty_reject = $value->qty_reject;
								@endphp
								@if ($qty_reject != 0)
									<span style="font-size: 10px; word-wrap: break-word;"> ({{ number_format(($value->qty_conversion), 2, '.', ',') }} {{ strtoupper($value->uom_conversion) }}) ({{ number_format($qty_reconversion, 2, '.', ',') }} {{ strtoupper($uom_reconversion) }})  (REJECT {{ number_format($qty_reject, 2, '.', ',') }} {{ strtoupper($value->uom_conversion) }}) <br/> @if($value->remark) {{ $value->remark }}@endif</span>
								
								@else
									<span style="font-size: 10px; word-wrap: break-word;"> ({{ number_format(($value->qty_conversion), 2, '.', ',') }} {{ strtoupper($value->uom_conversion) }}) ({{ number_format($qty_reconversion, 2, '.', ',') }} {{ strtoupper($uom_reconversion) }})</span>
								@endif
							</div>
							<!--<div class="block-kiri" style="height:35px;line-height: 35px">
								<span style="font-size: px; word-wrap: break-word;">PO BUYER: {{ $value->po_buyer }}  </span>
							</div-->
							<div class="block-kiri" style="height: 39px;">
								<span style="font-size: 10px; word-wrap: break-word;">
									@if ($value->document_no)
										{{ $value->document_no }}
										<br/>{{ $value->supplier_name }}
									@else
										FROM RETURN PRODUKSI
									@endif
									</span>
							</div>
							<div class="block-kiri" style="margin-top: 7px">
								<span style="font-size: 8px; word-wrap: break-word;">Article No : {{ $value->article_no }}<br/>Style : {{ $value->style }}</span>
							</div>
						</div>
						<div class="isi3">
							<div class="block-kiri" style="height: 120px;">
								
								@if($value->qc_status ==  'FULL REJECT')
									<span>BARCODE TIDAK DAPAT DIGUNAKAN KARNA SUDAH DI REJECT SEMUA OLEH QC</span>
								@else 
									@if ($value->total_carton == 1)
										<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$value->barcode", 'C128') }}" alt="barcode"   />			
										<br/><text style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $value->barcode }}</text>
									@else
										@php
											$_barcode = $value->barcode.'-'.($i+1);
										@endphp

										<img style="height: 2.5cm; width: 4.3cm;	 margin-top: 10px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$_barcode", 'C128') }}" alt="barcode"   />			
										<br/><text style="font: 10px monospace;" text-anchor="middle" x="65" y="62">{{ $_barcode }}</text>
									@endif
								@endif
							</div>
							<div class="block-kiri" style="margin-top: 6px;" >
								PO BUYER: <span style="font-size: 16px">
								{{ $value->po_buyer }} @if($value->is_additional)*@endif
								</span>
							</div>
						</div>
					</div>		
				</div>
				@php $flag++;@endphp
				
			@endfor
		@endforeach
		
    </body>
</html>