@extends('layouts.app', ['active' => 'fabric_report_daily_request_fabric'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Request Fabric</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Daily Request Fabric</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		{!!
			Form::open(array(
					'class' => 'form-horizontal',
					'role' => 'form',
					'url' => route('fabricReportDailyCuttingInstruction.export'),
					'method' => 'get',
					'target' => '_blank'		
			))
		!!}

			@include('form.select', [
				'field' => 'factory_id',
				'label' => 'Warehouse',
				'default' => auth::user()->warehouse,
				'label_col' => 'col-md-2 col-lg-2 col-sm-12',
				'form_col' => 'col-md-10 col-lg-10 col-sm-12',
				'options' => [
					// '' => '-- Select Warehouse --',
					'1' => 'Warehouse Fabric AOI 1',
					'2' => 'Warehouse Fabric AOI 2',
				],
				'class' => 'select-search',
				'attributes' => [
					'id' => 'select_warehouse'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'cutting_date_start',
				'label' 		=> 'Planning Date From (00:00)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes'	=> [
					'id' 			=> 'start_date',
					'autocomplete' 	=> 'off',
					'readonly' 		=> 'readonly'
				]
			])
			
			@include('form.date', [
				'field' 		=> 'cutting_date_end',
				'label' 		=> 'Planning Date To (23:59)',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->addDays(30)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'end_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])

		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_daily_request_fabric_table">
				<thead>
					<tr>
						<th>id</th>
						<th>Warehouse</th>
						<th>Queue</th>
						<th>Planning Date</th>
						<th>Article No</th>
						<th>Style</th>
						<th>Po Buyer</th>
						<th>Item Code</th>
						<th>Part no</th>
						{{-- <th>Uom</th> --}}
						<th>Total Need</th>
						<th>Total SSP</th>
						<th>Total BM</th>
						<th>Created By</th>
						<th>Action</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
</div>

{!! Form::hidden('page','index' , array('id' => 'page')) !!}
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_request_fabric.js') }}"></script>
@endsection
