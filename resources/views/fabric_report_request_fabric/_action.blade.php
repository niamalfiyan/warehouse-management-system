
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($export))
                <li><a href="{!! $export !!}" target="_blank"><i class="icon-file-excel"></i> Export</a></li>
            @endif
            @if (isset($confirm_whs))
                <li><a href="#" onclick="confirmWhs('{!! $confirm_whs !!}')"><i class="icon-pencil6"></i> Approve Whs</a></li>
            @endif
            @if (isset($confirm_lab))
                <li><a href="#" onclick="confirmLab('{!! $confirm_lab !!}')"><i class="icon-pencil6"></i> Approve Lab</a></li>
            @endif
        </ul>
    </li>
</ul>
