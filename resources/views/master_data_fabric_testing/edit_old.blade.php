@extends('layouts.app', ['active' => 'master_data_defect'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Defect</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataDefect.index') }}" id="url_master_data_defect_index">Defect</a></li>
				<li class="active">Edit</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-body">
			<form action="{{ route ('masterDataFabricTesting.update')}}" id="form-update" method="POST">
				{{ csrf_field() }}

				<div class="row">

					<div class="col-lg-6">

						<div class="row">

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Master Data Fabric Testing Code</label>
										<input type="hidden" name="id" id="id" value = "{{ $master_data_fabric_testing->id}}"class="form-control">

										<input type="text" name="no_trf" id="no_trf" value = "{{ $master_data_fabric_testing->no_trf}}" class="form-control" readonly>
									</div>
								</div>

							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Buyer</label>

										<input type="text" name="buyer" id="buyer" class="form-control" value = "{{ $master_data_fabric_testing->buyer}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Factory</label>
									   
										<input type="text" name="factory" id="factory" class="form-control" value = "{{ $master_data_fabric_testing->factory}}" style="text-transform: uppercase;">
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">User Id</label>

										<input type="text" name="user_id" id="user_id" class="form-control" value = "{{ $user->name}}" style="text-transform: uppercase;"  readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Asal Specimen</label>

										<input type="text" name="asal_specimen" id="asal_specimen" class="form-control" value = "{{ $master_data_fabric_testing->asal_specimen}}"style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Category Specimen</label>

										<input type="text" name="category_specimen" id="category_specimen" class="form-control" value = "{{ $master_data_fabric_testing->category_specimen}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Type Specimen</label>

										<input type="text" name="type_specimen" id="type_specimen" class="form-control" value = "{{ $master_data_fabric_testing->type_specimen}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Test Required</label>

										<input type="text" name="test_required" id="test_required" class="form-control" value = "{{ $master_data_fabric_testing->test_required}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Previous Trf</label>

										<input type="text" name="previous_trf" id="previous_trf" class="form-control" value = "{{ $master_data_fabric_testing->previous_trf}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Informasi Tanggal</label>

										<input type="text" name="informasi_tanggal" id="informasi_tanggal" class="form-control" value = "{{ $master_data_fabric_testing->informasi_tanggal}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Category</label>

										<input type="text" name="category" id="category" class="form-control" value = "{{ $master_data_fabric_testing->category}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Part Of Specimen</label>

										<input type="text" name="part_of_specimen" id="part_of_specimen" class="form-control" value = "{{ $master_data_fabric_testing->part_of_specimen}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">PO / MO</label>

										<input type="text" name="po_buyer" id="po_buyer" class="form-control" value = "{{ $master_data_fabric_testing->po_buyer}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

						</div>

					</div>

					<div class="col-lg-6">

						<div class="row">

							{{-- <div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">MO</label>

										<input type="text" name="mo" id="mo" class="form-control" value = "{{ $master_data_fabric_testing->mo}}" placeholder="Input Method name ">
									</div>
								</div>
							</div> --}}

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Size</label>

										<input type="text" name="size" id="size" class="form-control" value = "{{ $master_data_fabric_testing->size}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Style</label>

										<input type="text" name="style" id="style" class="form-control" value = "{{ $master_data_fabric_testing->style}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Article No</label>

										<input type="text" name="article_no" id="article_no" class="form-control" value = "{{ $master_data_fabric_testing->article_no}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Color</label>

										<input type="text" name="color" id="color" class="form-control" value = "{{ $master_data_fabric_testing->color}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

						   

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Return Test Sample</label>

										<input type="text" name="return_test_sample" id="return_test_sample" class="form-control" value = "{{ $master_data_fabric_testing->return_test_sample}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Status</label>

										<input type="text" name="status" id="status" class="form-control" value = "{{ $master_data_fabric_testing->status}}"  style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

							<div class="col-lg-12">
								<div class="col-md-12">
									<div class="row">
										<label class="display-block text-semibold">Testing Method Code</label>

										<input type="text" name="testing_method_id" id="testing_method_id" class="form-control" value = "{{ $methods}}" style="text-transform: uppercase;" readonly>
									</div>
								</div>
							</div>

						

						</div>

					</div>

				</div>

				<button type="submit" class="btn btn-blue-success col-xs-12" style="margin-top: 15px">Update <i class="icon-floppy-disk position-right"></i></button>

			</form>
		</div>
	</div>
@endsection

@section('page-js')
	{{-- <script src="{{ mix('js/master_data_defect.js') }}"></script> --}}
@endsection
