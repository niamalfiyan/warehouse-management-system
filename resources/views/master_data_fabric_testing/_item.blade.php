<script type="x-tmpl-mustache" id="upload-fabric-testing-table">
	{% #item %}
		<tr {% #error_upload %} style="background-color:#fab1b1" {% /error_upload %}>
			<td>{% testing_classification %}</td>
			<td>{% method %}</td>
			<td>{% testing_name %}</td>
			<td>{% testing_code %}</td>
			<td>{% requirement %} ({% uom %})</td>
			<td>{% calculation_method %}</td>
			<td>{% min %}</td>
			<td>{% max %}</td>
			<td>{% operator %}</td>
			<td>{% detail_requirement %}</td>
			<td>{% remark %}</td>
		</tr>
	{%/item%}
</script>
