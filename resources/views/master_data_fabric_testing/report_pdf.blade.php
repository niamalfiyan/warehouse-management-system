<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Detail TRF</title>

<style type="text/css">
    * {
        font-family: Arial;
    }
    table{
        font-size: small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: small;
    }
    .gray {
        background-color: lightgray
    }
    
    .marissa tr td{
      vertical-align: top;
    }
</style>

</head>
<body> 
  <?php
   use Carbon\Carbon;
   ?>

  
  <table width="100%" style="table-layout:fixed;  margin-left:auto; margin-right:auto; ">
    <tr>
      <td rowspan="2" style="width: 30%"><img src="{{ public_path('/images/logo_aoi.jpg') }}" alt="image" width="150" height="50" ></td>
      <td style="text-align: center"><h3>APPAREL MATERIAL / GARMENT</h3></td>
      <td rowspan="2" style="width: 30%"><img src="{{ public_path('/images/bbi_logo.png') }}" alt="image" width="165" height="50" ></td>
    </tr>
    <tr>
     <td style="text-align: center"><h3>TEST REQUISITION FORM</h3></td>
    </tr>
   

  </table>
  <br>
  <table width="100%" style="table-layout:fixed;">
    <tr>
      <td><div class="qrcode">
        <img style="height: 40px; width: 200px;	 margin-top: 10px; margin-left: 10px; margin-bottom:10px" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$detil_trf->no_trf", 'C128') }}" alt="barcode"   />	
     </div>
    </td>
    </tr>
    <tr>
      <td style="width:24%">No TRF</td>
      <td style="width:2%">:</td>
      <td>{{$detil_trf->no_trf }}</td>
    </tr>
    <tr>
      <td style="width:24%">Submitted by</td>
      <td style="width:2%">:</td>
      <td>{{ ucwords($name->name) }}</td>
    </tr>
    <tr>
      <td style="width:24%">Date of Submitted</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->created_at }}</td>
    </tr>
   
  </table>

  <br>
  <table width="100%" style="table-layout:fixed;">
    
    <tr>
      <td style="width:24%">Buyer</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->buyer }}</td>
    </tr>
    <tr>
      <td style="width:24%">Lab Location</td>
      <td style="width:2%">:</td>
      <td><?php if($detil_trf->lab_location == 'AOI1') echo 'AOI 1';
        else echo 'AOI 2'; ?></td>
    </tr>
    <tr>
      <td style="width:24%">Origin of speciment</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->asal_specimen }}</td>
    </tr>
    <tr>
      <td style="width:24%">Category Speciment</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->category_specimen }}</td>
    </tr>
    <tr>
      <td style="width:24%">Type of Speciment</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->type_specimen }}</td>
    </tr>
    <tr>
      <td style="width:24%">Category</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->category }}</td>
    </tr>
    <tr>
      <td style="width:24%">Test Required</td>
      <td style="width:2%">:</td>
      <td><?php 
        if($detil_trf->test_required == 'developtesting_t1') echo  'Develop Testing T1';
        elseif($detil_trf->test_required == 'developtesting_t2') echo  'Develop Testing T2';
        elseif($detil_trf->test_required == 'developtesting_selective') echo  'Develop Testing Selective';
        elseif($detil_trf->test_required == 'bulktesting_m') echo  '1st Bulk Testing M (Model Level)';
        elseif($detil_trf->test_required == 'bulktesting_a') echo  '1st Bulk Testing A (Article Level)';
        elseif($detil_trf->test_required == 'bulktesting_selective') echo  '1st Bulk Testing Selective';
        elseif($detil_trf->test_required == 'reordertesting_m') echo  'Re-Order Testing M (Model Level)';
        elseif($detil_trf->test_required == 'reordertesting_a') echo  'Re-Order Testing A (Article Level)';
        elseif($detil_trf->test_required == 'reordertesting_selective') echo  'Re-Order Testing Selective';
        else echo  'Re-Test';
        ?>
      </td>
    </tr>
    <tr>
      <td style="width:24%">Date Information</td>
      <td style="width:2%">:</td>
      <td><?php
        if($detil_trf->date_information_remark == 'buy_ready') echo 'Buy Ready '. $detil_trf->date_information ;
        elseif($detil_trf->date_information_remark == 'output_sewing') echo '1st Output Sewing '. $detil_trf->date_information ;
        else echo 'PODD '. $detil_trf->date_information ;
      ?></td>
    </tr>
   
  </table>

  <br>
  <?php

    if($detil_trf->category_specimen == 'FABRIC')
    {
  ?>

    <table width="100%" style="table-layout:fixed;">
        
      <tr>
        <td style="width:24%">PO Supplier</td>
        <td style="width:2%">:</td>
        <td><?php echo  $detil_trf->orderno . ' ' . $detil_trf->supplier_name; ?></td>
      </tr>
      <tr>
        <td style="width:24%">Item</td>
        <td style="width:2%">:</td>
        <td>{{ $detil_trf->item }}</td>
      </tr>
      <tr>
        <td style="width:24%">Composition</td>
        <td style="width:2%">:</td>
        <td>{{ $detil_trf->composition }}</td>
      </tr>
      <tr>
        <td style="width:24%">No Roll</td>
        <td style="width:2%">:</td>
        <td>{{ $detil_trf->nomor_roll }}</td>
      </tr>
      <tr>
        <td style="width:24%">Batch</td>
        <td style="width:2%">:</td>
        <td>{{ $detil_trf->batch }}</td>
      </tr>
    
    </table>

  <?php
    }else {
  ?>

  <table width="100%" style="table-layout:fixed;">
          
    <tr>
      <td style="width:24%">PO Buyer</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->orderno }}</td>
    </tr>
    <tr>
      <td style="width:24%">Item</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->item }}</td>
    </tr>
    <tr>
      <td style="width:24%">Composition</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->composition }}</td>
    </tr>
    <tr>
      <td style="width:24%">Size</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->size }}</td>
    </tr>
    <tr>
      <td style="width:24%">Style</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->style }}</td>
    </tr>
    <tr>
      <td style="width:24%">Article</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->article_no }}</td>
    </tr>
    <tr>
      <td style="width:24%">Destination</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->destination }}</td>
    </tr>

  </table>
  
  <?php
    }
  ?>
  <br>
  <table width="100%" style="table-layout:fixed;">
          
    <tr>
      <td style="width:24%">Return Test</td>
      <td style="width:2%">:</td>
      <td><?php 
        if($detil_trf->return_test_sample == true)
        {
          echo 'YES';
        }else{
          echo 'NO';
        }
        
        ?></td>
    </tr>
   

  </table>
  <table width="100%" style="table-layout:fixed;">
          
    <tr>
      <td style="width:24%; height:auto;">Part of speciment tested</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->part_of_specimen }}</td>
    </tr>
   

  </table>
  <table width="100%" style="table-layout:fixed;" class="marissa">
          
    <tr>
      <td style="width:24%">Testing methode</td>
      <td style="width:2%">:</td>
      <td><?php
      // dd($method);
      foreach ($method as $key => $metd) {
       
        echo "+ ".$metd->method_name."<br>";
        // echo "<pre>".implode($metd,"\n")."</pre>";
      }
      
     ?></td>
    </tr>
   

  </table>
  <br><br><br>
  <table width="100%" style="table-layout:fixed;">
          
    <tr>
      <td colspan="3">Remarks: Reporting maximum of 4 days</td>
    </tr>
    <tr>
      <td style="width:24%">Status</td>
      <td style="width:2%">:</td>
      <td>{{ $detil_trf->last_status }}</td>
    </tr>
    <tr>
      <td style="width:24%">Date of validate</td>
      <td style="width:2%">:</td>
      <td><?php if($detil_trf->verified_lab_date == null){
        echo '-';
      }else{
        echo $detil_trf->verified_lab_date;
      } ?></td>
    </tr>
    <tr>
      <td style="width:24%">Name of validate</td>
      <td style="width:2%">:</td>
      <td><?php if($detil_trf->verified_lab_by == null){
        echo '-';
      }else{
        $absensi = DB::connection('absence_aoi');
        $users = $absensi->table('adt_bbigroup_emp_new')->where('nik', $detil_trf->verified_lab_by)->first();
        echo $users->name;
      } ?></td>
    </tr>
   

  </table>
 
 

</body>
</html>