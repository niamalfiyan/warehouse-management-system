<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: TRF</title>
    <link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/barcode_rack_fabric.css')) }}">
</head>
<body>
 
       <div class="outer-mini text-uppercase">
		<div class="title">WAREHOUSE MANAGEMENT SYSTEM - FABRIC</div>
		<div class="company">PT. APPAREL ONE INDONESIA</div><br>
		<div class="qrcode">
           <img style="height: 2cm; width: 8cm;	 margin-top: 10px; margin-left: 15px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$master_testings->trf_id", 'C128') }}" alt="barcode"   />	
        </div>
        <br/>
		<div class="footer">			
			<p style="font-size:20px">{{ $master_testings->trf_id }}</p>
		</div>
	</div>	

</body>
</html> 
