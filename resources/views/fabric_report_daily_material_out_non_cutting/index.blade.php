@extends('layouts.app', ['active' => 'fabric_report_daily_material_out_non_cutting'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Daily Material Out Non Cutting</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Daily Material Out Non Cutting</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
		@include('form.select', [
			'field' => 'warehouse',
			'label' => 'Warehouse',
			'default' => auth::user()->warehouse,
			'label_col' => 'col-md-2 col-lg-2 col-sm-12',
			'form_col' => 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				'' => '-- Select Warehouse --',
				'1000001' => 'Warehouse Fabric AOI 1',
				'1000011' => 'Warehouse Fabric AOI 2',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_warehouse'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'start_date',
			'label' 		=> 'Arrival Date From (00:00)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->subDays(30)->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes'	=> [
				'id' 			=> 'start_date',
				'autocomplete' 	=> 'off',
				'readonly' 		=> 'readonly'
			]
		])
		
		@include('form.date', [
			'field' 		=> 'end_date',
			'label' 		=> 'Arrival Date To (23:59)',
			'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
			'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
			'default'		=> \Carbon\Carbon::now()->format('d/m/Y'),
			'placeholder' 	=> 'dd/mm/yyyy',
			'class' 		=> 'daterange-single',
			'attributes' 	=> [
				'id' 			=> 'end_date',
				'readonly' 		=> 'readonly',
				'autocomplete' 	=> 'off'
			]
		])

		{!!
			Form::open(array(
					'class' => 'heading-form',
					'role' => 'form',
					'url' => route('fabricReportDailyMaterialOutNonCutting.export'),
					'method' => 'get',
					'target' => '_blank'		
			))
		!!}
		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

	<div class="navbar navbar-default navbar-xs navbar-component no-border-radius-top">
		<ul class="nav navbar-nav visible-xs-block">
			<li class="full-width text-center"><a data-toggle="collapse" data-target="#navbar-filter"><i class="icon-menu7"></i></a></li>
		</ul>

		<div class="navbar-collapse collapse" id="navbar-filter">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#handover" data-toggle="tab" onclick="changeTab('handover')">Handover <i class="icon-stack3 position-left"></i></a></li>
				<li><a href="#moveorder" data-toggle="tab" onclick="changeTab('moveorder')"><i class="icon-file-check position-left"></i> Move Order <i class="icon-stack2 position-left"></i></a></li>
			</ul>
		</div>
	</div>

<div class="tabbable">
	<div class="tab-content">
	<div class="tab-pane fade in active" id="handover">
		<div class="panel panel-default border-grey">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table datatable-basic table-striped table-hover" id="daily_material_handover_table">
						<thead>
							<tr>
								<th>ID</th>
								<th>Created Date</th>
								<th>Complete Date</th>
								<th>Warehouse Location</th>
								<th>No PT</th>
								<th>Po Supplier</th>
								<th>Supplier Name</th>
								<th>Item Code</th>
								<th>Color</th>
								<th>Uom</th>
								<th>Total Handover</th>
								<th>Total Outstanding</th>
								<th>Total Supplied</th>
								<th>Action</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="tab-pane fade" id="moveorder">
				<div class="panel panel-default border-grey">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table datatable-basic table-striped table-hover table-responsive" id="daily_material_move_order_table">
								<thead>
									<th>ID</th>
									<th>Created Date</th>
									<th>Complete Date</th>
									<th>Warehouse Location</th>
									<th>Destination</th>
									<th>Document No Out</th>
									<th>Po Supplier</th>
									<th>Supplier Name</th>
									<th>Item Code</th>
									<th>Color</th>
									<th>Uom</th>
									<th>Total Handover</th>
									<th>Total Outstanding</th>
									<th>Total Supplied</th>
									<th>Action</th>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{!! Form::hidden('active_tab',$active_tab , array('id' => 'active_tab')) !!}
{!! Form::hidden('page','index', array('id' => 'page')) !!}
@endsection


@section('page-modal')
	@include('fabric_report_daily_material_out_non_cutting._edit_modal')
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_daily_material_out_non_cutting.js') }}"></script>
@endsection
