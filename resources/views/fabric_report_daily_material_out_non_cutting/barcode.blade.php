
<!DOCTYPE html>
<html>
<head>
    <title>BARCODE :: REPRINT BARCODE HANDOVER </title>
	<link rel="stylesheet" type="text/css" href="{{ asset(elixir('css/print_barcode_fabric.css')) }}">
</head>
    <body>
		@foreach ($material_stocks as $key_1 => $material_stock)
			<div class="outer">
				<div class="title">
					<span style="@if(strlen($material_stock->document_no)>50) font-size: 10px @elseif(strlen($material_stock->document_no)>30 && strlen($material_stock->document_no)<50) font-size: 12px @else font-size: 25px @endif">{{ $material_stock->document_no }}</span>
				</div>
				<div class="isi">
					<div class="isi1">
						<span style="@if(strlen($material_stock->item_code)>=50) font-size: 12px @elseif(strlen($material_stock->item_code)>20 && strlen($material_stock->item_code)<50) font-size: 20px @else font-size: 30px @endif">{{ $material_stock->item_code }}</span>
					</div>
					<div class="isi1" style="
						@if(strlen($material_stock->color)>100 && strlen($material_stock->color)<150) font-size: 12px;line-height:0.3cm;
						@elseif(strlen($material_stock->color)>150 && strlen($material_stock->color)<175) height:40px; 
						@elseif(strlen($material_stock->color)>175 && strlen($material_stock->color)<200) height:75px  
						@endif">
						<span style="@if(strlen($material_stock->color)>15) font-size: 10px @else font-size: 30px @endif">{{ $material_stock->color }}</span>
					</div>
					<div class="isi1">
						<span style="@if(strlen($material_stock->batch_number)>15) font-size: 25px @else font-size: 30px @endif"> {{ $material_stock->batch_number }}</span>
					</div>
					
					<div class="isi2">
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->supplier_name }}  </span>
						</div>
						<div class="block-kiri" style="justify-content: center; height: 45px;">
							<span style="font-size: 20px;word-wrap: break-word;">{{ number_format($material_stock->qty_handover, 2, '.', ',') }} {{ strtoupper($material_stock->uom) }}   </span>
						</div>
						<p style="font-size: 15px;word-wrap: break-word;">NO ROLL : <br/><span style="
							@if(strlen($material_stock->nomor_roll)>=10 && strlen($material_stock->nomor_roll)<15) font-size: 25px; 
							@elseif(strlen($material_stock->nomor_roll)>=4 && strlen($material_stock->nomor_roll)<10) font-size: 35px 
							@else font-size: 70px @endif">{{ $material_stock->nomor_roll }}
							</p>
					
					</div>
					<div class="isi3">
						<div class="block-kiri" style="height: 150px;">
							<img style="height: 2.5cm; width: 4.3cm; margin-top: 35px; margin-left: 2px;" src="data:image/png;base64,{{ DNS1D::getBarcodePNG("$material_stock->barcode", 'C128') }}" alt="barcode"   />			
							<br/><span style="font-size: 12px;word-wrap: break-word;">{{ $material_stock->barcode }}</span>
				
						</div>
						<div class="block-kiri">
							No. Invoice {{ $material_stock->no_invoice }}
						</div>
						<div class="block-kiri">
							Tgl. Receive {{ $material_stock->materialStock->created_at->format('d/M/Y H:i:s') }}</br>
							Tgl. Handover {{ $material_stock->created_at->format('d/M/Y H:i:s') }}
						</div>
					</div>
				</div>
						
			</div>
			<br/>
            
		@endforeach
		
    </body>
</html>