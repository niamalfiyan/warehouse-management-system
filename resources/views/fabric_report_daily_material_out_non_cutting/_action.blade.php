
<ul class="icons-list">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-menu9"></i>
        </a>
        <ul class="dropdown-menu dropdown-menu-right">
            @if (isset($detail))
                <li><a href="{!! $detail !!}"><i class="icon-search4"></i> Detail</a></li>
            @endif
            @if (isset($edit))
                <li><a href="#" onclick="edit('{!! $edit !!}')" ><i class="icon-pencil6"></i> Edit</a></li>
            @endif
            @if (isset($destroy))
                <li><a href="#" onclick="destroy('{!! $destroy !!}')"><i class="icon-x"></i> Delete</a></li>
            @endif
            @if (isset($print))
                <li><a href="{!! $print !!}" target="_blank"><i class="icon-printer2"></i> Print</a></li>
            @endif
        </ul>
    </li>
</ul>
