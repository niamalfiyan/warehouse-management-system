@extends('layouts.app', ['active' => 'master_data_po_buyer'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Master Data Po Buyer</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Master Data</li>
				<li><a href="{{ route('masterDataPoBuyer.index') }}">Po Buyer</a></li>
				<li class="active">Cancel</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
	<div class="panel panel-default border-grey">
		<div class="panel-heading">
			<h6 class="panel-title"> &nbsp <a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
			<div class="heading-elements">
				<div class="heading-btn">
					<a href="{{route('masterDataPoBuyer.exportCancelForm')}}" class="btn btn-primary" data-popup="tooltip" title="download form import" data-placement="bottom" data-original-title="download form import"><i class="icon-download"></i></a>
					<button id="upload_button" class="btn btn-success" data-popup="tooltip" title="upload form import" data-placement="bottom" data-original-title="upload form import"><i class="icon-upload"></i></button>
					{!!
						Form::open([
							'role' => 'form',
							'url' => route('masterDataPoBuyer.importCancelForm'),
							'method' => 'post',
							'id' => 'upload_file_po_buyer',
							'enctype' => 'multipart/form-data'
						])
					!!}
						<input type="file" class="hidden" id="upload_file" name="upload_file" accept=".csv, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
						<button type="submit" class="btn btn-success hidden btn-lg" id="submit_button" >Submit</button>
					{!! Form::close() !!}
					
				</div>
			</div>
		</div>
		<div class="panel-body">
			<div class="table-responsive">
				<table class="table table-striped table-hover ">
					<thead>
						<tr>
							<td>Po Buyer</td>
							<td>Reason</td>
							<td>Status</td>
						</tr>
					</thead>
					<tbody id="tbody-upload-po-buyer"></tbody>
				</table>
			</div>
		</div>
	</div>

	{!! Form::hidden('po-buyer','[]',array('id' => 'po-buyer')) !!}
	{!! Form::hidden('page','cancel', array('id' => 'page')) !!}
@endsection

@section('page-js')
	@include('master_data_po_buyer._item')
	<script src="{{ mix('js/master_data_po_buyer.js') }}"></script>
@endsection
