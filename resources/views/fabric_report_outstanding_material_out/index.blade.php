@extends('layouts.app', ['active' => 'fabric_report_outstanding_material_out'])

@section('page-header')
	<div class="page-header">
		<div class="page-header-content">
			<div class="page-title">
				<h4><i class="icon-grid5 position-left"></i> <span class="text-semibold">Report Material Outstanding Out</span></h4>
			</div>
		</div>
		<div class="breadcrumb-line breadcrumb-line-component"><a class="breadcrumb-elements-toggle"><i class="icon-menu-open"></i></a>
			<ul class="breadcrumb">
				<li><a href="{{ route('dashboard') }}"><i class="icon-home2 position-left"></i> Home</a></li>
				<li>Report</li>
				<li class="active">Report Material Outstanding Out</li>
			</ul>
		</div>
	</div>
@endsection

@section('page-content')
<div class="panel panel-default border-grey">
	<div class="panel-heading">
		<h5 class="panel-title">Filter<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
				<li><a data-action="collapse"></a></li>
			</ul>
		</div>
	</div>
	<div class="panel-body">
	{!!
		Form::open(array(
			'class'  => 'form-horizontal',
			'role'   => 'form',
			'url'    => route('fabricReportOutstandingMaterialOut.export'),
			'method' => 'get',
			'target' => '_blank'
		))
	!!}
	@include('form.date', [
				'field' 		=> 'planning_date',
				'label' 		=> 'Planning Date Dari',
				'label_col' 	=> 'col-md-2 col-lg-2 col-sm-12',
				'form_col' 		=> 'col-md-10 col-lg-10 col-sm-12',
				'default'		=> \Carbon\Carbon::now()->subDays(7)->format('d/m/Y'),
				'placeholder' 	=> 'dd/mm/yyyy',
				'class' 		=> 'daterange-single',
				'attributes' 	=> [
					'id' 			=> 'planning_date',
					'readonly' 		=> 'readonly',
					'autocomplete' 	=> 'off'
				]
			])

	@include('form.select', [
			'field' => 'is_additional',
			'label' => 'Type',
			'label_col' => 'col-md-2 col-lg-2 col-sm-12',
			'form_col' => 'col-md-10 col-lg-10 col-sm-12',
			'options' => [
				'reguler' => 'REGULER',
				'additional' => 'ADDITIONAL',
			],
			'class' => 'select-search',
			'attributes' => [
				'id' => 'select_type'
			]
		])


		<button type="submit" class="btn btn-default col-xs-12">Export All <i class="icon-file-excel position-left"></i></button>
	{!! Form::close() !!}
	</div>
</div>

<div class="panel panel-default border-grey">
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table datatable-basic table-striped table-hover table-responsive" id="fabric_report_outstanding_material_out">
				<thead>
					<tr>
						<th>Planning Date</th>
						<th>Warehouse</th>
                        <th>Is Additional</th>
                        <th>Style</th>
						<th>Article</th>
						<th>Po Supplier</th>
						<th>Barcode</th>
						<th>Item Code Source</th>
                        <th>Item Code Book</th>
						<th>Uom</th>
                        <th>Qty Prepare</th>
						<th>Preparation Date</th>
					</tr>
				</thead>
				<tbody>
            	</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('page-js')
	<script src="{{ mix('js/fabric_report_outstanding_material_out.js') }}"></script>
@endsection
