$('#po_buyerName').on('click',function(){
  buyerPickList('po_buyer','item_code', '/accessories/barcode/bill-of-material/create?');
});

$('#po_buyerButtonLookup').on('click',function(){
  buyerPickList('po_buyer','item_code', '/accessories/barcode/bill-of-material/create?');
});

$('#po_buyerAdditionalName').on('click',function(){
  buyerPickListAdditional('po_buyerAdditional','item_codeAdditional', '/accessories/barcode/bill-of-material/create?additional=true');
});

$('#po_buyerAdditionalButtonLookup').on('click',function(){
  buyerPickListAdditional('po_buyerAdditional','item_codeAdditional', '/accessories/barcode/bill-of-material/create?additional=true');
});

$(function()
{
  $('#form').submit(function (event) 
  {
    event.preventDefault();
    bootbox.confirm("Are you sure want to print this data ?.", function (result) {
      if (result) 
      {
        $.ajax({
          type: "POST",
          url: $('#form').attr('action'),
          data: $('#form').serialize(),
          beforeSend: function () {
            $.blockUI({
              message: '<i class="icon-spinner4 spinner"></i>',
              overlayCSS: {
                  backgroundColor: '#fff',
                  opacity: 0.8,
                  cursor: 'wait'
              },
              css: {
                  border: 0,
                  padding: 0,
                  backgroundColor: 'transparent'
              }
          });
          },
          complete: function () {
            $.unblockUI();
          },
          success: function (response) {
            $("#alert_success").trigger("click", 'Data successfully print.');
          },
          error: function (response) {
            $.unblockUI();
            if (response['status'] == 500 ) $("#alert_error").trigger("click", 'Please contact ICT');
            else $("#alert_warning").trigger("click", response)
            
          }
        })
        .done(function (response) 
        {
            resetForm();
            $('#list_barcodes').val(JSON.stringify(response));
            $('#getBarcode').submit();
            $('#list_barcodes').val('');
            $('#form').trigger('reset');
        });
      }
    });
  });
});


function buyerPickList(name,name2, url) 
{
  var search    = '#' + name + 'Search';
  var search2   = '#' + name2 + 'Search2';
  var list      = '#' + name + 'List';
  var modal     = '#' + name + 'Modal';
  var table     = '#' + name + 'Table';
  var buttonSrc = '#ButtonSrc';

  $(search).focus();

  function itemAjax() {
    var q = $(search).val();
    var q2 = $(search2).val();
    $(table).addClass('hidden');
    $(modal).find('.shade-screen').removeClass('hidden');
    $(modal).find('.form-search').addClass('hidden');

    $.ajax({
      url: url + '&po_buyer=' + q+'&item_code=' + q2

    })
    .done(function (data) {
      $(table).html(data);
      pagination(name);

      $(table).removeClass('hidden');
      $(modal).find('.shade-screen').addClass('hidden');
      $(modal).find('.form-search').removeClass('hidden');

      $(table).find('.btn-choose').on('click', chooseItem);
    });
  }

  function pagination() 
  {
    $(modal).find('.pagination a').on('click', function (e) {
      var params = $(this).attr('href').split('?')[1];
      url = $(this).attr('href') + (params == undefined ? '?' : '');

      e.preventDefault();
      itemAjax();
    });
  }

  function chooseItem()
  {
    var buyer = $(this).data('buyer');
    var item = $(this).data('item');
    var desc = $(this).data('desc');
    var style = $(this).data('style');
    var article_no = $(this).data('article');
    var need = $(this).data('need');
    var uom = $(this).data('uom');
    var job = $(this).data('job');
    var category = $(this).data('category');

    $('#msg').addClass('hidden');
    $('#remark').removeClass('hidden');
    $('#po_buyer').removeClass('hidden');
    $('#item_code').removeClass('hidden');
    $('#style').removeClass('hidden');
    $('#article').removeClass('hidden');
    $('#uom').removeClass('hidden');
    $('#qty').removeClass('hidden');

    $('#_po_buyer').html(buyer);
    $('#_item_code').html(item);
    $('#_style').html(style);
    $('#_article').html(article_no);
    $('#_uom').html(uom);
    $('#_qty').html(need);

    $('#__item_code').val(item);
    $('#__item_desc').val(desc);
    $('#__po_buyer').val(buyer);
    $('#__style').val(style);
    $('#___article').val(article_no);
    $('#__uom').val(uom);
    $('#__qty_required').val(need);
    $('#___job_order').val(job);
    $('#__category').val(category);
  }

  $(buttonSrc).unbind();
  $(search).unbind();
  $(search2).unbind();

  $(buttonSrc).on('click', itemAjax);

  $(search).on('keypress', function (e) {
    if (e.keyCode == 13)
      itemAjax();
  });

  $(search2).on('keypress', function (e) {
    if (e.keyCode == 13)
      itemAjax();
  });

  itemAjax();
}

function buyerPickListAdditional(name,name2, url) 
{
  var search    = '#' + name + 'Search';
  var search2   = '#' + name2 + 'Search2';
  var list      = '#' + name + 'List';
  var modal     = '#' + name + 'Modal';
  var table     = '#' + name + 'Table';
  var buttonSrc = '#ButtonSrc';
  
  $(search).focus();

  function itemAjax() {
    var q = $(search).val();
    var q2 = $(search2).val();
    $(table).addClass('hidden');
    $(modal).find('.shade-screen').removeClass('hidden');
    $(modal).find('.form-search').addClass('hidden');


    $.ajax({
      url: url + '&po_buyer=' + q+'&item_code=' + q2

    })
    .done(function (data) {
      $(table).html(data);
      pagination(name);

      $(table).removeClass('hidden');
      $(modal).find('.shade-screen').addClass('hidden');
      $(modal).find('.form-search').removeClass('hidden');

      $(table).find('.btn-choose').on('click', chooseItem);
    });
  }

  function pagination() {
    $(modal).find('.pagination a').on('click', function (e) {
      var params = $(this).attr('href').split('?')[1];
      url = $(this).attr('href') + (params == undefined ? '?' : '');

      e.preventDefault();
      itemAjax();
    });
  }

  function chooseItem()
  {
    var buyer = $(this).data('buyer');
    var item = $(this).data('item');
    var desc = $(this).data('desc');
    var style = $(this).data('style');
    var article_no = $(this).data('article');
    var need = $(this).data('need');
    var uom = $(this).data('uom');
    var job = $(this).data('job');
    var category = $(this).data('category');

    $('#item-additional').find('#msg').addClass('hidden');
    $('#item-additional').find('#remark').removeClass('hidden');
    $('#item-additional').find('#po_buyer').removeClass('hidden');
    $('#item-additional').find('#item_code').removeClass('hidden');
    $('#item-additional').find('#style').removeClass('hidden');
    $('#item-additional').find('#article').removeClass('hidden');
    $('#item-additional').find('#uom').removeClass('hidden');
    $('#item-additional').find('#qty').removeClass('hidden');

    $('#item-additional').find('#_po_buyer').html(buyer);
    $('#item-additional').find('#_item_code').html(item);
    $('#item-additional').find('#_style').html(style);
    $('#item-additional').find('#_article').html(article_no);
    $('#item-additional').find('#_uom').html(uom);
    $('#item-additional').find('#_qty').html(need);

    $('#item-additional').find('#__item_code').val(item);
    $('#item-additional').find('#__item_desc').val(desc);
    $('#item-additional').find('#__po_buyer').val(buyer);
    $('#item-additional').find('#__style').val(style);
    $('#item-additional').find('#___article').val(article_no);
    $('#item-additional').find('#__uom').val(uom);
    $('#item-additional').find('#__qty_required').val(0);
    $('#item-additional').find('#___job_order').val(job);
    $('#item-additional').find('#__category').val(category);
    $('#item-additional').find('#__is_additional').val(true);

    $('#item-additional').find('#additional_qty').on('change',function(e){
      $('#item-additional').find('#__qty_required').val($(this).val());
    })
  }

  $(buttonSrc).unbind();
  $(search).unbind();
  $(search2).unbind();

  $(buttonSrc).on('click', itemAjax);

  $(search).on('keypress', function (e) {
    if (e.keyCode == 13)
      itemAjax();
  });

  $(search2).on('keypress', function (e) {
    if (e.keyCode == 13)
      itemAjax();
  });

  itemAjax();
}

function resetForm()
{
  $('.msg').removeClass('hidden');
  $('.remark').addClass('hidden');
  $('.po_buyer').addClass('hidden');
  $('.item_code').addClass('hidden');
  $('.style').addClass('hidden');
  $('.article').addClass('hidden');
  $('.uom').addClass('hidden');
  $('.qty').addClass('hidden');

  $('._po_buyer').html("");
  $('._item_code').html("");
  $('._style').html("");
  $('._article').html("");
  $('._uom').html("");
  $('._qty').html("");

  $('.__item_code').val("");
  $('.__item_desc').val("");
  $('.__po_buyer').val("");
  $('.__style').val("");
  $('.___article').val("");
  $('.__uom').val("");
  $('.__qty_required').val("");
  $('.___job_order').val("");
  $('.__category').val("");
  $('.__is_additional').val("");
}
