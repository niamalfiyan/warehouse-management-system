$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        $('#master_data_po_buyer_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/po-buyer/data'
            },
            columns: [
                {data: 'lc_date', name: 'lc_date',searchable:false,visible:true,orderable:false},
                {data: 'statistical_date', name: 'statistical_date',searchable:false,visible:true,orderable:false},
                {data: 'promise_date', name: 'promise_date',searchable:false,visible:true,orderable:false},
                {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
                {data: 'brand', name: 'brand',searchable:true,visible:true,orderable:true},
                {data: 'type_stock', name: 'type_stock',searchable:true,visible:true,orderable:true},
                {data: 'cancel_date', name: 'cancel_date',searchable:false,visible:true,orderable:false},
                {data: 'cancel_reason', name: 'cancel_reason',searchable:false,visible:true,orderable:false},
                {data: 'cancel_user_id', name: 'cancel_user_id',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_po_buyer_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }else
    {
        list_po_buyer = JSON.parse($('#po-buyer').val());
        render();
    }
});

function cancel(url) 
{
    swal({
        title: "Are you sure want to cancel this data ?",
        text: "Reason:",
        type: "input",
        showCancelButton: true,
        confirmButtonColor: "#2196F3",
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Please type reason here"
    },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Bro / Sist, please type reason first !");
                return false
            }


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "post",
                url: url,
                data: {
                    'cancel_reason': inputValue
                },
                beforeSend: function () {
                    swal.close();
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    if (response['status'] == 422)
                        $("#alert_info_2").trigger("click", response['responseJSON']);
                }
            })
                .done(function ($result) {
                    $("#alert_success").trigger("click", 'Data successfully canceled.');
                    $('#master_data_po_buyer_table').DataTable().ajax.reload();
                });
        });
}

function render() 
{
    $('#po-buyer').val(JSON.stringify(list_po_buyer));

    var tmpl = $('#po-buyer-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_po_buyer };
    var html = Mustache.render(tmpl, data);
    $('#tbody-upload-po-buyer').html(html);
}

function renderpsd() 
{
    $('#po-buyer').val(JSON.stringify(list_po_buyer));

    var tmpl = $('#update-psd-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_po_buyer };
    var html = Mustache.render(tmpl, data);
    $('#tbody-psd-po-buyer').html(html);
}

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});


$('#upload_file').on('change', function () 
{
    $.ajax({
        type: "post",
        url: $('#upload_file_po_buyer').attr('action'),
        data: new FormData(document.getElementById("upload_file_po_buyer")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info_2").trigger("click", 'Upload successfully.');

            for (idx in response) {
                var data = response[idx];
                var input = {
                    'status': data.status,
                    'reason': data.reason,
                    'po_buyer': data.po_buyer,
                };
                list_po_buyer.push(input);
            }

        },
        error: function (response) {
            $.unblockUI();
            $('#upload_file_po_buyer').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

            if (response['status'] == 422)
                $("#alert_error").trigger("click", response.responseJSON);

        }
    })
    .done(function () {
        $('#upload_file_po_buyer').trigger('reset');
        render();
    });

})

$('#upload_button_psd').on('click', function () 
{
    $('#upload_file_psd').trigger('click');
});


$('#upload_file_psd').on('change', function () 
{
    $.ajax({
        type: "post",
        url: $('#upload_psd_po_buyer').attr('action'),
        data: new FormData(document.getElementById("upload_psd_po_buyer")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info_2").trigger("click", 'Upload successfully.');

            for (idx in response) {
                var data = response[idx];
                var input = {
                    'status': data.status,
                    'psd': data.psd,
                    'last_update': data.last_update,
                    'po_buyer': data.po_buyer,
                };
                list_po_buyer.push(input);
            }

        },
        error: function (response) {
            $.unblockUI();
            $('#upload_psd_po_buyer').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

            if (response['status'] == 422)
                $("#alert_error").trigger("click", response.responseJSON);

        }
    })
    .done(function () {
        $('#upload_psd_po_buyer').trigger('reset');
        renderpsd();
    });

})