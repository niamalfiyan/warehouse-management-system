$(function()
{
		
			regulerTables();

			var dtablRegulerTable = $('#out_table').dataTable().api();
			$(".dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtablRegulerTable.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtablRegulerTable.search("").draw();
					}
					return;
			});
			dtablRegulerTable.draw();
			$('#select_warehouse').on('change',function(){
				dtablRegulerTable.draw();
                
			});
			
			$('#start_date').on('change',function(){
				dtablRegulerTable.draw();
			});
			
			$('#end_date').on('change',function(){
				dtablRegulerTable.draw();
			});

		

	
});



function regulerTables()
{
	$('#out_table').DataTable().destroy();
	$('#out_table tbody').empty();
	
	var regulerTable =  $('#out_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
        url: '/accessories/report/report-material-out-subcont/data',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
		},
		
		fnCreatedRow: function (row, data, index) {
			var info = regulerTable.page.info();
			var value = index+1+info.start;
			$('td', row).eq(0).html(value);
		},
        columns: [
			{data: 'barcode', name: 'barcode',searchable:true},
			{data: 'movement_date', name: 'movement_date',searchable:true},
			{data: 'document_no', name: 'document_no',orderable:true},
			{data: 'item_code', name: 'item_code',orderable:true},
            {data: 'po_buyer', name: 'po_buyer',orderable:true},
			{data: 'destination', name: 'destination',orderable:true},
			{data: 'document_no_kk', name: 'document_no_kk',orderable:true},
			{data: 'document_no_out', name: 'document_no_out',orderable:true},
			{data: 'uom', name: 'uom'},
            {data: 'qty', name: 'qty',searchable:false},
     	]
    });
}


$('#export').click(function(){
	var warehouse 	= $('#select_warehouse').val();
	var start_date 	= $('#start_date').val();
	var end_date 	= $('#end_date').val();
	var url			= '/accessories/report/report-material-out-subcont/export';

	window.location.href = url + '?warehouse=' + warehouse + '&start_date=' + start_date + '&end_date=' + end_date;


})