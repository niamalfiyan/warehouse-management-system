$(function()
{
    $('#report_material_allocation_carton_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-allocation-carton/data',
        },
        columns: [
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:false},
            {data: 'brand', name: 'brand',searchable:true,visible:true,orderable:false},
            {data: 'destination', name: 'destination',searchable:true,visible:true,orderable:false},
            {data: 'lc_date', name: 'lc_date',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'qty_required', name: 'qty_required',searchable:true,visible:true,orderable:true},
            {data: 'qty_allocation', name: 'qty_allocation',searchable:true,visible:true,orderable:true},
            {data: 'qty_outstanding', name: 'qty_outstanding',searchable:true,visible:true,orderable:true},
            {data: 'season', name: 'season',searchable:false,visible:true,orderable:false},
            {data: 'psd', name: 'psd',searchable:false,visible:true,orderable:false},
            {data: 'promise_date', name: 'promise_date',searchable:false,visible:true,orderable:false},
            {data: 'batch_code', name: 'batch_code',searchable:true,visible:true,orderable:false},
            {data: 'updated_at', name: 'updated_at',searchable:true,visible:true,orderable:false},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:true,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#report_material_allocation_carton_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });;
});