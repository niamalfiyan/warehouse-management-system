$(function()
{
    lov('locator', '/accessories/report/material-stock/locator-picklist?');
    
    $('#active_tab').on('change',function()
    {
		var active_tab = $('#active_tab').val();
        if(active_tab == 'inactive')
        {
			inactiveDataTables();

			var dtableInactive = $('#inactiveTable').dataTable().api();
			$("#inactiveTable.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableInactive.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableInactive.search("").draw();
					}
					return;
			});
			dtableInactive.draw();
            $('#select_warehouse').on('change',function(){
                dtableInactive.draw();
            });

            $('#select_type_stock').on('change',function(){
                dtableInactive.draw();
            });
			
        }else if( active_tab == 'active')
        {
			activeDataTables();

			var dtableActive = $('#activeTable').dataTable().api();
			$("#activeTable.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableActive.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableActive.search("").draw();
					}
					return;
			});
            dtableActive.draw();
            
            $('#select_warehouse').on('change',function(){
                dtableActive.draw();
            });

            $('#select_type_stock').on('change',function(){
                dtableActive.draw();
            });
		} 
	});

    $('#active_modal_tab').on('change',function()
    {
		var active_tab = $('#active_modal_tab').val();
        if(active_tab == 'stock_origin')
        {
			stockOriginDataTables();

			var dtableStockOrigin = $('#source_stock_table').dataTable().api();
			$("#source_stock_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableStockOrigin.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableStockOrigin.search("").draw();
					}
					return;
			});
			dtableStockOrigin.draw();
            
			
        }else if(active_tab == 'allocation_buyer')
        {
			allocationBuyerDataTables();

			var dtableAllocationBuyer = $('#allocation_buyer_table').dataTable().api();
			$("#allocation_buyer_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableAllocationBuyer.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableAllocationBuyer.search("").draw();
					}
					return;
            });
            dtableAllocationBuyer.draw();
            
        }else if(active_tab == 'allocation_nonbuyer')
        {
            allocationNonBuyerDataTables();

			var dtableAllocationNonBuyer = $('#allocation_non_buyer_table').dataTable().api();
			$("#allocation_non_buyer_table.dataTables_filter input")
				.unbind() // Unbind previous default bindings
				.bind("keyup", function (e) { // Bind our desired behavior
					// If the user pressed ENTER, search
					if (e.keyCode == 13) {
						// Call the API search function
						dtableAllocationNonBuyer.search(this.value).draw();
					}
					// Ensure we clear the search if they backspace far enough
					if (this.value == "") {
						dtableAllocationNonBuyer.search("").draw();
					}
					return;
            });
            dtableAllocationNonBuyer.draw();
        }
    });
    
	var active_tab = $('#active_tab').val();
    if(active_tab == 'inactive')
    {
		inactiveDataTables();

		var dtableInactive = $('#inactiveTable').dataTable().api();
		$("#inactiveTable.dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableInactive.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableInactive.search("").draw();
				}
				return;
		});
        dtableInactive.draw();
        
        $('#select_warehouse').on('change',function(){
            dtableInactive.draw();
        });

        $('#select_type_stock').on('change',function(){
            dtableInactive.draw();
        });

		
    }else if( active_tab == 'active')
    {
		activeDataTables();

		var dtableActive = $('#activeTable').dataTable().api();
		$("#activeTable.dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableActive.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableActive.search("").draw();
				}
				return;
		});
        dtableActive.draw();
        
        $('#select_warehouse').on('change',function(){
            dtableActive.draw();
        });

        $('#select_type_stock').on('change',function(){
            dtableActive.draw();
        });
    } 
    
    
})

$('#select_warehouse').on('change',function()
{
    var value = $(this).val();
    $('#_warehouse_stock_id').val(value);
    $('#_warehouse_allocation_id').val(value);
});

$('#upload_manual_button').on('click', function () {
    $('#upload_manual_file').trigger('click');
});

$('#upload_manual_file').on('change', function () {
    // console.log($('#upload_change_type').val());
    
    $.ajax({
        type: "POST",
        url: $('#upload_change_type').attr('action'),
        data: new FormData(document.getElementById("upload_change_type")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'upload berhasil.');
            // document.location.href = response
            //$('#_po_buyer').val(response).trigger('change');
        },
        error: function (response) {
            $.unblockUI()
            $('#upload_change_type').trigger('reset');
            if (response['status'] == 500)
                $("#alert_error").trigger("click", 'Please Contact ICT.');

        }
    })
    .done(function (response) {
        $('#upload_change_type').trigger('reset');
    });

})

function allocating(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
        .done(function () {
            $("#alert_success").trigger("click", 'Success.');
            $('#activeTable').DataTable().ajax.reload();
        });
}

$('#qty_move').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#delete_qty').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_tranfer').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#new_available_qty_stock').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_change_stock').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#change_source_stock_actual').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#qty_move').change(function()
{
   var qty_move         = parseFloat($(this).val());
   var qty_available    = parseFloat($('#qty_available').val());

   var _temp = qty_available - qty_move;

   if(_temp < 0)
   {
        $("#alert_info").trigger("click", 'Qty move should be less than qty available.');
        $(this).val('');
        return false;
   }
});

$('#delete_qty').change(function()
{
    var qty_delete      = parseFloat($(this).val());
    var qty_available   = parseFloat($('#delete_qty_available').val());
    var _temp           = qty_available - qty_delete;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty delete should be less than qty available');
        $(this).val('');
        return false;
    }
    
});

$('#qty_tranfer').change(function()
{
    var qty_tranfer     = parseFloat($(this).val());
    var qty_available   = parseFloat($('#tranfer_qty_available').val());
    var _temp           = qty_available - qty_tranfer;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty transfer should be less than qty available');
        $(this).val('');
        return false;
    }
});

$('#qty_change_stock').change(function()
{
    var qty_tranfer     = parseFloat($(this).val());
    var qty_available   = parseFloat($('#qty_available_change_stock').val());
    var _temp           = qty_available - qty_tranfer;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty change type stock should be less than qty available');
        $(this).val('');
        return false;
    }
});

$('#change_source_stock_actual').change(function()
{
    var qty_actual     = parseFloat($(this).val());
    var qty_available   = parseFloat($('#change_source_stock_available').val());
    var _temp           = qty_available - qty_actual;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Qty change source stock should be less than qty available');
        $(this).val('');
        return false;
    }
});

$('#changeTypeStockForm').submit(function (event) 
{
    event.preventDefault();

    var qty_move                  = $('#qty_change_stock').val();
    var curr_type_stock           = $('#curr_type_stock').val();
    var mapping_type_stock_option = $('#mapping_type_stock_option').val();

    if(curr_type_stock == mapping_type_stock_option)
    {
        $("#alert_warning").trigger("click", 'Type stock stil same with the previous, nothing changed.');
        return false;
    }

    if(qty_move <= 0)
    {
        $("#alert_warning").trigger("click", 'Please insert qty move first.');
        return false;
    }

    $('#changeTypeStockModal').modal('hide');
    bootbox.confirm("Are you sure want to change this to "+mapping_type_stock_option+" ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#changeTypeStockForm').attr('action'),
                data: $('#changeTypeStockForm').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#changeTypeStockForm').trigger('reset');
                    $('#changeTypeStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#changeTypeStockModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Change Type successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

$('#moveLocatorform').submit(function (event) 
{
    event.preventDefault();
    var qty_move        = $('#qty_move').val();
    var qty_available   = $('#qty_available').val();
    var old_locator_id  = $('#old_locator_id').val();
    var locator_id      = $('#locatorId').val();
    var locator_name    = $('#locatorName').val();

    if(!locator_id)
    {
        $("#alert_warning").trigger("click", 'Please select locator first.');
        return false;
    }

    if(locator_id == old_locator_id)
    {
        $("#alert_warning").trigger("click", 'Destination locator is same with previous. nothing changed.');
        return false;
    }
    

    if(qty_move <= 0)
    {
        $("#alert_warning").trigger("click", 'Please fill qty move first.');
        return false;
    }

   
    $('#moveLocatorModal').modal('hide');
    bootbox.confirm("Are you sure want to move to locator "+locator_name+" ?.", function (result) 
    {
        if (result) 
        {
            $.ajax({
                type: "POST",
                url : $('#moveLocatorform').attr('action'),
                data: $('#moveLocatorform').serialize(),
                beforeSend: function () 
                {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () 
                {
                    $.unblockUI();
                    $('#moveLocatorform').trigger('reset');
                    $('#moveLocatorModal').modal('hide');
                },
                error: function (response) 
                {
                    $.unblockUI();
                    if(response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ict');
                    $('#moveLocatorModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Move locator successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });


   
    
});

$('#updateStockform').submit(function (event) {
    event.preventDefault();

    var new_qty_available = $('#new_available_qty_stock').val();
    var update_reason = $('#update_reason').val();
    
    if(!new_qty_available){
        $("#alert_info").trigger("click", 'silahkan isi qty terlebih dahulu');
        return false;
    }

    if(!update_reason){
        $("#alert_info").trigger("click", 'silahkan isi alasannya dulu bro dan sist');
        return false;
    }

    $('#updateReportStockModal').modal('hide');
    bootbox.confirm("Apakah anda yakin akan mengubah stock ini ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#updateStockform').attr('action'),
                data: $('#updateStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#updateStockform').trigger('reset');
                    $('#updateReportStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#updateReportStockModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Update successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

$('#restoreStockform').submit(function (event) {
    event.preventDefault();

    var new_qty_available = $('#restore_qty').val();
    var update_reason = $('#restore_reason').val();
    
    if(!new_qty_available)
    {
        $("#alert_warning").trigger("click", 'Please insert stock restore first');
        return false;
    }

    if(!update_reason)
    {
        $("#alert_warning").trigger("click", 'Please insert reason first');
        return false;
    }

    $('#restoreReportStockModal').modal('hide');
    bootbox.confirm("Are you sure want to save this data ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#restoreStockform').attr('action'),
                data: $('#restoreStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#restoreStockform').trigger('reset');
                    $('#restoreReportStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#restoreReportStockModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Restore successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

$('#transFerStockForm').submit(function (event) {
    event.preventDefault();

    var qty_tranfer = $('#qty_tranfer').val();
    
    if(!qty_tranfer){
        $("#alert_info").trigger("click", 'silahkan isi qty terlebih dahulu');
        return false;
    }

    $('#transFerStockModal').modal('hide');
    bootbox.confirm("Apakah anda yakin akan mentrasfer stock ini ?.", function (result) {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#transFerStockForm').attr('action'),
                data: $('#transFerStockForm').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#transFerStockForm').trigger('reset');
                    $('#transFerStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                    $('#transFerStockModal').modal();
                }
                
            })
            .done(function(response){
                $("#alert_success").trigger("click", 'Transfer successfully');

                $('#activeTable').DataTable().ajax.reload();

                if (response != 'no_need_print'){
                    var arrStr = encodeURIComponent(JSON.stringify(response));
                    window.open('/report/accessories/material-stock/transfer-stock/print-barcode?id=' + arrStr);
                }
            });
        }
    });
});

$('#deleteStockform').submit(function (event) 
{
    event.preventDefault();

    var delete_qty      = $('#delete_qty').val();
    var delete_reason   = $('#delete_reason').val();
    
    if(delete_qty <= 0)
    {
        $("#alert_info").trigger("click", 'Please fill qty delete first.');
        return false;
    }

    if(!delete_reason)
    {
        $("#alert_warning").trigger("click", 'Please insert reason first.');
        return false;
    }

    $('#deleteStockModal').modal('hide');
    bootbox.confirm("Are you sure want to delete this stock ?.", function (result) 
    {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#deleteStockform').attr('action'),
                data: $('#deleteStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#deleteStockform').trigger('reset');
                    $('#deleteStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ict');
                    $('#deleteStockModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Delete successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});

$('#changeSourceStockform').submit(function (event) 
{
    event.preventDefault();

    var change_source_stock_actual      = $('#change_source_stock_actual').val();
    var change_source_stock_reason      = $('#change_source_stock_reason').val();
    
    if(change_source_stock_actual <= 0)
    {
        $("#alert_info").trigger("click", 'Please fill qty delete first.');
        return false;
    }

    if(!change_source_stock_reason)
    {
        $("#alert_warning").trigger("click", 'Please insert reason first.');
        return false;
    }

    $('#changeSourceReportStockModal').modal('hide');
    bootbox.confirm("Are you sure want to change source stock ?.", function (result) 
    {
        if (result) {
            $.ajax({
                type: "POST",
                url: $('#changeSourceStockform').attr('action'),
                data: $('#changeSourceStockform').serialize(),
                beforeSend: function () {
                    $.blockUI({
                        message: '<i class="icon-spinner4 spinner"></i>',
                        overlayCSS: {
                            backgroundColor: '#fff',
                            opacity: 0.8,
                            cursor: 'wait'
                        },
                        css: {
                            border: 0,
                            padding: 0,
                            backgroundColor: 'transparent'
                        }
                    });
                },
                success: function () {
                    $.unblockUI();
                    $('#changeSourceStockform').trigger('reset');
                    $('#changeSourceReportStockModal').modal('hide');
                },
                error: function (response) {
                    $.unblockUI();
                    if(response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ict');
                    $('#changeSourceReportStockModal').modal();
                }
                
            })
            .done(function()
            {
                $("#alert_success").trigger("click", 'Change Source successfully');
                $('#activeTable').DataTable().ajax.reload();
            });
        }
    });
});


function move(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () 
        {
            $.unblockUI();
        },
        error: function (response) 
        {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
        if(response.available_qty <= 0)
        {
            $("#alert_info").trigger("click", 'This data doesn\'t have qty.');
            return false;
        }

        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#id').val(response.id);
        $('#uom').val(response.uom);
        $('#qty_available').val(response.available_qty);
        $('#locatorId').val(response.locator_id);
        $('#old_locator_id').val(response.locator_id);
        $('#locatorName').val(response.locator_name);
        $('#header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code +' '+response.locator_name+' '+_po_buyer);
        $('#moveLocatorModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function cancel(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "post",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Success.');
        $('#activeTable').DataTable().ajax.reload();
    });
}

function recalculate(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
        .done(function () {
            $("#alert_success").trigger("click", 'Success.');
            $('#activeTable').DataTable().ajax.reload();
        });
}

function unlocked(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            swal.close();
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422)
                $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function () {
        $("#alert_success").trigger("click", 'Success.');
        $('#activeTable').DataTable().ajax.reload();
    });
}

function showDetail(url)
{
    $('#detailModal').modal();
    function itemAjax(){
        $('#detailTable').addClass('hidden');
        $('#detailModal').find('.shade-screen').removeClass('hidden');
    
        $.ajax({
            url: url,
        })
        .done(function (data) {
            $('#detailTable').html(data);
            $('#detailTable').removeClass('hidden');
            $('#detailModal').find('.shade-screen').addClass('hidden');
        });
    }

    itemAjax();
}

function mutation(url)
{
    //$('#update_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
    
    $('#mutation_stock_table').DataTable().destroy();
    $('#mutation_stock_table tbody').empty();

    var mutation_stock_table = $('#mutation_stock_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        scroller:true,
        destroy:true,
        pageLength:-1,
        ajax: {
            type: 'GET',
            url: url,
        },
        fnCreatedRow: function (row, data, index) {
            var info = mutation_stock_table.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;

            // converting to interger to find total
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // computing column Total of the complete result
            var total_db = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            var total_cr = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            //$('#balance').text(total_balance);
            // Update footer by showing the total with the reference of the column index
            $( api.column( 3 ).footer() ).html('Total');
            $( api.column( 4 ).footer() ).html(total_db.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            $( api.column( 5 ).footer() ).html(total_cr.toFixed(4).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            //$( api.column( 5 ).footer() ).html(total_cr);
            

        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'created_at', name: 'created_at',searchable:true, visible:true,orderable:false},
            {data: 'pic', name: 'pic',searchable:true, visible:true,orderable:false},
            {data: 'remark', name: 'remark',searchable:true, visible:true,orderable:false},
            {data: 'qty_db', name: 'qty_db',searchable:false, visible:true,orderable:false},
            {data: 'qty_cr', name: 'qty_cr',searchable:false, visible:true,orderable:false},
        ],
        
    });


    $('#detailModal').modal();
}

function edit(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

            $('#update_stock_id').val('');
            $('#update_uom').val('');
            $('#update_qty_available_stock').val('');
            $('#update_header_stock').text('');
       
            

        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#update_stock_id').val(response.id);
        $('#update_uom').val(response.uom);
        $('#update_qty_available_stock').val(response.available_qty);
        $('#update_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
        $('#updateReportStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function restore(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });

            $('#restore_stock_id').val('');
            $('#restore_uom').val('');
            $('#restore_qty_available_stock').val('');
            $('#restore_header_stock').text('');
       
            

        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#restore_stock_id').val(response.id);
        $('#restore_uom').val(response.uom);
        $('#restore_qty_available_stock').val(response.available_qty);
        $('#restore_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
        $('#restoreReportStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function hapus(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#delete_id').val(response.id);
        $('#delete_uom').val(response.uom);
        $('#delete_qty_available').val(response.available_qty);
        $('#delete_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+response.locator_name+' '+_po_buyer );
        $('#deleteStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function transfer(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) {
        var _supplier_name = (response.supplier_name)? response.supplier_name : '';
        var _po_buyer = (response.po_buyer)? response.po_buyer : '';

        $('#transfer_id').val(response.id);
        $('#transfer_uom').val(response.uom);
        $('#tranfer_qty_available').val(response.available_qty);
        $('#transfer_header_stock').text(_supplier_name+' '+response.document_no+' '+response.item_code+' '+_po_buyer );
        $('#transFerStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function changeType(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
        var update = response;
        
        if(update.available_qty <= 0){
            $("#alert_info_2").trigger("click", 'Type stock cannot because doesnt have qty left.');
            return false;
        }

        var _supplier_name = (update.supplier_name)? update.supplier_name : '';
        var _po_buyer = (update.po_buyer)? update.po_buyer : '';

        $('#curr_type_stock').val(update.type_stock);
        $('#change_type_stock_id').val(update.id);
        $('#uom_change_type_stock').val(update.uom);
        $('#qty_available_change_stock').val(update.available_qty);
        $('#header_change_type_stock').text(_supplier_name+' '+update.document_no+' '+update.item_code+' '+_po_buyer );
        $("#mapping_type_stock_option").val(update.type_stock).trigger('change');

        $('#changeTypeStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

function changeTab(status)
{
	$('#active_tab').val(status).trigger('change');
}

function changeTabModal(status)
{
    $('#active_modal_tab').val(status).trigger('change');
}

function activeDataTables()
{
    $('#activeTable').DataTable().destroy();
    $('#activeTable tbody').empty();

    var activeTable =  $('#activeTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-stock/active',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "type_stock_erp_code" : $('#select_type_stock').val(),
                });
           }
        },
        fnCreatedRow: function (row, data, index) {
            var info = activeTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
            {data: 'po_detail_id', name: 'po_detail_id',searchable:true,visible:false,orderable:false},
            {data: 'supplier_code', name: 'supplier_code',searchable:true, visible:false,orderable:false},
            {data: 'item_code', name: 'item_code',searchable:true,visible:false,orderable:false},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:false,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true, visible:false,orderable:false},
            {data: 'available_qty', name: 'available_qty',searchable:false, visible:false,orderable:false},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,orderable:false,width:'45px'},
            {data: 'supplier', name: 'supplier',searchable:false,orderable:false,width:'45px'},
            {data: 'document_no', name: 'document_no',searchable:true, visible:true,orderable:false},
            {data: 'type_stock', name: 'type_stock',searchable:false,visible:true,orderable:true},
            {data: 'item', name: 'item',searchable:false,orderable:false,width:'45px'},
            {data: 'location', name: 'location',searchable:false,orderable:false},
            {data: 'uom', name: 'uom',searchable:false,orderable:false},
            {data: 'stock', name: 'stock',searchable:false,orderable:false},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:false, visible:true,orderable:true},
            {data: '_available_qty', name: '_available_qty',searchable:false, visible:true,orderable:true},
            {data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
        ]
    });
}

function inactiveDataTables()
{
  	$('#inactiveTable').DataTable().destroy();
    $('#inactiveTable tbody').empty();

    var inactiveTable = $('#inactiveTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-stock/inactive',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "type_stock_erp_code" : $('#select_type_stock').val(),
                });
            },
            
        },
        fnCreatedRow: function (row, data, index) {
            var info = inactiveTable.page.info();
            var value = index+1+info.start;
            $('td', row).eq(0).html(value);
        },
        columns: [
            {data: null, sortable: false, orderable: false, searchable: false},
            {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
            {data: 'po_detail_id', name: 'po_detail_id',searchable:true,visible:false,orderable:false},
            {data: 'supplier_code', name: 'supplier_code',searchable:true, visible:false,orderable:false},
            {data: 'item_code', name: 'item_code',searchable:true,visible:false,orderable:false},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:false,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true, visible:false,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true, visible:false,orderable:false},
            {data: 'available_qty', name: 'available_qty',searchable:false, visible:false,orderable:false},
            {data: 'warehouse_name', name: 'warehouse_name',searchable:false,visible:true,orderable:false},
            {data: 'source', name: 'source',searchable:false,orderable:false,width:'45px'},
            {data: 'supplier', name: 'supplier',searchable:false,orderable:false,width:'45px'},
            {data: 'document_no', name: 'document_no',searchable:true, visible:true,orderable:false},
            {data: 'type_stock', name: 'type_stock',searchable:false,visible:true,orderable:true},
            {data: 'item', name: 'item',searchable:false,orderable:false,width:'45px'},
            {data: 'location', name: 'location',searchable:false,orderable:false},
            {data: 'uom', name: 'uom',searchable:false,orderable:false},
            {data: 'stock', name: 'stock',searchable:false,orderable:false},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:false, visible:true,orderable:true},
            {data: '_available_qty', name: '_available_qty',searchable:false, visible:true,orderable:true},
            {data: 'action', name: 'action',width:'50px',searchable:false,visible:true},
        ]
    });
}

function changeSource(url)
{
    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            if (response['status'] == 422) $("#alert_warning").trigger("click", response['responseJSON']);
        }
    })
    .done(function (response) 
    {
        var update = response;
        
        if(update.available_qty <= 0)
        {
            $("#alert_info").trigger("click", 'Stock doesnt have qty left.');
            return false;
        }

        var _supplier_name = (update.supplier_name)? update.supplier_name : '';
        var _po_buyer = (update.po_buyer)? update.po_buyer : '';

        $('#change_source_stock_id').val(update.id);
        $('#change_source_stock_uom').val(update.uom);
        $('#change_source_stock_available').val(update.available_qty);
        $('#change_source_stock_header_stock').text(_supplier_name+' '+update.document_no+' '+update.item_code+' '+_po_buyer );
        
        $('#changeSourceReportStockModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });

   
}

