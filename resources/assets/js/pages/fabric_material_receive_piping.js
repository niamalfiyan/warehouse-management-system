

$(function()
{
	list_material_receive_piping__fabric = JSON.parse($('#material_receive_piping_fabrics').val());
	$('.barcode_value').focus();
	setFocusToTextBox();
	render();
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render()
{
	getIndex();
	$('#material_receive_piping_fabrics').val(JSON.stringify(list_material_receive_piping__fabric));
	var tmpl = $('#fabric_material_receive_piping_table').html();
	Mustache.parse(tmpl);
	var data = { item: list_material_receive_piping__fabric };
	var html = Mustache.render(tmpl, data);
	$('#fabric_material_receive_piping_tbody').html(html);
	$('.barcode_value').focus();
	bind();
}

function bind()
{
	$('#barcode_value').on('change', tambahItem);
	$('.btn-delete-item').on('click', deleteItem);
}

function deleteItem() 
{
	var i = $(this).data('id');
	list_material_receive_piping__fabric.splice(i, 1);
	render();
}

function getIndex()
{
	for (id in list_material_receive_piping__fabric) 
	{
		list_material_receive_piping__fabric[id]['_id'] = id;
		list_material_receive_piping__fabric[id]['no'] = parseInt(id) + 1;
	}
}

function checkItem(barcode)
{
	var flag = 0;
	for (var i in list_material_receive_piping__fabric) 
	{
		var data = list_material_receive_piping__fabric[i];
		if (data.barcode == barcode.toUpperCase()){
			flag++;
		}
	}
	return flag;
}

function tambahItem()
{
	var warehouse_id 								= $('#select_warehouse').val();
	var barcode 									= $('#barcode_value').val();
	var url_fabric_material_receive_piping_create 	= $('#url_fabric_material_receive_piping_create').val();
	
	var diff = checkItem(barcode);
	if (diff > 0)
	{
		$("#alert_info").trigger("click", 'Barcode already scanned.');
		setFocusToTextBox();
		render();
		return false;
	}
		

	$.ajax({
		type: "GET",
		url: url_fabric_material_receive_piping_create,
		data: {
			barcode			: barcode,
			warehouse_id	: warehouse_id,
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response){
			list_material_receive_piping__fabric.push(response);
			
		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			$('.barcode_value').focus();

			if (response.status == 500) $("#alert_error").trigger("click", 'Please contact ICT');
			if (response.status== 422) $("#alert_warning").trigger("click", response.responseJSON)


		}
	})
	.done(function () {
		render();
		$('#barcode_value').val('');
		$('.barcode_value').focus();

	});;
	

	
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

$('#form').submit(function (event) 
{
	event.preventDefault();
	
	bootbox.confirm("Are you sure wan to receive this item ?.", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#form').attr('action'),
				data: $('#form').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {
					$("#alert_success").trigger("click", 'Data successfully received');
				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500 ) $("#alert_error").trigger("click", 'Please Contact ICT');
					if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

				}
			})
			.done(function () 
			{
				$('#barcode_value').val('');
				$('.barcode_value').focus();
				list_material_receive_piping__fabric = [];
				render();
			});
		}
	});
});