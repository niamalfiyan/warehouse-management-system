$(document).ready( function ()
{ 
    var msg         = $('#message').val();
    var page 		= $('#page').val();
    
    if (msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
    else if (msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');

    if(page == 'index')
    {
        var masterDataAreaLocatorTable = $('#master_data_area_and_locator_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            scrollY:250,
            scroller:true,
            destroy:true,
            deferRender:true,
            bFilter:true,
            pageLength:100,
            ajax: {
                type: 'GET',
				url: '/master-data/area-and-locator/data',
				data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"       : $('#select_warehouse').val()
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = masterDataAreaLocatorTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'name', name: 'name',searchable:true,orderable:true},
				{data: 'erp_id', name: 'erp_id',searchable:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,orderable:false},
            ]
        });

		var dtable = $('#master_data_area_and_locator_table').dataTable().api();
		$("#master_data_area_and_locator_table.dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();

		$('#select_warehouse').on('change',function(){
			dtable.draw();
		});
    }else
    {
        $('#form').submit(function (event){
            event.preventDefault();
            var name            	= $('#name').val();
            var select_warehouse	= $('#select_warehouse').val();
            
            if(!name)
            {
                $("#alert_warning").trigger("click", 'Please insert name first');
                return false;
            }
    
            if(!select_warehouse)
            {
                $("#alert_warning").trigger("click", 'Please select warehouse first');
                return false
            }
    
            bootbox.confirm("Are you sure want to save this data ?.", function (result) {
                if(result){
                    $.ajax({
                        type: "POST",
                        url: $('#form').attr('action'),
                        data:new FormData($("#form")[0]),
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                            $.blockUI({
                                message: '<i class="icon-spinner4 spinner"></i>',
                                overlayCSS: {
                                    backgroundColor: '#fff',
                                    opacity: 0.8,
                                    cursor: 'wait'
                                },
                                css: {
                                    border: 0,
                                    padding: 0,
                                    backgroundColor: 'transparent'
                                }
                            });
                        },
                        complete: function () {
                            $.unblockUI();
                        },
                        success: function () {
                            document.location.href = '/master-data/area-and-locator';
                        },
                        error: function (response) {
                            $.unblockUI();
                            
                            if (response.status == 422) $("#alert_warning").trigger("click",response.responseJSON);
                        }
                    });
                }
            });
        });
    
        area_locators = JSON.parse($('#area_locator').val());
        render();
    
    }
});

$('#submit_button').on('click',function(){
    $('#form').trigger('submit');
});


function hapus(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $.ajax({
        type: "put",
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function () {
            $.unblockUI();
        }
    })
    .done(function () 
    {
        $('#master_data_area_and_locator_table').DataTable().ajax.reload();
        $("#alert_success").trigger("click", 'Data successfully deleted');
    });
}

function render() 
{
    getIndex();
    $('#area_locator').val(JSON.stringify(area_locators));

    var tmpl = $('#area_locator_table').html();
    Mustache.parse(tmpl);
    var data = { item: area_locators };
    var html = Mustache.render(tmpl, data);
    $('#tbody_area_locator').html(html);
    bind();
}

function bind()
{
    $('#AddItemButton').on('click', tambahItem);
    $('.btn-delete-item').on('click', deleteItem);
    $('.checkbox_item').on('click', checkedItem);
    $('.btn-edit-item').on('click', editItem);
    $('.btn-save-item').on('click', saveItem);
    $('.btn-cancel-item').on('click', cancelEdit);

    
    $('.input-new').keypress(function (e) {
        if (e.keyCode == 13) {
            e.preventDefault();
            tambahItem();
        }
    });

    $('.input-edit').keypress(function (e) {
        var i = $(this).data('id');

        if (e.keyCode == 13) {
            e.preventDefault();
            $('#simpan_' + i).click();
        }
    });

    $('.input-number').keypress(function (e) {
        if (e.keyCode > 31 && (e.keyCode < 47 || e.keyCode > 57)) {
            return false;
        }
        return true;
    });
}

function getIndex() 
{
    for (idx in area_locators) 
    {
        area_locators[idx]['_id'] = idx;
        area_locators[idx]['no'] = parseInt(idx) + 1;
    }
}

function checkItem(name,row, index) 
{
    for (var i in area_locators) 
    {
        var rack = area_locators[i];
        if (i == index) continue;
        if (rack.rack == name && rack.row == row ) return false;
    }

    return true;
}

function checkedItem() 
{
    var i = $(this).data('id');
    if ($('#check_'+i).prop('checked') == true) area_locators[i].has_many_po_buyer = true;
    else area_locators[i].has_many_po_buyer = false;
    
    render();
}

function tambahItem() 
{
    var name            = $('#rack').val();
    var row             = $('#row').val();
    var start_column    = $('#start_column').val();
    var to_column       = $('#to_column').val();
    
    if ($('#has_many_po_buyer').prop('checked') == true)  var _has_many_po_buyer =  true;
    else  var _has_many_po_buyer = false;
    

    if (!name || !row || !start_column || !to_column)
    {
        $("#alert_warning").trigger("click", 'Please fill all field');
        return false;
    }
    
    var diff = checkItem(name,row);
    
    if (!diff) 
    {
        $("#alert_warning").trigger("click", 'Data already exists');
        return false;
    }
    
    if (parseInt(to_column) < parseInt(start_column)) 
    {
        $("#alert_warning").trigger("click", 'Column cannot be less than ' + start_column);
        return false;
    }

    var input = 
    {
        'id'                : -1,
        'rack'              : name,
        'row'               : row,
        'start_colum'       : start_column,
        'end_colum'         : to_column,
        'has_many_po_buyer' : _has_many_po_buyer,
    };

    area_locators.push(input);
    render();
}

function editItem()
{
    var i = $(this).data('id');

    //add hidden div
    $('#zcolumn_' + i).addClass('hidden');
    
    $('#edit_' + i).addClass('hidden');
    $('#delete_' + i).addClass('hidden');

    //remove hidden textbox
    $('#ToColumnInput_' + i).removeClass('hidden');
    
    $('#simpan_' + i).removeClass('hidden');
    $('#cancel_' + i).removeClass('hidden');
}

function cancelEdit()
{
    var i = $(this).data('id');
    
    //remove hidden div
    $('#zcolumn_' + i).removeClass('hidden');

    $('#edit_' + i).removeClass('hidden');
    $('#delete_' + i).removeClass('hidden');

    //add hidden textbox
    $('#ToColumnInput_' + i).addClass('hidden');
    
    $('#simpan_' + i).addClass('hidden');
    $('#cancel_' + i).addClass('hidden');
}

function saveItem()
{
    var i                   = $(this).data('id');
    var start_column        = area_locators[i].start_colum;
    var to_column           = $('#ToColumnInput_' + i).val();
    var system_to_column    = $('#SystemToColumnInput_' + i).val();

    if (!to_column) 
    {
        $("#alert_warning").trigger("click", 'Please fill all field.');
        return false;
    }

    if (parseInt(to_column) < parseInt(system_to_column)) 
    {
        $("#alert_warning").trigger("click", 'Column cannot be less than ' + system_to_column);
        return false;
    }

    if (parseInt(to_column) < parseInt(start_column)) 
    {
        $("#alert_warning").trigger("click", 'Column cannot be less than ' + start_column);
        return false;
    }

    area_locators[i].end_colum = to_column;
    render();
}

function deleteItem()
{
    var i = $(this).data('id');

    list_area_locator.splice(i, 1);
    render();
}






