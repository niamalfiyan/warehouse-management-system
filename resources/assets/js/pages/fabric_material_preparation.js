var selected;
list_data_prepare = JSON.parse($('#data_prepare').val());
list_data_planning = JSON.parse($('#data_planning').val());
temporary_data_prepare = JSON.parse($('#temporary_data_prepare').val());

$(function () 
{
	renderPlanning();
	renderPreprare();
	setFocusToTextBox();
});

$('.start_date').datepicker({
	format: "dd/mm/yyyy",
	autoclose: true,
	todayHighlight: true
})
.on("change", function () 
{
	var is_piping 		= $('#is_piping').val();
	var item_id_source 	= $('#__item_id_source').val();
	var planning_date 	= $('#__planning_date').val();
	var start_date 		= $('#start_date').val();
	var c_order_id 		= $('#__c_order_id').val();
	var warehouse_id 	= $('#planning_warehouse_id').val();
	

	if ((is_piping == null || !is_piping) && start_date) 
	{
		$("#alert_warning").trigger("click", 'Please select piping type first');
		$('#start_date').val('');
	}else 
	{
		if (!planning_date) 
		{
			getPlanningPerArticle(item_id_source, is_piping, c_order_id,warehouse_id, this.value);
		}else 
		{
			if (planning_date != this.value) getPlanningPerArticle(item_id_source, is_piping, c_order_id,warehouse_id, this.value);
		}
	}
});

$('#locator_in_fabricName').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-preparation/locator-picklist?');
});

$('#locator_in_fabricButtonLookup').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-preparation/locator-picklist?');
});

$('#qty_preparationButtonEdit').click(function () 
{
	$("#qty_preparation").attr("readonly", false);
	$("#qty_preparation").val('');
	$("#is_edit").val('yes');
});

$('#qty_preparation').change(function () 
{
	var qty_preparation = $(this).val();
	if (!qty_preparation) {
		$("#alert_warning").trigger("click", 'Please insert qty preparation first.');
		return false;
	}

	var qty_relax = $('#qty_relax').val();
	var new_qty_outstanding = parseFloat(qty_preparation) - parseFloat(qty_relax);
	$('#qty_outstanding').val(new_qty_outstanding);

});

$('#qty_preparation').keypress(function (e) 
{

	if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
		return false;
	}
	return true;
});

$('#qty_preparation_add').keypress(function (e) 
{

	if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
		return false;
	}
	return true;
});

$('#select_warehouse').on('change',function(){
	var warehouse_id = $(this).val();
	$('#warehouse_id').val(warehouse_id);	
	$('#planning_warehouse_id').val(warehouse_id);	
	$('#additional_warehouse_id').val(warehouse_id);
	$('#bm_warehouse_id').val(warehouse_id);

	locatorPicklist('locator_in_fabric',warehouse_id, '/fabric/material-preparation/locator-picklist?');

});
$('#select_machine').on('change',function(){
	var machine = $(this).val();
	$('#machine').val(machine);
});

$('#is_piping').change(function () 
{
	$("#qty_relax").val('');
	$("#qty_outstanding").val('');
	$("#qty_preparation").val('');
	$("#start_date").val('');
	$(".start_date").datepicker("setDate", null);
	$("#qty_preparation").attr("readonly", true);
	$("#qty_preparationButtonEdit").attr("disabled", "disabled");
})

$('#FormPlanning').submit(function (event) 
{
	event.preventDefault();

	var qty_preparation = $('#qty_preparation').val();
	var qty_outstanding = $('#qty_outstanding').val();
	// var qty_outstanding = qty_outstanding.toFixed(4);

	if (!qty_preparation) 
	{
		$("#alert_warning").trigger("click", 'Please insert qty preparation first');
		return false;
	}

	if (qty_outstanding < 0 || isNaN(qty_outstanding)) 
	{
		$("#alert_warning").trigger("click", 'Something wrong, please check again qty preparation');
		return false;
	}

	if (qty_outstanding == 0) 
	{
		$("#alert_warning").trigger("click", 'This planning is already prepared');
		return false;
	}

	$.ajax({
			type: "post",
			url: $('#FormPlanning').attr('action'),
			data: $('#FormPlanning').serialize(),
			beforeSend: function () {
				$('#confirmationModal').modal('hide');
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) 
			{
				// console.log(success);
				var roll 	= temporary_data_prepare;
				var input 	= 
				{
					'id'						: response.id,
					'c_bpartner_id'				: response.c_bpartner_id,
					'supplier_name'				: response.supplier_name,
					'document_no'				: response.document_no,
					'item_code'					: response.item_code,
					'item_code_source'			: response.item_code_source,
					'item_id_book'				: response.item_id_book,
					'item_id_source'			: response.item_id_source,
					'c_order_id'				: response.c_order_id,
					'actual_planning_date'		: response.actual_planning_date,
					'planning_date'				: response.planning_date,
					'uom'						: response.uom,
					'article_no'				: response.article_no,
					'total_qty_outstanding'		: response.total_qty_outstanding,
					'_total_qty_outstanding'	: response._total_qty_outstanding,
					'total_qty_rilex'			: response.total_qty_rilex,
					'total_reserved_qty'		: response.total_reserved_qty,
					'warehouse_id'				: response.warehouse_id,
					'actual_length'				: response.actual_length,
					'is_exists'					: response.is_exists,
					'is_from_allocation'		: response.is_from_allocation,
					'details'					: response.details,
					'details_roll'				: response.details_roll,
					'is_selected'				: false,
					'is_additional'				: (response.is_from_additional == 1) ? true : false,
					'is_piping'					: (response.is_piping == 1) ? true : false
				};

				list_data_planning.push(input);
				list_data_prepare.push(roll);
				setSelected(roll.c_order_id, roll.item_id, roll.c_bpartner_id);

				if (roll.actual_length != null) 
				{
					roll.is_actual_not_null = true;
					selectedPlanning(roll.c_order_id,roll.document_no, roll.item_id,roll.item_code, roll.c_bpartner_id, response.planning_date, roll.actual_length, list_data_prepare.length - 1);

				}

				temporary_data_prepare = [];
				$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));
				$('#barcode_value').val('');
				$('#FormPlanning').trigger('reset');
				$(".start_date").datepicker("setDate", null);
				//$(".start_date").datepicker("setDate", new Date());
				setFocusToTextBox();
			},
			error: function (response) 
			{
				$.unblockUI();
				$('#barcode_value').focus();
				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please contact ICT');

				if (response['status'] == 422)
					$("#alert_warning").trigger("click", response.responseJSON);

				//temporary_data_prepare = [];
				//$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));

				$('#confirmationModal').modal({
					backdrop: 'static',
					keyboard: false // to prevent closing with Esc button (if you want this too)
				});
			}
	})
	.done(function () 
	{
		$('#barcode_value').val('');
		$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));
		renderPlanning();
		renderPreprare();
		setFocusToTextBox();
	});

});

$('#FormPlanningAdd').submit(function (event) 
{
	event.preventDefault();

	var qty_preparation_additional = $('#qty_preparation_additional').val();
	var qty_outstanding_additional = $('#qty_outstanding_additional').val();
	//var additional_note 	= $('#additional_note').val();

	if (!qty_preparation_additional) 
	{
		$("#alert_warning").trigger("click", 'Please insert qty preparation first');
		return false;
	}

	// if (!additional_note) 
	// {
	// 	$("#alert_warning").trigger("click", 'Please insert additional note first');
	// 	return false;
	// }

	$.ajax({
			type: "post",
			url: $('#FormPlanningAdd').attr('action'),
			data: $('#FormPlanningAdd').serialize(),
			beforeSend: function () {
				$('#confirmationModal').modal('hide');
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) 
			{
				var roll = temporary_data_prepare;
				var input =
				 {
					'id'						: response.id,
					'c_bpartner_id'				: response.c_bpartner_id,
					'supplier_name'				: response.supplier_name,
					'document_no'				: response.document_no,
					'c_order_id'				: response.c_order_id,
					'item_code'					: response.item_code,
					'item_code_source'			: response.item_code_source,
					'item_id_book'				: response.item_id_book,
					'item_id_source'			: response.item_id_source,
					'actual_planning_date'		: response.actual_planning_date,
					'planning_date'				: response.planning_date,
					'additional_note'			: response.additional_note,
					'uom'						: response.uom,
					'total_qty_outstanding'		: response.total_qty_outstanding,
					'_total_qty_outstanding'	: response._total_qty_outstanding,
					'total_qty_rilex'			: response.total_qty_rilex,
					'total_reserved_qty'		: response.total_reserved_qty,
					'warehouse_id'				: response.warehouse_id,
					'actual_length'				: response.actual_length,
					'is_exists'					: response.is_exists,
					'is_from_allocation'		: response.is_from_allocation,
					'details'					: response.details,
					'details_roll'				: response.details_roll,
					'is_selected'				: false,
					'is_additional'				: true,
					'is_piping'					: (response.is_piping == 1) ? true : false
				};

				list_data_planning.push(input);
				list_data_prepare.push(roll);
				setSelected(roll.c_order_id, roll.item_id, roll.c_bpartner_id);

				if (roll.actual_length != null) 
				{
					roll.is_actual_not_null = true;
					selectedPlanning(roll.c_order_id, roll.document_no,roll.item_id,roll.item_code, roll.c_bpartner_id, response.planning_date, roll.actual_length, list_data_prepare.length - 1);

				}

				temporary_data_prepare = [];
				$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));
				$('#barcode_value').val('');
				$('#select_booking_number').empty();
				$("#select_booking_number").append('<option value="">-- Select Booking Number --</option>');
				$('#FormPlanningAdd').trigger('reset');
				setFocusToTextBox();
			},
			error: function (response) 
			{
				$.unblockUI();
				$('#barcode_value').focus();
				$('#barcode_value').val('');

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

				//temporary_data_prepare = [];
				//$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));

				$('#confirmationModal').modal({
					backdrop: 'static',
					keyboard: false // to prevent closing with Esc button (if you want this too)
				});
			}
		})
		.done(function () 
		{
			$('#barcode_value').val('');
			$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));
			renderPlanning();
			renderPreprare();
			setFocusToTextBox();
		});

});

$('#FormPlanningBalanceMarker').submit(function (event) 
{
	event.preventDefault();

	//alert('jalan');

	var qty_preparation = $('#qty_preparation_balance_marker').val();
	var qty_outstanding = $('#qty_outstanding_balance_marker').val();
	//var qty_outstanding = qty_outstanding.toFixed(4);

	if (!qty_preparation) 
	{
		$("#alert_warning").trigger("click", 'Please insert qty preparation first');
		return false;
	}

	if (qty_outstanding < 0 || isNaN(qty_outstanding)) 
	{
		$("#alert_warning").trigger("click", 'Something wrong, please check again qty preparation');
		return false;
	}

	if (qty_outstanding == 0) 
	{
		$("#alert_warning").trigger("click", 'This planning is already prepared');
		return false;
	}

	$.ajax({
			type: "post",
			url: $('#FormPlanningBalanceMarker').attr('action'),
			data: $('#FormPlanningBalanceMarker').serialize(),
			beforeSend: function () {
				$('#confirmationModal').modal('hide');
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) 
			{
				//console.log(success);
				var roll 	= temporary_data_prepare;
				var input 	= 
				{
					'id'						: response.id,
					'c_bpartner_id'				: response.c_bpartner_id,
					'supplier_name'				: response.supplier_name,
					'document_no'				: response.document_no,
					'item_code'					: response.item_code,
					'item_code_source'			: response.item_code_source,
					'item_id_book'				: response.item_id_book,
					'item_id_source'			: response.item_id_source,
					'c_order_id'				: response.c_order_id,
					'actual_planning_date'		: response.actual_planning_date,
					'planning_date'				: response.planning_date,
					'uom'						: response.uom,
					'article_no'				: response.article_no,
					'total_qty_outstanding'		: response.total_qty_outstanding,
					'_total_qty_outstanding'	: response._total_qty_outstanding,
					'total_qty_rilex'			: response.total_qty_rilex,
					'total_reserved_qty'		: response.total_reserved_qty,
					'warehouse_id'				: response.warehouse_id,
					'actual_length'				: response.actual_length,
					'is_exists'					: response.is_exists,
					'is_from_allocation'		: response.is_from_allocation,
					'details'					: response.details,
					'details_roll'				: response.details_roll,
					'is_selected'				: false,
					'is_additional'				: (response.is_from_additional == 1) ? true : false,
					'is_piping'					: (response.is_piping == 1) ? true : false
				};

				list_data_planning.push(input);
				list_data_prepare.push(roll);
				setSelected(roll.c_order_id, roll.item_id, roll.c_bpartner_id);

				if (roll.actual_length != null) 
				{
					roll.is_actual_not_null = true;
					selectedPlanning(roll.c_order_id,roll.document_no, roll.item_id,roll.item_code, roll.c_bpartner_id, response.planning_date, roll.actual_length, list_data_prepare.length - 1);

				}

				temporary_data_prepare = [];
				$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));
				$('#barcode_value').val('');
				$('#FormPlanning').trigger('reset');
				$(".start_date").datepicker("setDate", null);
				//$(".start_date").datepicker("setDate", new Date());
				setFocusToTextBox();
			},
			error: function (response) 
			{
				$.unblockUI();
				$('#barcode_value').focus();
				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please contact ICT');

				if (response['status'] == 422)
					$("#alert_warning").trigger("click", response.responseJSON);

				//temporary_data_prepare = [];
				//$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));

				$('#confirmationModal').modal({
					backdrop: 'static',
					keyboard: false // to prevent closing with Esc button (if you want this too)
				});
			}
	})
	.done(function () 
	{
		$('#barcode_value').val('');
		$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));
		renderPlanning();
		renderPreprare();
		setFocusToTextBox();
	});

});

$('#form').submit(function (event) 
{
	event.preventDefault();

	var machine = $('#machine').val();

	if (!machine) 
	{
		$("#alert_warning").trigger("click", 'Please select machine');
		return false;
	}

	var check_input = checkInput();
	if (check_input > 0) {
		$("#alert_warning").trigger("click", 'Some roll doenst have actual width or actual yds. Please check again');
		return false;
	}
	bootbox.confirm("Are you sure want to save this data ?.", function (result) {
		if (result) 
		{
			$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#form').trigger("reset");
						list_data_planning = [];
						list_data_prepare = [];
						$(".modal-backdrop").remove();
					},
					error: function (response) {
						$.unblockUI();
						$('#barcode_value').val('');
						// $('.barcode_value').focus();
						$(".modal-backdrop").remove();

						if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
						if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

					}
			})
			.done(function (response) {
				$("#alert_success").trigger("click", 'Data successfully saved.');
				$('#selected_div').addClass('hidden');
				if (response != 'no_need_print') {
					$('#list_barcodes').val(JSON.stringify(response));
					$('#getBarcode').submit();
					$('#list_barcodes').val('');
				}

				renderPlanning();
				renderPreprare();
				setFocusToTextBox();
			});
		}
	});
});

$('#select_article').on('change',function()
{
	var is_piping 			= $('#is_piping').val();
	var warehouse_id 		= $('#planning_warehouse_id').val();
	var value 				= $(this).val();    
	var planning_date 		= $('#__planning_date').val();  
	
	if(value)
    {
		var active_tab = $('#active_tab').val();
		if(active_tab == 'reguler')
		{
        	var url_get_planning_per_style = $('#url_get_planning_per_style').val();
		}
		else if (active_tab == 'additional')
		{
			var url_get_planning_per_style = $('#url_get_planning_per_style_additional').val();
		}
		else
		{
			var url_get_planning_per_style = $('#url_get_planning_per_style_balance_marker').val();
		}

		$.ajax({
			type: "get",
			url: url_get_planning_per_style,
			data: {
				planning_date	: planning_date,
				article_no		: value,
				is_piping		: is_piping,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				var styles 	= response.styles;
				var total 	= response.total;
				console.log(styles);
				if(active_tab == 'reguler')
				{
					$("#select_style").empty();
					$.each(styles,function(id,name){
						$("#select_style").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_style').trigger('change');
				}
				else if (active_tab == 'additional')
				{
					$("#select_style_additional").empty();
					$.each(styles,function(id,name){
						$("#select_style_additional").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_style_additional').trigger('change');
				}
				else
				{
					$("#select_style_balance_marker").empty();
					$.each(styles,function(id,name){
						$("#select_style_balance_marker").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_style_balance_marker').trigger('change');
				}
			},
			error: function (response) {
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_style").empty();
					$("#select_style").append('<option value="">-- Select Style --</option>');
				
				}
			}
		});
    }else
    {
        $("#select_style").empty();
        $("#select_style").append('<option value="">-- select Style --</option>');

    }
});

$('#select_style').on('change',function()
{
	var value 				= $(this).val();    
	var article 			= $('#select_article').val();  
	var document_no 		= $('#__document_no').val();  
	var planning_date 		= $('#__planning_date').val();  
	var item_code_source 	= $('#__item_code_source').val();  
	var item_id_source 		= $('#__item_id_source').val();  
	var c_bpartner_id 		= $('#__c_bpartner_id').val();  
	var c_order_id 			= $('#__c_order_id').val();  
	var is_piping 			= $('#is_piping').val();  
	var warehouse_id 		= $('#planning_warehouse_id').val();
	
	if(value)
    {
		active_tab = $('#active_tab').val();
		if(active_tab == 'reguler')
		{
			var url_get_planning_per_date = $('#url_get_planning_per_date').val();
		}
		else if(active_tab == 'additional')
		{
			var url_get_planning_per_date = $('#url_get_planning_per_date_additional').val();
		}
		else
		{
			var url_get_planning_per_date = $('#url_get_planning_per_date_balance_marker').val();
		}
		$.ajax({
			type: "get",
			url: url_get_planning_per_date,
			data: {
				document_no		: document_no,
				item_code		: item_code_source,
				item_id_source	: item_id_source,
				c_bpartner_id	: c_bpartner_id,
				planning_date	: planning_date,
				article_no		: article,
				style			: value,
				c_order_id		: c_order_id,
				is_piping		: is_piping,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				if (response != -1) 
				{
					var material_preparation_fabric_id 	= response.id;
					var qty_outstanding 				= response.total_qty_outstanding;
					var qty_relax 						= response.total_qty_rilex;
					var total_reserved_qty				= response.total_reserved_qty;
					var planning_date					= response.planning_date;
					var warehouse_id					= response.warehouse_id;
					var item_code_book					= response.item_code;
					var item_id_book					= response.item_id_book;

					$('#qty_preparation').val(total_reserved_qty);
					$('#qty_outstanding').val(qty_outstanding);
					$('#qty_relax').val(qty_relax);
					
					$('#__item_code').val(item_code_book);
					$('#__item_id_book').val(item_id_book);
					$('#material_preparation_fabric_id').val(material_preparation_fabric_id);
					$("#qty_preparationButtonEdit").attr("disabled", "disabled");
					//if(planning_date >= '2019-09-03' && warehouse_id == '1000011') $("#qty_preparationButtonEdit").attr("disabled", "disabled");
					//else $("#qty_preparationButtonEdit").removeAttr("disabled");
					
					
					//$("#qty_preparationButtonEdit").removeAttr("disabled");
					$("#qty_preparation").attr("readonly", true);
				} else 
				{
					var warehouse_id = $('#warehouse_id').val();
					//if(warehouse_id == '1000011')
					//{
						$("#qty_preparation").attr("readonly", true);
						$("#qty_preparationButtonEdit").attr("disabled", "disabled");
						$("#alert_warning").trigger("click", 'Allocation not found.');
						return false;
					//}else
					// {
					// 	$('#material_preparation_fabric_id').val('');
					// 	$('#qty_preparation').val('');
					// 	$('#qty_outstanding').val('0');
					// 	$('#qty_relax').val('0');
					// 	$("#qty_preparation").attr("readonly", false);
					// 	$("#qty_preparationButtonEdit").attr("disabled", "disabled");
					// }
				}
			},
			error: function (response) {
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
					$("#alert_warning").trigger("click", response.responseJSON);


			}
		});
    }
});

function locatorPicklist(name,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#' + name + 'ButtonSrc';

	function itemAjax()
	{
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url:  url + '&q=' + q +'&warehouse_id=' + warehouse_id
		})
		.done(function (data) 
		{
			
			$(table).html(data);
			pagination(name);
			$(search).focus();
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem()
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');
		var last_used 	= $(this).data('lastused');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$('#last_locator_used').val(last_used);

		$('#alert_locator').removeClass('hidden');

		if(last_used == 'INV')
		{
			$('#msg_locator').text('Locator only accept barcode material stock');
		}else if(last_used == 'RLX')
		{
			$('#msg_locator').text('Locator only accept barcode material relax');
		}else $('#msg_locator').text('Locator accept either barcode material relax or barcode material stock');
	}

	function pagination()
	{
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	itemAjax();
}

function getPlanningPerArticle( item_id, is_piping, c_order_id,warehouse_id, selected_date) 
{
	if (selected_date) 
	{
		var active_tab = $('#active_tab').val();
		console.log(active_tab);
		if(active_tab == 'reguler') 
		{
			var url_get_planning_per_article = $('#url_get_planning_per_article').val();
		}
		else if(active_tab == 'additional')
		{
			var url_get_planning_per_article = $('#url_get_planning_per_date_additional').val();
		}
		else
		{
			var url_get_planning_per_article = $('#url_get_planning_per_article_balance_marker').val();
		}

		$.ajax({
			type: "get",
			url: url_get_planning_per_article,
			data: {
				item_id			: item_id,
				planning_date	: selected_date,
				is_piping		: is_piping,
				c_order_id		: c_order_id,
				warehouse_id		: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				var articles = response.articles;
				var total = response.total;
				console.log(response);

				if(active_tab == 'reguler') 
				{
					$("#select_article").empty();
					$.each(articles,function(id,name){
					$("#select_article").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_article').trigger('change');
				}
				else if(active_tab == 'additional')
				{
					//console.log(articles);
					$("#select_booking_number").empty();
					$.each(articles,function(id,name){
					$("#select_booking_number").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_booking_number').trigger('change');
				}
				else
				{
					$("#select_article_balance_marker").empty();
					$.each(articles,function(id,name){
					$("#select_article_balance_marker").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_article_balance_marker').trigger('change');
				}
			},
			error: function (response) 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_booking_number").empty();
					$("#select_booking_number").append('<option value="">-- Select No Booking --</option>');
				
				}
					


			}
		});
	}

	$('#__planning_date').val(selected_date);
	$('#__planning_date_bm').val(selected_date);
}

function dismisModal() 
{
	$('#confirmationModal').modal('hide');
	var id 						= temporary_data_prepare.id;
	var url_remove_last_used 	= $('#url_remove_last_used').val();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "put",
		url: url_remove_last_used,
		data: {
			id: id,
		}
	});

	$("#select_article").empty();
	$("#select_article").append('<option value="">-- Select Article --</option>');

	$("#select_style").empty();
	$("#select_style").append('<option value="">-- Select Style --</option>');

	$("#select_style_balance_marker").empty();
	$("#select_style_balance_marker").append('<option value="">-- Select Style --</option>');

	$("#select_article_balance_marker").empty();
	$("#select_article_balance_marker").append('<option value="">-- Select Article --</option>');

	$("#select_booking_number").empty();
	$("#select_booking_number").append('<option value="">-- Select No Booking --</option>');
	
	temporary_data_handover = [];
	$('#FormPlanning').trigger('reset');
	$('#FormPlanningAdd').trigger('reset');
	$("#is_piping").val('');
	$("#start_date").val('');
	$(".start_date").datepicker("setDate", null);
	//$(".start_date").datepicker("setDate", new Date());
	$('#temporary_data_handover').val(JSON.stringify(temporary_data_handover));
	$("#qty_preparation").attr("readonly", true);
	$("#qty_preparationButtonEdit").attr("disabled", "disabled");
	$(".modal-backdrop").remove();
	$('#barcode_value').focus();
}

function renderPlanning() 
{
	getIndexPlanning();
	$('#data_planning').val(JSON.stringify(list_data_planning));
	var tmpl = $('#material-planning-fabric-table').html();
	Mustache.parse(tmpl);
	var data = {
		list: list_data_planning
	};
	var html = Mustache.render(tmpl, data);
	$('#tbody-material-planning-fabric').html(html);
	console.log(data);
	bindPlanning();
}

function renderPreprare() 
{
	getIndexPreprare();
	$('#data_prepare').val(JSON.stringify(list_data_prepare));
	var tmpl = $('#material-preparation-fabric-table').html();
	Mustache.parse(tmpl);
	var data = {
		list: list_data_prepare
	};
	var html = Mustache.render(tmpl, data);
	console.log(data);
	$('#tbody-material-preparation-fabric').html(html);
	bindPrepare();
}

function sortList(array, key) 
{
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	});
}

function getIndexPlanning() 
{
	for (id in list_data_planning) 
	{
		list_data_planning[id]['_id'] 	= id;
		list_data_planning[id]['no'] 	= parseInt(id) + 1;

		for (var idx in list_data_planning[id]['details']) 
		{
			list_data_planning[id]['details'][idx]['_id'] 	= id;
			list_data_planning[id]['details'][idx]['_idx'] 	= idx;
			list_data_planning[id]['details'][idx]['nox'] 	= parseInt(idx) + 1;
		}

		for (var idx in list_data_planning[id]['details_roll']) 
		{
			var nomor_roll 			= list_data_planning[id]['details_roll'][idx]['nomor_roll'];
			var batch_number 		= list_data_planning[id]['details_roll'][idx]['batch_number'];
			var document_no 		= list_data_planning[id]['document_no'];
			var item_code_source	= list_data_planning[id]['item_code_source'];
			var c_bpartner_id 		= list_data_planning[id]['c_bpartner_id'];

			var barcode = setBarcode(document_no, item_code_source, c_bpartner_id, batch_number, nomor_roll);
			list_data_planning[id]['details_roll'][idx]['barcode'] 	= barcode;
			list_data_planning[id]['details_roll'][idx]['_id'] 		= id;
			list_data_planning[id]['details_roll'][idx]['_idx'] 	= idx;
			list_data_planning[id]['details_roll'][idx]['nox'] 		= parseInt(idx) + 1;
		}
	}
}

function setBarcode(document_no, item_code, c_bpartner_id, batch_number, nomor_roll) 
{
	for (idx in list_data_prepare) 
	{
		var data = list_data_prepare[idx];

		if (data.document_no == document_no 
			&& data.item_code == item_code 
			//&& data.c_bpartner_id == c_bpartner_id 
			&& data.batch_number == batch_number 
			&& data.nomor_roll == nomor_roll) 
		{

			var barcode = (data.available_qty <= 0) ? data.barcode : data.new_barcode;
			return barcode;
		}
	}
}

function getIndexPreprare() 
{
	for (idx in list_data_prepare) 
	{
		list_data_prepare[idx]['_id'] = idx;
		list_data_prepare[idx]['no'] = parseInt(idx) + 1;
	}
}

function bindPrepare() 
{
	$('#barcode_value').on('change', tambahItem);
	$('.btn-delete-item').on('click', deleteItem);
	$('.btn-edit-item').on('click', editItem);
	//$('.input-new-actualload').on('change', tambahActualLoad);
	$('.set-zero-begin').on('click', setZeroBegin);
	$('.set-zero-middle').on('click', setZeroMiddle);
	$('.set-zero-end').on('click', setZeroEnd);
	$('.input-new-actualwidth').on('change', tambahActualWidth);
	$('.input-new-actual-length').on('change', tambahActualLength);
	$('.set-zero-begin').keydown(function (e) {
		if (e.keyCode == 9) {
			var i = $(this).data('id');
			$('#middleWidhtInput_' + i).val(null);
		}
	});
	$('.set-zero-middle').keydown(function (e) {
		if (e.keyCode == 9) {
			var i = $(this).data('id');
			$('#endWidhtInput_' + i).val(null);
		}
	});

	$('.input-number').keypress(function (e) 
	{

		if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
			return false;
		}
		return true;
	});
}

function bindPlanning() 
{
	$('.btn-show-roll').on('click', showRoll);
}

function setZeroBegin() 
{
	var i = $(this).data('id');
	$('#beginWidhtInput_' + i).val(null);
}

function setZeroMiddle() 
{
	var i = $(this).data('id');
	$('#middleWidhtInput_' + i).val(null);
}

function setZeroEnd() 
{
	var i = $(this).data('id');
	$('#endWidhtInput_' + i).val(null);
}

function tambahActualWidth() 
{
	var i 				= $(this).data('id');
	var begin_width 	= $('#beginWidhtInput_' + i).val();
	var middle_width 	= $('#middleWidhtInput_' + i).val();
	var end_width 		= $('#endWidhtInput_' + i).val();

	var curr_begin 		= list_data_prepare[i].begin_width;
	var curr_middle 	= list_data_prepare[i].middle_width;
	var curr_end 		= list_data_prepare[i].end_width;

	if (!begin_width) $('#beginWidhtInput_' + i).val(curr_begin);
	else if (!middle_width) $('#middleWidhtInput_' + i).val(curr_middle);
	else if (!end_width) $('#endWidhtInput_' + i).val(curr_end);
	

	if (begin_width && middle_width && end_width) 
	{
		var array 							= [parseFloat(begin_width).toFixed(4), parseFloat(middle_width).toFixed(4), parseFloat(end_width).toFixed(4)];
		var min_value 						= Math.min.apply(Math, array);
		list_data_prepare[i].begin_width 	= parseFloat(begin_width).toFixed(2);
		list_data_prepare[i].middle_width 	= parseFloat(middle_width).toFixed(2);
		list_data_prepare[i].end_width 		= parseFloat(end_width).toFixed(2);
		list_data_prepare[i].actual_width 	= parseFloat(min_value).toFixed(2); //(is_double) ? parseFloat(min_value).toFixed(2) : parseFloat(min_value).toFixed(2);

		renderPreprare();
		setFocusNextInput($(this).attr('id'));
	}
}

function editItem() 
{
	var id 				= $(this).data('id');
	var data 			= list_data_prepare[id];
	var document_no 	= data.document_no;
	var item_code 		= data.item_code;
	var c_bpartner_id 	= data.c_bpartner_id;
	var planning_date 	= data.planning_date;
	var qty_booking 	= parseFloat(data.qty_booking);

	$('#actualLengthInput_' + id).removeAttr('readonly');

	if (qty_booking) 
	{
		for (idx in list_data_planning) 
		{
			
			var data_planning = list_data_planning[idx];
			if (data_planning.document_no == document_no 
				&& data_planning.item_code == item_code 
				//&& data_planning.c_bpartner_id == c_bpartner_id 
				&& data_planning.planning_date == planning_date) 
			{
				var total_qty_outstanding = parseFloat(data_planning.total_qty_outstanding);
				data_planning.total_qty_outstanding = parseFloat(qty_booking + total_qty_outstanding).toFixed(4);
			}
		}

		data.actual_length 		= null;
		data.qty_booking 		= null;
		data.is_actual_not_null = false;

		renderPlanning();
		renderPreprare();
	}

}

function tambahActualLength() 
{
	var i 				= $(this).data('id');
	var actual_length 	= $('#actualLengthInput_' + i).val();

	if (actual_length) 
	{
		var c_order_id 		= list_data_prepare[i].c_order_id;
		var document_no 	= list_data_prepare[i].document_no;
		var item_code 		= list_data_prepare[i].item_code;
		var item_id 		= list_data_prepare[i].item_id;
		var c_bpartner_id 	= list_data_prepare[i].c_bpartner_id;
		var planning_date 	= list_data_prepare[i].planning_date;
		var available_qty 	= list_data_prepare[i]._available_qty;
		//var available_qty = list_data_prepare[i].stock;

		var _actual_length = available_qty;
		if (parseFloat(available_qty) > parseFloat(actual_length)) var _actual_length = actual_length;
		else var _actual_length = available_qty;

		list_data_prepare[i].actual_length = parseFloat(actual_length).toFixed(4);

		selectedPlanning(c_order_id,document_no, item_id,item_code, c_bpartner_id, planning_date, _actual_length, i);
		list_data_prepare[i].is_actual_not_null = true;
	}

	renderPlanning();
	renderPreprare();
	//setFocusToTextBox();
}

function selectedPlanning(c_order_id,document_no,item_id,item_code, c_bpartner_id, planning_date, actual_length, row_roll) 
{
	var available_qty 	= parseFloat(actual_length);
	var qty_booking 	= parseFloat(0);
	var is_complate 	= 0;
	var roll 			= list_data_prepare[row_roll];

	for (idx in list_data_planning) 
	{
		var data = list_data_planning[idx];

		if (data.c_order_id == c_order_id 
			&& data.item_id_source == item_id 
			//&& data.c_bpartner_id == c_bpartner_id 
			&& data.planning_date == planning_date) 
		{
			if (parseFloat(data.total_qty_outstanding) <= 0.00) 
			{
				$("#alert_info").trigger("click", 'kebutuhan untuk item ' + item_code + ' asal dari ' + document_no + ' sudah terpenuhi semua');
				list_data_prepare.splice(row_roll, 1);
				removeLastUsed(roll.roll_id);
				removePlanning(c_order_id, item_id, c_bpartner_id);
				is_complate++;
				break;
			}


			if ((available_qty / parseFloat(data.total_qty_outstanding)) >= 1) var reserved_qty = data.total_qty_outstanding;
			else var reserved_qty = available_qty;

			available_qty 							-= parseFloat(reserved_qty);
			data.total_qty_rilex 					= parseFloat(parseFloat(data.total_qty_rilex) + parseFloat(reserved_qty)).toFixed(4);
			data.total_qty_outstanding 				= parseFloat(parseFloat(data.total_qty_outstanding) - parseFloat(reserved_qty)).toFixed(4);
			qty_booking 							+= parseFloat(reserved_qty);
			roll.material_preparation_fabric_id 	= data.id;
			roll.is_additional 						= data.is_additional;

			if (available_qty == 0) break;

		}

	}

	if (is_complate == 0) 
	{
		var _reserved_qty = parseFloat(list_data_prepare[row_roll]._reserved_qty);
		list_data_prepare[row_roll].reserved_qty 	= parseFloat(_reserved_qty + qty_booking).toFixed(4);
		list_data_prepare[row_roll].qty_booking 	= parseFloat(qty_booking).toFixed(4);
		list_data_prepare[row_roll].available_qty	= parseFloat(available_qty).toFixed(4);
	}


	$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));

}

function removeLastUsed(id) 
{
	var url_remove_last_used = $('#url_remove_last_used').val();

	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "put",
		url: url_remove_last_used,
		data: {
			id: id,
		}
	});
}

function deleteItem() 
{
	var i 								= $(this).data('id');
	var data_prapared 					= list_data_prepare[i];
	var material_preparation_fabric_id 	= data_prapared.material_preparation_fabric_id;
	var roll_id 						= data_prapared.id;
	var c_order_id 						= data_prapared.c_order_id;
	var item_id 						= data_prapared.item_id;
	var c_bpartner_id 					= data_prapared.c_bpartner_id;
	var available_qty 					= parseFloat(data_prapared.qty_booking);

	for (idx in list_data_planning) 
	{
		var data = list_data_planning[idx];
		if (data.id == material_preparation_fabric_id) {

			var total_qty_outstanding 	= parseFloat(data.total_qty_outstanding);
			var total_qty_rilex 		= parseFloat(data.total_qty_rilex);

			data.total_qty_rilex = parseFloat(total_qty_rilex - available_qty).toFixed(4);
			data.total_qty_outstanding = parseFloat(total_qty_outstanding + available_qty).toFixed(4);
		}
	}
	
	removeLastUsed(roll_id);
	list_data_prepare.splice(i, 1);
	removePlanning(c_order_id, item_id, c_bpartner_id);

	renderPlanning();
	renderPreprare();
	setFocusToTextBox();
}

function removePlanning(c_order_id, item_id, c_bpartner_id) 
{
	for (id in list_data_planning) 
	{
		var data = list_data_planning[id];
		
		if (data.c_order_id == c_order_id &&
			//data.c_bpartner_id == c_bpartner_id &&
			data.item_id_source == item_id) {

			var temp = 0;
			for (idx in list_data_prepare) 
			{
				var data_2 = list_data_prepare[idx];
				if (data_2.c_order_id == c_order_id &&
					//data_2.c_bpartner_id == c_bpartner_id &&
					data_2.item_id == item_id) temp++;
			}

			if (temp == 0)
			{
				var url_remove_status_preparation = $('#url_remove_status_preparation').val();
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				$.ajax({
					type: "put",
					url: url_remove_status_preparation,
					data: {
						id: data.id,
					}
				});
				list_data_planning.splice(id, 1);
			}
				
		}
	}
}

function removeDetailRoll(detail_rolls, roll_id) 
{
	for (var i = detail_rolls.length - 1; i >= 0; i--) {
		if (detail_rolls[i]['roll_id'] === roll_id) {
			detail_rolls.splice(i, 1);
		}
	}

}

function checkBarcode(barcode) 
{
	var flag = 0;
	for (idx in list_data_prepare) {
		var data = list_data_prepare[idx];

		if (data.barcode == barcode) {
			flag++;
			break;
		}

	}
	return flag;
}

function checkPlanningExists(document_no, item_id, c_bpartner_id) 
{
	var flag = 0;
	for (var idx in list_data_planning) 
	{
		var data = list_data_planning[idx];

		if (data.document_no == document_no
			 && data.item_id_source == item_id )
			// && data.c_bpartner_id == c_bpartner_id)
			flag++;
	}

	return flag;
}

function getPlanningDate(c_order_id, item_id, c_bpartner_id) 
{
	var planning_date = null;
	for (var idx in list_data_planning) 
	{
		var data = list_data_planning[idx];

		if (data.c_order_id == c_order_id 
			&& data.item_id_source == item_id )
			//&& data.c_bpartner_id == c_bpartner_id)
			planning_date = data.planning_date;
	}

	return planning_date;
}

function showRoll() 
{
	var id 				= $(this).data('id');
	var data 			= list_data_planning[id];
	var c_order_id 		= data.c_order_id;
	var item_id 		= data.item_id;
	var c_bpartner_id 	= data.c_bpartner_id;

	setSelected(c_order_id, item_id, c_bpartner_id);
	$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));
	renderPlanning();
	renderPreprare();
}

function setSelected(c_order_id, item_id, c_bpartner_id) 
{
	// selected planning
	var planning_date = null;
	for (idx in list_data_planning) 
	{
		var data = list_data_planning[idx];
		if (data.c_order_id 		== c_order_id &&
			data.item_id_source 	== item_id)
			//&& data.c_bpartner_id 		== c_bpartner_id
		 
		{
			data.sorting = 0;
			data.is_selected = true;
			planning_date = data.planning_date;

		} else {
			data.sorting = 1;
			data.is_selected = false;
		}
	}

	// selected preparation
	for (idx in list_data_prepare) 
	{
		var data = list_data_prepare[idx];
		if (data.c_order_id 		== c_order_id &&
			data.item_id 			== item_id)
			// &&data.c_bpartner_id 		== c_bpartner_id
		{
			data.sorting = 0;
			data.is_selected = true;
			data.planning_date = planning_date;
		} else {
			data.sorting = 1;
			data.is_selected = false;
		}
	}
}

function tambahItem() 
{
	var warehouse_id 		= $('#select_warehouse').val();
	var barcode 			= $('#barcode_value').val();
	var url_get_roll 		= $('#url_get_roll').val();
	var check_barcode 		= checkBarcode(barcode);

	if (check_barcode > 0) 
	{
		$("#alert_info").trigger("click", 'Barcode already scanned');
		$('#barcode_value').val('');
		setFocusToTextBox();
		return false;
	}

	$.ajax({
		type: "get",
		url: url_get_roll,
		data: {
			barcode 		: barcode,
			warehouse_id 	: warehouse_id
		},
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});


			$('#_supplier_name').text('');
			$('#_po_supplier').text('');
			$('#_item_code').text('');

			$('#__supplier_name').val('');
			$('#__c_bpartner_id').val('');
			$('#__item_code').val('');
			$('#__item_code_source').val('');
			$('#__document_no').val('');

			$('#is_piping').val('');
			$('#__supplier_name_add').val('');
			$('#__c_bpartner_id_add').val('');
			$('#__item_code_add').val('');
			$('#__item_code_source_add').val('');
			$('#__document_no_add').val('');

			$('#__supplier_name_bm').val('');
			$('#__c_bpartner_id_bm').val('');
			$('#__item_code_bm').val('');
			$('#__item_code_source_bm').val('');
			$('#__document_no_bm').val('');

			$('#__planning_date').val();
			$('#__planning_date_bm').val();
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$.unblockUI();
			var supplier_name 	= response.supplier_name;
			var c_bpartner_id 	= response.c_bpartner_id;
			var item_code 		= response.item_code;
			var item_id 		= response.item_id;
			var document_no 	= response.document_no;
			var c_order_id 		= response.c_order_id;

			var flag = checkPlanningExists(document_no, item_id, c_bpartner_id);

			if (flag == 0) 
			{
				temporary_data_prepare = response;
				$('#temporary_data_prepare').val(JSON.stringify(temporary_data_prepare));

				$("#select_article").empty();
				$("#select_article").append('<option value="">-- Select Article --</option>');

				$("#select_style").empty();
				$("#select_style").append('<option value="">-- Select Style --</option>');

				$('#confirmationModal').modal({
					backdrop: 'static',
					keyboard: false // to prevent closing with Esc button (if you want this too)
				});

				$('#_supplier_name').text(supplier_name);
				$('#_po_supplier').text(document_no);
				$('#_item_code').text(item_code);

				$('#__supplier_name').val(supplier_name);
				$('#__c_bpartner_id').val(c_bpartner_id);
				$('#__item_code_source').val(item_code);
				$('#__item_id_source').val(item_id);
				$('#__c_order_id').val(c_order_id);
				$('#__document_no').val(document_no);

				$('#__supplier_name_add').val(supplier_name);
				$('#__c_bpartner_id_add').val(c_bpartner_id);
				$('#__item_code_add').val(item_code);
				$('#__item_code_source_add').val(item_code);
				$('#__document_no_add').val(document_no);
				$('#__c_order_id_add').val(c_order_id);
				$('#__item_id_source_add').val(item_id);
				$('#__item_id_add').val(item_id);

				$('#__supplier_name_bm').val(supplier_name);
				$('#__c_bpartner_id_bm').val(c_bpartner_id);
				$('#__item_code_bm').val(item_code);
				$('#__item_code_source_bm').val(item_code);
				$('#__document_no_bm').val(document_no);
				$('#__c_order_id_bm').val(c_order_id);
				$('#__item_id_source_bm').val(item_id);
				$('#__item_id_bm').val(item_id);


				$("#qty_preparation").attr("readonly", true);
				$("#qty_preparationButtonEdit").attr("disabled", "disabled");
			}else 
			{

				list_data_prepare.push(response);
				var planning_date = getPlanningDate(response.c_order_id, response.item_id, response.c_bpartner_id);
				setSelected(response.c_order_id, response.item_id, response.c_bpartner_id);

				if (response.actual_length != null) 
				{
					response.is_actual_not_null = true;
					selectedPlanning(response.c_order_id,response.document_no, response.item_id,response.item_code, response.c_bpartner_id, planning_date, response.actual_length, list_data_prepare.length - 1);

				}

				$('#data_planning').val(JSON.stringify(sortList(list_data_planning, 'sorting')));
				renderPlanning();
				renderPreprare();
				setFocusToTextBox();
			}

		},
		error: function (response) {
			$.unblockUI();
			$('#barcode_value').val('');
			setFocusToTextBox();

			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT');

			if (response['status'] == 422)
				$("#alert_warning").trigger("click", response.responseJSON);


		}
	})
	.done(function () {
		$('#barcode_value').val('');

	});
}

function checkInput() 
{
	var flag = 0;
	for (var i in list_data_prepare) 
	{
		var data 					= list_data_prepare[i];
		var begin_width 			= $('#beginWidhtInput_' + i).val();
		var middle_width 			= $('#middleWidhtInput_' + i).val();
		var end_width 				= $('#endWidhtInput_' + i).val();
		var actual_length 			= $('#actualLengthInput_' + i).val();

		if (data.begin_width == null || data.begin_width == 0 || begin_width == null || begin_width == 0) flag++;
		if (data.middle_width == null || data.middle_width == 0 || middle_width == null || middle_width == 0) flag++;
		if (data.end_width == null || data.end_width == 0 || end_width == null || end_width == 0) flag++;
		if (data.actual_width == null || data.actual_width == 0) flag++;
		if (data.actual_length == null || data.actual_length == 0 || actual_length == null) flag++;
	}
	return flag;
}

function setFocusToTextBox() 
{
	$('#barcode_value').focus();
}

function setFocusNextInput(inputId) 
{
	split = inputId.split('_');
	classId = split[0];
	dataId = parseInt(split[1]);
	if (classId == 'beginWidhtInput') 
	{
		$('#middleWidhtInput_' + dataId).focus();
	}else if (classId == 'middleWidhtInput') 
	{
		$('#endWidhtInput_' + dataId).focus();
	}else if (classId == 'endWidhtInput') 
	{
		dataId += 1;
		if ($('#beginWidhtInput_' + dataId).length) 
		{
			$('#beginWidhtInput_' + dataId).focus();
		}else 
		{
			setFocusToTextBox();
		}
	}
}

function changeTab(status)
{
	$('#active_tab').val(status).trigger('change');
}

$('#select_article_balance_marker').on('change',function()
{
	var is_piping 			= $('#is_piping').val();
	var warehouse_id 		= $('#planning_warehouse_id').val();
	var value 				= $(this).val();    
	var planning_date 		= $('#__planning_date').val();  
	
	if(value)
    {
		var url_get_planning_per_style = $('#url_get_planning_per_style_balance_marker').val();

		$.ajax({
			type: "get",
			url: url_get_planning_per_style,
			data: {
				planning_date	: planning_date,
				article_no		: value,
				is_piping		: is_piping,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				var styles 	= response.styles;
				var total 	= response.total;

					$("#select_style_balance_marker").empty();
					$.each(styles,function(id,name){
						$("#select_style_balance_marker").append('<option value="'+id+'">'+name+'</option>');
					});

					if(total > 0) $('#select_style_balance_marker').trigger('change');

			},
			error: function (response) {
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_style_balance_marker").empty();
					$("#select_style_balance_marker").append('<option value="">-- Select Style --</option>');
				
				}
			}
		});
    }else
    {
        $("#select_style_balance_marker").empty();
        $("#select_style_balance_marker").append('<option value="">-- select Style --</option>');

    }
});

$('#select_booking_number').on('change',function()
{
	var is_piping 			= $('#is_piping').val();
	var warehouse_id 		= $('#planning_warehouse_id').val();
	var value 				= $(this).val();    
	var planning_date 		= $('#__planning_date').val();  
	
	if(value)
    {
		var url_get_planning_per_no_booking_additional = $('#url_get_planning_per_no_booking_additional').val();

		$.ajax({
			type: "get",
			url: url_get_planning_per_no_booking_additional,
			data: {
				planning_date	: planning_date,
				booking_number		: value,
				is_piping		: is_piping,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				//set qty prepared
				var material_preparation_fabric_id 	= response.id;
				var qty_outstanding 				= response.total_qty_outstanding;
				var qty_relax 						= response.total_qty_rilex;
				var total_reserved_qty				= response.total_reserved_qty;
				var planning_date					= response.planning_date;
				var warehouse_id					= response.warehouse_id;
				var item_code_book					= response.item_code;
				var item_id_book					= response.item_id_book;

				$('#material_preparation_fabric_id_additional').val(material_preparation_fabric_id);
				$('#qty_preparation_additional').val(total_reserved_qty);
				$('#qty_outstanding_additional').val(qty_outstanding);
				$('#qty_relax_additional').val(qty_relax);

			},
			error: function (response) {
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
				{
					$("#alert_warning").trigger("click", response.responseJSON);
					$("#select_booking_number").empty();
					$("#select_booking_number").append('<option value="">-- Select Booking Number --</option>');
				
				}
			}
		});
    }else
    {
        $("#select_booking_number").empty();
        $("#select_booking_number").append('<option value="">-- select Style --</option>');

    }
});

$('#select_style_balance_marker').on('change',function()
{
	var value 				= $(this).val();    
	var article 			= $('#select_article_balance_marker').val();  
	var document_no 		= $('#__document_no').val();  
	var planning_date 		= $('#__planning_date').val();  
	var item_code_source 	= $('#__item_code_source').val();  
	var item_id_source 		= $('#__item_id_source').val();  
	var c_bpartner_id 		= $('#__c_bpartner_id').val();  
	var c_order_id 			= $('#__c_order_id').val();  
	var is_piping 			= $('#is_piping').val();  
	var warehouse_id 		= $('#planning_warehouse_id').val();
	
	if(value)
    {
		active_tab = $('#active_tab').val();
		if(active_tab == 'reguler')
		{
			var url_get_planning_per_date = $('#url_get_planning_per_date').val();
		}
		// else if(active_tab == 'additional')
		// {
		// 	var url_get_planning_per_date = $('#url_get_planning_per_date_additional').val();
		// }
		else
		{
			var url_get_planning_per_date = $('#url_get_planning_per_date_balance_marker').val();
		}
		$.ajax({
			type: "get",
			url: url_get_planning_per_date,
			data: {
				document_no		: document_no,
				item_code		: item_code_source,
				item_id_source	: item_id_source,
				c_bpartner_id	: c_bpartner_id,
				planning_date	: planning_date,
				article_no		: article,
				style			: value,
				c_order_id		: c_order_id,
				is_piping		: is_piping,
				warehouse_id	: warehouse_id,
			},
			beforeSend: function () 
			{
				$('#modal_body').addClass('hidden');
				$('#confirmationModal').find('.shade-screen').removeClass('hidden');
			},
			complete: function () 
			{
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');
			},
			success: function (response) 
			{
				if (response != -1) 
				{
					var material_preparation_fabric_id 	= response.id;
					var qty_outstanding 				= response.total_qty_outstanding;
					var qty_relax 						= response.total_qty_rilex;
					var total_reserved_qty				= response.total_reserved_qty;
					var planning_date					= response.planning_date;
					var warehouse_id					= response.warehouse_id;
					var item_code_book					= response.item_code;
					var item_id_book					= response.item_id_book;

					$('#qty_preparation_balance_marker').val(total_reserved_qty);
					$('#qty_outstanding_balance_marker').val(qty_outstanding);
					$('#qty_relax_balance_marker').val(qty_relax);
					
					$('#__item_code_bm').val(item_code_book);
					$('#__item_id_book_bm').val(item_id_book);
					$('#material_preparation_fabric_id_bm').val(material_preparation_fabric_id);
					$("#qty_preparationButtonEdit").attr("disabled", "disabled");
					//if(planning_date >= '2019-09-03' && warehouse_id == '1000011') $("#qty_preparationButtonEdit").attr("disabled", "disabled");
					//else $("#qty_preparationButtonEdit").removeAttr("disabled");
					
					
					//$("#qty_preparationButtonEdit").removeAttr("disabled");
					$("#qty_preparation").attr("readonly", true);
				} else 
				{
					var warehouse_id = $('#warehouse_id').val();
					//if(warehouse_id == '1000011')
					//{
						$("#qty_preparation").attr("readonly", true);
						$("#qty_preparationButtonEdit").attr("disabled", "disabled");
						$("#alert_warning").trigger("click", 'Allocation not found.');
						return false;
					//}else
					// {
					// 	$('#material_preparation_fabric_id').val('');
					// 	$('#qty_preparation').val('');
					// 	$('#qty_outstanding').val('0');
					// 	$('#qty_relax').val('0');
					// 	$("#qty_preparation").attr("readonly", false);
					// 	$("#qty_preparationButtonEdit").attr("disabled", "disabled");
					// }
				}
			},
			error: function (response) {
				$('#modal_body').removeClass('hidden');
				$('#confirmationModal').find('.shade-screen').addClass('hidden');

				$('#barcode_value').val('');

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
					$("#alert_warning").trigger("click", response.responseJSON);


			}
		});
    }
});



