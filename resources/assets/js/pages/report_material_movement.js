$(function()
{
    $('#daily_material_movement_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/report/material-movement/data',
            data: function(d) {
                return $.extend({}, d, {
                    "po_buyer"     : $('#po_buyerName').val(),
                });
           }
        },
        columns: [
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
            {data: 'warehouse', name: 'warehouse',searchable:false,visible:true,orderable:true},
            {data: 'baclog_status', name: 'baclog_status',searchable:false,visible:true,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:false},
            {data: 'qty_need', name: 'qty_need',searchable:false,visible:true,orderable:false},
            {data: 'qty_movement', name: 'qty_movement',searchable:false,visible:true,orderable:false},
            {data: 'status', name: 'status',searchable:false,visible:true,orderable:false},
            {data: 'is_additional', name: 'is_additional',searchable:false,visible:true,orderable:false},
            {data: 'from_location', name: 'from_location',searchable:true,visible:true,orderable:false},
            {data: 'to_destination', name: 'to_destination',searchable:true,visible:true,orderable:false},
            {data: 'movement_date', name: 'movement_date',searchable:false,visible:true,orderable:false},
            {data: 'user_name', name: 'user_name',searchable:false,visible:true,orderable:false},
            {data: 'movement_line_notes', name: 'movement_line_notes',searchable:false,visible:true,orderable:false},
        ],
        order: [[16, 'asc']]
    });

    var dtable = $('#daily_material_movement_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    buyerLov('po_buyer', '/report/material-movement/po-buyer-picklist?');
    
    function buyerLov(name, url) 
    {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#ButtonSrc';
        var buttonDel = '#' + name + 'ButtonDel';
        var is_cancel = $('#is_cancel_order').val();

        function itemAjax() 
        {
            var q = $(search).val();
            
            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');

            $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) 
            {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
        }

        function chooseItem() 
        {
            var id      = $(this).data('id');
            var name    = $(this).data('name');
            
            $(item_id).val(id);
            $(item_name).val(name);
            dtable.draw();
        }

        function pagination() 
        {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');

                e.preventDefault();
                itemAjax();
            });
        }

        //$(search).val("");
        $(buttonSrc).unbind();
        $(search).unbind();

        $(buttonSrc).on('click', itemAjax);

        $(search).on('keypress', function (e) 
        {
            if (e.keyCode == 13)
                itemAjax();
        });

        $(buttonDel).on('click', function () 
        {
            $(item_id).val('');
            $(item_name).val('');

        });

        itemAjax();
    }
})


