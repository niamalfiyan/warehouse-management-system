list_detail_fir = JSON.parse($('#detail_fir').val());
list_defect_codes = JSON.parse($('#defect_codes').val());
list_deleted = JSON.parse($('#list_deleted').val());
list_header = JSON.parse($('#header').val());


$(function () 
{
    renderDefect();
    render();
    setFocusToTextBox();

    $('#form').submit(function (event) 
    {
        event.preventDefault();

        bootbox.confirm("Are you sure want to save this ?.", function (result) 
        {
            if (result) 
            {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    error: function (response) {
                        $.unblockUI();
                        $('#barcode_value').val('');
                        $('.barcode_value').focus();

                        if (response['return'] == 500)
                            $("#alert_error").trigger("click", 'Please Contact ICT');

                        if (response['return'] == 422)
                            $("#alert_error").trigger("click", response.responseJSON);

                    }
                })
                .done(function (){
                    var url_fabric_material_quality_control_detail = $('#url_fabric_material_quality_control_detail').val();
                    document.location.href  = url_fabric_material_quality_control_detail;
                });
            }
        });
    });
});

function render() 
{
    getIndex();
    setValueToInputHTML();
    renderHeader();
    var tmpl = $('#defect-code-table').html();
    Mustache.parse(tmpl);
    var data = {
        item: list_detail_fir
    };
    var html = Mustache.render(tmpl, data);
    $('#tbody-detail-fir').html(html);
    bind();
}

function setValueToInputHTML() 
{
    $('#detail_fir').val(JSON.stringify(list_detail_fir));
    $('#header').val(JSON.stringify(list_header));
    $('#list_deleted').val(JSON.stringify(list_deleted));

}

function renderDefect() 
{
    $('#defect_codes').val(JSON.stringify(list_defect_codes));
    var data = {
        item: list_defect_codes
    };
}

//fungsi ini untuk merubah nilai textbox di view
function renderHeader() 
{
    $('#point_1').val(list_header.point_1);
    $('#point_2').val(list_header.point_2);
    $('#point_3').val(list_header.point_3);
    $('#point_4').val(list_header.point_4);
    $('#percentage').val(list_header.percentage);
    $('#total_point').val(list_header.total_point);
}

//fungsi untuk binding input triger
function bind() 
{
    $('.input-edit-defect-value').on('click', editDetailDefectValue);
    $('.input-edit-point-from').on('change', editInputPointFrom);
    $('.input-edit-point-to').on('change', editInputPointTo);
    $('.input-edit-point-defect').on('change', cekDefectCode);
    $('.input-edit-remark').on('change', changeRemark);
    $('.btn-delete-item').on('click', deleteItem);


    $('.input-number').keypress(function (e) {
        var charCode = (e.which) ? e.which : e.keyCode;
        if ((charCode >= 46 && charCode <= 57) || (charCode >= 37 && charCode <= 40) || charCode == 9 || charCode == 8 || charCode == 46) {
            return true;
        }
        return false;
    });

    $('.input-edit').keypress(function (e) {
        var i = $(this).data('id');

        if (e.keyCode == 13) {
            e.preventDefault();
            $('#simpan_' + i).click();
        }
    });
}

function getIndex() 
{
    for (id in list_detail_fir) 
    {
        list_detail_fir[id]['_id'] = id;
        list_detail_fir[id]['no'] = parseInt(id) + 1;
    }
}

function editInputPointFrom() 
{
    var id = $('#' + this.id);
    var data_id = id.data("id");
    var value = id.val();
    list_detail_fir[data_id].start_point_check = value;
    updateTotalPointDefect(data_id);
    render();
    renderHeader();
}

//fungsi untuk merubah point to ketika input berubah
function editInputPointTo() 
{
    var id = $('#' + this.id);
    var data_id = id.data("id");
    var value = id.val();
    list_detail_fir[data_id].end_point_check = value;
    updateTotalPointDefect(data_id);
    render();
    renderHeader();
}

function deleteItem() 
{
    let idx                 = $(this).data('id');
    let detail              = list_detail_fir[idx];
    let curr_defect_value   = detail.defect_value;
    let defect_code         = detail.defect_code;
    let defect_value        = detail.defect_value;
    let multiply            = detail.multiply;
    let jumlah_defect       = list_header.jumlah_defect[defect_code];

    let curr_point_1 = parseInt(list_header.point_1);
    let curr_point_2 = parseInt(list_header.point_2);
    let curr_point_3 = parseInt(list_header.point_3);
    let curr_point_4 = parseInt(list_header.point_4);

    console.log(list_header);
    if (curr_defect_value == 1) 
    {
        curr_point_1 -= 1 * multiply;
        var total_point_reduce = defect_value * multiply;
    } else if (curr_defect_value == 2) 
    {
        curr_point_2 -= 2 * multiply;
        var total_point_reduce = defect_value * multiply;
    } else if (curr_defect_value == 3) 
    {
        curr_point_3 -= 3 * multiply;
        var total_point_reduce = defect_value * multiply;
    } else if (curr_defect_value == 4) 
    {
        curr_point_4 -= 4 * multiply;
        var total_point_reduce = defect_value * multiply;
    }

    let total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);

    setPercentage(total_point, id);
    list_header.point_1 = curr_point_1;
    list_header.point_2 = curr_point_2;
    list_header.point_3 = curr_point_3;
    list_header.point_4 = curr_point_4;
    list_header.total_point = total_point;
    list_deleted.push(detail.id);
    list_header.jumlah_defect[defect_code] = jumlah_defect - total_point_reduce;
    list_detail_fir.splice(idx, 1);

    render();
}

function changeRemark() 
{
    var id      = $('#' + this.id);
    var data_id = id.data("id");
    var value   = id.val();

    list_detail_fir[data_id].remark = value;
    render();
}

function cekDefectCode() 
{
    let id                  = $(this).data('id');
    let detail              = list_detail_fir[id];
    let defect_code_after   = $('#defect_code_' + id).val().toUpperCase();
    let defect_code_before  = detail.defect_code;
    let defect_value        = detail.defect_value;

    if (defect_code_after != null) 
    {
        let notFoundDefectCode = true;
        for (i in list_defect_codes) 
        {
            let data = list_defect_codes[i];
            if (data.code == defect_code_after) {
                notFoundDefectCode = false;
                break;
            }

        }

        if (notFoundDefectCode) 
        {
            $("#alert_error").trigger("click", 'Defect code not found.');
            $('#defect_code_' + id).val(defect_code_before);
            return false;
        }else 
        {
            if (id !== undefined) {
                $('#defect_code_' + id).val(defect_code_after);
                list_detail_fir[id].defect_code = defect_code_after;
                render();
            } else {
                $('#defect_code_' + id).val(defect_code_after);
            }
            changeJumlahDefect(defect_code_before, defect_code_after, defect_value, defect_value);
        }
    }

}

function updateMultiply(id) 
{
    var data = list_detail_fir[id];
    var point_check_from = data.start_point_check;
    var point_check_to = data.end_point_check;
    var multiply = parseInt((parseInt(point_check_to) - parseInt(point_check_from)) + 1);
    data.multiply = multiply;
}

function updateTotalPointDefect(id) 
{
    var data = list_detail_fir[id];

    var before_multiply = data.multiply;
    updateMultiply(id);
    var after_multiply = data.multiply;

    var is_selected_1 = data.is_selected_1;
    var is_selected_2 = data.is_selected_2;
    var is_selected_3 = data.is_selected_3;
    var is_selected_4 = data.is_selected_4;

    var curr_point_1 = parseInt(list_header.point_1);
    var curr_point_2 = parseInt(list_header.point_2);
    var curr_point_3 = parseInt(list_header.point_3);
    var curr_point_4 = parseInt(list_header.point_4);

    if (is_selected_1 == true) {
        var total_point_defect = 1 * before_multiply;
        curr_point_1 -= total_point_defect;
        var total_point_defect = 1 * after_multiply;
        curr_point_1 += total_point_defect;
    } else if (is_selected_2 == true) {
        var total_point_defect = 2 * before_multiply;
        curr_point_2 -= total_point_defect;
        var total_point_defect = 2 * after_multiply;
        curr_point_2 += total_point_defect;
    } else if (is_selected_3 == true) {
        var total_point_defect = 3 * before_multiply;
        curr_point_3 -= total_point_defect;
        var total_point_defect = 3 * after_multiply;
        curr_point_3 += total_point_defect;
    } else if (is_selected_4 == true) {
        var total_point_defect = 4 * before_multiply;
        curr_point_4 -= total_point_defect;
        var total_point_defect = 4 * after_multiply;
        curr_point_4 += total_point_defect;
    }
    var total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);

    setPercentage(total_point);
    list_header.point_1 = curr_point_1;
    list_header.point_2 = curr_point_2;
    list_header.point_3 = curr_point_3;
    list_header.point_4 = curr_point_4;
    list_header.total_point = total_point;
    data.total_point_defect = total_point_defect;
}

function editDetailDefectValue(data_id = null) 
{
    let id = $(this).data('id');
    let data = list_detail_fir[id];
    let curr_defect_value = data.defect_value;
    let defect_value_before = data.defect_value * data.multiply;
    let defect_code = data.defect_code;
    updateMultiply(id);
    let multiply = data.multiply;
    let defect_value = $('input[name=defect_code_value_' + id + ']:checked').val();

    let is_selected_1 = false;
    let is_selected_2 = false;
    let is_selected_3 = false;
    let is_selected_4 = false;

    let curr_point_1 = parseInt(list_header.point_1);
    let curr_point_2 = parseInt(list_header.point_2);
    let curr_point_3 = parseInt(list_header.point_3);
    let curr_point_4 = parseInt(list_header.point_4);

    if (curr_defect_value == 1)
        curr_point_1 -= 1 * multiply;
    else if (curr_defect_value == 2)
        curr_point_2 -= 2 * multiply;
    else if (curr_defect_value == 3)
        curr_point_3 -= 3 * multiply;
    else if (curr_defect_value == 4)
        curr_point_4 -= 4 * multiply;
    let total_point_defect
    if (document.getElementById('defect_code_value_' + id + '_1').checked == true) {
        is_selected_1 = true;
        total_point_defect = 1 * multiply;
        curr_point_1 += total_point_defect;
    } else if (document.getElementById('defect_code_value_' + id + '_2').checked == true) {
        is_selected_2 = true;
        total_point_defect = 2 * multiply;
        curr_point_2 += total_point_defect;
    } else if (document.getElementById('defect_code_value_' + id + '_3').checked == true) {
        is_selected_3 = true;
        total_point_defect = 3 * multiply;
        curr_point_3 += total_point_defect;
    } else if (document.getElementById('defect_code_value_' + id + '_4').checked == true) {
        is_selected_4 = true;
        total_point_defect = 4 * multiply;
        curr_point_4 += total_point_defect;
    }
    let defect_value_after = total_point_defect;
    let total_point = (curr_point_1 + curr_point_2 + curr_point_3 + curr_point_4);
    changeJumlahDefect(defect_code, defect_code, defect_value_before, defect_value_after);
    setPercentage(total_point);

    list_header.point_1 = curr_point_1;
    list_header.point_2 = curr_point_2;
    list_header.point_3 = curr_point_3;
    list_header.point_4 = curr_point_4;
    list_header.total_point = total_point;


    data.defect_value = defect_value;
    data.is_selected_1 = is_selected_1;
    data.is_selected_2 = is_selected_2;
    data.is_selected_3 = is_selected_3;
    data.is_selected_4 = is_selected_4;
    data.multiply = multiply;
    data.total_point_defect = total_point_defect;


    render();
}

function changeJumlahDefect(defect_code_before, defect_code_after, value_before, value_after) 
{
    let jumlah_defect = list_header.jumlah_defect;
    jumlah_defect[defect_code_before] = parseInt(jumlah_defect[defect_code_before]) - value_before;
    jumlah_defect[defect_code_after] = parseInt(jumlah_defect[defect_code_after]) + value_after;
    list_header.jumlah_defect = jumlah_defect;
}

function setPercentage(total_point) 
{
    var data = list_header;
    var qty = parseFloat(data.stock);

    var percentage = parseFloat((parseFloat(total_point) / qty) * 100).toFixed(2);

    data.percentage = percentage;

    if (percentage > 15.00) {
        data.status = 'REJECT';
    } else {
        data.status = 'RELEASE';
    }
}

function setFocusToTextBox() 
{
    $('#barcode_value').focus();
}
