list_material_statistical_dates = JSON.parse($('#material_statistical_dates').val());

$(function(){
	$('#no_invoiceName').click(function () {
		MonitoringStatisticalDateLov('no_invoice', '/report/accessories/monitoring-statistical-date/invoice-picklist?');
	});

	$('#no_invoiceButton').click(function () {
		MonitoringStatisticalDateLov('no_invoice', '/report/accessories/monitoring-statistical-date/invoice-picklist?');
	});

	$('#document_noName').click(function () {
		MonitoringStatisticalDateLov('document_no', '/report/accessories/monitoring-statistical-date/document-picklist?');
	});

	$('#document_noButton').click(function () {
		MonitoringStatisticalDateLov('document_no', '/report/accessories/monitoring-statistical-date/document-picklist?');
	});

	
	function MonitoringStatisticalDateLov(name, url) {
		var search = '#' + name + 'Search';
		var list = '#' + name + 'List';
		var item_id = '#' + name + 'Id';
		var item_name = '#' + name + 'Name';
		var modal = '#' + name + 'Modal';
		var table = '#' + name + 'Table';
		var buttonSrc = '#ButtonSrc';
		var buttonDel = '#' + name + 'ButtonDel';

		function itemAjax() {
			var q = $(search).val();

			$(table).addClass('hidden');
			$(modal).find('.shade-screen').removeClass('hidden');
			$(modal).find('.form-search').addClass('hidden');

			$.ajax({
				url: url + '&no_invoice=' + q
			})
			.done(function (data) {
				$(table).html(data);
				pagination(name);
				$(search).val('');
				$(search).focus();
				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
		}

		function chooseItem() {
			var invoice = $(this).data('invoice');

			$(item_id).val(invoice);
			$(item_name).val(invoice);

			getDataBuyer(invoice);
		}

		function pagination() {
			$(modal).find('.pagination a').on('click', function (e) {
				var params = $(this).attr('href').split('?')[1];
				url = $(this).attr('href') + (params == undefined ? '?' : '');

				e.preventDefault();
				itemAjax();
			});
		}

		$(buttonSrc).unbind();
		$(search).unbind();

		$(buttonSrc).on('click', itemAjax);

		$(search).on('keypress', function (e) {
			if (e.keyCode == 13)
				itemAjax();
		});

		$(buttonDel).on('click', function () {
			$(item_id).val('');
			$(item_name).val('');

		});

		itemAjax();
	}

	function render(){
		getIndex();
		$('#material_statistical_dates').val(JSON.stringify(list_material_statistical_dates));
		var tmpl = $('#monitoring-statistical-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_material_statistical_dates };
		var html = Mustache.render(tmpl, data);
		$('#tbody-monitoring-statistical-date').html(html);
	}

	function getIndex(){
		for (idx in list_material_statistical_dates){
			list_material_statistical_dates[idx]['_idx'] = idx;
		}
	}

	function getDataBuyer(no_invoice){
		var url_get_buyer = $('#url_get_buyer').attr('href');
		$.ajax({
			type: "GET",
			url: url_get_buyer,
			data: {
				no_invoice: no_invoice
			},
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				console.log(response);
				list_material_statistical_dates = response;
			},
			error: function (response) {
				$.unblockUI();

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 400) {
					$('#po_buyer_error').addClass('has-error');
					$('#item_code_error').addClass('has-error');
					$('#document_no_error').addClass('has-error');

					$('#po_buyer_danger').text('Harap salah satu field ini diisi');
					$('#item_code_danger').text('Harap salah satu field ini diisi');
					$('#document_no_danger').text('Harap salah satu field ini diisi');
				}

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response['responseJSON']);

			}
		})
		.done(function () {
			render();
		});
	}

	render();
});

