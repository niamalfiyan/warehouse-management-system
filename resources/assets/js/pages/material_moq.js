function hapus(link){
	$.ajax({
		type: "GET",
		url: link,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Data berhasil di hapus !');
			$('#dataTableBuilder').DataTable().ajax.reload();
		},
		error: function (response) {
			$.unblockUI()
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

		}
	});
}
$(function(){

	/*var stop = $('#stop').val();
	$('#_po_buyer').change(function(){
		var _po_buyer = $('#_po_buyer').val();
		var stop2 = $('#stop').val();

		if (_po_buyer && stop2 == false) {
			$('#form').submit();
		}

	});

	if (stop){
		$('#_po_buyer').val('');
		$('#stop').val('');
	}*/

	/*$('#form').submit(function (event) {
		event.preventDefault();
		$.ajax({
			type: "get",
			url: $('#form').attr('action'),
			data: $('#form').serialize(),
			beforeSend: function () {
				$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
			},
			complete: function () {
			},
			success: function (response) {
				$.unblockUI();
				$('#form').trigger('reset');
				//
			},
			error: function (response) {
				$.unblockUI();
				 if (response['status'] == 500) {
					$("#alert_error").trigger("click", 'Please Contact ICT');
				}
			}
		});
	});*/

	$('#upload_button').on('click', function () {
		$('#upload_file').trigger('click');
	});

	$('#upload_file').on('change', function () {
		//$('#upload_file_allocation').submit();
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			type: "POST",
			url: $('#upload_file_allocation').attr('action'),
			data: new FormData(document.getElementById("upload_file_allocation")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				$("#alert_info").trigger("click", 'upload berhasil.');
				//$('#_po_buyer').val(response).trigger('change');
				$('#dataTableBuilder').DataTable().ajax.reload();
			},
			error: function (response) {
				$.unblockUI()
				$('#upload_file_allocation').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');

			}
		})
		.done(function (response) {
			$('#upload_file_allocation').trigger('reset');
		});

	})



});
