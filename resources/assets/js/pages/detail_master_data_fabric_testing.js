
$(function()
{
  var master_data_fabric_testing_id = $('#master_data_fabric_testing_id').val();
  console.log(master_data_fabric_testing_id);
    $('#detail_master_data_fabric_testing_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        ajax: {
            type: 'GET',
            // data : {
            //   master_data_fabric_testing_id : master_data_fabric_testing_id
            // },
            url: '/master-data/fabric-testing/detail/data/' + master_data_fabric_testing_id,
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'subtesting_name', name: 'subtesting_name',searchable:false,visible:true,orderable:true},
            {data: 'require', name: 'require',searchable:false,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#detail_master_data_fabric_testing_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
})

function hapus(url) 
{
    bootbox.confirm("Are you sure want to delete this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "delete",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#master_data_fabric_testing_table').DataTable().ajax.reload();
			});
		}
	});
}

$('#form').submit(function (event) 
{
  event.preventDefault();
  var subtesting_name = $('#subtesting_name').val();
  var require = $('#require').val();

  if (!subtesting_name) 
  {
    $("#alert_warning").trigger("click", 'Please insert code first.');
    return false;
  }

  if (!require) 
  {
    $("#alert_warning").trigger("click", 'Please insert code first.');
    return false;
  }
   
  bootbox.confirm("Are you sure want to save this data ?.", function (result) 
  {
    if (result) 
    {
        $.ajax({
            type: "POST",
            url: $('#form').attr('action'),
            data: $('#form').serialize(),
            beforeSend: function () {
              $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
    
    
            },
            error: function (response) 
            {
              $.unblockUI();
              if (response.status == 500) $("#alert_error").trigger("click", 'Please contact ICT');
              if (response.status == 422) $("#alert_error").trigger("click", 'Code already exists');
            }
        })
        .done(function () 
        {
            var master_data_fabric_testing_id = $('#master_data_fabric_testing_id').val();
            var url                 = '/master-data/fabric-testing/detail/' + master_data_fabric_testing_id;
            document.location.href  = url;
        });
    }
  });
});
