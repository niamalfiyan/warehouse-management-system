list_material_preparations_fabric = JSON.parse($('#material_preparations_fabric').val());
list_style = JSON.parse($('#styles').val());
list_selected_material_preparations_fabric = JSON.parse($('#selected_material_preparations_fabric').val());

$(function () {
    function render_syle(){
        getIndexStyle();
        $('#styles').val(JSON.stringify(list_style));
        var tmpl = $('#_draw_style').html();
        Mustache.parse(tmpl);
        var data = { items: list_style };
        var html = Mustache.render(tmpl, data);
        $('#draw_style').html(html);
        isSelectedStyle();
        bindStyle();

    }

    function render() {
        getIndex();
        $('#material_preparations_fabric').val(JSON.stringify(list_material_preparations_fabric));
        var tmpl = $('#_draw').html();
        Mustache.parse(tmpl);
        var data = { items: list_material_preparations_fabric };
        var html = Mustache.render(tmpl, data);
        $('#draw_result').html(html);
        isSelected();
        bind();
    }

    function render_selected(){
        getIndexSelected();
        $('#selected_material_preparations_fabric').val(JSON.stringify(list_selected_material_preparations_fabric));
        console.log(list_selected_material_preparations_fabric);
    }
    

    function getIndex() {
        for (id in list_material_preparations_fabric) {
            list_material_preparations_fabric[id]['_id'] = id;
            list_material_preparations_fabric[id]['no'] = parseInt(id) + 1;

            var details = list_material_preparations_fabric[id]['detail'];

            for (var idx in details){
                details[idx]['_idx'] = idx;

                var sub_details = details[idx]['detail'];

                for (var idxy in sub_details){
                    sub_details[idxy]['_idxy'] = idxy;
                }
            }
        }
    }

    function getIndexStyle() {
        for (id in list_style) {
            list_style[id]['_id'] = id;
            list_style[id]['no'] = parseInt(id) + 1;
            
            var details = list_style[id]['detail'];

            for (var idx in details) {
                details[idx]['_idx'] = idx;
            }

        }
    }

    function getIndexSelected(){
        for (id in list_selected_material_preparations_fabric) {
            list_selected_material_preparations_fabric[id]['_id'] = id;
        }
    }

    function isSelected() {
        for (var i in list_material_preparations_fabric) {
            var data = list_material_preparations_fabric[i];
            for (var ix in data['detail']) {
                var detail = data['detail'][ix];

                if (detail.is_selected) {
                    $('#selected_panel_' + i + '_' + ix).addClass('bg-success');
                } else {
                    $('#selected_panel_' + i + '_' + ix).removeClass('bg-success');

                }
            }
           

        }
    }

    function isSelectedStyle() {
        for (var i in list_style) {
            var details = list_style[i]['detail'];
            for (var ix in details) {
                var detail = details[ix];
                if (detail.is_selected) {
                    $('#selected_article_' + i + '_' + ix).css('background-color', '#E8F5E9');
                } else {
                    $('#selected_article_' + i + '_' + ix).css('background-color', 'transparent');

                }
            }
            

        }
    }

    function bindStyle(){
        $('.selected-article').on('click', selectedArticle)
    }

    function bind() {
        $('.select-detail-panel').on('click', selectDetailPanel);
        //$('.listofValueDocSupplier').on('click', DocSupplierLov);
        //$('.input-edit-date').on('change', tambahTanggal);

    }
    
    function selectedArticle(){
       var id = $(this).data('id');
       var idx = $(this).data('idx');
       
       for (var i in list_style) {
           var data = list_style[i];
           var details = list_style[i]['detail'];

           for (var ix in details) {
               var detail = details[ix];

                if (i == id && ix == idx){
                    var selected = detail.is_selected;
                    if (selected == false) {
                        var url_get_detail_buyer = $('#url_get_detail_buyer').attr('href');
                        var planning_date = data.planning_date;
                        var style = data.style;
                        var article_no = detail.article_no;
                        
                        $.ajax({
                            type: "GET",
                            url: url_get_detail_buyer,
                            data: {
                                planning_date: planning_date, 
                                style: style, 
                                article_no: article_no 
                            },
                            beforeSend: function () {
                                $.blockUI({
                                    message: '<i class="icon-spinner4 spinner"></i>',
                                    overlayCSS: {
                                        backgroundColor: '#fff',
                                        opacity: 0.8,
                                        cursor: 'wait'
                                    },
                                    css: {
                                        border: 0,
                                        padding: 0,
                                        backgroundColor: 'transparent'
                                    }
                                });
                                list_material_preparations_fabric = [];
                            },
                            complete: function () {
                                $.unblockUI();
                            },
                            success: function (response) {
                                for (var id in response) {
                                    var data = response[id];
                                    
                                    for (var idy in list_selected_material_preparations_fabric){
                                        var selected = list_selected_material_preparations_fabric[idy];
                                        
                                        for (var idx in data.detail) {
                                            var detail = data.detail[idx];

                                            if (selected.article_no == data.article_no && selected.style == data.style &&
                                                selected.planning_date == data.planning_date && selected.document_no == data.document_no &&
                                                selected.item_code == data.item_code && selected.actual_width == detail.actual_width
                                                && selected.c_bpartner_id == data.c_bpartner_id
                                            ){
                                                detail.is_selected = true;
                                            }
                                        }
                                    }

                                    var input = {
                                        'style': data.style,
                                        'article_no': data.article_no,
                                        'planning_date': data.planning_date,
                                        'document_no': data.document_no,
                                        'c_bpartner_id': data.c_bpartner_id,
                                        'item_code': data.item_code,
                                        'total_reserved_qty_per_document': data.total_reserved_qty_per_document,
                                        'detail': data.detail,
                                    };
                                    list_material_preparations_fabric.push(input);
                                }
                            },
                            error: function (response) {
                                $.unblockUI();
                                if (response['status'] == 500)
                                    $("#alert_error").trigger("click", 'Please Contact ICT');

                            }
                        })
                        .done(function () {
                            render();
                        });


                        detail.is_selected = true;
                    } else if (selected == true) {
                        detail.is_selected = false;
                    }
                }else{
                    detail.is_selected = false;
                }
                
            }
               
           
       }


        

        render_syle();

    }

    function selectDetailPanel(){
        var id = $(this).data('id');
        var idx = $(this).data('idx');
        var data = list_material_preparations_fabric[id];
        var detail = list_material_preparations_fabric[id]['detail'][idx];
        
        if (detail.is_selected == false){
            detail.is_selected = true;
            var input = {
                article_no: data.article_no,
                style: data.style,
                c_bpartner_id: data.c_bpartner_id,
                planning_date: data.planning_date,
                document_no: data.document_no,
                item_code: data.item_code,
                actual_width: detail.actual_width
            };
            list_selected_material_preparations_fabric.push(input);
        } else if (detail.is_selected == true){
            if (!detail.is_already_planning){
                detail.is_selected = false;
                for (var i in list_selected_material_preparations_fabric) {
                    var selected = list_selected_material_preparations_fabric[i];

                    if (selected.article_no == data.article_no && selected.style == data.style &&
                        selected.planning_date == data.planning_date && selected.document_no == data.document_no &&
                        selected.item_code == data.item_code && selected.actual_width == detail.actual_width
                        && selected.c_bpartner_id == data.c_bpartner_id
                    ) {
                        list_selected_material_preparations_fabric.splice(i, 1);
                    }
                }
            }
           
        }
            
        render_selected();
        render();
    }

    $('#getPreparationFabric').submit(function (event){
        event.preventDefault();
        $.ajax({
            type: "GET",
            url: $('#getPreparationFabric').attr('action'),
            data: $('#getPreparationFabric').serialize(),
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
                list_style = [];
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                for (var id in response) {
                    var data = response[id];
                    var input = {
                        'planning_date': data.planning_date,
                        'style': data.style,
                        'total_style_prepared': data.total_style_prepared,
                        'is_selected': data.is_selected,
                        'detail': data.detail,
                    };
                    list_style.push(input);
                }
            },
            error: function (response) {
                $.unblockUI();
                
                if (response['status'] == 500)
                    $("#alert_error").trigger("click", 'Please Contact ICT');

                if (response['status'] == 400)
                    $("#alert_error").trigger("click", 'SILAHKAN CEK RANGE TANGGAL ANDA');
            }
        })
        .done(function () {
            render_syle();
        });
    });

    $('#form').submit(function (event) {
        event.preventDefault();

        bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function () {
                        $("#alert_success").trigger("click", 'Data berhasil disimpan');
                    },
                    error: function (response) {
                        $.unblockUI();
                       
                        if (response['status'] == 500)
                            $("#alert_error").trigger("click", 'Please Contact ICT');

                        if (response['status'] == 422)
                            $("#alert_error").trigger("click", response.responseJSON);

                      

                    }
                })
                .done(function () {
                    list_style = [];
                    list_material_preparations_fabric = [];
                    list_selected_material_preparations_fabric = [];

                    render();
                    render_selected();
                    render_syle();
                });
            }
        });
    });

    render_syle();

});


//# sourceMappingURL=material_arrival.js.map