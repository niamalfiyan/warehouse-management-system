$(function()
{
    $('#report_outstanding_material_cancel_order_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/material-outstanding-cancel-order/data',
        },           
        columns: [
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:false},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
            {data: 'job_order', name: 'job_order',searchable:true,visible:true,orderable:true},
            {data: 'uom_conversion', name: 'uom_conversion',searchable:true,visible:true,orderable:true},
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
            {data: 'warehouse', name: 'warehouse',searchable:false,visible:true,orderable:false},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:false},
            {data: 'locator', name: 'locator',searchable:true,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#report_outstanding_material_cancel_order_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });;
});