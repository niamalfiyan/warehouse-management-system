list_purchasing_order = JSON.parse($('#purchasing_order').val());

$(function(){
	$('#po_buyerName').click(function () {
		MonitoringStatisticalDateLov('po_buyer', '/report/accessories/monitoring-purchase-order/po-buyer-picklist?');
	});

	$('#po_buyerButton').click(function () {
		MonitoringStatisticalDateLov('po_buyer', '/report/accessories/monitoring-purchase-order/po-buyer-picklist?');
	});

	
	function MonitoringStatisticalDateLov(name, url) {
		var search = '#' + name + 'Search';
		var list = '#' + name + 'List';
		var item_id = '#' + name + 'Id';
		var item_name = '#' + name + 'Name';
		var modal = '#' + name + 'Modal';
		var table = '#' + name + 'Table';
		var buttonSrc = '#ButtonSrc';
		var buttonDel = '#' + name + 'ButtonDel';

		function itemAjax() {
			var q = $(search).val();

			$(table).addClass('hidden');
			$(modal).find('.shade-screen').removeClass('hidden');
			$(modal).find('.form-search').addClass('hidden');

			$.ajax({
				url: url + '&po_buyer=' + q
			})
			.done(function (data) {
				$(table).html(data);
				pagination(name);
				$(search).val('');
				$(search).focus();
				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
		}

		function chooseItem() {
			var pobuyer = $(this).data('po-buyer');

			$(item_id).val(pobuyer);
			$(item_name).val(pobuyer);

			getDataBuyer(pobuyer);
		}

		function pagination() {
			$(modal).find('.pagination a').on('click', function (e) {
				var params = $(this).attr('href').split('?')[1];
				url = $(this).attr('href') + (params == undefined ? '?' : '');

				e.preventDefault();
				itemAjax();
			});
		}

		$(buttonSrc).unbind();
		$(search).unbind();

		$(buttonSrc).on('click', itemAjax);

		$(search).on('keypress', function (e) {
			if (e.keyCode == 13)
				itemAjax();
		});

		$(buttonDel).on('click', function () {
			$(item_id).val('');
			$(item_name).val('');

		});

		itemAjax();
	}

	function render(){
		getIndex();
		$('#purchasing_order').val(JSON.stringify(list_purchasing_order));
		var tmpl = $('#monitoring-purchase-order-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_purchasing_order };
		var html = Mustache.render(tmpl, data);
		$('#tbody-monitoring-purchase-order-table').html(html);
	}

	function getIndex(){
		for (idx in list_purchasing_order){
			list_purchasing_order[idx]['_idx'] = idx;
		}
	}

	function getDataBuyer(po_buyer){
		var url_get_purchase_order = $('#url_get_purchase_order').attr('href');
		$.ajax({
			type: "GET",
			url: url_get_purchase_order,
			data: {
				po_buyer: po_buyer
			},
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				if(response.length==0){
					$("#alert_error").trigger("click", 'DATA UNTUK PO BUYER '+po_buyer+' TIDAK ADA !');
				}
				list_purchasing_order = response;
			},
			error: function (response) {
				$.unblockUI();

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');
				if (response['status'] == 422)
					$("#alert_error").trigger("click", response['responseJSON']);

			}
		})
		.done(function () {
			render();
			document.getElementById("purchasing_order").value = po_buyer;
		});
	}

	render();
});