
$(function(){
	var dtable = $('#material-requirement-datatable').dataTable().api();
	dtable.page.len(100);
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
		});
	dtable.draw();

	$('#upload_button').on('click', function () {
		$('#upload_file').trigger('click');
	});

	$('#upload_file').on('change', function () {
		
		$.ajax({
			type: "POST",
			url: $('#upload_file_allocation').attr('action'),
			data: new FormData(document.getElementById("upload_file_allocation")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				$("#alert_info").trigger("click", 'upload berhasil.');
				document.location.href = response
				//$('#_po_buyer').val(response).trigger('change');
			},
			error: function (response) {
				$.unblockUI()
				$('#upload_file_allocation').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');

			}
		})
		.done(function (response) {
			$('#upload_file_allocation').trigger('reset');
		});

	})

	$('#upload_manual_button').on('click', function () {
		$('#upload_manual_file').trigger('click');
	});

	$('#upload_manual_file').on('change', function () {
		
		$.ajax({
			type: "POST",
			url: $('#upload_file_manual_allocation').attr('action'),
			data: new FormData(document.getElementById("upload_file_manual_allocation")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				// $("#alert_info").trigger("click", 'upload berhasil.');
				
				
				var url = response.url;
				var code = response.code;
				console.log(url);
				swal({
					title: "For your information",
					text: 'Upload berhasil dengan Kode Batch\n '+code,
					confirmButtonColor: "#2196F3",
					button: true,
					showConfirmButton: true,
					type: "info",
				}, function() {
					document.location.href = response.url
				});

				// $("#alert_info_2").trigger("click", 'Upload berhasil dengan Kode Batch\n '+code);
				
				//$('#_po_buyer').val(response).trigger('change');
			},
			error: function (response) {
				$.unblockUI()
				$('#upload_file_allocation').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');
				if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

			}
		})
		.done(function (response) {
			$('#upload_file_allocation').trigger('reset');
		});

	})

	$('#form').submit(function (event) {
		var is_from_home = $('#is_from_home').val();
		event.preventDefault();
		$.ajax({
			type: "POST",
			url: $('#form').attr('action'),
			data: $('#form').serialize(),
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				}); 
			},
			success: function (response) {
				if (is_from_home)
					document.location.href = '/home/material-requirement?po_buyer=' + response;
				else
					document.location.href = '/erp/material-requirement?po_buyer=' + response;
			},
			error: function (response) {
				$.unblockUI();

				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT');

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response.responseJSON);

			}
		});

	});
});


function recalculate()
{
	// var uuid = "'"+id+"'";
	$('#recalculateModal').modal();
}

// $('#recalculate_download').submit(function (event){
// 	event.preventDefault();
// 	var c_keterangan = $('#batch_code').val();

// 	if(!c_keterangan){
// 		$("#alert_warning").trigger("click", 'Batch Code harus diisi terlebih dahulu');
// 		return false
// 	}
// });
