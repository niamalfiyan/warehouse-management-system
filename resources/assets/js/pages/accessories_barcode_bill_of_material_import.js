list_bom = JSON.parse($('#bom').val());

$(function()
{
	render();
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() {
	$('#bom').val(JSON.stringify(list_bom));
	
	getIndex();
	var tmpl = $('#bom-upload-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_bom };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-bom').html(html);
}

function getIndex() {
	for (idx in list_bom) {
		list_bom[idx]['_id'] = idx;
		list_bom[idx]['no'] = parseInt(idx) + 1;
	}
}

		
$('#upload_button').on('click', function () {
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () {
	//$('#upload_file_allocation').submit();
	$.ajax({
		cache: false,
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully, Please check again before print.');
			list_bom = response;
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');

			if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT.');
			if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) {
		$('#upload_file_allocation').trigger('reset');
		render();
	});

})

$('#print_out_btn').on('click',function(){
	$('#form_print_out').trigger('submit');

	list_bom = [];
	render();
});


