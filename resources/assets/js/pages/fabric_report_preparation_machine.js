
$(function()
{
  
    var url   = $('#url_data').val();
    $('#fabric_report_preparation_machine_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scrollX:true,
        ajax: {
            type: 'GET',
            url: url,
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" 	: $('#select_warehouse').val(),
                    "start_date" 	: $('#start_date').val(),
                    "end_date" 		: $('#end_date').val(),
                });
            }
        },
        
        columns: [
            {data: 'preparation_date', name: 'preparation_date',searchable:false,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:false,visible:true,orderable:true},
            {data: 'planning_date', name: 'planning_date',searchable:false,visible:true,orderable:true},
            {data: 'machine', name: 'machine',searchable:false,visible:true,orderable:true},
            {data: 'barcode', name: 'barcode',searchable:false,visible:true,orderable:true},
            {data: 'nomor_roll', name: 'nomor_roll',searchable:false,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:false,visible:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:false,visible:true,orderable:true},
            {data: 'style', name: 'style',searchable:false,visible:true,orderable:true},
            {data: 'reserved_qty', name: 'reserved_qty',searchable:false,visible:true,orderable:true},
           
	    ]
    });

    var dtable = $('#fabric_report_preparation_machine_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#select_warehouse').on('change',function(){
        dtable.draw();
        
    });
    
    $('#start_date').on('change',function(){
        dtable.draw();
    });
    
    $('#end_date').on('change',function(){
        dtable.draw();
    });



    $('#export').click(function(){
        var warehouse 	= $('#select_warehouse').val();
        var start_date 	= $('#start_date').val();
        var end_date 	= $('#end_date').val();
        var url			= '/fabric/report/report-preparation-machine/export';
    
        window.location.href = url + '?warehouse=' + warehouse + '&start_date=' + start_date + '&end_date=' + end_date;
    
    
    })
    
})


