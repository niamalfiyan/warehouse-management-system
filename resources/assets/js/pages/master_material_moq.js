
//upload
$('#upload_button').on('click', function () {
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () {
	$.ajax({
		type: "POST",
		url: $('#upload_file_material_moq').attr('action'),
		data: new FormData(document.getElementById("upload_file_material_moq")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'upload berhasil.');
			updateTable(response);
			//$('#dataTableBuilder').DataTable().ajax.reload();
		},
		error: function (response) {
			$.unblockUI()
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');
			if (response['status'] == 422)
				$("#alert_error").trigger("click",response['responseJSON']['message']);
			//$('#dataTableBuilder').DataTable().ajax.reload();
		}
	})
	.done(function (response) {
		$('#upload_file_allocation').trigger('reset');
	});

})

function updateTable(list_data){
	var table=document.getElementById('tbody-upload');
	$('#tbody-upload tr').remove();
	var no=1;
	for(i=0;i<list_data.length;i++){
		row=table.insertRow(i);
		var no=row.insertCell(0);
		var item_code=row.insertCell(1);
		var category=row.insertCell(2);
		var status=row.insertCell(3);

		no.innerHTML=i+1;
		item_code.innerHTML=list_data[i]['item_code'];
		category.innerHTML=list_data[i]['category'];
		status.innerHTML=list_data[i]['status'];
		no++;
	}
}

function edit(url) {
	$.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function (response) {
			$.unblockUI();
		},
	})
	.done(function (data) {
		var url = $('#url_mastermoqupdate').attr('href');

		$('#updateformnya').attr('action', url);
		$('#update_id').val(data['id']);
		$('#update_item_code').val(data['item_code']);
		$('#update_category').val(data['category']);
		$('#updateModal').modal();
	});
}

function hapus(url) {
	bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "DELETE",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data berhasil dihapus.');
				$('#dataTableBuilder').DataTable().ajax.reload();
			});
		}
	});


}

$('#insertformnya').submit(function (event) {
	event.preventDefault();

	var category = $('#category').val();

	if (!category) {
		$("#alert_warning").trigger("click", 'Silahkan masukan kategori terlebih dahulu.');
		return false;
	}

	$('#insertModal').modal('hide');
	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#insertformnya').attr('action'),
				data: $('#insertformnya').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
					$('#insertformnya').trigger("reset");
					$('#insertModal').modal('hide');

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500){
						$("#alert_error").trigger("click", 'Please Contact ICT');
					}else{
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
						}
					}
					$('#insertModal').modal();
				}
			})
			.done(function ($result) {
				$('#dataTableBuilder').DataTable().ajax.reload();
				$("#alert_success").trigger("click", 'Data berhasil disimpan.');

			});
		}else{
			$('#insertModal').modal();
		}
	});
});

$('#updateformnya').submit(function (event) {
	event.preventDefault();
	var category = $('#update_category').val();
	console.log(category);
	if (!category) {
		$("#alert_warning").trigger("click", 'Silahkan masukan kategori terlebih dahulu.');
		return false;
	}

	$('#updateModal').modal('hide');
	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
		if (result) {
			$.ajax({
				type: "PUT",
				url: $('#updateformnya').attr('action'),
				data: $('#updateformnya').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
					$('#updateModal').modal('hide');

				},
				success: function (response) {
					$.unblockUI();
					$('#updateformnya').trigger("reset");
					$('#updateModal').modal('hide');

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please Contact ICT');
					} else {
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
							$('#insertModal').modal();
						}
					}
					$('#updateModal').modal();
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data berhasil diubah.');
				$('#dataTableBuilder').DataTable().ajax.reload();
			});
		} else {
			$('#updateModal').modal();
		}
	});
});
