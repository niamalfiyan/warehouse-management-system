$(function () {
    var page = $('#page').val();

    if (page == 'index') {
        $('#report_mrp_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength: 100,
            scroller: true,
            ordering: false,
            deferRender: true,
            ajax: {
                type: 'GET',
                url: '/report/memo-request-production/data',
                data: function (d) {
                    return $.extend({}, d, {
                        "po_buyer": $('#po_buyerName').val(),
                    });
                }
            },
            columns: [{
                    data: 'season',
                    name: 'season',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'statistical_date',
                    name: 'statistical_date',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'backlog_status',
                    name: 'backlog_status',
                    searchable: false,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'style',
                    name: 'style',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'article_no',
                    name: 'article_no',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'po_buyer',
                    name: 'po_buyer',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'category',
                    name: 'category',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'item_code',
                    name: 'item_code',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'uom',
                    name: 'uom',
                    searchable: true,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'qty_need',
                    name: 'qty_need',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'qty_available',
                    name: 'qty_movement',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'qty_handover_no_received_yet',
                    name: 'qty_handover_no_received_yet',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'qty_supply',
                    name: 'qty_supply',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'document_no',
                    name: 'document_no',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'warehouse',
                    name: 'warehouse',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'last_status_movement',
                    name: 'last_status_movement',
                    searchable: true,
                    visible: true,
                    orderable: true
                },
                {
                    data: 'last_locator_code',
                    name: 'last_locator_code',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'last_movement_date',
                    name: 'last_movement_date',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'last_user_movement_name',
                    name: 'last_user_movement_name',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'remark',
                    name: 'remark',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'system_log',
                    name: 'system_log',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
                {
                    data: 'action',
                    name: 'action',
                    searchable: false,
                    visible: true,
                    orderable: false
                },
            ]
        });

        var dtable = $('#report_mrp_table').dataTable().api();
        $("#report_mrp_table.dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
            });
        dtable.draw();

        buyerLov('po_buyer', '/report/memo-request-production/po-buyer-picklist?');
    } else if (page == 'import') {
        list_closing_mrp = JSON.parse($('#closing-mrp').val());
        render();
    }
})

function render() {
    $('#closing-mrp').val(JSON.stringify(list_closing_mrp));

    var tmpl = $('#closing-mrp-table').html();
    Mustache.parse(tmpl);
    var data = {
        item: list_closing_mrp
    };
    var html = Mustache.render(tmpl, data);
    $('#tbody-upload-closing-mrp').html(html);
}

$('#upload_button').on('click', function () {
    $('#upload_file').trigger('click');
});

// $('#upload_file').on('change', function () {

//     $.ajax({
//             type: "POST",
//             url: $('#upload_file_closing_mrp').attr('action'),
//             data: new FormData(document.getElementById("upload_file_closing_mrp")),
//             processData: false,
//             contentType: false,
//             beforeSend: function () {
//                 $.blockUI({
//                     message: '<i class="icon-spinner4 spinner"></i>',
//                     overlayCSS: {
//                         backgroundColor: '#fff',
//                         opacity: 0.8,
//                         cursor: 'wait'
//                     },
//                     css: {
//                         border: 0,
//                         padding: 0,
//                         backgroundColor: 'transparent'
//                     }
//                 });
//             },
//             complete: function () {
//                 $.unblockUI();
//             },
//             success: function (response) {
//                 $("#alert_info").trigger("click", 'upload berhasil.');
//                 document.location.href = response
//                 //$('#_po_buyer').val(response).trigger('change');
//             },
//             error: function (response) {
//                 $.unblockUI()
//                 $('#upload_file_closing_mrp').trigger('reset');
//                 if (response['status'] == 500)
//                     $("#alert_error").trigger("click", 'Please Contact ICT.');

//             }
//         })
//         .done(function (response) {
//             $('#upload_file_closing_mrp').trigger('reset');
//         });

// })
$('#upload_file').on('change', function () {
    $.ajax({
            type: "post",
            url: $('#upload_file_closing_mrp').attr('action'),
            data: new FormData(document.getElementById("upload_file_closing_mrp")),
            processData: false,
            contentType: false,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                $("#alert_info_2").trigger("click", 'Upload successfully.');

                for (idx in response) {
                    var data = response[idx];
                    var input = {
                        'po_buyer': data.po_buyer,
                        'item_code': data.item_code,
                        'warehouse': data.warehouse,
                        'reason': data.reason,
                        'result': data.result,
                    };
                    list_closing_mrp.push(input);
                }

            },
            error: function (response) {
                $.unblockUI();
                $('#upload_file_closing_mrp').trigger('reset');
                if (response['status'] == 500)
                    $("#alert_error").trigger("click", 'Please Contact ICT.');

                if (response['status'] == 422)
                    $("#alert_error").trigger("click", response.responseJSON);

            }
        })
        .done(function () {
            $('#upload_file_closing_mrp').trigger('reset');
            render();
        });
})

$('#closing_qty').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#closing_qty').change(function () {
    var qty_closing = parseFloat($(this).val());
    var qty_outstanding = parseFloat($('#closing_outstanding_qty').val());

    var _temp = qty_outstanding - qty_closing;

    if (_temp < 0) {
        $("#alert_warning").trigger("click", 'Qty closing must be less than oustanding qty.');
        $(this).val('');
        return false;
    }
});

$('#closingMrpForm').submit(function (event) {
    event.preventDefault();

    var warehouse_closing = $('#warehouse_closing').val();
    var reason = $('#closing_reason').val();
    var closing_qty = $('#closing_qty').val();

    if (!closing_qty) {
        $("#alert_warning").trigger("click", 'Please insert qty closing first.');
        return false;
    }

    if (warehouse_closing == '') {
        $("#alert_warning").trigger("click", 'Please select warehouse first');
        return false;
    }

    if (!reason) {
        $("#alert_warning").trigger("click", 'Please insert reason first');
        return false;
    }

    $('#closingMrpModal').modal('hide');
    bootbox.confirm("Are you sure want to save this data ?.", function (result) {
        if (result) {
            $.ajax({
                    type: "POST",
                    url: $('#closingMrpForm').attr('action'),
                    data: $('#closingMrpForm').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    success: function () {
                        $.unblockUI();
                        $('#closingMrpForm').trigger('reset');
                        $('#closingMrpModal').modal('hide');
                    },
                    error: function (response) {
                        $.unblockUI();
                        if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                        $('#closingMrpModal').modal();
                    }

                })
                .done(function () {
                    $('#report_mrp_table').DataTable().ajax.reload();
                });
        }
    });

});

function buyerLov(name, url) {
    var search = '#' + name + 'Search';
    var item_id = '#' + name + 'Id';
    var item_name = '#' + name + 'Name';
    var modal = '#' + name + 'Modal';
    var table = '#' + name + 'Table';
    var buttonSrc = '#ButtonSrc';
    var buttonDel = '#' + name + 'ButtonDel';

    function itemAjax() {
        var q = $(search).val();

        $(table).addClass('hidden');
        $(modal).find('.shade-screen').removeClass('hidden');
        $(modal).find('.form-search').addClass('hidden');

        $.ajax({
                url: url + '&q=' + q
            })
            .done(function (data) {
                $(table).html(data);
                pagination(name);
                $(search).focus();
                $(table).removeClass('hidden');
                $(modal).find('.shade-screen').addClass('hidden');
                $(modal).find('.form-search').removeClass('hidden');

                $(table).find('.btn-choose').on('click', chooseItem);
            });
    }

    function chooseItem() {
        var id = $(this).data('id');
        var name = $(this).data('name');

        $(item_id).val(id);
        $(item_name).val(name);
        $('#_print_po_buyer').val(name);
        var dtable = $('#report_mrp_table').dataTable().api();
        dtable.draw();
    }

    function pagination() {
        $(modal).find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }

    //$(search).val("");
    $(buttonSrc).unbind();
    $(search).unbind();

    $(buttonSrc).on('click', itemAjax);

    $(search).on('keypress', function (e) {
        if (e.keyCode == 13)
            itemAjax();
    });

    $(buttonDel).on('click', function () {
        $(item_id).val('');
        $(item_name).val('');

    });

    itemAjax();
}

function closeMrp(array) {
    var split = array.split('|');
    var po_buyer = split[0];
    var item_code = split[1];
    var style = split[2];
    var article_no = split[3];
    var qty_need = parseFloat(split[4]);
    var qty_available = parseFloat(split[5]);
    var qty_supply = parseFloat(split[6]);
    var uom = split[7];
    var backlog_status = split[8];
    var _qty = parseFloat(qty_available + qty_supply);
    var outstanding_qty = qty_need - _qty;

    if (outstanding_qty <= 0) {
        $("#alert_info").trigger("click", 'This material already closed.');
        return false;
    }

    $('#closing_uom').val(uom);
    $('#closing_need_qty').val(qty_need);
    $('#closing_po_buyer').val(po_buyer);
    $('#closing_style').val(style);
    $('#closing_article').val(article_no);
    $('#closing_outstanding_qty').val(outstanding_qty);
    $('#closing_backlog_status').val(backlog_status);
    $('#closing_item_code').val(item_code);

    $('#update_closing_mrp').text(po_buyer + ' ' + item_code + ' ' + style + ' ' + article_no);
    $('#closingMrpModal').modal({
        backdrop: 'static',
        keyboard: false // to prevent closing with Esc button (if you want this too)
    });



}

function history(po_buyer, item_code, article_no, style) {
    $('#history_po_buyer').val(po_buyer);
    $('#history_item_code').val(item_code);
    $('#history_article_no').val(article_no);
    $('#history_style').val(style).trigger('change');

    $('#header_history_mrp').text(po_buyer + ' ' + item_code + ' ' + style + ' ' + article_no);
    $('#historyModal').modal();
    $('#report_memo_request_production_history_tabel').DataTable().destroy();
    $('#report_memo_request_production_history_tabel tbody').empty();

    $('#report_memo_request_production_history_tabel').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        ajax: {
            type: 'GET',
            url: '/report/memo-request-production/history-data',
            data: function (d) {
                return $.extend({}, d, {
                    "po_buyer": $('#history_po_buyer').val(),
                    "item_code": $('#history_item_code').val(),
                    "article_no": $('#history_article_no').val(),
                    "style": $('#history_style').val(),
                });
            }
        },
        columns: [{
                data: 'barcode',
                name: 'barcode',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'document_no',
                name: 'document_no',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'supplier_name',
                name: 'supplier_name',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'uom_conversion',
                name: 'uom_conversion',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'qty_conversion',
                name: 'qty_conversion',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'warehouse',
                name: 'warehouse',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_status_movement',
                name: 'last_status_movement',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_movement_date',
                name: 'last_movement_date',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_user_movement_id',
                name: 'last_user_movement_id',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_locator_id',
                name: 'last_locator_id',
                searchable: false,
                visible: true,
                orderable: true
            },
        ]
    });

    var dtableHistory = $('#report_memo_request_production_history_tabel').dataTable().api();
    $("#report_memo_request_production_history_tabel.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtableHistory.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtableHistory.search("").draw();
            }
            return;
        });
    }

    function showDetail(po_buyer, item_code, article_no, style)
    {
    $('#detail_handover_po_buyer').val(po_buyer);
    $('#detail_handover_item_code').val(item_code);
    $('#detail_handover_article_no').val(article_no);
    $('#detail_handover_style').val(style).trigger('change');

    $('#header_detail_handover_mrp').text(po_buyer + ' ' + item_code + ' ' + style + ' ' + article_no);
    $('#detailHandoverModal').modal();
    $('#report_memo_request_production_detail_handover_tabel').DataTable().destroy();
    $('#report_memo_request_production_detail_handover_tabel tbody').empty();

    $('#report_memo_request_production_detail_handover_tabel').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        ajax: {
            type: 'GET',
            url: '/report/memo-request-production/detail-handover-data',
            data: function (d) {
                return $.extend({}, d, {
                    "po_buyer": $('#detail_handover_po_buyer').val(),
                    "item_code": $('#detail_handover_item_code').val(),
                    "article_no": $('#detail_handover_article_no').val(),
                    "style": $('#detail_handover_style').val(),
                });
            }
        },
        columns: [{
                data: 'barcode',
                name: 'barcode',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'document_no',
                name: 'document_no',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'supplier_name',
                name: 'supplier_name',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'uom_conversion',
                name: 'uom_conversion',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'qty_conversion',
                name: 'qty_conversion',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'warehouse',
                name: 'warehouse',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_status_movement',
                name: 'last_status_movement',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_movement_date',
                name: 'last_movement_date',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'last_user_movement_id',
                name: 'last_user_movement_id',
                searchable: false,
                visible: true,
                orderable: true
            },
        ]
    });

    var dtableDetailHandover = $('#report_memo_request_production_detail_handover_tabel').dataTable().api();
    $("#report_memo_request_production_detail_handover_tabel.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtableDetailHandover.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtableDetailHandover.search("").draw();
            }
            return;
        });

}