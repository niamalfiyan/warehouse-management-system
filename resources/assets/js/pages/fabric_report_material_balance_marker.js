$(function()
{
	POSupplierLov('document_no', '/fabric/report/material-balance-marker/get-po-supplier-picklist?');
	
    $('#fabric_report_material_balance_marker').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-balance-marker/data',

            data: function(d) {
                return $.extend({}, d, {
					"start_date": $('#start_date').val(),
					"end_date": $('#end_date').val(),
					"factory_id": $('#factory_id').val(),
                });
           },
        },
        columns: [
			{data: 'factory_id', name: 'factory_id',searchable:true,visible:true,orderable:false},
            {data: 'cutting_date', name: 'cutting_date',searchable:true,visible:true,orderable:false},
            {data: 'factory_id', name: 'factory_id',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'articleno', name: 'articleno',searchable:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,orderable:true},
            {data: 'item_code_book', name: 'item_code_book',searchable:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,orderable:true},
            {data: 'actual_marker_prod', name: 'actual_marker_prod',searchable:true,orderable:true},
            {data: 'supply_whs', name: 'supply_whs',searchable:true,orderable:true},
            {data: 'balance', name: 'balance',searchable:true,orderable:true},
            {data: 'status', name: 'status',searchable:true,orderable:true},
            {data: 'persen_prepare', name: 'persen_prepare',searchable:true,orderable:true},
            {data: 'remark', name: 'remark',searchable:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ],
    });

    var dtable = $('#fabric_report_material_balance_marker').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();
    
    $('#start_date').on('change',function(){
        dtable.draw();
    });
	
    $('#end_date').on('change',function(){
        dtable.draw();
    });

	
    $('#factory_id').on('change',function(){
        dtable.draw();
    });
	
});
function changeFormatDate()
{
    var tanggal_input = $('#cutting_date').val();
    var tanggal_array = tanggal_input.split('/');
    var cutting_date  = tanggal_array[2]+'-'+tanggal_array[1]+'-'+tanggal_array[0];
    return cutting_date;
}

function confirm(url) 
{
    $.ajax({
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
		error: function (response) {
			$.unblockUI();
			
			if(response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT');
			
			if(response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);
				
		}
    })
    .done(function (data) {
        var url_store_balance_marker = $('#url_store_balance_marker').val();
        $('#confirmStore').attr('action', url_store_balance_marker);
        $('#marker_release_detail_id').val(data['marker_release_detail_id']);
        $('#warehouse_id').val(data['id']);
        $('#po_buyer').val(data['po_buyer']);
        $('#item_id_source').val(data['item_id']);
        $('#item_id_book').val(data['item_id_book']);
        $('#item_code').val(data['item_code']);
        $('#item_code_book').val(data['item_code_book']);
        $('#qty_allocation').val(data['balance']);
        $('#remark').val(data['remark']);
        $('#planning_date').val(data['cutting_date']);
        $('#style').val(data['style']);
        $('#article_no').val(data['articleno']);
        $('#lc_date').val(data['lc_date']);
        $('#promise_date').val(data['promise_date']);
        $('#confirmModal').modal();
    });
}


function confirmSave(url) 
{
    $.ajax({
        url: url,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function (response) {
            $.unblockUI();
        },
    })
    .done(function (data) {
		//alert('done')
        var url_store_saving = $('#url_store_saving').val();
        $('#confirmStoreSaving').attr('action', url_store_saving);
        $('#_marker_release_detail_id').val(data['marker_release_detail_id']);
        $('#_warehouse_id').val(data['factory_id']);
        $('#_po_buyer').val(data['po_buyer']);
        $('#_item_id_source').val(data['item_id']);
        $('#_item_id_book').val(data['item_id_book']);
        $('#_item_code').val(data['item_code']);
        $('#_item_code_book').val(data['item_code_book']);
        $('#_qty_allocation').val(data['balance']);
        $('#_remark').val(data['remark']);
        $('#_planning_date').val(data['cutting_date']);
        $('#_style').val(data['style']);
        $('#_article_no').val(data['articleno']);
        $('#_lc_date').val(data['lc_date']);
		$('#_promise_date').val(data['promise_date']);
		$.ajax({
			type: "GET",
			url: '/fabric/report/material-balance-marker/list-po-supplier-saving',
			data: { planning_date : data['planning_date'], style : data['style'], article_no : data['articleno'], warehouse_id : data['factory_id']},
			success: function (data) {
				console.log(data);
				var list_document_no = data.list_document_no;
				$("#select_document_no").empty();
					$.each(list_document_no,function(id,name){
					$("#select_document_no").append('<option value="'+id+'">'+name+'</option>');
				});
			}
		});

        $('#confirmSavingModal').modal();
    });
}

$('#select_document_no').on('change',function()
{
	var style 			= $('#_style').val();
	var warehouse_id 		= $('#_warehouse_id').val();
	var article_no 				= $('#_article_no').val();    
	var planning_date 		= $('#_planning_date').val();  
	var c_order_id 		= $('#select_document_no').val();  

	$.ajax({
		type   : "GET",
		url    : '/fabric/report/material-balance-marker/list-nomor-roll-saving',
		data   : { planning_date : planning_date, 
				   style         : style,
				   article_no    : article_no,
				   warehouse_id  : warehouse_id,
				   c_order_id   : c_order_id,
		},
		success     : function (data) {
			console.log(data);
			var list_nomor_roll = data.list_nomor_roll;
			$("#select_nomor_roll").empty();
				$.each(list_nomor_roll,function(id,name){
				$("#select_nomor_roll").append('<option value="'+id+'">'+name+'</option>');
			});
		}
	});

});

function POSupplierLov(name, url) 
{
	var search = '#' + name + 'Search';
	var list = '#' + name + 'List';
	var item_id = '#' + name + 'Id';
	var item_name = '#' + name + 'Name';
	var modal = '#' + name + 'Modal';
	var table = '#' + name + 'Table';
	var buttonSrc = '#' + name + 'ButtonSrc';
	var buttonDel = '#' + name + 'ButtonDel';

	function itemAjax() {
		var q = $(search).val();
		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&q=' + q
		})
		.done(function (data) {
			$(table).html(data);
			pagination(name);
			$(search).focus();
			//$(search).val('');
			$(table).removeClass('hidden');
			$(modal).find('.shade-screen').addClass('hidden');
			$(modal).find('.form-search').removeClass('hidden');

			$(table).find('.btn-choose').on('click', chooseItem);
		});
	}

	function chooseItem() {
		var id = $(this).data('id');
		var name = $(this).data('name');
		var supplier = $(this).data('supplier');
		var bpartner = $(this).data('bpartner');
		var typestockerp = $(this).data('typestockerp');
		var typestock = $(this).data('typestock');
		
		$(item_id).val(id);
		$(item_name).val(name);
		$('#supplier_name').val(supplier);
		$('#c_bpartner_id').val(bpartner);
		$('#type_stock').val(typestock);
		$('#type_stock_erp').val(typestockerp);
		


	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

$('#confirmStore').submit(function (event) {
	$('#confirmModal').modal('hide');
	event.preventDefault();
	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#confirmStore').attr('action'),
				data: $('#confirmStore').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {
                    $('#confirmStore').trigger('reset');
					$("#alert_success").trigger("click", 'Data successfully updated');
					$('#fabric_report_material_balance_marker').DataTable().ajax.reload();

				},
				error: function (response) {
					$.unblockUI();
					
					if(response['status'] == 500)
						$("#alert_error").trigger("click", 'Please Contact ICT');
					
					if(response['status'] == 422)
						$("#alert_error").trigger("click", response.responseJSON);
						
				}
			});
		}
	});
	
});

$('#confirmStoreSaving').submit(function (event) {
	$('#confirmSavingModal').modal('hide');
	event.preventDefault();
	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#confirmStoreSaving').attr('action'),
				data: $('#confirmStoreSaving').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				done: function (response) {
					$.unblockUI();
					$('#materials').val('[]');
					list_materials = [];
					render();

					$("#alert_success").trigger("click", 'Data successfully saved.');

					var arrStr = encodeURIComponent(JSON.stringify(response));
					window.open('/fabric/report/material-balance-marker/show-barcode?id=' + arrStr);
					$('#confirmStoreSaving').trigger('reset');
					$("#alert_success").trigger("click", 'Data successfully updated');
					//document.location.href = url_fabric_material_stock;
				},
				error: function (response) {
					$.unblockUI();
					
					if(response['status'] == 500)
						$("#alert_error").trigger("click", 'Please Contact ICT');
					
					if(response['status'] == 422)
						$("#alert_error").trigger("click", response.responseJSON);
						
				}
			});
		}
	});
	
});