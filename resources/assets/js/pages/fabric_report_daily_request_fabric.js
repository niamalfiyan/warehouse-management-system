$(function () {
	var active_tab = $('#page').val();

	if (active_tab == 'index') {
		$('#fabric_report_daily_request_fabric_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength: 100,
			scroller: true,
			deferRender: true,
			ajax: {
				type: 'GET',
				url: '/fabric/report/daily-request-fabric/data',
				data: function (d) {
					return $.extend({}, d, {
						"warehouse": $('#select_warehouse').val(),
						"start_date": $('#start_date').val(),
						"end_date": $('#end_date').val(),
					});
				}
			},
			columns: [{
					data: 'marker_release_detail_id',
					name: 'marker_release_detail_id',
					searchable: true,
					visible: false,
					orderable: false
				},
				{
					data: 'factory_id',
					name: 'factory_id',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'queu',
					name: 'queu',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'cutting_date',
					name: 'cutting_date',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'articleno',
					name: 'articleno',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'style',
					name: 'style',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'po_buyer',
					name: 'po_buyer',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'item_code',
					name: 'item_code',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'part_no',
					name: 'part_no',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'supply_whs',
					name: 'supply_whs',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'actual_marker_prod',
					name: 'actual_marker_prod',
					searchable: false,
					visible: true,
					orderable: false
				},
				
				{
					data: 'balance',
					name: 'balance',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'confirm_by',
					name: 'confirm_by',
					searchable: false,
					visible: true,
					orderable: false
				},
				{
					data: 'action',
					name: 'action',
					searchable: false,
					visible: true,
					orderable: false
				},
			]
		});

		var dtable = $('#fabric_report_daily_request_fabric_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();

		$('#select_warehouse').on('change', function () {
			dtable.draw();
		});

		$('#start_date').on('change', function () {
			dtable.draw();
		});

		$('#end_date').on('change', function () {
			dtable.draw();
		});
	} else {
		var url_fabric_report_daily_cutting_instruction_detail = $('#url_fabric_report_daily_cutting_instruction_detail').val();

		$('#fabric_report_daily_cutting_instruction_detail_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength: 100,
			scroller: true,
			deferRender: true,
			ajax: {
				type: 'GET',
				url: url_fabric_report_daily_cutting_instruction_detail,
				data: function (d) {
					return $.extend({}, d, {
						"style": $('#style').val(),
						"article_no": $('#article_no').val(),
						"warehouse_id": $('#warehouse_id').val(),
						"planning_date": $('#planning_date').val(),
					});
				}

			},
			columns: [
				{data: 'checkbox', name: 'checkbox',searchable:false,orderable:false},
				{
					data: 'TANGGAL_PREPARE',
					name: 'TANGGAL_PREPARE',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'PIC',
					name: 'PIC',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_movement_date',
					name: 'last_movement_date',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_user_movement',
					name: 'last_user_movement',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'last_status_movement',
					name: 'last_status_movement',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'user_receive_cutting_id',
					name: 'user_receive_cutting_id',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'date_receive_cutting',
					name: 'date_receive_cutting',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'piping',
					name: 'piping',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'NO_PO_SUPPLIER',
					name: 'NO_PO_SUPPLIER',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'KODE_ITEM',
					name: 'KODE_ITEM',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'WARNA',
					name: 'WARNA',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'NOMOR_ROLL',
					name: 'NOMOR_ROLL',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'barcode',
					name: 'barcode',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'locator',
					name: 'locator',
					searchable: true,
					visible: true,
					orderable: true
				},
				{
					data: 'AKTUAL_LOT',
					name: 'AKTUAL_LOT',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'TOP_WIDTH',
					name: 'TOP_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'MIDDLE_WIDTH',
					name: 'MIDDLE_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'BOTTOM_WIDTH',
					name: 'BOTTOM_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'AKTUAL_WIDTH',
					name: 'AKTUAL_WIDTH',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'QTY_RESERVED',
					name: 'QTY_RESERVED',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'ci_approval_status',
					name: 'ci_approval_status',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'ci_approval_date',
					name: 'ci_approval_date',
					searchable: false,
					visible: true,
					orderable: true
				},
				{
					data: 'ci_approval_user_id',
					name: 'ci_approval_user_id',
					searchable: false,
					visible: true,
					orderable: true
				},
			]
		});

		var dtable = $('#fabric_report_daily_cutting_instruction_detail_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
			});
		dtable.draw();
	}

});

function confirmWhs(url) 
{
    bootbox.confirm("Are you sure want to approve this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "get",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully updated.');
				$('#fabric_report_daily_request_fabric_table').DataTable().ajax.reload();
			});
		}
	});
}

function confirmLab(url) 
{
    bootbox.confirm("Are you sure want to approve this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "get",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully updated.');
				$('#fabric_report_daily_request_fabric_table').DataTable().ajax.reload();
			});
		}
	});
}