$(function () 
{
    $('#report_mrp_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength: 100,
        scroller: true,
        deferRender: true,
        ajax: {
            type: 'GET',
            url: '/memo-request-production/data',
            data: function (d) {
                return $.extend({}, d, {
                    "po_buyer": $('#po_buyerName').val(),
                });
            }
        },
        columns: [
            {
                data: 'season',
                name: 'season',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'statistical_date',
                name: 'statistical_date',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'backlog_status',
                name: 'backlog_status',
                searchable: false,
                visible: true,
                orderable: true
            },
            {
                data: 'style',
                name: 'style',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'article_no',
                name: 'article_no',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'po_buyer',
                name: 'po_buyer',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'category',
                name: 'category',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'item_code',
                name: 'item_code',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'uom',
                name: 'uom',
                searchable: true,
                visible: true,
                orderable: false
            },
            {
                data: 'qty_need',
                name: 'qty_need',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'qty_available',
                name: 'qty_movement',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'qty_supply',
                name: 'qty_supply',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'document_no',
                name: 'document_no',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'warehouse',
                name: 'warehouse',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'last_status_movement',
                name: 'last_status_movement',
                searchable: true,
                visible: true,
                orderable: true
            },
            {
                data: 'last_locator_code',
                name: 'last_locator_code',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'last_movement_date',
                name: 'last_movement_date',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'last_user_movement_name',
                name: 'last_user_movement_name',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'remark',
                name: 'remark',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'system_log',
                name: 'system_log',
                searchable: false,
                visible: true,
                orderable: false
            },
            {
                data: 'action',
                name: 'action',
                searchable: false,
                visible: true,
                orderable: false
            },
        ]
    });

    var dtable = $('#report_mrp_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
        });
    dtable.draw();

    buyerLov('po_buyer', '/memo-request-production/po-buyer-picklist?');

    function buyerLov(name, url) {
        var search = '#' + name + 'Search';
        var item_id = '#' + name + 'Id';
        var item_name = '#' + name + 'Name';
        var modal = '#' + name + 'Modal';
        var table = '#' + name + 'Table';
        var buttonSrc = '#ButtonSrc';
        var buttonDel = '#' + name + 'ButtonDel';

        function itemAjax() {
            var q = $(search).val();

            $(table).addClass('hidden');
            $(modal).find('.shade-screen').removeClass('hidden');
            $(modal).find('.form-search').addClass('hidden');

            $.ajax({
                    url: url + '&q=' + q
                })
                .done(function (data) {
                    $(table).html(data);
                    pagination(name);
                    $(search).focus();
                    $(table).removeClass('hidden');
                    $(modal).find('.shade-screen').addClass('hidden');
                    $(modal).find('.form-search').removeClass('hidden');

                    $(table).find('.btn-choose').on('click', chooseItem);
                });
        }

        function chooseItem() {
            var id = $(this).data('id');
            var name = $(this).data('name');

            $(item_id).val(id);
            $(item_name).val(name);
            $('#_print_po_buyer').val(name);
            dtable.draw();
        }

        function pagination() {
            $(modal).find('.pagination a').on('click', function (e) {
                var params = $(this).attr('href').split('?')[1];
                url = $(this).attr('href') + (params == undefined ? '?' : '');

                e.preventDefault();
                itemAjax();
            });
        }

        //$(search).val("");
        $(buttonSrc).unbind();
        $(search).unbind();

        $(buttonSrc).on('click', itemAjax);

        $(search).on('keypress', function (e) {
            if (e.keyCode == 13)
                itemAjax();
        });

        $(buttonDel).on('click', function () {
            $(item_id).val('');
            $(item_name).val('');

        });

        itemAjax();
    }
})

function history(po_buyer,item_code,article_no,style)
{
    $('#history_po_buyer').val(po_buyer);
    $('#history_item_code').val(item_code);
    $('#history_article_no').val(article_no);
    $('#history_style').val(style).trigger('change');

    $('#header_history_mrp').text(po_buyer + ' ' + item_code + ' ' + style + ' ' + article_no);
    $('#historyModal').modal();
    $('#report_memo_request_production_history_tabel').DataTable().destroy();
    $('#report_memo_request_production_history_tabel tbody').empty();
    
    $('#report_memo_request_production_history_tabel').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/memo-request-production/history-data',
            data: function (d) 
            {
                return $.extend({}, d, {
                    "po_buyer"  : $('#history_po_buyer').val(),
                    "item_code" : $('#history_item_code').val(),
                    "article_no": $('#history_article_no').val(),
                    "style"     : $('#history_style').val(),
                });
            }
        },
        columns: [
            {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'uom_conversion', name: 'uom_conversion',searchable:false,visible:true,orderable:true},
            {data: 'qty_conversion', name: 'qty_conversion',searchable:true,visible:true,orderable:true},
            {data: 'warehouse', name: 'warehouse',searchable:false,visible:true,orderable:true},
            {data: 'last_status_movement', name: 'last_status_movement',searchable:false,visible:true,orderable:true},
            {data: 'last_movement_date', name: 'last_movement_date',searchable:false,visible:true,orderable:true},
            {data: 'last_user_movement_id', name: 'last_user_movement_id',searchable:false,visible:true,orderable:true},
            {data: 'last_locator_id', name: 'last_locator_id',searchable:false,visible:true,orderable:true},
        ]
    });

    var dtableHistory = $('#report_memo_request_production_history_tabel').dataTable().api();
    $("#report_memo_request_production_history_tabel.dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtableHistory.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtableHistory.search("").draw();
            }
            return;
    });
    
}