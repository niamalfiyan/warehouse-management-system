$(function()
{
    $('#fabric_report_outstanding_material_out').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/outstanding-material-out/data',

            data: function(d) {
                return $.extend({}, d, {
                    "planning_date"     : changeFormatDate(),
                    "is_additional"     : $('#select_type').val(),
                });
           },
        },
        columns: [
            {data: 'planning_date', name: 'planning_date',searchable:true,visible:true,orderable:false},
            {data: 'warehouse_id', name: 'warehoouse_id',searchable:true,orderable:true},
            {data: 'is_additional', name: 'is_additional',searchable:true,orderable:true},
            {data: 'style', name: 'style',searchable:true,orderable:true},
            {data: 'article_no', name: 'article_no',searchable:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,orderable:true},
            {data: 'barcode', name: 'barcode',searchable:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,orderable:true},
            {data: 'item_code_source', name: 'item_code_source',searchable:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:true,orderable:true},
            {data: 'qty_prepare', name: 'qty_prepare',searchable:true,orderable:true},
            {data: 'preparation_date', name: 'preparation_date',searchable:true,orderable:false},
        ],
    });

    var dtable = $('#fabric_report_outstanding_material_out').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });

    dtable.draw();
    
    $('#planning_date').on('change',function(){
        dtable.draw();
    });

    $('#select_type').on('change',function(){
        dtable.draw();
    });

});

function changeFormatDate()
{
    var tanggal_input = $('#planning_date').val();
    var tanggal_array = tanggal_input.split('/');
    var planning_date  = tanggal_array[2]+'-'+tanggal_array[1]+'-'+tanggal_array[0];
    return planning_date;
}
