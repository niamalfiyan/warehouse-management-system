$(function()
{
    $('#daily_material_monitoring_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/fabric/report/material-monitoring-fabric/data',
            data: function(d) {
                return $.extend({}, d, {
                    "start_date"            : $('#start_date').val(),
                    "end_date"              : $('#end_date').val(),
                    "start_statistical_date": $('#start_statistical_date').val(),
                    "end_statistical_date"  : $('#end_statistical_date').val(),
                });
           }
        },

        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'lc_date', name: 'lc_date',searchable:true,visible:true,orderable:true},
            {data: 'statistical_date', name: 'statistical_date',searchable:true,visible:true,orderable:true},
            {data: 'promise_date', name: 'promise_date',searchable:true,visible:true,orderable:true},
            {data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: '_style', name: '_style',searchable:true,visible:true,orderable:true},
            {data: 'brand', name: 'brand',searchable:true,visible:true,orderable:true},
            {data: 'status_po_buyer', name: 'status_po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'qty_allocation', name: 'qty_allocation',searchable:true,visible:true,orderable:true},
            {data: 'qty_in_house', name: 'qty_in_house',searchable:false,visible:true,orderable:true},
            {data: 'qty_on_ship', name: 'qty_on_ship',searchable:false,visible:true,orderable:true},
            {data: 'qty_open', name: 'qty_open',searchable:false,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:true},
            {data: 'mrd', name: 'mrd',searchable:false,visible:true,orderable:false},
            {data: 'dd_pi', name: 'dd_pi',searchable:false,visible:true,orderable:false},
            {data: 'etd_date', name: 'etd_date',searchable:false,visible:true,orderable:false},
            {data: 'eta_date', name: 'eta_date',searchable:false,visible:true,orderable:false},
            {data: 'etd_actual', name: 'etd_actual',searchable:false,visible:true,orderable:false},
            {data: 'eta_actual', name: 'eta_actual',searchable:false,visible:true,orderable:false},
            {data: 'eta_delay', name: 'eta_delay',searchable:false,visible:true,orderable:false},
            {data: 'eta_max', name: 'eta_max',searchable:false,visible:true,orderable:false},
            {data: 'no_invoice', name: 'no_invoice',searchable:false,visible:true,orderable:false},
            {data: 'receive_date', name: 'eta_actual',searchable:false,visible:true,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:false},
            {data: 'remark_update', name: 'remark_update',searchable:false,visible:true,orderable:false},
            {data: 'remark_delay', name: 'remark_delay',searchable:false,visible:true,orderable:false},
            {data: 'status', name: 'status',searchable:true,visible:true,orderable:false},
        ]
    });

    var dtable = $('#daily_material_monitoring_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();

    $('#start_date').on('change',function(){
        var lc_date_from = $('#start_date').val();
        $('#lc_date_from').val(lc_date_from);
    });

    $('#end_date').on('change',function(){
        var lc_date_to = $('#end_date').val();
        $('#lc_date_to').val(lc_date_to);
    });

    $('#start_statistical_date').on('change',function(){
        var start_statistical_date = $('#start_statistical_date').val();
        $('#_start_statistical_date').val(start_statistical_date);
    });

    $('#end_statistical_date').on('change',function(){
        var end_statistical_date = $('#end_statistical_date').val();
        $('#_end_statistical_date').val(end_statistical_date);
    });

    $('#get_monitoring_material').submit(function (event)
	{
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        var start_statistical_date = $('#start_statistical_date').val();
        var end_statistical_date = $('#end_statistical_date').val();

		// if (!start_date || !end_date) {
        //     $("#alert_warning").trigger("click", 'Salah Satu LC Date Tidak Boleh Kosong');
        //     return false;
        // }
        // else{
            
        //     if (!start_statistical_date || !end_statistical_date) {
        //         $("#alert_warning").trigger("click", 'Salah Satu Statistical Date Tidak Boleh Kosong');
        //         return false;
        //     }
            
        // }

        
		event.preventDefault();
		$.ajax({
			type: "get",
			url: $('#get_monitoring_material').attr('action'),
			data: $('#get_monitoring_material').serialize(),
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) 
			{
				
				if (response.total == 0)
				{
					$.unblockUI();
					return false;
				}
				
				var dtable = $('#daily_material_monitoring_table').dataTable().api();
				dtable.draw();
				$.unblockUI();
			},
			error: function (response) 
			{
				$.unblockUI();

				if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
				if (response['status'] == 400) $("#alert_warning").trigger("click", 'Please check planning cutting date first');
			}
		});
	});



})