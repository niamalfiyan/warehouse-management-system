$(function()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'approved')
	{
		approveDataTables();

		var dtableApproved = $('#approvedTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableApproved.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableApproved.search("").draw();
				}
				return;
		});
		dtableApproved.draw();
		$('#select_warehouse').on('change',function(){
            dtableApproved.draw();
        });

	}else if( active_tab == 'waiting')
	{
		waitingforApprovedDataTables();

		var dtableWaiting = $('#waitingTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableWaiting.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableWaiting.search("").draw();
				}
				return;
		});
		dtableWaiting.draw();
		$('#select_warehouse').on('change',function(){
            dtableWaiting.draw();
		});
		
		$('#select_user_created').on('change',function(){
            dtableWaiting.draw();
        });
	} 

	
});

$('#active_tab').on('change',function()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'approved')
	{
		approveDataTables();

		var dtableApproved = $('#approvedTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableApproved.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableApproved.search("").draw();
				}
				return;
		});
		dtableApproved.draw();

		$('#select_warehouse').on('change',function(){
			dtableApproved.draw();
		});

		
	}else if( active_tab == 'waiting')
	{
		waitingforApprovedDataTables();

		var dtableWaiting = $('#waitingTable').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtableWaiting.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtableWaiting.search("").draw();
				}
				return;
		});
		dtableWaiting.draw();
		$('#select_warehouse').on('change',function(){
			dtableWaiting.draw();
		});
		
		$('#select_user_created').on('change',function(){
			dtableWaiting.draw();
		});
	} 
});

$('#approval_all_from').submit(function (event) 
{
	event.preventDefault();
	bootbox.confirm("Are you sure want to approve all data ?", function (result) 
	{
		if (result) 
		{
			$.ajax({
				type: "get",
				url: $('#approval_all_from').attr('action'),
				data: $('#approval_all_from').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {

					$("#alert_success").trigger("click", 'Data successfully approved.');
					$('#approval_all_from').trigger('reset');
					$('#waitingTable').DataTable().ajax.reload();
					
				},
				error: function () {
					$.unblockUI();
					$("#alert_error").trigger("click", 'Please contact ICT.');
				}
			});
		}
	});
});


function waitingforApprovedDataTables()
{
	$('#waitingTable').DataTable().destroy();
	$('#waitingTable tbody').empty();
	
	$('#waitingTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/accessories/allocation/approval/data-waiting-for-approved',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "user" : $('#select_user_created').val(),
                });
            }
        },
        columns: [
			{data: 'id', name: 'id',visible:false},
			{data: 'created_at', name: 'created_at',searchable:false},
			{data: 'warehouse', name: 'warehouse'},
			{data: 'confirm_date', name: 'confirm_date',searchable:false},
			{data: 'user_id', name: 'user_id'},
			{data: 'type_stock_material', name: 'type_stock_material'},
			{data: 'supplier_code', name: 'supplier_code'},
			{data: 'supplier_name', name: 'supplier_name'},
			{data: 'document_no', name: 'document_no'},
			{data: 'item_code', name: 'item_code'},
			{data: 'category', name: 'category'},
			{data: 'type_stock_buyer', name: 'type_stock_buyer'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'old_po_buyer_reroute', name: 'old_po_buyer_reroute'},
			{data: 'uom', name: 'uom'},
			{data: 'style', name: 'style'},
			{data: 'qty_booking', name: 'qty_booking',orderable:false,searchable:false},
			{data: 'is_additional', name: 'is_additional',orderable:false,searchable:false},
			{data: 'confirm_by_warehouse', name: 'confirm_by_warehouse',searchable:false},
			{data: 'barcode_status', name: 'barcode_status',searchable:false,orderable:false},
			{data: 'remark', name: 'remark'},
			{data: 'action', name: 'action',searchable:false,orderable:false},
		],
		order: [[1, 'desc']]
    });
}

function approveDataTables()
{
	$('#approvedTable').DataTable().destroy();
	$('#approvedTable tbody').empty();
	
	$('#approvedTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/accessories/allocation/approval/data-approved',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val()
                });
            }
        },
        columns: [
			{data: 'id', name: 'id',visible:false},
			{data: 'created_at', name: 'created_at',searchable:false},
			{data: 'warehouse', name: 'warehouse'},
			{data: 'confirm_date', name: 'confirm_date',searchable:false},
			{data: 'user_id', name: 'user_id'},
			{data: 'type_stock_material', name: 'type_stock_material'},
			{data: 'supplier_code', name: 'supplier_code'},
			{data: 'supplier_name', name: 'supplier_name'},
			{data: 'document_no', name: 'document_no'},
			{data: 'item_code', name: 'item_code'},
			{data: 'category', name: 'category'},
			{data: 'type_stock_buyer', name: 'type_stock_buyer'},
			{data: 'po_buyer', name: 'po_buyer'},
			{data: 'old_po_buyer_reroute', name: 'old_po_buyer_reroute'},
			{data: 'uom', name: 'uom'},
			{data: 'style', name: 'style'},
			{data: 'qty_booking', name: 'qty_booking',orderable:false,searchable:false},
			{data: 'is_additional', name: 'is_additional',orderable:false,searchable:false},
			{data: 'confirm_by_warehouse', name: 'confirm_by_warehouse',searchable:false},
			{data: 'barcode_status', name: 'barcode_status',searchable:false,orderable:false},
			{data: 'remark', name: 'remark'},
			{data: 'action', name: 'action',searchable:false,orderable:false},
		],
		order: [[1, 'desc']]
    });
}

function changeTab(status)
{
	$('#active_tab').val(status).trigger('change');
}

function approve(url) 
{
	bootbox.confirm("Are you sure want to approve ?.", function (result) 
	{
		if (result) 
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "put",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
			})
			.done(function () 
			{
				$("#alert_success").trigger("click", 'Data successfully approved.');
				$('#waitingTable').DataTable().ajax.reload();
			});
		}
	});
}

function cancel(url) 
{
	swal({
		title: "Are you sure want to cancel this allocation ?",
		text: "Please type reason :",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Please type reason here"
	},
	function (inputValue) 
	{
		if (inputValue === false) return false;
		if (inputValue === "") {
			swal.showInputError("Bro / Sist, you don\'t write reason, please write it first.");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "delete",
			url: url,
			data: {
				'cancel_reason': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422)
					$("#alert_error").trigger("click", response['responseJSON']);
			}
		})
		.done(function ($result) {
			$("#alert_success").trigger("click", 'Data successfully cancel.');
			$('#waitingTable').DataTable().ajax.reload();
		});
	});


}

function reject(url) 
{
	bootbox.confirm("Are you sure want to reject this ?.", function (result) 
	{
		if (result) 
		{
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "put",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#waitingTable').DataTable().ajax.reload();
			});
		}
	});


}





