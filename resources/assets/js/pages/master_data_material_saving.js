$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        $('#master_data_material_saving_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/master-data/material-saving/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"             : $('#select_warehouse').val(),
                        "start_date"            : $('#start_date').val(),
                        "end_date"              : $('#end_date').val(),
                    });
               }
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'planning_date', name: 'planning_date',searchable:false,visible:true,orderable:false},
                {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:false},
                {data: 'upload_user_id', name: 'upload_user_id',searchable:false,visible:true,orderable:false},
                {data: 'po_supplier', name: 'po_supplier',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:false,visible:true,orderable:false},
                {data: 'style', name: 'style',searchable:true,visible:true,orderable:true},
                {data: 'article_no', name: 'article_no',searchable:true,visible:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
                {data: 'qty_saving', name: 'qty_saving',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#master_data_material_saving_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function()
        {
            var warehouse_id = $(this).val();
            $('#_warehouse_id').val(warehouse_id);
            dtable.draw();
        });
    
        $('#start_date').on('change',function()
        {
            var start_date = $(this).val();
            $('#_start_date').val(start_date);
            dtable.draw();
        });

        $('#end_date').on('change',function()
        {
            var end_date = $(this).val();
           $('#_end_date').val(end_date);
            dtable.draw();
        });
        
    }else
    {
        list_material_saving = JSON.parse($('#material-saving').val());
        render();
    }
})




function render() 
{
    $('#material-saving').val(JSON.stringify(list_material_saving));

    var tmpl = $('#material-saving-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_material_saving };
    var html = Mustache.render(tmpl, data);
    $('#tbody-upload-material-saving').html(html);
}

$('#upload_button').on('click', function () 
{
    $('#upload_file').trigger('click');
});


$('#upload_file').on('change', function () 
{
    //$('#upload_file_material_saving').submit();
    $.ajax({
        type: "post",
        url: $('#upload_file_material_saving').attr('action'),
        data: new FormData(document.getElementById("upload_file_material_saving")),
        processData: false,
        contentType: false,
        beforeSend: function () {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        complete: function () {
            $.unblockUI();
        },
        success: function (response) {
            $("#alert_info").trigger("click", 'Upload successfully.');
            for (idx in response) 
            {
                var data = response[idx];
                var input = {
                    'pcd'           : data.pcd,
                    'style'         : data.style,
                    'article'       : data.article,
                    'uom'           : data.uom,
                    'item_code'     : data.item_code,
                    'po_supplier'   : data.po_supplier,
                    'qty_saving'    : data.qty_saving,
                    'warehouse'     : data.warehouse,
                    'is_error'      : data.is_error,
                    'status'        : data.status,
                };
                
                list_material_saving.push(input);
            }

        },
        error: function (response) 
        {
            $.unblockUI();
            $('#upload_file_material_saving').trigger('reset');
            if (response['status'] == 500)  $("#alert_error").trigger("click", 'Please Contact ICT.');
            if (response['status'] == 422) $("#alert_error").trigger("click", response.responseJSON);

        }
    })
    .done(function () {
        $('#upload_file_material_saving').trigger('reset');
        render();
    });

})