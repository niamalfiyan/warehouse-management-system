// const { size } = require("lodash");

$("#testing_methods").select2({

    placeholder: "Silahkan Pilih Testing Methods"

});
$('#row_po_mo').hide();
$('#row_barcode_fabric').hide();
$('#row_supplier_name').hide();
$('#row_document_no').hide();
$('#row_nomor_roll').hide();
$('#row_batch').hide();
$('#row_item').hide();
$('#row_item_code').hide();
$('#row_size').hide();
$('#row_style').hide();
$('#row_article').hide();
$('#row_color').hide();
$('#row_destination').hide();
$('#row_composition').hide();



$(function()
{
   
    var flag_msg = $('#flag_msg').val();
	if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
	else if (flag_msg == 'success_2') $("#alert_success").trigger("click", 'Data successfully updated.');
    
    
    $('#master_data_fabric_testing_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        deferRender:true,
        scrollX:true,
        ajax: {
            type: 'GET',
            url: '/master-data/fabric-testing/data',
        },
        
        columns: [
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'test_required', name: 'test_required',searchable:false,visible:true,orderable:true},
            {data: 'date_information_remark', name: 'date_information_remark',searchable:false,visible:true,orderable:true},
            {data: 'buyer', name: 'buyer',searchable:false,visible:true,orderable:true},
            {data: 'lab_location', name: 'lab_location',searchable:false,visible:true,orderable:true},
            {data: 'trf_id', name: 'trf_id',searchable:true,visible:true,orderable:true},
            {data: 'nik', name: 'nik',searchable:false,visible:true,orderable:true},
            {data: 'orderno', name: 'orderno',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:false,visible:true,orderable:true},
            {data: 'category_specimen', name: 'category_specimen',searchable:false,visible:true,orderable:true},
            {data: 'type_specimen', name: 'type_specimen',searchable:false,visible:true,orderable:true},
            {data: 'return_test_sample', name: 'return_test_sample',searchable:false,visible:true,orderable:true},
            {data: 'status', name: 'last_status',searchable:true,visible:true,orderable:true},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
	    ]
    });

    var dtable = $('#master_data_fabric_testing_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
  
    $('#buyer').on('change', function () {  
        var url_get_requirement = $('#url_get_requirement').val();
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
        

	    $.ajax({
	        type: 'get',
	        url :  url_get_requirement,
	        data : {buyer:$('#buyer').val()},
	        // beforeSend:function(){
	        // 	loading();
	        // },
	        success: function(response) {
				var data = response.response;

	        	$('#category_specimen').empty();
                $('#category_specimen').append('<option>== Select Category Specimen ==</option>');
              
				for (var i = 0; i < data.length; i++) {
				$('#category_specimen').append('<option value="'+data[i]['category_specimen']+'">'+data[i]['category_specimen']+'</option>')
				}
	         	
	        },
	        error: function(response) {
	           $.unblockUI();
		       alert(response.status,response.responseText);
	            
	        }
	    });
    });

    $('#category_specimen').on('change', function () {  
        var url_categoryspecimen = $('#url_categoryspecimen').val();
        if($('#category_specimen').val() == 'FABRIC')
        {
            $('#row_po_mo').hide();
            $('#row_barcode_fabric').show();
            $('#row_supplier_name').show();
            $('#row_nomor_roll').show();
            $('#row_batch').show();
            $('#row_item_code').show();
            $('#row_item').hide();
            $('#row_size').hide();
            $('#row_style').hide();
            $('#row_article').hide();
            $('#row_color').show();
            $('#row_destination').hide();
            $('#row_document_no').show();
            $('#row_composition').show();
            
        }else
        {
            $('#row_po_mo').show();
            $('#row_barcode_fabric').hide();
            $('#row_supplier_name').hide();
            $('#row_document_no').hide();
            $('#row_nomor_roll').hide();
            $('#row_batch').hide();
            $('#row_item_code').hide();
            $('#row_item').show();
            $('#row_size').show();
            $('#row_style').show();
            $('#row_article').show();
            $('#row_color').show();
            $('#row_destination').show();
            $('#row_composition').show();
            
        }
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
        

	    $.ajax({
	        type: 'get',
	        url :  url_categoryspecimen,
	        data : {category_specimen:$('#category_specimen').val()},
	        // beforeSend:function(){
	        // 	loading();
	        // },
	        success: function(response) {
				console.log(response);
				var data = response.response;

	        	$('#category').empty();
                $('#category').append('<option>== Select Category ==</option>');

				for (var i = 0; i < data.length; i++) {
                $('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+' - '+data[i]['category']+'</option>');
				
                $('#testing_methods').empty();
                $('#testing_methods').append('<option value="'+data[i]['id']+'">'+data[i]['method_code']+' - '+data[i]['method_name']+'</option>');
				
				}
              
	         	
	        },
	        error: function(response) {
	           $.unblockUI();
		       alert(response.status,response.responseText);
	            
	        }
	    });
    });

    $('#type_specimen').on('change', function (event) {
        event.preventDefault();
        var url_typespecimen = $('#url_typespecimen').attr('href');
        // console.log(url_typespecimen);
		$.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
        
        // console.log($('#type_specimen').val());
	    $.ajax({
	        type: 'get',
	        url :  url_typespecimen,
	        data : {category_specimen   :$('#category_specimen').val(),
                    type_specimen       :$('#type_specimen').val()},
	        success: function(response) {
				var data = response.response;
                $('#category').empty();
                $('#category').append('<option>== Select Category ==</option>');

				for (var i = 0; i < data.length; i++) {
                $('#category').append('<option value="'+data[i]['category']+'">'+data[i]['category']+' - '+data[i]['category']+'</option>');
				
                $('#testing_methods').empty();
                $('#testing_methods').append('<option value="'+data[i]['id']+'">'+data[i]['method_code']+' - '+data[i]['method_name']+'</option>');
				
				}

               
	         	
	        },
	        error: function(response) {
	           $.unblockUI();
		       alert(response.status,response.responseText);
	            
	        }
	    });

     
    });



    $('#po_mo').on('change', function () {  
        var asal_specimen   = 'PRODUCTION';
        // console.log(asal_specimen);
        
            var po_mo           = $('#po_mo').val();
            var url_getpo       = $('#url_getpo').val();
            var asal_specimen   = $('#asal_specimen').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
    
            $.ajax({
                type: 'get',
                url :  url_getpo,
                data : {po_mo:po_mo, asal_specimen:asal_specimen},
                // beforeSend:function(){
                // 	loading();
                // },
                success: function(response) {
                    // console.log(response);
                    var data = response.response;
                    console.log(data);
                    $('#item').empty();
                    for (var i = 0; i < data.length; i++) {
                    $('#item').append('<option value="'+data[i]['item_code']+'">'+data[i]['item_code']+'</option>')
                    $('#po_mo').val(data[i]['po']);
                    }
                   
                    // alert(notif.status,notif.output);
                    // print(notif.idpr);
                    // $.unblockUI();
                    // $('#modal_daftar').modal('hide');
                     
                },
                error: function(response) {
                   $.unblockUI();
                   alert(response.status,response.responseText);
                    
                }
            });
        
       

     
    });

    
    $('#item').on('change', function () {  
        var po_mo   = $('#po_mo').val();
        var item    = $('#item').val();
        var asal_specimen   = 'PRODUCTION';
        if(po_mo == null && item == null)
        {
          $("#alert_error").trigger("click", 'Silahkan isi PO/MO dan Size.');
          return false;
        }else{
            var urlsize       = $('#urlsize').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
    
            $.ajax({
                type: 'get',
                url :  urlsize,
                data : {po_mo:po_mo, item_code:item, asal_specimen},
                // beforeSend:function(){
                // 	loading();
                // },
                success: function(response) {
                    // console.log(response);
                    var data = response.response;
                    console.log(data);

                    $('#size').empty();
                    for (var i = 0; i < data.length; i++) {
                    $('#size').append('<option value="'+data[i]['size']+'">'+data[i]['size']+'</option>')
                    $('#style').val(data[i]['style']);
                    $('#article').val(data[i]['article']);
                    $('#color').val(data[i]['color']);;
                    $('#destination').val(data[i]['country']);
                    $('#composition').val(data[i]['description']);
                    }
                     
                },
                error: function(response) {
                   $.unblockUI();
                   alert(response.status,response.responseText);
                    
                }
            });
        }
       

     
    });

    $('#size').on('change', function () {  
        var po_mo   = $('#po_mo').val();
        var item    = $('#item').val();
        var Size    = $('#size').val();
        var asal_specimen   = 'PRODUCTION';
        if(po_mo == null && item == null)
        {
          $("#alert_error").trigger("click", 'Silahkan isi PO/MO dan Size.');
          return false;
        }else{
            var urlstyle       = $('#url_style').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
    
            $.ajax({
                type: 'get',
                url :  urlstyle,
                data : {po_mo:po_mo, item_code:item, asal_specimen, size:Size},
                // beforeSend:function(){
                // 	loading();
                // },
                success: function(response) {
                    // console.log(response);
                    var data = response.response;
                    console.log(data);
                    $('#style').val(data['style']);
                    $('#article').val(data['article']);
                    $('#color').val(data['color']);
                    $('#destination').val(data['country']);
                    $('#composition').val(data[i]['description']);
                    
                     
                },
                error: function(response) {
                   $.unblockUI();
                   alert(response.status,response.responseText);
                    
                }
            });
        }
       

     
    });


    $('#barcode_fabric').on('change', function () {  
        var barcode_fabric   = $('#barcode_fabric').val();
        var urlbarcode_fabric = $('#urlbarcode_fabric').val();
       
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
    
            $.ajax({
                type: 'get',
                url :  urlbarcode_fabric,
                data : {barcode_fabric:barcode_fabric},
                // beforeSend:function(){
                // 	loading();
                // },
                success: function(response) {
                    console.log(response);
                    if(response.response == null)
                    {
                        $("#alert_error").trigger("click", 'Barcode not found.');
                    }else{
                        var data = response.response;
                        $('#document_no').val(data['document_no']);
                        $('#supplier_name').val(data['supplier_name']);
                        $('#item_code').val(data['item_code']);
                        $('#nomor_roll').val(data['nomor_roll']);
                        $('#batch').val(data['batch_number']);
                        $('#color').val(data['color']);
                        $('#composition').val(data['composition']);
                        
                    }

                   
                     
                },
                error: function(response) {
                console.log(response);
                   $.unblockUI();
                   $("#alert_error").trigger("click", 'Barcode not found.');
                    
                }
            });
        
       

     
    });


    
})

function hapus(url) 
{
    bootbox.confirm("Are you sure want to delete this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "delete",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			}).done(function ($result) {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#master_data_fabric_testing_table').DataTable().ajax.reload();
			});
		}
	});
}



$('#form-create').submit(function () 
{
  event.preventDefault();
  var buyer             = $('#buyer').val();
  var asal_specimen     = $('#asal_specimen').val(); 
  var category_specimen = $('#category_specimen').val();
  var category          = $('#category').val();
  var factory           = $('#factory').val();
  
  var test_required     = $("input[type='radio'][name='test_required']:checked").val();
  var no_trf = null;
  var radio_date        = $("input[type='radio'][name='radio_date']:checked").val()
  var part_akan_dites   = $('#part_akan_dites').val();
  var po_mo             = $('#po_mo').val();
  var size              = $('#size').val();
  var style             = $('#style').val();
  var article           = $('#article').val();
  var color             = $('#color').val();
  var return_sample     = $('#return_sample').val();
  var other_buyer       = null;
  var testing_date      = $('#testing_date').val();
  var item              = $('#item').val();
  var destination       = $('#destination').val();
  var barcode_fabric    = $('#barcode_fabric').val();
  var document_no       = $('#document_no').val();
  var supplier_name     = $('#supplier_name').val();
  var item_code         = $('#item_code').val();
  var nomor_roll        = $('#nomor_roll').val();
  var batch             = $('#batch').val();
  var composition       = $('#composition').val();
//   console.log(testing_methods);

  if(buyer == "other")
  {
    var get_otherbuyer       = $('#other_buyer').val();
    other_buyer = get_otherbuyer;

  }

  if(category_specimen == "== Select Category Specimen ==" || category_specimen == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi category specimen.');
    return false;
  }

  if(item_code == "== Select Item ==" || item_code == null)
  {
    $("#alert_error").trigger("click", 'Silahkan pilih Item .');
    return false;
  }

  if(item_code == "== Select Item ==" || item_code == null)
  {
    $("#alert_error").trigger("click", 'Silahkan pilih Item .');
    return false;
  }


  if(category == "== Select Category ==" || category == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi category.');
    return false;
  }


  if(return_sample == "== Choose return Sample ==" || return_sample == null)
  {
    $("#alert_error").trigger("click", 'Silahkan pilih return sample.');
    return false;
  }
  
  

  if(testing_date == null)
  {
    $("#alert_error").trigger("click", 'Silahkan isi tanggal testing.');
    return false;
  }
  console.log(test_required);

  if(test_required == "retest" && no_trf == null)
  {
      
      var trf = $('#no_trf').val();
      no_trf = trf;
      $("#alert_error").trigger("click", 'Silahkan isi no trf sebelumnya.');
		return false;
  }
  var url_save = $('#url_save').val();

  

  bootbox.confirm("Are you sure want to save this data ?.", function (result) 
  {
      if (result) {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
          });

          $.ajax({
              type: 'POST',
              url: url_save,
              data: {
                  buyer                 :buyer,
                  asal_specimen         :asal_specimen,
                  category_specimen     :category_specimen,
                  category              :category,
                  factory               :factory,
                  test_required         :test_required,
                  no_trf                :no_trf,
                  radio_date            :radio_date,
                  part_akan_dites       :part_akan_dites,
                  po_mo                 :po_mo,
                  size                  :size,
                  style                 :style,
                  article               :article,
                  color                 :color,
                  return_sample         :return_sample,
                  other_buyer           :other_buyer,
                  testing_date          :testing_date,
                  item                  :item,
                  destination           :destination,
                  barcode_fabric        :barcode_fabric,
                  document_no           :document_no,
                  supplier_name         :supplier_name,
                  item_code             :item_code,
                  nomor_roll            :nomor_roll,
                  batch                 :batch,
                  composition           :composition


              },
              beforeSend: function () 
              {
                  $.blockUI({
                      message: '<i class="icon-spinner4 spinner"></i>',
                      overlayCSS: {
                          backgroundColor: '#fff',
                          opacity: 0.8,
                          cursor: 'wait'
                      },
                      css: {
                          border: 0,
                          padding: 0,
                          backgroundColor: 'transparent'
                      }
                  });
              },
            success: function (response) {
            var url = $('#url_master_data_fabric_testing_index').attr('href'); 
            document.location.href = url;
                $.unblockUI();
            },
            error: function(response) {
            console.log(response);
                $.unblockUI();
                console.log(response);
                $("#alert_error").trigger("click", response);
                
            },
            }).done(function ($result) {
                $.unblockUI();
                $("#alert_success").trigger("click", 'Data successfully saved.');
                $('#master_data_fabric_testing_table').DataTable().ajax.reload();
            });
            
      }
  });


});
