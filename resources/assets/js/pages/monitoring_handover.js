$('#start_date').on('change',submitFilter);
$('#end_date').on('change',submitFilter);

function submitFilter(){
    $('#form_filter').submit();
}
$(function(){
    var dtable = $('#handover-datatable').dataTable().api();
    dtable.page.len(100);
    dtable.draw();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
})