list_material_adjustment = JSON.parse($('#material_adjustments').val());

$(function () {
    setFocusToTextBox();
    render();

    $('#form').submit(function (event) {
        event.preventDefault();

        bootbox.confirm("Are you sure want to save this data ?.", function (result) {
            if (result) {
                $.ajax({
                    type: "POST",
                    url: $('#form').attr('action'),
                    data: $('#form').serialize(),
                    beforeSend: function () {
                        $.blockUI({
                            message: '<i class="icon-spinner4 spinner"></i>',
                            overlayCSS: {
                                backgroundColor: '#fff',
                                opacity: 0.8,
                                cursor: 'wait'
                            },
                            css: {
                                border: 0,
                                padding: 0,
                                backgroundColor: 'transparent'
                            }
                        });
                    },
                    complete: function () {
                        $.unblockUI();
                    },
                    success: function (response) {
                        $("#alert_success").trigger("click", 'Data successfully adjust.');
                    },
                    error: function (response) {
                        $.unblockUI();
                        $('#barcode_value').val('');
                        $('.barcode_value').focus();

                        if (response['status'] == 500)
                            $("#alert_error").trigger("click", 'Please Contact ICT');

                        if (response['status'] == 422)
                            $("#alert_error").trigger("click", response.responseJSON);

                        if (response['status'] == 504)
                            $("#alert_error").trigger("click", 'request timeout');

                    }
                })
                .done(function (response) {
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();
                    list_material_adjustment = [];
                    render();

                    
                    var arrStr = encodeURIComponent(JSON.stringify(response['data_print']));
                    if (arrStr != 'false') window.open('/accessories/barcode/adjustment/printout?id=' + arrStr);

                });
            }
        });
    });
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
});

function render() {
    getIndex();
    $('#material_adjustments').val(JSON.stringify(list_material_adjustment));
    var tmpl = $('#material-adjustment-table').html();
    Mustache.parse(tmpl);
    var data = { item: list_material_adjustment };
    var html = Mustache.render(tmpl, data);
    $('#tbody-material-adjustment').html(html);
    $('.barcode_value').focus();
    bind();
}

function bind() {
    $('#barcode_value').on('change', tambahItem);
    var delay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    $('.btn-delete-item').on('click', deleteItem);
    /*$("#barcode_value").on("input", function () {
        delay(function () {
            if ($("#barcode_value").val().length < 12) {
                $("#barcode_value").val("");
            }
        }, 20);
    });*/
}

function deleteItem() {
    var i = $(this).data('id');
    list_material_adjustment.splice(i, 1);
    render();
}

function getIndex() {
    for (idx in list_material_adjustment) {
        list_material_adjustment[idx]['_idx'] = idx;
        list_material_adjustment[idx]['nox'] = parseInt(idx) + 1;
    }
}

function checkItem(barcode) {
    for (var i in list_material_adjustment) {
        var data = list_material_adjustment[i];
        var barcodes = data.barcodes;
        for (var id in barcodes){
            var barcode_adj = barcodes[id];
            if (barcode_adj == barcode) {
                return false;
            }
        }
        

    }

    return true;
}

function AdjIsExists(po_buyer, item_code, style,auto_allocation_id)
{
    for (var i in list_material_adjustment) {
        var data = list_material_adjustment[i];

        if (data.po_buyer == po_buyer 
            && data.item_code == item_code 
            && data.style == style
            && data.auto_allocation_id == auto_allocation_id ) 
        {
            return false;
        }

    }

    return true;
}

function updateListAdj(response)
{
    for (var i in list_material_adjustment) {
        var data = list_material_adjustment[i];

        if (data.po_buyer == response.po_buyer 
            && data.item_code == response.item_code 
            && data.style == response.style
            && data.auto_allocation_id == response.auto_allocation_id
        )
        {
            var curr_qty_before_adj = parseFloat(data.qty_before_adj);
            var curr_qty_need = parseFloat(data.qty_need);
            var reponse_qty_before_adj = parseFloat(response.qty_before_adj);

            var new_qty_before_adj = curr_qty_before_adj + reponse_qty_before_adj;
            var new_qty_adj = new_qty_before_adj - curr_qty_need;
            var new_qty_after_adj = new_qty_before_adj - parseFloat(new_qty_adj);

            data.qty_before_adj = new_qty_before_adj;
            data.qty_adj = new_qty_adj;
            data.qty_after_adj = new_qty_after_adj;
            data.barcodes.push(response.barcode);
       
        }

    }
}

function tambahItem() 
{
    var warehouse_id 		= $('#select_warehouse').val();
    var barcode_val         = $('#barcode_value').val();
    var preparation_url     = $('#url_preperation_picklist').attr('href');
    var diff                = checkItem(barcode_val);

    if (!diff) {
        $("#barcode_value").val("");
        $(".barcode_value").focus();
        $("#alert_warning").trigger("click", 'Barcode already scanned.');
        return;
    } 
    
        $.ajax({
            type: "GET",
            url: preparation_url,
            data: {
                barcode         : barcode_val,
                warehouse_id    : warehouse_id
            },
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            complete: function () {
                $.unblockUI();
            },
            success: function (response) {
                /*if (!response.is_allow) {
                    $("#alert_warning").trigger("click", 'Item ini menghasilkan qty 0 ketika di adjust.');
                    $('#barcode_value').val('');
                    $('.barcode_value').focus();
                    return false;
                }*/

                var po_buyer            = response.po_buyer;
                var item_code           = response.item_code;
                var style               = response.style;
                var auto_allocation_id  = response.auto_allocation_id;
                var is_already_exists   = AdjIsExists(po_buyer, item_code, style,auto_allocation_id);

                if (!is_already_exists)
                {
                    updateListAdj(response);
                }else
                {
                    response.barcodes.push(response.barcode);
                    list_material_adjustment.push(response);
                }
            },
            error: function (response) {
                $.unblockUI();
                $('#barcode_value').val('');
                $('.barcode_value').focus();

                if (response['status'] == 500) $("#alert_error").trigger("click", 'Please Contact ICT');
                if (response['status'] == 422) $("#alert_warning").trigger("click", response.responseJSON);

                list_material_adjustment = [];
                render();
            }
        })
        .done(function () {
            
            $('#barcode_value').val('');
            $('.barcode_value').focus();
            render();
        });;
    


}

function setFocusToTextBox() {
    $('#barcode_value').focus();
}