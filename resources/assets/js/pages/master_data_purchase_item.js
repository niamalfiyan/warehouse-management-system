
$(function () 
{
	$('#master_data_purchase_order_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/master-data/purchase-item/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#select_warehouse').val(),
                    "month"    	: $('#select_month').val(),
                    "year"      : $('#select_year').val(),
                });
           }
        },
        columns: [
            {data: 'promise_date', name: 'promise_date',searchable:false,visible:true,orderable:true},
            {data: 'status_po_buyer', name: 'status_po_buyer',searchable:false,visible:true,orderable:true},
            {data: 'warehouse', name: 'warehouse',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
			{data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:false},
            {data: 'qty_order', name: 'qty_order',searchable:false,visible:true,orderable:false},
            {data: 'qty', name: 'qty',searchable:false,visible:true,orderable:false},
		]
    });

    var dtable = $('#master_data_purchase_order_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#select_month').on('change',function(){
        dtable.draw();
    });

    $('#select_year').on('change',function(){
        dtable.draw();
    });
});