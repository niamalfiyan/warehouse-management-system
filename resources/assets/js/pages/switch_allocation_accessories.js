list_allocations = JSON.parse($('#allocations').val());

$('#status').on('change',function(){
	$('#form-status').submit();
});
function hapus(url) {
	bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "get",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			})
				.done(function ($result) {
					$("#alert_success").trigger("click", 'Data berhasil dihapus.');
					$('#switch-accessories-datatables').DataTable().ajax.reload();
				});
		}
	});


}

function edit(url) {
	$.ajax({
		url: url,
		beforeSend: function () {
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		success: function () {
			$.unblockUI();
		},
		error: function (response) {
			$.unblockUI();
			if (response['status'] == 500) {
				$("#alert_error").trigger("click", 'Please Contact ICT');
			}
			if (response['status'] == 422) {
				$("#alert_error").trigger("click", response['responseJSON']);
			}
		}
		
	})
	.done(function (data) {
		var url = $('#url_switch_edit').attr('href');

		if (data.warehouse_source_id == '1000011') var warehouse_inventory_source = 'AOI-2';
		else if (data.warehouse_source_id == '1000001') var warehouse_inventory_source = 'AOI-1';

		if (data.warehouse_id == '1000011') var warehouse_inventory_new = 'AOI-2';
		else if (data.warehouse_id == '1000001') var warehouse_inventory_new = 'AOI-1';


		
		$('#updateformnya').attr('action', url);
		$('#update_id').val(data.id);
		$('#po_supplier_source').val(data.document_no_source);
		$('#po_supplier_new').val(data.document_no_new);
		$('#item_code').val(data.item_code);
		$('#po_buyer').val(data.po_buyer);
		$('#qty_switch').val(data.qty_switch);
		$('#warehouse_inventory_source').val(warehouse_inventory_source);
		$('#warehouse_inventory_new').val(warehouse_inventory_new);
		$('#updateModal').modal();
	});
}

$('#approvalall_from').submit(function (event) {
	event.preventDefault();
	
	bootbox.confirm("Apakah anda yakin akan menyimpan data alokasi ini ?", function (result) {
		if (result) {
			$.ajax({
				type: "get",
				url: $('#approvalall_from').attr('action'),
				data: $('#approvalall_from').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				complete: function () {
					$.unblockUI();
				},
				success: function () {

					$("#alert_success").trigger("click", 'data alokasi berhasil disimpan');
					$('#approvalall_from').trigger('reset');

					$('#dataTableBuilder').DataTable().ajax.reload();
					
				},
				error: function () {
					$.unblockUI();
					$("#alert_error").trigger("click", 'Please Contact ICT');
				}
			});
		}
	});
});

function approve(url) {
	bootbox.confirm("Apakah anda yakin akan menerima data ini ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "put",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data berhasil diterima.');
				$('#dataTableBuilder').DataTable().ajax.reload();
			});
		}
	});


}

function cancel(url) {
	swal({
		title: "Apakah anda yakin akan membatalkan bookingan ini ?",
		text: "Tulisankan alasan anda:",
		type: "input",
		showCancelButton: true,
		confirmButtonColor: "#2196F3",
		closeOnConfirm: false,
		animation: "slide-from-top",
		inputPlaceholder: "Silahkan tuliskan alasan anda"
	},
	function (inputValue) {
		if (inputValue === false) return false;
		if (inputValue === "") {
			swal.showInputError("Bro / Sist, ente belum menuliskan alasannya, tulis dlu laah !");
			return false
		}
		
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$.ajax({
			type: "put",
			url: url,
			data: {
				'cancel_reason': inputValue
			},
			beforeSend: function () {
				swal.close();
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			success: function () {
				$.unblockUI();
			},
			error: function (response) {
				$.unblockUI();
				if (response['status'] == 422)
					$("#alert_info_2").trigger("click", response['responseJSON']);
			}
		})
		.done(function ($result) {
			$("#alert_success").trigger("click", 'Data berhasil dicancel.');
			$('#dataTableBuilder').DataTable().ajax.reload();
		});
	});


}

function reject(url) {
	bootbox.confirm("Apakah anda yakin akan menghapus data ini ?.", function (result) {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "put",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
				},
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data berhasil ditolak.');
				$("#alert_success").trigger("click", 'Data berhasil ditolak.');
				$('#dataTableBuilder').DataTable().ajax.reload();
			});
		}
	});


}


$(function(){
	var dtable = $('#switch-accessories-datatables').dataTable().api();
	dtable.page.len(100);
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
		});
	dtable.draw();

	function render() {
		getIndex();
		$('#allocations').val(JSON.stringify(list_allocations));

		var tmpl = $('#allocation-accessories-table').html();
		Mustache.parse(tmpl);
		var data = { item: list_allocations };
		var html = Mustache.render(tmpl, data);
		$('#tbody-upload-allocation-accessories').html(html);
	}

	

	function getIndex() {
		for (idx in list_allocations) {
			list_allocations[idx]['_id'] = idx;
			list_allocations[idx]['no'] = parseInt(idx) + 1;
		}
	}

	$('#upload_button').on('click', function () {
		$('#upload_file').trigger('click');
	});

	$('#upload_file').on('change', function () {
		//$('#upload_file_allocation').submit();
		$.ajax({
			type: "post",
			url: $('#upload_file_allocation').attr('action'),
			data: new FormData(document.getElementById("upload_file_allocation")),
			processData: false,
			contentType: false,
			beforeSend: function () {
				$.blockUI({
					message: '<i class="icon-spinner4 spinner"></i>',
					overlayCSS: {
						backgroundColor: '#fff',
						opacity: 0.8,
						cursor: 'wait'
					},
					css: {
						border: 0,
						padding: 0,
						backgroundColor: 'transparent'
					}
				});
			},
			complete: function () {
				$.unblockUI();
			},
			success: function (response) {
				$("#alert_info_2").trigger("click", 'upload berhasil, silahkan cek kembali.');
				
				for (idx in response) {
					var data = response[idx];
					list_allocations.push(data);
				}
				
			},
			error: function (response) {
				$.unblockUI();
				$('#upload_file_allocation').trigger('reset');
				if (response['status'] == 500)
					$("#alert_error").trigger("click", 'Please Contact ICT.');

				if (response['status'] == 422)
					$("#alert_error").trigger("click", response.responseJSON);

			}
		})
		.done(function () {
			$('#upload_file_allocation').trigger('reset');
			render();
		});

	})

	render();

	
});

$('#updateformnya').submit(function (event) {
	event.preventDefault();
	var po_supplier_source = $('#po_supplier_source').val();
	var po_supplier_new = $('#po_supplier_new').val();
	var item_code = $('#item_code').val();
	var po_buyer = $('#item_code').val();
	var qty_switch = $('#qty_switch').val();
	var warehouse_inventory_source = $('#warehouse_inventory_source').val();
	var warehouse_inventory_new = $('#warehouse_inventory_new').val();

	if (!po_supplier_source) {
		$("#alert_warning").trigger("click", 'Silahkan masukan PO SUPPLIER ASAL terlebih dahulu.');
		return false;
	}

	if (!po_supplier_new) {
		$("#alert_warning").trigger("click", 'Silahkan masukan PO SUPPLIER BARU terlebih dahulu.');
		return false;
	}

	if (!item_code) {
		$("#alert_warning").trigger("click", 'Silahkan masukan KODE ITEM terlebih dahulu.');
		return false;
	}

	if (!po_buyer) {
		$("#alert_warning").trigger("click", 'Silahkan masukan PO BUYER terlebih dahulu.');
		return false;
	}

	if (!qty_switch) {
		$("#alert_warning").trigger("click", 'Silahkan masukan QTY SWITCH terlebih dahulu.');
		return false;
	}

	/*if (!warehouse_inventory_source) {
		$("#alert_warning").trigger("click", 'Silahkan masukan WAREHOUSE ASAL terlebih dahulu.');
		return false;
	}

	if (!warehouse_inventory_new) {
		$("#alert_warning").trigger("click", 'Silahkan masukan WAREHOUSE BARU terlebih dahulu.');
		return false;
	}*/

	$('#updateModal').modal('hide');
	bootbox.confirm("Apakah anda yakin akan menyimpan data ini ?.", function (result) {
		if (result) {
			$.ajax({
				type: "POST",
				url: $('#updateformnya').attr('action'),
				data: $('#updateformnya').serialize(),
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
					$('#updateModal').modal('hide');

				},
				success: function (response) {
					$.unblockUI();
					$('#updateformnya').trigger("reset");
					$('#updateModal').modal('hide');

				},
				error: function (response) {
					$.unblockUI();
					if (response['status'] == 500) {
						$("#alert_error").trigger("click", 'Please Contact ICT');
					} else {
						for (i in response.responseJSON) {
							$("#alert_error").trigger("click", response.responseJSON[i]);
							$('#insertModal').modal();
						}
					}
					$('#updateModal').modal();
				}
			})
			.done(function ($result) {
				$("#alert_success").trigger("click", 'Data berhasil diubah.');
				$('#switch-accessories-datatables').DataTable().ajax.reload();
			});
		} else {
			$('#updateModal').modal();
		}
	});
});


