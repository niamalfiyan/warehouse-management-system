$(function()
{
	$('#allocationNonBuyerTable').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
			url: '/accessories/allocation/non-buyer/data',
			data: function(d) {
                return $.extend({}, d, {
                    "warehouse" : $('#export_warehouse').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:false,orderable:false,visible:true},
            {data: 'user_id', name: 'user_id',searchable:false,orderable:false,visible:true},
            {data: 'supplier_code', name: 'supplier_code',searchable:true,orderable:true,visible:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,orderable:true,visible:true},
            {data: 'document_no', name: 'document_no',searchable:true,orderable:true,visible:true},
            {data: 'item_code', name: 'item_code',searchable:true,orderable:true,visible:true},
            {data: 'category', name: 'category',searchable:true,orderable:true,visible:true},
            {data: 'uom', name: 'uom',searchable:true,orderable:true,visible:true},
           	{data: 'qty_booking', name: 'qty_booking',searchable:false,orderable:false,visible:true},
			{data: 'remark', name: 'remark',searchable:true,orderable:false,visible:true},
			{data: 'action', name: 'action',searchable:false,orderable:false,visible:true},
	    ]
	});
	
	var dtable = $('#allocationNonBuyerTable').dataTable().api();
	dtable.page.len(100);
	$(".dataTables_filter input")
		.unbind() // Unbind previous default bindings
		.bind("keyup", function (e) { // Bind our desired behavior
			// If the user pressed ENTER, search
			if (e.keyCode == 13) {
				// Call the API search function
				dtable.search(this.value).draw();
			}
			// Ensure we clear the search if they backspace far enough
			if (this.value == "") {
				dtable.search("").draw();
			}
			return;
		});
	dtable.draw();

	$('#form').submit(function (event) 
	{
		event.preventDefault();
		var item_code 		= $('#item_allocationId').val();
		var remark 			= $('#remark').val();
		var qty_stock 		= $('#_temp_qty').val();
		var qty_book 		= $('#_qty_booking').val();

		if (!item_code) 
		{
			$("#alert_warning").trigger("click", 'Please select item code first.');
			return false;
		}

		if (!remark) 
		{
			$("#alert_warning").trigger("click", 'Please select remark first');
			return false;
		}

		if (parseInt(qty_book) > parseInt(qty_stock)) 
		{
			$("#alert_warning").trigger("click", 'Qty booking must be less than available qty.');
			return false;
		}

		

		bootbox.confirm("Are you sure want to save this ?", function (result) 
		{
			if (result) 
			{
				$.ajax({
					type: "POST",
					url: $('#form').attr('action'),
					data: $('#form').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						
						$("#alert_success").trigger("click", 'Data successfully saved.');
						$('#allocationNonBuyerTable').DataTable().ajax.reload();
						$('#document_no').addClass('hidden');
						$('#category').addClass('hidden');
						$('#uom').addClass('hidden');
						$('#qty_stock').addClass('hidden');
						$('#__document_no').val('');
						$('#__uom').val('');
						$('#__category').val('');
						$('#_temp_qty').val('');
						$('#item_allocationName').val('');
						$('#item_allocationId').val('');
						$('#___item_code').val('');
						$('#___locator').val('');
						$('#qty_booking').val('');
						$('#remark').val('');
						
					},
					error: function (response) 
					{
						$.unblockUI();
						if (response['status'] == 400) 
						{
							for (i in response['responseJSON']['errors']) 
							{
								if (i == 'po_buyer_allocation_id'){
									$('#po_buyer_allocation_error').addClass('has-error');
									$('#po_buyer_allocation_danger').text('Please Select Item Code First');
								}
							}
						} else if (response['status'] == 422) 
						{
							$("#alert_warning").trigger("click", response['responseJSON']);
						} else if (response['status'] == 500) 
						{
							$("#alert_error").trigger("click", 'Please Contact ICT');
						}
					}
				});
			}
		});
	});
	
});

$('#select_warehouse').on('change',function(){
	$('#warehouse_id').val($(this).val());
	$('#export_warehouse').val($(this).val());
	$('#export_warehouse').trigger('change');
});

$('#select_export_warehouse').on('change',function(){
	$('#export_warehouse').val($(this).val());
	$('#export_warehouse').trigger('change');
});

$('#export_warehouse').on('change', function () 
{
	var dtable = $('#allocationNonBuyerTable').dataTable().api();
	dtable.draw();
})

function hapus(url) 
{
	bootbox.confirm("Are you sure want to delete this ?.", function (result) 
	{
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "put",
				url: url,
				beforeSend: function () {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function () {
					$.unblockUI();
				},
			})
			.done(function () 
			{
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#allocationNonBuyerTable').DataTable().ajax.reload();
			});
		}
	});


}

$('#item_allocationName').click(function () 
{
	var warehouse_id = $('#select_warehouse').val();
	Allocationlov('item_allocation', 'document_no_allocation', 'po_buyer_allocation',warehouse_id, '/accessories/allocation/non-buyer/material-stock-picklist?');
});

$('#item_allocationButtonLookup').click(function ()
{
	var warehouse_id = $('#select_warehouse').val();
	Allocationlov('item_allocation', 'document_no_allocation', 'po_buyer_allocation',warehouse_id, '/accessories/allocation/non-buyer/material-stock-picklist?');
});

$('#_qty_booking').on('change', function () 
{
	var qty_booking 	= $('#_qty_booking').val();
	var _qty_booking 	= $('#__qty_booking').val();
	var _temp_qty		= $('#_temp_qty').val();

	var __temp = _temp_qty - qty_booking;
	if (__temp < 0) 
	{
		$('#_qty_booking').val(_qty_booking);
		$("#alert_warning").trigger("click", 'Qty booking must be less than available qty.');
		return;
	}
});

function Allocationlov(name, name2, name3,warehouse_id, url) 
{
	var search 		= '#' + name + 'Search';
	var search2 	= '#' + name2 + 'Search2';
	var search3 	= '#' + name3 + 'Search3';
	var list 		= '#' + name + 'List';
	var item_id 	= '#' + name + 'Id';
	var item_name 	= '#' + name + 'Name';
	var modal 		= '#' + name + 'Modal';
	var table 		= '#' + name + 'Table';
	var buttonSrc 	= '#ButtonSrc';
	var buttonDel 	= '#' + name + 'ButtonDel';

	function itemAjax() 
	{
		var q = $(search).val();
		var q2 = $(search2).val();
		var q3 = $(search3).val();

		$(table).addClass('hidden');
		$(modal).find('.shade-screen').removeClass('hidden');
		$(modal).find('.form-search').addClass('hidden');

		$.ajax({
			url: url + '&document_no=' + q + '&po_buyer=' + q2 + '&item_code=' + q3+ '&warehouse_id=' + warehouse_id
		})
			.done(function (data) {
				$(table).html(data);
				pagination(name);
				$(search).focus();
				//$(search2).val('');
				$(table).removeClass('hidden');
				$(modal).find('.shade-screen').addClass('hidden');
				$(modal).find('.form-search').removeClass('hidden');

				$(table).find('.btn-choose').on('click', chooseItem);
			});
	}

	function chooseItem() 
	{
		var id 			= $(this).data('id');
		var name 		= $(this).data('name');

		var item 		= $(this).data('item');
		var document 	= $(this).data('documentno');
		var category 	= $(this).data('category');
		var locator_id 	= $(this).data('locatorid');
		var locator 	= $(this).data('locator');
		var qtycurrency = $(this).data('qtycurrency');
		var qty 		= $(this).data('qty');
		var uom 		= $(this).data('uom');
		var status 		= $(this).data('status');

		$(item_id).val(id);
		$(item_name).val(name);

		$('#document_no').removeClass('hidden');
		$('#category').removeClass('hidden');
		$('#locator').removeClass('hidden');
		$('#qty_stock').removeClass('hidden');
		$('#uom').removeClass('hidden');

		$('#_document_no').html(document);
		$('#_category').html(category);
		$('#_locator').html(locator);
		$('#_qty_stock').html(qtycurrency);
		$('#_uom').html(uom);

		$('#conversion_status').val(status);
		$('#_temp_qty').val(qty);
		$('#___item_code').val(item);
		$('#___locator').val(locator_id);
		$('#__document_no').val(document);
		$('#__category').val(category);
		$('#__uom').val(uom);


	}

	function pagination() {
		$(modal).find('.pagination a').on('click', function (e) {
			var params = $(this).attr('href').split('?')[1];
			url = $(this).attr('href') + (params == undefined ? '?' : '');

			e.preventDefault();
			itemAjax();
		});
	}

	//$(search).val("");
	$(buttonSrc).unbind();
	$(search).unbind();
	$(search2).unbind();
	$(search3).unbind();

	$(buttonSrc).on('click', itemAjax);

	$(search).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(search2).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(search3).on('keypress', function (e) {
		if (e.keyCode == 13)
			itemAjax();
	});

	$(buttonDel).on('click', function () {
		$(item_id).val('');
		$(item_name).val('');

	});

	itemAjax();
}

