
$(function () 
{
	var page = $('#page').val();

	if(page == 'index')
	{
		var flag_msg = $('#flag_msg').val();
		if (flag_msg == 'success') $("#alert_success").trigger("click", 'Data successfully saved.');
		
		$('#accessories_material_stock_on_the_fly_table').DataTable({
			dom: 'Bfrtip',
			processing: true,
			serverSide: true,
			pageLength:100,
			ajax: {
				type: 'GET',
				url: '/accessories/material-stock-on-the-fly/data',
			},
			columns: [
				{data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
				{data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
				{data: 'name', name: 'name',searchable:false,visible:true,orderable:true},
				{data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
				{data: 'supplier_code', name: 'supplier_code',searchable:true,visible:true,orderable:true},
				{data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
				{data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
				{data: 'type_stock', name: 'type_stock',searchable:true,visible:true,orderable:true},
				{data: 'po_buyer', name: 'po_buyer',searchable:true,visible:true,orderable:true},
				{data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
				{data: 'item_desc', name: 'item_desc',searchable:false,visible:true,orderable:true},
				{data: 'category', name: 'category',searchable:false,visible:true,orderable:true},
				{data: 'uom', name: 'uom',searchable:false,visible:true,orderable:true},
				{data: 'stock', name: 'stock',searchable:false,visible:true,orderable:false},
				{data: 'stock_arrival', name: 'stock_arrival',searchable:false,visible:true,orderable:false},
				{data: 'stock_reserved', name: 'stock_reserved',searchable:false,visible:true,orderable:false},
				{data: 'stock_available', name: 'stock_available',searchable:false,visible:true,orderable:false},
				{data: 'status_arrival', name: 'status_arrival',searchable:false,visible:true,orderable:false},
				{data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
			]
		});
	
		var dtable = $('#accessories_material_stock_on_the_fly_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dtable.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dtable.search("").draw();
				}
				return;
		});
		dtable.draw();

		

	}else
	{
		list_materials	= JSON.parse($('#materials').val());
		render();
	}
});

function render() 
{
	getIndex();
	$('#materials').val(JSON.stringify(list_materials));
	var tmpl = $('#material-stock-on-the-fly-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_materials };
	var html = Mustache.render(tmpl, data);
	$('#tbody-material-stock-on-the-fly').html(html);

	bind();
}

function getIndex() 
{
	for (idx in list_materials) 
	{
		list_materials[idx]['_id'] = idx;
		list_materials[idx]['no'] = parseInt(idx) + 1;
	}
}

function bind() 
{
	$('.btn-delete-item').on('click', deleteItem);
}

function deleteItem() 
{
	var i = $(this).data('id');
	list_materials.splice(i, 1);
	render();
}

function hapus(url)
{
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$.ajax({
		type: "POST",
		url: url,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		success: function () {
			$.unblockUI();
		},
		error: function () {
			$.unblockUI();
			$("#alert_error").trigger("click", 'Please contact ICT');
		}
	})
	.done(function ($result) {
		$("#alert_success").trigger("click", 'Data successfully deleted.');
		$('#accessories_material_stock_on_the_fly_table').DataTable().ajax.reload();
	});
}
	

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	//$('#upload_file_allocation').submit();
	$.ajax({
		type: "post",
		url: $('#upload_file_allocation').attr('action'),
		data: new FormData(document.getElementById("upload_file_allocation")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully.');
			list_materials = response;
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_file_allocation').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) 
	{
		$('#upload_file_allocation').trigger('reset');
		render();
	});

});


$('#upload_button_delete').on('click', function () 
{
	$('#upload_delete_file').trigger('click');
});

$('#upload_delete_file').on('change', function () 
{
	//$('#upload_file_allocation').submit();
	//alert($('#upload_delete_file_stock').attr('action'))

	$.ajax({
		type: "post",
		url: $('#upload_delete_file_stock').attr('action'),
		data: new FormData(document.getElementById("upload_delete_file_stock")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.blockUI({
				message: '<i class="icon-spinner4 spinner"></i>',
				overlayCSS: {
					backgroundColor: '#fff',
					opacity: 0.8,
					cursor: 'wait'
				},
				css: {
					border: 0,
					padding: 0,
					backgroundColor: 'transparent'
				}
			});
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info_2").trigger("click", 'Upload successfully.');
			list_materials = response;
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_delete_file_stock').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function (response) 
	{
		$('#upload_delete_file_stock').trigger('reset');
		render();
	});

});
