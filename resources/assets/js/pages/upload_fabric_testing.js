
$(function()
{
	list_upload_fabric_testings = JSON.parse($('#upload_fabric_testings').val());
	render();
});

$('#upload_button').on('click', function () 
{
	$('#upload_file').trigger('click');
});

$('#upload_file').on('change', function () 
{
	$.ajax({
		type: "post",
		url: $('#upload_fabric_testing').attr('action'),
		data: new FormData(document.getElementById("upload_fabric_testing")),
		processData: false,
		contentType: false,
		beforeSend: function () {
			list_upload_fabric_testings = [];
			$.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
		},
		complete: function () {
			$.unblockUI();
		},
		success: function (response) {
			$("#alert_info").trigger("click", 'Upload Master Data Fabric Testing Successfully.');
			for(idx in response){
				var input = response[idx];
				list_upload_fabric_testings.push(input);
			}
		},
		error: function (response) {
			$.unblockUI();
			$('#upload_fabric_testing').trigger('reset');
			if (response['status'] == 500)
				$("#alert_error").trigger("click", 'Please Contact ICT.');

			if (response['status'] == 422)
				$("#alert_error").trigger("click", response.responseJSON);

		}
	})
	.done(function () {
		$('#upload_fabric_testing').trigger('reset');
		render();
	});

})

function render() 
{
	getIndex();
	$('#upload_fabric_testings').val(JSON.stringify(list_upload_fabric_testings));
	var tmpl = $('#upload-fabric-testing-table').html();
	Mustache.parse(tmpl);
	var data = { item: list_upload_fabric_testings };
	var html = Mustache.render(tmpl, data);
	$('#tbody-upload-fabric-testing').html(html);
}

function getIndex() 
{
	for (idx in list_upload_fabric_testings) {
		list_upload_fabric_testings[idx]['_id'] = idx;
		list_upload_fabric_testings[idx]['no'] = parseInt(idx) + 1;
	}
}
