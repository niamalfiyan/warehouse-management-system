$(function()
{
    $('#daily_material_arrival_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        scroller:true,
        deferRender:true,
        ajax: {
            type: 'GET',
            url: '/accessories/report/daily-material-arrival/data',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:true},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:true},
            {data: 'name', name: 'name',searchable:true,visible:true,orderable:true},
            {data: 'no_invoice', name: 'no_invoice',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'item_desc', name: 'item_desc',searchable:true,visible:true,orderable:true},
            {data: 'category', name: 'category',searchable:true,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:true,visible:true,orderable:false},
            {data: 'qty_carton', name: 'qty_carton',searchable:false,visible:true,orderable:false},
            {data: 'total_carton_in', name: 'total_carton_in',searchable:false,visible:true,orderable:false},
            {data: 'qty_ordered', name: 'qty_ordered',searchable:false,visible:true,orderable:false},
			{data: 'prepared_status', name: 'prepared_status',searchable:false,visible:true,orderable:false},
          
        ]
    });

    var dtable = $('#daily_material_arrival_table').dataTable().api();
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("keyup", function (e) { // Bind our desired behavior
            // If the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dtable.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dtable.search("").draw();
            }
            return;
    });
    dtable.draw();
    
    $('#select_warehouse').on('change',function(){
        dtable.draw();
    });

    $('#start_date').on('change',function(){
        dtable.draw();
    });

    $('#end_date').on('change',function(){
        dtable.draw();
    });
})