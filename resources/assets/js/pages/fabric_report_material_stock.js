$(function () 
{
    var page = $('#page').val();

    if(page == 'index')
    {
        var fabricSummaryStockTable =  $('#summary_stock_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: '/fabric/report/material-stock/data',
                data: function(d) {
                    return $.extend({}, d, {
                        "warehouse"             : $('#select_warehouse').val(),
                        "type_stock_erp_code"   : $('#select_type_stock').val()
                    });
               }
            },
            fnCreatedRow: function (row, data, index) {
                var info = fabricSummaryStockTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'warehouse_id', name: 'warehouse_id',searchable:false,visible:true,orderable:false},
                {data: 'category_stock', name: 'category_stock',searchable:false,visible:true,orderable:true},
                {data: 'supplier_code', name: 'supplier_code',searchable:true,visible:true,orderable:true},
                {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
                {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
                {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
                {data: 'item_desc', name: 'item_desc',searchable:true,visible:true,orderable:true},
                {data: 'color', name: 'color',searchable:true,visible:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
                {data: 'qty_order', name: 'qty_order',searchable:false,visible:true,orderable:true},
                {data: 'available_qty', name: 'available_qty',searchable:false,visible:true,orderable:true},
                {data: 'source', name: 'source',searchable:false,visible:true,orderable:true},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#summary_stock_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
        
        $('#select_warehouse').on('change',function()
        {
            var warehouse_id = $(this).val();
            $('#_warehouse_id').val(warehouse_id);
            dtable.draw();
        });

        $('#select_type_stock').on('change',function(){
            dtable.draw();
        });
    
        
    }else
    {
        var url_material_stock_detail_data  = $('#url_material_stock_detail_data').val();
        var fabricDetailStockTable =  $('#detail_stock_table').DataTable({
            processing: true,
            serverSide: true,
            pageLength:100,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url_material_stock_detail_data,
                data: function(d) {
                    return $.extend({}, d, {
                        "summar_stock_fabric_id" : $('#summar_stock_fabric_id').val()
                    });
               }
               
            },
            fnCreatedRow: function (row, data, index) {
                var info = fabricDetailStockTable.page.info();
                var value = index+1+info.start;
                $('td', row).eq(0).html(value);
            },
            columns: [
                {data: 'material_stock_id', name: 'material_stock_id',searchable:true,visible:false,orderable:false},
                {data: null, sortable: false, orderable: false, searchable: false},
                {data: 'barcode_supplier', name: 'barcode_supplier',searchable:true,visible:true},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true},
                {data: 'load_actual', name: 'load_actual',searchable:true,visible:true,orderable:true},
                {data: 'begin_width', name: 'begin_width',searchable:false,visible:true,orderable:false},
                {data: 'middle_width', name: 'middle_width',searchable:false,visible:true,orderable:false},
                {data: 'end_width', name: 'end_width',searchable:false,visible:true,orderable:false},
                {data: 'actual_width', name: 'actual_width',searchable:false,visible:true},
                {data: 'qty_order', name: 'qty_order',searchable:false,visible:true},
                {data: 'available_qty', name: 'available_qty',searchable:false,visible:true,orderable:true},
                {data: 'inspect_lot_result', name: 'inspect_lot_result',searchable:false,visible:true,orderable:true},
                {data: 'qc_result', name: 'qc_result',searchable:false,visible:true,orderable:true},
                {data: 'orgin_stock', name: 'orgin_stock',searchable:false,visible:true,orderable:false},
                {data: 'lokasi_terakhir', name: 'lokasi_terakhir',searchable:true,visible:true,orderable:true},
                {data: 'last_update', name: 'last_update',searchable:true,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#detail_stock_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    }
})

function recalculate(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "put",
            url: url,
            beforeSend: function () {
                swal.close();
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function () {
                $.unblockUI();
            },
            error: function (response) {
                $.unblockUI();
                if (response['status'] == 422)
                    $("#alert_info_2").trigger("click", response['responseJSON']);
            }
        })
        .done(function () {
            $("#alert_success").trigger("click", 'Data successfully recalculate.');
            $('#summary_stock_table').DataTable().ajax.reload();
        });


}

function unlocked(url) 
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
            type: "get",
            url: url,
            beforeSend: function () {
                $.blockUI({
                    message: '<i class="icon-spinner4 spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'transparent'
                    }
                });
            },
            success: function (response) {
                $.unblockUI();
            },
        })
        .done(function (response) {
            $("#alert_success").trigger("click", 'Data successfully unlocked');
        });
}

function history(url)
{
    $('#historyModal').modal();
    
    function itemAjax()
    {
        $('#historyTable').addClass('hidden');
        $('#historyModal').find('.shade-screen').removeClass('hidden');
    
        $.ajax({
            url: url,
        })
        .done(function (data) 
        {
            $('#historyTable').html(data);
            $('#historyTable').removeClass('hidden');
            $('#historyModal').find('.shade-screen').addClass('hidden');
            pagination('detail');
        });
    }

    function pagination() 
    {
        $('#detailModal').find('.pagination a').on('click', function (e) {
            var params = $(this).attr('href').split('?')[1];
            url = $(this).attr('href') + (params == undefined ? '?' : '');

            e.preventDefault();
            itemAjax();
        });
    }
    itemAjax();
}