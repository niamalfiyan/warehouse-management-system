$(function()
{
    var page = $('#page').val();
    if(page == 'index')
    {
        var active_tab = $('#active_tab').val();
        if(active_tab == 'handover')
		{
        
            handoverTables();
            var dTableHandover = $('#daily_material_handover_table').dataTable().api();
            $(".dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dTableHandover.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dTableHandover.search("").draw();
                    }
                    return;
            });
            dTableHandover.draw();
            
            $('#select_warehouse').on('change',function(){
                dTableHandover.draw();
            });
        
            $('#start_date').on('change',function(){
                dTableHandover.draw();
            });
        
            $('#end_date').on('change',function(){
                dTableHandover.draw();
            });
        } else if(active_tab == 'moveorder')
		{
        
            moveOrderTable();
            var dTableMoveOrder = $('#daily_material_move_order_table').dataTable().api();
            $(".dataTables_filter input")
                .unbind() // Unbind previous default bindings
                .bind("keyup", function (e) { // Bind our desired behavior
                    // If the user pressed ENTER, search
                    if (e.keyCode == 13) {
                        // Call the API search function
                        dTableMoveOrder.search(this.value).draw();
                    }
                    // Ensure we clear the search if they backspace far enough
                    if (this.value == "") {
                        dTableMoveOrder.search("").draw();
                    }
                    return;
            });
            dTableMoveOrder.draw();
            
            $('#select_warehouse').on('change',function(){
                dTableMoveOrder.draw();
            });
        
            $('#start_date').on('change',function(){
                dTableMoveOrder.draw();
            });
        
            $('#end_date').on('change',function(){
                dTableMoveOrder.draw();
            });
        }
    }else
    {
        var url_fabric_report_daily_material_handover = $('#url_fabric_report_daily_material_handover').val();
        $('#daily_data_material_handover_table').DataTable({
            dom: 'Bfrtip',
            processing: true,
            serverSide: true,
            pageLength:100,
            scroller:true,
            deferRender:true,
            ajax: {
                type: 'GET',
                url: url_fabric_report_daily_material_handover
            },
            columns: [
                {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
                {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:false},
                {data: 'user_created', name: 'user_created',searchable:false,visible:true,orderable:false},
                {data: 'barcode', name: 'barcode',searchable:true,visible:true,orderable:false},
                {data: 'nomor_roll', name: 'nomor_roll',searchable:true,visible:true,orderable:true},
                {data: 'batch_number', name: 'batch_number',searchable:true,visible:true,orderable:true},
                {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
                {data: 'qty_handover', name: 'qty_handover',searchable:false,visible:true,orderable:false},
                {data: 'warehouse_to_id', name: 'warehouse_to_id',searchable:false,visible:true,orderable:false},
                {data: 'user_receive', name: 'user_receive',searchable:false,visible:true,orderable:false},
                {data: 'receive_date', name: 'receive_date',searchable:false,visible:true,orderable:false},
                {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
            ]
        });
    
        var dtable = $('#daily_data_material_handover_table').dataTable().api();
        $(".dataTables_filter input")
            .unbind() // Unbind previous default bindings
            .bind("keyup", function (e) { // Bind our desired behavior
                // If the user pressed ENTER, search
                if (e.keyCode == 13) {
                    // Call the API search function
                    dtable.search(this.value).draw();
                }
                // Ensure we clear the search if they backspace far enough
                if (this.value == "") {
                    dtable.search("").draw();
                }
                return;
        });
        dtable.draw();
    } 
    
    $('#updateForm').submit(function (event) 
	{
		event.preventDefault();
        
        $('#editModal').modal('hide');
        bootbox.confirm("Are you sure want to update this data ?.", function (result) 
		{
			if (result) {
				$.ajax({
					type: "POST",
					url: $('#updateForm').attr('action'),
					data: $('#updateForm').serialize(),
					beforeSend: function () {
						$.blockUI({
							message: '<i class="icon-spinner4 spinner"></i>',
							overlayCSS: {
								backgroundColor: '#fff',
								opacity: 0.8,
								cursor: 'wait'
							},
							css: {
								border: 0,
								padding: 0,
								backgroundColor: 'transparent'
							}
						});
					},
					complete: function () {
						$.unblockUI();
					},
					success: function () {
						$('#updateForm').trigger("reset");
					},
					error: function (response) {
						$.unblockUI();
						if (response['status'] == 500) $("#alert_error").trigger("click", 'Please contact ICT');
                        if (response['status'] == 422 || response['status'] == 400) $("#alert_warning").trigger("click", response.responseJSON);
                        $('#editModal').modal();
					}
				})
				.done(function () 
				{
					$("#alert_success").trigger("click", 'Data successfully updated');
					$('#daily_material_handover_table').DataTable().ajax.reload();
				});
			}
		});

	});
});
function handoverTables()
{
    $('#daily_material_handover_table').DataTable().destroy();
	$('#daily_material_handover_table tbody').empty();
    $('#daily_material_handover_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        ajax: {
            type: 'GET',
            url: '/fabric/report/daily-material-out-non-cutting/data-handover',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:false},
            {data: 'complete_date', name: 'complete_date',searchable:false,visible:true,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true,visible:true,orderable:true},
            {data: 'no_pt', name: 'no_pt',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_handover', name: 'total_qty_handover',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_outstanding', name: 'total_qty_outstanding',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_supply', name: 'total_qty_supply',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });
}

function moveOrderTable()
{
    $('#daily_material_move_order_table').DataTable().destroy();
	$('#daily_material_move_order_table tbody').empty();
    $('#daily_material_move_order_table').DataTable({
        dom: 'Bfrtip',
        processing: true,
        serverSide: true,
        pageLength:100,
        autoWidth: false,
        ajax: {
            type: 'GET',
            url: '/fabric/report/daily-material-out-non-cutting/data-move-order',
            data: function(d) {
                return $.extend({}, d, {
                    "warehouse"     : $('#select_warehouse').val(),
                    "start_date"    : $('#start_date').val(),
                    "end_date"      : $('#end_date').val(),
                });
           }
        },
        columns: [
            {data: 'id', name: 'id',searchable:true,visible:false,orderable:false},
            {data: 'created_at', name: 'created_at',searchable:false,visible:true,orderable:false},
            {data: 'complete_date', name: 'complete_date',searchable:false,visible:true,orderable:false},
            {data: 'warehouse_id', name: 'warehouse_id',searchable:true,visible:true,orderable:true},
            {data: 'no_pt', name: 'no_pt',searchable:true,visible:true,orderable:true},
            {data: 'document_no_out', name: 'document_no_out',searchable:true,visible:true,orderable:true},
            {data: 'document_no', name: 'document_no',searchable:true,visible:true,orderable:true},
            {data: 'supplier_name', name: 'supplier_name',searchable:true,visible:true,orderable:true},
            {data: 'item_code', name: 'item_code',searchable:true,visible:true,orderable:true},
            {data: 'color', name: 'color',searchable:false,visible:true,orderable:true},
            {data: 'uom', name: 'uom',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_handover', name: 'total_qty_handover',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_outstanding', name: 'total_qty_outstanding',searchable:false,visible:true,orderable:false},
            {data: 'total_qty_supply', name: 'total_qty_supply',searchable:false,visible:true,orderable:false},
            {data: 'action', name: 'action',searchable:false,visible:true,orderable:false},
        ]
    });
}


function changeTab(status)
{
    $('#active_tab').val(status).trigger('change');
}

$('#active_tab').on('change',function()
{
	var active_tab = $('#active_tab').val();
	if(active_tab == 'handover')
	{
		handoverTables();
        var dTableHandover = $('#daily_material_handover_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dTableHandover.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dTableHandover.search("").draw();
				}
				return;
		});
		dTableHandover.draw();
		$('#select_warehouse').on('change',function(){
            dTableHandover.draw();
		});
		
		$('#start_date').on('change',function(){
            dTableHandover.draw();
		});
		
		$('#end_date').on('change',function(){
            dTableHandover.draw();
        });

		
	}else if( active_tab == 'moveorder')
	{
		moveOrderTable();
        var dTableMoveOrder = $('#daily_material_move_order_table').dataTable().api();
		$(".dataTables_filter input")
			.unbind() // Unbind previous default bindings
			.bind("keyup", function (e) { // Bind our desired behavior
				// If the user pressed ENTER, search
				if (e.keyCode == 13) {
					// Call the API search function
					dTableMoveOrder.search(this.value).draw();
				}
				// Ensure we clear the search if they backspace far enough
				if (this.value == "") {
					dTableMoveOrder.search("").draw();
				}
				return;
		});
		dTableMoveOrder.draw();
		$('#select_warehouse').on('change',function(){
            dTableMoveOrder.draw();
		});
		
		$('#start_date').on('change',function(){
            dTableMoveOrder.draw();
		});
		
		$('#end_date').on('change',function(){
            dTableMoveOrder.draw();
        });
	} 
});


function edit(url)
{
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: "get",
        url: url,
        beforeSend: function () 
        {
            $.blockUI({
                message: '<i class="icon-spinner4 spinner"></i>',
                overlayCSS: {
                    backgroundColor: '#fff',
                    opacity: 0.8,
                    cursor: 'wait'
                },
                css: {
                    border: 0,
                    padding: 0,
                    backgroundColor: 'transparent'
                }
            });
        },
        success: function () {
            $.unblockUI();
        },
        error: function (response) {
            $.unblockUI();
            
            if(response.status == 500)
            {
                $("#alert_error").trigger("click", 'Please contact ict.');
                return false;
            }
        },
    })
    .done(function (response) {
        $('#update_id').val(response.id);
        $('#update_no_pt').val(response.handover_document_no);
        $('#update_no_bc').val(response.no_bc);
        $('#update_uom').val(response.uom);
        $('#update_total_qty_handover').val(response.total_qty_handover);
        $('#update_old_total_qty_handover').val(response.total_qty_handover);
        $('#update_total_qty_supplied').val(response.total_qty_supply);
        $('#update_header').text(response.supplier_name+' '+response.document_no+' '+response.item_code);
        $('#editModal').modal({
            backdrop: 'static',
            keyboard: false  // to prevent closing with Esc button (if you want this too)
        });
    });
}

$('#update_total_qty_handover').keypress(function (e) {

    if (e.keyCode > 31 && (e.keyCode < 46 || e.keyCode > 57)) {
        return false;
    }
    return true;
});

$('#update_total_qty_handover').change(function()
{
    var total_qty_handover              = parseFloat($(this).val());
    var total_qty_supplied              = parseFloat($('#update_total_qty_supplied').val());
    var update_old_total_qty_handover   = parseFloat($('#update_old_total_qty_handover').val());
    var _temp                           = total_qty_handover - total_qty_supplied;
 
    if(_temp < 0)
    {
        $("#alert_info").trigger("click", 'Total handover should be more or equals than than total supplied');
        $(this).val(update_old_total_qty_handover);
        return false;
    }
    
});


function destroy(url)
{
    bootbox.confirm("Are you sure want to delete this data ?.", function (result) 
    {
		if (result) {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				type: "post",
				url: url,
                beforeSend: function () 
                {
					$.blockUI({
						message: '<i class="icon-spinner4 spinner"></i>',
						overlayCSS: {
							backgroundColor: '#fff',
							opacity: 0.8,
							cursor: 'wait'
						},
						css: {
							border: 0,
							padding: 0,
							backgroundColor: 'transparent'
						}
					});
				},
				success: function (response) {
					$.unblockUI();
                },
                error: function (response) {
                    $.unblockUI();
                    
                    if(response.status == 422)
                    {
                        $("#alert_error").trigger("click", 'Cannot delete all data due some material already receiving on destination.');
                        return false;
                    }

                    if(response.status == 500)
                    {
                        $("#alert_error").trigger("click", 'Please contact ict.');
                        return false;
                    }
				},
            })
            .done(function () {
				$("#alert_success").trigger("click", 'Data successfully deleted.');
				$('#daily_material_handover_table').DataTable().ajax.reload();
			});
		}
	});
}